import numpy as np
import pyaudio as pa
import xarray as xr
from bi_aural import probe_signal, pa_np_interface, loop_buffer, echo_processor
import numpy as np
import matplotlib.pyplot as plt
import time
import sys
from io import BytesIO
import queue
from cycler import cycler

class MultiChirp:

    def __init__(self):
        # Create a probe signal
        fc = 7000  # Hz
        bw = 12000  # Hz
        self.fs = int(44.1e3)  # Hz
        duty_cycle = 0.5
        T = duty_cycle + 0.3  # second
        self.num_cycles = 6  # seconds
        self.rough_time = self.num_cycles * T + 0.1
        # Specify recording parameters
        lfm = probe_signal.LFM(duty_cycle, fc, bw, T, self.fs)
        # Matched filter specifications
        replica = probe_signal.LFM(duty_cycle, fc, bw, duty_cycle, self.fs).signal

        f_bounds = (fc - bw / 2, fc + bw / 2)
        self.sp_er = echo_processor.Processor(replica, f_bounds, self.fs)
        # Recording specifications
        self.samples_per_chirp = int(T * self.fs)

        # input data
        self.read_samples = int(2 ** 10)
        self.looper = loop_buffer.LoopBuffer(lfm.signal)
        self.chunck_size = int(self.read_samples *
                               self.looper.num_out_channels *
                               self.looper.num_bytes)

        # output method
        self.dc = pa_np_interface.Dechunker()
        # play and record callback
        self.q = queue.Queue()

    def __call__(self):
        """Run a single data collection run"""
        try:
            p = pa.PyAudio()
            device = self.find_UA101(p)
            # start up loop
            self.looper(self.num_cycles, self.chunck_size)
            callback = lambda *args: self._buffer_callback(self.looper, *args)
            stream = p.open(format=pa.paInt24,
                            channels=self.looper.num_out_channels,
                            rate=int(self.fs),
                            output=True,
                            input=True,
                            output_device_index=device,
                            input_device_index=device,
                            stream_callback=callback,
                            frames_per_buffer=self.read_samples)
            stream.start_stream()
            completed_samples = self.run_loop()
        finally:
            stream.stop_stream()
            stream.close()
            p.terminate()
        return completed_samples


    def find_UA101(self, p):
        """Find device with UA-101 in string"""
        for i in range(p.get_device_count()):
            name = p.get_device_info_by_index(i)['name']
            if 'UA-101' in name:
                return i

    def _buffer_callback(self, lfm_out, in_data, frame_count, time_info,
                        status):
        self.q.put([in_data])
        out_data = lfm_out.read()
        return (out_data, pa.paContinue)


    def run_loop(self):
        """Put pyaudio stuff in a try block :)"""
        elapsed_time = 0
        index_start = 0
        current_cycle = 0
        completed_cycles = []
        samples = np.zeros((self.looper.num_out_channels,
                            self.samples_per_chirp),
                        dtype=np.float32)
        # make samples a power of 2 for ease of filtering
        start_time = time.time()
        while time.time() - start_time < self.rough_time:
            # Rely on callback to break loop
            time.sleep(0.1)
            while not self.q.empty():
                if index_start + self.read_samples < self.samples_per_chirp:
                    next_samples = slice(index_start,
                                         index_start + self.read_samples)
                    current_buffer = self.dc.buf_to_np(b''.join(self.q.get()))
                    current_buffer = current_buffer.T
                    samples[:, next_samples] = current_buffer
                    index_start += self.read_samples
                else:
                    amount_extra = int(index_start + self.read_samples -
                                       self.samples_per_chirp)
                    amount_space = int(self.read_samples - amount_extra)
                    samples[:, index_start: ] = current_buffer[:,
                                                               :amount_space]
                    result = samples.copy()
                    result = self.sp_er(result)
                    # Save the rest of the buffer to the new samples array
                    samples[:, :amount_extra] = current_buffer[:,
                                                               amount_space:]
                    completed_cycles.append(result)
                    index_start = amount_extra

        reading = self._cleanup(completed_cycles)

        return reading

    def _cleanup(self, completed_cycles):
        """Return a bunch of readings as a channel list"""
        t_axes = []
        t_max = 1e12
        for i, cs in enumerate(completed_cycles):
            # Don't know why we are adding a buffer each time
            t_range = np.arange(cs.shape[1]) - self.read_samples * i
            t_range = t_range / self.fs
            t_max = min(t_max, np.max(t_range))  # used to create a common axis
            t_axes.append(t_range)

        readings = []

        for ta, cc in zip(t_axes, completed_cycles):
            indmin, indmax = np.searchsorted(ta, (0, t_max))
            readings.append(cc[:, indmin: indmax])

        readings = np.array(readings)
        t_axis = ta[indmin: indmax]
        da = xr.DataArray(readings, coords=[range(readings.shape[0]),
                                            range(readings.shape[1]),
                                            t_axis],
                                    dims=['sample', 'channel', 'time'])
        return da


if __name__ == '__main__':
    chirper = MultiChirp()
    completed_samples = chirper()

    if True:
        fig, ax = plt.subplots()
        all_max = np.max(np.array([np.max(np.abs(cs)) for cs in completed_samples]))
        ax.set_prop_cycle(cycler('color', ['b', 'g']))
        for i, cs in completed_samples.groupby('sample'):
            ax.plot(cs.time * 1e3, np.abs(cs.T) / all_max + 0.5 * i)
        ax.set_xlim(45, 65)
        ax.grid()

    else:
        from span_selector import MultiLine
        plot_er = MultiLine()
        plot_er(completed_samples)

    plt.show(block=False)
