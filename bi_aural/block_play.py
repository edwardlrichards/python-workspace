"""PyAudio Example: Play a WAVE file."""

import pyaudio
import wave
import sys

CHUNK = 1024 * 4

wave_file = 'output_3.wav'

p = pyaudio.PyAudio()

def find_UA101():
    """Find device with UA-101 in string"""
    for i in range(p.get_device_count()):
        name = p.get_device_info_by_index(i)['name']
        if 'UA-101' in name:
            return i

dev_ID = find_UA101()

with wave.open(wave_file, 'rb') as f:
    data = f.readframes(CHUNK)
    stream = p.open(format=p.get_format_from_width(f.getsampwidth()),
                    channels=f.getnchannels(),
                    rate=f.getframerate(),
                    output_device_index=dev_ID,
                    output=True)

    while data != '':
        stream.write(data)
        data = f.readframes(CHUNK)

stream.stop_stream()
stream.close()

p.terminate()
