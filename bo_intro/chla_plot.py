import giovanni_plots
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec

#Create a Hovmoller plot of measurement and climatology
fig = plt.figure(figsize=(8, 6))
gs = gridspec.GridSpec(100,100,bottom=0.18,left=0.18,right=0.88)
ax1 = plt.subplot(gs[:,:25])
ax2 = plt.subplot(gs[:,30:])

_ = giovanni_plots.hovmoller(plot_type='clim', cbar=False,
                             ylabel=True, ax=ax1, title=False)
ax1.set_title('climatology')
_ = giovanni_plots.hovmoller(plot_type='measured', cbar=True,
                             ylabel=False, ax=ax2, title=False)
ax2.set_title('measured')
_ = plt.suptitle('100 km average of coastal primary production',
                 fontsize=16)
plt.show(block=False)
