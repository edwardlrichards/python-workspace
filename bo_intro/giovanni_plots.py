import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import dates
from mpl_toolkits.basemap import Basemap
import os
from datetime import datetime

data_dir = '/enceladus0/e2richards/bo_chl_a'
cmap = plt.cm.RdBu_r
cmap_value = plt.cm.magma_r
#cmap_value = plt.cm.gnuplot2
cmap_value.set_under(color='w')


def parse_folder():
    """parse a folder and return lists of measurments and climatology files"""
    all_files = os.listdir(data_dir)
    all_files = [f for f in all_files if f[0] != '.']
    file_type = [_parse_fileName(f) for f in all_files]
    file_type = list(zip(*file_type))
    dates = list(file_type[0])
    is_clim = list(file_type[1])

    #create a dict of the climatalogical data
    clim = {'%02i'%d.month: f for d, f, i in zip(dates, all_files, is_clim) if i}
    #dict of measured data
    meas = {'%02i_%i'%(d.month, d.year): f for d, f, i
            in zip(dates, all_files, is_clim) if not i}

    return clim, meas

def _create_da(meas, clim, plot_type):
    """return a data array of appropriate data type"""
    lat_bounds = [29, 39]
    lon_bounds = [-127, -114]
    if plot_type == 'measured':
        load_file = os.path.join(data_dir, meas)
        data = xr.open_dataset(load_file)
        data = xr.DataArray(data.l3m_data.values, dims=['lat', 'lon'],
                            coords=[data.G3fakeDim0, data.G3fakeDim1])
    elif plot_type == 'clim':
        load_file = os.path.join(data_dir, clim)
        data = xr.open_dataset(load_file)
        data = xr.DataArray(data.l3m_data.values, dims=['lat', 'lon'],
                            coords=[data.G3fakeDim0, data.G3fakeDim1])
    elif plot_type == 'anomoly':
        clim_file = os.path.join(data_dir, clim)
        clim_data = xr.open_dataset(clim_file)
        clim_data = xr.DataArray(clim_data.l3m_data.values,
                                   dims=['lat', 'lon'],
                                   coords=[clim_data.G3fakeDim0,
                                           clim_data.G3fakeDim1])
        meas_file = os.path.join(data_dir, meas)
        meas_data = xr.open_dataset(meas_file)
        meas_data = xr.DataArray(meas_data.l3m_data.values,
                                   dims=['lat', 'lon'],
                                   coords=[meas_data.G3fakeDim0,
                                           meas_data.G3fakeDim1])
        data = meas_data - clim_data
    else:
        Exception('Unknown plot type')

    data = data.sel(lat=slice(lat_bounds[0], lat_bounds[1]),
                    lon=slice(lon_bounds[0], lon_bounds[1]))

    return data

def _mask_outofbounds(data):
    """mask invalid data points, salton sea and gulf of california"""
    slatI = (data.lat.values > 32) & (data.lat.values < 35)
    slonI = (data.lon.values > -116.4) & (data.lon.values < -115.4)
    sI = slatI[:, None] & slonI
    gclatI = (data.lat.values > 29.5) & (data.lat.values < 32)
    gclonI = (data.lon.values > -115) & (data.lon.values < -113.9)
    gcI = gclatI[:, None] & gclonI

    temp = np.ma.masked_invalid(data.values)
    temp.mask = np.bitwise_or(temp.mask, sI)
    temp.mask = np.bitwise_or(temp.mask, gcI)
    data.values = np.ma.filled(temp, np.nan)
    return data

def _add_nvalues(data, n):
    """Add the first n values of each lat bin"""
    lat_mean = []
    for l, val in data.groupby('lat'):
        temp = val.dropna('lon')
        lat_mean.append(np.sum(temp.isel(lon=slice(-int(n), None))))
    return np.array(lat_mean) / float(n)


def cali_map(month, year, plot_type='measured', masked=True):
    """plot giovanni data on a map of California
    plot_type: 'anomoly', 'measured', 'clim'
    """
    clim, meas = parse_folder()
    meas = meas['%02i_%i'%(month, year)]
    clim = clim['%02i'%(month)]

    data = _create_da(meas, clim, plot_type)
    if masked:
        data = _mask_outofbounds(data)
    fig, ax = plt.subplots()
    llcrnrlat=29
    llcrnrlon=-127
    urcrnrlat=39
    urcrnrlon=-113

    m = Basemap(projection='tmerc', resolution='i', lat_0=34, lon_0=-122,
                llcrnrlat=llcrnrlat, llcrnrlon=llcrnrlon,
                urcrnrlat=urcrnrlat, urcrnrlon=urcrnrlon)
    X, Y = np.meshgrid(data.lon, data.lat)
    X, Y = m(X, Y)
    topo = np.ma.masked_invalid(data.values)

    if plot_type == 'anomoly':
        pm = m.pcolormesh(X, Y, topo, cmap=cmap, vmin=-6, vmax=6)
    elif (plot_type == 'measured') | (plot_type == 'clim'):
        pm = m.pcolormesh(X, Y, topo, cmap=cmap_value, vmin=5, vmax=30)
    plt.colorbar(pm)
    m.drawcoastlines()
    m.drawparallels(np.arange(llcrnrlat,urcrnrlat,2), labels=[1,0,0,0])
    m.drawmeridians(np.arange(llcrnrlon,urcrnrlon,2), labels=[0,0,0,1])
    plt.show(block=False)
    return fig, ax



def hovmoller(plot_type='anomoly', cbar=True, ylabel=True, ax=None,
        title=True):
    """plot a hovmoller diagram from downloaded giovanni data"""
    num_points = 25
    clim, meas = parse_folder()
    lat_mean = []
    for m in meas:
        month = datetime.strptime(m, '%m_%Y').month
        data = _create_da(meas[m], clim['%02i'%month], plot_type)
        data = _mask_outofbounds(data)
        lat_mean.append(_add_nvalues(data, num_points))
    lat_mean = np.array(lat_mean)
    lat_axis = data.lat.values
    months = [datetime.strptime(m, '%m_%Y') for m in meas]
    date_nums = np.array(list(map(dates.date2num, months)))
    sortI = np.argsort(date_nums)
    date_nums = date_nums[sortI]
    lat_mean = lat_mean[sortI]
    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig = ax.get_figure()
    if plot_type == 'anomoly':
        cnt = ax.contourf(date_nums, lat_axis, lat_mean.T, 20, cmap=cmap, 
                          vmin=-5, vmax=5)
        if title:
            ax.set_title('Longitude averaged primary production anomoly')
    else:
        cnt = ax.contourf(date_nums, lat_axis, lat_mean.T, 20, cmap=cmap_value,
                          vmin=0, vmax=14)
        if title:
            ax.set_title('Longitude averaged primary production')
    #X, Y = np.meshgrid(lat_axis, date_nums)
    #cnt = ax.pcolormesh(X, Y, lat_mean, cmap=cmap, vmin=-5, vmax=5)
    if cbar:
        cb = plt.colorbar(cnt)
        cb.set_label('mg C / m**3')
    #xlabel
    if plot_type == 'clim':
        ax.xaxis.set_major_locator(dates.MonthLocator(bymonth=range(1,13,2)))
        ax.xaxis.set_minor_locator(dates.MonthLocator())
        ax.set_xlim(dates.date2num(datetime.strptime('10_2013', '%m_%Y')),
                    dates.date2num(datetime.strptime('09_2014', '%m_%Y')))
        hfmt = dates.DateFormatter('%b')
    else:
        ax.xaxis.set_major_locator(dates.MonthLocator(bymonth=range(1, 13, 2)))
        ax.xaxis.set_minor_locator(dates.MonthLocator())
        hfmt = dates.DateFormatter('%b/%y')
    ax.xaxis.set_major_formatter(hfmt)
    #rotate by 45 degrees
    if ylabel:
        ax.set_ylabel('Latitude')
    else:
        plt.setp(ax.get_yticklabels(), visible=False)
    import ipdb; ipdb.set_trace() # BREAKPOINT
    temp_label = ax.get_xticklabels()
    plt.show(block=False)
    ax.set_xticklabels(temp_label, rotation=60)
    plt.show(block=False)
    return fig, ax

def _parse_fileName(file_name):
    """Parse the file name, determine type and date"""
    start_date = datetime.strptime(file_name[:8], 'A%Y%j')
    end_date = datetime.strptime(file_name[8:15], '%Y%j')
    is_clim = (file_name[20:22] == 'MC')
    assert start_date.month == end_date.month
    return end_date, is_clim
