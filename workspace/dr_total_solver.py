import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

Tspread = np.array([600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600])

log_kH2 = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
log_kH = np.array([-16.335, -13.597, -11.538, -9.932, -8.644, -7.587, -6.705, -5.956, -5.313, -4.754, -4.264])
log_kH2O = np.array([18.631, 15.582, 13.287, 11.496, 10.060, 8.881, 7.897, 7.063, 6.346, 5.724, 5.179])
log_kOH = np.array([-2.568, -2.085, -1.724, -1.444, -1.222, -1.041, -0.891, -0.764, -0.656, -0.563, -0.482])
log_kO = np.array([-18.570, -15.446, -13.098, -11.269, -9.803, -8.603, -7.601, -6.752, -6.024, -5.392, -4.839])
log_kO2 = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
log_kCO = np.array([14.320, 12.948, 11.916, 11.109, 10.461, 9.928, 9.481, 9.101, 8.774, 8.488, 8.236])
log_kCH4 = np.array([1.993, 0.943, 0.138, -0.500, -1.018, -1.447, -1.807, -2.115, -2.379, -2.609, -2.810])
log_kMg = np.array([-6.801, -4.998, -3.654, -2.615, -1.828, -1.201, -0.683, -0.249, 0, 0, 0])
log_kSi = np.array([-31.385, -25.794, -21.604, -18.349, -15.747, -13.622, -11.852, -10.357, -9.078, -7.970, -7.003])
log_kSiO = np.array([13.393, 12.117, 11.154, 10.399, 9.790, 9.289, 8.867, 8.508, 8.197, 7.926, 7.686]) #gas
log_kSiO2 = np.array([26.806, 22.992, 20.128, 17.899, 16.114, 14.652, 13.433, 12.400, 11.514, 10.745, 10.070])
log_kMgO = np.array([-1.052, -0.348, 0.182, 0.597, 0.894, 1.129, 1.326, 1.493, 1.518, 1.327, 1.161])

P = 1e-4 #nebula pressure (overall)

initHe = 2.343e9

#Conc of He, (H,O,C,Si,Mg)(Lodders, 2003)

initC = np.array([2.431e10, 1.413e7, 7.079e6, 1.0e6, 1.020e6])

Atotal = np.sum([initC]) + initHe
Xatom = initC / Atotal
Ppress = Xatom * P

def pressure_objective(param):

    pH = np.sqrt(param[0]) * np.sqrt(kH)
    #pH = np.sqrt(param[0]) * kH
    pH2 = kH2 * param[0]
    pOH = kOH * np.sqrt(param[1]) * np.sqrt(kH)
    #pOH = kOH * np.sqrt(param[1]) * kH
    pH2O = kH2O * param[0] * np.sqrt(param[1])
    pCH4 = kCH4 * param[2] * param[0] ** 2
    pO = np.sqrt(kO) * np.sqrt(param[1])
    #pO = kO * np.sqrt(param[1])
    pOH = kOH * np.sqrt(param[0]) * np.sqrt(param[1])
    pCO = kCO * param[2] * np.sqrt(param[1])
    pO2 = kO2 * param[1]
    pSiO = kSiO * param[3] * np.sqrt(param[1])
    pSiO2 = kSiO2 * param[3] * param[1]
    pSi = param[3] * kSi
    pMg = param[4] * kMg
    pMgO = kMgO * param[4] * np.sqrt(param[1])
    HeFrac = initHe / param[5]

    sigmaH = pH + 2 * pH2 + pOH + 2 * pH2O + 4 * pCH4
    sigmaO = pO + pOH + pH2O + pCO + 2 * pO2 + pSiO + 2 * pSiO2
    sigmaC = pCO + pCH4
    sigmaSi = pSi + pSiO + pSiO2
    sigmaMg = pMg + pMgO

    pFrac = pH + pH2 + pOH + pH2O + pCH4 + pO + pOH + pCO + pO2 + pSiO\
             + pSiO2 + pSi + pMg + pMgO

    pFrac /= P
    pFrac += HeFrac

    n_est = param[5] * pFrac
    return np.array([sigmaH, sigmaO, sigmaC, sigmaSi, sigmaMg, n_est, pFrac])

def obj_function(log_param):
    """the objective function"""
    # convert from logarithmic to linear parameters
    lfug = 10 ** log_param
    output = pressure_objective(lfug)
    pp = P * initC / output[5]
    fugglescheck = (output[:5] - pp) / pp
    fugglescheck = np.hstack([fugglescheck, output[-1] - 1])

    return fugglescheck

#for iT in np.arange(Tspread.size):
fug_val = []
for iT in range(Tspread.size):
    kH2 = 10 ** log_kH2[iT]
    kH = 10 ** log_kH[iT]
    kH2O = 10 ** log_kH2O[iT]
    kOH = 10 ** log_kOH[iT]
    kO = 10 ** log_kO[iT]
    kO2 = 10 ** log_kO2[iT]
    kCO = 10 ** log_kCO[iT]
    kCH4 = 10 ** log_kCH4[iT]
    kMg = 10 ** log_kMg[iT]
    kSi = 10 ** log_kSi[iT]
    kSiO = 10 ** log_kSiO[iT] #gas
    kSiO2 = 10 ** log_kSiO2[iT] # (gas)
    kMgO = 10 ** log_kMgO[iT]

    # intital guess
    fH2_init =  Ppress[0] / 2
    fO2_init = ((Ppress[1] - Ppress[2]) / (kH2O * fH2_init)) ** 2
    aGr_init = Ppress[2] / (kCO * np.sqrt(fO2_init))
    aSi_init = Ppress[3] / (kSiO * np.sqrt(fO2_init))
    aMg_init = Ppress[4] / kMg

    initial_guess = np.array([fH2_init, fO2_init, aGr_init,
                              aSi_init, aMg_init, Atotal / 2])

    fug1 = fsolve(obj_function, np.log10(initial_guess))
    fug_val.append(fug1)
fug_val = np.array(fug_val)

fig, ax = plt.subplots()
for i, val in enumerate(['fH2', 'fO2', 'aGr', 'aSi', 'aMg']):
    ax.plot(Tspread, fug_val[:, i], label=val)
ax.set_ylabel('log fugacities')
ax.set_xlabel('Temperature, K')
ax.legend()
ax.grid()
plt.show(block=False)
