import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.signal import resample
from scipy.interpolate import interp1d, UnivariateSpline
from scipy.io import loadmat
from icepyks import pulse_signal, signal_interp
from icepyks.poodle.cosine_surface import CosineSurface
from icepyks.poodle.wni_batch import BatchWNI
from concurrent.futures import ThreadPoolExecutor
import os

curr_dir = os.path.dirname(__file__)
loadname = os.path.join(os.path.dirname(curr_dir),
                        'ym_corrispondance','cos_8height_RH3D.npz')

# setup the wave specific solver
hwave = 0.8
lwave = 10.
kwave = 2 * np.pi / lwave
attn = 0.  # dB / km
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# specify source and receiver location
rsrc = np.array([0., 0, -15])
rrcr = np.array([55., 0, -10])
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri

# load cos_ier result
rh_4amp = np.load(loadname)

# setup batch wni
wn_ier = BatchWNI(hwave, lwave, rsrc, rrcr)

# setup integration parameters
dx = 0.1
xbounds = (-300, 600)
xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]

# create the test signal and its fourier transform
test_signal, tsig = pulse_signal.pulse_q1(1500)
sig_NFFT = int(2 ** np.ceil(np.log2(test_signal.size) + 4))
frange = (500, 2500)  # generous bounds
NFFT = 2 ** 13

# make a signal interpolator
s_ier_obj = signal_interp.ContinuousSignal(test_signal, tsig)
s_ier_half = lambda t: s_ier_obj.half_derivative(t)

# setup time and frequency axis
faxis = np.arange(NFFT) / NFFT * 2 * (frange[-1] + 500)
df = faxis[1] - faxis[0]
dt = 1 / df / NFFT
taxis = np.arange(NFFT) * dt
fcompute = np.arange(frange[0], frange[1] + 1)

# compute scattered field with cos_ier
rinterp_numa = 200
num_eva = 5
numa = 2 ** 16
nvec = lambda a, freq: np.arange(np.ceil(-freq * lwave / 1500
                                    * (1 + a)) - num_eva, 
                                    np.floor(freq * lwave / 1500
                                    * (1 - a)) + num_eva);
all_vec = lambda f: np.unique(np.hstack([nvec(0, f), nvec(1, f)]))

def RH_interper(frun):
    """Solve for reflection coefficents and create an interpolator"""
    qvec = all_vec(frun)
    aaxis = np.arange(rinterp_numa) / rinterp_numa
    r_RH = []
    for a in aaxis:
        r_RH.append(cos_ier.r_RH(a, qvec, frun))
    r_RH = np.array(r_RH)
    r_RH = np.vstack([r_RH[::-1, ::-1], r_RH[1:, :]])
    aaxis = np.hstack([-aaxis[::-1], aaxis[1: ]])
    return cos_ier.create_rinterp(aaxis, r_RH, qvec)

ftest = 1500
wn_ier.read_coeff(ftest)
b_sum = np.sum([wn_ier.wni_sta(n) for n in wn_ier.ns])
rh = cos_ier.wn_3D(RH_interper(ftest),
                               ftest,
                               rsrc[[0, 2]],
                               rrcr[[0, 2]],
                               numa,
                               all_vec(ftest))

# compute scattered field using batch processing
wni_sta_FT = []
#wni_full = []
for f in fcompute:
    wn_ier.read_coeff(f)
    wni_sta_FT.append(np.sum([wn_ier.wni_sta(n) for n in wn_ier.ns]))
    #wni_full.append(np.sum([wn_ier.wn_integration(n, 15000) for n in ns]))

# plot the fourier transforms of greens functions
fig, ax = plt.subplots()
ax.plot(fcompute, np.real(wni_sta_FT), color='C0')
ax.plot(fcompute, np.imag(wni_sta_FT), '--', color='C0')
ax.plot(rh_4amp['fcompute'], np.real(rh_4amp['allf']), color='C1')
ax.plot(rh_4amp['fcompute'], np.imag(rh_4amp['allf']), '--', color='C1')
#ax.plot(fcompute, np.real(wni_full), color='C2')
#ax.plot(fcompute, np.imag(wni_full), '--', color='C2')

plt.show(block=False)

# 8 height surface TS
cos_ier.hwave = 0.8
hk_4ts = cos_ier.hk_spatial_3D_time(rrcr[[0, 2]],
                                    rsrc[[0, 2]],
                                    xaxis,
                                    taxis,
                                    s_ier_half)

rh_4ts, t_rh4 = cos_ier.make_timeseries(s_ier_obj,
                                        rh_4amp['allf'],
                                        rh_4amp['fcompute'])

b_4ts, t_b4 = cos_ier.make_timeseries(s_ier_obj,
                                        wni_sta_FT,
                                        fcompute)


# upsample timeseries before plotting
hk_4ts_up, t_hk_up = resample(hk_4ts, taxis.size * 4, t=taxis)
rh_4ts_up, t_rh4_up = resample(rh_4ts, t_rh4.size * 4, t=t_rh4)
b_4ts_up, t_b4_up = resample(b_4ts, t_b4.size * 4, t=t_b4)

# create differences plots
d1 = np.interp(t_rh4_up, t_hk_up, hk_4ts_up) - rh_4ts_up
d2 = np.interp(t_rh4_up, t_b4_up, b_4ts_up) - rh_4ts_up

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot((t_rh4_up - t_spec) * 1e3, rh_4ts_up / direct_amp,
             color='C1', label='RH')
axes[0].plot((t_b4_up - t_spec) * 1e3, b_4ts_up / direct_amp,
             color='C2', label='bRH')
axes[0].plot((t_hk_up - t_spec) * 1e3, hk_4ts_up / direct_amp,
             color='C4',label='nHK')

axes[1].plot((t_rh4_up - t_spec) * 1e3, d1 / direct_amp, color='C4')#, label='time HK')
axes[1].plot((t_rh4_up - t_spec) * 1e3, d2 / direct_amp, color='C2')#, label='time HK')
axes[1].set_xlim(-1, 6)
axes[0].set_ylim(-1, 1)
axes[1].set_ylim(-1, 1)
axes[0].set_ylabel('pressure, mPa')
axes[1].set_xlabel('time, ms')
axes[0].set_title('0.4 m amplitude surface scatter time series')
axes[1].set_title('Difference from Rayleigh Hypothesis')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)

plt.show(block=False)
