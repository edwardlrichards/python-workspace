import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.signal import resample
from scipy.interpolate import interp1d, UnivariateSpline
from scipy.io import loadmat
from icepyks import pulse_signal, signal_interp, surface_integral, f_synthesis
from icepyks.surfaces import sin_surface
from icepyks.poodle.cosine_surface import CosineSurface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field
from icepyks.poodle import wn_synthesis
from concurrent.futures import ThreadPoolExecutor
import os

# setup the batch load parameters
hwave = 0.8
lwave = 10.
#signal parameter
fc = 1.5e3
frange = (500, 2500)  # generous bounds
save_dir = os.path.join(r'/enceladus0', 'e2richards', 'rayliegh_rs',
                        'wh_%02i_wl%02i'%(hwave * 10, lwave * 10))

# setup phase of wave
w_surf = np.sqrt(9.81 * 2 * np.pi / lwave)
#wave_time = 2.5  # seconds
wave_time = 0  # seconds
wave_phase = wave_time * w_surf  # match multitime_hk

# specify source and receiver location
rsrc = np.array([0., 0, -15])
z_source = rsrc[-1]
rrcr = np.array([55., 0, -10])

ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri

y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)
fcompute = np.arange(frange[0], frange[1] + 1)

# setup batch wni
# offset source and reciever to create wave phase offset
rs_w = rsrc.copy()
rr_w = rrcr.copy()
rs_w[0] = rs_w[0] + lwave * wave_phase / (2 * np.pi)
rr_w[0] = rr_w[0] + lwave * wave_phase / (2 * np.pi)
wn_ier = wn_synthesis.WaveNumberSynthesis(rs_w, rr_w, lwave)

# HK integrations
# setup integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
dx = 0.1
xbounds = (-300, 600)
xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
# create integration bounds
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3,
            np.ceil(t_spec * 1e3 + 8) / 1e3)
fs = 50e3
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wave_phase)

# ray solution
ray_field = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                               range_buffer=40)
ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
ray_field.to_top(ray_gun)
ray_field.to_wave()

ray_HK = surface_integral.TimeHK(ray_field, xaxis, taxis, i_er_signal)
ray_result = ray_HK(rrcr)

# isospeed solution
iso_field = point_iso_field.IsospeedPoint(eta, z_source)
isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal)
iso_result = isospeed_HK(rrcr)

# compute scattered field using batch processing
rh_FT = []
for frun in fcompute:
    qvec, raxis, rvals = wn_synthesis.read_coeff(hwave, lwave, frun, save_dir)
    qvec_sym, thetaaxis_sym, rs_sym = wn_ier.make_symetric(qvec, raxis, rvals)
    p_ref = [wn_ier.kx_sta_ky_sta(q, qvec_sym, rs_sym, thetaaxis_sym, frun)
             for q in qvec_sym]
    if (frun % 200) == 0:
        print('run number %i'%frun)
    rh_FT.append(np.sum(p_ref))

rh_ts, t_rh = f_synthesis.synthesize_ts(rh_FT, i_er_signal, fcompute)

# upsample timeseries before plotting
rh_ts_up, t_rh_up = resample(rh_ts, t_rh.size * 4, t=t_rh)

# create differences plots
d1 = np.interp(t_rh_up, taxis, iso_result) - rh_ts_up
d2 = np.interp(t_rh_up, taxis, ray_result) - rh_ts_up

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot((taxis - t_spec) * 1e3, iso_result / direct_amp,
             color='C4',label='nHK')
axes[0].plot((taxis - t_spec) * 1e3, ray_result / direct_amp,
             color='C2', label='sHK')
axes[0].plot((t_rh_up - t_spec) * 1e3, rh_ts_up / direct_amp,
             color='C1', label='RH')

axes[1].plot((t_rh_up - t_spec) * 1e3, d1 / direct_amp, color='C4')
axes[1].plot((t_rh_up - t_spec) * 1e3, d2 / direct_amp, color='C2')
axes[1].set_xlim(-1, 6)
axes[0].set_ylim(-1, 1)
axes[1].set_ylim(-1, 1)
axes[0].set_ylabel('pressure, mPa')
axes[1].set_xlabel('time, ms')
axes[0].set_title('surface scatter time series')
axes[1].set_title('Difference from Rayleigh Hypothesis')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)

plt.show(block=False)
