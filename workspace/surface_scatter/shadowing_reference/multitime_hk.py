"""
==========================
Scaled W&D tank experiment
==========================

High frequency pulse reflected from a single wave, scaled to a 15kz center
frequency.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.clumber import ray_field, iso_rg
from icepyks.surfaces import sin_surface

#cmap = plt.cm.RdBu_r
cmap = plt.cm.PuOr_r
# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 40.
wave_amp = 0.5
num_phase = 150
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j]
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)

c0 = 1500
z_source = -20
r_receiver = np.array((200.,0.,-10))
z_bottom = -30
z_off = -(wave_amp + 0.1)

spec_r = np.sqrt((z_source + r_receiver[2]) ** 2 + r_receiver[0] ** 2)

#signal parameter
fc = 3.5e3
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

#Setup integration bounds
x_bounds = (-50, 250)
fs = 20e3  # Hz
dx = c0 / fc / 5
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
spec_tau = spec_r / c0
taxis = np.arange(spec_tau - 1e-3, spec_tau + 4e-3, 1 / fs)

# solve for pressure field at surface
ray_gun = iso_rg.Isospeed(c0, z_bottom, z_off)
eta = sin_surface.Sine(lamda_surf, wave_amp)
proper = ray_field.RayField(eta, z_source, r_receiver,
                            tt_tol=5e-6, range_buffer=6)
proper.to_top(ray_gun)

dir_series = []
sha_series = []
for p in wave_phase:
    eta = sin_surface.Sine(lamda_surf, wave_amp, phase=p)
    proper.to_wave(eta)
    proper('shad')

    hk_time = surface_integral.TimeHK(proper, xaxis, taxis, i_er_signal)
    sha_series.append(hk_time(r_receiver))

    proper('wave')
    hk_time = surface_integral.TimeHK(proper, xaxis, taxis, i_er_signal)
    dir_series.append(hk_time(r_receiver))

dir_series = np.array(dir_series)
sha_series = np.array(sha_series)

# reference arrivals to specular reflection
dir_series = dir_series * spec_r
sha_series = sha_series * spec_r

# stack 3 timeseries
wp = np.hstack([wave_phase - 2 * np.pi, wave_phase, wave_phase + 2 * np.pi])
shad = np.vstack(3 * [sha_series]).T
noshad = np.vstack(3 * [dir_series]).T

N, T = np.meshgrid(wp / w_surf, (taxis - spec_tau) * 1e3)

fig, ax = plt.subplots()
cm = ax.pcolormesh(N, T, noshad, vmin=-1.2, vmax=1.2, cmap=cmap)
fig.colorbar(cm)
ax.set_xlim(-2.5, 7.5)
ax.set_xlabel('experimental time, s')
ax.set_ylabel('time relative to specular, ms')
ax.set_title('non-shadowed arrival over a full wave cycle\n'+
             '%i m wave length, %i m wave height'%(lamda_surf, 2 * wave_amp))
ax.grid(color='gainsboro')

fig, ax = plt.subplots()
cm = ax.pcolormesh(N, T, shad, vmin=-1.2, vmax=1.2, cmap=cmap)
fig.colorbar(cm)
ax.set_xlim(-2.5, 7.5)
ax.set_xlabel('experimental time, s')
ax.set_ylabel('time relative to specular, ms')
ax.set_title('shadowed arrival over a full wave cycle\n'+
             '%i m  wave length, %i m wave height'%(lamda_surf, 2 * wave_amp))
ax.grid(color='gainsboro')

fig, ax = plt.subplots()
cm = ax.pcolormesh(N, T, noshad - shad, vmin=-.6, vmax=.6, cmap=cmap)
fig.colorbar(cm)
ax.set_xlim(-2.5, 7.5)
ax.set_xlabel('experimental time, s')
ax.set_ylabel('time relative to specular, ms')
ax.set_title('difference between shadowing corrections\n'+
             '%i m wave length, %i m wave height'%(lamda_surf, 2 * wave_amp))
ax.grid(color='gainsboro')

plt.show(block=False)
