import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks import sin_surface, pulse_signal
from icepyks.porty import hk_integral, iso_speed, surface_field
from scipy.signal import hilbert
from icepyks.jrussell import iso_rg as iso_rg
from icepyks.jrussell.propagator import Propagator
from icepyks.jrussell.hk_gridded import Field

cmap = plt.cm.magma_r
cmap.set_under('w')

# Basic test geometry
r_source = np.array((0., 0, 10))
r_receiver = np.array((80., 0, 5))  # shadowing apparent
# r_receiver = np.array((40., 0, 5))  # no clear shadowing
a_bounds = (-5, r_receiver[0] + 5)
z_rcr = r_receiver[-1]

# Signal paramters
fmax = 12000
# signal = pulse_signal.sine_four_hanning(fc)
# signal = pulse_signal.pulse_udel()
signal = pulse_signal.pulse_narrowband()

# Perform HK integration using a sine surface
# Sine wave parameters
lambda_surf = 25
w_surf = np.sqrt(9.81 * 2 * np.pi / lambda_surf)
wave_amp = 1.5
z_offset = 1
c = 1500
num_phase = 80

# phases of interest
phase_a = -200  # 80m caustic feature
phase_b = -150  # Shadowed 80m caustic feature # calculate flat surface quantities
t_dir = np.linalg.norm(r_source - r_receiver) / c
t_spec = np.linalg.norm(r_source + r_receiver) / c
dir_dist = np.linalg.norm(r_source - r_receiver)
dx1 = .1  # m

# correct for wave z offset
r_source[2] -= (wave_amp + z_offset)
r_receiver[2] -= (wave_amp + z_offset)

#Setup integration bounds
x_bounds = a_bounds
y_bounds = (-30, 30)
z_bottom = 300
t_bounds = (np.floor(t_dir * 1e3) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
fs = 5e5
dx = c / fmax / 5
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
yaxis = np.arange(min(y_bounds), max(y_bounds), dx)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
X, Y = np.meshgrid(xaxis, yaxis)

#Solve for wave surface
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]

# numerical solution
src_rg = iso_rg.Isospeed(c, r_source)
rrc_rg = iso_rg.Isospeed(c, r_receiver)
hk_solver = hk_integral.HK(signal, taxis)
proper = Propagator(src_rg, rrc_rg, a_bounds, z_bottom)

t_series_full = []
t_series_sp = []
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (4 * np.pi / 2)
ord_phase = []
wp = [phase_a, phase_b]
for p in [phase_a, phase_b]:
    eta = sin_surface.Eta(lambda_surf, wave_amp, phase=p, z_offset=z_offset)
    field1 = iso_speed.IsospeedField(eta, r_source, c, x_bounds,
                                    x1_bounds, is_grad=True)
    field2 = iso_speed.IsospeedField(eta, r_receiver, c, x_bounds,
                                    x1_bounds, is_grad=False)
    # numeric solution

    # compute stationary phase path
    at_top = surface_field.SurfaceField(field2, field1, c)
    t_series_full.append(hk_solver(X, at_top, y=Y))
    at_top.setup_stationary_phase(x_bounds, x1_bounds)
    t_series_sp.append(hk_solver(xaxis, at_top))
    # easiest just to keep track of phase inside loop
    ord_phase.append(p)

t_series_full = xr.DataArray([np.array(ts) for ts in t_series_full],
                        dims=['phase', 'sec'],
                        coords=[wp, taxis])

t_series_sp = xr.DataArray([np.array(ts) for ts in t_series_sp],
                        dims=['phase', 'sec'],
                        coords=[wp, taxis])

sp = t_series_sp.sel(phase=200, method='nearest')
full = t_series_full.sel(phase=200, method='nearest')

fig, ax = plt.subplots()
ax.plot(sp.sec * 1e3, np.real(sp), label='stationary')
ax.plot(full.sec * 1e3, np.real(full), linewidth=4, label='2D', alpha=0.5)
ax.set_title('Stationary phase approximation and full 2D integration')
ax.set_xlabel('time, ms')
ax.set_ylabel('amplitude')
ax.set_xlim(56.5, 59.5)
ax.grid()
ax.legend(loc=2)

plt.show(block=False)
