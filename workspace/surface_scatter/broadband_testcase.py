import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks import sin_surface, pulse_signal
from icepyks.porty import hk_integral, iso_speed, surface_field
from scipy.signal import hilbert
from icepyks.jrussell import iso_rg as iso_rg
from icepyks.jrussell.propagator import Propagator
from icepyks.jrussell.hk_gridded import Field

cmap = plt.cm.magma_r
cmap.set_under('w')

# Basic test geometry
r_source = np.array((0., 0, 10))
r_receiver = np.array((80., 0, 5))  # shadowing apparent
# r_receiver = np.array((40., 0, 5))  # no clear shadowing
a_bounds = (-5, r_receiver[0] + 5)
z_rcr = r_receiver[-1]

# Signal paramters
fmax = 12000
# signal = pulse_signal.sine_four_hanning(fc)
# signal = pulse_signal.pulse_udel()
signal = pulse_signal.pulse_narrowband()

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 25
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 1.5
z_offset = 1
c = 1500
num_phase = 80

# phases of interest
phase_a = -200  # 80m caustic feature
phase_b = -150  # Shadowed 80m caustic feature # calculate flat surface quantities
t_dir = np.linalg.norm(r_source - r_receiver) / c
t_spec = np.linalg.norm(r_source + r_receiver) / c
dir_dist = np.linalg.norm(r_source - r_receiver)
dx1 = .1  # m

# correct for wave z offset
r_source[2] -= (wave_amp + z_offset)
r_receiver[2] -= (wave_amp + z_offset)

#Setup integration bounds
x_bounds = a_bounds
z_bottom = 300
t_bounds = (np.floor(t_dir * 1e3) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
fs = 5e5
dx = c / fmax / 5
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

#Solve for wave surface
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]

# numerical solution
src_rg = iso_rg.Isospeed(c, r_source)
rrc_rg = iso_rg.Isospeed(c, r_receiver)
hk_solver = hk_integral.HK(signal, taxis)
proper = Propagator(src_rg, rrc_rg, a_bounds, z_bottom)

t_series_port = []
t_series_jr = []
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (4 * np.pi / 2)
ord_phase = []
for p in wave_phase[::-1]:
    eta = sin_surface.Eta(lamda_surf, wave_amp, phase=p, z_offset=z_offset)
    field1 = iso_speed.IsospeedField(eta, r_source, c, x_bounds,
                                    x1_bounds, is_grad=True)
    field2 = iso_speed.IsospeedField(eta, r_receiver, c, x_bounds,
                                    x1_bounds, is_grad=False)
    # numeric solution
    one_wave = proper.for_wave(eta)
    diff_field = Field(one_wave)
    # compute stationary phase path
    at_top = surface_field.SurfaceField(field2, field1, c)
    at_top.setup_stationary_phase(x_bounds, x1_bounds)
    t_series_port.append(hk_solver(xaxis, at_top))
    t_series_jr.append(hk_solver(xaxis, diff_field))
    # easiest just to keep track of phase inside loop
    ord_phase.append(p)

ord_phase = np.rad2deg(np.array(ord_phase))
t_series_port = xr.DataArray([np.array(ts) for ts in t_series_port],
                             dims=['wave_phase', 'delay'],
                             coords=[ord_phase,
                                    (taxis - t_dir) * 1e3])

t_series_jr = xr.DataArray([np.array(ts) for ts in t_series_jr],
                            dims=['wave_phase', 'delay'],
                            coords=[ord_phase,
                                    (taxis - t_dir) * 1e3])

direct_amp = 1 / dir_dist

X, Y = np.meshgrid(t_series_jr.wave_phase, t_series_jr.delay)
arr_port = np.array(t_series_port)
dB_port = np.abs(hilbert(arr_port, axis=-1))
dB_port = 20 * np.log10(dB_port)

arr_jr = np.array(t_series_jr)
dB_jr = np.abs(hilbert(arr_jr, axis=-1))
dB_jr = 20 * np.log10(dB_jr)

# normalize both to same value
max_db = max(np.max(dB_jr), np.max(dB_port))
dB_jr -= max_db
dB_port -= max_db

fig1 = plt.figure()
fig2 = plt.figure()
fig3 = plt.figure()

ax1 = fig1.add_subplot(111)
cm = ax1.pcolormesh(X, Y, dB_port.T, vmin=-20, vmax=0, cmap=cmap)
ax1.set_title('Amplitude of arrival over a full wave cycle' +\
          '\n point source, no shadowing')
fig1.colorbar(cm)
ax1.set_xlabel('wave phase, deg')
ax1.set_ylabel('delay from direct arrival, ms')
ax1.grid()

ax2 = fig2.add_subplot(111, sharex=ax1, sharey=ax1)
cm = ax2.pcolormesh(X, Y, dB_jr.T, vmin=-20, vmax=0, cmap=cmap)
ax2.set_title('Amplitude of arrival over a full wave cycle' +\
          '\n point source with shadowing')
fig2.colorbar(cm)
ax2.set_xlabel('wave phase, deg')
ax2.set_ylabel('delay from direct arrival, ms')
ax2.set_xlim(np.min(X), np.max(X))
ax2.set_ylim(2, np.max(Y))
ax2.grid()

def add_anotation(axis):
    """Add lines at phase A and B"""
    axis.plot((phase_a, phase_a), (-100, 100), 'k')
    axis.plot((phase_b, phase_b), (-100, 100), 'k')
    axis.annotate('A', 
                xy=(phase_a, 6.5),  
                xycoords='data',
                xytext=(phase_a - 40, 6.8),
                textcoords='data',
                arrowprops=dict(arrowstyle="->"))
    axis.annotate('B', 
                xy=(phase_b, 6.5),  
                xycoords='data',
                xytext=(phase_b + 40, 6.8),
                textcoords='data',
                arrowprops=dict(arrowstyle="->"))

add_anotation(ax1)
add_anotation(ax2)

axs_1 = fig3.add_subplot(211)
axs_1.plot(t_series_port.delay,
        t_series_port.sel(wave_phase=phase_a, method='nearest'),
        label='free space')
axs_1.plot(t_series_jr.delay,
        t_series_jr.sel(wave_phase=phase_a, method='nearest'),
        label='shadowed')
axs_1.set_title('Point source surface scatter\n phase A')
axs_1.set_xlim(2, np.max(Y))
axs_1.legend()
axs_1.grid()
plt.setp(axs_1.get_xticklabels(), visible=False)

axs_2 = fig3.add_subplot(212, sharex=axs_1, sharey=axs_1)
axs_2.plot(t_series_port.delay,
        t_series_port.sel(wave_phase=phase_b, method='nearest'),
        label='free space')
axs_2.plot(t_series_jr.delay,
        t_series_jr.sel(wave_phase=phase_b, method='nearest'),
        label='shadowed')
axs_2.set_xlabel('delay, ms')
axs_2.set_title('phase B')
axs_2.set_xlim(2, np.max(Y))
axs_2.grid()

plt.show(block=False)
