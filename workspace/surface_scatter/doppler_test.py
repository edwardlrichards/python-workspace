import matplotlib.pyplot as plt
import matplotlib.collections as mcoll
import numpy as np
import xarray as xr
from icepyks import sin_surface, pulse_signal, flat_surface
from icepyks.porty import hk_integral
from scipy.signal import hilbert
from icepyks.jrussell import iso_rg as iso_rg
from icepyks.jrussell.propagator import Propagator
from icepyks.jrussell.hk_gridded import Field
from icepyks.jrussell.geo_rays import geo_rays
import plotting.basic_plots as p_er

c0 = 1500
x1 = 5
r_source = np.array([0., 0, 108])
r_receiver = np.array([460., 0, 25])
z_bottom = max(r_source[2], r_receiver[2]) + 10
a_bounds = (-r_receiver[0] * 0.2, r_receiver[0] * 1.3)

# surface specifications
wave_amp = 1.5
length = 40
# calculate flat surface quantities
t_dir = np.linalg.norm(r_source - r_receiver) / c0
t_spec = np.linalg.norm(r_source + r_receiver) / c0
# correct for wave offset and mean water level
r_source[2] = r_source[2] - 1 - wave_amp
r_receiver[2] = r_receiver[2] - 1 - wave_amp

# setup rayguns
src_rg = iso_rg.Isospeed(c0, r_source)
rrc_rg = iso_rg.Isospeed(c0, r_receiver)

# time series simulations
fc = 20e3
x_mitt = pulse_signal.sine_four_hanning(fc)
signal = pulse_signal.pulse_udel()
x_mitt = signal

# hk integration axis
dx = 3e-2
xaxis = np.arange(min(a_bounds), max(a_bounds), dx)

# Create a time axis
t_bounds = (np.floor(t_dir * 1e3) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
taxis = np.arange(np.min(t_bounds), np.max(t_bounds), np.diff(x_mitt.time[: 2]))

# setup both geometric and hk solver
hk_solver = hk_integral.HK(x_mitt, taxis)
proper = Propagator(src_rg, rrc_rg, a_bounds, z_bottom, s_tol=1e-2)

# decimate for plotting routines
a_range = np.r_[a_bounds[0]: a_bounds[1]: 1000j]

phase = -0.3 * 2 * np.pi
#phase = 0
eta = sin_surface.Eta(length, wave_amp, z_offset=1, phase=phase)
onewave = proper.for_wave(eta)
diff_field = Field(onewave)

# choose a single normal to plot along
norm_oi = 0.

# hk time series
ga_ts, zc, deltas = geo_rays(a_range, onewave, norm_oi, taxis, x_mitt)
hk_ts = hk_solver(xaxis, diff_field)

fig, ax = p_er.plot_eigenrays(a_range, onewave, norm_oi, zc)
ax.plot(a_range, -onewave.eta.height(a_range, 0), 'b')
ax.plot([-1e3, 5e3], [-1 - wave_amp, -1 - wave_amp], '-.k', alpha=0.5)
ax.set_xlim(100, 550)
ax.set_ylim(25, -5)

# Create a colored line that represents wave velocity
def colorline(
        x, y, z=None, cmap='copper', norm=plt.Normalize(0.0, 1.0),
        linewidth=3, alpha=1.0):
    """
    http://nbviewer.ipython.org/github/dpsanders/matplotlib-examples/blob/master/colorline.ipynb
    http://matplotlib.org/examples/pylab_examples/multicolored_line.html
    Plot a colored line with coordinates x and y
    Optionally specify colors in the array z
    Optionally specify a colormap, a norm function and a line width
    """

    # Default colors equally spaced on [0,1]:
    if z is None:
        z = np.linspace(0.0, 1.0, len(x))

    # Special case if a single number:
    # to check for numerical input -- this is a hack
    if not hasattr(z, "__iter__"):
        z = np.array([z])

    z = np.asarray(z)

    segments = make_segments(x, y)
    lc = mcoll.LineCollection(segments, array=z, cmap=cmap, norm=norm,
                              linewidth=linewidth, alpha=alpha)

    ax = plt.gca()
    ax.add_collection(lc)

    return lc

def make_segments(x, y):
    """
    Create list of line segments from x and y coordinates, in the correct format
    for LineCollection: an array of the form numlines x (points per line) x 2 (x
    and y) array
    """

    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    return segments

s_x = onewave.cat('shadow', a_range, 'x_vec', normal=norm_oi,
                    element='source')
r_x = onewave.cat('shadow', a_range, 'x_vec', normal=norm_oi,
                    element='receiver')
r_source = onewave.r_src
r_receiver = onewave.r_rcr

x2_eig = s_x[zc, 2]
fig, ax = plt.subplots()
[ax.plot([r_source[0], s_x[i, 0]], [r_source[2], s_x[i, 2]], 'k')
            for i in zc]
[ax.plot([r_receiver[0], r_x[i, 0]], [r_receiver[2], r_x[i, 2]], 'k')
            for i in zc]

r_surf = np.array([a_range, -onewave.eta.height(a_range, 0)])
r_src_surf = r_source[[0,2], None] - r_surf
angle_tosurf = np.arctan2(r_src_surf[1, :], -r_src_surf[0,:])

z_vel = -onewave.eta.velocity(a_range, 0)
proj_vel = np.sin(angle_tosurf) * z_vel

c_norm = plt.Normalize(-0.6, 0.6)
lc=colorline(a_range, -onewave.eta.height(a_range, 0), proj_vel, norm=c_norm,
        cmap=plt.cm.RdBu_r)
cb = plt.colorbar(lc)
cb.set_label('projected wave velocity, m/s')
ax.set_ylim(30, -5)
ax.set_title('Geometric acoustics eigenrays')
ax.set_xlabel('along track distance, m')
ax.set_ylabel('depth, m')
ax.set_xlim(250, 450)
ax.set_ylim(0, -5)
ax.grid()

plt.show(block=False)
