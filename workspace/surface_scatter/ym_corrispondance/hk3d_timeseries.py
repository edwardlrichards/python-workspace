import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp
from icepyks.poodle.cosine_surface import CosineSurface
from concurrent.futures import ThreadPoolExecutor

# setup the wave specific solver
hwave = 0.4
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1  # dB / km
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# specify source and receiver location
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
#rsrc = np.array([0, -3])
#rrcr = np.array([20, -5])

# setup integration parameters
dx = 0.1
xbounds = (-15, 80)
xbounds = (-300, 600)
numa = 2 ** 16
ftest = 1050
savename = 'cos_3dHK'
isexecute = False  # True calculates coefficents, False loads pre-calculated

# load ym timeseries
from scipy.io import loadmat
ym = loadmat('ym_ts.mat')

# create the test signal and its fourier transform
#test_signal, tsig = pulse_signal.pulse_q1(1500)
test_signal, tsig = pulse_signal.sine_four_hanning(1500)
sig_NFFT = int(2 ** np.ceil(np.log2(test_signal.size) + 4))
f_sig = np.fft.rfft(test_signal, sig_NFFT)
f_sig_axis = np.arange(sig_NFFT // 2 + 1) / sig_NFFT / (tsig[1] - tsig[0])
frange = (500, 2500)  # generous bounds
NFFT = 2 ** 13
df = 1

# make a signal interpolator
s_ier_obj = signal_interp.ContinuousSignal(test_signal, tsig)
s_ier = lambda t: s_ier_obj(t, derivative=False)
s_ier_half = lambda t: s_ier_obj.half_derivative(t)

# create a q vector of scattering orders
qvec = np.arange(-20, 21)

xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
taxis = np.arange(NFFT) * df / NFFT
faxis = np.arange(NFFT) * df
fcompute = np.arange(frange[0], frange[1] + 1)

hk = cos_ier.hk_spatial_3D(ftest, rsrc, rrcr, xaxis)
print('Spatial HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk), np.imag(hk)))

rfunc = lambda a0, freq: cos_ier.r_hk(a0, qvec, freq, return_bs=False)
hk = cos_ier.wn_3D(rfunc, ftest, rsrc, rrcr, numa, qvec)
print('WN HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk), np.imag(hk)))

# spatial HK integral
onef = lambda f: cos_ier.hk_spatial_3D(f, rsrc, rrcr, xaxis)
with ThreadPoolExecutor(max_workers=3) as executor:
    hk_sp_allf = list(executor.map(onef, fcompute))

hk_spatial_xform, t_xform = cos_ier.make_timeseries(s_ier, hk_sp_allf, fcompute)
hk_spatial_xform_up, t_spatial_up = resample(hk_spatial_xform, t_xform.size * 4, t=t_xform)

# wave number HK integral
onef = lambda f: cos_ier.wn_3D(rfunc, f, rsrc, rrcr, numa, qvec)
hk = cos_ier.wn_3D(rfunc, ftest, rsrc, rrcr, numa, qvec)

if isexecute:
    tstart = time.time()
    with ThreadPoolExecutor(max_workers=5) as executor:
        hk_wn_allf = list(executor.map(onef, fcompute))
    tend = time.time()
    np.savez(savename, fcompute=fcompute, allf=hk_wn_allf)
else:
    hk_data = np.load(savename + '.npz')
    hk_wn_allf = hk_data['allf']

hk_wn_xform, t_xform = cos_ier.make_timeseries(s_ier, hk_wn_allf, fcompute)
hk_wn_xform_up, t_wn_up = resample(hk_wn_xform, t_xform.size * 4, t=t_xform)

# Plot the spatial and wave number integral
hk_ts = cos_ier.hk_spatial_3D_time(rrcr, rsrc, xaxis, taxis, s_ier_half)
hk_ts_up, t_hk_up = resample(hk_ts, taxis.size * 4, t=taxis)

fig, ax = plt.subplots()
ax.plot(f_sig_axis, np.abs(f_sig) / 5000, color='C0', label='window')
ax.plot(fcompute, np.abs(hk_sp_allf), color='C1', label='spatial HK')
#ax.plot(fcompute, np.real(hk_sp_allf), color='C1', label='spatial HK')
#ax.plot(fcompute, np.imag(hk_sp_allf), color='C1', label='spatial HK')
ax.plot(fcompute, np.abs(hk_wn_allf), color='C2', label='wn HK')
#ax.plot(fcompute, np.real(hk_wn_allf), color='C2', label='wn HK')
#ax.plot(fcompute, np.imag(hk_wn_allf), color='C2', label='')
ax.set_xlabel('frequency, Hz')
ax.set_ylabel('magnitude')
ax.set_title('Frequency content of scattered signal')
ax.set_xlim(fcompute[0], fcompute[-1])
ax.legend()
ax.grid()
plt.show(block=False)

fig, ax = plt.subplots()
ax.plot(t_hk_up * 1e3, hk_ts_up * 1e3, linewidth=3, label='spatial HK, time')
ax.plot(t_wn_up * 1e3, hk_wn_xform_up * 1e3, linewidth=3, label='wn HK, Xform')
ax.plot(t_spatial_up * 1e3, hk_spatial_xform_up * 1e3, linewidth=3, label='spatial HK, Xform')
ax.plot(np.squeeze(ym['t_ob']) * 1e3,
        np.squeeze(ym['sig_ob']) * 1e3,
        linewidth=3, label='ym HK, Xform')
ax.set_xlim(38, 48)
#ax.set_xlim(12, 20)
ax.set_ylim(-30, 30)
ax.set_ylabel('pressure, mUnits')
ax.set_xlabel('time, ms')
ax.set_title('surface scatter time series, 3D')
ax.grid()
ax.legend()

plt.show(block=False)
