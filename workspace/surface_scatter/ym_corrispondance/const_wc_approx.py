import numpy as np
import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp
from scipy.io import loadmat

fc = 1500;

signal, taxis = pulse_signal.pulse_q1(fc)
ym_source = loadmat('ym_src.mat')
fs = 5e6
t = np.arange(0, 5e-4, 1 / fs)

def comp_vals(t_in, s_in):
    """Compute the compairison values"""
    dt = t_in[1] - t_in[0]
    fs = 1 / dt
    NFFT = int(2 ** np.ceil(np.log2(s_in.size)))
    # create new frequency axis
    f = np.arange(NFFT // 2 + 1) / NFFT * fs
    t_up = np.arange(NFFT) / fs

    # Multiply transmitted signal by derivative factor
    sig_FT = np.fft.rfft(s_in, NFFT)
    sig_2d_FT = np.exp(3j * np.pi / 4)\
                * np.sqrt(2 * np.pi * f + 0j + np.spacing(1))
    sig_2d_FT[f == 0] = 0  # set DC integral to zero
    sig_2d_FT = np.conj(sig_2d_FT) * sig_FT
    # Construct half derivative time series
    sig_2d = np.fft.irfft(sig_2d_FT)

    # compute 1st derivative
    sig_1st_FT = 1j * 2 * np.pi * f + np.spacing(1)
    sig_1st_FT[f == 0] = 0  # set DC integral to zero
    sig_1st_FT = np.conj(sig_1st_FT) * sig_FT
    # Construct half derivative time series
    sig_1st = np.fft.irfft(sig_1st_FT)

    # compute constant frequency approximation to half derivative
    sig_2da_FT = np.exp(3j * np.pi / 4)\
                * np.sqrt(2 * np.pi * fc + 0j + np.spacing(1))
    sig_2da_FT = np.conj(sig_2da_FT) * sig_FT
    # Construct half derivative time series
    sig_2da = np.fft.irfft(sig_2da_FT)
    plt.figure()
    plt.plot(t_up * 1e3, sig_1st / (2 * np.pi * fc), label=
            r'$s\prime(t) / \omega_c$')
    plt.plot(t_up * 1e3, sig_2d / np.sqrt(2 * np.pi * fc), label=
            r'$\frac{d^{1/2}}{dx^{1/2}} s(t) / \sqrt{\omega_c}$')
    plt.plot(t_in * 1e3, -s_in, label='$s(t)$')
    plt.legend()
    plt.xlabel('m seconds')
    plt.title('signal and half derivative')

    f, axarr = plt.subplots(2, sharex=True)
    axarr[0].plot(t_up * 1e3, sig_2d / np.sqrt(2 * np.pi * fc), label=
            'exact')
    axarr[0].plot(t_up * 1e3, sig_2da / np.sqrt(2 * np.pi * fc), label=
            'approximate')
    axarr[1].plot(t_up * 1e3, (sig_2d - sig_2da) / np.sqrt(2 * np.pi * fc))

    axarr[0].legend()
    plt.xlabel('m seconds')
    axarr[0].set_title('signal and half derivative')

    plt.show(block=False)

comp_vals(taxis, signal)
comp_vals(np.squeeze(ym_source['t_src']), np.squeeze(ym_source['sig_src']))
