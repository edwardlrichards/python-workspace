import numpy as np
from scipy.interpolate import UnivariateSpline
from scipy.signal import hilbert
from icepyks import signal_interp
import matplotlib.pyplot as plt
from icepyks import pulse_signal

fc = 1500
x_mit, taxis = pulse_signal.sine_four_hanning(fc)

fs = fc * 50
t = np.arange(-1e-3, 4e-3, 1 / fs)
x_inter = signal_interp.ContinuousSignal(x_mit, taxis)

x_sig = x_inter(t, derivative=False)
x_pts = x_inter(t, derivative=True)

# investigate the frequency domain
NFFT = int(2 ** np.ceil(np.log2(x_sig.size) + 1))
# create new frequency axis
f = np.arange(NFFT // 2 + 1) / NFFT * fs
t_up = np.arange(NFFT) / fs + t[0]

sig_FT = np.fft.rfft(x_sig, NFFT)
sig_2d_FT = np.exp(3j * np.pi / 4)\
            * np.sqrt(2 * np.pi * f + 0j + np.spacing(1))
sig_p_FT = 1j * 2 * np.pi * f + 0j + np.spacing(1)

# convolve greens function with signal, account for Helmholtz eq. convention
sig_2d_FT = sig_2d_FT * sig_FT
sig_p_FT = sig_p_FT * sig_FT

# Construct half derivative time series
sig_0 = np.fft.irfft(sig_FT)
sig_2d = np.fft.irfft(sig_2d_FT)
sig_p = np.fft.irfft(sig_p_FT)

# recreate real time series as a sum of two complex time series
sig_hilb = np.real(np.exp(1j * np.pi / 4) * hilbert(sig_p))\
           / np.sqrt(2 * np.pi * fc)

# This is the time series which works in poodle integration
s_ier_half = lambda t: x_inter.half_derivative(t)

fig, ax = plt.subplots()
ax.plot(t_up * 1e3, sig_0, label='signal')
ax.plot(t_up * 1e3, sig_2d  / np.sqrt(2 * np.pi * fc), label='half')
ax.plot(t_up * 1e3, sig_p / (2 * np.pi * fc), label='first')
ax.plot(t * 1e3, x_pts / (2 * np.pi * fc), '--')
ax.plot(t_up * 1e3, sig_hilb  / np.sqrt(2 * np.pi * fc), '--')
ax.legend()
ax.set_xlim(-.5, 3)
ax.grid()
ax.set_xlabel('time, ms')
ax.set_ylabel('amplitude, normalized')
ax.set_title('xmitt signal and derivatives')

fig, ax = plt.subplots()
ax.plot(t_up * 1e3, sig_hilb)
ax.plot(t_up * 1e3, s_ier_half(t_up))
ax.set_xlim(0, 3)
ax.grid()
ax.set_ylim(-100, 100)

plt.show(block=False)
