import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.signal import resample
from scipy.interpolate import interp1d, UnivariateSpline
from scipy.io import loadmat
from icepyks import pulse_signal, signal_interp
from icepyks.poodle.cosine_surface import CosineSurface
from concurrent.futures import ThreadPoolExecutor

# setup the wave specific solver
hwave = 0
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1  # dB / km
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# specify source and receiver location
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[1] + rrcr[1]) ** 2)

# setup integration parameters
dx = 0.1
xbounds = (-300, 600)
xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
numa = 2 ** 16

# create the test signal and its fourier transform
test_signal, tsig = pulse_signal.sine_four_hanning(1500)
sig_NFFT = int(2 ** np.ceil(np.log2(test_signal.size) + 4))
frange = (500, 2500)  # generous bounds
NFFT = 2 ** 13

ym_source = loadmat('ym_source.mat')
ym_flat = loadmat('ym_flat_surface.mat')
ym_1amp = loadmat('ym_1amp.mat')
ym_2amp = loadmat('ym_2amp.mat')
ym_4amp = loadmat('ym_4amp.mat')
rh_1amp = np.load('cos_2height_RH3D.npz')
rh_2amp = np.load('cos_4height_RH3D.npz')
rh_4amp = np.load('cos_8height_RH3D.npz')

# Create a interpolator for the ym source
ym_ier = UnivariateSpline(np.squeeze(ym_source['t_src']),
                          np.squeeze(ym_source['sig_src']),
                          ext=1, s=0, k=3)

# make a signal interpolator
s_ier_obj = signal_interp.ContinuousSignal(test_signal, tsig)
s_ier_half = lambda t: s_ier_obj.half_derivative(t)

# setup time and frequency axis
faxis = np.arange(NFFT) / NFFT * 2 * (frange[-1] + 500)
df = faxis[1] - faxis[0]
dt = 1 / df / NFFT
taxis = np.arange(NFFT) * dt
fcompute = np.arange(frange[0], frange[1] + 1)

# take FT of signal
t_sig = s_ier_obj(taxis)
f_sig = np.fft.rfft(t_sig, NFFT)

im_allf = -np.exp(1j * cos_ier.kacous(fcompute) * ri) / ri
im_xform, t_im = cos_ier.make_timeseries(s_ier_obj, im_allf, fcompute)
im_xform_up, t_im_up = resample(im_xform, t_im.size * 4, t=t_im)

# image source result
img_ts = -s_ier_obj(taxis - ri / 1500) / ri
img_ym = -ym_ier(taxis - ri / 1500) / ri
hk_ts = cos_ier.hk_spatial_3D_time(rrcr, rsrc, xaxis, taxis, s_ier_half)

# 4x upsample
img_ts_up, t_up = resample(img_ts, taxis.size * 4, t=taxis)
img_ym_up, t_up = resample(img_ym, taxis.size * 4, t=taxis)
hk_ts_up, t_up = resample(hk_ts, taxis.size * 4, t=taxis)

# flat surface FT

# flat surface TS
fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(t_up * 1e3, img_ym_up * 1e3, color='C4', label='image')
axes[0].plot(np.squeeze(ym_flat['t_ob']) * 1e3,
        np.squeeze(ym_flat['sig_ob'])* 1e3, label='YM HK', color='C1')
axes[0].plot(t_up * 1e3, hk_ts_up * 1e3, label='NR HK', color='C2')
d1 = np.interp(t_up, np.squeeze(ym_flat['t_ob']),
               np.squeeze(ym_flat['sig_ob'])) - img_ym_up
d2 = hk_ts_up - img_ym_up
axes[1].plot(t_up * 1e3, d1 * 1e3, color='C1')#, label='ym no shadow')
axes[1].plot(t_up * 1e3, d2 * 1e3, color='C2')#, label='ym shadow')
axes[1].set_xlim(40, 50)
axes[0].set_ylim(-16, 16)
axes[0].set_ylabel('pressure, mPa')
axes[1].set_xlabel('time, ms')
axes[1].set_title('Difference from image source')
axes[0].set_title('Flat surface reflection time series, 3D')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)

# 2 height surface TS
cos_ier.hwave = 0.2
hk_1ts = cos_ier.hk_spatial_3D_time(rrcr, rsrc, xaxis, taxis, s_ier_half)
hk_1ts_up, _ = resample(hk_1ts, taxis.size * 4, t=taxis)
rh_1ts, t_rh1 = cos_ier.make_timeseries(s_ier_obj,
                                         rh_1amp['allf'],
                                         rh_1amp['fcompute'])
rh_1ts_up, t_rh1_up = resample(rh_1ts, t_rh1.size * 4, t=t_rh1)

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(t_rh1_up * 1e3, rh_1ts_up * 1e3, color='C4', label='RH')
axes[0].plot(np.squeeze(ym_1amp['t_ob']) * 1e3,
        np.squeeze(ym_1amp['sig_no_shadow'])* 1e3, label='YM no shadow', linewidth=3, alpha=0.8)
axes[0].plot(np.squeeze(ym_1amp['t_ob']) * 1e3,
        np.squeeze(ym_1amp['sig_ob'])* 1e3, label='YM shadow')
axes[0].plot(t_up * 1e3, hk_1ts_up * 1e3, label='NR no shadow')
d1 = np.interp(t_rh1_up, np.squeeze(ym_1amp['t_ob']),
               np.squeeze(ym_1amp['sig_no_shadow'])) - rh_1ts_up
d2 = np.interp(t_rh1_up, np.squeeze(ym_1amp['t_ob']),
               np.squeeze(ym_1amp['sig_ob'])) - rh_1ts_up
d3 = np.interp(t_rh1_up, t_up, hk_1ts_up) - rh_1ts_up
axes[1].plot(t_rh1_up * 1e3, d1 * 1e3, linewidth=3, alpha=0.8)#, label='ym no shadow')
axes[1].plot(t_rh1_up * 1e3, d2 * 1e3)#, label='ym shadow')
axes[1].plot(t_rh1_up * 1e3, d3 * 1e3)#, label='time HK')
axes[0].set_xlim(40, 50)
#ax.set_ylim(-45, 45)
axes[0].set_ylim(-16, 16)
axes[1].set_ylim(-4, 4)
axes[0].set_ylabel('pressure, mPa')
axes[1].set_xlabel('time, ms')
axes[0].set_title('0.1 m amplitude surface scatter time series')
axes[1].set_title('Difference from Rayleigh Hypothesis')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)
#axes[1].legend()

# 4 height surface TS
cos_ier.hwave = 0.4
hk_2ts = cos_ier.hk_spatial_3D_time(rrcr, rsrc, xaxis, taxis, s_ier_half)
hk_2ts_up, _ = resample(hk_2ts, taxis.size * 4, t=taxis)
rh_2ts, t_rh2 = cos_ier.make_timeseries(s_ier_obj,
                                         rh_2amp['allf'],
                                         rh_2amp['fcompute'])
rh_2ts_up, t_rh2_up = resample(rh_2ts, t_rh2.size * 4, t=t_rh2)
fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(t_rh2_up * 1e3, rh_2ts_up * 1e3, color='C4', label='RH')
axes[0].plot(np.squeeze(ym_2amp['t_ob']) * 1e3,
        np.squeeze(ym_2amp['sig_no_shadow'])* 1e3, linewidth=3, alpha=0.8, label='YM no shadow')
axes[0].plot(np.squeeze(ym_2amp['t_ob']) * 1e3,
        np.squeeze(ym_2amp['sig_ob'])* 1e3, label='YM shadow')
axes[0].plot(t_up * 1e3, hk_2ts_up * 1e3, label='NR no shadow')
d1 = np.interp(t_rh2_up, np.squeeze(ym_2amp['t_ob']),
               np.squeeze(ym_2amp['sig_no_shadow'])) - rh_2ts_up
d2 = np.interp(t_rh2_up, np.squeeze(ym_2amp['t_ob']),
               np.squeeze(ym_2amp['sig_ob'])) - rh_2ts_up
d3 = np.interp(t_rh2_up, t_up, hk_2ts_up) - rh_2ts_up
axes[1].plot(t_rh2_up * 1e3, d1 * 1e3, linewidth=3, alpha=0.8)#, label='ym HK, no shadow')
axes[1].plot(t_rh2_up * 1e3, d2 * 1e3)#, label='ym HK, shadow')
axes[1].plot(t_rh2_up * 1e3, d3 * 1e3)#, label='time HK')
axes[0].set_ylim(-16, 16)
axes[1].set_xlim(40, 50)
axes[1].set_ylim(-4, 4)
axes[0].set_ylabel('pressure, mPa')
axes[1].set_xlabel('time, ms')
axes[0].set_title('0.2 m amplitude surface scatter time series')
axes[1].set_title('Difference from Rayleigh Hypothesis')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)
#axes[1].legend()

# 8 height surface TS
cos_ier.hwave = 0.8
hk_4ts = cos_ier.hk_spatial_3D_time(rrcr, rsrc, xaxis, taxis, s_ier_half)
hk_4ts_up, _ = resample(hk_4ts, taxis.size * 4, t=taxis)
rh_4ts, t_rh4 = cos_ier.make_timeseries(s_ier_obj,
                                         rh_4amp['allf'],
                                         rh_4amp['fcompute'])
rh_4ts_up, t_rh4_up = resample(rh_4ts, t_rh4.size * 4, t=t_rh4)

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(t_rh4_up * 1e3, rh_4ts_up * 1e3, color='C4', label='RH')
axes[0].plot(np.squeeze(ym_4amp['t_ob']) * 1e3,
        np.squeeze(ym_4amp['sig_no_shadow'])* 1e3, linewidth=3, alpha=0.8,
        label='ym no shadow')
axes[0].plot(np.squeeze(ym_4amp['t_ob'])[2: ] * 1e3,
        np.squeeze(ym_4amp['sig_ob'])[2: ] * 1e3, label='ym shadow')
axes[0].plot(t_up * 1e3, hk_4ts_up * 1e3, label='NR no shadow')
d1 = np.interp(t_rh4_up, np.squeeze(ym_4amp['t_ob']),
               np.squeeze(ym_4amp['sig_no_shadow'])) - rh_4ts_up
d2 = np.interp(t_rh4_up, np.squeeze(ym_4amp['t_ob']),
               np.squeeze(ym_4amp['sig_ob'])) - rh_4ts_up
d3 = np.interp(t_rh4_up, t_up, hk_4ts_up) - rh_4ts_up
axes[1].plot(t_rh4_up * 1e3, d1 * 1e3, linewidth=3, alpha=0.8)#, label='ym HK, no shadow')
axes[1].plot(t_rh4_up * 1e3, d2 * 1e3)#, label='ym HK, shadow')
axes[1].plot(t_rh4_up * 1e3, d3 * 1e3)#, label='time HK')
axes[1].set_xlim(40, 50)
axes[0].set_ylim(-16, 16)
axes[1].set_ylim(-4, 4)
axes[0].set_ylabel('pressure, mPa')
axes[1].set_xlabel('time, ms')
axes[0].set_title('0.4 m amplitude surface scatter time series')
axes[1].set_title('Difference from Rayleigh Hypothesis')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)
#axes[1].legend()

plt.show(block=False)
