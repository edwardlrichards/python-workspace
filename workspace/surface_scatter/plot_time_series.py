import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks import sin_surface, pulse_signal
from icepyks.porty import hk_integral, iso_speed, surface_field

# Basic test geometry
r_source = np.array((0., 0, 10))
r_receiver = np.array((80., 0, 5))
a_bounds = (-5, 85)
z_rcr = r_receiver[-1]

# Signal paramters
# signal = pulse_signal.pulse_udel()
signal = pulse_signal.pulse_narrowband()

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 25
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 1.5
c = 1500
num_phase = 40


# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf=0.693
wave_amp = 31e-3 / 2
num_phase = 120
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (3 * np.pi / 2)
c = 1469
# make wave move away from source

#signal parameter
fc = 200e3

#Setup integration bounds
x_bounds = (-0.5, 1.75)
y_bounds = (-1, 1)
#taxis is from Walstead and Dean
t_bounds = (841e-6, 881e-6)
fs = 5e6
dx = 3e-3
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
yaxis = np.arange(min(y_bounds), max(y_bounds), dy)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
X, Y = np.meshgrid(xaxis, yaxis)
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]

#Solve for wave surface
r_source = np.array((0, 0, -0.2))
r_receiver = np.array((1.215, 0, -0.14))

# correct for wave z offset
r_source[2] += wave_amp
r_receiver[2] += wave_amp

distance = np.linalg.norm(r_source - r_receiver)

signal = pulse_signal.pulse()

t_series = []
for p in wave_phase[::-1]:
    eta = sin_surface.Eta(lamda_surf, wave_amp, phase=p)
    field1 = iso_speed.IsospeedField(eta, r_source, c, x_bounds,
                                    x1_bounds, is_grad=True)
    field2 = iso_speed.IsospeedField(eta, r_receiver, c, x_bounds,
                                    x1_bounds, is_grad=False)
    at_top = surface_field.SurfaceField(field2, field1, c)
    hk_solver = hk_integral.HK(signal, taxis)
    t_series.append(hk_solver(X, at_top, y=Y))

t_series = xr.DataArray([np.array(ts) for ts in t_series],
                        dims=['index', 'delay'],
                        coords=[np.arange(wave_phase.size),
                                  (taxis - np.min(taxis)) * 1e6])

distance = np.linalg.norm(r_source - r_receiver)

plt.figure()
plt.plot(signal.time * 1e6, signal.values)
plt.title('direct pressure time series, t0 = %.1f us'%(distance/c * 1e6))
plt.xlabel('time, us')
plt.ylabel('amplitude, pressure')
plt.xlim(0, 30)

ind_a = 10
ind_b = 86
fig, axes = plt.subplots(2, sharex=True)
axes[0].plot((taxis - np.min(taxis)) * 1e6, t_series.isel(index=ind_a))
axes[0].set_title('Scattered pulse A')
axes[0].set_ylim(-1, 1)
axes[1].plot((taxis - np.min(taxis)) * 1e6, t_series.isel(index=ind_b))
axes[1].set_title('Scattered pulse B')
axes[1].set_ylim(-1, 1)
axes[1].set_xlabel('time, us')
axes[1].set_ylabel('amplitude, pressure')

plt.figure()
t_series.T.plot(vmin=-1, vmax=1, cmap=plt.cm.jet)
plt.plot((ind_a, ind_a), (-100, 100), 'k')
plt.plot((ind_b, ind_b), (-100, 100), 'k')

plt.annotate('A', 
             xy=(ind_a, 30),  
             xycoords='data',
             xytext=(ind_a + 10, 30),
             textcoords='data',
             arrowprops=dict(arrowstyle="->"))

plt.annotate('B', 
             xy=(ind_b, 30),  
             xycoords='data',
             xytext=(ind_b + 10, 30),
             textcoords='data',
             arrowprops=dict(arrowstyle="->"))

plt.ylim(0, (np.max(taxis) - np.min(taxis)) * 1e6)
plt.title('Amplitude of arrival over a full wave cycle' +\
          '\n sinusoindal wave setup of Walstead and Dean')

plt.show(block=False)
