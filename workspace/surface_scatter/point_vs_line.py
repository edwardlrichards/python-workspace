import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
from icepyks.newfie import f_synthesis, process_results
from icepyks import pulse_signal
import os

line_name = '../newfie/results/sine_hk_v_h_J214_14_06_35.nc'
point_name = '../newfie/results/pointsource_a.nc'
curr_dir = os.path.dirname(__file__)
file_name = os.path.join(curr_dir, line_name)
bb_result = xr.open_dataset(file_name)
h2 = process_results.combine_ri(bb_result, 'Helm_2nd')
hk = process_results.combine_ri(bb_result, 'HK')
t_dir = 0.053437398472937998  # hardcode time offset

# Create arrival time series
signal_ts = pulse_signal.pulse_narrowband()
sig_FT = f_synthesis.signal_FT(signal_ts, hk.Hz)
ts_series_hk = f_synthesis.synthesis_ts(hk * sig_FT)
ts_series_2 = f_synthesis.synthesis_ts(h2 * sig_FT)

fig, ax = plt.subplots()
ax.plot(ts_series_hk.sec * 1e3, np.real(ts_series_hk), 'k',
        label='Kirchoff')
ax.plot(ts_series_2.sec * 1e3, np.real(ts_series_2), 'g',  linewidth=4,
        label='Helmholtz', alpha=0.5)
ax.set_title('Helmholtz solution and Kirchoff approximation' +
             '\nPoint A')
ax.set_xlabel('time, ms')
ax.set_ylabel('amplitude')
ax.set_xlim(56.5, 61)
ax.grid()
ax.legend(loc=4)
xticks = ax.xaxis.get_major_ticks()[0].label1.set_visible(False)

file_name = os.path.join(curr_dir, point_name)
point_result = xr.open_dataset(file_name)
fig, ax = plt.subplots()
ax.plot(ts_series_hk.sec * 1e3, np.real(ts_series_hk), 'k', label='line')
ax.plot(point_result.delay + t_dir * 1e3,
        np.real(point_result.point_a), 'g',  linewidth=4,
        label='point', alpha=0.5)
ax.set_title('Kirchoff result for point and line source' +
             '\nPoint A')
ax.set_xlabel('time, ms')
ax.set_ylabel('amplitude')
ax.set_xlim(56.5, 61)
ax.grid()
ax.legend(loc=4)
ax.xaxis.get_major_ticks()[0].label1.set_visible(False)

shadow_file = '/home/e2richards/python-workspace/icepyks/newfie/results/shadowed.nc'
shadow_result = xr.open_dataset(shadow_file)
shad_a = shadow_result.sel(wave_phase = -200, method='nearest')
shad_b = shadow_result.sel(wave_phase = -150, method='nearest')
fig, ax = plt.subplots()
ax.plot(shad_a.delay + t_dir * 1e3, np.real(shad_a.snapshot), 'k',
        label='Kirchoff')
ax.plot(ts_series_2.sec * 1e3, np.real(ts_series_2), 'g',  linewidth=4,
        label='Helmholtz', alpha=0.5)
ax.set_title('Helmholtz solution and shadowed Kirchoff approximation' +
             '\nPoint B')
ax.set_xlabel('time, ms')
ax.set_ylabel('amplitude')
ax.set_xlim(56.5, 61)
ax.grid()
ax.legend(loc=4)
ax.xaxis.get_major_ticks()[0].label1.set_visible(False)


line_name = '../newfie/results/sine_hk_v_h_J214_12_55_28.nc'
point_name = '../newfie/results/pointsource_b.nc'
file_name = os.path.join(curr_dir, line_name)
bb_result = xr.open_dataset(file_name)
h2 = process_results.combine_ri(bb_result, 'Helm_2nd')
hk = process_results.combine_ri(bb_result, 'HK')
ts_series_hk = f_synthesis.synthesis_ts(hk * sig_FT)
ts_series_2 = f_synthesis.synthesis_ts(h2 * sig_FT)


fig, ax = plt.subplots()
ax.plot(ts_series_hk.sec * 1e3, np.real(ts_series_hk), 'k',
        label='Kirchoff')
ax.plot(ts_series_2.sec * 1e3, np.real(ts_series_2), 'g',  linewidth=4,
        label='Helmholtz', alpha=0.5)
ax.set_title('Helmholtz solution and Kirchoff approximation' +
             '\nPoint B')
ax.set_xlabel('time, ms')
ax.set_ylabel('amplitude')
ax.set_xlim(56.5, 61)
ax.grid()
ax.legend(loc=4)
ax.xaxis.get_major_ticks()[0].label1.set_visible(False)

file_name = os.path.join(curr_dir, point_name)
point_result = xr.open_dataset(file_name)
fig, ax = plt.subplots()
ax.plot(ts_series_hk.sec * 1e3, np.real(ts_series_hk), 'k', label='line')
ax.plot(point_result.delay + t_dir * 1e3,
        np.real(point_result.point_b), 'g',  linewidth=4,
        label='point', alpha=0.5)
ax.set_title('Kirchoff result for point and line source' +
             '\nPoint B')
ax.set_xlabel('time, ms')
ax.set_ylabel('amplitude')
ax.set_xlim(56.5, 61)
ax.grid()
ax.legend(loc=4)
ax.xaxis.get_major_ticks()[0].label1.set_visible(False)

fig, ax = plt.subplots()
ax.plot(shad_b.delay + t_dir * 1e3, np.real(shad_b.snapshot), 'k',
        label='Kirchoff')
ax.plot(ts_series_2.sec * 1e3, np.real(ts_series_2), 'g',  linewidth=4,
        label='Helmholtz', alpha=0.5)
ax.set_title('Helmholtz solution and shadowed Kirchoff approximation' +
             '\nPoint B')
ax.set_xlabel('time, ms')
ax.set_ylabel('amplitude')
ax.set_xlim(56.5, 61)
ax.grid()
ax.legend(loc=4)
ax.xaxis.get_major_ticks()[0].label1.set_visible(False)

plt.show(block=False)
