import os
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks.newfie.process_results import combine_ri
from icepyks.newfie import f_synthesis
from icepyks import pulse_signal

# phases of interest
phase_a = -200  # 80m caustic feature
phase_b = -150  # Shadowed 80m caustic feature
result_b_file = '../newfie/results/sine_hk_v_h_J214_12_55_28.nc'
result_a_file = '../newfie/results/sine_hk_v_h_J214_14_06_35.nc'

# Create arrival time series
signal_ts = pulse_signal.pulse_narrowband()

def load_ts(file_name):
    """Load helmholtz simulation result and generate timeseries"""
    curr_dir = os.path.dirname(__file__)
    file_name = os.path.join(curr_dir, file_name)
    bb_result = xr.open_dataset(file_name)

    t_dir = np.linalg.norm(bb_result.attrs['sd'] - bb_result.attrs['rd']) /\
            bb_result.attrs['soundspeed']
    #h1 = combine_ri(bb_result, 'Helm_1st')
    h2 = combine_ri(bb_result, 'Helm_2nd')
    hk = combine_ri(bb_result, 'HK')
    sig_FT = f_synthesis.signal_FT(signal_ts, hk.Hz)
    ts_series_hk = f_synthesis.synthesis_ts(hk * sig_FT)
    ts_series_2 = f_synthesis.synthesis_ts(h2 * sig_FT)
    return ts_series_hk, ts_series_2, t_dir

hk_a, h2_a, _ = load_ts(result_a_file)
hk_b, h2_b, t_dir = load_ts(result_b_file)

fig = plt.figure()

axs_1 = fig.add_subplot(211)
axs_1.plot((hk_a.sec - t_dir) * 1e3, hk_a, label='HK')
axs_1.plot((h2_a.sec - t_dir) * 1e3, h2_a, label='Helmholtz')
axs_1.set_title('Line source surface scatter\n phase A')
axs_1.set_xlim(2, 9)
axs_1.legend()
axs_1.grid()
plt.setp(axs_1.get_xticklabels(), visible=False)

axs_2 = fig.add_subplot(212, sharex=axs_1, sharey=axs_1)
axs_2.plot((hk_b.sec - t_dir) * 1e3, hk_b, label='HK')
axs_2.plot((h2_b.sec - t_dir) * 1e3, h2_b, label='Helmholtz')
axs_2.set_xlabel('delay, ms')
axs_2.set_title('phase B')
axs_2.set_xlim(2, 9)
axs_2.grid()

plt.show(block=False)
