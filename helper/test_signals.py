from os import path
import numpy as np
from pandas import Index
import xarray as xr
from helper import config
from kam11 import context
from sioLoad import loadSIO
from scipy.interpolate import interp1d
import scipy.signal as sig

def create_ts(bell_A, tbounds, tscale=1e3):
    """Create a synthetic time series for each element in bell_A"""
    arrival_df = bell_A.arrivals
    #create synthetic signal
    x = mfchirp()
    signal_interp = _create_interp(x, tscale)
    try:
        ts = []
        for arr in arrival_df:
            ts.append(_oneelement_ts(arr, signal_interp, tbounds, tscale))
        index = ts[0].time.values
    except TypeError:
        ts = _oneelement_ts(arrival_df, signal_interp, tbounds, tscale)
        index = ts.time.values
    rd =  Index(bell_A.rd, name='depth')
    total_ts = np.squeeze(xr.concat(ts, rd))
    return total_ts

def _create_interp(signal, tscale):
    """Create an interpolator for the X-mitt signal"""
    synth_upsample = 10
    #lfm signal specifications
    fs = context.fs
    lfmInfo = context.LFM_Info()
    signalLength = signal.size
    #allow for scaling of the t axis
    fsignal = fs / tscale
    signalt = np.arange(signalLength) / fsignal
    signalt -= np.mean(signalt)
    #Synthetic signal interpolator, need to be complex for phase
    x_hilb = sig.hilbert(signal)
    xUp, tUp = sig.resample(x_hilb, signalLength * synth_upsample, t = signalt)
    #interpolator used to create time series
    signal_interp = interp1d(tUp, xUp, bounds_error=False, fill_value=0,
                            copy=False, assume_sorted=True)
    return signal_interp


def _oneelement_ts(a_df, signal_interp, tbounds, tscale):
    """create a time series from arrivals predicted by bellhop"""
    fs = context.fs
    fsignal = fs / tscale
    #linear superposition of signals to make time series
    taxis = np.arange(int((max(tbounds) - min(tbounds)) * fsignal)) / fsignal
    taxis += min(tbounds)
    p = np.zeros(taxis.size, dtype='complex_')
    for i, arr in a_df.iterrows():
        p += arr['amp'] * np.exp(1j * np.radians(arr['degree'])) *\
                signal_interp(taxis - arr['time'])
    return xr.DataArray(np.real(p), dims=['time'], coords=[taxis])

def mfchirp(scale=1e3):
    probe_name = 'TX1W_S02_N01_R09_TR_V01.sio'
    probe_dir = config.probe_dir()
    probe_file = path.join(probe_dir, probe_name)
    #load lfm chirp from KAM11 data file
    load_length=5e3
    x = loadSIO.load_selection(probe_file, 47900, load_length, [0])['data']
    mf = x[::-1]
    x = sig.convolve(x, mf)
    signalLength = x.size
    signalt = (np.arange(signalLength) / context.fs) * scale
    signalt -= np.mean(signalt)
    return xr.DataArray(x, coords=[signalt], dims=['time'])
