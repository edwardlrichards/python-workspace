import xarray as xr
from numpy import squeeze, array, amin, amax

def shift(da, channel_shifts, dim='depth'):
    """Apply channel shifts to specified dimension. Return merged DataArray"""
    if channel_shifts is None:
        return da

    channel_shifts = squeeze(array(channel_shifts))

    #Account for channel delays: split da, shifting time, and recombine

    def split_channels(channel_shifts=channel_shifts):
        """shift all channels time, recombine on first channels indicies"""
        ref_channel = da[{dim: 0}].copy()
        ref_channel['time'] = ref_channel['time'] - channel_shifts[0]
        #ref_channel['time'] -= channel_shifts[0]
        yield ref_channel

        shift_channels = da[{dim: slice(1, None)}]
        channel_shifts = channel_shifts[1:]

        for chan, tau in zip(shift_channels.groupby(dim), channel_shifts):
            ts = chan[1].copy()
            # shift time axis
            ts.coords['time'] = ts.coords['time'] - tau
            #TODO: find bug in this line
            #ts['time'] -= tau
            ts = ts.reindex_like(ref_channel, method='pad', tolerance = 1e-2)
            yield ts

    #Merge each channel accounting for any delay
    tom = list(split_channels())
    result = xr.concat(tom, dim=dim)
    # set nans to 0
    result = result.fillna(0)
    return result
