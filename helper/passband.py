import scipy.signal as sig
from numpy import array, squeeze, ceil, log2
import xarray as xr
from kam11 import context
from helper import input_axis
import concurrent.futures

def hilbert_pb(ts, taxis=None):
    """Return the passbanded hilbert transform. Frequencies of KAM11
    returns DataArray. Will transpose data to make time last index
    """
    #Sanitize data to DataArray
    ts = input_axis.prep_bylabel(ts, 'time', inaxis=taxis)
    #Singal processing with arrays
    num_times = ts.shape[-1]
    NFFT = int(2**ceil(log2(num_times)))

    #hilbert transform
    h_ts = sig.hilbert(array(ts, ndmin=2), NFFT)
    h_ts = h_ts[:, :num_times]

    #passband filter
    f = array(passband_filter(), ndmin=2)
    h_ts = sig.fftconvolve(h_ts, f, mode='valid')
    #use ts coordinates in final result, but drop filter edge values
    tStart = ts.time[f.size // 2].values
    tEnd = ts.time[-(f.size // 2)].values
    ts = ts.sel(time=slice(tStart, tEnd))
    ts.values = h_ts
    return squeeze(ts)

def passband_filter():
    """Passband filter used for KAM11 data"""
    fc = context.LFM_Info().fc
    bw = context.LFM_Info().bw
    fs = context.fs
    fw = bw / 2
    numtaps = 2**8
    rolloff = 1/8 * fw
    bands = [0, fc - (fw + rolloff),
            fc - fw, fc + fw,
            fc + (fw + rolloff), fs / 2]

    return sig.remez(numtaps, bands, [0,1,0], Hz = fs)

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import numpy as np

    pb = passband_filter()
    NFFT = 512
    f = np.arange(NFFT) / NFFT * context.fs
    pb_FT = np.fft.fft(pb, NFFT)
    dB = 20 * np.log10(np.abs(pb_FT) + np.spacing(1))
    dB -= np.max(dB)

    plt.figure()
    plt.plot(f, dB)
    plt.title('Passband filter')
    plt.xlabel('Frequency, Hz')
    plt.ylabel('Magnitude, dB ref max')
    plt.xlim(0, context.fs/2)
    plt.ylim(-100, 2)

    plt.figure()
    plt.plot(f, dB)
    plt.title('Passband filter')
    plt.xlabel('Frequency, Hz')
    plt.ylabel('Magnitude, dB ref max')
    plt.xlim(10500, 35500)
    plt.ylim(-0.5, 0.5)

    plt.show(block=False)
