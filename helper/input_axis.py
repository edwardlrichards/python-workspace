from xarray import DataArray, concat
from numpy import array, squeeze

def prep_bylabel(indata, label, inaxis=None):
    """For array or DataArray, make indata have label in last dimension"""
    outdata = DataArray(indata)
    #if input is 1D make it 2D
    if len(outdata.shape) < 2:
        outdata = concat([outdata], 'angle')
    try:
        axis = outdata.get_axis_num(label)
        if axis == 0:
            outdata = outdata.T
    except Exception:
        outdata = outdata.rename({outdata.dims[-1]: label})
        if inaxis is not None:
            outdata = outdata.assign_coords(time=inaxis)
    return outdata

def get_label(indata, xaxis=None, yaxis=None):
    """Extract axis information if DataArray, but give preference to UI"""
    indata = DataArray(indata)
    if xaxis is None:
        xaxis = array(indata.index)
    if yaxis is None:
        yaxis = array(indata.columns)
    #only remove length 1 axis if it is in axis[1]
    indata = array(indata)
    if indata.shape[1] == 1:
        return squeeze(indata), xaxis
    else:
        return indata, xaxis, yaxis

def set_label(indata, xaxis=None, yaxis=None):
    """Return a pandas data structure appropriate to input data"""
    indata = DataFrame(indata, ndmin=2)
    if xaxis is not None:
        indata.index = xaxis
    if yaxis is not None:
        indata.columns = yaxis
    return squeeze(indata)
