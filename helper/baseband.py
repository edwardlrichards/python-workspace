from numpy import arange, array, exp, pi, complex_, squeeze
from numpy.testing import assert_allclose
from pandas import DataFrame, Series
from scipy.signal import fftconvolve, remez, convolve
from kam11 import context
from helper import input_axis

FS = context.fs
FC = context.LFM_Info().fc
BW = context.LFM_Info().bw

def baseband(data, fs = FS, fc = FC, bw = BW, taxis=None, b=None):
    """modulate and then lowpass signal
    try and return same type of data as input
    if taxis is none, data needs to have taxis as index
    """
    #Sanitize data to DataArray
    data = input_axis.prep_bylabel(data, 'time', inaxis=taxis)
    #modulation, ensure basebanding signal has dt of 1/FS
    dt = float(data.time[1] - data.time[0])
    scale = 1 / dt / FS
    data.values = array(data.values, dtype='complex_') *\
            exp(-1j * 2 * pi * data.time.values * FC * scale)
    # Create a lowpass filter
    if b is None:
        b = _default_filter()
    dataMatrix = fftconvolve(array(data, ndmin=2), array(b, ndmin=2), mode='valid')
    dataMatrix = squeeze(dataMatrix)
    #drop filter edge values
    tStart = data.time[b.size // 2].values
    tEnd = data.time[-(b.size // 2)].values
    data = data.sel(time=slice(tStart, tEnd))
    data.values = dataMatrix
    return data

def _default_filter():
    """default lowpass filter used in basebanding"""
    filterlength = 256
    filtercut = BW / 2
    b = remez(filterlength,
                [0, filtercut, filtercut + (filtercut / 4), FS / 2],
                [1, 0], Hz=FS)
    return b
