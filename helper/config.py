import os
import sys
import configparser

config_filename = os.path.expanduser('~/.config/kam11.cfg')
config = configparser.ConfigParser()

def main():
    """prompt user for relavent information"""
    probe_dir = input('Enter directory containing probe signal: ')
    acoustic_dir = input('Enter directory containing acoustic data: ')
    enviornment_dir = input('Enter directory containing enviornment data: ')
    bellhop_dir = input('Enter path to bellhop.exe: ')
    write_config(probe_dir, acoustic_dir, enviornment_dir, bellhop_dir)

def write_config(probe_dir, acoustic_dir, enviornment_dir, bellhop_dir):
    config.add_section('filelocations')
    config['filelocations']['probe_dir'] = probe_dir
    config['filelocations'][ 'enviornment_dir'] = enviornment_dir
    config['filelocations']['acoustic_dir'] = acoustic_dir
    config['filelocations']['bellhop_dir'] = bellhop_dir

    """write a blank config file"""
    with open(config_filename, 'w') as f:
        config.write(f)

def probe_dir():
    config.read(config_filename)
    return config.get('filelocations', 'probe_dir')

def acoustic_dir():
    config.read(config_filename)
    return config.get('filelocations', 'acoustic_dir')

def enviornment_dir():
    config.read(config_filename)
    return config.get('filelocations', 'enviornment_dir')

def bellhop_dir():
    config.read(config_filename)
    return config.get('filelocations', 'bellhop_dir')

if __name__ == '__main__':
    main()
