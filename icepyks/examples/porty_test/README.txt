.. _porty_examples:
Porty examples
--------------

Solutions which use free-space iso-speed Green's function at discrete points along surface, and then use the Kirchoff approximation to solve for these source amplitudes.
