"""
============================
Young Min's test case, sd=20
============================

Unshadowed version of Choo et al. results for sd = 20m
"""

import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks.porty import sin_surface, pulse_signal, hk_integral,\
                  iso_speed, surface_field
from scipy.signal import hilbert

cmap = plt.cm.magma_r
cmap.set_under('w')


# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 40
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 1.5
c = 1500
num_phase = 80

#signal parameter
fc = 20e3

# pad result for better plotting
num_cycles = 4

#Setup integration bounds
x_bounds = (-50, 250)
y_bounds = (-100, 100)
t_bounds = (0.133, 0.1385)
fs = 1e5
dx = 3e-2
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
yaxis = np.arange(min(y_bounds), max(y_bounds), dy)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
X, Y = np.meshgrid(xaxis, yaxis)

#Solve for wave surface
r_source = np.array((0, 0, 20))
r_receiver = np.array((200, 0, 10))
# correct for wave z offset
r_source[2] -= wave_amp
r_receiver[2] -= wave_amp

signal = pulse_signal.sine_four_hanning(fc)
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j]
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]

t_series = []
for p in wave_phase:
    # setup wave field for each wave phase
    eta = sin_surface.Eta(lamda_surf, wave_amp, phase=p)
    field1 = iso_speed.IsospeedField(eta, r_source, c, x_bounds,
                                    x1_bounds, is_grad=True)
    field2 = iso_speed.IsospeedField(eta, r_receiver, c, x_bounds,
                                    x1_bounds, is_grad=False)
    # compute stationary phase path
    at_top = surface_field.SurfaceField(field2, field1, c)
    at_top.setup_stationary_phase(x_bounds, x1_bounds)

    # perform hk integral
    hk_solver = hk_integral.HK(signal, taxis)
    t_series.append(hk_solver(xaxis, at_top))

# add direct arrival
distance = np.linalg.norm(r_source - r_receiver)
t_dir = distance / c
t_axis =  t_series[0].time
amp_dir = 1 / distance
dir_i = np.digitize(signal.time + t_dir, t_axis)
ud, ui = np.unique(dir_i, return_index=True)
dir_sig = np.zeros_like(t_axis)
dir_sig[ud] = np.array(np.abs(hilbert(signal))[ui]) * amp_dir

temp_ts = [np.array(ts) + dir_sig for ts in t_series]
t_series = xr.DataArray(np.array(temp_ts),
                             dims=['time', 'delay'],
                             coords=[wave_phase / w_surf,
                             t_axis * 1e3])

plt.figure()
plt.plot(signal.time * 1e3, signal.values)
plt.title('direct pressure time series, t0 = %.1f ms'%(t_dir * 1e3))
plt.xlabel('time, ms')
plt.ylabel('amplitude, pressure')
plt.grid()

amp_lim = np.max(np.abs(t_series))

plt.figure()
plt.plot(t_series.delay, t_series[0])
plt.title('Scattered pressure time series, t=%.1f ms'%(
        t_bounds[0] * 1e3 + t_series.delay[0]))
plt.ylim(-amp_lim, amp_lim)
plt.xlabel('time, ms')
plt.ylabel('amplitude, pressure')
plt.grid()

h_ts = hilbert(t_series).T
h_ts = np.tile(h_ts, num_cycles)
t_wave = np.r_[0: num_cycles * 2 * np.pi: num_cycles * num_phase * 1j]
t_wave /= w_surf
X, Y = np.meshgrid(t_series.delay, t_wave)
X -= t_dir * 1e3 - 5
h_db = 20 * np.log10(np.abs(h_ts) + np.spacing(1))
h_db -= np.max(h_db)

plt.figure()
pc = plt.pcolormesh(X, Y, h_db.T, vmin=-25, vmax=0, cmap=plt.cm.jet)
plt.colorbar(pc)
plt.xlabel('time, ms')
plt.ylabel('Experimental time, sec')
plt.title('Amplitude of arrival over a full wave cycle' +\
          '\n Young Min sinusoindal wave setup')
plt.xlim(4.5, 9.5)
plt.ylim(0, 20)
plt.grid()

plt.show(block=False)
