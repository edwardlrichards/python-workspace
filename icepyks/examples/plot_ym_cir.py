"""
==========================
Numerical model comparison
==========================

Ray tracing code v. greens function code. In iso-speed enviornments ray
tracing has the additional capability of a shadowing correction, but the
results should be the same when this is turned off. Secondly, shadowing only
removes a portion of the total energy used in the HK integral, so distinct
arrivals are expected to be affected, but the overall picture should remain
the same before and after shadowing.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal
from icepyks.porty import hk_integral, iso_speed, surface_field
from scipy.signal import hilbert
from icepyks.jrussell import iso_rg as iso_rg
from icepyks.jrussell.propagator import Propagator
from icepyks.jrussell.hk_gridded import Field
import concurrent.futures

#cmap = plt.cm.jet
cmap = plt.cm.magma_r
cmap.set_under('w')

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 40
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
z_off = 1  # m
wave_amp = 1.5
c = 1500
num_phase = 60
r_source = np.array((0., 0, 20))
r_receiver = np.array((200., 0, 10))
z_rcr = r_receiver[-1]

# plot time series time
t_oi = 6  # seconds

# calculate flat surface quantities
dir_r = np.linalg.norm(r_source - r_receiver)
spec_r = np.linalg.norm(r_source + r_receiver)
t_dir = dir_r / c
t_spec = spec_r / c
a_bounds = (-r_receiver[0] * 0.2, r_receiver[0] * 1.2)
dx1 = .1  # m

# correct for wave z offset
r_source[2] -= (z_off + wave_amp)
r_receiver[2] -= (z_off + wave_amp)

#signal parameter
fc = 20e3

#Setup integration bounds
x_bounds = a_bounds
y_bounds = (-100, 100)
z_bottom = 300
t_bounds = (np.floor(t_dir * 1e3) / 1e3, np.ceil(t_spec * 1e3 + 4) / 1e3)
fs = 5e5
dx = 3e-2
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
yaxis = np.arange(min(y_bounds), max(y_bounds), dy)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
X, Y = np.meshgrid(xaxis, yaxis)

#Solve for wave surface
signal = pulse_signal.sine_four_hanning(fc)
# signal = pulse_signal.pulse_udel()
# signal = signal.sel(time=slice(0.023, 0.025))
# signal['time'] = signal.time - np.min(signal.time)
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (np.pi)
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]

# numerical solution
src_rg = iso_rg.Isospeed(c, r_source)
rrc_rg = iso_rg.Isospeed(c, r_receiver)
hk_solver = hk_integral.HK(signal, taxis)
proper = Propagator(src_rg, rrc_rg, a_bounds, z_bottom)

def one_phase(p):
    """Run models for a single phase"""
    # setup wave field for each wave phase
    eta = sin_surface.Eta(lamda_surf, wave_amp, phase=p, z_offset=z_off)
    field1 = iso_speed.IsospeedField(eta, r_source, c, is_grad=True)
    field2 = iso_speed.IsospeedField(eta, r_receiver, c, is_grad=False)
    # numeric solution
    one_wave = proper.for_wave(eta)
    diff_field = Field(one_wave)
    # compute stationary phase path
    at_top = surface_field.SurfaceField(field2, field1, c)
    at_top.setup_stationary_phase(x_bounds, x1_bounds)
    port_result = hk_solver(xaxis, at_top)
    jr_result = hk_solver(xaxis, diff_field)
    return port_result, jr_result

#with concurrent.futures.ProcessPoolExecutor(max_workers=6) as executor:
    #all_result = list(executor.map(one_phase, wave_phase))
all_result = list(map(one_phase, wave_phase))

# add direct arrival
t_axis =  all_result[0][0].time
amp_dir = 1 / dir_r
dir_i = np.digitize(signal.time + t_dir, t_axis)
ud, ui = np.unique(dir_i, return_index=True)
dir_sig = np.zeros_like(t_axis)
dir_sig[ud] = np.array(np.abs(hilbert(signal))[ui]) * amp_dir

t_series_port = np.array([np.array(ts[0]) + dir_sig for ts in all_result])
t_series_port = xr.DataArray(np.array(temp_ts),
                             dims=['phase', 'delay'],
                             coords=[np.degrees(wave_phase),
                             t_axis * 1e3])
t_series_jr = np.array([np.array(ts[1]) + dir_sig for ts in all_result])
t_series_jr = xr.DataArray(np.array(temp_ts),
                           dims=['phase', 'delay'],
                           coords=[np.degrees(wave_phase),
                           t_axis * 1e3])

# used to agree with youngmin's unexplained start at 5 ms delay
tplot = (taxis - t_dir) * 1e3 + 5 - 0.1
# setup ploting quantities
amp_lim = max(np.max(np.abs(t_series_port)), np.max(np.abs(t_series_jr)))
#db_max = float(20 * np.log10(amp_lim))
db_ref = float(20 * np.log10(np.max(dir_sig)))

# Create a wave train time series
num_cycles = 4
dt = (wave_phase[1] - wave_phase[0]) / w_surf
t_wave = np.r_[0: num_cycles * 2 * np.pi: num_cycles * wave_phase.size * 1j]
t_wave /= w_surf

pi = np.argmin(np.abs(twave - t_oi))

# plot travel time curve for time of interest
plt.figure()
plt.plot(t_axis * 1e3, t_series_port[:, t_oi], label='analytic')
plt.plot(t_axis * 1e3, t_series_jr[:, t_oi], 'g', linewidth=2,
         label='numeric')
plt.title('Scattered pressure time series, t=%.1f s'%(
        t_wave[t_oi]))
#plt.ylim(-amp_lim, amp_lim)
plt.ylim(-.0015, .0015)
plt.xlim(t_bounds[0] * 1e3, t_bounds[1] * 1e3)
plt.xlabel('time, ms, t0 = %.1f ms'%(t_bounds[0] * 1e3))
plt.ylabel('amplitude, pressure')
plt.grid()

# stack a few cycles for better visulization
h_ts = hilbert(t_series_port).T
h_ts = np.tile(h_ts, num_cycles)
h_db = 20 * np.log10(np.abs(h_ts)) - db_ref
# X, Y = np.meshgrid(t_series_port.delay - t_dir * 1e3, t_wave)
X, Y = np.meshgrid(tplot, t_wave)
ref_t = t_dir * 1e3
plt.figure()
pc = plt.pcolormesh(X, Y, h_db.T, vmax=amp_lim, vmin=-20, cmap=cmap)
plt.plot(np.zeros_like(t_wave), t_wave, 'r', linewidth=2)
plt.colorbar(pc)
plt.ylabel('experiment time, s')
plt.xlabel('time, ms, t0 = %.1f ms'%(t_dir * 1e3))
plt.ylim(np.max(t_wave), np.min(t_wave))
plt.xlim(4.5, 9.5)
plt.title('Magnitude of arrival over a full wave cycle' +\
          '\n analytic solution, rd = %.1f'%(z_rcr))
plt.grid()

h_ts = hilbert(t_series_jr).T
h_ts = np.tile(h_ts, num_cycles)
h_db = 20 * np.log10(np.abs(h_ts)) - db_ref
#X, Y = np.meshgrid(t_wave, t_series_jr.delay)
X, Y = np.meshgrid(tplot, t_wave)

plt.figure()
pc = plt.pcolormesh(X, Y, h_db.T, vmax=amp_lim, vmin=-20, cmap=cmap)
#pc = plt.pcolormesh(X, Y, h_db.T, cmap=cmap)
plt.plot(np.zeros_like(t_wave), t_wave, 'r', linewidth=2)
plt.colorbar(pc)
plt.ylabel('experiment time, s')
plt.xlabel('time, ms, t0 = %.1f ms'%(t_dir * 1e3))
plt.ylim(np.max(t_wave), np.min(t_wave))
plt.xlim(4.5, 9.5)
plt.title('Magnitude of arrival over a full wave cycle' +\
          '\n numeric solution, rd = %.1f'%(z_rcr))
plt.grid()

plt.show(block=False)
