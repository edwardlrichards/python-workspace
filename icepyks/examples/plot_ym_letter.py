import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks import sin_surface, pulse_signal
from icepyks.porty import hk_integral, iso_speed, surface_field
from scipy.signal import hilbert
from icepyks.jrussell import iso_rg as iso_rg
from icepyks.jrussell.propagator import Propagator
from icepyks.jrussell.hk_gridded import Field

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 40
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 1.5
c = 1500
num_phase = 40
r_source = np.array((0., 0, 20))
r_receiver = np.array((200., 0, 10))
z_rcr = r_receiver[-1]
# calculate flat surface quantities
t_dir = np.linalg.norm(r_source - r_receiver) / c
t_spec = np.linalg.norm(r_source + r_receiver) / c
dir_dist = np.linalg.norm(r_source - r_receiver)
a_bounds = (-r_receiver[0] * 0.2, r_receiver[0] * 1.2)
dx1 = .1  # m

# correct for wave z offset
r_source[2] -= wave_amp
r_receiver[2] -= wave_amp

#signal parameter
fc = 20e3

#Setup integration bounds
x_bounds = a_bounds
y_bounds = (-100, 100)
z_bottom = 300
t_bounds = (np.floor(t_dir * 1e3) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
fs = 5e5
dx = 3e-2
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
yaxis = np.arange(min(y_bounds), max(y_bounds), dy)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
X, Y = np.meshgrid(xaxis, yaxis)

#Solve for wave surface
signal = pulse_signal.sine_four_hanning(fc)
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]

# numerical solution
src_rg = iso_rg.Isospeed(c, r_source)
rrc_rg = iso_rg.Isospeed(c, r_receiver)
hk_solver = hk_integral.HK(signal, taxis)
proper = Propagator(src_rg, rrc_rg, a_bounds, z_bottom)

# Only one wave phase in this case
eta = sin_surface.Eta(lamda_surf, wave_amp, phase=0.)
field1 = iso_speed.IsospeedField(eta, r_source, c, is_grad=True)
field2 = iso_speed.IsospeedField(eta, r_receiver, c, is_grad=False)
# numeric solution
one_wave = proper.for_wave(eta)
diff_field = Field(one_wave)
# compute stationary phase path
at_top = surface_field.SurfaceField(field2, field1, c)
at_top.setup_stationary_phase(x_bounds, x1_bounds)
port_result = hk_solver(xaxis, at_top)
jr_result = hk_solver(xaxis, diff_field)
direct_amp = 1 / dir_dist

fig, ax = plt.subplots()
ax.plot(port_result.time * 1e3, port_result / direct_amp)
ax.set_xlim(134.5, 137)
ax.set_ylim(-0.6, 0.6)
ax.set_xlabel('delay, ms')
ax.set_title('Time series from Choo and Song letter')
ax.grid()

fig, ax = plt.subplots()
ax.plot(port_result.time * 1e3, port_result / direct_amp, label='free space')
ax.plot(jr_result.time * 1e3, jr_result / direct_amp, label='shadowed')
ax.set_xlim(134.5, 137.5)
ax.set_ylim(-0.6, 0.6)
ax.set_xlabel('delay, ms')
ax.set_title('Time series from Choo and Song letter')
ax.legend(loc=2)
ax.grid()


plt.show(block=False)
