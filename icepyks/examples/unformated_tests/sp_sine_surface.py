import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks.porty import sin_surface, pulse_signal, hk_integral,\
                  iso_speed, surface_field

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 0.693
h_surf = 31e-3 / 2
num_phase = 120
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (3 * np.pi / 2)
c = 1469
# make wave move away from source
wave_phase = wave_phase[::-1]

#signal parameter
fc = 200e3

#Setup integration bounds
x_bounds = (-0.5, 1.75)
y_bounds = (-1, 1)
#taxis is from Walstead and Dean
t_bounds = (841e-6, 891e-6)
fs = 1e6
dx = 3e-3
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
yaxis = np.arange(min(y_bounds), max(y_bounds), dy)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
X, Y = np.meshgrid(xaxis, yaxis)
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]

#Solve for wave surface
r_source = np.array((0, 0, -0.2))
r_receiver = np.array((1.215, 0, -0.14))
signal = pulse_signal.pulse()

t_series = []
for p in wave_phase:
    # setup wave field for each wave phase
    eta = sin_surface.Eta(lamda_surf, h_surf, phase=p)
    field1 = iso_speed.IsospeedField(eta, r_source, c, x_bounds,
                                    x1_bounds, is_grad=True)
    field2 = iso_speed.IsospeedField(eta, r_receiver, c, x_bounds,
                                    x1_bounds, is_grad=False)
    # compute stationary phase path
    at_top = surface_field.SurfaceField(field2, field1, c)
    at_top.setup_stationary_phase(x_bounds, x1_bounds)

    # perform hk integral
    hk_solver = hk_integral.HK(signal, taxis)
    t_series.append(hk_solver(xaxis, at_top))

t_series = xr.DataArray(t_series, dims=['phase', 'delay'],
                          coords=[np.degrees(wave_phase),
                                  (taxis - np.min(taxis)) * 1e6])

distance = np.linalg.norm(r_source - r_receiver)

plt.figure()
plt.plot((signal.time + distance / c) * 1e3, signal.values)
plt.title('direct pressure time series')
plt.xlabel('time, ms')
plt.ylabel('amplitude, pressure')

plt.figure()
plt.plot((taxis - np.min(taxis)) * 1e6, t_series[0])
plt.title('Scattered pressure time series')
plt.ylim(-1, 1)
plt.xlabel('time, us')
plt.ylabel('amplitude, pressure')

plt.figure()
t_series.T.plot(vmin=-1, vmax=1, cmap=plt.cm.RdBu_r)
plt.ylim(0, (np.max(taxis) - np.min(taxis)) * 1e6)
plt.title('Amplitude of arrival over a full wave cycle' +\
          '\n sinusoindal wave setup of Walstead and Dean')

plt.show(block=False)
