import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks.porty import flat_surface, pulse_signal, hk_integral,\
                  iso_speed, surface_field

# Perform HK integration using a flat surface

#Setup integration bounds
x_bounds = (-0.5, 1.75)
y_bounds = (-1, 1)
t_bounds = (0, 2e-3)
fs = 1e6 * 4
c = 1500
dx = 2e-3
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
yaxis = np.arange(min(y_bounds), max(y_bounds), dy)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
X, Y = np.meshgrid(xaxis, yaxis)

eta = flat_surface.Flat_Eta()
r_source = np.array((0, 0, -0.2))
#r_receiver = np.array((1.215, 0, 0.14))
r_receiver = np.array((1, 0, -0.2))
signal = pulse_signal.pulse()
# setup surface field
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]
field1 = iso_speed.IsospeedField(eta, r_source, c, x_bounds,
                                 x1_bounds, is_grad=True)
field2 = iso_speed.IsospeedField(eta, r_receiver, c, x_bounds,
                                 x1_bounds, is_grad=False)
at_top = surface_field.SurfaceField(field2, field1, c)

at_top.setup_stationary_phase(x_bounds, x1_bounds)

hk_solver = hk_integral.HK(signal, taxis)
t_series = hk_solver(xaxis, at_top)

distance = np.linalg.norm(r_source - r_receiver)

plt.figure()
plt.plot((signal.time + distance / c) * 1e3, signal.values / distance)
plt.title('direct pressure time series')
plt.xlabel('time, ms')
plt.ylabel('amplitude, pressure')

plt.figure()
plt.plot(taxis * 1e3, t_series)
plt.title('Scattered pressure time series')
#plt.ylim(-1, 1)
plt.xlim(0.71, 0.74)
plt.xlabel('time, ms')
plt.ylabel('amplitude, pressure')

# estimate total power of both direct and scattered pulses
power = (float(np.sum(signal ** 2) * np.diff(signal.time)[0]),
         float(np.sum(np.abs(t_series) ** 2) * np.diff(taxis)[0]))

image_source = r_source
image_source[2] = -image_source[2]
r = (distance, np.linalg.norm(image_source - r_receiver))

print('scattering power from image source: %6e' %(power[0] / r[1] ** 2))
print('scattering power from numerical result: %6e' %(power[1]))
plt.show(block=False)
