import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from icepyks.porty import sin_surface, pulse_signal, hk_integral

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf=39.0327
h_surf=1.4

#signal parameter
fc = 20e3

#Setup integration bounds
x_bounds = (-50, 250)
y_bounds = (-100, 100)
t_bounds = (0.1, 0.2)
fs = 1e5
dx = 3e-2
dy = dx
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
yaxis = np.arange(min(y_bounds), max(y_bounds), dy)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
X, Y = np.meshgrid(xaxis, yaxis)

#Solve for wave surface
eta = sin_surface.Eta(lamda_surf, h_surf)

r_source = np.array((0, 0, -20))
#r_receiver = np.array((1.215, 0, 0.14))
r_receiver = np.array((200, 0, -10))
signal = pulse_signal.sine_four_hanning(fc)
hk_solver = hk_integral.HK(r_source, r_receiver, eta, signal)
#hk_solver = hk_integral.Nearfield_HK(r_source, r_receiver, eta, signal)
hk_solver.compute_weights(X, Y)
t_series = hk_solver.time_series(taxis)

distance = np.linalg.norm(r_source - r_receiver)

plt.figure()
plt.plot((signal.time + distance / 1500) * 1e3, signal.values)
plt.title('direct pressure time series')
plt.xlabel('time, ms')
plt.ylabel('amplitude, pressure')

plt.figure()
plt.plot(taxis*1e3, t_series)
plt.title('Scattered pressure time series')
#plt.ylim(-1, 1)
#plt.xlim(0.71, 0.74)
plt.xlabel('time, ms')
plt.ylabel('amplitude, pressure')

import sys; sys.exit("User break") # SCRIPT EXIT
weights, delays = hk_solver._path_contributions(X, Y)
plt.figure()
plt.pcolormesh(X, Y, weights)
plt.title('Path weigths')
plt.xlabel('x distance, m')
plt.ylabel('y distance, m')

plt.figure()
plt.pcolormesh(X, Y, delays)
plt.title('Path delays')
plt.xlabel('x distance, m')
plt.ylabel('y distance, m')

plt.show(block=False)
