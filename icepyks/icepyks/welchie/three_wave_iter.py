import numpy as np
import matplotlib.pyplot as plt
from icepyks.surfaces import sin_surface, flat_surface
from icepyks.welchie import plane_iso_field
from icepyks.poodle import plane_wave_rs
from icepyks.welchie import horizontal_array
from icepyks import surface_integral
from scipy.linalg import solve
from scipy.special import hankel1

f_acous = 1050  # Hz
c = 1500

# periodic surface reflection
l_wave = 40
h_wave = 2.
inter_n = 6  # points per acoustic wavelength

# make surface an integer number of waves long
num_waves = 15 * 3
dx = c / f_acous / inter_n
ppw = l_wave // dx + 1
xax_onewave = np.arange(ppw) / ppw * l_wave
dx = (xax_onewave[-1] - xax_onewave[0]) / (xax_onewave.size - 1)

x_starts = (np.arange(num_waves) - num_waves // 2) * l_wave - l_wave / 2
xaxis = np.reshape(x_starts[:, None] + xax_onewave[None, :], -1)
# add the last index
xaxis = np.hstack([xaxis, xaxis[-1] + dx])

total_x = num_waves * l_wave
wave_x = (-total_x / 2 - l_wave / 2, total_x / 2 + l_wave / 2)
g = float(np.abs(np.diff(wave_x))) / 5  # plane wave resolution parameter

# integration decimation
alpha = 0  # dB/km
cos_ier = plane_wave_rs.CosineReflectionCoefficents(h_wave,
                                                    2 * np.pi / l_wave,
                                                    attn=alpha)

cos_phase = np.pi / 2  # surface phase
ti_inc = np.radians(10.)

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous
kx_inc = np.cos(ti_inc) * k_acous

# setup problem
cos_eta = sin_surface.Sine(l_wave, h_wave, phase=cos_phase)
cos_win = plane_iso_field.FinitePlaneWave(cos_eta, g, xaxis, attn=0)
cos_win(f_acous, ti_inc)

# setup beamforming for the result
z_array = -10
dx_array = l_acous / 2
x_array = np.arange(np.min(xaxis) + 100, np.max(xaxis) - 100, dx_array)
r_array = np.array((x_array, np.full_like(x_array, z_array))).T

# common normalization term
flat_eta = flat_surface.Flat(z_offset=z_array)
flat_pw = plane_iso_field.FinitePlaneWave(flat_eta, g, x_array)
flat_pw(f_acous, ti_inc)
p_inc = flat_pw.p_inc(f_acous, ti_inc)
p_inc_FT, k_inc_axis = horizontal_array.beamform(p_inc, dx_array)
bmax = np.max(np.abs(p_inc_FT), axis=-1)

# Windowed HIE
surfer_HIE2 = surface_integral.FSurfaceIntegral(cos_win, xaxis, kind=2)
rcr_greens = surfer_HIE2.rcr_greens(r_array)

# Iterative HIE
psi_HIE2 = surfer_HIE2.psi()
A = surfer_HIE2.A_matricies(2)

# one wave iteration test
i1wave = np.arange(xax_onewave.size)
l1wave = xax_onewave.size

ier_1wave = [2 * surfer_HIE2.psi_inc()]

for i in range(1):
    ier_1wave.append(np.zeros_like(ier_1wave[0]))
    for j in range(num_waves):
        xcurr = i1wave + j * l1wave
        one_A = A[np.ix_(xcurr, xcurr)]
        iforward = one_A @ ier_1wave[-2][xcurr]
        iforward *= surfer_HIE2.gamma[xcurr] * surfer_HIE2.dx / 2j
        ier_1wave[-1][xcurr] = ier_1wave[0][xcurr] - iforward

ier_1wave = np.array(ier_1wave)

# extended wave iteration test
# manually build up an flat extension matrix
xax_3wave = np.hstack([xax_onewave, xax_onewave[-1] + dx]) - l_wave / 2
xax_3wave = np.hstack([xax_3wave[: -1] - l_wave,
                       xax_3wave[: -1],
                       xax_3wave + l_wave])

i3wave = np.arange(xax_3wave.size)
l3wave = xax_3wave.size
z_wave = cos_eta.z(xax_3wave)
num_ext = 300

z_ext = np.hstack([np.full(num_ext, z_wave[0]),
                   z_wave,
                   np.full(num_ext, z_wave[-1])])

x_ext = np.arange(num_ext) * dx
x_ext = np.hstack([x_ext - dx - np.max(x_ext) + xax_3wave[0],
                   xax_3wave,
                   x_ext + dx + xax_3wave[-1]])

r_ext = np.array([x_ext, z_ext])
r_diff = r_ext[:, :, None] - r_ext[:, None, :]

rel_proj = np.vstack([[[0, 1]] * num_ext,
                      cos_eta.n_hat(xax_3wave),
                      [[0, 1]] * num_ext]).T
rel_proj = np.add.reduce(rel_proj[:, :, None] * r_diff, axis=0)
ndi = np.ones(rel_proj.shape, dtype=np.bool_)
ndi[np.diag_indices_from(rel_proj)] = False

r_diff = np.linalg.norm(r_diff, axis=0)

rel_proj[ndi] = rel_proj[ndi] / r_diff[ndi]

gamma = cos_eta.norm_grad(xax_3wave)
gamma = np.hstack([np.ones(num_ext), gamma, np.ones(num_ext)])

f_pp = cos_eta.z_pp(xax_3wave)
f_pp = np.hstack([np.zeros(num_ext), f_pp, np.zeros(num_ext)])

kr = -k_acous * hankel1(1, k_acous * r_diff)
sing_values = -1j * f_pp / (np.pi * gamma ** 3)

A_ext = kr * rel_proj
A_ext[np.diag_indices_from(A_ext)] = sing_values

test_m = A_ext[num_ext : -num_ext, num_ext : -num_ext]

ier_exwave = [2 * surfer_HIE2.psi_inc()]

for i in range(9):
    ier_exwave.append(np.zeros_like(ier_exwave[0]))

    for j in range(num_waves - 2):
        icurr = i3wave + j * l1wave
        one_b = ier_exwave[-2][icurr]
        ext_b = one_b * np.exp(-1j * kx_inc * xaxis[icurr]) 
        ext_b = np.hstack([np.full(num_ext, ext_b[0]),
                           ext_b,
                           np.full(num_ext, ext_b[-1])])
        newx = x_ext + xaxis[icurr][0] - xax_3wave[0]
        ext_b *= np.exp(1j * kx_inc * newx) 
        iforward = A_ext @ ext_b
        # cut off edges
        if j == 0:
            iwin = icurr[: 2 * l1wave + 1]
            iforward = iforward[num_ext: num_ext + 2 * l1wave + 1]
        elif j == num_waves - 2:
            iwin = icurr[l1wave: ]
            iforward = iforward[num_ext + l1wave: -num_ext]
        else:
            iwin = icurr[l1wave: -l1wave]
            iforward = iforward[num_ext + l1wave: -(num_ext + l1wave)]
        iforward *= surfer_HIE2.gamma[iwin] * surfer_HIE2.dx / 2j
        ier_exwave[-1][iwin] = ier_exwave[0][iwin] - iforward

ier_exwave = np.array(ier_exwave)

ref = psi_HIE2 * np.exp(-1j * kx_inc * xaxis)
p_hk = ier_1wave[0, :] * np.exp(-1j * kx_inc * xaxis)
p_HIE1_local = ier_1wave[-1, :] * np.exp(-1j * kx_inc * xaxis)
p_HIE1_ext = ier_exwave[1, :] * np.exp(-1j * kx_inc * xaxis)
p_HIE1_ext1 = ier_exwave[-1, :] * np.exp(-1j * kx_inc * xaxis)

# reflection coefficents
p_HIE2 = surfer_HIE2(r_array, f_acous, psi=psi_HIE2, rcr_greens=rcr_greens)
r_HIE2, k_axis = horizontal_array.beamform(p_HIE2, dx_array)

p_HK = surfer_HIE2(r_array, f_acous,
                   psi=ier_exwave[0, :], rcr_greens=rcr_greens)
r_HK, _ = horizontal_array.beamform(p_HK, dx_array)

p_1wave = surfer_HIE2(r_array, f_acous,
                   psi=ier_1wave[-1, :], rcr_greens=rcr_greens)
r_1wave, _ = horizontal_array.beamform(p_1wave, dx_array)

p_Iend = surfer_HIE2(r_array, f_acous,
                     psi=ier_exwave[-1, :], rcr_greens=rcr_greens)

r_Iend, _ = horizontal_array.beamform(p_Iend, dx_array)

fig, ax = plt.subplots()

ax.plot(k_axis, np.abs(r_HK - r_HIE2) / bmax, label='HK')
ax.plot(k_axis, np.abs(r_1wave - r_HIE2) / bmax, label='1 wave')
ax.plot(k_axis, np.abs(r_Iend - r_HIE2) / bmax, label='3 wave')
ax.plot([k_acous, k_acous], [-100, 100], ':', color='0.4')
ax.set_xlim(2, 6)
ax.set_ylim(0, 1)
ax.set_title(r'HKA Reflection coefficents for $\theta_{inc}$=%.1f$^o$'%
             np.degrees(ti_inc) + '\n' +
             'f=%.1f Hz, k=%.2f $m^{-1}$'%(f_acous, k_acous))
ax.set_ylabel('Magnitude')
ax.set_xlabel(r'Horizontal wave number, $k_x$')
ax.legend()


fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(p_hk - ref), label='HK')
ax.plot(xaxis, np.abs(p_HIE1_local - ref), label='local')
ax.plot(xaxis, np.abs(p_HIE1_ext - ref), label='ext')
ax.plot(xaxis, np.abs(p_HIE1_ext1 - ref), label='ext multi')
ax.legend()
ax.grid()
ax.set_ylim(0, 1.2)
ax.set_xlim(-30, 30)
ax.set_xlabel('x distance, m')
ax.set_ylabel('Error magnitude')
ax.set_title('Surface pressure field error')

plt.show(block=False)
