import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from icepyks.surfaces import flat_surface, sin_surface
from icepyks.clumber import isospeed_integral, plane_iso_field, horizontal_array

f_acous = 1050  # Hz
c = 1500
wave_x = (-500, 500)
xspan = (wave_x[1] - wave_x[0])
plot_x = (-100, 300)
plotspan = (plot_x[1] - plot_x[0])
z_array = -0.5  # meter from waterline
g = 2 * np.abs(wave_x[0]) / 8  # plane wave resolution parameter
theta_i = 36
# periodic surface reflection
l_wave = 10
h_wave = 0.4
# integration decimation
inter_n = 6  # points per acoustic wavelength
alpha = 0  # dB/km

# Ploting specfications
plot_n = 2  # points per acoustic wavelength
plotdx = c / f_acous / plot_n
dx = c / f_acous / inter_n
z_max = -10
z_span = (z_max - h_wave)
num_z = np.ceil(np.abs(z_span / plotdx))
num_x = np.ceil(xspan / dx)
num_p = np.ceil(plotspan / plotdx)
z_plot = np.arange(num_z) / num_z * z_max - (h_wave + 0.05)
x_plot = np.arange(num_p) / num_p * plotspan + plot_x[0]
x_integrate = np.arange(num_x) / num_x * xspan + wave_x[0]

X, Z = np.meshgrid(x_plot, z_plot)

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous

ti = np.radians(theta_i)
n_theta = np.arange(20) - 10
tn = np.cos(ti) + n_theta * k_wave / k_acous

p_inc = []
for z in z_plot:
    eta = flat_surface.Flat(z_offset=z)
    flat_pw = plane_iso_field.PlaneWave(eta, g)
    p_inc.append(flat_pw.p_inc(x_plot, theta_i=ti, Hz=f_acous))
p_inc = np.array(p_inc)

sine_eta = sin_surface.Sine(l_wave, h_wave)
sine_pw = plane_iso_field.PlaneWave(sine_eta, g)
R = np.dstack((X, Z))
r_shape = R.shape
r_plot = R.reshape((r_shape[0] * r_shape[1], -1))

def measured_p(kind, r_meas, dp_dn=None, As=None, rcr_greens=None):
    """cycle through solution kinds"""
    surfer = isospeed_integral.FreqHelm(sine_pw, x_integrate, kind=kind)
    if dp_dn is None:
        if As is None:
            As = surfer.A_matricies(f_acous, alpha=alpha)
        dp_dn = surfer.dp_dn(f_acous, theta_i=ti, As=As, is_approx=True)
    if rcr_greens is None:
        rcr_greens = surfer.rcr_greens(r_meas, f_acous)
    p = surfer(r_meas, f_acous, dp_dn=dp_dn, rcr_greens=rcr_greens)
    return p, dp_dn, As, rcr_greens 

def plot_field(p_field):
    """Create a uniform plotting template"""
    fig = plt.figure()
    gs = gridspec.GridSpec(100, 100)
    ax1 = plt.subplot(gs[20:, :80])
    ax2 = plt.subplot(gs[0:20, :80])
    axcb = plt.subplot(gs[20:, 85:89])
    cm = ax1.pcolormesh(X, Z, np.abs(p_field))
    ax2.plot(x_plot, sine_eta.z(x_plot))
    ax2.set_xticklabels('')
    fig.colorbar(cm, cax=axcb)
    plt.show(block=False)

#plot_field(p_inc)

p_reflect, dp_dn, As, plot_g = measured_p(2, r_plot)
p_reflect = p_reflect.reshape(r_shape[:2])
#plot_field(p_reflect)

# do beamforming with result
z_array = -10
dx_array = l_acous / 2
array_bounds = (wave_x[0] - 100, np.max(wave_x) + 100)
x_array = np.arange(array_bounds[0], array_bounds[1], dx_array)
x_offset = np.abs(z_array) / np.tan(ti)  # the location hits (0, 0)
r_array = np.array((x_array + x_offset, np.full_like(x_array, z_array))).T

# Incident acoustic field
eta = flat_surface.Flat(z_offset=z_array)
flat_pw = plane_iso_field.PlaneWave(eta, g)
p_array_inc = flat_pw.p_inc(x_array - x_offset, theta_i=ti, Hz=f_acous)

p_array, _, _, g_rcr = measured_p(2, r_array, dp_dn=dp_dn)
p_inc_FT, k_inc_axis = horizontal_array.beamform(p_array_inc, dx_array)
p_FT, k_axis = horizontal_array.beamform(p_array, dx_array)

p_max = np.max(np.abs(p_inc_FT))

ri = horizontal_array.R_coeff(p_FT, k_axis, k_acous)
rs = np.abs(p_FT[ri]) / p_max

kz = np.sqrt(k_acous ** 2 - k_axis[ri] ** 2)
k_scale = kz / (k_acous * np.sin(ti))

r_energy = np.sum(np.abs(rs) ** 2 * k_scale)

fig = plt.figure()
gs = gridspec.GridSpec(100, 100)
ax = plt.subplot(gs[:, 8:])
ax.plot(x_array, np.abs(p_array_inc), label=r'$\theta_i$ = %i'%theta_i)
ax.plot(x_array, np.abs(p_array))
ax.set_title('pressure at z=-10 m')
ax.set_xlabel('x, m')
ax.set_ylabel('$|p|$')
ax.set_xticks([-1000, 0, 1000])
ax.set_xticklabels([-1000, '', 1000])
ax.set_ylim(-0.1, 2)
ax.set_yticks([0, 0.5, 1, 1.5])
ax.xaxis.set_label_coords(0.5, -0.05)
ax.legend(loc=2)
ax.grid()
#ax.set_yticklabels([0, '', 1])

plt.show(block=False)

fig = plt.figure()
gs = gridspec.GridSpec(100, 100)
ax = plt.subplot(gs[:, 8:])
ax.plot(k_inc_axis, np.abs(p_inc_FT) / p_max, label='inc')
ax.plot(k_axis, np.abs(p_FT) / p_max, label='refl')
ax.set_xlim(0, k_acous + 0.1)
ax.set_title(r'$\theta_i$ = %i$^o$'%theta_i)
ax.set_ylabel(r'$|R|$')
ax.set_xlabel(r'$k_x$, m$^{-1}$')
ax.set_xticks([0, 2, 4])
ax.set_xticklabels([0, '', 4])
ax.xaxis.set_label_coords(0.5, -0.05)
ax.grid()
ax.legend(loc=2)

plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
p_reflect, dp_dn = measured_p('HK')
p_reflect = p_reflect.reshape(r_shape[:2])
plot_field(p_reflect)
