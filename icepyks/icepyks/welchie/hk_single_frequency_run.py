import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from icepyks.surfaces import flat_surface, sin_surface
from icepyks.welchie import plane_iso_field, horizontal_array
from icepyks import surface_integral

f_acous = 1050  # Hz
c = 1500

wave_x = (-1000, 1000)
xspan = (wave_x[1] - wave_x[0])
plot_x = (-100, 300)

z_array = -0.5  # meter from waterline
g = 2 * np.abs(wave_x[0]) / 5  # plane wave resolution parameter
# periodic surface reflection
l_wave = 40
h_wave = 2
# integration decimation

inter_n = 6  # points per acoustic wavelength
alpha = 0  # dB/km
z_array = -10

cos_phase = np.pi / 2  # surface phase
incident_angles = np.radians(90 - np.arange(89))

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous

# integration specfications
dx = c / f_acous / inter_n
num_x = np.ceil(xspan / dx)
xaxis = np.arange(num_x) / num_x * xspan + wave_x[0]

# do beamforming with result
dx_array = l_acous / 2
array_bounds = (wave_x[0] - 100, np.max(wave_x) + 100)
x_array = np.arange(array_bounds[0], array_bounds[1], dx_array)
r_array = np.array((x_array, np.full_like(x_array, z_array))).T

# incident energy
eta = flat_surface.Flat(z_offset=z_array)
flat_pw = plane_iso_field.FinitePlaneWave(eta, g)

# reflected energy
sine_eta = sin_surface.Sine(l_wave, h_wave, phase=cos_phase)
sine_pw = plane_iso_field.FinitePlaneWave(sine_eta, g)
surfer = surface_integral.FSurfaceIntegral(sine_pw, xaxis, kind='HK')
#As = surfer.A_matricies(f_acous, alpha=alpha)
rcr_greens = surfer.rcr_greens(r_array, f_acous)

ttest = incident_angles[34]
psi = sine_pw.psi_inc(f_acous, xaxis, ttest, is_approx=True)

# compute HK reflection coefficents
rnorm = sine_pw.rnorm(f_acous, ttest, xaxis)
kx, r_vec = sine_pw.rs(f_acous, xaxis, 2 * psi)

r_vec /= np.abs(rnorm)

1/0
p_refl = []
p_inc = []
for ti in incident_angles:
    psi = surfer.psi_inc(f_acous, theta_i=ti, is_approx=True)
    p_refl.append(surfer(r_array, f_acous, psi=psi,
                          rcr_greens=rcr_greens))
    p_inc.append(flat_pw.p_inc(x_array, theta_i=ti, Hz=f_acous))
p_refl = np.array(p_refl)
p_inc = np.array(p_inc)

p_FT, k_axis = horizontal_array.beamform(p_refl, dx_array)
p_inc_FT, k_inc_axis = horizontal_array.beamform(p_inc, dx_array)

# Incident acoustic field
p_max = np.max(np.abs(p_inc_FT), axis=-1)

refl_coeff = []
for pr, ti, pm in zip(p_FT, incident_angles, p_max):
    r_i = horizontal_array.R_coeff(pr, k_axis, k_acous)
    r = pr[r_i] / pm
    r_0 = np.argmin(np.abs(k_axis[r_i] - k_acous * np.cos(ti)))
    r_index = np.arange(np.sum(r_i)) - r_0
    refl_coeff.append((r_index, np.array((k_axis[r_i], r))))

# Energy conservation calculations
energy = []
for ri, rs in refl_coeff:
    k_z = np.sqrt(k_acous ** 2 - rs[0, :] ** 2)
    k_z0 = k_z[ri == 0]
    energy.append(np.sum(np.abs(rs[1, :]) ** 2 * k_z / k_z0))

energy = np.array(energy)

# plot a few reflection coefficents
plot_i = np.array([-3, -2, -1, 0, 1, 2, 3])
plot_r = []
for ri, rs in refl_coeff:
    ip = np.in1d(ri, plot_i)
    refls = np.zeros(plot_i.shape)
    refls[: np.sum(ip)] = np.abs(rs[1, ip])
    plot_r.append(refls)
plot_r = np.array(plot_r)

fig, ax = plt.subplots()
for pr, ip in zip(plot_r.T, plot_i):
    label = 'R%i'%ip
    ax.plot(np.degrees(incident_angles), pr, label=label)

ax.set_title('HK Reflection coefficents with incident angle')
ax.set_xlabel(r'$\theta_i$, deg')
ax.set_ylabel('$|R|$')
ax.set_ylim(-.1, 1.3)
ax.legend(loc=1)
ax.grid()

fig, ax = plt.subplots()
ax.plot(np.degrees(incident_angles), energy)
ax.set_ylim(.8, 1.1)
ax.set_xlabel('Incident angle, degrees')
ax.set_ylabel(r'Energy conservered, \%')
ax.set_title('HK energy conservation')

plt.show(block=False)
