import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from icepyks.surfaces import flat_surface, sin_surface
from icepyks.clumber import isospeed_integral, plane_iso_field,\
                            horizontal_array

f_acous = 1050  # Hz
c = 1500
#wave_x = (-1500, 1500) # needed for really shallow grazing angles
wave_x = (-1000, 1000)
xspan = (wave_x[1] - wave_x[0])
plot_x = (-100, 300)
plotspan = (plot_x[1] - plot_x[0])
z_array = -0.5  # meter from waterline
g = 2 * np.abs(wave_x[0]) / 5  # plane wave resolution parameter
# periodic surface reflection
l_wave = 10
h_wave = 0.4
# integration decimation
inter_n = 6  # points per acoustic wavelength
alpha = 0  # dB/km
z_array = -10
#sine_phase = np.pi  # surface phase
sine_phase = 0.  # surface phase
#incident_angles = [90, 45, 3]
#incident_angles = np.radians(np.arange(3, 10))[::-1]
incident_angles = np.radians(90 - np.arange(80))

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous

# integration specfications
dx = c / f_acous / inter_n
num_x = np.ceil(xspan / dx)
x_integrate = np.arange(num_x) / num_x * xspan + wave_x[0]

# do beamforming with result
dx_array = l_acous / 2
array_bounds = (wave_x[0] - 100, np.max(wave_x) + 100)
x_array = np.arange(array_bounds[0], array_bounds[1], dx_array)
r_array = np.array((x_array, np.full_like(x_array, z_array))).T

# incident energy
eta = flat_surface.Flat(z_offset=z_array)
flat_pw = plane_iso_field.PlaneWave(eta, g)

# reflected energy
sine_eta = sin_surface.Sine(l_wave, h_wave, phase=sine_phase)
sine_pw = plane_iso_field.PlaneWave(sine_eta, g)
surfer = isospeed_integral.FreqHelm(sine_pw, x_integrate, kind=2)
As = surfer.A_matricies(f_acous, alpha=alpha)
rcr_greens = surfer.rcr_greens(r_array, f_acous)

p_refl = []
p_inc = []
for ti in incident_angles:
    dp_dn = surfer.dp_dn(f_acous, theta_i=ti, As=As, is_approx=True)
    p_refl.append(surfer(r_array, f_acous, dp_dn=dp_dn,
                          rcr_greens=rcr_greens))
    p_inc.append(flat_pw.p_inc(x_array, theta_i=ti, Hz=f_acous))
p_refl = np.array(p_refl)
p_inc = np.array(p_inc)

p_FT, k_axis = horizontal_array.beamform(p_refl, dx_array)
p_inc_FT, k_inc_axis = horizontal_array.beamform(p_inc, dx_array)

# Incident acoustic field
p_max = np.max(np.abs(p_inc_FT), axis=-1)

refl_coeff = []
for pr, ti, pm in zip(p_FT, incident_angles, p_max):
    r_i = horizontal_array.R_coeff(pr, k_axis, k_acous)
    r = pr[r_i] / pm
    r_0 = np.argmin(np.abs(k_axis[r_i] - k_acous * np.cos(ti)))
    r_index = np.arange(np.sum(r_i)) - r_0
    refl_coeff.append((r_index, np.array((k_axis[r_i], r))))

# Energy conservation calculations
energy = []
for ri, rs in refl_coeff:
    k_z = np.sqrt(k_acous ** 2 - rs[0, :] ** 2)
    k_z0 = k_z[ri == 0]
    energy.append(np.sum(np.abs(rs[1, :]) ** 2 * k_z / k_z0))

energy = np.array(energy)

#plot_i = np.array([40, 65])
#fig, ax = plt.subplots()
#ax.plot(x_array, np.abs(p_inc[plot_i, :]).T)
#ax.plot(x_array, np.abs(p_refl[plot_i, :]).T)

#fig, ax = plt.subplots()
#for ip in plot_i:
    #label = r'$\theta_i$ = %i $^o$'%np.degrees(incident_angles[ip])
    #ax.plot(k_axis, np.abs(p_FT[ip, :]).T / p_max[ip], label=label)
#ax.set_prop_cycle(None)
#for ip in plot_i:
    #xp = np.cos(incident_angles[ip]) * k_acous
    #ax.plot((xp, xp), (-100, -.05), '--')
#ax.set_xlim(-k_acous, k_acous)
#ax.set_ylim(-0.2, 1.2)
#ax.set_title('Magnitude of reflection coefficents')
#ax.set_xlabel('$k_x$, m$^{-1}$')
#ax.set_ylabel('$|R|$')
#ax.set_yticks([0, 0.5, 1])
#ax.set_yticklabels([0, '', 1])
#ax.set_xticks([-2.5, 0, 2.5])
#ax.set_xticklabels([-2.5, '', 2.5])
#ax.xaxis.set_label_coords(0.5, -0.05)
#ax.yaxis.set_label_coords(-0.03, 0.5)
#ax.grid()
#ax.legend(loc=2)

# plot a few reflection coefficents
plot_i = np.array([-3, -2, -1, 0, 1, 2, 3])
plot_r = []
for ri, rs in refl_coeff:
    ip = np.in1d(ri, plot_i)
    refls = np.zeros(plot_i.shape)
    refls[: np.sum(ip)] = np.abs(rs[1, ip])
    plot_r.append(refls)
plot_r = np.array(plot_r)

fig, ax = plt.subplots()
for pr, ip in zip(plot_r.T, plot_i):
    label = 'R%i'%ip
    ax.plot(np.degrees(incident_angles), pr, label=label)

ax.set_title('HIE Reflection coefficents with incident angle')
ax.set_xlabel(r'$\theta_i$, deg')
ax.set_ylabel('$|R|$')
ax.set_ylim(.8, 1.1)
ax.legend(loc=1)
ax.grid()

fig, ax = plt.subplots()
ax.plot(np.degrees(incident_angles), energy)
ax.set_ylim(.8, 1.1)
ax.set_xlabel('Incident angle, degrees')
ax.set_ylabel(r'Energy conservered, \%')
ax.set_title('HIE energy conservation')

plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
p_reflect, dp_dn = measured_p('HK')
p_reflect = p_reflect.reshape(r_shape[:2])
plot_field(p_reflect)
