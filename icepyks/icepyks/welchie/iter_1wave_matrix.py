import numpy as np
import matplotlib.pyplot as plt
from icepyks.surfaces import sin_surface, flat_surface
from icepyks.welchie import plane_iso_field
from icepyks.poodle import plane_wave_rs
from icepyks.welchie import horizontal_array
from icepyks import surface_integral
from scipy.linalg import solve
from scipy.special import hankel1

f_acous = 1050  # Hz
c = 1500

# periodic surface reflection
l_wave = 40
h_wave = 2.
inter_n = 6  # points per acoustic wavelength

# make surface an integer number of waves long
num_waves = 15 * 3
dx = c / f_acous / inter_n
ppw = l_wave // dx + 1
xax_onewave = np.arange(ppw) / ppw * l_wave
dx = (xax_onewave[-1] - xax_onewave[0]) / (xax_onewave.size - 1)

x_starts = (np.arange(num_waves) - num_waves // 2) * l_wave - l_wave / 2
xaxis = np.reshape(x_starts[:, None] + xax_onewave[None, :], -1)
# add the last index
xaxis = np.hstack([xaxis, xaxis[-1] + dx])

total_x = num_waves * l_wave
wave_x = (-total_x / 2 - l_wave / 2, total_x / 2 + l_wave / 2)
g = float(np.abs(np.diff(wave_x))) / 5  # plane wave resolution parameter

# integration decimation
alpha = 0  # dB/km
cos_ier = plane_wave_rs.CosineReflectionCoefficents(h_wave,
                                                    2 * np.pi / l_wave,
                                                    attn=alpha)

cos_phase = np.pi / 2  # surface phase
ti_inc = np.radians(10.)

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous
kx_inc = np.cos(ti_inc) * k_acous

# setup problem
cos_eta = sin_surface.Sine(l_wave, h_wave, phase=cos_phase)
cos_win = plane_iso_field.FinitePlaneWave(cos_eta, g, xaxis, attn=0)
cos_win(f_acous, ti_inc)

# setup beamforming for the result
z_array = -10
dx_array = l_acous / 2
x_array = np.arange(np.min(xaxis) + 100, np.max(xaxis) - 100, dx_array)
r_array = np.array((x_array, np.full_like(x_array, z_array))).T

# common normalization term
flat_eta = flat_surface.Flat(z_offset=z_array)
flat_pw = plane_iso_field.FinitePlaneWave(flat_eta, g, x_array)
flat_pw(f_acous, ti_inc)
p_inc = flat_pw.p_inc(f_acous, ti_inc)
p_inc_FT, k_inc_axis = horizontal_array.beamform(p_inc, dx_array)
bmax = np.max(np.abs(p_inc_FT), axis=-1)

# Windowed HIE
surfer_HIE2 = surface_integral.FSurfaceIntegral(cos_win, xaxis, kind=2)
rcr_greens = surfer_HIE2.rcr_greens(r_array)

# Iterative HIE
psi_HIE2 = surfer_HIE2.psi()
A = surfer_HIE2.A_matricies(2)

# one wave iteration test
i1wave = np.arange(xax_onewave.size)
l1wave = xax_onewave.size

# construct an index array of connected points
iconn = np.zeros(A.shape, dtype=np.bool_)

for j in range(num_waves):
    xcurr = i1wave + j * l1wave
    xcurr = np.hstack([xcurr, xcurr[-1] + 1])
    iconn[np.ix_(xcurr, xcurr)] = True
    # add diagonal indicies
    if j < (num_waves - 1):
        xdiag = i1wave + (j + 1) * l1wave
        iconn[np.ix_(xdiag, xcurr)] = True
        iconn[np.ix_(xdiag, xcurr)] = np.triu(iconn[np.ix_(xdiag, xcurr)])
        iconn[np.ix_(xcurr, xdiag)] = np.triu(iconn[np.ix_(xdiag, xcurr)]).T

Aconn = np.zeros_like(A)
Aconn[iconn] = A[iconn]

#add a window to A matrix
theta = np.arctan2(i1wave[None, :], i1wave[:, None])

rtot = np.zeros_like(theta)
rtot[theta <= np.pi / 4] = 1 / np.cos(theta[theta <= np.pi / 4])
rtot[theta > np.pi / 4] = 1 / np.sin(theta[theta > np.pi / 4])

rpos = np.sqrt(i1wave[None, :] ** 2 + i1wave[:, None] ** 2)
rrat = rpos / rtot

#expwin = np.exp(-rrat * 3 / np.max(i1wave))[:, ::-1]
expwin = np.exp((rrat - np.max(i1wave)) * 3 / np.max(i1wave))[::-1, :]
#expwin = np.exp((rrat - np.max(i1wave)) * 3 / np.max(i1wave))


plotconn = np.array(iconn.copy(), dtype=np.float_)

for j in range(num_waves - 1):
    xcurr = i1wave + j * l1wave
    xdiag = i1wave + (j + 1) * l1wave
    Aconn[np.ix_(xdiag, xcurr)] *= np.triu(expwin)
    Aconn[np.ix_(xcurr, xdiag)] *= np.triu(expwin)[::-1, ::-1]
    plotconn[np.ix_(xdiag, xcurr)] *= np.triu(expwin)
    plotconn[np.ix_(xcurr, xdiag)] *= np.triu(expwin)[::-1, ::-1]

# plot connectivity matrix
ploti = np.hstack([i1wave + (num_waves // 2 - 1) * l1wave,
                   i1wave + num_waves // 2 * l1wave,
                   i1wave + (num_waves // 2 + 1) * l1wave,
                   i1wave + (num_waves // 2 + 2) * l1wave])

PI1, PI2 = np.meshgrid(ploti, ploti)

fig, ax = plt.subplots()
ax.pcolormesh(PI1, PI2, np.abs(Aconn[np.ix_(ploti, ploti)].T))

plt.show(block=False)

fig, ax = plt.subplots()
ax.pcolormesh(PI1, PI2, plotconn[np.ix_(ploti, ploti)].T, cmap=plt.cm.binary)
ax.set_xlabel('index, n')
ax.set_ylabel('index, m')
ax.set_title('connectivity matrix')
plt.show(block=False)

ier_1mat = [2 * surfer_HIE2.psi_inc()]

for i in range(2):
    ier_1mat.append(np.zeros_like(ier_1mat[0]))
    iforward = Aconn @ ier_1mat[-2]
    iforward *= surfer_HIE2.gamma * surfer_HIE2.dx / 2j
    ier_1mat[-1] = ier_1mat[0] - iforward

ier_1mat = np.array(ier_1mat)

ref = psi_HIE2 * np.exp(-1j * kx_inc * xaxis)
p_hk = ier_1mat[0, :] * np.exp(-1j * kx_inc * xaxis)
p_HIE1_mat = ier_1mat[1, :] * np.exp(-1j * kx_inc * xaxis)

# reflection coefficents
p_HIE2 = surfer_HIE2(r_array, f_acous, psi=psi_HIE2, rcr_greens=rcr_greens)
r_HIE2, k_axis = horizontal_array.beamform(p_HIE2, dx_array)

p_HK = surfer_HIE2(r_array, f_acous,
                   psi=ier_1mat[0, :], rcr_greens=rcr_greens)
r_HK, _ = horizontal_array.beamform(p_HK, dx_array)

p_Imat = surfer_HIE2(r_array, f_acous,
                     psi=ier_1mat[-1, :], rcr_greens=rcr_greens)
r_Imat, _ = horizontal_array.beamform(p_Imat, dx_array)

fig, ax = plt.subplots()
ax.plot(k_axis, np.abs(r_HK - r_HIE2) / bmax, label='HK')
ax.plot(k_axis, np.abs(r_Imat - r_HIE2) / bmax, label='1 wave win')
ax.plot([k_acous, k_acous], [-100, 100], ':', color='0.4')
ax.set_xlim(2, 6)
ax.set_ylim(0, 1)
ax.set_title(r'HKA Reflection coefficents for $\theta_{inc}$=%.1f$^o$'%
             np.degrees(ti_inc) + '\n' +
             'f=%.1f Hz, k=%.2f $m^{-1}$'%(f_acous, k_acous))
ax.set_ylabel('Magnitude')
ax.set_xlabel(r'Horizontal wave number, $k_x$')
ax.legend()

fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(p_hk - ref), label='HK')
ax.plot(xaxis, np.abs(p_HIE1_mat - ref), label='1 wave win')
ax.legend()
ax.grid()
ax.set_ylim(0, 1.2)
ax.set_xlim(-30, 30)
ax.set_xlabel('x distance, m')
ax.set_ylabel('Error magnitude')
ax.set_title('Surface pressure field error')

plt.show(block=False)
