"""
====================================
Welch Terrier :mod:`icepyks.welchie`
====================================

.. currentmodule: icepyks.welchie

Helmholtz integral equation for plane wave sources

Solves the full wave scattering solution with the Helmholtz integral equation
by windowing the plane wave incident field.

.. autosummary::
   :toctree: generated/

   plane_iso_field

"""
