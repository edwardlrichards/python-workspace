import numpy as np
import matplotlib.pyplot as plt
from icepyks.surfaces import sin_surface, flat_surface
from icepyks.welchie import plane_iso_field
from icepyks.poodle import plane_wave_rs
from icepyks.welchie import horizontal_array
from icepyks import surface_integral
from scipy.linalg import solve
from scipy.special import hankel1

f_acous = 1050  # Hz
c = 1500

# periodic surface reflection
l_wave = 40
h_wave = 2.
inter_n = 6  # points per acoustic wavelength

# make surface an integer number of waves long
num_waves = 15 * 3
dx = c / f_acous / inter_n
ppw = l_wave // dx + 1
xax_onewave = np.arange(ppw) / ppw * l_wave
dx = (xax_onewave[-1] - xax_onewave[0]) / (xax_onewave.size - 1)

x_starts = (np.arange(num_waves) - num_waves // 2) * l_wave - l_wave / 2
xaxis = np.reshape(x_starts[:, None] + xax_onewave[None, :], -1)
# add the last index
xaxis = np.hstack([xaxis, xaxis[-1] + dx])

total_x = num_waves * l_wave
wave_x = (-total_x / 2 - l_wave / 2, total_x / 2 + l_wave / 2)
g = float(np.abs(np.diff(wave_x))) / 5  # plane wave resolution parameter

# integration decimation
alpha = 0  # dB/km
cos_ier = plane_wave_rs.CosineReflectionCoefficents(h_wave,
                                                    2 * np.pi / l_wave,
                                                    attn=alpha)

cos_phase = np.pi / 2  # surface phase
ti_inc = np.radians(45.)

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous
kx_inc = np.cos(ti_inc) * k_acous

# setup problem
cos_eta = sin_surface.Sine(l_wave, h_wave, phase=cos_phase)
cos_win = plane_iso_field.FinitePlaneWave(cos_eta, g, xaxis, attn=0)
cos_win(f_acous, ti_inc)

# setup beamforming for the result
z_array = -10
dx_array = l_acous / 2
x_array = np.arange(np.min(xaxis) + 100, np.max(xaxis) - 100, dx_array)
r_array = np.array((x_array, np.full_like(x_array, z_array))).T

# common normalization term
flat_eta = flat_surface.Flat(z_offset=z_array)
flat_pw = plane_iso_field.FinitePlaneWave(flat_eta, g, x_array)
flat_pw(f_acous, ti_inc)
p_inc = flat_pw.p_inc(f_acous, ti_inc)
p_inc_FT, k_inc_axis = horizontal_array.beamform(p_inc, dx_array)
bmax = np.max(np.abs(p_inc_FT), axis=-1)

# Windowed HIE
surfer_HIE2 = surface_integral.FSurfaceIntegral(cos_win, xaxis, kind=2)
rcr_greens = surfer_HIE2.rcr_greens(r_array)

# Iterative HIE
psi_HIE2 = surfer_HIE2.psi()
A = surfer_HIE2.A_matricies(2)

# one wave iteration test
i1wave = np.arange(2 * xax_onewave.size)
l1wave = i1wave.size

# connectivity matrix is symetric about main diagonal
xstart = np.arange(num_waves) * xax_onewave.size
allconni = xstart[None, :] + i1wave[:, None]

# always populate main diagonal
iconn = np.zeros(A.shape, dtype=np.float_)
iconn[np.diag_indices_from(iconn)] = 1
rows, cols = np.diag_indices_from(iconn)

for k in i1wave[1: ]:
    ktmp = np.zeros(A.shape[0] - k, dtype=np.float_)
    imat = np.ravel(allconni[k:, :])
    ktmp[imat[imat < A.shape[0]] - k] = 1.
    # fill in diagonal windowed values
    win_length = np.diff(np.where(np.diff(ktmp))[0])
    if np.all(ktmp == 1):
        iconn[rows[:-k], cols[k:]] = ktmp
        iconn[rows[k:], cols[:-k]] = ktmp
        continue
    fz = np.where(ktmp == 0)[0][0]
    min_dist = [np.zeros(fz)]
    for wl in win_length:
        min_dist.append(np.abs(wl // 2 - np.abs(np.arange(wl) - wl // 2)))
    min_dist.append(np.arange(ktmp.size - np.sum(win_length) - fz))
    min_dist = np.hstack(min_dist)
    win = np.exp(-min_dist / xax_onewave.size * 4)
    ktmp[ktmp == 0] = win[ktmp == 0]
    iconn[rows[:-k], cols[k:]] = ktmp
    iconn[rows[k:], cols[:-k]] = ktmp

Aconn = A * iconn
# plot connectivity matrix
ploti = np.arange(A.shape[0] // 2 - 600, A.shape[0] // 2 + 600)
PI1, PI2 = np.meshgrid(ploti, ploti)

fig, ax = plt.subplots()
ax.pcolormesh(PI1, PI2, iconn[np.ix_(ploti, ploti)].T, cmap=plt.cm.binary)
ax.set_xlabel('index, n')
ax.set_ylabel('index, m')
ax.set_title('connectivity matrix')
plt.show(block=False)

fig, ax = plt.subplots()
ax.pcolormesh(PI1, PI2, np.abs(Aconn[np.ix_(ploti, ploti)].T))

plt.show(block=False)

ier_1mat = [2 * surfer_HIE2.psi_inc()]

for i in range(6):
    ier_1mat.append(np.zeros_like(ier_1mat[0]))
    iforward = Aconn @ ier_1mat[-2]
    iforward *= surfer_HIE2.gamma * surfer_HIE2.dx / 2j
    ier_1mat[-1] = ier_1mat[0] - iforward

ier_1mat = np.array(ier_1mat)

ref = psi_HIE2 * np.exp(-1j * kx_inc * xaxis)
p_hk = ier_1mat[0, :] * np.exp(-1j * kx_inc * xaxis)

fig, ax = plt.subplots()
for i, p in enumerate(ier_1mat):
    ax.plot(xaxis, np.abs(p * np.exp(-1j * kx_inc * xaxis) - ref),
            label='iter %i'%i)
ax.legend()
ax.grid()
ax.set_ylim(0, 1.2)
ax.set_xlim(-60, 60)
ax.set_xlabel('x distance, m')
ax.set_ylabel('Error magnitude')
ax.set_title('Surface pressure field error\n' +
             'Three wave length connectivity matrix')

plt.show(block=False)

# reflection coefficents
p_HIE2 = surfer_HIE2(r_array, f_acous, psi=psi_HIE2, rcr_greens=rcr_greens)
r_HIE2, k_axis = horizontal_array.beamform(p_HIE2, dx_array)

p_HK = surfer_HIE2(r_array, f_acous,
                   psi=ier_1mat[0, :], rcr_greens=rcr_greens)
r_HK, _ = horizontal_array.beamform(p_HK, dx_array)

p_Imat = surfer_HIE2(r_array, f_acous,
                     psi=ier_1mat[-1, :], rcr_greens=rcr_greens)
r_Imat, _ = horizontal_array.beamform(p_Imat, dx_array)

fig, ax = plt.subplots()
ax.plot(k_axis, np.abs(r_HK - r_HIE2) / bmax, label='HK')
ax.plot(k_axis, np.abs(r_Imat - r_HIE2) / bmax, label='1 wave win')
ax.plot([k_acous, k_acous], [-100, 100], ':', color='0.4')
ax.set_xlim(2, 6)
ax.set_ylim(0, 1)
ax.set_title(r'HKA Reflection coefficents for $\theta_{inc}$=%.1f$^o$'%
             np.degrees(ti_inc) + '\n' +
             'f=%.1f Hz, k=%.2f $m^{-1}$'%(f_acous, k_acous))
ax.set_ylabel('Magnitude')
ax.set_xlabel(r'Horizontal wave number, $k_x$')
ax.legend()
plt.show(block=False)
