"""
==================================
Windowed plane wave incident field
==================================

Both approximate and exact implimentations of the plane wave window of Thosos.
Plane wave has finite spatial extent which is adaquate except at Bragg cut-off
angles.
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import hankel1
from scipy.integrate import simps

class FinitePlaneWave:
    """compute incident pressure with various windows"""
    def __init__(self, eta, g, x_axis, attn=1.):
        """one plane wave object per incident angle"""
        self.c_src = 1500.
        self.eta = eta
        self.g = g
        self.x_axis = np.array(x_axis, ndmin=1)
        self.attn = attn
        self.frequency = None
        self.theta_inc = None
        self.delta = lambda k: 1j * self.attn / 8686.\
                               / np.real(k + np.spacing(1))

    def __call__(self, frequency, theta_inc):
        """method to set plane wave parameters"""
        self.frequency = frequency
        self.theta_inc = theta_inc

    @property
    def kacous(self):
        """complex wavenumeber of medium"""
        k = 2 * np.pi * self.frequency / self.c_src
        return (1 + self.delta(k)) * k

    def p_inc(self, num_quad=1001, num_dims=2, is_approx=True):
        """surface field incident pressure"""
        p = self._p_pw(False, is_approx, num_quad)
        return p

    def psi_inc(self, num_quad=1001, num_dims=2, is_approx=True):
        """surface field incident pressure normal derivative"""
        p_grad = self._p_pw(True, is_approx, num_quad)
        dp_dn = np.add.reduce(self.eta.grad(self.x_axis)[:, [0, -1]]
                              * p_grad, axis=-1)
        return dp_dn

    def rcr_greens(self, r_rcr, num_dims=2):
        """Hankel function if 2D greens function to r_receivers"""
        r_receivers = np.array(r_rcr, ndmin=2)
        rel_z = self.eta.z(self.x_axis) - r_receivers[:, -1][:, None]
        rel_x = self.x_axis - r_receivers[:, 0][:, None]
        rel_range = np.sqrt(rel_z ** 2 + rel_x ** 2)
        gr = hankel1(0, self.kacous * rel_range)
        return gr

    def _p_pw(self, is_grad, is_approx, num_quad):
        """windowed pressure field at posions
        r_oi is a N x 2 position matrix
        """
        r_oi =  np.vstack((self.x_axis,
                           self.eta.z(self.x_axis))).T
        if not is_approx:
            p = self._exact_p(r_oi, num_quad, is_grad)
        else:
            p = self._approx_p(r_oi, is_grad)
        return p

    def _exact_p(self, r_oi, num_quad, is_grad):
        """Perform a wavenumber integration for exact helmholtz solution"""
        dtheta = 2 / (np.real(self.kacous) * self.g * np.sin(self.theta_inc))
        # create a range of arrival angles for integration
        sig_4 = 4 * dtheta / 2  # exponential at 1 ppt
        if (self.theta_inc - sig_4) < 0 or (self.theta_inc + sig_4) > np.pi:
            raise(ValueError('angle in is too close to integration boundry'))
        tbounds = self.theta_inc - sig_4, self.theta_inc + sig_4
        tr = np.arange(num_quad) / (num_quad - 1) *\
             (tbounds[1] - tbounds[0]) + tbounds[0]
        pw = self._p_plane(tr, r_oi)
        igrand = np.exp(-(tr - self.theta_inc) ** 2 / dtheta ** 2)[None, :]\
                 * pw[0]
        if is_grad:
            # need to check where to put this dimension
            igrand = 1j * np.array((pw[1] * igrand, pw[2] * igrand))
            igrand = np.rollaxis(igrand, 1)
        # integrate over arrival angle
        ivalue = simps(igrand, x=tr, axis=-1)
        scale = 1 / np.sqrt(np.pi) / dtheta
        return scale * ivalue

    def _approx_p(self, r_oi, is_grad):
        """Approximate solution to plane wave window"""
        # incident wave is upgoing (kz negative)
        cot_term = (r_oi[..., 0] - r_oi[..., 1] / np.tan(self.theta_inc)) ** 2
        cot_term /= self.g ** 2
        w_bar = 2 * cot_term - 1
        w_bar /= (np.real(self.kacous) * self.g * np.sin(self.theta_inc)) ** 2
        pw = self._p_plane(self.theta_inc, r_oi, w_bar=w_bar)
        p_inc = np.squeeze(pw[0]) * np.exp(-cot_term)
        kx = pw[1]
        kz = pw[2]
        if is_grad:
            dc_dx = 2 * r_oi[..., 0]\
                    - 2 * r_oi[..., 1] / np.tan(self.theta_inc)
            dc_dz = 2 * r_oi[..., 1] / np.tan(self.theta_inc) ** 2\
                    - 2 * r_oi[..., 0] / np.tan(self.theta_inc)
            dc_dx /= self.g ** 2
            dc_dz /= self.g ** 2
            dw_dx = (2 * dc_dx)\
                    / (self.kacous * self.g * np.sin(self.theta_inc)) ** 2
            dw_dz = (2 * dc_dz)\
                    / (self.kacous * self.g * np.sin(self.theta_inc)) ** 2
            # phase derivative term
            dp_dx = 1j * (kx + kx * w_bar + kx * r_oi[..., 0] * dw_dx
                    + kz * r_oi[..., 1] * dw_dx) - dc_dx
            dp_dz = 1j * (kz + kz * w_bar + kz * r_oi[..., 1] * dw_dz\
                    + kx * r_oi[..., 0] * dw_dz) - dc_dz
            p_inc = np.array((dp_dx, dp_dz)) * p_inc
            p_inc = p_inc.T
        return p_inc

    def _p_plane(self, t_range, r_oi, w_bar=None):
        """Handle broadcasting for integration pressure term"""
        t_range = np.array(t_range, ndmin=1)
        kx = self.kacous * np.cos(t_range)
        kz = self.kacous * np.sin(t_range)
        r_oi = np.array(r_oi, ndmin=2)
        kr = kx[:, None] * r_oi[..., 0] + kz[:, None] * r_oi[..., 1]
        if w_bar is None:
            pressure = np.exp(1j * kr).T
        else:
            pressure = np.exp(1j * kr * (1 + w_bar)).T
        return pressure, kx, kz

class InfinitePlaneWave:
    """compute incident pressure with various windows"""
    def __init__(self, eta, x_axis, attn=1.):
        """one plane wave object per incident angle"""
        self.c_src = 1500.
        self.eta = eta
        self.x_axis = np.array(x_axis, ndmin=1)
        self.attn = attn
        self.frequency = None
        self.theta_inc = None
        self.delta = lambda k: 1j * self.attn / 8686.\
                               / np.real(k + np.spacing(1))

    def __call__(self, frequency, theta_inc):
        """method to set plane wave parameters"""
        self.frequency = frequency
        self.theta_inc = theta_inc

    @property
    def kacous(self):
        """complex wavenumeber of medium"""
        k = 2 * np.pi * self.frequency / self.c_src
        return (1 + self.delta(k)) * k

    def p_inc(self):
        """surface field incident pressure"""
        kacous = np.real(self.kacous)
        kx = np.cos(self.theta_inc) * kacous
        kxy = np.array([kx, -np.sqrt(kacous ** 2 - kx ** 2)])
        r = np.array([self.x_axis, self.eta.z(self.x_axis)])
        p = np.exp(1j * np.sum(kxy[:, None] * r, axis=0))
        return p

    def psi_inc(self):
        """surface field incident pressure normal derivative"""
        kacous = np.real(self.kacous)
        kx = np.cos(self.theta_inc) * kacous
        kxy = np.array([kx, -np.sqrt(kacous ** 2 - kx ** 2)])
        r = np.array([self.x_axis, self.eta.z(self.x_axis)])
        phase = np.exp(1j * np.sum(kxy[:, None] * r, axis=0))
        gn = np.sum(self.eta.grad(self.x_axis).T * kxy[:, None], axis=0)
        psi = 1j * gn * phase
        return psi

    def rcr_greens(self, r_rcr, num_dims=2):
        """Hankel function if 2D greens function to r_receivers"""
        r_receivers = np.array(r_rcr, ndmin=2)
        rel_z = self.eta.z(self.x_axis) - r_receivers[:, -1][:, None]
        rel_x = self.x_axis - r_receivers[:, 0][:, None]
        rel_range = np.sqrt(rel_z ** 2 + rel_x ** 2)
        gr = hankel1(0, self.kacous * rel_range)
        return gr
