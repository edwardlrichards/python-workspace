"""
==============================================
Equivalence of H solutions of 1st and 2nd kind
==============================================
Recreate Eric Thorsos result in Computational Acoustics vol 2.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber import isospeed_integral, plane_iso_field
from icepyks.surfaces import sin_surface

#cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface

c0 = 1500
z_bottom = -4
# wave parameters
l_acous = 1
fc = 1500 / l_acous
lamda_surf = 4 * l_acous
wave_amp = 0.3 * l_acous
l_total = 40 * l_acous
dx = 0.1 * l_acous
theta_i = np.radians(45)

#Setup integration bounds
g = l_total / 4
num_x = np.ceil(l_total / dx)
xaxis = np.arange(num_x) * dx
xaxis -= np.mean(xaxis)

# solve for pressure field at surface
eta = sin_surface.Sine(lamda_surf, wave_amp, phase=0.)
iso_field = plane_iso_field.PlaneWave(eta, g)

# check the horizontal extent of the planewave
fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(iso_field.p_inc(xaxis, theta_i=theta_i, Hz=fc)))
ax.set_title('Amplitude of incident pressure')
ax.set_xlabel('Position, m')
ax.set_ylabel('$|p|$')
ax.grid()
plt.show(block=False)

isospeed_HK = isospeed_integral.FreqHelm(iso_field, xaxis, kind='HK')
helm_1 = isospeed_integral.FreqHelm(iso_field, xaxis, kind=1)
helm_2 = isospeed_integral.FreqHelm(iso_field, xaxis, kind=2)

dp_dn_HK = isospeed_HK.dp_dn(fc, theta_i=theta_i,)
dp_dn_1 = helm_1.dp_dn(fc, theta_i=theta_i,)
dp_dn_2 = helm_2.dp_dn(fc, theta_i=theta_i,)

fig, ax = plt.subplots()
#ax.plot(xaxis, np.real(dp_dn_HK), label='HK')
ax.plot(xaxis / l_acous, np.real(dp_dn_1) * l_acous, label='first')
ax.plot(xaxis / l_acous, np.real(dp_dn_2) * l_acous, label='second')
ax.grid()
#ax.legend()

plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(e1), label='First')
ax.plot(xaxis, np.abs(e2), label='Second')
ax.grid()
ax.legend()


plt.show(block=False)
# Beamform for reflection coefficents
