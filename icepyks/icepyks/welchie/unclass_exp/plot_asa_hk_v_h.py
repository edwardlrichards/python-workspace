"""
================================
Equivalence of H and HK solution
================================

High frequency pulse reflected from a flat surface, short range
and fc = 15 kHz.
"""

import numpy as np
import xarray as xr
from scipy.special import hankel1
#import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp
from icepyks.clumber import surface_field, surface_integral,\
                            iso_rg, f_synthesis
from icepyks.clumber.surfaces import sin_surface
from concurrent.futures import ProcessPoolExecutor

#cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface

c0 = 1500
z_source = -2.723
r_receiver = np.array((16.542, 0, -1.906))
z_bottom = -4
# wave parameters
lamda_surf = 9.435
wave_amp = 0.422 / 2
num_phase = 120
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (np.pi / 2)
z_off = wave_amp + 0.01

ind_a = 10

# put in isospeed layer
spec_r = np.sqrt((z_source + r_receiver[2]) ** 2 + r_receiver[0] ** 2)
# offset for wave height
r_receiver[2] = r_receiver[2] + z_off
z_source += z_off

#signal parameter
fc = 15e3
f_min = 5e3
f_max = 25e3
freq = np.arange(f_min, f_max)

#Setup integration bounds
x_bounds = (-2, 18.5)
dx = c0 / f_max / 5
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
fs = f_max * 3
spec_tau = spec_r / c0

# solve for pressure field at surface
ray_gun = iso_rg.Isospeed(c0)
proper = surface_field.SurfaceField(z_source, r_receiver, z_bottom,
                                    range_buffer=6)
proper.to_top(ray_gun)
p = wave_phase[ind_a]
eta = sin_surface.Sine(lamda_surf, wave_amp, phase=p, z_offset=z_off)
proper.to_wave(eta)
proper('wave')

hk_freq = surface_integral.FreqHK(proper, xaxis, np.ones_like(xaxis),
                                  num_dims=2)
helm_hk = surface_integral.FreqHelm(proper, xaxis, kind='HK')
helm_freq_1 = surface_integral.FreqHelm(proper, xaxis, kind=1)
helm_freq_2 = surface_integral.FreqHelm(proper, xaxis, kind=2)

def one_freq(f):
    """Result for one frequency"""
    dx = c0 / f / 5
    xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
    hk_freq.xaxis = xaxis
    helm_hk.xaxis = xaxis
    helm_freq_1.xaxis = xaxis
    helm_freq_2.xaxis = xaxis

    HK_result = hk_freq(f)
    #Helm_HK.append(helm_hk(f))
    #Helm1_result = helm_freq_1(f)
    Helm2_result = helm_freq_2(f)
    if f % 1000 == 0:
        print('run number %f'%f)
    return np.array((HK_result, Helm2_result))

with ProcessPoolExecutor(max_workers=3) as executor:
    result = list(executor.map(one_freq, freq))
result = np.array(result)

np.savez('asa_short_frequency_%i'%ind_a, signal=result, signal_f=freq)
1/0
for f in freq:
    # compute a single frequency value
    dx = c0 / f / 5
    xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
    hk_freq.xaxis = xaxis
    helm_hk.xaxis = xaxis
    helm_freq_1.xaxis = xaxis
    helm_freq_2.xaxis = xaxis

    HK_result.append(hk_freq(f))
    Helm_HK.append(helm_hk(f))
    Helm1_result.append(helm_freq_1(f))
    Helm2_result.append(helm_freq_2(f))

fig, ax = plt.subplots()
ax.plot(freq[: len(HK_result)], np.abs(HK_result), '.')
ax.plot(freq[: len(Helm_HK)], np.abs(Helm_HK), '.')
ax.plot(freq[: len(Helm1_result)], np.abs(Helm1_result), '.')
ax.plot(freq[: len(Helm2_result)], np.abs(Helm2_result), '.')
k_freq = 2 * np.pi * np.array(freq) / c0
ax.plot(freq, np.sqrt(2 / np.pi / k_freq / spec_r))
plt.show(block=False)
