import numpy as np
import matplotlib.pyplot as plt
from icepyks.surfaces import flat_surface, sin_surface
from icepyks import surface_integral
from icepyks.porty import plane_iso_field

f_acous = 1050  # Hz
c = 1500
array_span = (-50, 50)
wave_x = (-60, 60)
z_array = -0.5  # meter from waterline
g = 20  # plane wave resolution parameter
theta_i = 50
# periodic surface reflection
#h_wave = 0.32
#l_wave = 0.95
#l_wave = 9.435
l_wave = 10
h_wave = 0.422 / 2
# integration decimation
inter_n = 8  # points per acoustic wavelength

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous
ti = np.radians(theta_i)
n_theta = np.arange(20) - 10
tn = np.cos(ti) + n_theta * k_wave / k_acous
# integration axis
inter_dx = l_acous / inter_n

# find evanescent wave with 2 e-folding depths
k_z_cut = 2 * np.pi / np.abs(z_array)
k_x_cut = np.sqrt(float(np.abs(k_acous ** 2 - k_z_cut ** 2)))
dx_array = np.pi / k_x_cut

# create array
num_x = np.ceil((array_span[1] - array_span[0]) / dx_array)
x_array = np.arange(num_x) * dx_array + array_span[0]
r_array = np.array((x_array, np.full_like(x_array, z_array)))

# integration axis
num_x = int((wave_x[1] - wave_x[0]) / inter_dx)
inter_x = np.arange(num_x) * inter_dx + np.min(wave_x)

eta = flat_surface.Flat()
flat_pw = plane_iso_field.PlaneWave(eta, g)

p_inc_1 = flat_pw.p_inc(x_array, theta_i=ti, Hz=f_acous)

def beamform(p, dx):
    """simple beamformer result"""
    NFFT = 2 ** int(np.ceil(np.log2(p.size)) + 1)
    k_max = 2 * np.pi / dx
    k_axis = np.arange(NFFT) / NFFT * k_max
    k_axis -= k_max / 2
    P = np.fft.fftshift(np.fft.fft(p, NFFT))
    return P, k_axis

fig, ax = plt.subplots()
ax.plot(x_array, np.abs(p_inc_1))
ax.set_title('Magnitude of incident pressure')
ax.set_xlabel('x, m')
ax.set_ylabel('$|p(x)|$')
ax.set_xticks([-50, 0, 50])
ax.set_xticklabels([-50, '', 50])
ax.set_yticks([-0, 0.5, 1])
ax.set_yticklabels([-0, '', 1])
ax.xaxis.set_label_coords(0.5, -0.05)
ax.yaxis.set_label_coords(-0.03, 0.5)
ax.grid()
ax.set_ylim(-0.1, 1.1)

P_FT1, k_axis = beamform(p_inc_1, dx_array)
max_p = np.max(np.abs(P_FT1))

fig, ax = plt.subplots()
ax.plot(k_axis, np.abs(P_FT1) / max_p)
k_x = np.cos(ti) * k_acous
ax.plot([k_acous, k_acous], [-1e6, 1e6], 'k')
ax.plot([-k_acous, -k_acous], [-1e6, 1e6], 'k')
ax.set_title('Magnitude of incident pressure')
ax.set_xlabel('$k_x$, $m^{-1}$')
ax.set_ylabel('$|p(k_x)| / |p_{inc}|$')
ax.set_ylim(-0.05, 1.05)
ax.set_xlim(1, 7)
ax.set_xticks([2, 4, 6])
ax.set_xticklabels([2, '', 6])
ax.set_yticks([0, 0.5, 1])
ax.set_yticklabels([0, '', 1])
ax.annotate('$k_{acoustic}$', xy=(k_acous, 0.4), xytext=(k_acous + 0.7, 0.55),
            arrowprops=dict(facecolor='black', shrink=0.05),)
ax.xaxis.set_label_coords(0.5, -0.05)
ax.yaxis.set_label_coords(-0.03, 0.5)
ax.grid()
plt.show(block=False)

sine_eta = sin_surface.Sine(l_wave, h_wave)
sine_pw = plane_iso_field.PlaneWave(sine_eta, g)
r_array = np.array((x_array, np.full_like(x_array, z_array))).T

fig, ax = plt.subplots()
ax.plot(inter_x, sine_eta.z(inter_x))
#for r in r_array:
ax.plot(r_array[:, 0], r_array[:, 1], 'g.')
ax.annotate('wave position', xy=(-25, 0.2),
            xytext=(-15, 0.35),
            arrowprops=dict(facecolor='black', shrink=0.05),)

ax.annotate('array position', xy=(-20, z_array),
            xytext=(-10, -0.4),
            arrowprops=dict(facecolor='black', shrink=0.05),)
ax.set_ylim(-0.75, .5)
ax.set_yticks([-.5, 0, .5])
ax.set_yticklabels([-.5, '', .5])
ax.set_xlim(-60, 60)
ax.set_xticks([-50, 0, 50])
ax.set_xticklabels([-50, '', 50])
ax.set_xlabel('x, m')
ax.set_ylabel('z, m')
ax.set_title('Simulation geometry')
ax.xaxis.set_label_coords(0.5, -0.05)
ax.yaxis.set_label_coords(-0.03, 0.5)
plt.show(block=False)

def kind_comp(kind):
    """cycle through solution kinds"""
    surfer = surface_integral.FreqHelm(sine_pw, inter_x, kind=kind)
    dp_dn = surfer.dp_dn(f_acous, theta_i=ti, is_approx=False)
    p_reflect = surfer(r_array, f_acous, dp_dn=dp_dn)
    P_result, k_axis = beamform(p_reflect, dx_array)
    # check first kind result

    #surfer = surface_integral.FreqHelm(sine_pw, inter_x, kind=1)
    #b = sine_pw.p_inc(inter_x, theta_i=ti)
    #A = surfer._A(k_acous)
    #c = dp_dn / 4 / 1j * surfer.dx * surfer.gamma
    #b_forward = np.dot(A, c)
    #d1 = b - b_forward

    #surfer = surface_integral.FreqHelm(sine_pw, inter_x, kind=2)
    #dp_inc = sine_pw.dp_dn_inc(inter_x, theta_i=ti)
    #A = surfer._A(k_acous)
    #c = dp_dn / 4 / 1j * surfer.dx * surfer.gamma
    #b_forward = np.dot(A, c)

    #d2 = b - b_forward
    #return P_result, k_axis, d1, d2
    return P_result, k_axis, dp_dn

P_HK, k_HK, dp_dn_HK = kind_comp('HK')
#P_HK, k_HK, eHK1, eHK2 = kind_comp('HK')
P_1, k_1, dp_dn_1 = kind_comp(1)
#P_1, k_1, e11, e12 = kind_comp(1)
P_2, k_2, dp_dn_2 = kind_comp(2)
#P_2, k_2, e21, e22 = kind_comp(2)

fig, ax = plt.subplots()
ax.plot(k_HK, np.abs(P_HK) / max_p, label='HK')
ax.plot(k_1, np.abs(P_1) / max_p, label='1st')
ax.plot(k_2, np.abs(P_2) / max_p, label='2nd')
ax.plot([k_acous, k_acous], [-1e6, 1e6], 'k')
ax.plot([-k_acous, -k_acous], [-1e6, 1e6], 'k')

for n in tn:
    kn = k_acous * n
    ax.plot([kn, kn], [-1e6, 0.1], 'g:')

ax.set_title('Magnitude of reflected pressure')
#ax.set_title('Magnitude of reflected pressure\n'+
             #'Dotted lines are grating angles')
ax.set_xlabel('$k_x$, $m^{-1}$')
ax.set_ylabel('$|p(k_x)| / |p_{inc}|$')
ax.set_xlim(1, 7)
ax.set_xticks([2, 4, 6])
ax.set_xticklabels([2, '', 6])
ax.set_ylim(-0.05, 1.05)
ax.set_yticks([0, 0.5, 1])
ax.set_yticklabels([0, '', 1])
ax.annotate('$k_{acoustic}$', xy=(k_acous, 0.4), xytext=(k_acous + 0.7, 0.55),
            arrowprops=dict(facecolor='black', shrink=0.05),)
ax.legend(loc=2)
ax.xaxis.set_label_coords(0.5, -0.05)
ax.yaxis.set_label_coords(-0.03, 0.5)
ax.grid()
#plt.tight_layout()

# find the indecies of the reflection coefficents
gamma_0 = np.sqrt(1 - np.cos(ti) ** 2)
Rs = []
for n in tn:
    gamma_n = 1 - n ** 2
    if gamma_n < 0:
        continue
    xi = np.argmin(np.abs(n * k_acous - k_2))
    h1 = np.abs(P_1[xi] / max_p)
    h2 = np.abs(P_2[xi] / max_p)
    hk = np.abs(P_HK[xi] / max_p)
    Rs.append(np.array((h1, h2, hk, np.sqrt(gamma_n), k_2[xi])))

Rs = np.array(Rs)
ax.plot(Rs[:, -1], Rs[:, 0], '*', color='orange')
ax.plot(Rs[:, -1], Rs[:, 1], 'g*')
ax.plot(Rs[:, -1], Rs[:, 2], 'b*')

energy = np.array((np.sum(Rs[:, 0] ** 2 * Rs[:, 3] / gamma_0),
                   np.sum(Rs[:, 1] ** 2 * Rs[:, 3] / gamma_0),
                   np.sum(Rs[:, 2] ** 2 * Rs[:, 3] / gamma_0)))
plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
fig, ax = plt.subplots()
ax.plot(inter_x, np.abs(dp_dn_HK), label='HK')
ax.plot(inter_x, np.abs(dp_dn_1), label='1st')
ax.plot(inter_x, np.abs(dp_dn_2), label='2nd')

fig, ax = plt.subplots()
ax.plot(inter_x, eHK1, label='HK')
ax.plot(inter_x, e11, label='1st')
ax.plot(inter_x, e21, label='2nd')
ax.legend(loc=0)

fig, ax = plt.subplots()
ax.plot(inter_x, eHK1, label='HK')
ax.plot(inter_x, e12, label='1st')
ax.plot(inter_x, e22, label='2nd')
ax.legend(loc=0)


plt.show(block=False)
