"""
================================
Equivalence of H and HK solution
================================

High frequency pulse reflected from a flat surface, short range
and fc = 15 kHz.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber import isospeed_integral, plane_iso_field
from icepyks.surfaces import sin_surface

#cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface

c0 = 1500
# wave parameters
l_wave = 0.95 * 1e-2
h_wave = 0.32 * 1e-2
theta_i = np.radians(50)
g = 0.12

# wave height to acoustic wavenumber ratio
num_points = 6
point_bound = (0.6, 1.8)
point_range = point_bound[1] - point_bound[0]
points = np.arange(num_points) / (num_points - 1) * point_range
points += point_bound[0]
k_acous = points / h_wave
f_acous = k_acous * c0 / 2 / np.pi

# setup acoustic array
l_array = 75 * l_wave
l_integration = 300 * l_wave
z_array = -1 * 1e-2

# plot incident energy at each wavenumber
num_plot = 200
x_axis = np.arange(num_plot) / (num_plot - 1) * l_integration
x_axis -= np.mean(x_axis)
eta = sin_surface.Sine(l_wave, h_wave, phase=0.)
iso_field = plane_iso_field.PlaneWave(eta, g)

# check the horizontal extent of the planewave
if True:
    fig, ax = plt.subplots()
    for f in f_acous:
        ax.plot(x_axis, np.abs(iso_field.p_inc(x_axis, theta_i=theta_i,
                            Hz=f, is_approx=False)))
        #ax.plot(x_axis, np.abs(iso_field.p_inc(x_axis, theta_i=theta_i,
                            #Hz=f, is_approx=True)))
    ax.set_title('Amplitude of incident pressure')
    ax.set_xlabel('Position, m')
    ax.set_ylabel('$|p|$')
    ax.grid()
    plt.show(block=False)

# cut a very generous acoustic array
dx_array = (c0 / np.max(f_acous)) / 3
num_array = np.ceil(l_array / dx_array)
x_array = np.arange(num_array) * dx_array
x_array -= np.mean(x_array)
r_array = np.array((x_array, np.full_like(x_array, z_array))).T

# setup integration axis
for f in [f_acous[3]]:
    dx = c0 / f / 6
    k = 2 * np.pi * f / c0
    num_x = np.ceil(l_integration / dx)
    x_axis = np.arange(num_x) / (num_x - 1) * l_integration
    x_axis -= np.mean(x_axis)

    isospeed_HK = isospeed_integral.FreqHelm(iso_field, x_axis, kind='HK')
    helm_1 = isospeed_integral.FreqHelm(iso_field, x_axis, kind=1)
    helm_2 = isospeed_integral.FreqHelm(iso_field, x_axis, kind=2)

    dp_dn_HK = isospeed_HK.dp_dn(f, theta_i=theta_i, is_approx=False)
    dp_dn_1, e11, e12 = helm_1.dp_dn(f, theta_i=theta_i, is_approx=False,
                                     return_error=True)
    dp_dn_2, e21, e22 = helm_2.dp_dn(f, theta_i=theta_i, is_approx=False,
                                     return_error=True)

    p_Helm1 = helm_1(r_array, f, dp_dn=dp_dn_1)
    p_Helm2 = helm_2(r_array, f, dp_dn=dp_dn_2)
    p_HK = isospeed_HK(r_array, f, dp_dn=dp_dn_HK)

# Iterate a solution
dp_dn_inc = helm_2.dp_dn_inc(f, theta_i=theta_i)
A_2 = helm_2._A(k, 2)
pressures = [2 * dp_dn_inc]
for _ in np.arange(10):
    forward = A_2.dot(pressures[-1]) * helm_2.dx / 4 / 1j * helm_2.gamma
    pressures.append(2 * dp_dn_inc - forward)

iter_num = 1
p_HelmIter = helm_2(r_array, f, dp_dn=pressures[iter_num])

if True:
    fig, ax = plt.subplots()
    ax.plot(x_axis, np.abs(dp_dn_HK), label='HK')
    ax.plot(x_axis, np.abs(dp_dn_1), label='First')
    ax.plot(x_axis, np.abs(dp_dn_2), label='Second')
    ax.plot(x_axis, np.abs(pressures[iter_num]))
    ax.grid()

def beamform(p, dx):
    """simple beamformer result"""
    NFFT = 2 ** int(np.ceil(np.log2(p.size)) + 1)
    k_max = 2 * np.pi / dx
    k_axis = np.arange(NFFT) / NFFT * k_max
    k_axis -= k_max / 2
    P = np.fft.fftshift(np.fft.fft(p, NFFT))
    return P, k_axis

P_FTHK, k_axis = beamform(p_HK, dx_array)
P_FTHelm1, k_axis = beamform(p_Helm1, dx_array)
P_FTHelm2, k_axis = beamform(p_Helm2, dx_array)
P_FTHelmI, k_axis = beamform(p_HelmIter, dx_array)
fig, ax = plt.subplots()
ax.plot(k_axis, np.abs(P_FTHK))
ax.plot(k_axis, np.abs(P_FTHelm1))
ax.plot(k_axis, np.abs(P_FTHelm2))
ax.plot(k_axis, np.abs(P_FTHelmI))
ax.grid()

plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT

#Setup integration bounds
x_bounds = (-40, 40)
dx = c0 / fc / 8
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
spec_r = np.sqrt(r_receiver[0] ** 2 + (z_source + r_receiver[-1]) ** 2)
spec_tau = spec_r / c0

# solve for pressure field at surface
eta = sin_surface.Sine(lamda_surf, wave_amp, phase=0.)
iso_field = plane_iso_field.PlaneWave(eta, g)

# check the horizontal extent of the planewave
fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(iso_field.p_inc(xaxis, theta_i=theta_i, Hz=fc)))
ax.set_title('Amplitude of incident pressure')
ax.set_xlabel('Position, m')
ax.set_ylabel('$|p|$')
ax.grid()
plt.show(block=False)

isospeed_HK = isospeed_integral.FreqHelm(iso_field, xaxis, kind='HK')
#helm_1 = isospeed_integral.FreqHelm(iso_field, xaxis, kind=1)
helm_2 = isospeed_integral.FreqHelm(iso_field, xaxis, kind=2)

dp_dn_HK = isospeed_HK.dp_dn(fc, theta_i=theta_i,)
#dp_dn_1 = helm_1.dp_dn(fc, theta_i=theta_i,)
dp_dn_2, e1, e2 = helm_2.dp_dn(fc, theta_i=theta_i,)

fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(dp_dn_HK), label='HK')
#ax.plot(xaxis, np.abs(dp_dn_1), label='first')
ax.plot(xaxis, np.abs(dp_dn_2), label='second')
ax.grid()
ax.legend()

fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(e1), label='First')
#ax.plot(xaxis, np.abs(dp_dn_1), label='first')
ax.plot(xaxis, np.abs(e2), label='Second')
ax.grid()
ax.legend()

plt.show(block=False)

# Beamform for reflection coefficents
