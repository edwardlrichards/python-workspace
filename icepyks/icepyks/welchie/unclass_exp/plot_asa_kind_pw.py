"""
================================
Equivalence of H and HK solution
================================

High frequency pulse reflected from a flat surface, short range
and fc = 15 kHz.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber import isospeed_integral, plane_iso_field
from icepyks.surfaces import sin_surface

#cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface

c0 = 1500
z_source = -2.723
r_receiver = np.array((16.542, -1.906))
z_bottom = -4
# wave parameters
lamda_surf = 9.435
wave_amp = 0.422 / 2
theta_i = np.radians(50)
g = 15

#signal parameter
fc = 15e3

#Setup integration bounds
x_bounds = (-40, 40)
dx = c0 / fc / 8
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
spec_r = np.sqrt(r_receiver[0] ** 2 + (z_source + r_receiver[-1]) ** 2)
spec_tau = spec_r / c0

# solve for pressure field at surface
eta = sin_surface.Sine(lamda_surf, wave_amp, phase=0.)
iso_field = plane_iso_field.PlaneWave(eta, g)

# check the horizontal extent of the planewave
fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(iso_field.p_inc(xaxis, theta_i=theta_i, Hz=fc)))
ax.set_title('Amplitude of incident pressure')
ax.set_xlabel('Position, m')
ax.set_ylabel('$|p|$')
ax.grid()
plt.show(block=False)

isospeed_HK = isospeed_integral.FreqHelm(iso_field, xaxis, kind='HK')
#helm_1 = isospeed_integral.FreqHelm(iso_field, xaxis, kind=1)
helm_2 = isospeed_integral.FreqHelm(iso_field, xaxis, kind=2)

dp_dn_HK = isospeed_HK.dp_dn(fc, theta_i=theta_i,)
#dp_dn_1 = helm_1.dp_dn(fc, theta_i=theta_i,)
dp_dn_2, e1, e2 = helm_2.dp_dn(fc, theta_i=theta_i,)

fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(dp_dn_HK), label='HK')
#ax.plot(xaxis, np.abs(dp_dn_1), label='first')
ax.plot(xaxis, np.abs(dp_dn_2), label='second')
ax.grid()
ax.legend()

fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(e1), label='First')
#ax.plot(xaxis, np.abs(dp_dn_1), label='first')
ax.plot(xaxis, np.abs(e2), label='Second')
ax.grid()
ax.legend()

plt.show(block=False)

# Beamform for reflection coefficents
