import numpy as np
import matplotlib.pyplot as plt
from icepyks.surfaces import sin_surface, flat_surface
from icepyks.welchie import plane_iso_field
from icepyks.poodle import plane_wave_rs
from icepyks.welchie import horizontal_array
from icepyks import surface_integral
from scipy.linalg import solve

f_acous = 1050  # Hz
c = 1500

# periodic surface reflection
l_wave = 40
h_wave = 2.
inter_n = 6  # points per acoustic wavelength

# make surface an integer number of waves long
num_waves = 15 * 3
dx = c / f_acous / inter_n
ppw = l_wave // dx + 1
xax_onewave = np.arange(ppw) / ppw * l_wave
dx = (xax_onewave[-1] - xax_onewave[0]) / (xax_onewave.size - 1)

x_starts = (np.arange(num_waves) - num_waves // 2) * l_wave - l_wave / 2
xaxis = np.reshape(x_starts[:, None] + xax_onewave[None, :], -1)

total_x = num_waves * l_wave
wave_x = (-total_x / 2 - l_wave / 2, total_x / 2 + l_wave / 2)
g = float(np.abs(np.diff(wave_x))) / 5  # plane wave resolution parameter

# integration decimation
alpha = 0  # dB/km

cos_phase = np.pi / 2  # surface phase
ti_inc = np.radians(15.)

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous

# setup problem
cos_eta = sin_surface.Sine(l_wave, h_wave, phase=cos_phase)
cos_win = plane_iso_field.FinitePlaneWave(cos_eta, g, xaxis)
cos_win(f_acous, ti_inc)

# setup beamforming for the result
z_array = -10
dx_array = l_acous / 2
x_array = np.arange(np.min(xaxis) + 100, np.max(xaxis) - 100, dx_array)
r_array = np.array((x_array, np.full_like(x_array, z_array))).T

# common normalization term
flat_eta = flat_surface.Flat(z_offset=z_array)
flat_pw = plane_iso_field.FinitePlaneWave(flat_eta, g, x_array)
flat_pw(f_acous, ti_inc)

p_inc = flat_pw.p_inc(f_acous, ti_inc)
p_inc_FT, k_inc_axis = horizontal_array.beamform(p_inc, dx_array)
p_max = np.max(np.abs(p_inc_FT), axis=-1)

# reference periodic reflection coefficents
cos_per = plane_wave_rs.CosineReflectionCoefficents(h_wave, k_wave, attn=alpha)
kx_inc = np.cos(ti_inc) * k_acous
qvec = cos_per.qvec(5, kx_inc, f_acous)
_, kx_q, _, _ = cos_per.bragg_angles(kx_inc, qvec, f_acous)

# Exact reflection coefficents
r_hk_per = cos_per.r_HK(kx_inc, qvec, f_acous)
r_rh_per = cos_per.r_RH(kx_inc, qvec, f_acous)

# Windowed HIE
surfer_HIE2 = surface_integral.FSurfaceIntegral(cos_win, xaxis, kind=2)

# Iterative HIE
psi_HIE2 = surfer_HIE2.psi()
A = surfer_HIE2.A_matricies(2)

ier = [2 * surfer_HIE2.psi_inc()]
for i in range(20):
    inex = ier[0] - (A @ ier[-1]) * surfer_HIE2.gamma * surfer_HIE2.dx / 2j
    ier.append(inex)
ier = np.array(ier)

# one wave iteration test
xi = np.arange(xax_onewave.size)

ier_1wave = [2 * surfer_HIE2.psi_inc()]

for i in range(1):
    ier_1wave.append(np.zeros_like(ier_1wave[0]))
    for j in range(num_waves):
        xcurr = xi + j * xax_onewave.size
        one_A = A[np.ix_(xcurr, xcurr)]
        iforward = one_A @ ier_1wave[-2][xcurr]
        iforward *= surfer_HIE2.gamma[xcurr] * surfer_HIE2.dx / 2j
        ier_1wave[-1][xcurr] = ier_1wave[0][xcurr] - iforward

ier_1wave = np.array(ier_1wave)

ref = psi_HIE2 * np.exp(-1j * kx_inc * xaxis)
p_hk = ier[0, :] * np.exp(-1j * kx_inc * xaxis)
p_HIE1_total = ier[1, :] * np.exp(-1j * kx_inc * xaxis)
p_HIE1_local = ier_1wave[1, :] * np.exp(-1j * kx_inc * xaxis)

fig, ax = plt.subplots()
ax.plot(xaxis, np.abs(p_hk - ref), label='HK')
ax.plot(xaxis, np.abs(p_HIE1_total - ref), label='total')
ax.plot(xaxis, np.abs(p_HIE1_local - ref), label='local')

ax.legend()

rcr_greens = surfer_HIE2.rcr_greens(r_array)
pout = surfer_HIE2(r_array, f_acous, psi=psi_HIE2, rcr_greens=rcr_greens)
p_HIE2, k_axis = horizontal_array.beamform(pout, dx_array)

# beamform on HK pressure result
pout = surfer_HIE2(r_array, f_acous, psi=ier[0, :], rcr_greens=rcr_greens)
p_HK, k_axis = horizontal_array.beamform(pout, dx_array)

# beamform on first iterate result
pout = surfer_HIE2(r_array, f_acous, psi=ier[1, :], rcr_greens=rcr_greens)
p_I1, k_axis = horizontal_array.beamform(pout, dx_array)

# beamform on local result
pout = surfer_HIE2(r_array, f_acous, psi=ier_1wave[1, :], rcr_greens=rcr_greens)
p_Ilocal, k_axis = horizontal_array.beamform(pout, dx_array)

fig, ax = plt.subplots()

ax.plot(k_axis, np.abs(p_I1 - p_HIE2)/ p_max, label='I1')
ax.plot(k_axis, np.abs(p_Ilocal - p_HIE2)/ p_max, label='one wave')
ax.plot(k_axis, np.abs(p_HK - p_HIE2)/ p_max, label='HK')
ax.grid()
ax.legend()
plt.show(block=False)

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(xaxis, np.real(ier[0, :] * np.exp(-1j * kx_inc * xaxis)), label='HK')
axes[0].plot(xaxis, np.real(ier[1, :] * np.exp(-1j * kx_inc * xaxis)), label='I 1')
axes[0].plot(xaxis, np.real(psi_HIE2 * np.exp(-1j * kx_inc * xaxis)), label='HIE')
axes[0].set_title('Pressure field at surface\n'+
                  '$\partial / \partial n$, real')
axes[0].grid()
axes[0].legend(loc=3)

axes[1].plot(xaxis, np.imag(ier[0, :] * np.exp(-1j * kx_inc * xaxis)))
axes[1].plot(xaxis, np.imag(ier[1, :] * np.exp(-1j * kx_inc * xaxis)))
axes[1].plot(xaxis, np.imag(psi_HIE2 * np.exp(-1j * kx_inc * xaxis)))
axes[1].set_title('$\partial / \partial n$, imaginary')
axes[1].set_xlabel('Position, x')
axes[1].grid()

fig, ax = plt.subplots()
for i in ier:
    ax.plot(xaxis, np.abs(i - psi_HIE2))

ax.set_title('Error magnitude of pressure field vs. iteration')
ax.set_xlabel('Position, x')

plt.show(block=False)

beams = []
for i in ier:
    p_refl = surfer_HIE2(r_array, f_acous, psi=i, rcr_greens=rcr_greens)
    p, k_axis = horizontal_array.beamform(p_refl, dx_array)
    beams.append(p)

beams = np.array(beams)

fig, ax = plt.subplots()

ax.plot(k_axis, np.abs(beams[-1, :] - p_HIE2) / p_max, label='I 20')
ax.plot(k_axis, np.abs(beams[1, :] - p_HIE2) / p_max, label='I 1')
ax.plot(k_axis, np.abs(beams[0, :] - p_HIE2) / p_max, label='HK')
ax.plot([k_acous, k_acous], [-100, 100], ':', color='0.4')
ax.set_xlim(2, 6)
ax.set_ylim(0, .4)
ax.set_title(r'Beam error, $\theta_{inc}$=%.1f$^o$'%
             np.degrees(ti_inc) + '\n' +
             'f=%.1f Hz, k=%.2f $m^{-1}$'%(f_acous, k_acous))
ax.set_ylabel('Magnitude')
ax.set_xlabel(r'Horizontal wave number, $k_x$')
ax.grid()
ax.legend()

plt.show(block=False)
