import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from icepyks.surfaces import flat_surface, sin_surface
from icepyks.welchie import plane_iso_field, horizontal_array

f_acous = 1050  # Hz
c = 1500
wave_x = (-300, 300)
xspan = (wave_x[1] - wave_x[0])
plot_x = (-300, 300)
plotspan = (plot_x[1] - plot_x[0])
z_array = -0.5  # meter from waterline
g = 2 * np.abs(wave_x[0]) / 5  # plane wave resolution parameter
theta_i = 31
# periodic surface reflection
l_wave = 10
h_wave = 0.4
# integration decimation
inter_n = 6  # points per acoustic wavelength
alpha = 0  # dB/km

# Ploting specfications
plot_n = 2  # points per acoustic wavelength
plotdx = c / f_acous / plot_n
dx = c / f_acous / inter_n
z_max = -10
z_span = (z_max - h_wave)
num_z = np.ceil(np.abs(z_span / plotdx))
num_x = np.ceil(xspan / dx)
num_p = np.ceil(plotspan / plotdx)
z_plot = np.arange(num_z) / num_z * z_max - (h_wave + 0.05)
x_plot = np.arange(num_p) / num_p * plotspan + plot_x[0]
x_integrate = np.arange(num_x) / num_x * xspan + wave_x[0]

X, Z = np.meshgrid(x_plot, z_plot)

# calculated values
k_wave = 2 * np.pi / l_wave
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous

ti = np.radians(theta_i)
n_theta = np.arange(20) - 10
tn = np.cos(ti) + n_theta * k_wave / k_acous
d_theta = 2 / (k_acous * g * np.sin(ti))

p_inc = []
for z in z_plot:
    eta = flat_surface.Flat(z_offset=z)
    flat_pw = plane_iso_field.PlaneWave(eta, g)
    p_inc.append(flat_pw.p_inc(x_plot, theta_i=ti, Hz=f_acous, is_approx=False))
p_inc = np.array(p_inc)

fig, ax = plt.subplots()
ax.pcolormesh(X, Z, np.abs(p_inc))

# beamform on incident field at a single depth
p_beam = p_inc[0, :]
dx_array = x_plot[1] - x_plot[0]
p_FT, k_axis = horizontal_array.beamform(p_beam, dx_array)
i_max = np.argmax(np.abs(p_FT))
p_max = np.abs(p_FT[i_max]) ** 2

# scale incident energy with incident direction
dk = k_axis[1] - k_axis[0]
kz = np.sqrt(k_acous ** 2 - k_axis ** 2)
k_scale = kz / kz[i_max]
total_energy = np.sum(np.abs(p_FT) ** 2 * k_scale) * dk

fig, ax = plt.subplots()
ax.plot(k_axis, np.abs(p_FT))
ax.set_xlabel('Horizontal wave number, 1/m')
ax.set_ylabel('Beam response')
ax.set_title('Wave number of incident field')

plt.show(block=False)
