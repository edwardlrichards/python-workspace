import numpy as np
import matplotlib.pyplot as plt
from icepyks.surfaces import flat_surface, sin_surface
from icepyks.welchie import plane_iso_field

def beamform(p, dx):
    """simple beamformer result"""
    NFFT = 2 ** int(np.ceil(np.log2(p.shape[-1])) + 1)
    k_max = 2 * np.pi / dx
    k_axis = np.arange(NFFT) / NFFT * k_max
    k_axis -= k_max / 2
    P = np.fft.fftshift(np.fft.fft(p, NFFT), axes=-1)
    return P, k_axis

def R_coeff(P, k_axis, k_cuttoff):
    """Determine the location and energy of each reflection coefficent"""
    # peaks should have some percent of total energy
    p_thresh = 0.00001
    energy = np.abs(P) ** 2
    min_e = np.sum(energy) * p_thresh
    de = np.diff(energy)
    # code for detecting positive zero crossing from SO
    pos = de > 0
    pos = np.bitwise_and(pos[:-1], ~pos[1:])
    pos = np.hstack((False, pos, False))
    pos = np.bitwise_and(pos, energy > min_e)
    pos = np.bitwise_and(pos, k_axis < k_cuttoff)
    return pos


if __name__ == '__main__':
    from icepyks.welchie import isospeed_integral
    f_acous = 1050  # Hz
    c = 1500
    array_span = (-50, 50)
    wave_x = (-60, 60)
    z_array = -0.5  # meter from waterline
    g = 20  # plane wave resolution parameter
    theta_i = 15
    # periodic surface reflection
    l_wave = 10
    h_wave = 0.4
    # integration decimation
    inter_n = 8  # points per acoustic wavelength

    # calculated values
    k_wave = 2 * np.pi / l_wave
    l_acous = c / f_acous
    k_acous = 2 * np.pi / l_acous
    ti = np.radians(theta_i)
    n_theta = np.arange(20) - 10
    tn = np.cos(ti) + n_theta * k_wave / k_acous
    # integration axis
    inter_dx = l_acous / inter_n

    # find evanescent wave with 2 e-folding depths
    k_z_cut = 2 * np.pi / np.abs(z_array)
    k_x_cut = np.sqrt(float(np.abs(k_acous ** 2 - k_z_cut ** 2)))
    dx_array = np.pi / k_x_cut

    # create array
    num_x = np.ceil((array_span[1] - array_span[0]) / dx_array)
    x_array = np.arange(num_x) * dx_array + array_span[0]
    r_array = np.array((x_array, np.full_like(x_array, z_array)))

    # integration axis
    num_x = int((wave_x[1] - wave_x[0]) / inter_dx)
    inter_x = np.arange(num_x) * inter_dx + np.min(wave_x)

    eta = flat_surface.Flat()
    flat_pw = plane_iso_field.PlaneWave(eta, g)

    p_inc_1 = flat_pw.p_inc(x_array, theta_i=ti, Hz=f_acous)


    P_FT1, k_axis = beamform(p_inc_1, dx_array)

    def energy_summation(p_FT, k_axis, theta_i):
        """Energy conservation, modeled after the MCs"""
        k_real = np.abs(k_axis) < k_acous
        gamma_i = k_acous * np.sin(theta_i)
        gamma_real = np.sqrt(k_acous ** 2 - k_axis[k_real] ** 2)
        energy = np.sum(np.abs(p_FT[k_real]) ** 2 * gamma_real / gamma_i)
        return energy

    max_p = np.max(np.abs(P_FT1))
    inc_energy = energy_summation(P_FT1, k_axis, ti)

    fig, ax = plt.subplots()
    ax.plot(k_axis, np.abs(P_FT1) / max_p)
    k_x = np.cos(ti) * k_acous
    ax.plot([k_acous, k_acous], [-1e6, 1e6], 'k')
    ax.plot([-k_acous, -k_acous], [-1e6, 1e6], 'k')
    ax.set_title('Magnitude of incident pressure')
    ax.set_xlabel('$k_x$, $m^{-1}$')
    ax.set_ylabel('$|p(k_x)| / |p_{inc}|$')
    ax.set_ylim(-0.05, 1.05)
    ax.set_xlim(0, 6)
    ax.set_xticks([1, 3, 5])
    ax.set_xticklabels([1, '', 5])
    ax.set_yticks([0, 0.5, 1])
    ax.set_yticklabels([0, '', 1])
    ax.annotate('$k_{acoustic}$', xy=(k_acous, 0.4), xytext=(k_acous + 0.7, 0.55),
                arrowprops=dict(facecolor='black', shrink=0.05),)
    ax.xaxis.set_label_coords(0.5, -0.05)
    ax.yaxis.set_label_coords(-0.03, 0.5)
    ax.grid()
    plt.show(block=False)

    sine_eta = sin_surface.Sine(l_wave, h_wave)
    sine_pw = plane_iso_field.PlaneWave(sine_eta, g)
    r_array = np.array((x_array, np.full_like(x_array, z_array))).T

    def kind_comp(kind):
        """cycle through solution kinds"""
        surfer = isospeed_integral.FreqHelm(sine_pw, inter_x, kind=kind)
        dp_dn = surfer.dp_dn(f_acous, theta_i=ti, is_approx=False)
        p_reflect = surfer(r_array, f_acous, dp_dn=dp_dn)
        P_result, k_axis = beamform(p_reflect, dx_array)
        return P_result, k_axis, dp_dn

    P_HK, k_HK, dp_dn_HK = kind_comp('HK')
    P_1, k_1, dp_dn_1 = kind_comp(1)
    P_2, k_2, dp_dn_2 = kind_comp(2)

    fig, ax = plt.subplots()
    ax.plot(k_HK, np.abs(P_HK) / max_p, label='HK')
    ax.plot(k_1, np.abs(P_1) / max_p, label='1st')
    ax.plot(k_2, np.abs(P_2) / max_p, label='2nd')
    ax.plot([k_acous, k_acous], [-1e6, 1e6], 'k')
    ax.plot([-k_acous, -k_acous], [-1e6, 1e6], 'k')

    for n in tn:
        kn = k_acous * n
        ax.plot([kn, kn], [-1e6, 0.1], 'g:')

    ax.set_title('Magnitude of reflected pressure')
    ax.set_xlabel('$k_x$, $m^{-1}$')
    ax.set_ylabel('$|p(k_x)| / |p_{inc}|$')
    ax.set_xlim(0, 6)
    ax.set_xticks([1, 3, 5])
    ax.set_xticklabels([1, '', 5])
    ax.set_ylim(-0.05, 1.05)
    ax.set_yticks([0, 0.5, 1])
    ax.set_yticklabels([0, '', 1])
    ax.annotate('$k_{acoustic}$', xy=(k_acous, 0.4), xytext=(k_acous + 0.7, 0.55),
                arrowprops=dict(facecolor='black', shrink=0.05),)
    ax.legend(loc=2)
    ax.xaxis.set_label_coords(0.5, -0.05)
    ax.yaxis.set_label_coords(-0.03, 0.5)
    ax.grid()
    peaks = R_coeff(P_HK, k_HK, k_acous)
    kz = k_acous * np.sin(ti)
    gamma_correction = np.sqrt(k_acous ** 2 - k_HK[peaks] ** 2) / kz

    e_HK = np.sum((np.abs(P_HK[peaks]) / max_p) ** 2 * gamma_correction)
    e_2 = np.sum((np.abs(P_2[peaks]) / max_p) ** 2 * gamma_correction)

    e_HK_Sum = energy_summation(P_HK, k_HK, ti) / inc_energy
    e_2_Sum = energy_summation(P_2, k_2, ti) / inc_energy

    ax.plot(k_HK[peaks], np.abs(P_HK[peaks]) / max_p, 'k.')

    plt.show(block=False)
    import sys; sys.exit("User break") # SCRIPT EXIT
