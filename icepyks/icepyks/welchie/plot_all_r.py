import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from icepyks.surfaces import flat_surface, sin_surface
from icepyks.clumber import isospeed_integral, plane_iso_field,\
                            horizontal_array

f_acous = 1050  # Hz
c = 1500
l_acous = c / f_acous
k_acous = 2 * np.pi / l_acous

refl_coeff = []
ia = []
energy = []
for f in ['off_grazing_2nd.npz', 'on_grazing_2nd.npz']:

    npzfile = np.load(f)
    incident_angles = npzfile['incident_angles']
    dx_array = float(npzfile['dx_array'])
    p_refl = npzfile['p_refl']
    p_max = npzfile['p_max']
    p_FT, k_axis = horizontal_array.beamform(p_refl, dx_array)
    for pr, ti, pm in zip(p_FT, incident_angles, p_max):
        r_i = horizontal_array.R_coeff(pr, k_axis, k_acous)
        r = pr[r_i] / pm
        r_0 = np.argmin(np.abs(k_axis[r_i] - k_acous * np.cos(ti)))
        r_index = np.arange(np.sum(r_i)) - r_0
        refl_coeff.append((r_index, np.array((k_axis[r_i], r))))
    ia.append(incident_angles)
    energy.append(npzfile['energy'])
incident_angles = np.hstack(ia)
energy = np.hstack(energy)

# plot a few reflection coefficents
plot_i = np.array([-3, -2, -1, 0, 1, 2, 3])
plot_r = []
for ri, rs in refl_coeff:
    ip = np.in1d(ri, plot_i)
    refls = np.zeros(plot_i.shape)
    refls[: np.sum(ip)] = np.abs(rs[1, ip])
    plot_r.append(refls)
plot_r = np.array(plot_r)

fig, ax = plt.subplots()
for pr, ip in zip(plot_r.T, plot_i):
    label = 'R%i'%ip
    ax.plot(np.degrees(incident_angles), pr, label=label)

ax.set_title('HIE Reflection coefficents with incident angle')
ax.set_xlabel(r'$\theta_i$, deg')
ax.set_ylabel('$|R|$')
ax.legend(loc=1)
ax.set_ylim(-.1, 1.3)
ax.grid()

fig, ax = plt.subplots()
ax.plot(np.degrees(incident_angles), energy)
ax.set_ylim(.8, 1.1)
ax.set_xlabel('Incident angle, degrees')
ax.set_ylabel(r'Energy conservered, \%')
ax.set_title('HIE energy conservation')

plt.show(block=False)
