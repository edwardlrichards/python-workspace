import numpy as np
import scipy.signal as sig

samples_percycle = 40  # designed for 2nd order interpolator

def pulse_nuttall(fc):
    """approx Q = 1 pulse, simple derivatives"""
    T = 1 / fc
    num_cycles = 6
    total_time = num_cycles * T
    t_sample = np.r_[0: total_time: 1j * num_cycles * samples_percycle]
    y_sample = np.sin(2 * np.pi * fc * t_sample) *\
        np.sin(np.pi * t_sample / total_time) ** 4
    return y_sample, t_sample

def pulse_q1(fc):
    """Q = 1 pulse"""
    T = 1 / fc
    num_cycles = 4
    total_time = num_cycles * T
    t_sample = np.r_[0: total_time: 1j * num_cycles * samples_percycle]
    y_sample = np.sin(2 * np.pi * fc * t_sample) *\
        sig.windows.kaiser(t_sample.size, 3 * np.pi)
    return y_sample, t_sample


def kam():
    """use KAM11 probe"""
    from beamforming.tests import lfm_planewave
    y_sample = lfm_planewave.lfmchirp()
    t_sample = np.arange(y_sample.size) / 1e5
    return y_sample, t_sample


def mseq():
    """use KAM11 probe"""
    from kam11.mseq import mseq
    y_sample = mseq.modulated_mseq(23e3)
    return y_sample


def pulse_udel():
    """pulse used in the KAM11 tripod x-missions"""
    T = 48 / 1e3  # signal duration
    fs = 1e6
    f0 = 7e3
    f1 = 13e3
    # Wiki LFM formulation
    k = (f1 - f0) / T
    num_samples = np.ceil(T * fs)
    t_sample = np.arange(num_samples) / fs
    phase = 2 * np.pi * (f0 * t_sample + (k / 2) * t_sample ** 2)
    y_sample = np.sin(phase) * sig.windows.kaiser(t_sample.size, 1.5 * np.pi)
    y_sample = sig.correlate(y_sample, y_sample, mode='same')
    y_sample /= np.max(y_sample)

    # cut out some of the signal edge
    t_i = np.bitwise_and(t_sample >= 21e-3, t_sample <= 27e-3)
    y_sample = y_sample[t_i]
    t_sample = t_sample[t_i]
    t_sample -= t_sample[0]
    return y_sample, t_sample


def pulse_narrowband():
    """Manufactured to keep bandwidth in simulations low"""
    T = 48 / 1e3  # signal duration
    fs = 1e6
    f0 = 8e3
    f1 = 12e3
    # Wiki LFM formulation
    k = (f1 - f0) / T
    num_samples = np.ceil(T * fs)
    t_sample = np.arange(num_samples) / fs
    phase = 2 * np.pi * (f0 * t_sample + (k / 2) * t_sample ** 2)
    y_sample = np.sin(phase) * sig.windows.kaiser(t_sample.size, 0.75 * np.pi)
    y_sample = sig.correlate(y_sample, y_sample, mode='same')
    y_sample /= np.max(y_sample)

    # cut out some of the signal edge
    t_i = np.bitwise_and(t_sample >= 21e-3, t_sample <= 27e-3)
    y_sample = y_sample[t_i]
    t_sample = t_sample[t_i]
    t_sample -= t_sample[0]
    return y_sample, t_sample


def pulse():
    """A truncated sine wave at 200 kHz, like Walstead and Deane"""
    f0 = 200e3
    T = 1 / f0
    dt = T / samples_percycle
    num_cycles = 3
    t_sample = np.arange(num_cycles * samples_percycle + 1) * dt
    y_sample = np.sin(2 * np.pi * f0 * t_sample) *\
        sig.windows.kaiser(t_sample.size, 2.0 * np.pi)
    return y_sample, t_sample

def sine_four_hanning(fc_src):
    """Windowed sine wave used by Young Min"""
    T = 1 / fc_src
    num_cycles = 4
    total_time = num_cycles * T
    tpulse = np.r_[0: total_time: 1j * num_cycles * samples_percycle]
    BW = fc_src
    N = tpulse.size
    tpulse = tpulse[tpulse <= 4 * 1 / fc_src]
    src_sig = 1 / 2 * np.sin(np.pi + 2 * np.pi * fc_src * tpulse) *\
        (1 - np.cos(np.pi / 2 * fc_src * tpulse))
    return src_sig, tpulse

def plot_signal(signal_oi, taxis):
    """Plot values of interest for a given signal"""
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots()
    ax.plot(taxis * 1e3, signal_oi, linewidth=3)
    ax.set_title('Transmitted signal')
    ax.set_xlabel('Time, ms')
    ax.grid()

    # Fourier transform
    dt = taxis[1] - taxis[0]
    NFFT = 2 ** 12
    f_axis = np.arange(NFFT // 2 + 1) / NFFT / dt
    x_mit_FT = np.fft.rfft(signal_oi, NFFT)

    fig, ax1 = plt.subplots()
    ft_db = 20 * np.log10(np.abs(x_mit_FT))
    ft_max = np.max(ft_db)
    ax1.plot(f_axis / 1e3, ft_db, linewidth=3)
    ax1.set_xlabel('Frequency, kHz')
    ax1.set_title('Transmitted signal')
    ax1.set_ylabel('signal, dB')
    ax1.set_ylim(ft_max - 60, ft_max + 1)
    ax1.set_xlim(0, 5)
    ax1.grid()
    return ax, ax1

if __name__ == '__main__':
    import matplotlib.pyplot as plt

    #x_mit, taxis = pulse_narrowband()
    #x_mit, taxis = pulse_q1(1500)
    x_mit, taxis = sine_four_hanning(2500)
    plot_signal(x_mit, taxis)

    plt.show(block=False)
