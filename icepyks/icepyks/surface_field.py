"""
Surface derivative terms and Green's functions at surface, for sources and
receivers that are not on the surface.
"""
import numpy as np
from icepyks.clumber.wf_atsurface import wf_attop
from icepyks.clumber.wf_atwave import WFAtWave
from icepyks.clumber.interp1d import tau_stationary

class SurfaceField:
    """organizes the movement of wavefronts from one surface to the next"""
    def __init__(self, eta, z_source):
        """Propagator both manages wavefronts and provides computed values
        """
        #properties and objects common to all wavefronts
        self.eta = eta
        self.z_src = z_source

    def p_inc(self, x_axis, num_dims=3, Hz=None):
        """Amplitude and delay of incident pressure at surface"""
        pass

    def dp_dn_inc(self, x_axis, num_dims=3, Hz=None):
        """normal derivative of the incident pressure field """
        pass

    def rcr_greens(self, x_axis, r_rcr, num_dims=3, Hz=None):
        """Amplitude and delay from surface at x_axis to receiver"""
        pass

    def direct_ray(self, r_rcr, num_dims=3, Hz=None):
        """Amplitude and delay of direct ray from source to receiver"""
        pass

    def grad_scale(self, x_axis):
        """A comonly used pre-muliplier arrising from normal derivative"""
        z = self.eta.z(x_axis)
        r_surf = np.array((x_axis, z - self.z_src)).T
        proj_vec = np.sum(r_surf * self.eta.grad(x_axis), axis=-1)\
                   / np.linalg.norm(r_surf, axis=-1)
        return proj_vec
