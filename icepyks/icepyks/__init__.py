"""
==============================
Icepyks suface scatter package
==============================

.. currentmodule: icepyks

.. autosummary::
   :toctree: generated/

   surface_integral
   eigen_rays
"""
