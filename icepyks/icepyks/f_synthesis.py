import numpy as np
from scipy.special import hankel1

def add_direct(direct_r, scatter_FT, fcompute, num_dim=3, c=1500.):
    """Add the direct path pressure to the scattered pressure field"""
    k_compute = 2 * np.pi * fcompute / c
    if num_dim == 2:
        gdir = hankel1(0, k_compute * direct_r)
    elif num_dim == 3:
        gdir = np.exp(1j * k_compute * direct_r) / direct_r
    else:
        raise(ValueError('Number of dimensions is not valid'))
    return scatter_FT + gdir

def synthesize_ts(greens_FT, signal_interpolator, fcompute):
    """Synthesis the time series at the receiver from it's FT"""
    greens_FT = np.array(greens_FT, ndmin=1)
    # setup fourier synthesis
    fcompute = np.array(fcompute, ndmin=1)
    df = (fcompute[-1] - fcompute[0]) / (fcompute.size - 1)
    # don't assume that fcompute is populated down to dc
    NFFT = int(2 ** np.ceil(np.log2(fcompute[-1] / df) + 1))
    # axes are normalized to each other, and start at 0
    faxis = np.arange(NFFT // 2 + 1) * df
    taxis = np.arange(NFFT) / NFFT / df
    # match the greens function size to the new faxis
    xmitt_f = np.zeros(faxis.size) + 0j
    xstart = int(fcompute[0] / df)
    xmitt_f[xstart: xstart + greens_FT.size] = greens_FT
    # sample the signal according to the taxis
    signal_ts = signal_interpolator(taxis)
    # Convolove the greens function with the xmitt signal, as real FFT
    signal_f = np.fft.rfft(signal_ts)
    xmission_f = np.conj(xmitt_f) * signal_f
    xmission_ts = np.fft.irfft(xmission_f)
    return xmission_ts, taxis

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from icepyks import pulse_signal, signal_interp

    r_dir = 20.  # m
    r_spec = 21  # m

    fc = 15000.
    df = 0.8  # Hz, approx
    frange = (6000, 24000)

    # new time axis
    signal, tsignal = pulse_signal.pulse_q1(fc)
    sig_ier = signal_interp.ContinuousSignal(signal, tsignal)

    # compute fourier transform of image source result
    numf = int(np.ceil((frange[1] - frange[0]) / df))
    faxis = np.arange(numf) * df + frange[0]

    g_FT3d = -np.exp(1j * 2 * np.pi * faxis / 1500. * r_spec) / r_spec
    g_FT2d = -hankel1(0, 2 * np.pi * faxis / 1500. * r_spec)

    dir_FT3d = add_direct(r_dir, g_FT3d, faxis)
    dir_FT2d = add_direct(r_dir, g_FT2d, faxis, num_dim=2)

    ts_3d, taxis = synthesize_ts(dir_FT3d, sig_ier, faxis)
    ts_2d, _ = synthesize_ts(dir_FT2d, sig_ier, faxis)

    fig, ax = plt.subplots()
    ax.plot(taxis * 1e3, ts_3d, label='3d')
    ax.plot(taxis * 1e3, ts_2d, label='2d')
    ax.grid()
    ax.legend()
    ax.set_title('Direct arrival and surface bounce')
    ax.set_xlim(13, 15)

    plt.show()
