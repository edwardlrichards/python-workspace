"""
========================================
Line and point sources on cosine surface
========================================

Class designed for exact solutions of Rayleigh hypothesis and Helmholtz 
integral equations.
"""
import numpy as np
from icepyks.surfaces import sin_surface
from scipy.special import jn
from scipy.interpolate import interp1d
from scipy.linalg import solve

class Cosine_HKA:
    """a surface described as a cosine"""
    def __init__(self, hwave, kwave, attn=1):
        """surface specific properties
        attn: volume attenuation, dB / km"""
        self.hwave = hwave
        self.kwave = kwave
        self.lwave = 2 * np.pi / self.kwave
        self.c = 1500.
        self.attn = attn
        self.delta = lambda k: 1j * self.attn / 8686.\
                               / np.real(k + np.spacing(1))
        self.eta = sin_surface.Sine(self.lwave, self.hwave, phase=np.pi/2)

    def kacous(self, freq):
        """complex wavenumeber of medium"""
        k = 2 * np.pi * freq / self.c
        return (1 + self.delta(k)) * k

    def rspec(self, rsrc, rrcr):
        """Return the specular reflection distance"""
        r = np.sqrt((rrcr[0] - rsrc[0]) ** 2 + (rrcr[-1] + rsrc[-1]) ** 2)
        return r

    def hk_spatial_2D(self, freq, rsrc, rrcr, xaxis):
        """Perform the spatial 2D HK integral, 1 freq"""
        rwave = np.array([xaxis, self.eta.z(xaxis)])
        rvec = rwave - rsrc[:, None]
        r1 = np.linalg.norm(rvec, axis=0)
        r2 = np.linalg.norm(rrcr[:, None] - rwave, axis=0)
        dx = xaxis[1] - xaxis[0]
        # put attenuation directly into the wavenumber
        k = self.kacous(freq)
        # use far field hankel approximation
        igrand = np.exp(1j * k * (r1 + r2))\
                 * np.sum(self.eta.grad(xaxis) * rvec, axis=0)\
                 / r1 ** (3 / 2) / np.sqrt(r2)
        hk = 1j / np.pi * np.sum(igrand) * dx
        return hk

    def hk_spatial_3D(self, freq, rsrc, rrcr, xaxis):
        """Perform the spatial 3D HK integral, 1 freq
        assumes yr = 0
        """
        rwave = np.array([xaxis, self.eta.z(xaxis)])
        rvec = rwave - rsrc[:, None]
        r1 = np.linalg.norm(rvec, axis=0)
        r2 = np.linalg.norm(rrcr[:, None] - rwave, axis=0)
        dx = xaxis[1] - xaxis[0]
        # put attenuation directly into the wavenumber
        k = self.kacous(freq)
        # setup the amplitude vectors
        A2 = 1j * k / 2 / np.pi\
             * np.sum(self.eta.grad(xaxis) * rvec, axis=0) / r1 ** 2 / r2
        # This definition does not allow for y to vary from 0
        d2r_dy2 = (1 / r1 + 1 / r2)
        A1 = np.sqrt(2 * np.pi / k / d2r_dy2) * A2 * np.exp(1j * np.pi / 4)
        hk = np.sum(A1 * np.exp(1j * k * (r1 + r2))) * dx
        return hk

    def hk_spatial_2D_time(self, rrcr, rsrc, xaxis, taxis, signal_interper):
        """Perform the spatial 2D HK integral"""
        rwave = np.array([xaxis, self.eta.z(xaxis)])
        rvec = rwave - rsrc[:, None]
        r1 = np.linalg.norm(rvec, axis=0)
        r2 = np.linalg.norm(rrcr[:, None] - rwave, axis=0)
        dx = xaxis[1] - xaxis[0]
        tau = (r1 + r2) / self.c
        delays = taxis - tau[:, None]
        igrand = np.sum(self.eta.grad(xaxis) * rvec, axis=0) / r1 ** (3 / 2)\
                 / np.sqrt(r2)
        sval = signal_interper(delays)
        hk = 1 / np.pi * np.sum(igrand[:, None] * sval, axis=0) * dx
        return hk

    def hk_spatial_3D_time(self, rrcr, rsrc, xaxis, taxis, signal_interper):
        """Perform the spatial 3D HK integral, currently assume yr=0"""
        rwave = np.array([xaxis, self.eta.z(xaxis)])
        rvec = rwave - rsrc[:, None]
        r1 = np.linalg.norm(rvec, axis=0)
        r2 = np.linalg.norm(rrcr[:, None] - rwave, axis=0)
        dx = xaxis[1] - xaxis[0]
        # define amplitude without imaginary and omega terms
        A2 = np.sum(self.eta.grad(xaxis) * rvec, axis=0) / r1 ** 2 / r2\
             / 2 / np.pi / self.c
        # This definition does not allow for y to vary from 0
        d2r_dy2 = (1 / r1 + 1 / r2)
        A1 = np.sqrt(2 * np.pi * self.c / d2r_dy2) * A2
        # time domain search and interpolate
        tau = (r1 + r2) / self.c
        delays = taxis - tau[:, None]
        sval = signal_interper(delays)
        hk = np.sum(A1[:, None] * sval, axis=0) * dx
        return hk
