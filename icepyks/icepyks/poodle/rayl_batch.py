"""
=========================================
Compute reflection coefficents in batches
=========================================

"""

import numpy as np
from icepyks.poodle import cosine_surface, wn_synthesis
import time
from scipy.linalg import solve
from concurrent.futures import ThreadPoolExecutor
from os.path import join, isdir
from os import makedirs

hwave = 1.
lwave = 40.
num_eva = 4  # number of evanescent orders
num_thetas = 400  # number of values in incident angle axis
#hwave = 0.6
#lwave = 10.
#num_thetas = 200  # number of values in incident angle axis
#num_eva = 3  # number of evanescent orders
eva_range = 0.1  # extension into imaginary angles
theta_axis = wn_synthesis.make_theta_axis(num_thetas, eva_range)
save_dir = join(r'/enceladus0', 'e2richards', 'rayliegh_rs',
                'wh_%02i_wl%02i'%(hwave * 10, lwave * 10))
if not isdir(save_dir): makedirs(save_dir)

kwave = 2 * np.pi / lwave
cos_ier = cosine_surface.CosineSurface(hwave, kwave)

def write_coeff(frun):
    """Write reflection coefficents to a txt file"""
    # get the requested reflection coefficents
    save_name = join(save_dir, 'rh_%04i_Hz_h%02i_l%02i'%(
                frun, hwave * 10, lwave * 10))
    # preallocate result array
    all_ns = np.unique(np.hstack([cos_ier.qvec(num_eva, theta_axis[0], frun),
                                  cos_ier.qvec(num_eva, theta_axis[-1], frun)]))
    one_freq = np.zeros((theta_axis.size, all_ns.size), dtype=np.complex_)
    rh_ab = lambda ti: cos_ier.rh_terms(np.real(np.cos(ti)),
                                        cos_ier.qvec(num_eva, ti, frun),
                                        frun)

    # There is a cost benifit tipping point for batch calculation of A matrix
    if frun < 2500:
        for i, ti in enumerate(theta_axis):
            ns = cos_ier.qvec(num_eva, ti, frun)
            savei = np.bitwise_and(all_ns >= ns[0],  all_ns <= ns[-1])
            one_freq[i, savei] = cos_ier.r_RH(np.real(np.cos(ti)), ns, frun)
    else:
        with ThreadPoolExecutor(max_workers=4) as executor:
            rh_coeffs = list(executor.map(rh_ab, theta_axis))
        for i, (ti, coeffs) in enumerate(zip(theta_axis, rh_coeffs)):
            #HK calculations
            ns = cos_ier.qvec(num_eva, ti, frun)
            savei = np.bitwise_and(all_ns >= ns[0],  all_ns <= ns[-1])
            one_freq[i, savei] = solve(coeffs[0], coeffs[1])

    np.savez(save_name, r_vec=one_freq, frun=frun,
             theta_axis=theta_axis, nvec=all_ns, hwave=hwave, lwave=lwave)
        
frange = np.arange(500, 6501)
#frange = np.arange(6293, 6501)
list(map(write_coeff, frange))
