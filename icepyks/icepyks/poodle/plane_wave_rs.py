"""
=========================================
Plane wave reflection from cosine surface
=========================================

Compute reflection coefficents using Rayleigh hypothesis (RH), Helmholtz
integral equations (HIE) and the Kirchhoff approximation (HKA). Analytical 
integrations specific to the cosine surface are used for RH and HKA.
"""
import numpy as np
from icepyks.surfaces import sin_surface
from scipy.special import jn
from scipy.interpolate import interp1d
from scipy.linalg import solve

class CosineReflectionCoefficents:
    """Compute reflection coefficents for cosine surface"""
    def __init__(self, hwave, kwave, attn=1):
        """surface specific properties
        attn: volume attenuation, dB / km"""
        self.hwave = hwave
        self.kwave = kwave
        self.lwave = 2 * np.pi / self.kwave
        self.c = 1500.
        self.attn = attn
        self.delta = lambda k: 1j * self.attn / 8686.\
                               / np.real(k + np.spacing(1))
        self.eta = sin_surface.Sine(self.lwave, self.hwave, phase=np.pi/2)

    def kacous(self, freq):
        """complex wavenumeber of medium"""
        k = 2 * np.pi * freq / self.c
        return (1 + self.delta(k)) * k

    def rspec(self, rsrc, rrcr):
        """Return the specular reflection distance"""
        r = np.sqrt((rrcr[0] - rsrc[0]) ** 2 + (rrcr[-1] + rsrc[-1]) ** 2)
        return r

    def p_inc(self, x, kx, facous, is_per=True):
        """Incident pressure field"""
        kacous = np.real(self.kacous(facous))  # no attenuation of incident pw
        x = np.array(x, ndmin=1)
        kxy = np.array([kx, -np.sqrt(kacous ** 2 - kx ** 2)])
        r = np.array([x, self.eta.z(x)])
        psi = np.exp(1j * np.sum(kxy[:, None] * r, axis=0))
        if is_per:
            psi = psi * np.exp(-1j * kx * x)
        return np.squeeze(psi)

    def psi_inc(self, x, kx, facous, is_per=True):
        """Solve for psi incident on surface"""
        kacous = np.real(self.kacous(facous))  # no attenuation of incident pw
        x = np.array(x, ndmin=1)
        kxy = np.array([kx, -np.sqrt(kacous ** 2 - kx ** 2 + 0j)])
        r = np.array([x, self.eta.z(x)])
        phase = np.exp(1j * np.sum(kxy[:, None] * r, axis=0))
        gn = np.sum(self.eta.grad(x).T * kxy[:, None], axis=0)
        psi = np.squeeze(1j * gn * phase)
        if is_per:
            psi = psi * np.exp(-1j * kx * x)
        return np.squeeze(psi)

    def back_project(self, rs, qs, xaxis, kx, freq, is_per=True):
        """Back project reflection coefficents to surface"""
        r = np.array([xaxis, self.eta.z(xaxis)]).T
        a0, aq, b0, bq = self.bragg_angles(kx, qs, freq)
        wave_num = np.array([aq, bq]).T
        phase = np.exp(1j * np.sum(wave_num[:, None, :]
                                   * r[None, :, :], axis=-1))
        gn = np.sum(wave_num[:, None, :]
                    * self.eta.grad(xaxis)[None, :, :], axis=-1)
        psi = np.sum(1j * rs[:, None] * gn * phase, axis=0)
        if is_per:
            psi = psi * np.exp(-1j * kx * xaxis)
        return np.squeeze(psi)

    def r_psi(self, xaxis, psi_vec, kx, qs, freq):
        """Calculate the reflection coefficents from psi at surface"""
        DX = (xaxis[-1] - xaxis[0]) / (xaxis.size - 1)
        a0, aq, b0, bq = self.bragg_angles(kx, qs, freq)
        basis_funcs = np.exp(-1j * (bq[:, None] * self.eta.z(xaxis)
                                    + qs[:, None] * self.kwave * xaxis))\
                      / (2j * self.lwave * bq[:, None])
        rs = np.sum(basis_funcs * psi_vec, axis=-1) * DX
        return rs

    def qvec(self, num_eva, kx, freq):
        """Return vector of bragg grating orders
        cutoff after num_eva evanescent orders on each side
        """
        kacous = np.real(self.kacous(freq))
        kx = np.real(kx)
        num_p = np.fix((kacous - kx) / self.kwave) + num_eva
        num_n = np.fix((kacous + kx) / self.kwave) + num_eva
        qvec = np.arange(-num_n, num_p + 1)
        return qvec

    def r_HK(self, kx, qs, freq):
        """Return a vector of the q-th reflection coefficents, HK approximation
        """
        a0, aq, b0, bq = self.bragg_angles(kx, qs, freq)
        rs = 1j ** -qs\
             * jn(qs, self.hwave / 2 * (b0 + bq))\
             * (a0 * (aq - a0) - (b0 * (b0 + bq))) / (bq * (b0 + bq))
        return rs

    def r_RH(self, kx, qs, freq):
        """Compute reflection coefficents using Rayleigh Hypothesis
        """
        A, b = self.rh_terms(kx, qs, freq)
        return solve(A, b)

    def psi_HIE_1st(self, xaxis, kx, freq, num_eva=3):
        """Solve for the normal derivative of the pressure at the surface"""
        gmat = -self._g(xaxis, kx, freq, num_eva)
        b = self.p_inc(xaxis, kx, freq, is_per=True)
        return solve(gmat, b)

    def psi_HIE_2nd(self, xaxis, kx, freq, num_eva=3):
        """Solve for the normal derivative of the pressure at the surface"""
        nmat = self._n(xaxis, kx, freq, num_eva)
        nmat = np.diag(np.ones(xaxis.size)) / self.eta.norm_grad(xaxis)\
               - 2 * nmat
        b = 2 * self.psi_inc(xaxis, kx, freq, is_per=True)\
            / self.eta.norm_grad(xaxis)
        return solve(nmat, b)

    def p_ref(self, rs, cosines, freq, rsrc, rrcr):
        """Synthesize plane waves for reflected pressure amplitude
        rs is a vector of complex plane wave weights
        """
        rs = np.array(rs, ndmin=2)
        a0 = np.array(cosines[0], ndmin=1)
        aq = np.array(cosines[1], ndmin=2)
        b0 = np.array(cosines[2], ndmin=1)
        bq = np.array(cosines[3], ndmin=2)
        k = self.kacous(freq)
        dist = np.sum(np.array([aq, -bq]) * rrcr[:, None, None], axis=0)\
                - np.sum(np.array([a0, b0]) * rsrc[:, None], axis=0)[:, None]
        # the real part of k still has to be expained
        p_ref = np.sum(rs * np.exp(1j * np.real(k) * dist), axis=-1)
        return p_ref

    def r_energy(self, rs, kx, qs, freq):
        """Calculate the energy conservation relative to 1"""
        a0, aq, b0, bq = self.bragg_angles(kx, qs, freq)
        kacous = self.kacous(freq)
        # compute energy
        reali = np.abs(np.real(aq ** 2)) <= np.real(kacous) ** 2
        en_conn = np.abs(rs[reali]) ** 2 * np.real(bq[reali]) / np.real(b0)
        return np.sum(en_conn)

    def rh_terms(self, kx, qs, freq):
        """separating solving step from matrix construction"""
        a0, aq, b0, bq = self.bragg_angles(kx, qs, freq)
        b = jn(qs,  b0 * self.hwave / 2)
        nm_diff = qs[:, None] - qs[None, :]
        A = -1j ** qs[None, :] * jn(nm_diff, -self.hwave / 2 * bq)
        return A, b

    def bragg_angles(self, kx, qs, freq):
        """Computer the brag angle cosine vectors"""
        kacous = self.kacous(freq)
        a0 = np.array(kx, ndmin=1)
        qs = np.array(qs, ndmin=1)
        b0 = np.sqrt(kacous ** 2 - a0 ** 2)
        aq = a0 + qs * self.kwave
        bq = np.sqrt(kacous ** 2 - aq ** 2)
        return a0, aq, b0, bq

    def _g(self, xaxis, kx, freq, num_eva):
        """integral kernel, with asymptotics removed"""
        kacous = self.kacous(freq)
        xaxis = np.array(xaxis, ndmin=1)
        dx = xaxis[:, None] - xaxis[None, :]
        DX = (xaxis[-1] - xaxis[0]) / (xaxis.size - 1)
        adz = np.abs(self.eta.z(xaxis)[:, None] - self.eta.z(xaxis)[None, :])
        qrange = self.qvec(num_eva, kx, freq)
        a0, aq, b0, bq = self.bragg_angles(kx, qrange, freq)
        # setup naive kernel
        Gkernel = np.exp(1j * (bq[None, :, None] * adz[:, None, :]
                              + qrange[None, :, None]
                              * self.kwave * dx[:, None, :])) \
                  / bq[None, :, None]
        # remove asymptotic values
        qpi = qrange > 0
        qni = qrange < 0
        # G_infty values for positive qrange
        Gkernel[:, qpi, :] += (np.exp(-a0 * adz[:, None, :])
                               * np.exp(qrange[None, qpi, None] * self.kwave
                                        * (1j * dx[:, None, :]
                                           - adz[:, None, :])))\
                               * 1j / (qrange[None, qpi, None] * self.kwave)
        # G_infty values for negative qrange
        Gkernel[:, qni, :] -= (np.exp(a0 * adz[:, None, :])
                               * np.exp(qrange[None, qni, None] * self.kwave
                                        * (1j * dx[:, None, :]
                                           + adz[:, None, :])))\
                               * 1j / (qrange[None, qni, None] * self.kwave)
        # sum up q terms to get (G - G_infty)
        Gkernel = np.sum(Gkernel, axis=1)
        # compute G_inifity result, requires special treatment when dx == 0
        nzi = np.ones(Gkernel.shape, dtype=np.bool_)
        nzi[np.diag_indices_from(nzi)] = 0
        G_inf = np.zeros_like(Gkernel)
        # analytical result of G_infty sum
        G_inf[nzi] = np.exp(-a0 * adz[nzi])\
                * np.log(1 - np.exp(self.kwave * (1j * dx[nzi] - adz[nzi])))\
                + np.exp(a0 * adz[nzi])\
                * np.log(1 - np.exp(-self.kwave * (1j * dx[nzi] + adz[nzi])))
        G_inf *= 1j / self.kwave
        # populate diagonal
        G_p_inf = np.zeros(Gkernel.shape, dtype=np.float_)
        G_p_inf[nzi] = np.log(np.abs(dx[nzi]) / self.lwave) \
                       + np.log(1 - np.abs(dx[nzi]) / self.lwave)
        eq_val = np.log(2 * np.pi) + np.log(1 + self.eta.z_p(xaxis) ** 2) / 2
        # sum up all elements of G'_inf except when m == n
        G_i_d = 2j / self.kwave * (eq_val - np.sum(G_p_inf, axis=1))
        G_inf[np.diag_indices_from(G_inf)] = G_i_d
        # Add back infinite result
        G = (Gkernel + G_inf) * DX / (2j * self.lwave)
        # Add back analytical integral result
        G[np.diag_indices_from(G)] -= self.lwave / np.pi
        return G

    def _n(self, xaxis, kx, freq, num_eva):
        """integral kernel, with asymptotics removed"""
        xaxis = np.array(xaxis, ndmin=1)
        z = self.eta.z(xaxis)
        dx = xaxis[:, None] - xaxis[None, :]
        DX = (xaxis[-1] - xaxis[0]) / (xaxis.size - 1)
        # pull out the sign of dz before taking absolute value
        dz = z[:, None] - z[None, :]
        sdz = np.sign(dz)
        ep = self.eta.z_p(xaxis)
        # Numerically sum series in Bragg order number
        qrange = self.qvec(num_eva, kx, freq)
        # second term does not contribute to asymptotic sum when n==0
        signq = np.sign(qrange)
        # zero-ith term is a pain
        qzi = signq != 0
        # angle sines and cosines
        alpha0, alphas, _, betas = self.bragg_angles(kx, qrange, freq)
        # calculate complex phase term
        Nnaive = (sdz[:, None, :]
                  - (alphas / betas)[None, :, None] * ep[:, None, None])\
                          * np.exp(1j * (betas[None, :, None]
                                         * np.abs(dz)[:, None, :] + 
                                         qrange[None, :, None] * self.kwave
                                         * dx[:, None, :]))
        # aysmptotic terms
        Ninf = (sdz[:, None, :]
                + 1j * signq[None, qzi, None] * ep[:, None, None])\
               * np.exp(-signq[None, qzi, None]
                        * (alpha0 + qrange[None, qzi, None] * self.kwave)
                        * np.abs(dz)[:, None, :])\
               * np.exp(1j * qrange[None, qzi, None] * self.kwave
                        * dx[:, None, :])
        # subtract asymptotic values from all but q=0 term
        Nrobust = Nnaive
        Nrobust[:, qzi, :] -= Ninf
        Nrobust = np.sum(Nrobust, axis=1)
        # don't calculate results for main diagonal
        nzi = np.ones(Nrobust.shape, dtype=np.bool_)
        nzi[np.diag_indices_from(nzi)] = 0
        # Add back analytic sum of asymptotic values
        ep2 = ep[:, None] * np.ones_like(ep)
        qpos = (sdz[nzi] + 1j * ep2[nzi])\
               * np.exp(-alpha0 * np.abs(dz)[nzi])\
               * np.exp(1j * self.kwave * dx[nzi])\
               / (np.exp(self.kwave * np.abs(dz)[nzi])
                  - np.exp(1j * self.kwave * dx[nzi]))
        qneg = (sdz[nzi] - 1j * ep2[nzi])\
               * np.exp(alpha0 * np.abs(dz[nzi]))\
               / (np.exp(self.kwave * (1j * dx[nzi] + np.abs(dz)[nzi])) - 1)
        Nrobust[nzi] += qpos + qneg
        Nrobust /= 2 * self.lwave
        # replace 0 dx term with asymptotic value
        # This is not EQ 3.106 of EToG (Petit et al.) (1/2d) difference!!
        natx = -ep / (2 * self.lwave)\
               * (np.sum(alphas / betas) + 1j * alpha0 / (2 * np.pi))\
               - self.eta.z_pp(xaxis) / (4 * np.pi * (1 + ep ** 2))
        Nrobust[np.diag_indices_from(Nrobust)] = natx
        Nrobust *= DX
        return Nrobust
