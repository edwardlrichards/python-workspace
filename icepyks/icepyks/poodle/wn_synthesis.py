"""
====================
Wave number sythesis
====================

Methods that standardize wave number integrations for reflection coefficents
that are expressed as values at specific point of as functions.
"""
import numpy as np
from os.path import join, isfile
import matplotlib.pyplot as plt
from scipy.optimize import brentq

class WaveNumberSynthesis:
    """Defines a bunch of commonly used procedures for wave number synthesis
    """
    def __init__(self, rsrc, rrcr, lwave,
                 is_symetric=True, is_r_function=False):
        """Define parameters expected to remain relativly constant"""
        self.rsrc = rsrc
        self.rrcr = rrcr
        # depth terms come up a lot
        self.zr = self.rrcr[-1]
        self.zs = self.rsrc[-1]

        self.lwave = lwave
        self.is_symetric = is_symetric
        self.is_r_function = is_r_function
        # grating order functions
        self.an = lambda a0, q, frun: a0 + q * 1500.  / (frun * self.lwave)
        self.bn = lambda a: np.sqrt(1 - a ** 2 + 0j)

    def _cphase(self, frun, a0, b0, an, bn):
        """compute the complex phase function of the wave number term"""
        p = np.exp(1j * 2 * np.pi * frun / 1500. * (
                   an * self.rrcr[0] - bn * self.rrcr[-1]
                   - a0 * self.rsrc[0] - b0 * self.rsrc[-1]))
        return p

    def kx_int(self, qcoeff, qvec, rs, r_th_axis, frun, numquad, eva_range=0.1,
               num_dims=3):
        """Compute the wave number integral with stationary phase in ky

        Computes integral for a single q coefficent
        Assumes the ky stationary point is at 0
        """
        # pick out reflection coefficent of interest
        qi = (qvec == qcoeff)
        roi = np.squeeze(rs[:, qi])
        # interpolation axis
        a0 = np.linspace(-(1 + eva_range), (1 + eva_range), num=numquad)
        da = a0[1] - a0[0]
        # setup the grating order terms
        b0 = self.bn(a0)
        an = self.an(a0, qcoeff, frun)
        bn = self.bn(an)
        # compute the source and receiver phase terms
        cphase = self._cphase(frun, a0, b0, an, bn)
        # interpolate reflection coefficent axis
        aaxis = np.cos(r_th_axis)
        # make sure that cos did not flip axis order
        if aaxis[1] < aaxis[0]:
            roi = roi[::-1]
            aaxis = aaxis[::-1]
        # interpolation and extrapolation step
        # extend the reflection coefficents past the calculated bounds
        if (qcoeff == 0):
            rup = np.interp(a0, aaxis, roi, left=-1., right=-1.)
        else:
            rup = np.interp(a0, aaxis, roi, left=0., right=0.)
        # modulate the reflection coefficent with the source and receiver info
        if num_dims == 3:
            # stationary phase corrections
            kmod = np.sqrt(np.abs(b0 / bn / (self.zr * b0 + self.zs * bn)))
            iconst = np.exp(1j * np.pi / 4) * np.sqrt(frun / 1500.)
        elif num_dims == 2:
            # prevent a divide by 0 by removing singularities from integral
            nzeroi = np.abs(b0) > 1e-7
            kmod = np.empty_like(b0)
            kmod[nzeroi] = 1 / b0
            kmod[np.bitwise_not(nzeroi)] = 0
            iconst = 1 / np.pi
        else:
            raise(ValueError('Number of dimensions must be 2 or 3'))
        # sum to get result
        pn = iconst *  np.nansum(rup * cphase * kmod) * da
        return pn

    def theta_int(self, qcoeff, qvec, rs, r_th_axis, frun, numquad, eva_range=0.1,
                  num_dims=3):
        """Compute the integral kernel with source and receiver added
        """
        # pick out reflection coefficent of interest
        qi = (qvec == qcoeff)
        roi = np.squeeze(rs[:, qi])
        # interpolation axis
        th_axis = make_theta_axis(numquad, eva_range, is_half=False)
        # create an index to the real values
        ri = np.full(th_axis.size, False)
        numimag = (th_axis.size - numquad) // 2
        ri[numimag: numquad + numimag - 1] = True
        # setup the grating order terms
        # real an prevents issues with sqrt branch cut arrising from small imag
        a0 = np.real(np.cos(th_axis))
        b0 = self.bn(a0)
        an = self.an(a0, qcoeff, frun)
        bn = self.bn(an)
        # compute the source and receiver phase terms
        cphase = self._cphase(frun, a0, b0, an, bn)
        # extend the reflection coefficents past the calculated bounds
        # The negative sign is to convince numpy axis is monotonic increasing,
        # when in fact it is monotonic decrease, but keeps a 1 to 1 equivalence
        rup = np.interp(-a0,
                        -np.real(np.cos(r_th_axis)),
                        roi, left=0., right=0.)
        if num_dims == 3:
            # stationary phase corrections
            kmod = np.empty(b0.size, dtype=np.complex_)
            singi = np.abs(self.zr * b0 + self.zs * bn) == 0
            nsi = np.bitwise_not(singi)
            kmod[nsi] = np.sqrt(bn[nsi] * b0[nsi] / (np.abs(self.zr * b0[nsi]
                                                     + self.zs * bn[nsi])))
            # bn == b0 == 0 has a removable singularity
            kmod[singi] = np.sqrt(b0[singi] / (np.abs(self.zr + self.zs)))
            # common coefficent
            iconst = np.exp(1j * np.pi / 4) * np.sqrt(frun / 1500.)
        elif num_dims == 2:
            kmod = np.ones(th_axis.size)
            iconst = 1 / np.pi
        else:
            raise(ValueError('Number of dimensions must be 2 or 3'))
        # modulate the reflection coefficent with the source and receiver info
        # sum to get result
        #return a0, kmod * rup * cphase
        dt = float(np.real(th_axis[ri][1] - th_axis[ri][0]))
        rint = np.nansum(kmod[ri] * rup[ri] * cphase[ri]) * dt
        ii = np.bitwise_not(ri)
        iint = np.nansum(kmod[ii] * rup[ii] * cphase[ii]) * -1j * dt
        pn = iconst * (rint + iint)
        return pn

    def kx_sta_ky_sta(self, qcoeff, qvec, rs, r_th_axis, frun, num_dims=3,
                      return_asta=False):
        """Compute the wave number integral result with stationary phase"""
        # pick out reflection coefficent of interest
        qi = (qvec == qcoeff)
        roi = np.squeeze(rs[:, qi])
        # Solve rooter to find stationary phase point
        rooter = lambda a: self.an(a, qcoeff, frun) * np.abs(self.zr)\
                           / np.real(self.bn(self.an(a, qcoeff, frun)))\
                           + a * np.abs(self.zs) / np.real(self.bn(a))\
                           - (self.rrcr[0] - self.rsrc[0])
        #check if there is no possible stationary phase point
        noff = qcoeff * 1500. / (frun * self.lwave)
        if np.abs(noff) >= 1:
            return 0. + 0j
        elif noff <= 0:
            bound = (-noff + 1e-6, 1 - 1e-6)
        elif noff > 0:
            bound = (1e-6, 1 - noff - 1e-6)
        # even if there is a possible area for the stationary phase point,
        # there is no guarantee that there will be one
        try:
            asta = brentq(rooter, bound[0], bound[1])
        except ValueError:
            return 0. + 0j
        # find the reflection coefficent value at stationary point
        # The negative sign is to convince numpy axis is monotonic increasing,
        # when in fact it is monotonic decrease, but keeps a 1 to 1 equivalence
        rns = np.interp(-asta, -np.real(np.cos(r_th_axis)), roi)
        b0 = self.bn(asta)
        an = self.an(asta, qcoeff, frun)
        bn = self.bn(an)
        # compute the source and receiver phase terms
        cphase = self._cphase(frun, asta, b0, an, bn)
        # stationary phase result
        if num_dims == 3:
            pn = rns * b0 * bn ** 2 * cphase \
                / (np.sqrt(np.abs(self.zr * b0 + self.zs * bn))
                * np.sqrt(np.abs(self.zr * b0 ** 3 + self.zs * bn ** 3)))
        elif num_dims == 2:
            pn = np.sqrt(1500. / (np.pi ** 2 * frun)) * rns * cphase\
                 * np.exp(-1j * np.pi / 4)\
                 * np.sqrt(bn ** 3 * b0)\
                 / np.sqrt(np.abs(self.zr * b0 ** 3 + self.zs * bn ** 3))
        if return_asta:
            return pn, asta
        else:
            return pn

def read_coeff(hwave, lwave, frun, save_dir, is_symetric=True):
    """Load file from batch reflection coefficent routine
    is_symetric: Boolean
        Do we assume that we can make the reflection coefficents symetric
    """
    load_name = join(save_dir,
                     'wh_%02i_wl%02i'%(hwave * 10, lwave * 10),
                     'rh_%04i_Hz_h%02i_l%02i'%(frun, hwave * 10, lwave * 10))

    # if there isn't a file, don't contibute to wave number integral
    if not isfile(load_name + '.npz'):
        return None, None, None

    with np.load(load_name + '.npz') as data:
        tia = data['theta_axis']
        rs = data['r_vec']
        ns = data['nvec']
        assert data['hwave'] == hwave
        assert data['lwave'] == lwave

    if is_symetric:
        ns, tia, rs = make_symetric(ns, tia, rs)

    return ns, tia, rs

def make_theta_axis(num_real, eva_range, is_half=True):
    """Make a theta axis that extends into a small part of imaginary space
    is_half: axis range: (0 + j eva_range, np.pi / 2)
                if not, axis range: (0 + j eva_range, np.pi - j eva_range)
    """
    if is_half:
        max_val = np.pi / 2
    else:
        max_val = np.pi
    theta_axis = np.linspace(0, max_val, num_real)
    dth = theta_axis[1] - theta_axis[0]
    eva = np.arange(dth, eva_range, dth) * 1j
    if is_half:
        theta_axis = np.hstack([eva[::-1], theta_axis])
    else:
        theta_axis = np.hstack([eva[::-1], theta_axis, np.pi - eva])
    return theta_axis

def make_symetric(qvec, thetas, rvalues):
    """ Make reflection coefficents symetric around vertical incidence
    Appropriate for symetric wave profiles
    """
    qvec = np.array(qvec, ndmin=1)
    thetas = np.array(thetas, ndmin=1)
    rvalues = np.array(rvalues, ndmin=1)
    # flip all the rvectors around 0 incidence angle
    max_calc = int(max([np.abs(np.min(qvec)), np.max(qvec)]))
    min_calc = int(min([np.abs(np.min(qvec)), np.max(qvec)]))
    full_qvec = np.arange(-max_calc, max_calc + 1)

    # stack zeros on rvalues to make it symetric with full_qvec
    diff_calc = max_calc - min_calc
    if diff_calc > 0:
        extra_zeros = np.zeros((rvalues.shape[0], diff_calc),
                                dtype=np.complex_)
        # find the side that needs the zeros
        if np.abs(qvec[0]) == min_calc:
            rvalues = np.hstack([extra_zeros, rvalues])
        else:
            rvalues = np.hstack([rvalues, extra_zeros])

    # Add symetric arrivals
    rvalues = np.vstack([rvalues, rvalues[-2::-1, ::-1]])
    taxis = np.hstack([thetas, np.pi - thetas[-2::-1]])

    return full_qvec, taxis, rvalues
