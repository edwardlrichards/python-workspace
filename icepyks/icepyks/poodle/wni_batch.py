"""
=================================
Batch processing for WN synthesis
=================================

Precompute reflection coefficents before wave number synthesis
"""

import numpy as np
from os.path import join, isfile
import matplotlib.pyplot as plt
from scipy.optimize import brentq

class BatchWNI:
    """Load reflection coefficent files and perform wave number integration"""
    def __init__(self, hwave, lwave, r_src, r_rcr, numval=None, max_imag=0.05):
        """Basic wave number integration specifications"""
        self.hwave = hwave
        self.lwave = lwave
        self.r_src = r_src
        self.r_rcr = r_rcr
        self.save_dir = join(r'/enceladus0', 'e2richards', 'rayliegh_rs',
                             'wh_%02i_wl%02i'%(hwave * 10, lwave * 10))
        # coefficents used in numerical wave number integration
        self.numval = None
        self.max_imag = 0.05

    def __call__(self, fcompute, is_stationary=True):
        """Load each of the files in fcompute, and perform wni"""
        fcompute = np.array(fcompute, ndmin=1)
        one_wni = lambda f: self._one_wni(f, is_stationary)
        return list(map(one_wni, fcompute))

    def _one_wni(self, frun, is_stationary):
        """Load a sigle frequency file and return a complex amplitude"""
        ns, tis, rs = self.read_coeff(frun)
        if ns is not None:
            if is_stationary:
                return np.sum([self.wni_sta(n, ns, tis, rs, frun)
                              for n in ns])
            else:
                return np.sum([self.wn_integration(n, ns, tis, rs, frun)
                               for n in ns])
        else:
            return 0. + 0j

    def read_coeff(self, frun):
        """Load file from batch reflection coefficent routine"""
        load_name = join(self.save_dir, 'rh_%04i_Hz_h%02i_l%02i'%(
                    frun, self.hwave * 10, self.lwave * 10))

        # if there isn't a file, don't contibute to wave number integral
        if not isfile(load_name + '.npz'):
            return None, None, None

        with np.load(load_name + '.npz') as data:
            tia = data['tiaxis']
            rs = data['r_vec']
            ns = data['nvec']
            assert data['hwave'] == self.hwave
            assert data['lwave'] == self.lwave

        # flip all the rvectors around 0 incidence angle
        max_calc = int(min([np.abs(np.min(ns)), np.max(ns)]))
        flipi = np.abs(ns) <= max_calc

        # only count vectors which have a symetric component
        rs = rs[:, flipi]
        ns = ns[flipi]

        # Add symetric arrivals
        rs = np.vstack([rs, rs[-2::-1, ::-1]])
        tis = np.hstack([tia, np.pi - tia[-2::-1]])

        return ns, tis, rs

    def wn_integration(self, qcoeff, ns, tis, rs, f):
        """Compute the integral kernel with source and receiver added
        """
        if ns is None:
            return 0. + 0j

        ni = (ns == qcoeff)
        roi = np.squeeze(rs[:, ni])

        tup = np.linspace(0, np.pi, num=numval)
        # add a small portion of the evanecent spectrum
        dt = tup[1] - tup[0]
        numimag = int(np.ceil(max_imag / dt))
        tup = np.hstack([np.linspace(max_imag, dt, num=numimag) * 1j,
                        tup,
                        np.pi - 1j * np.linspace(dt, max_imag, num=numimag)])

        a0 = np.cos(tup)
        b0 = np.sqrt(1 - a0 ** 2 + 0j)
        an = a0 + qcoeff * 1500. / (f * self.lwave)
        bn = np.sqrt(1 - an ** 2 + 0j)

        # compute the source and receiver phase terms
        cphase = np.exp(1j * 2 * np.pi * f / 1500. * (
                        an * self.r_rcr[0] - bn * self.r_rcr[-1]
                        - a0 * self.r_src[0] - b0 * self.r_src[-1]))

        # extend the reflection coefficents past the calculated bounds
        if (qcoeff == 0):
            rup = np.interp(np.real(tup[numimag: numval + numimag]),
                            tis, roi, left=-1., right=-1.)
            rup = np.hstack([-np.ones(numimag, dtype=np.complex_),
                            rup,
                            -np.ones(numimag, dtype=np.complex_)])
        else:
            rup = np.interp(np.real(tup[numimag: numval + numimag]),
                            tis, roi, left=0., right=0.)
            rup = np.hstack([np.zeros(numimag, dtype=np.complex_),
                            rup,
                            np.zeros(numimag, dtype=np.complex_)])
        # setup the grating order terms
        if (qcoeff == 0):
            # n == 0 has a removable singularity
            kmod = np.sqrt(b0 / (np.abs(self.r_rcr[-1] + self.r_src[-1])))
        else:
            kmod = np.sqrt(bn * b0 / (np.abs(self.r_rcr[-1] * b0
                                      + self.r_src[-1] * bn)))
        # modulate the reflection coefficent with the source and receiver info
        kern = kmod * rup * cphase
        pn = np.exp(1j * np.pi / 4) * np.sqrt(f / 1500.)\
            * np.sum(kern) * dt
        return pn

    def wni_sta(self, qcoeff, ns, tis, rs, f):
        """Compute the wave number integral result with stationary phase"""
        if ns is None:
            return 0. + 0j

        b0 = lambda a: np.sqrt(1 - a ** 2)
        an = lambda a: a + qcoeff * 1500. / (f * self.lwave)
        bn = lambda a: np.sqrt(1 - an(a) ** 2)

        rooter = lambda a: -(self.r_rcr[0] - self.r_src[0])\
                        + an(a) * np.abs(self.r_rcr[-1]) / bn(a)\
                        + a * np.abs(self.r_src[-1]) / b0(a)
        #check if there is no possible stationary phase point
        noff = qcoeff * 1500. / (f * self.lwave)
        if np.abs(noff) >= 1:
            return 0. + 0j
        elif noff <= 0:
            bound = (-noff + 1e-6, 1 - 1e-6)
        elif noff > 0:
            bound = (1e-6, 1 - noff - 1e-6)

        try:
            asta = brentq(rooter, bound[0], bound[1])
            return asta
        except ValueError:
            #print('No stationary point, f=%i Hz, q=%i'%(f, qcoeff))
            return 0. + 0j

        # find the reflection coefficent value at stationary point
        ni = (ns == qcoeff)
        roi = np.squeeze(rs[:, ni])
        rns = np.interp(np.arccos(asta), tis, roi)

        stationary = rns * b0(asta) * bn(asta) ** 2 \
                    * np.exp(1j * 2 * np.pi * f / 1500. * (
                                an(asta) * self.r_rcr[0]
                                - bn(asta) * self.r_rcr[-1]
                                - asta * self.r_src[0]
                                - b0(asta) * self.r_src[-1]))\
                    / (np.sqrt(np.abs(self.r_rcr[-1] * b0(asta)
                                + self.r_src[-1] * bn(asta)))
                        * np.sqrt(np.abs(self.r_rcr[-1] * b0(asta) ** 3
                                + self.r_src[-1] * bn(asta) ** 3)))
        return stationary
