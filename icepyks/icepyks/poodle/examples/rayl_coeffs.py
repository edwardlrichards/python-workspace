import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1, iv
from scipy.signal import resample
from scipy.linalg import solve
from icepyks import pulse_signal, signal_interp
from icepyks.poodle import cosine_surface

hwave = 3
lwave = 40.
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
ftest = 0
titest = 0.01 * np.pi / 180
numx = 300
numqs = 10  # number of evanescent orders
tiaxis = np.linspace(0.01, 90, 500) * np.pi / 180

kwave = 2 * np.pi / lwave
cos_ier = cosine_surface.CosineSurface(hwave, kwave, attn=0)

# test convergence of incident pressure
katest = 2 * np.pi * ftest / 1500

xaxis = np.arange(numx) / numx * lwave

# get the requested reflection coefficents
nvec = lambda a: np.arange(np.ceil(-ftest * lwave / 1500
                           * (1 + a)) - numqs, 
                           np.floor(ftest * lwave / 1500
                           * (1 - a)) + numqs + 1);

a0 = np.cos(titest)

#HK calculations
rhk, bs = cos_ier.r_hk(a0, nvec(a0), ftest, return_bs=True)

# Rayleigh hypothesis
r_RH = cos_ier.r_RH(a0, nvec(a0), ftest)

# energy conservation calculations
ehk = cos_ier.r_energy(rhk, bs[2], bs[3], bs[1])
e_RH = cos_ier.r_energy(r_RH, bs[2], bs[3], bs[1])

print('HK Energy conservation at %.1f degrees: %.4f'%
      (titest * 180 / np.pi, ehk))
print('RH Energy conservation at %.1f degrees: %.4f'%
      (titest * 180 / np.pi, e_RH))
import sys; sys.exit("User break") # SCRIPT EXIT

fig, ax = plt.subplots()
ax.set_title('Reflection coefficents, ti = %.1f degrees'%np.degrees(titest))
ax.plot(nvec(a0), np.abs(r_RH), '*', label='Rayleigh')
ax.plot(nvec(a0), np.abs(rhk), '.', label='HK')
ax.grid()
ax.set_ylim(-0.2, 1)
ax.legend()
ax.set_xlabel('Bragg order, $n$')
ax.set_ylabel(r'$|R_n|$')


rhk = []
r_RH = []
# all angle run
ts = time.time()
for i, ti in enumerate(tiaxis):
    a0 = np.cos(ti)
    #HK calculations
    rt, bs = cos_ier.r_hk(a0, nvec(ti), ftest, return_bs=True)
    rhk.append(rt)
    # Rayleigh hypothesis
    r_RH.append(cos_ier.r_RH(a0, nvec(ti), ftest))
    if (i % 20) == 0:
        print('run number %i'%i)

te = time.time()

print('Elapased time is %f seconds'%(te - ts))

rhk = np.array(rhk)
r_RH = np.array(r_RH)

ri = 1
rh_plot = []
hk_plot = []
for i, ti in enumerate(tiaxis):
    ns = nvec(ti)
    ploti = (ns == ri)
    rh_plot.append(np.abs(r_RH[i, ploti]))
    hk_plot.append(np.abs(rhk[i, ploti]))

rh_plot = np.array(rh_plot)
hk_plot = np.array(hk_plot)

fig, ax = plt.subplots()
ax.set_title('Reflection coefficents, $n$ = %i'%ri)
ax.plot(np.degrees(tiaxis), rh_plot, linewidth=4, label='Rayleigh')
ax.plot(np.degrees(tiaxis), hk_plot, label='HK')
ax.grid()
ax.set_ylim(-0.2, 1.5)
ax.legend()
ax.set_xlabel('incident angle, degrees')
ax.set_ylabel(r'$|R_n|$')

rh_plot = []
hk_plot = []
for i, ti in enumerate(tiaxis):
    ns = nvec(ti)
    ploti = np.where(np.abs(ns) <= 2)
    rh_plot.append(np.abs(r_RH[i, ploti]))
    hk_plot.append(np.abs(rhk[i, ploti]))

rh_plot = np.squeeze(np.array(rh_plot))
hk_plot = np.squeeze(np.array(hk_plot))

fig, ax = plt.subplots()
ax.set_title('Rayleigh Hypothesis reflection coefficents')
for ii in [-2, -1, 0, 1, 2]:
    ax.plot(np.degrees(tiaxis), rh_plot[:, ii],
            linewidth=3, label='$n$=%i'%ii)
ax.grid()
ax.set_ylim(-0.05, 1.2)
ax.legend()
ax.set_xlabel('incident angle, degrees')
ax.set_ylabel(r'$|R_n|$')

plt.show(block=False)
