import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp
from icepyks.poodle.cosine_surface import CosineSurface
from concurrent.futures import ThreadPoolExecutor

rh_name = 'cos_RH3D'

# For the FT of the test signal and xmission
NFFT = 2 ** 13
df = 1
frange = (500, 2500)  # generous bounds

# setup integration parameters
dx = 0.1
xbounds = (-300, 600)
# specify source and receiver location
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
# setup the wave specific solver for the HK approximation
hwave = 0.4
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1  # dB / km
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# make a signal interpolator
test_signal, tsig = pulse_signal.pulse_q1(1500)
s_ier_obj = signal_interp.ContinuousSignal(test_signal, tsig)
s_ier = lambda t: s_ier_obj(t, derivative=False)
s_ier_2d = lambda t: s_ier_obj.timeseries_2d(t)

# create a q vector of scattering orders
qvec = np.arange(-20, 21)
hie_numa = 300

xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
taxis = np.arange(NFFT) * df / NFFT
faxis = np.arange(NFFT) * df
fcompute = np.arange(frange[0], frange[1] + 1)

# load precomputed results
rh_data = np.load(rh_name + '.npz')
rhf = rh_data['allf']
faxis_rh = rh_data['fcompute']
#hie = np.load(hie_name)
#hief = rh_data['allf']
#faxis_hie = rh_data['fcompute']

# construct HK spectum
onef = lambda f: cos_ier.hk_spatial_3D(f, rsrc, rrcr, xaxis)
with ThreadPoolExecutor(max_workers=7) as executor:
    hkf = list(executor.map(onef, fcompute))

# take the fft of the xmitt signal
signalf = np.fft.fft(s_ier(taxis))

fig, ax = plt.subplots()
ax.plot(faxis, 20 * np.log10(np.abs(signalf)), linewidth=3, label='signal')
ax.plot(fcompute, 20 * np.log10(np.abs(hkf)), linewidth=3, label='HK')
ax.plot(faxis_rh, 20 * np.log10(np.abs(rhf)), linewidth=3, label='Rayleigh')
#ax.plot(faxis_hie, 20 * np.log10(np.abs(hief)), linewidth=2, label='HIE', color='C5')
ax.grid()
ax.legend()
ax.set_title('Fourier transform of surface scatter')
ax.set_xlabel('Frequency, Hz')
ax.set_ylabel('$|P(f)|$')
ax.set_xlim(frange[0], frange[1])
ax.set_ylim(-60, 20)
plt.show(block=False)


# create exact time series
#hie_ts, t_hie = cos_ier.make_timeseries(s_ier, hief, fcompute)
rh_ts, t_rh = cos_ier.make_timeseries(s_ier, rhf, fcompute)
hk_ts, t_hk = cos_ier.make_timeseries(s_ier, hkf, fcompute)

# create HK timseries directly
#hk_ts = cos_ier.hk_spatial_3D(rrcr, rsrc, xaxis, taxis, s_ier_2d)

# 4x upsample
#hie_ts_up, t_hie_up = resample(hie_ts, t_hie.size * 4, t=t_hie)
rh_ts_up, t_rh_up = resample(rh_ts, t_rh.size * 4, t=t_rh)
hk_ts_up, t_hk_up = resample(hk_ts, t_hk.size * 4, t=t_hk)

fig, ax = plt.subplots()
#ax.plot(t_hie_up, hie_ts_up, linewidth=2, label='HIE', color='C5')
ax.plot(t_hk_up * 1e3, hk_ts_up * 1e3, label='HK', linewidth=3)
ax.plot(t_rh_up * 1e3, rh_ts_up * 1e3, label='Rayleigh', linewidth=3)
ax.set_xlim(40, 46)
ax.set_ylim(-15, 15)
ax.grid()
ax.legend()
ax.set_title('Surface scatter timeseries')
ax.set_ylabel('pressure, mUnits')
ax.set_xlabel('time, ms')

plt.show(block=False)
