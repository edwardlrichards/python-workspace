import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp
from icepyks.poodle.cosine_surface import CosineSurface
from concurrent.futures import ThreadPoolExecutor

# setup the wave specific solver
hwave = 0.4
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1  # dB / km
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# specify source and receiver location
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])

# setup integration parameters
dx = 0.1
xbounds = (-15, 80)
xbounds = (-300, 600)
num_wave_axis = np.ceil(lwave / dx)
numa = 2 ** 16
ftest = 1050

# create the test signal and its fourier transform
test_signal, tsig = pulse_signal.pulse_q1(1500)
#frange = (500, 1000)  # generous bounds
#savename = 'f0'
#frange = (1001, 1500)  # generous bounds
#savename = 'f1'
#frange = (1501, 2000)  # generous bounds
#savename = 'f2'
frange = (500, 2500)  # generous bounds
savename = '5_25_f_07_17_17'

NFFT = 2 ** 13
df = 1

# make a signal interpolator
s_ier_obj = signal_interp.ContinuousSignal(test_signal, tsig)
s_ier = lambda t: s_ier_obj(t, derivative=False)
s_ier_2d = lambda t: s_ier_obj.timeseries_2d(t)

# create a q vector of scattering orders
qvec = np.arange(-20, 21)
hie_numa = 300

xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
per_xaxis = np.arange(num_wave_axis) / num_wave_axis * lwave
taxis = np.arange(NFFT) * df / NFFT
faxis = np.arange(NFFT) * df
fcompute = np.arange(frange[0], frange[1] + 1)

# construct hie reflection coefficent interpolator
rhk = lambda a0, freq: cos_ier.r_hk(a0, qvec, freq, return_bs=False)
rhie = cos_ier.rhie(per_xaxis, hie_numa, ftest, qvec)

hk = cos_ier.wn_2D(rhk, ftest, rsrc, rrcr, numa, qvec)
print('WN HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk), np.imag(hk)))

hie = cos_ier.wn_2D(rhie, ftest, rsrc, rrcr, numa, qvec)
print('WN HIE at %i Hz: %.6f + %.6fj'%(ftest, np.real(hie), np.imag(hie)))

# compare rcoefficents directly
atest = (np.arange(500) - 250) / 479
rhie_1 = rhie(atest, 0)
rhk_1 = rhk(atest, ftest)

#rplot = 18
#fig, ax = plt.subplots()
#ax.plot(atest, np.real(rhie_1[:, rplot]))
#ax.plot(atest, np.imag(rhie_1[:, rplot]))
#ax.set_prop_cycle(None)
#ax.plot(atest, np.real(rhk_1[:, rplot]), '--')
#ax.plot(atest, np.imag(rhk_1[:, rplot]), '--')
#plt.show(block=False)

tstart = time.time()
with ThreadPoolExecutor(max_workers=5) as executor:
    onef = lambda f: cos_ier.wn_2D(cos_ier.rhie(per_xaxis, hie_numa, f, qvec),
                                   f, rsrc, rrcr, numa, qvec)
    allf = list(executor.map(onef, fcompute))
tend = time.time()

np.savez(savename, fcompute=fcompute, allf=allf)

import sys; sys.exit("User break") # SCRIPT EXIT

onef = lambda f: cos_ier.hk_spatial_2D(f, rsrc, rrcr, xaxis)
# construct HK spectum
with ThreadPoolExecutor(max_workers=3) as executor:
    hkf = list(executor.map(onef, fcompute))
hkf = np.hstack([np.zeros(frange[0] - 1),
                    np.array(hkf),
                    np.zeros(NFFT // 2 + 1 - frange[1])])

# make fft spectrum of transmission
hief = np.hstack([np.zeros(frange[0] - 1),
                    np.array(allf),
                    np.zeros(NFFT // 2 + 1 - frange[1])])

nrfft = int(NFFT // 2 + 1)
fig, ax = plt.subplots()
ax.plot(faxis[: nrfft], np.abs(hkf))
ax.plot(faxis[: nrfft], np.abs(hief))
plt.show(block=False)

# take the fft of the xmitt signal
signal_ts = s_ier(taxis)
signal_f = np.conj(np.fft.rfft(signal_ts))
xmission_f = hief * signal_f
xmission_ts = np.fft.irfft(xmission_f)[::-1]

# Plot the spatial and wave number integral
hk_ts = cos_ier.hk_spatial_2D_time(rrcr, rsrc, xaxis, taxis, s_ier_2d)

# 4x upsample
xmission_ts_up, _ = resample(xmission_ts, taxis.size * 4, t=taxis)
hk_ts_up, t_up = resample(hk_ts, taxis.size * 4, t=taxis)

fig, ax = plt.subplots()
ax.plot(t_up, xmission_ts_up, label='HIE')
ax.plot(t_up, hk_ts_up, label='HK')
ax.set_xlim(0.035, 0.065)
ax.set_ylim(-0.03, 0.03)

plt.show(block=False)
