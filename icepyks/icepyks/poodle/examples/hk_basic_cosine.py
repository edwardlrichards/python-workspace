"""
Script to run the methods in hk_pw_synthesis.py
Compairs spatial and wavenumber integrals frequency by frequency, as well as
a time synthesis
"""
import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp
from icepyks.poodle import cosine_surface
from concurrent.futures import ThreadPoolExecutor

hwave = 0.4
lwave = 10.
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
ftest = 1050  # for single frequency calculations
# broadband frequency range
fcenter = 1500
frange = (500, 2500)  # generous bounds
qvec = np.arange(-20, 21)  # reflection coefficents used in pw synthesis

# integration specifications
df = 1
dx = 0.1
xbounds = (-15, 80)
numa = 2 ** 16

# transmitted signal
NFFT = 2 ** 13
test_signal = pulse_signal.pulse_q1(fcenter)

kwave = 2 * np.pi / lwave
cos_ier = cosine_surface.CosineSurface(hwave, kwave)

numx = 100
xaxis = np.arange(numx) / (numx - 1) * lwave

# _ = pulse_signal.plot_signal(test_signal)

# make a signal interpolator
s_ier_obj = signal_interp.ContinuousSignal(test_signal)
s_ier = lambda t: s_ier_obj(t, derivative=False)

xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
taxis = np.arange(NFFT) * df / NFFT
faxis = np.arange(NFFT / 2 + 1) * df
# only compute pw synthesis for a subset of the frequencies
fcompute = faxis[np.bitwise_and(faxis >= frange[0], faxis <= frange[1])]

hk = cos_ier.hk_spatial_2D(ftest, rsrc, rrcr, xaxis)
print('Spatial HK at %i Hz: %.5f + %.5fj'%(ftest, np.real(hk), np.imag(hk)))

r_func = lambda a0, freq: cos_ier.r_hk(a0, qvec, freq)
hk = cos_ier.hk_wn_2D(r_func, ftest, rsrc, rrcr, numa)
print('WN HK at %i Hz: %.5f + %.5fj'%(ftest, np.real(hk), np.imag(hk)))

import sys; sys.exit("User break") # SCRIPT EXIT

# plot the integrand
bounds = 1.1
numpoints = numa
aaxis = (np.arange(numpoints) - numpoints / 2)\
        * bounds / (numpoints - 1) * 2
da = aaxis[1] - aaxis[0]
sw = lambda p, b: p / b
pwsynth = lambda f, r_out: cos_ier.p_ref(r_out[0], r_out[1],
                                    f, rsrc, rrcr)
pff = sw(*pwsynth(ftest, r_func(aaxis, ftest)))

tstart = time.time()
with ThreadPoolExecutor(max_workers=7) as executor:
    onef = lambda f: hk_pw_synthesis.hk_wn_2D(f, aaxis)
    allf = list(executor.map(onef, fcompute))

tend = time.time()
print('Elasped time is %.2f seconds'%(tend-tstart))

# make real fft spectrum of transmission
xmitt_f = np.hstack([np.zeros(frange[0] - 1),
                      np.array(allf),
                      np.zeros(NFFT // 2 + 1 - frange[1])])

# take the fft of the xmitt signal
signal_ts = s_ier(taxis)
signal_f = np.fft.rfft(signal_ts)
# shift signal by length of signal
t_max = float(np.max(test_signal.time))
signal_f = signal_f * np.exp(1j * 2 * np.pi * faxis * t_max)

xmission_f = xmitt_f * signal_f
# Conjugate because we are not truely doing an ifft, but rather multiplying
# in the time dependence.
xmission_ts = -np.fft.irfft(np.conj(xmission_f))
# 4x upsample
xmission_ts_up, _ = resample(xmission_ts, taxis.size * 4, t=taxis)

# Plot the spatial and wave number integral

hk_ts = hk_pw_synthesis.hk_spatial_2D_time(xaxis, taxis, s_ier)
# 4x upsample
hk_ts_up, t_up = resample(hk_ts, taxis.size * 4, t=taxis)

fig, ax = plt.subplots()
#ax.plot(taxis, hk_ts)
#ax.plot(taxis, xmission_ts)
ax.plot(t_up, hk_ts_up)
ax.plot(t_up, xmission_ts_up)
ax.set_xlim(0.035, 0.065)
ax.set_ylim(-0.03, 0.03)

plt.show(block=False)
