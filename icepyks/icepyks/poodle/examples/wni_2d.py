import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import RectBivariateSpline
from icepyks.poodle import cosine_surface

def make_theta_axis(num_real, eva_range, is_half=True):
    """Make a theta axis that extends into a small part of imaginary space
    is_half: axis range: (0 + j eva_range, np.pi / 2)
                if not, axis range: (0 + j eva_range, np.pi - j eva_range)
    """
    if is_half:
        max_val = np.pi / 2
    else:
        max_val = np.pi
    theta_axis = np.linspace(0, max_val, num_real)
    dth = theta_axis[1] - theta_axis[0]
    eva = np.arange(dth, eva_range, dth) * 1j
    if is_half:
        theta_axis = np.hstack([eva[::-1], theta_axis])
    else:
        theta_axis = np.hstack([eva[::-1], theta_axis, np.pi - eva])
    return theta_axis

f_acous = 4500.
k_acous = 2 * np.pi * f_acous / 1500
num_theta = 5000
num_phi = 2000

r_src = np.array([0., 0, -20])
r_rcr = np.array([200., 0, -10])
r_img = np.sqrt((r_src[0] - r_rcr[0]) ** 2 + (r_src[-1] + r_rcr[-1]) ** 2)

#theta_axis = np.arange(num_theta) / num_theta * np.pi
theta_axis = make_theta_axis(num_theta, 0.3, is_half=False)
phi_axis = (np.arange(num_phi) / num_phi - 0.5) * np.pi

dtheta = np.diff(theta_axis)
dtheta = np.hstack([dtheta, dtheta[-1]])
dphi = np.abs(phi_axis[1] - phi_axis[0])

phase = np.exp(1j * k_acous
        * (np.sin(phi_axis)[:, None] * np.cos(theta_axis)[None, :]
           * (r_rcr[0] - r_src[0])
           + np.sin(theta_axis) * np.abs(r_rcr[-1] + r_src[-1])))

wn = -1j * k_acous / (8 * np.pi ** 2)\
     * np.sum(phase * np.abs(np.cos(theta_axis)[None, :]) * dtheta)\
     * dphi

sc_wn = wn * (4 * np.pi * r_img)

image = -np.exp(1j * k_acous * r_img)

print('wavenumber synthesis gives   %.5f + j%.5f'%
        (np.real(sc_wn), np.imag(sc_wn)))
print('image result is              %.5f + j%.5f'%
        (np.real(image), np.imag(image)))

# perform wni with a cosine surface
hwave = 2.
lwave = 40.

cos_er = cosine_surface.CosineSurface(hwave, 2 * np.pi / lwave)
krange = (np.arange(200) + 1) / 200 * 2 * np.pi * f_acous / 1500
r0 = []
for k in krange:
    f = k * 1500 / (2 * np.pi)
    r0.append(cos_er.r_hk(np.cos(theta_axis), np.array([0]), f, return_bs=False))

r0 = np.array(r0)
# create 2d interpolator of reflection coefficents
x = np.real(np.cos(theta_axis))[:: -1]
r0_r_ier = RectBivariateSpline(krange, x, np.real(r0[:, :: -1]))
r0_i_ier = RectBivariateSpline(krange, x, np.imag(r0[:, :: -1]))
