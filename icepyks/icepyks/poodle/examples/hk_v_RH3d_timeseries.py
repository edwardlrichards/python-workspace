import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp
from icepyks.poodle.cosine_surface import CosineSurface
from concurrent.futures import ThreadPoolExecutor

# setup the wave specific solver
hwave = 0.4
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# specify source and receiver location
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])

# setup integration parameters
dx = 0.1
xbounds = (-15, 80)
xbounds = (-300, 600)
num_wave_axis = np.ceil(lwave / dx)
numa = 2 ** 16
ftest = 1500

# create the test signal and its fourier transform
test_signal, tsig = pulse_signal.pulse_q1(1500)
frange = (500, 2500)  # generous bounds
savename = 'cos_2height_RH3D'
isexecute = True  # True calculates coefficents, False loads pre-calculated

NFFT = 2 ** 13
df = 1

# make a signal interpolator
s_ier_obj = signal_interp.ContinuousSignal(test_signal, tsig)
s_ier = lambda t: s_ier_obj(t, derivative=False)
s_ier_half = lambda t: s_ier_obj.half_derivative(t)

# create a q vector of scattering orders
qvec = np.arange(-30, 30)
rinterp_numa = 200

xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
per_xaxis = np.arange(num_wave_axis) / num_wave_axis * lwave
taxis = np.arange(NFFT) * df / NFFT
faxis = np.arange(NFFT) * df
fcompute = np.arange(frange[0], frange[1] + 1)

# construct hie reflection coefficent interpolator
rhk = lambda a0, freq: cos_ier.r_hk(a0, qvec, freq, return_bs=False)

def RH_interper(ftest):
    """Solve for reflection coefficents and create an interpolator"""
    aaxis = np.arange(rinterp_numa) / rinterp_numa
    r_RH = []
    for a in aaxis:
        r_RH.append(cos_ier.r_RH(a, qvec, ftest))
    r_RH = np.array(r_RH)
    r_RH = np.vstack([r_RH[::-1, ::-1], r_RH[1:, :]])
    aaxis = np.hstack([-aaxis[::-1], aaxis[1: ]])
    return cos_ier.create_rinterp(aaxis, r_RH, qvec)

hk = cos_ier.wn_3D(rhk, ftest, rsrc, rrcr, numa, qvec)
print('WN HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk), np.imag(hk)))

rh = cos_ier.wn_3D(RH_interper(ftest), ftest, rsrc, rrcr, numa, qvec)
print('WN RH at %i Hz: %.6f + %.6fj'%(ftest, np.real(rh), np.imag(rh)))
