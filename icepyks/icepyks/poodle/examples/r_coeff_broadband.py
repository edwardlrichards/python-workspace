import numpy as np
import matplotlib.pyplot as plt
import time
from cycler import cycler
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp
from icepyks.poodle import cosine_surface
from concurrent.futures import ThreadPoolExecutor

hwave = 0.4
lwave = 10.
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
ftest = 1050
titest = np.linspace(0.5, 80, 200) * np.pi / 180
numx = 100
numqs = 10  # 1/2 number of calculated reflection coefficents

kwave = 2 * np.pi / lwave
cos_ier = cosine_surface.CosineSurface(hwave, kwave, attn=0)

# test convergence of incident pressure
katest = 2 * np.pi * ftest / 1500

xaxis = np.arange(numx) / numx * lwave

# get the requested reflection coefficents
qvec = np.arange(-numqs, numqs + 1)
rhk = []
rhie = []
ehk = []
ehie = []

for ti in titest:
    a0 = np.cos(ti)
    #HK calculations
    hk_per = 2 * cos_ier.dpinc(xaxis, a0, ftest, is_per=True)
    rtemp, bs = cos_ier.r_psi(xaxis, hk_per, a0, qvec, ftest)
    rhk.append(rtemp)
    ehk.append(cos_ier.r_energy(rtemp, bs[2], bs[3], bs[1]))
    # HIE calculations
    hie_per = cos_ier.pn_surf_1st(xaxis, a0, ftest, num_terms=45)
    rtemp, bs = cos_ier.r_psi(xaxis, hie_per, a0, qvec, ftest)
    rhie.append(rtemp)
    ehie.append(cos_ier.r_energy(rtemp, bs[2], bs[3], bs[1]))

rhk = np.array(rhk)
rhie = np.array(rhie)
ehk = np.array(ehk)
ehie = np.array(ehie)

iplot = np.where(np.abs(qvec) <= 1)[0]
#iplot = np.ones(qvec.shape, dtype=np.bool_)

fig, (ax, ax1) = plt.subplots(2, 1, gridspec_kw = {'height_ratios':[2, 1]} )
is1 = True
for i in iplot:
    if is1:
        ax.plot(np.degrees(titest), np.abs(rhk[:, i]), '--', label='R -1, HK')
        is1 = False
    else:
        ax.plot(np.degrees(titest), np.abs(rhk[:, i]), '--', label='')

ax.set_prop_cycle(None)
labels = ['R -1, HIE', 'R 0', 'R 1']

for i, l in zip(iplot, labels):
        ax.plot(np.degrees(titest), np.abs(rhie[:, i]), label=l)

ax.set_title('Reflection coefficents')
ax.grid()
ax.legend()
ax.set_xticklabels('')
ax.set_ylim(0, 2)
ax.set_ylabel(r'$|R|$')

ax1.set_title('Energy conservation')
# draw the brewster angles
brew = [1, 2, 3]
tib = [np.degrees(np.real(np.arccos(1 - b * kwave / cos_ier.kacous(ftest))))
       for b in brew]
for t in tib:
    ax1.annotate("",
                xy=(t, 1.3), xycoords='data',
                xytext=(t, 1.45), textcoords='data',
                arrowprops=dict(arrowstyle="->",
                                connectionstyle="arc3"),
                )

ax1.plot(np.degrees(titest), ehk, label='HK')
ax1.plot(np.degrees(titest), ehie, label='HIE')
ax1.set_ylim(0.7, 1.5)
ax1.grid()
ax1.legend()
ax1.set_xlabel('incident angle, degrees')

plt.show(block=False)
