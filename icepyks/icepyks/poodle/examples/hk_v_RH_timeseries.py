import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp
from icepyks.poodle.cosine_surface import CosineSurface
from concurrent.futures import ThreadPoolExecutor

# setup the wave specific solver
hwave = 0.4
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1  # dB / km
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# specify source and receiver location
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])

# setup integration parameters
dx = 0.1
xbounds = (-15, 80)
xbounds = (-300, 600)
num_wave_axis = np.ceil(lwave / dx)
numa = 2 ** 16
ftest = 1050

# create the test signal and its fourier transform
test_signal, tsig = pulse_signal.pulse_q1(1500)
frange = (500, 2500)  # generous bounds
savename = 'cos_RH2D'
isexecute = False  # True calculates coefficents, False loads pre-calculated

NFFT = 2 ** 13
df = 1

# make a signal interpolator
s_ier_obj = signal_interp.ContinuousSignal(test_signal, tsig)
s_ier = lambda t: s_ier_obj(t, derivative=False)
s_ier_2d = lambda t: s_ier_obj.timeseries_2d(t)

# create a q vector of scattering orders
qvec = np.arange(-20, 21)
rinterp_numa = 300

xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
per_xaxis = np.arange(num_wave_axis) / num_wave_axis * lwave
taxis = np.arange(NFFT) * df / NFFT
faxis = np.arange(NFFT) * df
fcompute = np.arange(frange[0], frange[1] + 1)

# construct hie reflection coefficent interpolator
rhk = lambda a0, freq: cos_ier.r_hk(a0, qvec, freq, return_bs=False)

def RH_interper(ftest):
    """Solve for reflection coefficents and create an interpolator"""
    aaxis = np.arange(rinterp_numa) / rinterp_numa
    r_RH = []
    for a in aaxis:
        r_RH.append(cos_ier.r_RH(a, qvec, ftest))
    r_RH = np.array(r_RH)
    r_RH = np.vstack([r_RH[::-1, ::-1], r_RH[1:, :]])
    aaxis = np.hstack([-aaxis[::-1], aaxis[1: ]])
    return cos_ier.create_rinterp(aaxis, r_RH, qvec)

hk = cos_ier.wn_2D(rhk, ftest, rsrc, rrcr, numa, qvec)
print('WN HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk), np.imag(hk)))

hie = cos_ier.wn_2D(RH_interper(ftest), ftest, rsrc, rrcr, numa, qvec)
print('WN RH at %i Hz: %.6f + %.6fj'%(ftest, np.real(hie), np.imag(hie)))
import sys; sys.exit("User break") # SCRIPT EXIT

if isexecute:
    tstart = time.time()
    with ThreadPoolExecutor(max_workers=5) as executor:
        onef = lambda f: cos_ier.wn_2D(RH_interper(f),
                                    f, rsrc, rrcr, numa, qvec)
        allf = list(executor.map(onef, fcompute))
    tend = time.time()
    print('Elasped time is %.2f seconds'%(tend-tstart))
    np.savez(savename, fcompute=fcompute, allf=allf)
else:
    rh_data = np.load(savename + '.npz')
    allf = rh_data['allf']

# create a timeseries from the fft of the xmitt signal
rh_ts, t_rh = cos_ier.make_timeseries(s_ier, allf, fcompute)
rh_ts_up, rh_t_up = resample(rh_ts, t_rh.size * 4, t=t_rh)

onef = lambda f: cos_ier.hk_spatial_2D(f, rsrc, rrcr, xaxis)
# construct HK spectum
with ThreadPoolExecutor(max_workers=3) as executor:
    hkf = list(executor.map(onef, fcompute))

fig, ax = plt.subplots()
ax.plot(fcompute, np.abs(hkf), label='HK')
ax.plot(fcompute, np.abs(allf), label='RH')
ax.grid()
ax.legend()

# Plot the spatial and wave number integral
hk_ts = cos_ier.hk_spatial_2D_time(rrcr, rsrc, xaxis, taxis, s_ier_2d)

# 4x upsample
hk_ts_up, t_up = resample(hk_ts, taxis.size * 4, t=taxis)

fig, ax = plt.subplots()
ax.plot(t_up * 1e3, hk_ts_up, label='HK')
ax.plot(rh_t_up * 1e3, rh_ts_up, label='RH')
ax.set_xlabel('time, ms')
ax.set_ylabel('$p_{sc}$')
ax.set_xlim(38, 48)
ax.set_ylim(-0.03, 0.03)
ax.grid()
ax.legend()

plt.show(block=False)
