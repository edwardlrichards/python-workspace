"""
================================
Reflection coefficent comparison
================================
Comparison of reflection coefficents computed with direct methods; Rayleigh
Hypothesis (RH), Helmholtz integral equations (HIE) (1st and 2nd kind),
and the Kirchhoff approximation (HKA).
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks.poodle import plane_wave_rs

hwave = 2.
lwave = 40.
ftest = 500
titest = 15 * np.pi / 180
numqs = 8  # number of evanescent orders
tiaxis = np.linspace(0.01, 90, 500) * np.pi / 180

kwave = 2 * np.pi / lwave
cos_ier = plane_wave_rs.CosineReflectionCoefficents(hwave, kwave, attn=0)

xdec = 30 
dx = 1500 / (xdec * ftest)
xaxis = np.arange(0, lwave, dx)

kx = cos_ier.kacous(ftest) * np.cos(titest)

#HK calculations
qtest = cos_ier.qvec(numqs, kx, ftest)
r_HK = cos_ier.r_HK(kx, qtest, ftest)
psi_HK = 2 * cos_ier.psi_inc(xaxis, kx, ftest)

# Rayleigh hypothesis
r_RH = cos_ier.r_RH(kx, qtest, ftest)

# compute field at surface
psi_RH = cos_ier.back_project(r_RH, qtest, xaxis, kx, ftest) \
        + cos_ier.psi_inc(xaxis, kx, ftest)

# HIE calculations
psi_HIE1 = cos_ier.psi_HIE_1st(xaxis, kx, ftest, num_eva=numqs)
psi_HIE2 = cos_ier.psi_HIE_2nd(xaxis, kx, ftest, num_eva=numqs)

# compute reflection coefficents
r_HIE1 = cos_ier.r_psi(xaxis, psi_HIE1, kx, qtest, ftest)
r_HIE2 = cos_ier.r_psi(xaxis, psi_HIE2, kx, qtest, ftest)

# energy conservation calculations
ehk = cos_ier.r_energy(r_HK, kx, qtest, ftest)
e_RH = cos_ier.r_energy(r_RH, kx, qtest, ftest)
e_HIE1 = cos_ier.r_energy(r_HIE1, kx, qtest, ftest)
e_HIE2 = cos_ier.r_energy(r_HIE2, kx, qtest, ftest)

print('HKA Energy conservation at %.1f degrees: %.8f'%
      (titest * 180 / np.pi, ehk))
print('RH Energy conservation at %.1f degrees: %.8f'%
      (titest * 180 / np.pi, e_RH))
print('1st HIE Energy conservation at %.1f degrees: %.8f'%
      (titest * 180 / np.pi, e_HIE1))
print('2nd HIE Energy conservation at %.1f degrees: %.8f'%
      (titest * 180 / np.pi, e_HIE2))

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(xaxis, np.real(psi_HK), label='HK')
axes[0].plot(xaxis, np.real(psi_HIE1), label='1st kind')
axes[0].plot(xaxis, np.real(psi_RH), '.-', markevery=5, label='RH')
axes[0].plot(xaxis, np.real(psi_HIE2), '--', label='2nd kind')
axes[0].legend()
axes[0].grid()

axes[1].plot(xaxis, np.imag(psi_HK))
axes[1].plot(xaxis, np.imag(psi_HIE1))
axes[1].plot(xaxis, np.imag(psi_RH), '.-', markevery=5)
axes[1].plot(xaxis, np.imag(psi_HIE2), '--')
axes[1].grid()

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(qtest, np.real(r_HK), '.', label='HK')
axes[0].plot(qtest, np.real(r_HIE1), '.', label='HIE1')
axes[0].plot(qtest, np.real(r_RH), '.', label='RH')
axes[0].plot(qtest, np.real(r_HIE2), '.', label='HIE2')
axes[0].legend()
axes[0].grid()
axes[0].set_title('Real part, ti = %.1f degrees'%(titest * 180 / np.pi))

axes[1].plot(qtest, np.imag(r_HK), '.')
axes[1].plot(qtest, np.imag(r_HIE1), '.')
axes[1].plot(qtest, np.imag(r_RH), '.')
axes[1].plot(qtest, np.imag(r_HIE2), '.')
axes[1].grid()
axes[1].set_xlabel('coefficent number')
axes[1].set_title('Imaginary')

axes[1].set_xlim(-6, 6)

plt.show(block=False)
