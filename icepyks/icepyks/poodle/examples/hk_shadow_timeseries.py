import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hilbert, resample
# import direct iso-speed solver
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp
# import numerical tracer
from icepyks.clumber import isospeed_integral, iso_rg, ray_field,\
                            point_iso_field
# used only for broadband synthesis
from icepyks.poodle.cosine_surface import CosineSurface

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 10
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 0.4
c0 = 1500
z_source = -15
r_receiver = np.array((55., -10))
r_buffer = 20.
z_const = -(wave_amp + 0.1)
z_bottom = -30

#signal parameter
fc = 1.5e3
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# used only for synthesis
cos_ier = CosineSurface(wave_amp * 2, 2 * np.pi / lamda_surf, attn=0.)

# flat surface results
ri = np.sqrt(r_receiver[0] ** 2 + (z_source + r_receiver[1]) ** 2) 
t_spec = ri / c0
direct_amp = 1 / np.sqrt(r_receiver[0] ** 2 + (z_source - r_receiver[1]) ** 2)

# create integration bounds
x_bounds = (-r_buffer, r_receiver[0] + r_buffer)
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3,
            np.ceil(t_spec * 1e3 + 8) / 1e3)
fs = 5e5
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# setup surface
eta = sin_surface.Sine(lamda_surf, wave_amp, phase=np.pi/2)

# ray solution
ray_field = ray_field.RayField(eta, z_source, r_receiver, tt_tol=5e-7,
                               range_buffer=40)
ray_gun = iso_rg.Isospeed(c0, z_bottom, z_const)
ray_field.to_top(ray_gun)
ray_field.to_wave()
ray_HK = isospeed_integral.TimeHK(ray_field, xaxis, taxis, i_er_signal)
ray_result = ray_HK(r_receiver)

# isospeed solution
iso_field = point_iso_field.IsospeedPoint(eta, z_source)
isospeed_HK = isospeed_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal)
iso_result = isospeed_HK(r_receiver)

# image source
img_ts = -i_er_signal(taxis - ri / 1500) / ri

# RH solution
rh_4amp = np.load('cos_8height_RH3D.npz')
rh_4ts, t_rh4 = cos_ier.make_timeseries(i_er_signal,
                                         rh_4amp['allf'],
                                         rh_4amp['fcompute'])
rh_4ts_up, t_rh4_up = resample(rh_4ts, t_rh4.size * 4, t=t_rh4)

# differences between solutions
d1 = np.interp(taxis, t_rh4_up, rh_4ts_up) + iso_result
d2 = np.interp(taxis, t_rh4_up, rh_4ts_up) + ray_result

fig, axes = plt.subplots(2, 1, sharex=True)
#ax.plot(taxis * 1e3, img_ts * 1e3, color='C4', label='image')
axes[0].plot(t_rh4_up * 1e3, rh_4ts_up * 1e3, color='C4', label='RH')
axes[0].plot(taxis * 1e3, -iso_result * 1e3, color='C1', label='naive')
axes[0].plot(taxis * 1e3, -ray_result * 1e3, color='C2', label='shadow')
axes[0].set_xlim(40, 50)
axes[0].set_ylim(-16, 16)
axes[1].set_xlabel('delay, ms')
axes[0].set_title('Comparison of solutions, 0.4 m amplitude wave')
axes[0].legend()
axes[0].grid()

axes[1].set_title('Difference from Rayleigh Hypothesis')
axes[1].plot(taxis * 1e3, d1 * 1e3, color='C1', label='naive')
axes[1].plot(taxis * 1e3, d2 * 1e3, color='C2', label='shadow')
axes[1].grid()
axes[1].set_ylim(-4, 4)

plt.show(block=False)
