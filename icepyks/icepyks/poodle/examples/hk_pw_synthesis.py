"""Framework for plane wave synthesis shown for the HK approximation"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import jn, hankel1

hwave = 0
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1  # dB / km
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
tspec = np.sqrt((rrcr[0] - rsrc[0]) ** 2 + (rrcr[1] + rsrc[1]) ** 2) / 1500

delta = lambda f: 1j * attn / 8686 * 1500 / 2 / np.pi / f
kacous = lambda f: 2 * np.pi * f / 1500

# surface profile coordinates
eta = lambda x: np.array([np.array(x, ndmin=1),
                          hwave * np.cos(kwave * np.array(x, ndmin=1))])
# surface gradient vector
gf = lambda x: np.array([hwave * kwave * np.sin(kwave * np.array(x, ndmin=1)),
                         np.ones_like(np.array(x, ndmin=1))])

def hk_r(a0, q, freq, return_bs=False):
    """Return the q-th reflection coefficent, HK approximation"""
    a0 = np.array(a0, ndmin=1)
    q = np.array(q, ndmin=1)
    k = kacous(freq)
    d = delta(freq)
    b0 = np.sqrt((1 + d) ** 2 - a0 ** 2)
    # output is [numa0, numq]
    aq = a0[:, None] + q[None, :] * kwave / k
    bq = np.sqrt((1 + d) ** 2 - aq ** 2)
    rs = -1j ** q * (a0[:, None] * (aq - a0[:, None])\
         - b0[:, None] * (b0[:, None] + bq))\
         / (bq * (b0[:, None] + bq)) \
         * jn(q, hwave * k * (b0[:, None] + bq))
    if return_bs:
        return np.squeeze(rs), (aq, b0, bq)
    else:
        return np.squeeze(rs)

def p_ref(a0, freq, return_b0=False):
    """Synthesize plane waves for reflected pressure amplitude"""
    qrange = np.arange(-20, 21)
    k = kacous(freq) * (1 + delta(freq))
    rs, (aq, b0, bq) = hk_r(a0, qrange, freq, return_bs=True)
    rs = np.array(rs, ndmin=2)
    aq = np.array(aq, ndmin=2)
    bq = np.array(bq, ndmin=2)
    a0 = np.array(a0, ndmin=1)
    b0 = np.array(b0, ndmin=1)
    dist = np.sum(np.array([aq, -bq]) * rrcr[:, None, None], axis=0)\
            - np.sum(np.array([a0, b0]) * rsrc[:, None], axis=0)[:, None]
    p_ref = np.sum(rs * np.exp(1j * np.real(k) * dist), axis=-1)
    if return_b0:
        return np.squeeze(p_ref), b0
    else:
        return np.squeeze(p_ref)

# 2D HK for 1 frequency
def hk_spatial_2D(freq, xaxis):
    """Perform the spatial 2D HK integral"""
    rvec = eta(xaxis) - rsrc[:, None]
    r1 = np.linalg.norm(rvec, axis=0)
    r2 = np.linalg.norm(rrcr[:, None] - eta(xaxis), axis=0)
    dx = xaxis[1] - xaxis[0]
    # put attenuation directly into the wavenumber
    k = kacous(freq) * (1 + delta(freq))
    # use far field hankel approximation
    igrand = np.exp(1j * k * (r1 + r2)) * np.sum(gf(xaxis) * rvec, axis=0)\
            / r1 ** (3 / 2) / np.sqrt(r2)
    hk = 1j / np.pi * np.sum(igrand) * dx
    return hk

def make_aaxis(numpoints, bounds=1.1):
    """Make an evenly spaced aaxis"""
    aaxis = (np.arange(numpoints) - numpoints / 2)\
            * bounds / (numpoints - 1) * 2
    return aaxis

def hk_wn_2D(freq, aaxis):
    """Perform the wavenumber 2D HK integral"""
    da = aaxis[1] - aaxis[0]
    p, b = p_ref(aaxis, freq, return_b0=True)
    hk = np.sum(p / b) * da / np.pi
    return hk

# 2D time series HK
def hk_spatial_2D_time(xaxis, taxis, signal_interper):
    """Perform the spatial 2D HK integral"""
    rvec = eta(xaxis) - rsrc[:, None]
    r1 = np.linalg.norm(rvec, axis=0)
    r2 = np.linalg.norm(rrcr[:, None] - eta(xaxis), axis=0)
    dx = xaxis[1] - xaxis[0]
    tau = (r1 + r2) / 1500
    delays = taxis - tau[:, None]
    igrand = np.sum(gf(xaxis) * rvec, axis=0) / r1 ** (3 / 2) / np.sqrt(r2)
    sval = signal_interper(delays)
    hk = 1 / np.pi * np.sum(igrand[:, None] * sval, axis=0) * dx
    return hk
