import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from scipy.interpolate import interp1d
from icepyks import pulse_signal, signal_interp
from icepyks.poodle.cosine_surface import CosineSurface
from concurrent.futures import ThreadPoolExecutor

# setup the wave specific solver
hwave = 0.4
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1  # dB / km
# create a q vector of scattering orders
qvec = np.arange(-20, 21)
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# specify source and receiver location
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])

# setup integration parameters
dx = 0.1
xbounds = (-15, 80)
xbounds = (-300, 600)
numa = 2 ** 16
numqs = 10  # number of evanscent orders
ftest = 1050
aaxis = np.arange(numa) / (numa - 1)

xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]

# get the requested reflection coefficents
nvec = lambda a, f: np.arange(np.ceil(-(2 * np.pi * f / 1500)
                                      * katest * lwave / 2 / np.pi
                                      * (1 + a)) - numqs, 
                              np.floor((2 * np.pi * f / 1500)
                                       * lwave / 2 / np.pi
                                       * (1 - a)) + numqs);



hk = cos_ier.hk_spatial_2D(ftest, rsrc, rrcr, xaxis)
print('Spatial HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk), np.imag(hk)))

r_hks = cos_ier.r_hk(aaxis[:-1], ftest, return_bs=False)
fill_value = np.zeros(cos_ier.qs.size)
fill_value[cos_ier.qs == 0] = 1
# add theta = 0  and some evanescent tail to the integration

aint = np.arange(numa) / numa * 2 * (np.max(aaxis) - 0.002)
aint = aint - np.mean(aint)
dai = aint[1] - aint[0]

bs = cos_ier._bragg_angles(aint, cos_ier.qs, ftest)
r_interp = interp1d(aaxis, r_hks, axis=0, bounds_error=False,
                    fill_value=fill_value)
rvec = r_interp(aint)
p, b = cos_ier.p_ref(rvec, bs, ftest, rsrc, rrcr)

hk_wi = np.sum(p / b) * dai / np.pi

#hk_wi = cos_ier.wn_2D(rfunc, ftest, rsrc, rrcr, numa)
print('WN HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk_wi), np.imag(hk_wi)))

import sys; sys.exit("User break") # SCRIPT EXIT


rfunc = lambda a0, freq: cos_ier.r_hk(a0, freq, return_bs=False)
tstart = time.time()
with ThreadPoolExecutor(max_workers=4) as executor:
    onef = lambda f: cos_ier.wn_2D(rfunc, f, rsrc, rrcr, numa)
    allf = list(executor.map(onef, fcompute))
tend = time.time()


# make fft spectrum of transmission
xmitt_f = np.hstack([np.zeros(frange[0] - 1),
                      np.array(allf),
                      np.zeros(NFFT // 2 + 1 - frange[1])])
xmitt_f = np.hstack([xmitt_f[:-1], np.conj(xmitt_f[1:][::-1])])

# take the fft of the xmitt signal
signal_ts = s_ier(taxis)
signal_f = np.fft.ifft(signal_ts)
# shift signal by length of signal
t_max = float(np.max(test_signal.time))
tshift = 0.003
#signal_f = signal_f * np.exp(-1j * 2 * np.pi * faxis *  t_max)

xmission_f = xmitt_f * signal_f
# Conjugate because we are not truely doing an ifft, but rather multiplying
# in the time dependence.
xmission_ts = np.fft.fft(xmission_f)
# 4x upsample
xmission_ts_up, _ = resample(xmission_ts, taxis.size * 4, t=taxis)

# Plot the spatial and wave number integral
hk_ts = cos_ier.hk_spatial_2D_time(rrcr, rsrc, xaxis, taxis, s_ier)
# 4x upsample
hk_ts_up, t_up = resample(hk_ts, taxis.size * 4, t=taxis)

fig, ax = plt.subplots()
#ax.plot(taxis, hk_ts)
#ax.plot(taxis, xmission_ts)
ax.plot(t_up * 1e3, np.imag(hk_ts_up) * 1e3, linewidth=3, alpha=0.6, label='spatial HK')
ax.plot(t_up * 1e3, xmission_ts_up * 1e3, label='Wavenumber HK')
ax.set_xlim(39, 47)
ax.set_ylim(35, -30)
ax.set_ylabel('pressure, mPa')
ax.set_xlabel('time, ms')
ax.set_title('surface scatter time series, 2D')
ax.grid()
ax.legend()

plt.show(block=False)
