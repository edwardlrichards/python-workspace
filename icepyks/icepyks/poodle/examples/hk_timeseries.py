import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp, f_synthesis
from icepyks.poodle.cosine_surface import CosineSurface
from concurrent.futures import ThreadPoolExecutor

# setup the wave specific solver
hwave = 0.4
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1  # dB / km
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# specify source and receiver location
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])

# setup integration parameters
dx = 0.1
xbounds = (-15, 80)
xbounds = (-300, 600)
numa = 2 ** 16
ftest = 1050

# create the test signal and its fourier transform
test_signal, tsig = pulse_signal.pulse_q1(1500)
frange = (500, 2500)  # generous bounds
NFFT = 2 ** 13
df = 1

# should we compute wave number HK coefficents
savename = 'cos_2dHK'
isexecute = False  # True calculates coefficents, False loads pre-calculated

# make a signal interpolator
s_ier_obj = signal_interp.ContinuousSignal(test_signal, tsig)
s_ier = lambda t: s_ier_obj(t)
s_ier_2d = lambda t: s_ier_obj.signal_2d_ts(t)

# create a q vector of scattering orders
qvec = np.arange(-20, 21)

xaxis = np.arange(np.ceil((xbounds[1] - xbounds[0]) / dx))\
        * dx + xbounds[0]
taxis = np.arange(NFFT) * df / NFFT
faxis = np.arange(NFFT) * df
fcompute = np.arange(frange[0], frange[1] + 1)

hk = cos_ier.hk_spatial_2D(ftest, rsrc, rrcr, xaxis)
print('Spatial HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk), np.imag(hk)))

rfunc = lambda a0, freq: cos_ier.r_hk(a0, qvec, freq, return_bs=False)
hk = cos_ier.wn_2D(rfunc, ftest, rsrc, rrcr, numa, qvec)
print('WN HK at %i Hz: %.6f + %.6fj'%(ftest, np.real(hk), np.imag(hk)))

onef = lambda f: cos_ier.hk_spatial_2D(f, rsrc, rrcr, xaxis)
tstart = time.time()
with ThreadPoolExecutor(max_workers=3) as executor:
    allf = list(executor.map(onef, fcompute))
tend = time.time()

hk_sp, hk_sp_t = f_synthesis.synthesize_ts(allf, s_ier, fcompute)
hk_sp_up, hk_sp_t_up = resample(hk_sp, hk_sp_t.size * 4, t=hk_sp_t)

if isexecute:
    onef = lambda f: cos_ier.wn_2D(rfunc, f, rsrc, rrcr, numa, qvec)
    tstart = time.time()
    with ThreadPoolExecutor(max_workers=3) as executor:
        allf = list(executor.map(onef, fcompute))
    tend = time.time()
    print('Elasped time is %.2f seconds'%(tend-tstart))
    np.savez(savename, fcompute=fcompute, allf=allf)
else:
    hk_data = np.load(savename + '.npz')
    allf = hk_data['allf']

hk_wn, hk_wn_t = f_synthesis.synthesize_ts(allf, s_ier, fcompute)
hk_wn_up, hk_wn_t_up = resample(hk_wn, hk_wn_t.size * 4, t=hk_wn_t)

# Plot the spatial and wave number integral
hk_ts = cos_ier.hk_spatial_2D_time(rrcr, rsrc, xaxis, taxis, s_ier_2d)

# 4x upsample
hk_td, hk_t = resample(hk_ts, taxis.size * 4, t=taxis)
hk_td_up, hk_t_up = resample(hk_td, hk_t.size * 4, t=hk_t)
#hk_wn_xform_up, _ = resample(hk_wn_xform, taxis.size * 4, t=taxis)

fig, ax = plt.subplots()
ax.plot(hk_t_up * 1e3, hk_td_up * 1e3, linewidth=3, label='spatial HK')
ax.plot(hk_sp_t_up * 1e3, hk_sp_up * 1e3, linewidth=3, label='spatial HK, Xform')
ax.plot(hk_wn_t_up * 1e3, hk_wn_up * 1e3, linewidth=3, label='wn HK, Xform')
ax.set_xlim(39, 47)
ax.set_ylim(-45, 40)
ax.set_ylabel('pressure, mPa')
ax.set_xlabel('time, ms')
ax.set_title('surface scatter time series, 2D')
ax.grid()
ax.legend()

plt.show(block=False)
