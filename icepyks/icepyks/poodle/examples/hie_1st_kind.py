"""Worked out development of the integral equation of the first kind
"""
import numpy as np
import matplotlib.pyplot as plt
from icepyks.poodle import cosine_surface

hwave = 0.4
lwave = 10.
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
ftest = 1050
titest = 15 * np.pi / 180
numx = 600
xtest = 4.1  # approximate test position

kwave = 2 * np.pi / lwave
cos_ier = cosine_surface.CosineSurface(hwave, kwave, attn=0)

# test convergence of incident pressure
a0 = np.cos(titest)
katest = 2 * np.pi * ftest / 1500

xaxis = np.arange(numx) / numx * lwave

# use HK kernel for integraion
dpsi_hk = 2 * cos_ier.dpinc(xaxis, a0, katest, is_per=True)

# setup the naive G integration
def G_naive(xtest, xaxis, a0, ftest, num_terms):
    """G kernel with no asymptotics"""
    ka = 2 * np.pi * ftest / 1500
    qs = np.arange(-num_terms, num_terms + 1)
    a0, aq, b0, bq = cos_ier._bragg_angles(a0, qs, ftest)
    dx =  xtest - xaxis
    dz =  np.abs(cos_ier.eta(xtest) - cos_ier.eta(xaxis))
    terms = np.exp(1j * katest * (bq[:, :, None] * dz[None, None, :]
                                  + (aq - a0)[:, :, None] * dx[None, None, :]))
    terms = terms / bq[:, :, None] / ka
    terms = terms[0]
    G = 1 / 2 / 1j / lwave * np.sum(terms, axis=0)
    return G

def G_diff(xtest, xaxis, a0, ftest, num_terms):
    """G kernel with asymptotic term removed"""
    ka = 2 * np.pi * ftest / 1500
    qs = np.arange(-num_terms, num_terms + 1)
    a0, aq, b0, bq = cos_ier._bragg_angles(a0, qs, ftest)
    dx =  xtest - xaxis
    dz =  np.abs(cos_ier.eta(xtest) - cos_ier.eta(xaxis))
    terms = np.exp(1j * katest * (bq[:, :, None] * dz[None, None, :]
                                  + (aq - a0)[:, :, None] * dx[None, None, :]))
    terms = terms / bq[:, :, None] / ka
    terms = 1 / 2 / 1j / lwave * terms[0]
    # remove asymptotic values
    qpi = qs > 0
    qni = qs < 0
    # asymptotic values for positive qs
    terms[qpi, :] += np.exp(-(ka * a0 + qs[qpi, None] * kwave) * dz[None, :]
                            + 1j * qs[qpi, None] * kwave * dx[None, :])\
                     / qs[qpi, None] * ka / 4 / np.pi
    # asymptotic values for negative qs
    terms[qni, :] -= np.exp((ka * a0 + qs[qni, None] * kwave) * dz[None, :]
                            + 1j * qs[qni, None] * kwave * dx[None, :])\
                     / qs[qni, None] * ka / 4 / np.pi
    return np.sum(terms, axis=0)

def g_infty(xtest, xaxis, a0, ftest):
    """analytical sum of asymptotic series"""
    ka = 2 * np.pi * ftest / 1500
    dx =  xtest - xaxis
    dz =  np.abs(cos_ier.eta(xtest) - cos_ier.eta(xaxis))
    val = np.exp(ka * a0 * dz) * np.log(1 - np.exp(-kwave * (dz + 1j * dx)))\
          + np.exp(-ka * a0 * dz) * np.log(1 - np.exp(kwave * (1j * dx - dz)))
    return ka / 4 / np.pi * val

# test the convergence of g_infty
def g_infty_sum(xtest, xaxis, a0, ftest, num_terms):
    """numerical sum of asymptotic series"""
    qp = np.arange(num_terms) + 1
    qn = -qp
    ka = 2 * np.pi * ftest / 1500
    dx =  xtest - xaxis
    dz =  np.abs(cos_ier.eta(xtest) - cos_ier.eta(xaxis))
    t1 = np.exp(-(qp * kwave + ka * a0) * dz[:, None])\
         * np.exp(1j * qp * kwave * dx[:, None]) / qp
    t2 = np.exp((qn * kwave + ka * a0) * dz[:, None])\
         * np.exp(1j * qn * kwave * dx[:, None]) / qn
    val = np.sum(t2 - t1, axis=-1)
    return ka / 4 / np.pi * val

def g_infty_p(xtest, xaxis, ftest, lwave):
    """asymptotic value of integral kernel"""
    ka = 2 * np.pi * ftest / 1500
    dx =  xtest - xaxis
    g_i_p = (np.log(np.abs(dx) / lwave) + np.log(1 - np.abs(dx) / lwave))\
            * ka / 2 / np.pi
    return g_i_p

def integrate_g_asympt(Gd, G_i, G_i_p, psi, xtest, xaxis, ftest, fp_atx):
    """Perform total aymptotic quadrature"""
    ka = 2 * np.pi * ftest / 1500
    dx = xaxis[1] - xaxis[0]
    singi = np.argmin(np.abs(xaxis - xtest))
    igrand = G_i * psi - G_i_p * psi[singi]
    igrand[singi] = (np.log(2 * np.pi) + np.log(1 + fp_atx ** 2) / 2)\
                    * psi[singi] * ka / 2 / np.pi
    integral1 = np.sum(igrand) * dx
    integral2 = - ka * lwave * psi[singi] / np.pi
    integral3 = np.sum(Gd * dpsi_hk) * dx
    return integral1 + integral2 + integral3

itest = np.argmin(np.abs(xaxis - xtest))
xtest = xaxis[itest]
xtest2 = xaxis[itest + 10]
Gn = G_naive(xtest, xaxis, a0, ftest, 45)
Gn_100 = G_naive(xtest, xaxis, a0, ftest, 100)

G_i_ana = g_infty(xtest, xaxis, a0, ftest)
G_i_sum = g_infty_sum(xtest, xaxis, a0, ftest, 100)

# setup asymptotic treatment of singularity
fp_atx = cos_ier.eta(xtest, der=1)
G_i_p = g_infty_p(xtest, xaxis, ftest, lwave)

# limiting value of G_\infty
g_lim = (np.log(2 * np.pi)
         - np.log(np.sum(cos_ier.gf(xtest) ** 2)) / 2
         + np.log(np.abs(xtest - xaxis) / lwave))\
        * katest / 2 / np.pi
Gd = G_diff(xtest, xaxis, a0, ftest, 45)

dx = xaxis[1] - xaxis[0]
total_naive = np.sum(Gn_100 * dpsi_hk)  * dx
ana_naive = np.sum((G_i_sum + Gd) * dpsi_hk) * dx

ana_asmp = integrate_g_asympt(Gd, G_i_ana, G_i_p, dpsi_hk, xtest, xaxis,
                              ftest, fp_atx)
G_asmp = cos_ier._g([xtest, xtest2], xaxis, a0, ftest, 45)
ier_asmp = np.sum(G_asmp * dpsi_hk, axis=-1)  * dx

print('Naive integration: %.5f + %.5f j'%
        (np.real(total_naive), np.imag(total_naive)))
print('Naive asymptotic integration: %.5f + %.5f j'%
        (np.real(ana_naive), np.imag(ana_naive)))
print('Total asymptotic integration: %.5f + %.5f j'%
        (np.real(ana_asmp), np.imag(ana_asmp)))
print('Total asymptotic integration, as multipy: %.5f + %.5f j'%
        (np.real(ier_asmp[0]), np.imag(ier_asmp[0])))

fp_atx2 = cos_ier.eta(xtest2, der=1)
Gd2 = G_diff(xtest2, xaxis, a0, ftest, 45)
G_i_sum2 = g_infty_sum(xtest2, xaxis, a0, ftest, 100)
G_i_ana2 = g_infty(xtest2, xaxis, a0, ftest)
G_i_p2 = g_infty_p(xtest2, xaxis, ftest, lwave)

ana_asmp2 = integrate_g_asympt(Gd2, G_i_ana2, G_i_p2, dpsi_hk, xtest2,
                               xaxis, ftest, fp_atx2)

G_asmp = cos_ier._g(xtest2, xaxis, a0, ftest, 45)
ana_naive = np.sum((G_i_sum2 + Gd2) * dpsi_hk) * dx

print('\n Test point 2')
print('Total asymptotic integration: %.5f + %.5f j'%
        (np.real(ana_asmp2), np.imag(ana_asmp2)))
print('Total asymptotic integration, as multipy: %.5f + %.5f j'%
        (np.real(ier_asmp[1]), np.imag(ier_asmp[1])))

import sys; sys.exit("User break") # SCRIPT EXIT

fig, ax = plt.subplots()
ax.plot(xaxis, np.real(Gn), '-', label='45 terms')
ax.plot(xaxis, np.real(G_asmp[0, :]), '-', label='asympt')
ax.plot(xaxis, np.real(G_i_ana + Gd), '-.', label='diff')
ax.plot([xtest, xtest], [-1, 10], 'k-.', alpha=0.3)
ax.set_ylim(-2.5, 1.5)
ax.set_xlabel('x position, m')
ax.set_ylabel('G kernel')
ax.legend()
ax.grid()
ax.set_title('G integral kernel implimentation\n'
             + 'Evaluatated at x=%.1f m'%xtest)

fig, ax = plt.subplots()
ax.plot(xaxis, np.real(G_i_ana), '-', label='analytical')
ax.plot(xaxis, np.real(G_i_sum), '--', label='100 terms')
ax.plot(xaxis, np.real(g_lim), '--', label='limit')
ax.plot([xtest, xtest], [-1, 10], 'k-.', alpha=0.3)
ax.set_ylim(-2.5, 1.5)
ax.set_xlabel('x position, m')
ax.set_ylabel(r'$G_\infty$ kernel')
ax.legend()
ax.grid()
ax.set_title(r'$G_\infty$ analytical and numerical estimate'
             + '\nEvaluatated at x=%.1f m'%xtest)


plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
# plot the result of the integration
fig, ax = plt.subplots()

ax.plot(xaxis, np.real(dpsi_hk), '-')
ax.plot(xaxis, np.imag(dpsi_hk), '--')
ax.set_xlabel('x position, m')
ax.set_ylabel('dp/dn')

plt.show(block=False)
