"""
=============================
Poodle: :mod:`icepyks.poodle`
=============================

.. currentmodule: icepyks.poodle

Scattering soultions specific to the sinusoidal surface. Offers both HK 
approximation as well as full wave scattering solutions.

Additionally provides a more general framework for plane wave synthesis
into line and point source solutions.

Cosine surface solution methods
===============================

.. autosummary::
   :toctree: generated/

   curved_source_HKA
   plane_wave_rs

Wave number synthesis routines
==============================

.. autosummary::
   :toctree: generated/

   wn_synthesis

Batch processing routines
=========================

.. autosummary::
   :toctree: generated/

   wni_batch

"""
