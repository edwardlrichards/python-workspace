import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jn, hankel1
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp
from icepyks.poodle import cosine_surface
from concurrent.futures import ThreadPoolExecutor

hwave = 0.4
lwave = 10.
rsrc = np.array([0, -15])
rrcr = np.array([55, -10])
ftest = 1050
numx = 100
numqs = 10  # 1/2 number of calculated reflection coefficents
tbound = (0, 90)
numt = 300
titest = np.arange(numt) / (numt - 1) * (tbound[1] - tbound[0]) + tbound[0]

kwave = 2 * np.pi / lwave
cos_ier = cosine_surface.CosineSurface(hwave, kwave, attn=0)

# test convergence of incident pressure
katest = 2 * np.pi * ftest / 1500
dkx = 0.01

# setup ky axis
num_ky = 400
ky_axis = np.arange(num_ky) / (num_ky - 1) * katest

def one_ky(ky, dkx, katest, qvec):
    """setup kx axis specific to ky, and solve for r coefficents"""
    kp = np.sqrt(katest ** 2 - ky ** 2)
    fp = kp * 1500 / 2 / np.pi
    max_kx = 1.1 * kp
    num_kx = np.ceil(max_kx / dkx)
    kx_axis = np.arange(num_kx) / (num_kx - 1) * max_kx
    #a0_axis = np.arccos(kx_axis / kp + 0j)
    a0_axis = kx_axis / kp + 0j
    for kx, a0 in zip(kx_axis, a0_axis):
        yield cos_ier.r_hk(a0, qvec, fp, return_bs=False)

def ky_run(ky_axis, dkx, katest, qvec):
    """Find HK reflection coefficents for each value of ky"""
    ky_axis = np.array(ky_axis, ndmin=1)
    all_kys = []
    for ky in np.sort(ky_axis):
        all_kys.append(np.array(list(one_ky(ky, dkx, katest, qvec))))
    num_kx = all_kys[0].shape
    for ky in all_kys:
        tmp = np.zeros(num_kx, dtype=np.complex_)
        tmp[:ky.shape[0], ...] = ky
        yield tmp

wave_num = np.array(list(ky_run(ky_axis, dkx, katest, 0)))

kx_axis = np.arange(wave_num.shape[1]) * dkx
X, Y = np.meshgrid(kx_axis, ky_axis)

fig, ax = plt.subplots()
cm = ax.pcolormesh(X, Y, np.abs(wave_num), vmax=1.2, vmin=0)
ax.plot(kx_axis, np.sqrt(katest ** 2 - kx_axis ** 2), '--', color='C1')
plt.colorbar(cm)

plt.show(block=False)
