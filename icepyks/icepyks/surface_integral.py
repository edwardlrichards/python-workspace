"""
=================================
Normal pressure field integration
=================================

Common methods for surface integration of pressure field normal derivative.
The integration is formulated in the frequency domain, and can also be extended
to the time domain with ray-approximations.
"""
import numpy as np
from scipy.linalg import solve
from scipy.special import hankel1

class SurfaceIntegral:
    """Common methods for surface integrals"""
    def __init__(self, surface_field, x_axis):
        """Required valuies for all surface integrals"""
        self.field = surface_field
        self.x_axis = x_axis  # quadrature points
        self.num_dims = None

    def __call__(self, i_grand, r_rcr):
        """Preform surface integral, specific to freq or time domain"""
        pass

    @property
    def dx(self):
        """Allow for the x_axis to change on the fly"""
        return (self.x_axis[-1] - self.x_axis[0]) / (self.x_axis.size - 1)

    @property
    def norm_grad(self):
        """Magnitude of the gradient is used in differential correction"""
        return self.field.eta.norm_grad(self.x_axis)

    def _iscale(self, r_rcr, Hz=None):
        """Compute the integral scaling terms appropriate to dimension"""
        if self.num_dims == 3:
            # calculate ampltitude with stationary phase
            fpp = self.field.range_d2dy2(self.x_axis, r_rcr=r_rcr)
            term_scale = 1 / np.sqrt(np.squeeze(fpp))
            if Hz is None:
                i_scale = 1 / (2 * np.sqrt(2 * np.pi * self.field.c_top))
            else:
                k = 2 * np.pi * Hz / self.field.c_top
                i_scale = np.exp(1j * 3 * np.pi / 4) * np.sqrt(k)\
                          / (2 * np.sqrt(2 * np.pi))
        elif self.num_dims == 2:
            if Hz is None:
                i_scale = 1 / 4
            else:
                i_scale = 1j / 4
            term_scale = np.ones_like(self.x_axis)
        else: raise(ValueError('number of dimensions must be 2 or 3'))
        return i_scale, term_scale

class FSurfaceIntegral(SurfaceIntegral):
    """Common methods for single frequency surface integrals"""
    def __init__(self, surface_field, x_axis, kind, num_dims=2):
        """Frequency solutions are specific to 2D problem"""
        SurfaceIntegral.__init__(self, surface_field, x_axis)
        self.num_dims = num_dims
        self.kind = kind  # 1, 2 or 'HK' are allowed
        if self.kind == 1 or self.kind == 2:
            if self.num_dims != 2:
                raise(Exception('Integral equations are are 2D only'))
            self._setup_interaction()

    def __call__(self, r_rcr, Hz, psi=None, rcr_greens=None):
        """Perform a single frequency quadrature, return complex weight"""
        if psi is None:
            psi = self.psi(Hz)
        if rcr_greens is None:
            rcr_greens = self.rcr_greens(r_rcr)
        i_grand = psi * rcr_greens
        i_scale, term_scale = self._iscale(r_rcr, Hz)
        surf_result = np.nansum(term_scale * i_grand, axis=-1)
        surf_result *= i_scale * self.dx
        return surf_result

    def p_inc(self, Hz, **kwargs):
        """Normal derivative of incident pressure field"""
        p_inc = self.field.p_inc(self.x_axis, num_dims=self.num_dims,
                                 Hz=Hz, **kwargs)
        return p_inc

    def psi(self, **kwargs):
        """Solve integral equation for normal pressure derivative"""
        if self.kind == 'HK':
            b = self.field.psi_inc(self.x_axis, num_dims=self.num_dims,
                                   **kwargs)
            return 2 * b
        A = self.A_matricies(self.kind)
        if self.kind == 1:
            b1 = self.field.p_inc(**kwargs)
            x = solve(A, b1)
        elif self.kind == 2:
            # diagonal loading of A matrix for HIE 2nd kind
            A[np.diag_indices_from(A)] += 1j * 2 / self.dx / self.gamma
            b2 = self.field.psi_inc(**kwargs)
            x = solve(A, b2)
        else:
            raise(ValueError('Kind not known'))
        psi = x * 4 * 1j / (self.dx * self.gamma)
        return psi

    def psi_inc(self, **kwargs):
        """Normal derivative of incident pressure field"""
        psi_inc = self.field.psi_inc(num_dims=self.num_dims, **kwargs)
        return psi_inc

    def rcr_greens(self, r_rcr, **kwargs):
        """Greens function from surface to receiver"""
        g_rcr = self.field.rcr_greens(r_rcr, num_dims=self.num_dims, **kwargs)
        return g_rcr

    def direct_ray(self, r_rcr):
        """For adding direct arrival"""
        return self.field.direct_ray(r_rcr, num_dims=self.num_dims)

    def _setup_interaction(self):
        """Pre-compute quanititues used in self interaction calculations"""
        self.surf = np.vstack((self.x_axis, self.field.eta.z(self.x_axis))).T
        # create square self distances matricies
        rel_r = self.surf[:, None] - self.surf[None, :]
        self.rel_range = np.linalg.norm(rel_r, axis=-1)
        # Thorsos formulation
        self.gamma = np.linalg.norm(self.field.eta.grad(self.x_axis), axis=-1)
        # used in 2nd kind
        self.f_pp = self.field.eta.z_pp(self.x_axis)
        # hat values are used to calculate cosine values
        # N by N matrix
        n_hat = self.field.eta.n_hat
        self.rel_proj = np.add.reduce(n_hat(self.x_axis)[:, None, :] * rel_r,
                                      axis=-1)
        ndi = np.ones(self.rel_range.shape, dtype=np.bool_)
        ndi[np.diag_indices_from(self.rel_range)] = False
        self.rel_proj[ndi] = self.rel_proj[ndi] / self.rel_range[ndi]

    def A_matricies(self, kind):
        """Loop through both A types"""
        triu_i = np.triu_indices_from(self.rel_range)
        ka = self.field.kacous
        kr = ka * self.rel_range[triu_i]
        if kind == 1:
            kr = hankel1(0, kr)
            sing_values = hankel1(0, ka * self.dx / 2 / np.e * self.gamma)
        elif kind == 2:
            kr = -ka * hankel1(1, kr)
            sing_values = -1j * self.f_pp / (np.pi * self.gamma ** 3)
        else:
            raise(ValueError('Kind not know'))
        greens_mat = np.zeros_like(self.rel_range, dtype=np.complex_)
        greens_mat[triu_i] = kr
        greens_mat = greens_mat + greens_mat.T
        # second kind is not symeteric
        if kind == 2:
            greens_mat = greens_mat * self.rel_proj
        # compute main diagonal, assume constant element spacing
        dia_i = np.diag_indices_from(greens_mat)
        greens_mat[dia_i] = sing_values
        return greens_mat


class TimeHK(SurfaceIntegral):
    """Common methods for time domain integrals"""
    def __init__(self, surface_field, x_axis, t_axis, signal, num_dims=3):
        """Integral by time domain sort and search
        t_axis: time axis in seconds
        signal: xmitt signal interpolator object
        """
        SurfaceIntegral.__init__(self, surface_field, x_axis)
        self.t_axis = t_axis
        # setup stationary phase interpolator
        if num_dims == 3:
            self.ier = signal.hk_sta_ts
        elif num_dims == 2:
            self.ier = signal.line_source_ts
        # save orignal signal interpolator
        self.signal = signal
        self.num_dims = num_dims

    def __call__(self, r_rcr):
        """Perform a quadrature, returns a complex time series"""
        weights, delays, sI = self._time_sort(self.psi(),
                                              self.rcr_greens(r_rcr))
        # differential scaling term
        i_scale, term_scale = self._iscale(r_rcr)
        # in time domain squish all scaling into a single vector
        weights =  weights * term_scale[sI]
        t_scatter = [self._one_time(t, weights, delays) for t in self.t_axis]
        t_scatter = np.array(t_scatter) * i_scale * self.dx
        return t_scatter

    def psi_inc(self):
        """Normal derivative of incident pressure field
        """
        # ommiting Hz keyword leads to time domain result
        return self.field.psi_inc(self.x_axis, num_dims=self.num_dims)

    def psi(self):
        """Helmholtz Kirchhoff approximation is 2 * incident field"""
        hk_a, hk_tau = self.psi_inc()
        return 2 * hk_a, hk_tau

    def rcr_greens(self, r_rcr):
        """Greens function from surface to receiver
        if Hz is None return time domain form
        """
        # ommiting Hz keyword gives time domain answer
        g_r = self.field.rcr_greens(self.x_axis, r_rcr, num_dims=self.num_dims)
        return g_r

    def _time_sort(self, *args):
        """sort amplitude and delay by delay"""
        if len(args) == 0:
            raise(ValueError('No tuples supplied'))
        amplitude = np.array([a[0] for a in args], ndmin=2)
        delays = np.array([a[1] for a in args], ndmin=2)
        amplitude = np.prod(amplitude, axis=0)
        delays = np.sum(delays, axis=0)
        # sort by delay
        amplitude = np.ravel(amplitude)
        delays = np.ravel(delays)
        sortI = np.argsort(delays)
        delays = delays[sortI]
        amplitude = amplitude[sortI]
        return amplitude, delays, sortI

    def _one_time(self, t, amp, tau):
        """only look up times that have non-zero signal values"""
        #Signs may seem a bit wierd at first
        tau_bounds = (t - self.signal.bbounds)[::-1]
        #Check if any delays are active
        if (tau[-1] < tau_bounds[0]) | (tau[0] > tau_bounds[1]):
            return 0
        #There are some active delays
        activeI = np.searchsorted(tau, tau_bounds)
        activeI = np.arange(activeI[0], activeI[1])
        #numerical integration
        y = np.nansum(self.ier(t - tau[activeI]) * amp[activeI])
        return y
