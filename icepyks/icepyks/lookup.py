import numpy as np
from scipy.interpolate import UnivariateSpline

def lookup(initial_guess, lu_func, tol, sort_index, max_iter=10):
    """Evaluate function with increasing resolution untill convergance
    lu_func takes parameter values between bbound and returns ray parameters
    """
    # Algorithm really only works on a sorted array
    uI = np.argsort(initial_guess[:, sort_index])
    initial_guess = initial_guess[uI, :]
    ray_block, not_ok = _test_convergence(initial_guess, None, tol, sort_index)

    # Loop until convergence
    for num_it in np.arange(max_iter):
        if not len(not_ok):
            # convergence, break loop with return
            return ray_block
        if np.any(np.isnan(ray_block)):
            raise(ValueError('NaN values appeared in lookup function'))
        test_p = _test_range(ray_block, not_ok, sort_index)
        # Shoot off the new fan
        new_rays = lu_func(test_p)
        # test new fan
        ray_block, not_ok = _test_convergence(ray_block, new_rays, tol,
                                              sort_index)

    # didn't achieve necassary convergence
    if len(not_ok):
        raise ValueError('Function did not converge in specified iterations')
    else:
        return ray_block

def _test_convergence(old_rays, new_rays, tol, sort_index):
    """create a boolean array not_ok, interp values that need improvement"""
    # do all tolerance testing on a0
    if old_rays.shape[0] == 2:
        # Test can not pass when there are only 2 old rays
        if new_rays is not None:
            all_rays = np.vstack([old_rays, new_rays])
        else:
            all_rays = old_rays
        uI = np.argsort(all_rays[:, sort_index])
        all_rays = all_rays[uI, :]
        return all_rays, [tuple(old_rays[:, sort_index])]

    if new_rays is None:
        # preventing blind testing on 1st ray
        uI = np.argsort(old_rays[:, sort_index])
        old_rays = old_rays[uI, :]
        # split old_rays in half, and use one half to test the other
        new_rays = old_rays[1:-1:2, :]
        last_ray = old_rays[-1, :]  # used to deal with even size case
        old_rays = old_rays[::2, :]
        old_rays[-1, :] = last_ray

    guess = np.interp(new_rays[:, sort_index], old_rays[:, sort_index],
                      old_rays[:, tol[1]])
    is_fail = np.abs(guess - new_rays[:, tol[1]]) > tol[0]
    # Return ranges of p for which test fails
    not_ok = []
    # is_fail is exhausted over loop
    section_index = 0  # keeps tracks of is_fail relative to p
    while len(is_fail):
        start_section = np.argmax(is_fail)
        # argmax of all false is 0, so this is safe
        is_fail = is_fail[start_section: ]
        section_index += start_section

        # end section is the next time we don't fail
        if not np.any(is_fail):
            # rest of array is good
            break
        elif np.all(is_fail):
            # use up whole array at once if array all fail
            end_section = len(is_fail) - 1
        else:
            # select last failing index
            end_section = np.argmax(np.bitwise_not(is_fail)) - 1

        # get p before and after fail
        fail1 = new_rays[section_index, sort_index]
        fail2 = new_rays[section_index + end_section, sort_index]
        # p1 test needs argmax from right to left
        pI = np.argmax(old_rays[::-1, sort_index] < fail1) + 1
        p1 = old_rays[-pI, :][sort_index]
        p2 = old_rays[np.argmax(old_rays[:, sort_index] > fail2)][sort_index]

        not_ok.append((p1, p2))
        # move along fail array
        is_fail = is_fail[end_section + 1: ]
        section_index += end_section + 1

    all_rays = np.vstack([old_rays, new_rays])
    uI = np.argsort(all_rays[:, sort_index])
    all_rays = all_rays[uI, :]
    return all_rays, not_ok

def _test_range(old_range, not_ok, sort_index, min_splits=10):
    """Loop through a list of indicies designated not_ok, create new ray list
    Since oversampling is fairly cheap, require a certain # of splits
    """
    # Count the total number of gaps
    num_gaps = 0
    gap_points = []
    for p_bound in not_ok:
        # difference between points will be used to create the new points
        old_p = (old_range[:, sort_index] >= p_bound[0]) &\
                (old_range[:, sort_index] <= p_bound[1])
        # Count the total number of gaps need testing
        num_gaps += old_p.size - 1
        gap_points.append(old_range[old_p, sort_index])
    points_per_gap = int(np.ceil(min_splits / num_gaps))

    new_p = []
    for gap in gap_points:
        # create new points inside of existing point range
        gap_diff = np.diff(gap) / (points_per_gap + 1)
        for i in range(points_per_gap):
            new_points = gap[: -1] + gap_diff * (i + 1)
            new_p.append(np.array(new_points))
    new_p = np.sort(np.hstack(new_p))
    if new_p.size < 1:
        import ipdb; ipdb.set_trace() # BREAKPOINT

    return new_p
