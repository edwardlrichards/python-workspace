"""
A sinusoidal surface. z positive up.
"""
import numpy as np
from icepyks.surfaces import eta

class Sine(eta.Eta):
    """Class to specify a sine ocean surface"""
    def __init__(self, wave_length, height, bearing=0, phase=0, z_offset=0):
        """Sinusoidal paramters, all angles in radians"""
        eta.Eta.__init__(self, z_offset)
        self.wave_length = wave_length
        self.k = 2 * np.pi / self.wave_length
        self.height = height
        self.bearing = bearing
        self.phase = phase

    def z(self, *args):
        """sea surface height at a position
        min value of self.z_off
        """
        inplane_distance = self._inplane(*args)
        z = self.height / 2 * np.sin(self.phase + self.k * inplane_distance)\
            + self.z_offset
        return z

    def z_p(self, *args):
        """first spatial derivative of the sea surface"""
        inplane_distance = self._inplane(*args)
        z_p = self.height / 2 * self.k \
              * np.cos(self.k * inplane_distance + self.phase)
        if len(inplane_distance.shape) == 2:
            z_p = np.dstack((np.cos(self.bearing) * z_p,
                             np.sin(self.bearing) * z_p))
        return z_p

    def z_pp(self, *args):
        """second spatial derivative of the sea surface"""
        inplane_distance = self._inplane(*args)
        z_pp = -self.height / 2 * self.k ** 2 \
               * np.sin(self.k * inplane_distance + self.phase)
        if len(inplane_distance.shape) == 2:
            z_pp = np.dstack((np.cos(self.bearing) * z_pp,
                              np.sin(self.bearing) * z_pp))
        return z_pp

    def grad(self, *args):
        """Gradient of the sea surface function f=0"""
        # negative eta derivatives define outward normal
        z_p = -self.z_p(*args)
        # postitve z derivative
        if len(z_p.shape) == 1:  # 1-d surface
            grad_f = np.vstack((z_p, np.ones(z_p.size))).T
        elif len(z_p.shape) == 3:  # 2-d surface
            grad_f = np.dstack((z_p, np.ones(z_p.shape[: -1])))
        return grad_f

    def n_hat(self, *args):
        """Normalized gradient"""
        grad_f = self.grad(*args)
        return self.grad(*args) / self.norm_grad(*args)[..., None]

    def norm_grad(self, *args):
        """Norm of the gradient"""
        grad_f = self.grad(*args)
        return np.linalg.norm(grad_f, axis=-1)

    def _inplane(self, *args):
        """For plane wave surface inplane distance determines properties"""
        x, y = self._shaper(*args)
        if y is None:
            inplane_distance = x
        else:
            coords = np.dstack((x, y))
            inplane = np.array([np.cos(self.bearing), np.sin(self.bearing)])
            inplane_distance = np.inner(coords, inplane)
        return inplane_distance


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    wl = 0.7
    amp = 0.031
    bear = np.pi / 4
    phase = 0
    thesurf = Sine(wl, amp, bearing=bear, phase=phase)
    x = np.r_[0: 5 * wl: 300j]
    y = np.r_[0: 5 * wl: 301j]
    X, Y = np.meshgrid(x, y)
    Z = thesurf.z(X, Y)
    plt.figure()
    plt.pcolormesh(X, Y, Z)
    plt.colorbar()

    plt.figure()
    plt.pcolormesh(X, Y, thesurf.grad(X, Y)[:, :, 0])
    plt.colorbar()
    plt.show(block=False)
