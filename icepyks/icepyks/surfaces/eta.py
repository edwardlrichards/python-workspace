"""
Basic surface methods.
"""
import numpy as np

class Eta:
    """methods are mostly specific to the wave type"""
    def __init__(self, z_offset):
        """z_const is depth of surface iso-speed layer
        """
        self.z_offset = z_offset

    def z(self):
        """z position of free surface"""
        pass

    def z_p(self):
        """first spatial derivative of the free surface"""
        pass

    def z_pp(self):
        """second spatial derivative of free surface"""
        pass

    def grad(self):
        """Gradient vector of surface"""
        pass

    def n_hat(self):
        """Normalized gradient vector"""
        pass

    def norm_grad(self):
        """amplitude of gradient vector"""
        pass

    def _shaper(self, *args):
        """Figures out the shape of the return values based on user input
        There can be one or two arguments

        First argument is the x position of interest. It can have 0 - 2 dims
        If x has 2 dims, a second argument of the same size is needed

        If two arguments are passed a 2 dimesional surface quanity is returned
        if y is a scalar it is broadcast to the size of x
        if x and y are vectors or matricies, they must be the same size
        """
        if len(args) == 1:
            x = args[0]
            y = None
        elif len(args) == 2:
            x = np.array(args[0], ndmin=2)
            y = np.array(args[1], ndmin=2)
            # cheap broadcasting to deal with case where y is a scalar
            if y.size == 1 and x.size != 1:
                y = np.ones(x.shape) * y[0]
            if x.shape != y.shape:
                raise(ValueError('x and y vectors need same shape'))
        else:
            raise(ValueError('Must pass 1 or 2 arguments to surface method'))
        return x, y
