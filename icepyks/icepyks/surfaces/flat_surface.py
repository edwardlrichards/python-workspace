"""
A totally flat surface.
"""
import numpy as np
from icepyks.surfaces import eta

class Flat(eta.Eta):
    """Class to specify a flat surface"""
    def __init__(self, z_offset=0.):
        """For flat surface, mainly stores experimental geometry"""
        eta.Eta.__init__(self, z_offset)

    def z(self, *args):
        """Return the sea surface height at a position"""
        x, y = self._shaper(*args)
        return np.zeros(x.shape) + self.z_offset

    def z_p(self, *args):
        """Return the sea surface height at a position"""
        x, y = self._shaper(*args)
        return np.zeros(x.shape)

    def z_pp(self, *args):
        """Return the sea surface height at a position"""
        x, y = self._shaper(*args)
        return np.zeros(x.shape)

    def grad(self, *args):
        """Gradient of the sea surface"""
        x, y = self._shaper(*args)
        if y is None:
            grad_f = np.vstack((np.zeros(x.shape),
                                np.ones(x.shape))).T
        else:
            grad_f = np.dstack((x, y, np.ones(x.shape)))
        return grad_f

    def n_hat(self, *args):
        """Return the sea surface height at a position"""
        grad_f = self.grad(*args)
        return grad_f

    def norm_grad(self, *args):
        """Norm of the gradient"""
        x, y = self._shaper(*args)
        return np.ones_like(x)

if __name__ == '__main__':
    thesurf = Flat_Eta()
    wl = 0.7
    x = np.r_[0: 5 * wl: 300j]
    y = np.r_[0: 5 * wl: 301j]
    X, Y = np.meshgrid(x, y)
    Z = thesurf.height(X, Y)
    grad_Z = thesurf.grad(X, Y)
    n = thesurf.norm_vec(X, Y)
