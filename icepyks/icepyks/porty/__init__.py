"""
===================================================
Portuguese water dog: :mod:`icepyks.porty`
===================================================

.. currentmodule:: icepyks.porty

Iso-speed free-space HK integral solver

Solves the HK integral with rectangular quadrature, iso-speed Green's
function. The free-space Green's function is used for both the source
and at the surface.


Pressure field for iso-speed source
===================================

.. autosummary::
   :toctree: generated/

   point_iso_field

"""
