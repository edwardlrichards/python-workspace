"""
=======================================
Point source isospeed field HK integral
=======================================
Solves the HK integral using the analytic point source as an incident field.
"""
import numpy as np
from scipy.special import hankel1
from icepyks import surface_field

class IsospeedPoint(surface_field.SurfaceField):
    """
    IsospeedPoint(eta, z_source, c=1500)

    Acoustic incident field generator for iso speed enviornment

    Parameters
    ----------
    eta : surface object
    z_source : source depth, m
    c : sound speed in water, default is 1500 m/s

    Methods
    -------
    __call__
    p_inc
    psi_inc
    rcr_greens
    tau_d2dy2
    """
    def __init__(self, eta, z_source, c=1500):
        """if is_source return the gradient of the Green's function"""
        surface_field.SurfaceField.__init__(self, eta, z_source)
        self.c_src = c
        self.c_top = c
        self.zsrc = z_source

    def __call__(self, interface):
        """Only here to be compatable with ray based fields"""
        pass

    def p_inc(self, x_axis, num_dims=3, Hz=None):
        """Incident pressure field"""
        r_src = np.array((0., 0., self.zsrc))
        return self._p_calculator(x_axis, num_dims, Hz, r_src)

    def psi_inc(self, x_axis, num_dims=3, Hz=None):
        """Normal derivative of incident pressure field"""
        if not(num_dims == 2 or num_dims == 3):
            raise(ValueError('Number of dimensions must be 2 or 3'))
        z = self.eta.z(x_axis)
        r_surf = np.array((x_axis, z - self.zsrc)).T
        r = np.linalg.norm(r_surf, ord=2, axis=-1)
        # projection of gradient onto surface normal
        grad_scale = self.grad_scale(x_axis)
        delay = r / self.c_src
        if Hz is None:   # time domain scaling
            if num_dims == 3:
                p =  grad_scale / r, delay
            if num_dims == 2:
                p =  np.sqrt(2 / (np.pi * r)) * grad_scale, delay
            return p
        # for frequency calculations include complex terms
        k = 2 * np.pi * Hz / self.c_top
        if num_dims == 3:
            p = grad_scale * np.exp(1j * k * r) / r
        else:
            # use the exact hankel form
            p = -k * grad_scale * hankel1(1, k * r)
        return p

    def rcr_greens(self, x_axis, r_rcr, num_dims=3, Hz=None):
        """Greens function from surface to receiver"""
        return self._p_calculator(x_axis, num_dims, Hz, r_rcr)

    def direct_ray(self, r_rcr, num_dims=3, Hz=None):
        """Direct ray from source to receiver"""
        # XXX: not implimented
        import ipdb; ipdb.set_trace()
        pass

    def _p_calculator(self, x_axis, num_dims, Hz, r_rcr):
        """internal method for common caluclation"""
        if not(num_dims == 2 or num_dims == 3):
            raise(ValueError('Number of dimensions must be 2 or 3'))
        z = self.eta.z(x_axis)
        r = np.sqrt((x_axis - r_rcr[0]) ** 2 + (z - r_rcr[-1]) ** 2)
        if Hz is None:  # time domain scaling
            if num_dims == 3:
                amp = 1 / r
            else:
                amp = np.sqrt(2 / (np.pi * r))
            return amp, r / self.c_src
        # for frequency calculations include complex terms
        k = 2 * np.pi * Hz / self.c_src
        if num_dims == 3:
            p = np.exp(1j * k * r) / r
        else:
            # use the exact hankel form
            p = hankel1(0, k * r)
        return p

    def range_d2dy2(self, x_axis, r_rcr):
        """second derivative of range with respect to y"""
        # delay wrt source location
        z = self.eta.z(x_axis)
        r_surf = np.array((x_axis, z - self.zsrc)).T
        denom = r_surf[:, 0] ** 2 + r_surf[:, 1] ** 2
        tau_pp_s = denom / (np.sum(r_surf ** 2, axis=-1) ** (3 / 2))
        # repeat with receiver location
        r_surf = np.array((x_axis - r_rcr[0], z - r_rcr[-1])).T
        denom = r_surf[:, 0] ** 2 + r_surf[:, 1] ** 2
        tau_pp_r = denom / (np.sum(r_surf ** 2, axis=-1) ** (3 / 2))
        return tau_pp_s + tau_pp_r
