"""
================
Eigen-ray solver
================
This eigen ray solver uses the perspective that eigen rays are stationary
points on the travel time curve.
"""
import numpy as np
from scipy.interpolate import UnivariateSpline as interper

class EigenRays:
    """Perform a surface integral using stationary phase approximation"""
    def __init__(self, surface_integral, r_rcr):
        """uses the surface integral framework"""
        self.x_axis = surface_integral.x_axis
        self.surface_integral = surface_integral
        self.r_rcr = r_rcr
        # currently only formulated for time domain surface integrals
        a0, t0 = self.surface_integral.psi()
        a1, t1 = self.surface_integral.rcr_greens(self.r_rcr)
        # create interpolators from the data, there really shouldn't be nans
        amp = a0 * a1
        tau = t0 + t1
        self.tom = tau
        nani = np.bitwise_not(np.isnan(amp))
        self.amp = interper(self.x_axis[nani], amp[nani], s=0)
        self.tau = interper(self.x_axis[nani], tau[nani], s=0, k=4)
        self.tau_p = self.tau.derivative(1)
        self.tau_pp = self.tau.derivative(2)

    @property
    def eig_x(self):
        """Return the x position of all eigenrays"""
        return self.tau_p.roots()

    @property
    def eig_tau(self):
        """Return the x position of all eigenrays"""
        return self.tau(self.eig_x)

    @property
    def eig_amp(self):
        """Return the x position of all eigenrays"""
        ex = self.eig_x
        # second derivative of tau wrt x
        _,  ts = self.surface_integral._iscale(self.r_rcr)
        # interpolate to eigen points
        tsi = np.interp(ex, self.x_axis, ts)
        # stationary phase approximation, factor of 2 is reason for HK approx
        tpp = self.tau_pp(ex)
        ep = np.exp(1j * (3 * np.pi / 4 + np.sign(tpp) * np.pi / 4))\
             * self.amp(ex) * tsi\
             / (2 * np.sqrt(np.abs(tpp) * self.surface_integral.field.c_top))
        if self.surface_integral.num_dims == 2:
            ep *= np.sqrt(np.pi * self.surface_integral.field.c_top / 2)
        return ep

    def make_timeseries(self):
        """Synthesize a time series from eigen ray arrivals
        """
        sig_ier = self.surface_integral.signal
        taxis = self.surface_integral.t_axis
        sig_neg = sig_ier.make_ier(np.conj(1j) * sig_ier.get_spectrum()[1])
        ts = np.zeros_like(taxis)
        for et, ea in zip(self.eig_tau, self.eig_amp):
            if np.abs(ea.imag) < 1e-9:
                ts += ea.real * sig_ier(taxis - et.real)
            else:
                ts += ea.imag * sig_neg(taxis - et.real)
        return ts
