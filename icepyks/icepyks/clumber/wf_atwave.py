from __future__ import division
import numpy as np
from icepyks.clumber import iso_rg as iso_rg
from icepyks.clumber.wavefront import Wavefront
from icepyks.clumber.interp1d import Interp1d
from scipy.interpolate import UnivariateSpline
from scipy.optimize import brentq, newton

class WFAtWave:
    """Create a curve of arrival angle and delay at the surface"""
    # XXX: designed with x0 as lookup index, so will not work with 90 deg prop
    def __init__(self, top_wf, x_src, x_bounds, c0, tol, z_const):
        """Basic setup, one wf for source and receiver
        z_const is offset between top_wf and MWL
        """
        self.c0 = c0  # sound speed in iso speed layer
        # for iso speed propagation to wave
        self.iso_gun = iso_rg.Isospeed(self.c0, -5e3, 0.)
        self.iso_gun(0.)
        self.top_wf = top_wf
        self.la_bounds = top_wf.bounds[0]  # la is index for top wavefront
        self.x_bounds = np.sort(np.array(x_bounds, ndmin=1))
        self.tol = tol
        self.x_src = x_src
        self.z_const = z_const

    def __call__(self, eta):
        """Return 2 wavefronts at wave, w & wo shadowing
        x1 can be a number or a function of x0
        """
        wave_params = self._top_to_boundry(eta)
        wave_wf, shadowed_wf = self._param_to_wavefront(wave_params)
        return wave_wf, shadowed_wf

    def _top_to_boundry(self, eta):
        """Shoot all the rays at the surface to the wave"""
        # use range increments at top as initial grid
        r_range = self.top_wf.knots('la')
        # a is <= range, so it can enforce loose bounds
        r_bounds = np.abs(self.x_bounds)
        # range is not signed, so take care of this logically
        if (np.min(self.x_bounds) < 0) and (np.max(self.x_bounds) > 0):
            #use range as a proxy for line position
            x_right = r_range[r_range < r_bounds[1]]
            x_left = -r_range[r_range < r_bounds[0]]
            x_test = np.hstack((x_left, x_right))
            x_test = np.sort(x_test)
        else:
            x_test = r_range[np.bitwise_and(r_range > r_bounds[0],
                                            r_range < r_bounds[1])]
        # initilize newton with angle to top boundry
        la_guess = self.top_wf(np.abs(x_test[0]), 'x0', is_inverse=True)
        return np.array(list(self._surface_rays(x_test, la_guess, eta)))

    def _surface_rays(self, x_test, la_guess, eta):
        """Generator to shoot a ray to the surface for each x_test"""
        for x in x_test:
            to_wave = self._one_to_boundry(x, la_guess, eta)
            if to_wave is not None:
                yield to_wave
                # use last launch angle to initilize new one
                la_last = to_wave[-1]
                # rays are alway shot forward in root finder
                la_guess = min(la_last, np.abs(la_last - np.pi))

    def _refine_parameters(self, param_init):
        """Given a first guess of the ray parameters, refine to tolerance"""
        pass

    def _one_to_boundry(self, x_surf, la_guess, eta):
        """Find ray parameter that hits surface at range = r_dist
        x1_surf is used to specify a constant horizontal offset
        """
        z_surf = eta.z(x_surf) - self.z_const
        # for 'top' wf, x0 is equivalent to range
        la_to_r = lambda la: float(self.top_wf(la, 'x0'))
        root_func = lambda la: z_surf - np.tan(la)\
                               * (np.abs(self.x_src - x_surf) - la_to_r(la))
        # special case for rays that are almost straight up
        if np.abs(x_surf - self.x_src) < 1e-10:
            # return angle to surface in this case (straight up ray)
            la_tosurf = np.pi / 2
        else:
            la_tosurf = brentq(root_func, *self.la_bounds)
        # use p to construct a ray
        param = self.top_wf(la_tosurf, 'ray')
        param = param.squeeze()
        if self.x_src > 1e-3:  # receiver shoots backwards, wrt source
            x_surf = self.x_src - x_surf
        if x_surf < 0:  # add signed parameters in x
            param[0] = -param[0]
            param[2] = -param[2]
            param[-1] = np.pi - la_tosurf
        # shoot ray up to surface
        at_surf = self.iso_gun._one_to_boundry(param, z_surf, np.nan)
        if self.x_src > 1e-3:  # recover coordinates with source at x=0
            at_surf[0] = self.x_src - at_surf[0]
        return at_surf

    def _param_to_wavefront(self, params):
        """Convert parameters to wavefront, index by a variable"""
        # rays need to be sorted and unique in ray index, a
        _, ui = np.unique(params[:, 0], return_index=True)
        params = params[ui, :]
        params = params[np.argsort(params[:, 0]), :]
        interp_param = params[:, 1:]
        param_names = ['x2', 's0', 's2', 'a0', 'tt', 'la']
        # Create new wave fronts
        index_name = 'x0'  # x0 is the only unique index at this point
        index_variable = params[:, 0]
        # split interpolator around x_src
        xi = np.argmin(np.abs(self.x_src - index_variable))
        i_er = Interp1d(index_variable, interp_param, index_name, param_names)
        #i_er = Interp1d([index_variable[: xi], index_variable[xi + 1: ]],
                        #[interp_param[: xi, :], interp_param[xi + 1: , :]],
                        #index_name, param_names)
        # unshadowed wavefront
        wave_wf = Wavefront(i_er, self.top_wf.sl + ['wave'])
        # shadow correction
        shadow_i = self._shadower(params)
        i_sh = [index_variable[sh] for sh in shadow_i]
        param_sh = [interp_param[sh, :] for sh in shadow_i]
        shadows = Interp1d(i_sh, param_sh, index_name, param_names)
        # shadowed wavefront
        shadowed_wf = Wavefront(shadows, self.top_wf.sl + ['shadow'])
        return wave_wf, shadowed_wf

    def _shadower(self, wave_param):
        """Return a list of tuples of wave shadowed range values"""
        # all ranges in wavefront
        x = wave_param[:, 0]
        la = wave_param[:, -1]
        la_dx = np.zeros_like(la)
        # absolute value means ds/dx0 is measured relative to r_source
        la_dx[1:] = np.diff(la) / np.diff(x)
        la_dx[0] = la_dx[1]
        t = np.array([x, la, la_dx]).T
        t = t[np.bitwise_not(np.isnan(t[:, -1])), :]
        shadow_sec = [r for r in self._rachet(t) if r is not None]
        # remove any completely shadowed sections
        shadow_sec = [r for r in shadow_sec if np.any(r)]
        return shadow_sec

    def _rachet(self, shadow_param):
        """Work through x0 vs slowness plot, mark non-increasing sections"""
        # find sign of derivative at source, shift it a little from x0
        src_index = np.argmin(np.abs(shadow_param[:, 0] - self.x_src))
        src_sign = np.sign(shadow_param[src_index, -1])
        # no shadowing at all
        if np.all(np.sign(shadow_param[:, 2]) != -src_sign):
            yield np.full(shadow_param[:, 2].shape, True, dtype=np.bool_)
            return
        # Find contigous regions of uniform sign launch angle first derivative
        d_sg = np.sign(shadow_param[:, 2]) == -src_sign
        pos_reg = self._pos_region(d_sg, src_index)
        next_left = lambda li: li - np.argmin(d_sg[: li][:: -1])\
                               if not np.all(d_sg[: li]) else None
        next_right = lambda ri: ri + np.argmin(d_sg[ri: ])\
                               if not np.all(d_sg[ri:]) else None
        regions = [pos_reg]
        # all regions to left, then right
        while regions[-1][0] is not None:
            next_i = next_left(regions[-1][0])
            if next_i is None:  # rest of indices are shadowed
                break
            regions.append(self._pos_region(d_sg, next_i))
        regions = regions[:: -1]
        while regions[-1][1] is not None:
            next_i = next_right(regions[-1][1])
            if next_i is None:  # rest of indices are shadowed
                break
            regions.append(self._pos_region(d_sg, next_i))
        # sort regions by maximum la
        la_max = lambda p: np.max(shadow_param[p[0]: p[1], 1])
        regions.sort(key=la_max, reverse=True)
        # unshadowed la < lowest launch angle of previous section
        la_min = lambda p: np.min(shadow_param[p[0]: p[1], 1])
        not_shadow = [shadow_param[p1[0]: p1[1], 1] < la_min(p0) for p0, p1
                     in zip(regions[: -1], regions[1: ])]
        # return index to each unshadowed section
        blank = np.full(shadow_param.shape[0], False, dtype=np.bool_)
        # first region is guarenteed unshadowed
        ns = blank.copy()
        ns[regions[0][0]: regions[0][1]] = True
        yield ns
        # return index to unshadowed part of each successive region
        for i, p in enumerate(regions[1: ]):
            ns = blank.copy()
            ns[p[0]: p[1]] = not_shadow[i]
            yield ns

    def _pos_region(self, bools, center_index):
        """given an array of booleans, find the first bracketing True
        return None when a side does not have a bracketing True
        """
        if center_index is None:
            return
        left_side = bools[: center_index]
        right_side = bools[center_index: ]
        if np.all(np.bitwise_not(left_side)):
            ls = None
        else:
            ls = center_index - np.argmax(left_side[:: -1])
        if np.all(np.bitwise_not(right_side)):
            rs = None
        else:
            rs = center_index + np.argmax(right_side)
        return (ls, rs)

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from icepyks.clumber.wf_atsurface import wf_attop
    from icepyks.clumber.surfaces import sin_surface

    c0 = 1500
    m = -0.3
    z_src = -20
    r_rcr = [200, 0, -10]
    x_bounds = (-20, 220)
    z_bottom = -30
    tol = (1e-2 / c0, 5)
    amp = 1.5  # m
    wave_length = 40  # m

    src_rg = iso_rg.Isospeed(c0)
    src_rg(z_src)
    src_top = wf_attop(src_rg, z_bottom, x_bounds[1], tol)
    src_atwave = WFAtWave(src_top, 0., x_bounds, c0, tol)
    rcr_atwave = WFAtWave(src_top, r_rcr[0], x_bounds, c0, tol)

    eta = sin_surface.Sine(wave_length, amp, z_off=1)

    rcr_wave_wf, rcr_shad_wf = rcr_atwave(eta)
    src_wave_wf, src_shad_wf = src_atwave(eta)
    x_range = np.r_[x_bounds[0]: x_bounds[1]: 300j]

    src_la_vec = src_wave_wf(x_range, 'la')
    src_x2_vec = src_wave_wf(x_range, 'x2')
    rcr_la_vec = rcr_wave_wf(x_range, 'la')
    rcr_x2_vec = rcr_wave_wf(x_range, 'x2')

    fig, ax = plt.subplots()
    ax.plot(x_range, src_shad_wf(x_range, 'la'), 'b', linewidth=2)
    ax.plot(x_range, src_la_vec, 'k')
    ax.plot(x_range, rcr_shad_wf(x_range, 'la'), 'g', linewidth=2)
    ax.plot(x_range, rcr_la_vec, 'k')
    ax.set_title('lauch angle vs x position')

    fig, ax = plt.subplots()
    ax.plot(x_range, src_x2_vec)
    ax.plot(x_range, rcr_x2_vec)
    ax.set_title('surface z vs x')

    plt.show(block=False)
