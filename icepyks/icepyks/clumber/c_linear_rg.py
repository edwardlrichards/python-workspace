"""
============================================
ray gun assuming linear sound speed gradient
============================================
"""
import numpy as np
from icepyks.clumber.ray_gun import RayGun

class CLinear(RayGun):
    """Simple ray trace with one c linear gradient"""
    def __init__(self, m, c_surf):
        """set up ray parameters
        user can pass an ssp interpolator so that c_source matches exactly
        """
        self.m = -m  # z is positive down in rg, opposite in HK
        ssp = lambda z: c_surf + z * self.m
        self.c_surf = c_surf
        self.ssp = ssp
        self.c_m = self.c_surf / self.m  # z where c(z) = 0 used frequently
        RayGun.__init__(self)

    def __call__(self, z_source):
        """Complete the initilization of the raygun for depth of interest"""
        self.z_source = z_source
        self.c_source = self.ssp(self.z_source)
        RayGun.__call__(self, z_source)

    def to_boundry(self, la_vector, z_max, r_max):
        """Just call the ray gun method"""
        if self.z_source is None: raise(ValueError('no source depth'))
        return RayGun.to_boundry(self, la_vector, z_max, r_max)

    def _one_to_boundry(self, param, z_bounds, r_max):
        """Return ray parameters at surface"""
        x0, x2, s0, s2, a0, tt, la = tuple(param)
        # keep track of the sign of vertical slowness
        # MBP formulation, from COA
        R = np.abs(1 / s0 / self.m)
        # Test which interface is hit first
        c_ref = self.ssp(x2)
        b = s2 * c_ref / (s0 * self.m)
        r, z = self._intersection(z_bounds, r_max - x0, R, b)
        return  self._prop(r, z, R, b, param, c_ref)

    def _prop(self, r, z, R, b, param, c_ref):
        """Propagate initial ray to specified end points"""
        x0, x2, s0, s2, a0, tt, la = tuple(param)
        # keep track of the sign of vertical slowness
        sign_s2 = np.sign(s2)
        if sign_s2 == 0: sign_s2 = 1 # need to make a special case for 0 la
        c_z = self.ssp(z)
        # angle to start and end of ray
        t0 = np.arctan2(x2 + self.c_m, -b)
        tf = np.arctan2(z + self.c_m, r - b)
        is_turn = self._isturn(sign_s2, t0, tf)
        # set s2 at boundry
        s2_end = np.sqrt(1 / c_z ** 2 - s0 ** 2)
        s2_end = s2_end * sign_s2 if not is_turn else s2_end * -sign_s2
        # store start and end ray parameters
        x0 += r
        x2 = z
        a0_t = self._a0(s2, s2_end, c_z, c_ref)
        tt_t = self._tau(s0, c_z, is_turn, c_ref)
        # 90 degrees
        if a0_t is None:
            a0_t = np.abs(z - self.z_source)
            tt_t = np.abs(1 / self.m * (np.log(c_z) - np.log(c_ref)))
        a0 += a0_t
        tt += tt_t
        param_end = np.array([x0, x2, s0, -s2_end, a0, tt, la])
        if np.any(np.isnan(param_end)):
            raise(ValueError('Ray gun has produced a nan result'))
        return param_end

    def _intersection(self, z_bounds, r_max, R, b):
        """nearest boundry finding routine"""
        # For saving intersection results
        r_inter = np.zeros((3, 2))
        z_inter = np.zeros((3, 2))
        z_atrmax = np.array((np.sqrt(R ** 2 - (r_max - b) ** 2) - self.c_m,
                             -np.sqrt(R ** 2 - (r_max - b) ** 2) - self.c_m))
        r_inter[0, :] = r_max
        z_inter[0, :] = z_atrmax
        # range max
        r_atzmax = np.array((np.sqrt(R ** 2 - (z_bounds + self.c_m) ** 2) + b,
                             -np.sqrt(R ** 2 - (z_bounds + self.c_m) ** 2) + b))
        r_inter[1:, :] = r_atzmax
        z_inter[1:, :] = z_bounds
        # XXX: require r to be some threshold
        r_inter[np.abs(r_inter) < 1e-6] = np.nan
        # Find the closest actual boundry intersection
        in_r = np.bitwise_and(r_inter >= 0., r_inter <= r_max)
        if np.isnan(np.max(z_bounds)):
            # nan is used to indicate no z boundry
            in_z = z_inter >= np.nanmin(z_bounds)
        else:
            in_z = np.bitwise_and(z_inter >= np.min(z_bounds),
                                  z_inter <= np.max(z_bounds))
        coords = np.dstack((r_inter, z_inter))[np.bitwise_and(in_r, in_z), :]
        coords = np.squeeze(coords)
        # Fix the coordinate of intersection, choose when 2 intersections
        if coords.size != 2:
            if coords.size == 0 or np.all(coords[0, :] < 1e-2):
                # when launch angle is about +- 90 degrees
                # assume a linear path
                z_b = 0. if b < 0. else max(z_bounds)
                coords = [0., z_b]
            else:
                min_norm = np.argmin(np.linalg.norm(coords, axis=-1))
                coords = coords[min_norm, :]
        return tuple(coords)

    def _isturn(self, sign_s2, theta0, theta):
        """from launch angle and circle arc see if ray turned"""
        is_turn = False
        if sign_s2 * np.sign(self.m) > 0:
            tt = sign_s2 * np.pi / 2
            if np.sign(tt - theta0) != np.sign(tt - theta):
                is_turn = True
        return is_turn

    def _tau(self, s0, c_z, is_turn, c_ref):
        """Travel time expression"""
        arg = lambda c: (1 + np.sqrt(1 - s0 ** 2 * c ** 2)) / s0 / c
        arg_cs = arg(c_ref)
        arg_z = arg(c_z)
        # Nans pop up due to very small negative arguments
        arg_cs = arg_cs if not np.isnan(arg_cs) else 1.
        arg_z = arg_z if not np.isnan(arg_z) else 1.
        if is_turn:
            # Turning point inside medium (3.205)
            tau = 1 / np.abs(self.m) * (np.log(arg_cs) + np.log(arg_z))
        else:
            tau = np.abs(1 / self.m * (np.log(arg_z) - np.log(arg_cs)))
        return tau

    def _a0(self, s2_0, s2_end, c_z, c_ref):
        """dynamic ray trace term expression"""
        sin_cs = s2_0 * c_ref
        cos_cs = np.sqrt(1 - sin_cs ** 2)
        sin_z = c_z * s2_end
        # breaks down near vertical la
        scale = c_ref / self.m / cos_cs ** 2
        if np.abs(scale) > 1e20 or np.isnan(scale):
            return None
        a = (sin_cs - sin_z) * scale
        return a

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from icepyks.clumber.iso_rg import Isospeed

    m = -1
    c0 = 1500
    r_max = 410
    z_source = -50
    z_bottom = -200
    ray_gun = CLinear(m, c0)
    ray_gun(z_source)
    iso_gun = Isospeed(c0)   # compare with isospeed
    iso_gun(z_source)

    # shoot and plot a ray fan
    angles = np.radians(np.r_[90: -90: 601j])
    f = ray_gun.to_boundry(angles, z_bottom, r_max)
    p = np.cos(np.radians(angles)) / c0
    f_iso = iso_gun.to_boundry(angles, z_bottom, r_max)

    fig, ax = plt.subplots()
    ax.plot(f[:, 0], f[:, 5], 'b', label='clin')
    ax.plot(f_iso[:, 0], f_iso[:, 5], 'r', label='iso')
    ax.set_xlabel('distance, m')
    ax.set_ylabel('travel time, s')
    ax.grid()
    ax.legend(loc=2)

    fig, ax = plt.subplots()
    ax.plot(f[:, 0], f[:, 1], 'b', label='clin')
    ax.plot(f_iso[:, 0], f_iso[:, 1], 'r', label='iso')
    ax.set_xlabel('distance, m')
    ax.set_ylabel('depth, m')
    ax.grid()
    ax.legend()

    fig, ax = plt.subplots()
    ax.plot(f[:, 0], f[:, 3], 'b', label='clin')
    ax.plot(f_iso[:, 0], f_iso[:, 3], 'r', label='iso')
    ax.set_xlabel('x, m')
    ax.set_ylabel('$s_z$, s / m')
    ax.grid()
    ax.legend()

    fig, ax = plt.subplots()
    ax.plot(f[:, 1], f[:, 3], 'b', label='clin')
    ax.plot(f_iso[:, 1], f_iso[:, 3], 'r', label='iso')
    ax.set_xlabel('z, m')
    ax.set_ylabel('$s_z$, s / m')
    ax.grid()
    ax.legend()

    plt.show(block=False)
