"""
=====================================================
Ray gun with range indipendent sound speed variations
=====================================================
Solve for ray paths in refracting enviornments using an ode solver
"""
import numpy as np
from icepyks.clumber.ray_gun import RayGun
from scipy.integrate import solve_ivp

class Refracting(RayGun):
    """ODE solution of eikonal equation"""
    def __init__(self, ssp):
        """set up ray parameters"""
        RayGun.__init__(self)
        self.ssp = ssp
        # check whether to compute term related to 2nd derivative
        self.snd_der = False
        try:
            ssp.derivative(2)
            self.snd_der = True
        except ValueError:
            pass

    def __call__(self, z_source):
        """Complete the initilization of the raygun for depth of interest"""
        self.z_source = z_source
        RayGun.__call__(self, z_source)

    def to_boundry(self, la_vector, z_max, r_max):
        """Just call the ray gun method"""
        return RayGun.to_boundry(self, la_vector, z_max, r_max)

    def _one_to_boundry(self, param, z_bounds, r_bound):
        """Propagate ray untill it hits a boundry"""
        la = param[-1]
        result, _ = self._solve_ode(param, z_bounds, r_bound)
        x0, s0, x2, s2, a0, _, tt = tuple(result.roots.y[-1, :])
        return np.array((x0, x2, s0, s2, a0, tt, la))

    def eigen_ray(self, la, r_max, num_points=100):
        """Shoot an eigen ray to z_max, reflecting at top boundry
        Returns a params matrix interpolated along the ray path"""
        start_param = np.squeeze(self._init_ray(la))
        z_bounds = np.array((0., np.nan))
        result, solver = self._solve_ode(start_param, z_bounds, r_max)
        params = [self._eig_interpolate(la, result, solver)]
        # have we gotten to r_max yet?
        while not np.isclose(result.roots.y[-1, 0], r_max):
            t_start = result.roots.t[-1]
            y_start = result.roots.y[-1, :]
            # reflect ray
            y_start[3] = -y_start[3]
            y_start[2] = 0.
            # trying to enforce that solver is reset
            solver.init_step(t_start, y_start)
            result = solver.solve(np.array((t_start, r_max * 5)), y_start)
            params.append(self._eig_interpolate(la, result, solver))

        return np.vstack(params), solver

    def _eig_interpolate(self, la, result, solver):
        """Create a dense sampling along an eigen ray"""
        # interpolate points up to and including root
        start_t = result.values.t[-1]
        stop_t = result.roots.t[-1]
        dt = stop_t - start_t
        dr = max(dt / 40, 1)
        num_r = np.ceil(dt / dr)
        value_range = np.arange(num_r) / (num_r - 1) * dt + start_t
        samp_res = solver.solve(value_range, result.values.y[0, :])
        y_values = samp_res.values.y
        # add root value at the end if necassary
        if samp_res.roots.y is not None:
            y_values = np.vstack((y_values, samp_res.roots.y[-1, :]))
        # rearrange result to parameter space
        params = y_values[:, [0, 2, 1, 3, 4, 6]]
        params = np.column_stack((params, np.full_like(params[:, 0], la)))
        return params

    def _solve_ode(self, param, z_bounds, r_bound):
        """Return solution of the ode"""
        # CAO formulation with dynamic ray tracing
        x0, x2, s0, s2, a0, tt, la = tuple(param)
        p0 = 1 / self.ssp(self.z_source)
        x_init = np.array((x0, s0, x2, s2, tt, p0, 0.))

        rooter = lambda r, z, result, userdata:\
               self._crosses_bound(r, z, result, userdata, z_bounds, r_bound)

        solver = ode('cvode', self._rhseqn, rootfn=rooter, nr_rootfns=3,
                     old_api=False, max_steps=5000)

        result = solver.solve([0., r_bound * 5], x_init)
        if result.errors.t is not None:
            print('Error: ', result.message, 'Error at time', result.errors.t)
        return result, solver

    def _rhseqn(self, r, z, zdot):
        """Eikonal equation"""
        c = self.ssp(z[2])
        zdot[0] = c * z[1]
        zdot[1] = 0.  # horizontal slowness is conserved
        zdot[2] = c * z[3]
        zdot[3] = -(1 / c ** 2) * self.ssp(z[2], nu=1)
        zdot[4] = c * z[5]
        if self.snd_der:
            zdot[5] = -self.ssp(z[2], nu=2) / c ** 2 * z[4]
        else:
            zdot[5] = 0.
        zdot[6] = 1 / c

    def _crosses_bound(self, r, z, result, userdata, z_bounds, r_bound):
        """Root functions for boundry events"""
        result[0] = z[2]  #  surface at z = 0
        result[1] = z[0] - r_bound  #  surface at z = 0
        result[2] = z[2] - np.max(z_bounds)  #  surface at z = z_bot

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from scipy.interpolate import UnivariateSpline
    z = np.linspace(0, 100, 100)
    m = 0.1
    r_max = 1000.
    z_source = 30
    z_bottom = 300
    ssp = UnivariateSpline([0, z_bottom], [1500, 1500 + 0.3 * z_bottom],
                           s=0, k=1, ext=0)
    c0 = ssp(z_source)
    ray_gun = Refracting(ssp)
    ray_gun(z_source)

    # shoot and plot a ray fan
    angles = np.radians(np.r_[90: -90: 31j])
    f = ray_gun.to_boundry(angles, z_bottom, r_max)

    fig, ax = plt.subplots()
    [ax.plot([0, r], [z_source, z], 'b') for r, z in zip(f[:, 0], f[:, 1])]
    ax.set_ylim(z_bottom, 0.)

    # eigenray shooter
    r, s = ray_gun.eigen_ray(np.radians(-21), 4800.)
    fig, ax = plt.subplots()
    ax.plot(r[:, 0], r[:, 1])
    ax.set_ylim(400, -10)
    ax.grid()

    plt.show(block=False)
