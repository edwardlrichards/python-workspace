"""
==================================
Waltead and Dean's tank experiment
==================================

High frequency pulse reflected from a single wave.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.surfaces import sin_surface
from icepyks.clumber import ray_field, iso_rg

cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 0.693
wave_height = 31e-3
num_phase = 120
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (np.pi / 2)

c0 = 1469
z_source = -0.2
r_receiver = np.array((1.215, 0, -0.14))
z_bottom = -1
z_off = wave_height / 2 + 0.01

# Test out if measurements are from Highest water point
r_receiver[2] = r_receiver[2] + 2 * z_off
z_source += 2 * z_off

#signal parameter
fc = 200e3

#Setup integration bounds
x_bounds = (-0.5, 1.75)
#taxis is from Walstead and Dean
t_bounds = (841e-6, 881e-6)
fs = 5e6
dx = 3e-3
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# solve for pressure field at surface
ray_gun = iso_rg.Isospeed(c0, z_bottom, -z_off)
eta = sin_surface.Sine(lamda_surf, wave_height)
proper = ray_field.RayField(eta, z_source, r_receiver,
                            tt_tol=5e-7, range_buffer=1)
proper.to_top(ray_gun)

# initialize xmitted signal
signal, tsignal = pulse_signal.pulse()
i_er_signal = signal_interp.ContinuousSignal(signal, tsignal)

t_series = []
eta_series = []
for p in wave_phase[:: -1]:
    eta = sin_surface.Sine(lamda_surf, wave_height, phase=p)
    proper.to_wave(eta)
    proper('wave')
    hk_solver = surface_integral.TimeHK(proper, xaxis, taxis, i_er_signal)
    t_series.append(hk_solver(r_receiver))

t_series = np.array(t_series)

distance = np.sqrt((z_source - r_receiver[2]) ** 2 + r_receiver[0] ** 2)

# indicies taken from paper
ind_a = 10
ind_b = 86

fig, axes = plt.subplots(2, sharex=True)
axes[0].plot((taxis - np.min(taxis)) * 1e6, t_series[ind_a])
axes[0].set_title('Scattered pulse A')
axes[0].set_ylim(-1, 1)
axes[1].plot((taxis - np.min(taxis)) * 1e6, t_series[ind_b])
axes[1].set_title('Scattered pulse B')
axes[1].set_ylim(-1, 1)
axes[1].set_xlabel('time, us')
axes[1].set_ylabel('amplitude, pressure')

N, T = np.meshgrid(np.arange(wave_phase.size), (taxis - taxis[0]) * 1e6)
fig, ax = plt.subplots()
cm = ax.pcolormesh(N, T, t_series.T, vmin=-1, vmax=1, cmap=cmap)
ax.plot((ind_a, ind_a), (-100, 100), 'k')
ax.plot((ind_b, ind_b), (-100, 100), 'k')

ax.annotate('A',
             xy=(ind_a, 30),
             xycoords='data',
             xytext=(ind_a + 10, 30),
             textcoords='data',
             arrowprops=dict(arrowstyle="->"))

ax.annotate('B',
             xy=(ind_b, 30),
             xycoords='data',
             xytext=(ind_b + 10, 30),
             textcoords='data',
             arrowprops=dict(arrowstyle="->"))

fig.colorbar(cm)
ax.set_ylim(0, (np.max(taxis) - np.min(taxis)) * 1e6)
ax.set_title('Amplitude of arrival over a full wave cycle' +\
          '\n sinusoindal wave setup of Walstead and Dean')

plt.show(block=True)
