"""
==========================
Scaled W&D tank experiment
==========================

High frequency pulse reflected from a single wave, scaled to a 15kz center
frequency.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.clumber import ray_field, iso_rg
from icepyks.surfaces import sin_surface

cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 9.435
wave_amp = 0.422 / 2
num_phase = 120
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (np.pi / 2)

c0 = 1500
z_source = -2.723
r_receiver = np.array((3 * 16.542, 0, -1.906))
z_bottom = -4
z_off = -(wave_amp + 0.01)

spec_r = np.sqrt((z_source + r_receiver[2]) ** 2 + r_receiver[0] ** 2)

#signal parameter
fc = 15e3
f_max = 25e3
f_min = 5e3
df = 100
NFFT = int(2 ** np.ceil(np.log2(f_max * 2 / df)))
f_axis = np.arange(NFFT / 2, dtype=np.float_) * df
sig_i = np.bitwise_and(f_axis >= f_min, f_axis <=  f_max)
i_start = np.argmax(sig_i)

#Setup integration bounds
x_bounds = (-4, 14)
fs = f_max * 3
dx = c0 / fc / 5
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
spec_tau = spec_r / c0
taxis = np.arange(spec_tau - 1e-3, spec_tau + 4e-3, 1 / fs)

# solve for pressure field at surface
ray_gun = iso_rg.Isospeed(c0, z_bottom, z_off)
eta = sin_surface.Sine(lamda_surf, wave_amp)
proper = ray_field.RayField(eta, z_source, r_receiver,
                            tt_tol=5e-6, range_buffer=6)
proper.to_top(ray_gun)

y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

ind_a = 25
ind_b = 75

t_series = []

for p in wave_phase:
    eta = sin_surface.Sine(lamda_surf, wave_amp, phase=p)
    proper.to_wave(eta)
    proper('wave')

    hk_time = surface_integral.TimeHK(proper, xaxis, taxis, i_er_signal)
    t_series.append(hk_time(r_receiver))

t_series = np.array(t_series)

#ax, ax1 = pulse_signal.plot_signal(signal)

N, T = np.meshgrid(np.arange(wave_phase.size), (taxis - spec_tau) * 1e3)
fig, ax = plt.subplots()
cm = ax.pcolormesh(N, T, t_series.T, vmin=-0.1, vmax=0.1, cmap=cmap)
ax.plot((ind_a, ind_a), (-100, 100), 'k')
ax.plot((ind_b, ind_b), (-100, 100), 'k')
fig.colorbar(cm)
ax.set_ylim(0, 0.6)
ax.set_title('Amplitude of arrival over a full wave cycle' +\
          '\n sinusoindal wave setup of Walstead and Dean')

plt.show(block=True)
