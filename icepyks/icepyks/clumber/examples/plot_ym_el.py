import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hilbert
# import direct iso-speed solver
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp, surface_integral
# import numerical tracer
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 40
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 1.5
c0 = 1500
z_source = -20
r_receiver = np.array((200.,0.,-10))
r_buffer = 20.
z_const = -(wave_amp + 0.1)
z_bottom = -300

#signal parameter
fc = 20e3
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# flat surface results
t_spec = np.sqrt(r_receiver[0] ** 2 + (z_source + r_receiver[2]) ** 2) / c0
direct_amp = 1 / np.sqrt(r_receiver[0] ** 2 + (z_source - r_receiver[2]) ** 2)

# create integration bounds
x_bounds = (-r_buffer, r_receiver[0] + r_buffer)
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3,
            np.ceil(t_spec * 1e3 + 8) / 1e3)
fs = 5e5
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# setup surface
eta = sin_surface.Sine(lamda_surf, wave_amp, phase=0.)

# ray solution
ray_field = ray_field.RayField(eta, z_source, r_receiver, tt_tol=5e-7,
                               range_buffer=40)
ray_gun = iso_rg.Isospeed(c0, z_bottom, z_const)
ray_field.to_top(ray_gun)
ray_field.to_wave()
ray_HK = surface_integral.TimeHK(ray_field, xaxis, taxis, i_er_signal)
ray_result = ray_HK(r_receiver)

# isospeed solution
iso_field = point_iso_field.IsospeedPoint(eta, z_source)
isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal)
iso_result = isospeed_HK(r_receiver)

fig, ax = plt.subplots()
ax.plot(taxis * 1e3, iso_result / direct_amp, linewidth=3, alpha=0.6, label='naive')
ax.plot(taxis * 1e3, ray_result / direct_amp, 'k', label='shadow')
ax.set_xlim(134.5, 137)
ax.set_ylim(-0.6, 0.6)
ax.set_xlabel('delay, ms')
ax.set_title('Time series from Choo and Song letter')
ax.legend()
ax.grid()

plt.show(block=True)
