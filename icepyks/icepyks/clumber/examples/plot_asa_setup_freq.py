"""
==========================
Scaled W&D tank experiment
==========================

High frequency pulse reflected from a single wave, scaled to a 15kz center
frequency.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.clumber import ray_field, iso_rg
from icepyks.surfaces import sin_surface

cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 9.435
wave_amp = 0.422 / 2
num_phase = 120
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (np.pi / 2)

c0 = 1500
z_source = -2.723
r_receiver = np.array((16.542, 0, -1.906))
z_bottom = -4
z_off = -(wave_amp + 0.01)

#signal parameter
fc = 15e3
f_max = 25e3
f_min = 5e3
df = 100
NFFT = int(2 ** np.ceil(np.log2(f_max * 2 / df)))
f_axis = np.arange(NFFT / 2, dtype=np.float_) * df
sig_i = np.bitwise_and(f_axis >= f_min, f_axis <=  f_max)
i_start = np.argmax(sig_i)

#Setup integration bounds
x_bounds = (-4, 14)
fs = f_max * 3
dx = c0 / fc / 5
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

spec_tau = np.sqrt((z_source + r_receiver[2]) ** 2 + r_receiver[0] ** 2) / c0
taxis = np.arange(spec_tau - 1e-3, spec_tau + 4e-3, 1 / fs)

# solve for pressure field at surface
ray_gun = iso_rg.Isospeed(c0, z_bottom, z_off)
eta = sin_surface.Sine(lamda_surf, wave_amp)
proper = ray_field.RayField(eta, z_source, r_receiver,
                            tt_tol=1e-6, range_buffer=6)
proper.to_top(ray_gun)

# initialize xmitted signal
signal, tsignal = pulse_signal.pulse_q1(fc)
# take fft of xmitt
dt_sig = float(tsignal[1] - tsignal[0])
fs_sig = 1 / dt_sig
signal_FT = np.fft.rfft(signal, 16 * int(2 ** np.ceil(np.log2(signal.size))))
f_signal = np.arange(signal_FT.size) / (signal_FT.size * 2) * fs_sig

i_er_signal = signal_interp.ContinuousSignal(signal, tsignal)

ind_a = 10
ind_b = 86

f_series = []
eta_series = []

#for ind in (ind_a, ind_b):
for ind in (ind_a, ):
    p = wave_phase[ind]
    eta = sin_surface.Sine(lamda_surf, wave_amp, phase=p)
    proper.to_wave(eta)
    proper('wave')
    hk_freq = surface_integral.FSurfaceIntegral(proper, xaxis, 'HK')
    f_temp = f_axis.copy() * 0j
    for f_i, f in enumerate(f_axis[sig_i]):
        if f_i % 1000 == 0:
            print('iter number %i'%f_i)
        dx = c0 / f / 8
        xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
        hk_freq.xaxis = xaxis
        f_temp[f_i + i_start] = hk_freq(r_receiver, f)
    f_series.append(f_temp)

    hk_time = surface_integral.TimeHK(proper, xaxis, taxis, i_er_signal)
    t_series = hk_time(r_receiver)

f_series = np.array(f_series)

ft_series = np.fft.rfft(t_series, NFFT)
ft_axis = np.arange(ft_series.size) / NFFT * fs

# interpolate signal to inverted frequencies
f_real = np.interp(f_axis, f_signal, np.real(signal_FT))
f_imag = np.interp(f_axis, f_signal, np.imag(signal_FT))
f_signal_interp = f_real + 1j * f_imag

fig, ax = plt.subplots()
ax.plot(f_axis, np.abs(f_signal_interp), 'b', linewidth=2, alpha=0.6)
ax.plot(f_signal, np.abs(signal_FT), 'g')
ax.set_xlim(5e3, 25e3)

fig, ax = plt.subplots()
ax.plot(f_axis, np.abs(f_signal_interp * np.squeeze(f_series)), 'b', linewidth=2, alpha=0.6)
ax.plot(ft_axis, np.abs(ft_series), 'g')
ax.set_xlim(5e3, 25e3)

# plot time domain version of signals
t_ff = np.fft.irfft(ft_series)
ref_ff = np.fft.irfft(f_signal_interp * np.squeeze(f_series))
ref_df = f_axis[1] - f_axis[0]

fig, ax = plt.subplots()
ax.plot(np.arange(ref_ff.size) / ref_df, ref_ff, 'b', linewidth=2, alpha=0.6)
ax.plot(np.arange(t_ff.size) / df, t_ff, 'g')
plt.show(block=True)
