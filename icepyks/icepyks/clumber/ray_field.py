"""
=================
Surface ray field
=================
Surface derivative terms and Green's functions at surface, for sources and
receivers that are not on the surface.
"""
import numpy as np
from icepyks import surface_field
from icepyks.clumber.wf_atsurface import wf_attop
from icepyks.clumber.wf_atwave import WFAtWave
from icepyks.clumber.interp1d import tau_stationary

class RayField(surface_field.SurfaceField):
    """organizes the movement of wavefronts from one surface to the next"""
    def __init__(self, eta, z_source, r_receiver, tt_tol=5e-6, max_iter=50,
                 range_buffer=50):
        """Propagator both manages wavefronts and provides computed values
        """
        surface_field.SurfaceField.__init__(self, eta, z_source)
        self.current_surface = None  # returns values at the this surface
        self._is_inverse = None
        self.z_src = z_source
        self.r_rcr = r_receiver
        self.z_bottom = None
        self.z_const = None
        self.range_rcr = np.linalg.norm(self.r_rcr[: 2])
        self.global_coord = None  # define for 2D surface coordinates
        self.tt_tol = (tt_tol, 5)  # travel time error
        self.max_iter = max_iter
        max_range = max((np.linalg.norm(self.r_rcr), np.abs(self.z_src)))
        self.a_bounds = (-range_buffer, max_range + range_buffer)
        # wavefronts at z=0
        self.src_top = None
        self.rcr_top = None
        self.c_top = None
        self.c_src = None
        self.direct = None
        # wavefronts at z=eta
        self.src_towave = None
        self.rcr_towave = None
        self.src_wave = None
        self.rcr_wave = None
        self.src_shad = None
        self.rcr_shad = None

    def __call__(self, surface):
        """set the current surface to input value"""
        self.current_surface = surface
        # is x coordinate an index variable or not
        if self.current_surface == 'top':
            self._is_inverse = True
        else:
            self._is_inverse = False

    def psi_inc(self, x_axis, num_dims=3, Hz=None):
        """Amplitude and delay of incident pressure at surface"""
        if not(num_dims == 2 or num_dims == 3):
            raise(ValueError('Number of dimensions must be 2 or 3'))
        # XXX: does not support stationary phase paths
        ys = np.zeros_like(x_axis)
        src_tau = self.delay(x_axis, ys, 'source')
        src_amp = self.amplitude(x_axis, ys, 'source', num_dims=num_dims)
        # include projection scaling of normal derivative
        src_amp = src_amp * self.grad_scale(x_axis)
        if Hz is None:  # time domain scaling
            return src_amp, src_tau
        # for frequency calculations include complex terms
        w = 2 * np.pi * Hz
        p = src_amp * np.exp(1j * w * src_tau)
        if num_dims == 2:
            # complex contribution of far field Hankel approximation
            p *= -np.sqrt(2 * w / (np.pi * self.c_src))\
                 * np.exp(-3j * np.pi / 4)
        return p

    def rcr_greens(self, x_axis, r_rcr, num_dims=3, Hz=None):
        """Amplitude and pressure from surface to receiver"""
        if not(num_dims == 2 or num_dims == 3):
            raise(ValueError('Number of dimensions must be 2 or 3'))
        # XXX: does not support stationary phase paths
        ys = np.zeros_like(x_axis)
        a_rcr = self.amplitude(x_axis, ys, 'receiver', num_dims=num_dims)
        t_rcr = self.delay(x_axis, ys, 'receiver')
        if Hz is None:  # time domain scaling
            return a_rcr, t_rcr
        # for frequency calculations include complex terms
        w = 2 * np.pi * Hz
        g_rcr = a_rcr * np.exp(1j * w * t_rcr)
        if num_dims == 2:
            # complex contribution of far field Hankel approximation
            g_rcr *= np.sqrt(2 * self.c_src /  (np.pi * w))\
                      * np.exp(-1j * np.pi / 4)
        return g_rcr

    def grad_scale(self, x_axis):
        """overwrite grad scale to account for curved rays"""
        # only source element is relevant for this calculation
        wavefront = self._get_wf('source')
        # cos vector of ray and gradient vector of surface
        # XXX: does not impliment y axis consderations
        if self.current_surface == 'top':
            wavefront = self._get_wf('source')
            gradF = np.dstack((np.zeros_like(x_axis),
                               np.zeros_like(x_axis),
                               np.ones_like(x_axis)))
            cos_vec = wavefront(np.abs(x_axis), 's_vec', is_inverse=True)\
                      * self.c_top
        else:
            gradF = self.eta.grad(x_axis)
            # XXX: surfaces make no sense right now, insert y dimension
            gradF = np.array([gradF[:, 0], np.zeros_like(x_axis), gradF[:, 1]])
            cos_vec = wavefront(np.abs(x_axis), 's_vec')  * self.c_top
        return np.add.reduce(cos_vec * gradF.T, axis=-1)

    def to_top(self, ray_gun):
        """Use ray gun to find wave front at z=0"""
        self.z_bottom = ray_gun.z_bottom
        self.z_const = ray_gun.z_const
        # Transmit from source to surfaces
        ray_gun(self.z_src)
        self.c_top = ray_gun.ssp(0.)
        self.c_src = ray_gun.c_source
        # radiated ray target
        r_rel = self.r_rcr.copy()
        r_rel[-1] = r_rel[-1] - ray_gun.z_const
        self.src_top, rad = wf_attop(ray_gun,
                                     self.a_bounds[1], self.tt_tol,
                                     r_rcr=r_rel,
                                     max_iter=self.max_iter)
        # Transmit from receiver to surfaces
        ray_gun(self.r_rcr[-1])
        self.rcr_top = wf_attop(ray_gun,
                                self.a_bounds[1], self.tt_tol,
                                max_iter=self.max_iter)
        # Calculate amplitude and phase of radiated ray
        rad_rho = float(rad[:, 0])
        rad_pr = float(rad[:, 2])
        rad_q = float(rad[:, 4])
        rad_tt = float(rad[:, 5])
        # always assume 3d ray for radiation
        self('top')  # move current surface to the top
        self._rad_ray = (rad_rho, rad_pr, rad_q, rad_tt)
        # now have enough to initilize wavefront to wave propagators
        self.src_towave = WFAtWave(self.src_top, 0., self.a_bounds,
                                   self.c_top, self.tt_tol, self.z_const)
        # XXX: Hardcode doen't allow for 2d surface
        self.rcr_towave = WFAtWave(self.rcr_top, self.r_rcr[0], self.a_bounds,
                                   self.c_top, self.tt_tol, self.z_const)

    def to_wave(self, eta=None):
        """Propagate from top wavefront to wavefront at eta"""
        if eta is not None:
            self.eta = eta
        self.src_wave, self.src_shad = self.src_towave(self.eta)
        self.rcr_wave, self.rcr_shad = self.rcr_towave(self.eta)
        self('shad')  # move current surface to the shadowed wave

    def direct_ray(self, Hz, num_dims):
        """Calculate magnitude and phase of direct ray"""
        w = 2 * np.pi * Hz
        if num_dims == 3:
            dim_scale = 1
        elif num_dims == 2:
            k = w / self.c_src
            dim_scale = np.sqrt(2 / np.pi / k) * np.exp(-1j * np.pi / 4)
        rr = self._rad_ray
        ray_amp = self._amp_calculation(rr[0], rr[1], rr[2], num_dims)
        rad_ray = ray_amp * np.exp(1j * w * rr[3])
        #XXX: cast to complex does not work
        rad_ray = float(np.real(rad_ray)) + 1j * float(np.imag(rad_ray))
        return dim_scale * rad_ray

    def is_shadow(self, x0):
        """Return boolean array, true at each x0 that is shadowed"""
        if self.src_shad is None:
            return(ValueError('Wave front has not propagated to surface'))
        wh = self.src_shad(x0, 'x2') + self.rcr_shad(x0, 'x2')
        return np.isnan(wh)

    def delay(self, x0, x1, element):
        """Total travel delay from source to receiver"""
        x0 = np.array(x0, ndmin=2)
        x1 = np.array(x1, ndmin=2)
        if self.current_surface == 'top':
            rho_s, rho_r = self._rho(x0, x1)
            if element == 'source':
                tau = self.src_top(rho_s, 'tt', is_inverse=True)
            elif element == 'receiver':
                tau = self.rcr_top(rho_r, 'tt', is_inverse=True)
        else:
            tau = self._get_wf(element)(x0, 'tt')
        return np.squeeze(tau)

    def amplitude(self, x0, x1, element, num_dims=3):
        """ray amplitude from source and receiver"""
        x0 = np.array(x0, ndmin=2)
        x1 = np.array(x1, ndmin=2)
        if self.current_surface == 'top':
            rho_s, rho_r = self._rho(x0, x1)
            if element == 'source':
                amp = self._ray_amp(rho_s, self.src_top, num_dims)
            elif element == 'receiver':
                amp = self._ray_amp(rho_r, self.rcr_top, num_dims)
        else:
            amp = self._ray_amp(x0, self._get_wf(element), num_dims)
        return np.squeeze(amp)

    def _ray_amp(self, rho, wavefront, num_dims):
        """calculate the amplitude of a single ray path
        """
        if self.current_surface == 'top':
            q = wavefront(rho, 'a0', is_inverse=True)
            p_vec = wavefront(rho, 's_vec', is_inverse=True)
        else:
            q = wavefront(rho, 'a0')
            p_vec = wavefront(rho, 's_vec')
            if wavefront == self.rcr_shad or wavefront == self.rcr_wave:
                rho = np.abs(self.r_rcr[0] - rho)
        p_r = np.linalg.norm(p_vec[..., : 2], axis=-1)
        # amplitude calculation
        if num_dims == 3:  # point source
            amp = np.sqrt(np.abs(self.c_top * p_r / (rho + 1e-4) / q))
            # intepolate linearly to rho_min
            ri = np.argmin(np.abs(rho))
            amp[0, ri] = (amp[0, ri - 1] + amp[0, ri + 1]) / 2
        elif num_dims == 2:
            amp = np.sqrt(np.abs(self.c_top / self.c_src / q))
            amp *= np.sqrt(2 / np.pi)
        else:
            raise(ValueError('Number of dimensions needs to be 2 or 3'))
        return amp

    def range_d2dy2(self, x0, x1=None, r_rcr=None):
        """stationary phase second derivative term"""
        if x1 is None:
            x1 = np.zeros_like(x0)
        if self.current_surface == 'top':
            rho_s, rho_r = self._rho(x0, x1)
            f_src, f_rcr = self._dr_dy(x0, x1, dy=2)
            fpp = self.rcr_top(rho_r, 'tau', nu=1) * f_rcr\
                  + self.src_top(rho_s, 'tau', nu=1) * f_src
        # XXX: This formulation has not been really confirmed at all
        else:
            fpp = self.amplitude(x0, x1, 'source')\
                  + self.amplitude(x0, x1, 'receiver')
        return fpp

    def sp_path(self, x0):
        """Path for stationary phase integration"""
        x1 = np.zeros_like(x0)
        return x1

    def eig_pos(self):
        """Stationary points along stationary path"""
        if self.current_surface == 'top':
            # create a conversion from source coords to rcr coords
            rcr_rho_func = lambda x0: self._rho(x0, np.zeros_like(x0))[1]
            x0 = tau_stationary(self.src_top.interper, self.rcr_top.interper,
                                rcr_rho_func)
        x0 = np.array(x0, ndmin=1)
        return x0

    def _get_wf(self, element):
        """Return requested wave front object"""
        wf = None  # is unintilized raise error
        if self.current_surface == 'top':
            if element == 'source':
                wf = self.src_top
            elif element == 'receiver':
                wf = self.rcr_top
        elif self.current_surface == 'wave':
            if element == 'source':
                wf = self.src_wave
            elif element == 'receiver':
                wf = self.rcr_wave
        elif self.current_surface == 'shad':
            if element == 'source':
                wf = self.src_shad
            elif element == 'receiver':
                wf = self.rcr_shad
        if wf is None:
            raise(ValueError('Not a known wavefront or element'))
        return wf

    def _rho(self, x0, x1):
        """src and rcr range to elements on the z=0 surface"""
        x_coords = np.squeeze(np.dstack((x0, x1)))
        if self.global_coord is None:
            rho_s = np.linalg.norm(x_coords, axis=-1)
            x_rcr = self.r_rcr[: 2]
            rho_r = np.linalg.norm(x_coords - x_rcr[None, None, :], axis=-1)
        else: raise(ValueError('2D coordinate grid is not implimented'))
        return rho_s, rho_r


    def _dr_dy(self, x0, x1, dy=2):
        """Calculate the 1st or second order derivative of range wrt y"""
        if self.current_surface == 'top':
            # solve at z = 0
            rho, rho_r = self._rho(x0, x1)
            return 1 / rho, 1 / rho_r
        c1 = (y0 - self.r_rcr[0]) ** 2 + (x2 - self.r_rcr[2]) ** 2
        c2 = x0 ** 2 + (x2 - self.z_src) ** 2
        c3 = self.r_rcr[1]
        if dy == 1:
            f_src = (x1 - c3) / np.sqrt(c1 + (x1 - c3) ** 2)
            f_rcr = x1 / np.sqrt(c2 + x1 ** 2)
        elif dy == 2:
            f_src = c1 / (c1 + (x1 - c3) ** 2) ** (3 / 2)
            f_rcr = c2 / (c2 + x1 ** 2) ** (3 / 2)
        return f_src, f_rcr

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from icepyks.clumber import iso_rg
    from icepyks.clumber.surfaces import sin_surface

    c0 = 1500
    a_bounds = (-20, 220)
    x1 = 5
    z_source = -20
    r_receiver = [200, 0, -10]
    z_bottom = -300
    amp = 1.5  # m
    wave_length = 40  # m

    ray_gun = iso_rg.Isospeed(c0)
    proper = SurfaceField(z_source, r_receiver, z_bottom)
    proper.to_top(ray_gun)

    # plot travel time curve
    r_range = np.r_[0: proper.range_rcr: 300j]
    fig, ax = plt.subplots()
    tau_s = proper.delay(r_range, proper.sp_path(r_range), 'source')
    tau_r = proper.delay(r_range, proper.sp_path(r_range), 'receiver')
    ax.plot(r_range, (tau_s + tau_r) * 1e3, label='top')

    eta = sin_surface.Sine(wave_length, amp, z_off=1)
    proper.to_wave(eta)

    # plot travel time curve
    x_range = np.r_[a_bounds[0]: a_bounds[1]: 300j]
    proper('wave')
    tau_s = proper.delay(x_range, proper.sp_path(x_range), 'source')
    tau_r = proper.delay(x_range, proper.sp_path(x_range), 'receiver')
    ax.plot(r_range, (tau_s + tau_r) * 1e3, 'k', alpha=0.6, label='wave')
    proper('shad')
    tau_s = proper.delay(x_range, proper.sp_path(x_range), 'source')
    tau_r = proper.delay(x_range, proper.sp_path(x_range), 'receiver')
    ax.plot(r_range, (tau_s + tau_r) * 1e3, 'r',
            linewidth=2, label='illuminated')
    ax.set_xlabel('range, m')
    ax.set_ylabel('delay, ms')
    ax.set_title('Total travel time from source to receiver')
    ax.grid()
    ax.legend(loc=9)
    ax.set_ylim(134, 155)

    plt.show(block=False)
