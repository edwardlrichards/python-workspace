"""
======================================
Clumber spaniel :mod:`icepyks.clumber`
======================================

.. currentmodule: icepyks.clumber

Ray-tracing HK integral solver

Solves the HK integral with rectangular quadrature, ray-based Green's
function. Ray tracing provides shadowing corrections, and may additionally
allow for depth variation of sound speed.

Ray gun types
=============

.. autosummary::
   :toctree: generated/

    iso_rg
    c_linear_rg
    ode_rg
"""
