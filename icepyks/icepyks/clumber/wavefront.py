import numpy as np
from scipy.interpolate import UnivariateSpline
from icepyks.clumber.ray_gun import RayGun

class Wavefront:
    """A data structure to hold ray parameters at surface, 1 variable index"""
    def __init__(self, jr_interper, surface_list):
        """surface parameters: DataArray, surface parameters v launch angle"""
        # check if sp is a list, if so link it together
        self.sl = surface_list
        self.interper = jr_interper
        self.bounds = jr_interper.bounds
        self.value_bounds = jr_interper.value_bounds

    def __call__(self, in_range, param_id, is_inverse=False, nu=0):
        """return ray values at given in_range, determined by interpolator
        derivative term is only used in interpolated quantities
        """
        in_range = np.array(in_range, ndmin=1)
        if param_id == self.interper.index_name:
            return in_range
        # is inverse indicates that the in_range is inverted before lookup
        if is_inverse:
            in_range = self.interper(in_range, self.interper.index_name)
            # if param id is also the inverse, don't convert back again
            if param_id == self.interper.inverse_name:
                return in_range
        if param_id == 'ray':  # special request, return ray array
            x_vec = np.array(self.x_vec(in_range), ndmin=2)
            # rays are in r, z coordinates
            r = np.array(np.linalg.norm(x_vec[:, :2], axis=-1), ndmin=2)
            z = np.array(x_vec[:, 2], ndmin=2)
            s_vec = np.array(self.s_vec(in_range), ndmin=2)
            s0 = np.array(np.linalg.norm(s_vec[:, :2], axis=-1), ndmin=2)
            s2 = np.array(s_vec[:, 2], ndmin=2)
            a0 = np.array(self.interper(in_range, 'a0'), ndmin=2)
            tt = np.array(self.interper(in_range, 'tt'), ndmin=2)
            in_range = np.array(in_range, ndmin=2)
            ray_param = np.vstack((r, z, s0, s2, a0, tt, in_range)).T
            return ray_param
        # Allow paramters to be either interpolatated or calculated values
        if param_id in self.interper.param_names:
            return self.interper(in_range, param_id, nu=nu)
        else:
            return self._calc_quantities(in_range, param_id)

    def knots(self, param):
        """Return the x values where an intepolator is defined"""
        return self.interper.knots(param)

    def x_vec(self, index_range):
        """calculate catesian coordinates"""
        # XXX: does not have a 2D grid
        x_top = np.vstack((self(index_range, 'x0'),
                            np.zeros_like(index_range))).T
        z = np.array(self.z(index_range), ndmin=2).T
        x = np.hstack([x_top, z])
        return x.squeeze()

    def s_vec(self, index_range):
        """s_2 is a calculated from horizontal slowness and sound speed"""
        index_range = np.array(index_range, ndmin=1)
        # add off axis component if necassary
        s0 = np.array(self.interper(index_range, 's0'), ndmin=2)
        s2 = np.array(self.interper(index_range, 's2'), ndmin=2)
        if 's1' in self.interper.param_names:
            s1 = np.array(self.interper(index_range, 's1'), ndmin=2)
        else:
            s1 = np.array(np.zeros_like(index_range), ndmin=2)
        return np.dstack((s0, s1, s2)).squeeze()

    def z(self, index_range):
        """z may be a constant or an interpolated parameter"""
        if 'x2' in self.params:
            z = self.interper(index_range, 'x2')
        else:
            # XXX: hardcode of top boundry
            z = np.full_like(index_range, 0.)
        return z

    def amplitude(self, index_range):
        """ray amplitude"""
        amp = self.interper(index_range, 'a0')
        pass

    def arrival_angle(self, index_range, eta):
        """Compute ray angle arrive on fly from ray paramters"""
        s_vec = self.s_vec(index_range)
        s_vec = np.array(s_vec, ndmin=2)
        t_z = np.arctan2(s_vec[:, -1], np.linalg.norm(s_vec[:, :2], axis=-1))
        if self.interper.index_name == 'la':
            phi = np.zeros_like(index_range)
        else:
            phi = self.line.phi(index_range, self.rho_source)
        return np.vstack((t_z, phi)).T

    def reflect_angle(self, index_range, eta):
        """reflect incoming ray from surface, only works for receiver"""
        # XXX: Only works for receiver ray
        x_vec = self.x_vec(index_range)
        x_vec = np.array(x_vec, ndmin=2)
        try:
            n_hat = eta.n_hat(x_vec[:, 0])
            n_hat = np.array(n_hat, ndmin=2)
        except KeyError:
            # assume flat if eta is not defined
            n_hat = np.zeros_like(x_vec); n_hat[:, -1] = 1
        householder = [np.identity(3) - 2 * np.outer(a, a) for a in n_hat]
        s_vec = self.s_vec(index_range)
        s_vec = np.array(s_vec, ndmin=2)
        ra = [np.dot(hh, sv) for hh, sv in zip(householder, s_vec)]
        ra = np.array(ra, ndmin=2)
        ra[:, 2] = -ra[:, 2]
        return ra

    @property
    def normal(self):
        """ID is held in the sp Datasat"""
        try:
            norm =  self.line.normal
        except AttributeError:
            raise(AttributeError('wave front in radial coordinates'))
        return norm

    @property
    def params(self):
        """parameters are values found by interpolation"""
        return self.interper.param_names

    @property
    def defined_regions(self):
        """Return list of bounding boxes for wavefront"""
        return self.interper.defined_regions

    def _calc_quantities(self, index_range, param):
        """a common method for the lookup of calculated quantities"""
        value = getattr(self, param)(index_range)
        return value.squeeze()
