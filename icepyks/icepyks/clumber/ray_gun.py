"""
Wrapper for ray guns. For convience ray guns are written with z positive down,
and HK integral is written with z positive up. RayGun class is called using
the HK convention, and manages the switch of coordinate systems internally.
"""
import numpy as np

class RayGun:
    """standarizes behavior of ray guns"""
    def __init__(self, z_bottom, z_const):
        """All ray guns should know this"""
        self.param_list = ['x0', 'x2', 's0', 's2', 'a0', 'tt', 'la']
        self.index_name = 'la'  # launch angle is ray index
        self.z_bottom = z_bottom
        self.z_const = z_const

    def __call__(self, z_source):
        """This is the method used to specify the source depth"""
        # make raygun think z_const is z=0
        self.z_source = float(z_source - self.z_const)
        # ssp should be implimented in the ray gun
        self.c_source = self.ssp(-z_source)

    def to_boundry(self, la_vector, r_max):
        """Returns parameters indexed by p at nearest boundry"""
        # setup boundries, NaN is used to indicate no specification
        # z boundries are positve for ray guns
        z_bounds = np.array((0., -(self.z_bottom - self.z_const)))
        # initilize a DataArray of rays from source
        params = self._init_ray(la_vector)
        if len(params.shape) == 1:
            rg_out = [self._one_to_boundry(params, z_bounds, r_max)]
        else:
            rg_out = [self._one_to_boundry(p, z_bounds, r_max)
                           for p in params]
        # flip back the sign of z before returning result
        rays = np.array(rg_out, ndmin=2)
        rays[:, 1] *= -1
        rays[:, 3] *= -1
        return rays

    def _one_to_boundry(self, param, z_bounds, r_max):
        """This is specific to ray gun implimentation"""
        pass

    def _init_ray(self, la):
        """Create a ray at the source, blank initial conditions"""
        la = np.array(la, ndmin=1)
        s0 = np.cos(la) / self.c_source
        """create a initial ray parameter vector"""
        s2 = np.sin(la) / self.c_source
        x0 = np.zeros(la.shape)
        x2 = self.z_source * np.ones(la.shape)
        a0 = np.zeros(la.shape)
        tt = np.zeros(la.shape)
        # Flip the sign of z source before calling ray gun
        blank_param = np.vstack((x0, -x2, s0, -s2, a0, tt, la)).T
        return np.array(blank_param, ndmin=2)
