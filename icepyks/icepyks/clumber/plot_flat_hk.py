"""
================================
Equivalence of H and HK solution
================================

High frequency pulse reflected from a flat surface, short range
and fc = 15 kHz.
"""

import numpy as np
import xarray as xr
from scipy.special import hankel1
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.clumber import iso_rg, f_synthesis, point_iso_field
from icepyks.clumber.surfaces import flat_surface
from concurrent.futures import ProcessPoolExecutor

# Perform HK integration using a sine surface

c0 = 1500
z_source = -2.723
r_receiver = np.array((16.542, 0, -1.906))
z_bottom = -4

#signal parameter
fc = 15e3
f_min = 5e3
f_max = 25e3
freq = np.arange(f_min, f_max)

#Setup integration bounds
x_bounds = (-4, 14)
fs = f_max * 3
dx = c0 / fc / 5
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

NFFT = int(2 ** np.ceil(np.log2(fs)))
signal_f = np.arange(NFFT, dtype=np.float_) / NFFT * fs
i_test = np.argmin(np.abs(signal_f - fc))

spec_r = np.sqrt((z_source + r_receiver[2]) ** 2 + r_receiver[0] ** 2)
spec_tau = spec_r / c0
taxis = np.arange(0., spec_tau + 4e-3, 1 / fs)

# solve for pressure field at surface
ray_gun = iso_rg.Isospeed(c0, z_bottom, z_const=0)
eta = flat_surface.Eta(z_off=0.1)
proper = point_iso_field.IsospeedPoint(z_source, eta)

hk_freq = surface_integral.FSurfaceIntegral(proper, xaxis, 'HK')

HK_result = hk_freq(f)
HK_result = []


import matplotlib.pyplot as plt
cmap = plt.cm.RdBu_r

fig, ax = plt.subplots()
k_freq = 2 * np.pi * np.array(result_f) / c0
ax.plot(result_f, np.sqrt(2 / np.pi / k_freq) * np.abs(result[:, 0]), '.')
ax.plot(result_f, np.abs(result[:, 1]), '.')
ax.plot(result_f, np.sqrt(2 / np.pi / k_freq / spec_r))
plt.show(block=False)
