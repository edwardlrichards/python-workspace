"""
==================================
Waltead and Dean's tank experiment
==================================

High frequency pulse reflected from a single wave.
"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
from icepyks import pulse_signal, signal_interp
from icepyks.clumber import surface_field, surface_integral, iso_rg, eigen_rays
from icepyks.clumber.surfaces import sin_surface

FFMpegWriter = manimation.writers['ffmpeg']
metadata = dict(title='Movie Test', artist='Matplotlib',
                comment='Movie support!')
writer = FFMpegWriter(fps=3, metadata=metadata)

cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 0.693
wave_per = 2 * np.pi / np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 31e-3 / 2
num_phase = 60
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (np.pi / 2)

c0 = 1469
z_source = -0.2
r_receiver = np.array((1.215, 0, -0.14))
z_bottom = -1
z_const = -(wave_amp + 0.01)

# Test out if measurements are from Highest water point
r_receiver[2] = r_receiver[2] + 2 * wave_amp
z_source += 2 * wave_amp

#signal parameter
fc = 200e3

#Setup integration bounds
x_bounds = (-0.5, 1.75)
#taxis is from Walstead and Dean
t_bounds = (841e-6, 881e-6)
fs = 5e6
dx = 3e-3
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# solve for pressure field at surface
ray_gun = iso_rg.Isospeed(c0)
proper = surface_field.SurfaceField(z_source, r_receiver, z_bottom, z_const,
                                    tt_tol=5e-7, range_buffer=1)
proper.to_top(ray_gun)

# initialize xmitted signal
signal = pulse_signal.pulse()
i_er_signal = signal_interp.ContinuousSignal(signal)

eigen_x = []
eta_z = []
taus = []
t_series = []

for p in wave_phase[::-1]:
    eta = sin_surface.Sine(lamda_surf, wave_amp, phase=p)
    eta_z.append(eta.z(xaxis))
    proper.to_wave(eta)
    proper('wave')
    hk_solver = surface_integral.TimeHK(proper, xaxis, taxis, i_er_signal)
    eigs = eigen_rays.EigenRays(hk_solver)
    eigen_x.append(eigs.eig_x)
    taus.append(eigs.tau(xaxis))
    t_series.append(hk_solver())

t_series = np.array(t_series)
N, T = np.meshgrid(wave_phase / 2 / np.pi / wave_per , (taxis - taxis[0]) * 1e6)
N = N - np.min(N)

fig, ax = plt.subplots(nrows=3)

ax[2].pcolormesh(N, T, t_series.T, vmin=-1, vmax=1, cmap=cmap)
ax[2].set_ylim(0, 35)
ax[2].set_xlim(0, np.max(N))
ax[2].set_yticks([])
ax[2].set_xticks([])

with writer.saving(fig, "eigen_rays.mp4", len(eta_z)):
    for i, e in enumerate(zip(eigen_x, eta_z, taus)):
        ei, ez, ts = e
        # eigenray plot
        ax[0].plot(0, z_source, 'g*')
        ax[0].plot(r_receiver[0], r_receiver[2], 'k*', alpha=0.4)
        ax[0].plot(xaxis, ez, 'b')
        for e in ei:
            zi = np.argmin(np.abs(e - xaxis))
            ax[0].plot([0, e], [z_source, ez[zi]], 'k')
            ax[0].plot([e, r_receiver[0]], [ez[zi], r_receiver[2]], 'k')
        ax[0].set_yticks([])
        ax[0].set_xticks([])
        ax[0].set_ylabel('z')
        ax[0].set_xlabel('x distance')

        # travel time curve
        ax[1].plot(xaxis, ts, 'b')
        for e in ei:
            zi = np.argmin(np.abs(e - xaxis))
            ax[1].plot(e, ts[zi], 'k*')
        ax[1].set_ylim(0.00084, 0.0009)
        ax[1].set_yticks([])
        ax[1].set_xticks([])
        ax[1].set_xlabel('x distance')
        ax[1].set_ylabel('delay')

        # current position in time
        t_cur = N[0, i]
        j = ax[2].plot([t_cur, t_cur], ax[2].get_xlim(), 'k')
        ax[2].set_xlabel('time')
        ax[2].set_ylabel('delay')

        plt.show(block=False)
        writer.grab_frame()
        ax[0].clear()
        ax[1].clear()
        j.pop(0).remove()

plt.show(block=False)
