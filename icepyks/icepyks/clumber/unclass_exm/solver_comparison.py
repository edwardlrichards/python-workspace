"""
===========================================
Surface integral equation solver comparison
===========================================
"""
import numpy as np
from scipy.special import hankel1
import matplotlib.pyplot as plt
from icepyks import pulse_signal
from icepyks.clumber import iso_rg, ray_field, isospeed_integral
from icepyks.surfaces import sin_surface

z_src = -20
r_rcr = np.array([200., 0, -10])
z_bottom = -40

c0 = 1500  # m/s

# signal parameters
fc = 23000
fmin = fc * 0.5  # assume q=1
fmax = fc * 1.5  # assume q=1

# wave param
wave_l = 40
amp = 1.5

# quadrature specification
x_bounds = (-20, 220)
dx = c0 / fmin / 6
x_axis = np.arange(x_bounds[0], x_bounds[1], dx)
weights = np.ones(x_axis.shape)

# setup broadband synthesis
signal = pulse_signal.pulse_q1(fc)

# setup the HK integral
eta = sin_surface.Sine(wave_l, amp)
ray_gun = iso_rg.Isospeed(c0, z_bottom, -(amp+0.1))
proper = ray_field.RayField(eta, z_src, r_rcr)
proper.to_top(ray_gun)
proper.to_wave(eta)

hk_er = isospeed_integral.FreqHelm(proper, x_axis, 'HK')
helm_1 = isospeed_integral.FreqHelm(proper, x_axis, 1)
dp_dn_HK = hk_er.dp_dn(fmin)
dp_dn_H1 = helm_1.dp_dn(fmin)

fig, ax = plt.subplots()
#Plot the absolute value of HK and HIE
ax.plot(x_axis, np.abs(dp_dn_HK), label='HK')
ax.plot(x_axis, np.abs(dp_dn_H1), label='HIE')
ax.grid()
ax.legend()
ax.set_title('amplitude of source normal derivative')
#ax.set_ylim(-0.2, 9.5)
ax.set_xlim(*x_bounds)

# unwrap phase to get travel time, needs contious field
hk_er.field('wave')
# create a reference time for unwraped phase
ref_i = 100  # random choice
x_ref = x_axis[ref_i]
t_ref = hk_er.field.delay(x_ref, 0, 'source')\
        + hk_er.field.delay(x_ref, 0, 'receiver')
fig, ax = plt.subplots()
phase = np.unwrap(np.angle(hk_er.dp_dn(fc) * hk_er.rcr_greens(r_rcr, fc)))
tau = phase / 2 / np.pi / fc
offset = t_ref - tau[ref_i]
tau = tau + offset
tau_dir = hk_er.field.delay(x_axis, 0, 'source')\
            + hk_er.field.delay(x_axis, 0, 'receiver')
ax.plot(x_axis, tau * 1e3, label='HK unwrap')
ax.plot(x_axis, tau_dir * 1e3, linewidth=2, alpha=0.4, label='direct')
ax.set_xlabel('range, m')
ax.set_ylabel('delay, ms')
ax.set_ylim(135, 138)
#ax.legend(loc=0)
ax.grid()

plt.show(block=False)
