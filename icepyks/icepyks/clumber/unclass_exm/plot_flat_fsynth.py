"""
==========================================
Equivalence of time and frequency solution
==========================================

High frequency pulse reflected from a flat surface, short range
and fc = 15 kHz.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp, f_synthesis, surface_integral
from icepyks.clumber import ray_field, iso_rg
from icepyks.surfaces import sin_surface

#cmap = plt.cm.RdBu_r
# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 9.435
wave_amp = 0.422 / 2
num_phase = 120
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j] - (np.pi / 2)

c0 = 1500
z_source = -2.723
r_receiver = np.array((16.542, 0, -1.906))
z_bottom = -4
z_off = -(wave_amp + 0.01)

#signal parameter
fc = 15e3
f_min = 5e3
f_max = 25e3

#Setup integration bounds
x_bounds = (-4, 20)
fs = f_max * 3
dx = c0 / fc / 5
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

NFFT = int(2 ** np.ceil(np.log2(fs)))
signal_f = np.arange(NFFT, dtype=np.float_) / NFFT * fs
sig_i = np.bitwise_and(signal_f >= f_min, signal_f <= f_max)
i_start = np.argmax(sig_i)

spec_r = np.sqrt((z_source + r_receiver[2]) ** 2 + r_receiver[0] ** 2)
spec_tau = spec_r / c0
taxis = np.arange(0., spec_tau + 4e-3, 1 / fs)

# solve for pressure field at surface
ray_gun = iso_rg.Isospeed(c0, z_bottom, z_off)
eta = sin_surface.Sine(lamda_surf, wave_amp)
proper = ray_field.RayField(eta, z_source, r_receiver,
                            tt_tol=5e-6, range_buffer=6)
proper.to_top(ray_gun)

# initialize xmitted signal
pulse, tsignal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(pulse, tsignal)
# interpolate signal to inverted frequencies
pulse_FT = f_synthesis.signal_FT(pulse, signal_f)

hk_freq = surface_integral.FSurfaceIntegral(proper, xaxis, 'HK')
signal_FT = np.zeros(signal_f.shape) + 0j

for f_i, f in enumerate(signal_f[sig_i]):
    dx = c0 / f / 8
    xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
    hk_freq.xaxis = xaxis
    signal_FT[f_i + i_start] = hk_freq(f)

hk_freq = np.array(pulse_FT) * signal_FT
hk_freq = f_synthesis.make_2sided(hk_freq)
hk_freq = xr.DataArray(hk_freq, dims=['Hz'], coords=[signal_f])

f_synth = f_synthesis.synthesis_ts(hk_freq)

hk_time = surface_integral.TimeHK(proper, xaxis, taxis, i_er_signal)
t_series = hk_time()

fig, ax = plt.subplots()
ax.plot(taxis, t_series, 'b', linewidth=2, alpha=0.6)
ax.plot(f_synth.sec, f_synth, 'g')
ax.plot([-100, 100], [1 / spec_r, 1 / spec_r], 'r--')
ax.plot([-100, 100], [-1 / spec_r, -1 / spec_r], 'r--')
ax.set_xlim(0, 6)
plt.show(block=False)




NFFT = int(2 ** np.ceil(np.log2(t_series.size)))
time_FT = np.fft.fft(t_series, NFFT)
time_f = np.arange(NFFT) / NFFT * fs

fig, ax = plt.subplots()
ax.plot(signal_f, np.abs(hk_freq), 'b', linewidth=2, alpha=0.6)
ax.plot(time_f, np.abs(time_FT), 'g')
ax.plot(signal_f, np.abs(pulse_FT) / spec_r, 'r--',
        linewidth=2, alpha=0.6)
#ax.set_xlim(5e3, 25e3)

# plot time domain version of signals
t_ff = np.fft.ifft(time_FT)
df_time = time_f[1] - time_f[0]
ref_ff = np.fft.ifft(hk_freq)

ffig, ax = plt.subplots()
ax.plot(np.arange(ref_ff.size) / df, ref_ff, 'b', linewidth=2, alpha=0.6)
ax.plot(np.arange(t_ff.size) / 2 / np.pi / df_time, t_ff, 'g')
ax.plot(taxis, t_series, 'g--')
ax.plot([-100, 100], [1 / spec_r, 1 / spec_r], 'r--')
ax.plot([-100, 100], [-1 / spec_r, -1 / spec_r], 'r--')
ax.set_xlim(0, 6)
plt.show(block=False)
