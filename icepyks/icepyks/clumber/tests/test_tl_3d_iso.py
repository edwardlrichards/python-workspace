"""
simple iso speed case to get reference ray scaling
"""
import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber.surfaces import flat_surface
from icepyks.clumber.surface_field import SurfaceField
from icepyks.clumber import surface_integral, iso_rg
from atpy import bellhop
import os

c0 = 1500
z_source = -25.
z_rcr = -200.
rho_range = np.r_[50: 5000: 200j]
fc = 150
z_bottom = -5e3

# computed values
w = 2 * np.pi * fc
dx = c0 / fc / 8

# setup incident field
rg = iso_rg.Isospeed(c0)

rays_1d = []
rays = []
for r_r in rho_range:
#for r_r in rho_range[:1]:
    r_rcr = [r_r, 0, z_rcr]
    surf_in = SurfaceField(z_source, r_rcr, z_bottom)
    surf_in.to_top(rg)
    x_axis_buffer = 1.2 * r_r
    num_x = np.ceil((r_r + x_axis_buffer) / dx)
    x_range = np.arange(num_x) * dx - (x_axis_buffer / 2)
    hk_er = surface_integral.FreqHK(surf_in, x_range,
                                    np.ones_like(x_range), num_dims=3)
    rad_ray = hk_er.direct_ray(fc)
    # add direct path and bounce rays
    rays_1d.append(rad_ray - hk_er(fc))
    rays.append((rad_ray, hk_er(fc)))

rays_1d = np.array(rays_1d)
tl_hk = 20 * np.log10(np.abs(np.array(rays_1d)))

# iso llyods mirror
k = w / c0
r1 = np.sqrt(rho_range ** 2 + (z_rcr - z_source) ** 2)
r2 = np.sqrt(rho_range ** 2 + (z_rcr + z_source) ** 2)

p_p = lambda r_r: np.exp(1j * k * r_r) / r_r

# simple iso-speed addition
p_1d = p_p(r1) - p_p(r2)
tl_ll = 20 * np.log10(np.abs(np.array(p_1d)))

# plot TL curves
fig, ax = plt.subplots()
ax.plot(rho_range / 1e3, -tl_ll, 'b', linewidth=3, alpha=0.6, label='image')
ax.plot(rho_range / 1e3, -tl_hk, 'r', label='HK')
#ax.set_ylim(60, 10)
ax.set_xlim(0, 5)
ax.set_ylabel('TL')
ax.set_xlabel('range, km')
ax.legend()
ax.grid()

fig, ax = plt.subplots()
#ax.plot(rho_range / 1e3, np.abs(p_p(r1)), 'b')
ax.plot(rho_range / 1e3, np.real(p_p(r1)), 'b')
ax.plot(rho_range / 1e3, np.imag(p_p(r1)), 'b--')
#ax.plot(rho_range / 1e3, np.abs([r[0] for r in rays]),'g')
ax.plot(rho_range / 1e3, np.real([r[0] for r in rays]),'g')
ax.plot(rho_range / 1e3, np.imag([r[0] for r in rays]), 'g--')

fig, ax = plt.subplots()
#ax.plot(rho_range / 1e3, np.abs(p_p(r2)), 'b')
ax.plot(rho_range / 1e3, np.real(p_p(r2)), 'b')
ax.plot(rho_range / 1e3, np.imag(p_p(r2)), 'b--')
#ax.plot(rho_range / 1e3, np.abs([r[1] for r in rays]),'g')
ax.plot(rho_range / 1e3, np.real([r[1] for r in rays]),'g')
ax.plot(rho_range / 1e3, np.imag([r[1] for r in rays]), 'g--')


plt.show(block=False)
