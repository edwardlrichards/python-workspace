"""
compaire first iterate results to reference solution.
"""
import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber.surface_interaction import c_lin_flat
from icepyks.clumber.surface_field import SurfaceField
from icepyks.clumber.c_linear_rg import CLinear
from atpy import bellhop
import os

m = -0.3
#m = -0.01
c0 = 1500
z_source = -25.
z_rcr = -200.
rho_range = np.r_[50: 5000: 200j]
fc = 150
z_bottom = -5e3
x_axis_buffer = 200

# computed values
w = 2 * np.pi * fc
k_src = w / (c0 + m * z_source)
dx = c0 / fc / 8

# setup incident field
rg = CLinear(m, c0)

# setup surface interaction field
surf_er = c_lin_flat(m, fc, c0=c0, num_dims=2)  # upward refracting profile
# common frequency dependent correction point to line source conversion
point_to_line = np.sqrt(2 / np.pi / k_src) * np.exp(-1j * np.pi / 4)

# HK solution
def hk_1d(inc_field):
    """compute scatter amplitude for a given range"""
    # need to impliment a custom hk for single frequency propagation
    y_range = np.zeros_like(x_range)
    src_amp = inc_field.amplitude(x_range, y_range, 'source', is_gradient=True)
    rcr_amp = inc_field.amplitude(x_range, y_range, 'receiver')
    # delays relate to the phase term
    delays = inc_field.delay(x_range, y_range, 'source')\
             + inc_field.delay(x_range, y_range, 'receiver')
    i_grand = src_amp * rcr_amp * np.exp(1j * w * delays)
    # a product of point sources needs to be advanced in phase 2x
    i_grand *= point_to_line * np.exp(-1j * np.pi / 4)
    # frequency domain solution, put in derivative term directly
    i_grand *= 1j * w / c0  # derivative term correction
    # Basic quadrature
    surf_result = np.nansum(i_grand) * dx / 2 / 1j
    # Direct ray calculation
    rad_amp, rad_tau = inc_field.rad_ray
    # radiated ray is also corrected for derivative term
    rad_ray = rad_amp * np.exp(1j * w * rad_tau)
    # green's function correction
    #rad_ray *= np.sqrt(2 / np.pi / k_src) * np.exp(1j * np.pi / 4)
    rad_ray *= point_to_line
    return surf_result, rad_ray

rays_1d = []
for r_r in rho_range:
    r_rcr = [r_r, 0, z_rcr]
    surf_in = SurfaceField(z_source, r_rcr, z_bottom,
                           range_buffer=x_axis_buffer, num_dims=2)
    surf_in.to_top(rg)
    num_x = np.ceil((r_r + x_axis_buffer) / dx)
    x_range = np.arange(num_x) * dx - (x_axis_buffer / 2)
    rays_1d.append(hk_1d(surf_in))

# add direct path and bounce rays
p_1d = [complex(tr[1] - tr[0]) for tr in rays_1d]
tl_1d = 20 * np.log10(np.abs(np.array(p_1d)))

# reference solution
dir_name = os.getcwd()
kraken_file = 'envs/c_linear_kraken2d'
krak_r, krak_p = bellhop.readSHD(os.path.join(dir_name, kraken_file))
krak_p = 20 * np.log10(np.abs(np.squeeze(krak_p)))

def load_plt(file_name):
    """load the oases plot format into numpy"""
    specta = []
    tl = []
    with open(file_name, 'r') as f:
        is_value = True
        for line in f:
            if line == '\n':
                is_value = False
                continue
            if is_value: specta.append(float(line))
            else: tl.append(float(line))
    specta = np.array(specta)
    tl = np.array(tl)
    return specta, tl

#oases_point = '/home/e2richards/python-workspace/icepyks/clumber/envs/c_lin_point.plt'
oases_line = '/home/e2richards/python-workspace/icepyks/clumber/envs/c_lin_line.plt'
#dx = 0.199976E-02  # iso
dx = 0.366211E-02

o_data, o_i = load_plt(oases_line)

# plot TL curves
fig, ax = plt.subplots()
ax.plot(krak_r / 1e3, -krak_p, 'k', linewidth=2, alpha=0.4, label='kraken')
ax.plot(np.arange(o_i.size) * dx, o_i, linewidth=2, alpha=0.6, label='oases')
ax.plot(rho_range / 1e3, -tl_1d, 'r', label='HK result')
ax.set_ylim(60, 10)
ax.set_xlim(0, 5)
ax.set_ylabel('TL')
ax.set_xlabel('range, km')
ax.set_title('Comparison of 1d HK to reference solutions \n' +
             'Line source')
ax.legend(loc=8)
ax.grid()

plt.show(block=False)

fig, ax = plt.subplots()
ax.plot(rho_range, np.array([np.abs(tr[0]) for tr in rays_1d]),
        'b', label='bounce')
ax.plot(rho_range, np.array([np.abs(tr[1]) for tr in rays_1d]),
        'r', label='direct')
ax.legend()

plt.show(block=False)
