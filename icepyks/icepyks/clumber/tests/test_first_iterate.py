"""
Plots related to the process of the first iterate. C linear profile.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber.surface_interaction import c_lin_flat
from icepyks.clumber.surface_field import SurfaceField
from icepyks.clumber.c_linear_rg import CLinear

m = -0.3
c0 = 1500
z_source = -25.
z_rcr = -200.
x_rcr = 4800.
x_oi = 3020.  # point with computed total travel time
fc = 150
z_bottom = -5e3

# computed values
w = 2 * np.pi * fc
r_rcr = np.array([x_rcr, 0., z_rcr])

# setup incident field
rg = CLinear(m, c0)
surf_in = SurfaceField(z_source, r_rcr, z_bottom)
surf_in.to_top(rg)
x_range = np.r_[-20: x_rcr + 20: 300j]  # used when plotting surface values
rel_x = x_oi - x_range

# setup surface interaction field
surf_er = c_lin_flat(m, fc, c0=c0)  # negative slope is upward refracting
rho_range = np.abs(x_range)
rel_rho = np.abs(rel_x)
la_range = surf_er.range_to_angle(rho_range)

if np.any(la_range > 0):
    raise(ValueError('Ray is leaving the domain'))

# plots
fig, ax = plt.subplots(2, sharex=True)
ax[0].plot(x_range, surf_in.amplitude(x_range, np.zeros_like(x_range),
                                      'source', is_gradient=True),
           label='$p_{inc}$')
ax[0].plot(x_range, surf_er.ray_amplitude(rel_rho, is_normal=True),
           label='$p_{surf}$')
ax[0].set_yscale('log')
ax[0].set_title(r'$\left|\frac{\partial p}{\partial n^{\prime}}\right|$')
ax[0].grid()
ax[0].legend()
ax[1].plot(x_range,
           surf_in.delay(x_range, np.zeros_like(x_range), 'source') * 1e3)
ax[1].plot(x_range, surf_er.range_to_tt(rel_rho) * 1e3)
ax[1].set_title(r'$\tau(x)$')
ax[1].set_ylabel('ms')
ax[1].set_xlabel('m')
ax[1].grid()

# second derivative of total travel time wrt y
fig, ax = plt.subplots()
tau_self = np.squeeze(surf_er.d2tau_dy2(rel_rho))
tau_source = np.squeeze(surf_in.tau_d2dy2(x_range,
                                          np.zeros_like(x_range), 'source'))

fpp = tau_self + tau_source
ax.plot(x_range, fpp * 1e3)
ax.plot(x_range, tau_self * 1e3)
ax.plot(x_range, tau_source * 1e3)
ax.grid()
ax.set_ylim(0, 0.07)
ax.set_title('Second derivative of tau wrt y')

t_l = np.squeeze(surf_er.dtau_drho(rel_rho))
t_s = np.squeeze(surf_in.src_top(x_range, 'tau', nu=1))


fig, ax = plt.subplots()
ax.plot(x_range, t_l)
ax.plot(x_range, t_s)
ax.set_title(r'derivative of tau wrt $\rho$')
plt.show(block=False)
