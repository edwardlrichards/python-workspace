import matplotlib.pyplot as plt
import numpy as np
from scipy.special import hankel1
from icepyks.newfie import f_synthesis, the_hk, the_helm, sine_surface
from icepyks.porty import hk_integral, iso_speed, surface_field
from concurrent.futures import ThreadPoolExecutor
import datetime
import os

r_src = np.array((0., -25))
r_rcr = np.array((100., -200))  # Creates obvious shadowing
x_bounds = np.array((-200, 400))

c = 1500  # m/s
sr_dist = np.linalg.norm(r_src - r_rcr)  # m
# signal parameters
fc = 150

# Sine wave parameters
lamda_surf = 25
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 1e-3
phase = np.deg2rad(0.)  # Shadowed 80m caustic feature

# quadrature specification
dx = c / fc / 5
x_range = np.arange(x_bounds[0], x_bounds[1], dx)
weights = np.ones(x_range.shape)
# May as well do a trapazoid rule
#weights[0] = .5; weights[-1] = 0.5
weights[:3] = [1/3, 4/3, 1/3]
weights[-3:] = [1/3, 4/3, 1/3]
weights *= dx

# setup the HK integral
eta = sine_surface.Sine(lamda_surf, wave_amp, phase=phase)
helm_er = the_helm.HIE(r_src, r_rcr, eta, x_range)

w_test = 2 * np.pi * fc
k_test = w_test / c
dp_HK = helm_er.dp_dn(k_test, kind='HK')
dp_1st = helm_er.dp_dn(k_test, kind=1)
dp_2nd = helm_er.dp_dn(k_test, kind=2)

fig, ax = plt.subplots()
ax.plot(x_range, np.abs(dp_HK), label='HK')
ax.plot(x_range, np.abs(dp_1st), label='1st')
ax.plot(x_range, np.abs(dp_2nd), label='2nd')
ax.legend()
ax.grid()
ax.set_xlabel('range, m')
ax.set_xlim(-60, 150)
ax.set_ylabel(r'$|\partial p / \partial n|$')
ax.set_title('Normal derivative of pressure at surface\n' +
             'Wave height = %.1f, length = %.1f m'%(wave_amp, lamda_surf))
plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT

fig, ax = plt.subplots()
ax.plot(x_range, np.unwrap(np.angle(dp_HK)) / w_test, label='HK')
ax.plot(x_range, np.unwrap(np.angle(dp_1st)) / w_test, label='1st')
ax.plot(x_range, np.unwrap(np.angle(dp_2nd)) / w_test, label='2nd')
ax.legend()
ax.grid()
ax.set_xlabel('range, m')
ax.set_xlim(-60, 150)
ax.set_ylabel(r'$|\partial p / \partial n|$')
ax.set_title('Normal derivative of pressure at surface\n' +
             'Wave height = %.1f, length = %.1f m'%(wave_amp, lamda_surf))

plt.show(block=False)
# draw enviornment
normal_x = [12, 50]
norm_i = [np.argmin(np.abs(helm_er.surf[:, 0] - nx)) for nx in normal_x]
fig, ax = plt.subplots()
ax.plot(helm_er.surf[:, 0], helm_er.surf[:, 1])
ax.plot(helm_er.r_source[0], helm_er.r_source[1], 'g*')
ax.plot(helm_er.r_receiver[0], helm_er.r_receiver[1], 'k*')
[ax.arrow(helm_er.surf[ix, 0], helm_er.surf[ix, 1], helm_er.src_hat[ix, 0],
          helm_er.src_hat[ix, 1], head_width=1, head_length=0.5, fc='k')
    for ix in norm_i]
[ax.arrow(helm_er.surf[ix, 0], helm_er.surf[ix, 1], helm_er.n_hat[ix, 0],
          helm_er.n_hat[ix, 1], head_width=1, head_length=0.5, fc='k')
    for ix in norm_i]


ax.set_ylim(-12, 2)
ax.set_xlim(-40, 120)

plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
