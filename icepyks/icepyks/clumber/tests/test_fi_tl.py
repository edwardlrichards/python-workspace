"""
compaire first iterate results to reference solution.
"""
import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber.surface_interaction import c_lin_flat
from icepyks.clumber.surface_field import SurfaceField
from icepyks.clumber.c_linear_rg import CLinear
from atpy import bellhop
import os

m = -0.3
c0 = 1500
z_source = -25.
z_rcr = -200.
rho_range = np.r_[50: 5000: 20j]
fc = 150
z_bottom = -5e3
x_axis_buffer = 200

# computed values
w = 2 * np.pi * fc
dx = c0 / fc / 5

# setup incident field
rg = CLinear(m, c0)

# setup surface interaction field
surf_er = c_lin_flat(m, fc, c0=c0)  # negative slope is upward refracting

# HK solution
def hk_1d(inc_field):
    """compute scatter amplitude for a given range"""
    # need to impliment a custom hk for single frequency propagation
    y_range = np.zeros_like(x_range)
    src_amp = inc_field.amplitude(x_range, y_range, 'source', is_gradient=True)
    rcr_amp = inc_field.amplitude(x_range, y_range, 'receiver')
    weights = src_amp * rcr_amp / 2 / np.pi / c0
    # correct for stationary phase
    fpp = inc_field.tau_d2dy2(x_range, y_range, 'source')\
          + inc_field.tau_d2dy2(x_range, y_range, 'receiver')
    weights = weights * np.sqrt(2 * np.pi / np.abs(fpp))
    # delays relate to the phase term
    delays = inc_field.delay(x_range, y_range, 'source')\
             + inc_field.delay(x_range, y_range, 'receiver')
    # frequency domain solution, put in derivative term directly
    derv_term = np.exp(-1j * np.pi / 4) * np.sqrt(w)
    i_grand = weights * np.exp(1j * w * delays)
    # Basic quadrature
    surf_result = derv_term * np.nansum(i_grand) * dx
    rad_amp, rad_tau = inc_field.rad_ray
    return surf_result, rad_amp * np.exp(1j * w * rad_tau)

rays_1d = []
for r_r in rho_range:
    r_rcr = [r_r, 0, z_rcr]
    surf_in = SurfaceField(z_source, r_rcr, z_bottom)
    surf_in.to_top(rg)
    num_x = np.ceil((r_r + x_axis_buffer) / dx)
    x_range = np.arange(num_x) * dx - (x_axis_buffer / 2)
    rays_1d.append(hk_1d(surf_in))

# add direct path and bounce rays
p_1d = [tr[1] - tr[0] for tr in rays_1d]
tl_1d = 20 * np.log10(np.abs(np.array(p_1d)))

# reference solution
dir_name = os.getcwd()
kraken_file = 'envs/c_linear_kraken3d'
krak_r, krak_p = bellhop.readSHD(os.path.join(dir_name, kraken_file))
krak_p = 20 * np.log10(np.abs(np.squeeze(krak_p)))

# plot TL curves
fig, ax = plt.subplots()
ax.plot(krak_r / 1e3, -krak_p, 'k', linewidth=2, alpha=0.4, label='kraken')
ax.plot(rho_range / 1e3, -tl_1d, 'r.', label='HK result')

ax.set_ylim(90, 40)
ax.set_xlim(0, 5)
ax.set_ylabel('TL')
ax.set_xlabel('range, km')
ax.legend()
ax.grid()

fig, ax = plt.subplots()
ax.plot(rho_range, np.array([np.abs(tr[0]) for tr in rays_1d]))
ax.plot(rho_range, np.array([np.abs(tr[1]) for tr in rays_1d]))

plt.show(block=False)
