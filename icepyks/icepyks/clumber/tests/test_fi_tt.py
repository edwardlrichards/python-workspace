"""
compaire first iterate results to reference solution.
"""
import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber.surface_interaction import c_lin_flat
from icepyks.clumber.surface_field import SurfaceField
from icepyks.clumber.c_linear_rg import CLinear

m = -0.3
c0 = 1500
z_source = -25.
z_rcr = -200.
x_rcr = 1500.
fc = 150
z_bottom = -5e3
x_axis_buffer = 50
num_y = 100  # number of y points in 2-D integration

# computed values
w = 2 * np.pi * fc
dx = c0 / fc / 4.
y_range = np.arange(num_y) * dx
y_range -= np.mean(y_range)

# setup incident field
rg = CLinear(m, c0)

# setup surface interaction field
surf_inter = c_lin_flat(m, fc, c0=c0)  # negative slope is upward refracting

# HK solution
def hk_1d(inc_field):
    """compute scatter amplitude for a given range"""
    # need to impliment a custom hk for single frequency propagation
    y_range = np.zeros_like(x_range)
    src_amp = inc_field.amplitude(x_range, y_range, 'source', is_gradient=True)
    rcr_amp = inc_field.amplitude(x_range, y_range, 'receiver')
    weights = src_amp * rcr_amp / 2 / np.pi / c0
    # correct for stationary phase
    fpp = inc_field.tau_d2dy2(x_range, y_range, 'source')\
          + inc_field.tau_d2dy2(x_range, y_range, 'receiver')
    weights = weights * np.sqrt(2 * np.pi / np.abs(fpp))
    # delays relate to the phase term
    delays = inc_field.delay(x_range, y_range, 'source')\
             + inc_field.delay(x_range, y_range, 'receiver')
    # frequency domain solution, put in derivative term directly
    derv_term = np.exp(-1j * np.pi / 4) * np.sqrt(w)
    #i_grand = weights * np.exp(1j * w * delays)
    # Basic quadrature
    #surf_result = derv_term * np.nansum(i_grand) * dx
    return weights, delays

# first iterate
def fi(inc_field):
    """Compute the incident pressure for the HK integral"""
    # only solve for incident pressure along x axis
    X, Y = np.meshgrid(x_range, y_range)
    # Interaction matrix
    rho = np.sqrt((X[:, :, None] - x_range) ** 2 + Y[:, :, None] ** 2)
    G = surf_inter.ray_amplitude(rho, is_normal=True)\
        + np.exp(1j * w * surf_inter.range_to_tt(rho))
    # incident pressure
    p_inc = inc_field.amplitude(X, Y, 'source', is_gradient=True)\
            * np.exp(1j * w * inc_field.delay(X, Y, 'source'))
    iterate = np.add.reduce(G * p_inc[:, :, None], axis=0)
    iterate = np.add.reduce(iterate, axis=0)
    return iterate

r_rcr = [x_rcr, 0, z_rcr]
inc_field = SurfaceField(z_source, r_rcr, z_bottom)
inc_field.to_top(rg)
num_x = np.ceil((x_rcr + x_axis_buffer) / dx)
x_range = np.arange(num_x) * dx - (x_axis_buffer / 2)

# solve for incident pressure field normal derivative
amp, tau = hk_1d(inc_field)
iterate = fi(inc_field)
dt_dx = np.diff(tau) / dx

fig, ax = plt.subplots()
ax.plot(x_range[1: ], dt_dx, label='HK result')
ax.set_title('First derivative of travel time wrt x')
ax.set_ylim(-1e-4, 1e-4)
ax.grid()

plt.show(block=False)
