"""
simple iso speed case to get reference ray scaling
"""
import numpy as np
import matplotlib.pyplot as plt
from icepyks.clumber.surface_interaction import c_lin_flat
from icepyks.clumber.surface_field import SurfaceField
from icepyks.clumber.c_linear_rg import CLinear
from atpy import bellhop
import os

m = -0.3
m = -0.01
c0 = 1500
z_source = -25.
z_rcr = -200.
rho_range = np.r_[50: 5000: 300j]
fc = 150
z_bottom = -5e3
x_axis_buffer = 200

# computed values
w = 2 * np.pi * fc
k_src = w / (1500 + m * z_source)
dx = c0 / fc / 5

# setup incident field
rg = CLinear(m, c0)

# setup surface interaction field
surf_er = c_lin_flat(m, fc, c0=c0)  # negative slope is upward refracting

# HK solution
def hk_1d(inc_field):
    """compute scatter amplitude for a given range"""
    # need to impliment a custom hk for single frequency propagation
    y_range = np.zeros_like(x_range)
    src_amp = inc_field.amplitude(x_range, y_range, 'source', is_gradient=True)
    rcr_amp = inc_field.amplitude(x_range, y_range, 'receiver')
    weights = src_amp * rcr_amp / 2 / 1j
    weights *= np.sqrt(2 * np.pi)
    weights *= 1j * w / np.sqrt(c0)  # derivative term correction
    # delays relate to the phase term
    delays = inc_field.delay(x_range, y_range, 'source')\
             + inc_field.delay(x_range, y_range, 'receiver')
    # frequency domain solution, put in derivative term directly
    derv_term = np.exp(-1j * np.pi / 4) / np.sqrt(w)
    i_grand = weights * np.exp(1j * w * delays) * derv_term
    # Basic quadrature
    surf_result = derv_term * np.nansum(i_grand) * dx
    rad_amp, rad_tau = inc_field.rad_ray
    # radiated ray is also corrected for derivative term
    rad_ray = rad_amp * np.exp(1j * w * rad_tau) * derv_term
    rad_ray *= np.sqrt(2 * np.pi)
    return surf_result, rad_ray

#rays_1d = []
#for r_r in rho_range:
    #r_rcr = [r_r, 0, z_rcr]
    #surf_in = SurfaceField(z_source, r_rcr, z_bottom, num_dims=2)
    #surf_in.to_top(rg)
    #num_x = np.ceil((r_r + x_axis_buffer) / dx)
    #x_range = np.arange(num_x) * dx - (x_axis_buffer / 2)
    #rays_1d.append(hk_1d(surf_in))

# add direct path and bounce rays
#p_1d = [complex(tr[1] - tr[0]) for tr in rays_1d]
#tl_1d = 20 * np.log10(np.abs(np.array(p_1d)))

# reference solution
def load_plt(file_name):
    """load the oases plot format into numpy"""
    specta = []
    tl = []
    with open(file_name, 'r') as f:
        is_value = True
        for line in f:
            if line == '\n':
                is_value = False
                continue
            if is_value: specta.append(float(line))
            else: tl.append(float(line))
    specta = np.array(specta)
    tl = np.array(tl)
    return specta, tl

oases_line = '/home/e2richards/python-workspace/icepyks/clumber/envs/iso_line_hs.plt'
oases_point = '/home/e2richards/python-workspace/icepyks/clumber/envs/iso_point_hs.plt'
dx = 0.199976E-02  # iso

# iso llyods mirror
k = w / c0
r1 = np.sqrt(rho_range ** 2 + (z_rcr - z_source) ** 2)
r2 = np.sqrt(rho_range ** 2 + (z_rcr + z_source) ** 2)

p_p = lambda r_r: np.exp(1j * k * r_r) / r_r

cyln_ff = lambda r_r: np.exp(1j * np.pi / 4)\
                      * np.sqrt(2 * r_r / k / np.pi)\
                      * p_p(r_r)

# simple iso-speed addition
o_data, o_i = load_plt(oases_point)
p_1d = p_p(r1) - p_p(r2)
tl_1d = 20 * np.log10(np.abs(np.array(p_1d)))

# plot TL curves
fig, ax = plt.subplots()
ax.plot(np.arange(o_i.size) * dx, o_i, label='oases')
ax.plot(rho_range / 1e3, -tl_1d, 'r', label='HK result')
ax.set_ylim(90, 40)
ax.set_xlim(0, 5)
ax.set_ylabel('TL')
ax.set_xlabel('range, km')
ax.legend()
ax.grid()


plt.show(block=False)

o_data, o_i = load_plt(oases_line)
# simple iso-speed addition
p_1d = cyln_ff(r1) - cyln_ff(r2)
tl_1d = 20 * np.log10(np.abs(np.array(p_1d)))

# plot TL curves
fig, ax = plt.subplots()
ax.plot(np.arange(o_i.size) * dx, o_i, label='oases')
ax.plot(rho_range / 1e3, -tl_1d, 'r', label='HK result')
ax.set_ylim(60, 10)
ax.set_xlim(0, 5)
ax.set_ylabel('TL')
ax.set_xlabel('range, km')
ax.legend()
ax.grid()

fig, ax = plt.subplots()
ax.plot(rho_range, np.abs(cyln_ff(r2)), 'b', label='bounce')
ax.plot(rho_range, np.abs(cyln_ff(r1)), 'r', label='direct')
ax.legend()

plt.show(block=False)
