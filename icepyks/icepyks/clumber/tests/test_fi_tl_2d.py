"""
compaire first iterate results to reference solution.
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import toeplitz, solve_toeplitz
import os
from icepyks.clumber.surface_interaction import c_lin_flat
from icepyks.clumber.surface_field import SurfaceField
from icepyks.clumber.c_linear_rg import CLinear
from atpy import bellhop

m = -0.3
c0 = 1500
z_source = -25.
z_rcr = -200.
rho_range = np.r_[50: 10000: 300j]
rho_range = rho_range[:: -1]
fc = 150
z_bottom = -5e3
x_axis_buffer = 200

# computed values
w = 2 * np.pi * fc
k_src = w / (c0 + m * z_source)
dx = c0 / fc / 5

# setup incident field
rg = CLinear(m, c0)

# setup surface interaction field
surf_er = c_lin_flat(m, fc, c0=c0, num_dims=2)  # upward refracting
point_to_line = np.sqrt(2 / np.pi / k_src) * np.exp(-1j * np.pi / 4)

# common method used in integrating for pressure at rcr
def rcr_greens(inc_field):
    """Greens function from surface to receiver"""
    y_range = np.zeros_like(x_range)
    g_rcr = inc_field.amplitude(x_range, y_range, 'receiver')\
            * np.exp(1j * w * inc_field.delay(x_range, y_range, 'receiver'))
    g_rcr *= np.exp(-1j * np.pi / 4)
    return g_rcr

# Helmholtz integral
def surf_integral(surf_dpdn, inc_field):
    """compute scatter amplitude for a given range"""
    g_rcr = rcr_greens(inc_field)
    surf_result = np.nansum(surf_dpdn * g_rcr) * dx / 4 / 1j
    # Direct ray calculation
    rad_amp, rad_tau = inc_field.rad_ray
    # radiated ray is also corrected for derivative term
    rad_ray = rad_amp * np.exp(1j * w * rad_tau)
    # green's function correction
    rad_ray *= point_to_line
    return surf_result, rad_ray

def HK(inc_field):
    """compute the HK result"""
    y_range = np.zeros_like(x_range)
    # source ray projected onto surface
    src_amp = inc_field.amplitude(x_range, y_range, 'source', is_gradient=True)
    src_tau = inc_field.delay(x_range, y_range, 'source')
    HK_est = 2 * src_amp * np.exp(1j * w * src_tau)
    # 3d to 2d source scaling and phase conversion
    HK_est *= point_to_line
    HK_est *= 1j * w / c0  # derivative term correction
    return HK_est

def Helmholtz(HK_est):
    """Solve for exact pressure on surface using Helmholtz equation"""
    g = g_self()
    g[0] = 2j / dx
    exact = solve_toeplitz((g, g), HK_est / 2)
    exact /= (dx / 4 / 1j)
    return exact

def iterate(dp_dn_est):
    """Solve for pressure on surface using the iterate solution"""
    g = g_self()
    G = toeplitz(g, g)
    i_er = np.dot(G, dp_dn_est) * dx / 1j / 2
    return dp_dn_est - i_er

def g_self():
    """Create self interaction matrix, it is toepliz for flat surface"""
    x_diff = np.abs(x_range - x_range[0])
    g = surf_er.ray_amplitude(x_diff, is_normal=True)\
        * np.exp(1j * w * surf_er.range_to_tt(x_diff))
    g *= np.exp(-1j * np.pi / 4)
    g *= 1j * w / c0  # derivative term correction
    return g

def xmitt_totl(i_result):
    """simple conversion from the output of for loop to tl"""
    p = np.array([complex(tr[1] - tr[0]) for tr in i_result])
    tl = 20 * np.log10(np.abs(p))
    return tl


hk_res = []
helm_res = []
fi_res = []
for r_r in rho_range:
    r_rcr = [r_r, 0, z_rcr]
    surf_in = SurfaceField(z_source, r_rcr, z_bottom, max_iter=75,
                           range_buffer=x_axis_buffer, num_dims=2)
    surf_in.to_top(rg)
    num_x = np.ceil((r_r + x_axis_buffer) / dx)
    x_range = np.arange(num_x) * dx - (x_axis_buffer / 2)
    # solve for incident fields
    dpdn_hk = HK(surf_in)
    dpdn_fi = iterate(dpdn_hk)
    dpdn_helm = Helmholtz(dpdn_hk)
    # integrate incident fields
    hk_res.append(surf_integral(dpdn_hk, surf_in))
    helm_res.append(surf_integral(dpdn_helm, surf_in))
    fi_res.append(surf_integral(dpdn_fi, surf_in))

# add direct path and bounce rays
tl_hk = xmitt_totl(hk_res)
tl_helm = xmitt_totl(helm_res)
tl_fi = xmitt_totl(fi_res)

def load_plt(file_name):
    """load the oases plot format into numpy"""
    specta = []
    tl = []
    with open(file_name, 'r') as f:
        is_value = True
        for line in f:
            if line == '\n':
                is_value = False
                continue
            if is_value: specta.append(float(line))
            else: tl.append(float(line))
    specta = np.array(specta)
    tl = np.array(tl)
    return specta, tl

#oases_point = '/home/e2richards/python-workspace/icepyks/clumber/envs/c_lin_point.plt'
oases_line = '/home/e2richards/python-workspace/icepyks/clumber/envs/c_lin_line_extend.plt'
#dx = 0.199976E-02  # iso
dx = 0.366211E-02

o_data, o_i = load_plt(oases_line)

# plot TL curves
fig, ax = plt.subplots()
ax.plot(np.arange(o_i.size) * dx, o_i, linewidth=2, alpha=0.6, label='oases')
ax.plot(rho_range / 1e3, -tl_fi, 'g', label='FI')
#ax.plot(rho_range / 1e3, -tl_helm, 'k', label='Helmholtz')
ax.set_ylim(60, 5)
ax.set_xlim(0, 5)
ax.set_ylabel('TL')
ax.set_xlabel('range, km')
ax.set_title('Comparison of 1d HK to reference solutions \n' +
             'Line source')
ax.legend(loc=8)
ax.grid()

plt.show(block=False)

import sys; sys.exit("User break") # SCRIPT EXIT

# plot the surface self interaction matrix
# Assume far field approximation
x_rcr = 4000.
r_rcr = [x_rcr, 0, z_rcr]
surf_in = SurfaceField(z_source, r_rcr, z_bottom,
                       range_buffer=x_axis_buffer, num_dims=2)
surf_in.to_top(rg)
num_x = np.ceil((x_rcr + x_axis_buffer) / dx)
x_range = np.arange(num_x) * dx - (x_axis_buffer / 2)
distance = np.abs(x_range)
g_launch = surf_er.range_to_angle(distance)

fig, ax = plt.subplots()
ax.plot(x_range, np.degrees(g_launch))
ax.set_title('Launch angle of self interacting ray')
ax.set_ylabel('degrees')
ax.set_xlabel('arrival distance, m')

g_amp = surf_er.ray_amplitude(distance, is_normal=True)
fig, ax = plt.subplots()
ax.plot(x_range, g_amp)
ax.set_title('amplitude self interacting ray normal derivative')
ax.set_ylabel(r'$|\partial p / \partial n|$')
ax.set_xlabel('arrival distance, m')

plt.show(block=False)

dpdn_fi, dpdn_hk  = fi_1d(surf_in)
g_rcr = rcr_greens(surf_in)
fig, ax = plt.subplots()
ax.plot(x_range, np.unwrap(np.angle(dpdn_fi * g_rcr)) * 1e3 / w)
ax.plot(x_range, np.unwrap(np.angle(dpdn_hk * g_rcr)) * 1e3 / w)
ax.set_title('Travel time curve of first iterate')
ax.set_ylabel(r'$\tau$, ms')
ax.set_xlabel('distance from source, m')

plt.show(block=False)
