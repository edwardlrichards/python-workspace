import matplotlib.pyplot as plt
from icepyks.clumber import c_linear_rg, surface_field, surface_interaction
import numpy as np
from scipy.linalg import solve

c0 = 1500
z_source = 25
z_receiver = 200
rho_range = np.r_[50: 5000: 20j]
z_bottom = 5000
fc = 150  # Hz
omega = 2 * np.pi * fc
m = 0.3

ray_gun = c_linear_rg.CLinear(m, c0)

# test travel time derivative
x_rcr = 100.
r_receiver = [x_rcr, 0, z_receiver]
surf_self = surface_interaction.c_lin_flat(m, c0, fc)
dx = c0 / fc / 12
rho_range = [50]

def helmer(proper):
    """compute scatter amplitude for a given range"""
    p_inc = proper.amplitude(x_axis, y_axis, 'source')\
            * np.exp(1j * omega * proper.delay(x_axis, y_axis, 'source'))
    if not np.all(np.isfinite(p_inc)):
        raise(ValueError('Pressure is not finite at all points'))
    # Adjust for integral scaling
    p_inc *= 4 * np.pi
    G = surf_self(x_axis)
    dpdn = solve(G, p_inc)
    # compare with HK value

    hk = proper.amplitude(x_axis, y_axis, 'source', is_gradient=True) + 0j
    hk *= (-1j * omega / (2 * np.pi * c0))
    fpp = surf_self.d2tau_dy2(np.abs(proper.range_rcr - x_axis))
    hk *= np.sqrt(2 * np.pi / omega / fpp) * np.exp(1j * np.pi / 4)
    return dpdn, hk
    # integrate from surface to receiver
    weights = proper.amplitude(x_axis, y_axis, 'receiver') + 0j
    weights *= (-1j * omega / (2 * np.pi * c0))
    # adjust amplitude for stationary phase approximation
    weights = weights / np.sqrt(proper.tau_d2dy2(x_axis))
    weights *= np.sqrt(2 * np.pi / omega) * np.exp(1j * np.pi / 4)
    # create complex kernel
    kernel = weights\
             * np.exp(1j * omega * proper.delay(x_axis, y_axis, 'receiver'))
    surf_result = dx * np.nansum(dpdn * kernel)
    rad_amp, rad_tau = proper.rad_ray
    return surf_result, rad_amp * np.exp(1j * omega * rad_tau)

def hk_1d(proper):
    """compute scatter amplitude for a given range"""
    # need to impliment a custom hk for single frequency propagation
    weights = proper.amplitude(x_axis, y_axis, 'source', is_gradient=True)\
              * proper.amplitude(x_axis, y_axis, 'receiver')
    weights /= (2 * np.pi * c0)
    weights = np.array(weights, dtype=np.complex_) * -1j * omega
    # adjust amplitude for stationary phase approximation
    weights = weights / np.sqrt(proper.tau_d2dy2(x_axis))
    weights *= np.sqrt(2 * np.pi / omega) * np.exp(1j * np.pi / 4) 
    delays = proper.delay(x_axis, y_axis, 'source')\
             + proper.delay(x_axis, y_axis, 'receiver')
    surf_result = dx * np.nansum(weights * np.exp(1j * omega * delays))
    rad_amp, rad_tau = proper.rad_ray
    return surf_result, rad_amp * np.exp(1j * omega * rad_tau)

# Run HK models
helm = []
rays_1d = []
for r_r in rho_range:
    r_receiver = [r_r, 0, z_receiver]
    proper = surface_field.SurfaceField(z_source, r_receiver, z_bottom,
                                        max_iter=60, range_buffer=200)
    proper.to_top(ray_gun)
    num_x = np.ceil(proper.range_rcr + 380 / dx)
    x_axis = np.arange(num_x) * dx - 160
    x_axis += 0.1  # make sure x=0 is not a point
    y_axis = proper.sp_path(x_axis)

    #G, p_inc = helmer(proper)
    helm.append(helmer(proper))
    rays_1d.append(hk_1d(proper))

#fig, ax = plt.subplots()
#ax.plot(x_axis, np.abs(G[:, 100]))
#ax.plot(x_axis, np.abs(G[:, 200]))
#ax.plot(x_axis, np.abs(G[:, 300]))
#ax.plot(x_axis, np.abs(G[:, 400]))
#ax.legend()
#ax.grid()
#plt.show(block=False)

# plot stationary phase product
fig, ax = plt.subplots()
ax.plot(x_axis, surf_self.dtau_drho(np.abs(x_axis)))
#ax.plot(x_axis, np.full_like(x_axis, 1 / c0 / z_source))

plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
fig, ax = plt.subplots()
ax.plot(x_axis, np.abs(helm[0][0]), label='Helm')
ax.plot(x_axis, np.abs(helm[0][1]), label='HK')
ax.set_title('Normal derivative amplitude')
ax.legend()
ax.grid()

# plot phase
rcr_phase = np.exp(1j * omega * proper.delay(x_axis, y_axis, 'receiver'))
t1 = np.unwrap(np.angle(helm[0][0] * rcr_phase))
t2 = np.unwrap(np.angle(helm[0][1] * rcr_phase))

fig, ax = plt.subplots()
ax.plot(x_axis, t1, label='Helm')
ax.plot(x_axis, t2, label='HK')
ax.set_title('travel time curves')
ax.legend()
ax.grid()

plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
# calculate total pressure
p_0d = [tr[0] + tr[1] for tr in helm]
p_1d = [tr[0] + tr[1] for tr in rays_1d]

tl_0d = 20 * np.log10(np.abs(np.array(p_0d)))
tl_1d = 20 * np.log10(np.abs(np.array(p_1d)))

# Plot tl curves
fig, ax = plt.subplots()
ax.plot(rho_range, tl_0d, '.r', linewidth=2, label='eigen ray')
ax.plot(rho_range, tl_1d, label='HK result')
ax.set_xlabel('range, m')
ax.set_ylabel('TL, dB')
ax.set_title('Iso-speed lloyd''s mirror pattern')
ax.grid()
ax.legend(loc=3)

plt.show(block=False)
