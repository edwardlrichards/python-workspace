"""
======================================
Ray gun assuming iso-speed propagation
======================================
Analyitic ray solution
"""
import numpy as np
from icepyks.clumber.ray_gun import RayGun

class Isospeed(RayGun):
    """Simple ray trace with one c linear gradient"""
    def __init__(self, c_0, z_bottom, z_const):
        """set up ray parameters"""
        # Simplest sound speed of all
        self.c_source = c_0
        self.ssp = lambda z: c_0 + z * 0
        self.z_source = None  # still need to initialize depth
        RayGun.__init__(self, z_bottom, z_const)

    def __call__(self, z_source):
        """Complete the initilization of the raygun for depth of interest"""
        self.z_source = z_source
        RayGun.__call__(self, z_source)

    def to_boundry(self, la_vector, r_max):
        """Just call the ray gun method"""
        if self.z_source is None: raise(ValueError('no source depth'))
        return RayGun.to_boundry(self, la_vector, r_max)

    def _one_to_boundry(self, param, z_bounds, r_bound):
        """Propagate ray untill it hits a boundry"""
        # all slowness is fixed in iso-speed
        x0, x2, s0, s2, a0, tt, la = tuple(param)
        z_bounds = np.array(z_bounds, ndmin=1)
        r_bound = np.array(r_bound, ndmin=1)
        c_t = s0 * self.c_source
        s_t = s2 * self.c_source
        # Determine distance to interface: top, bottom or side
        s_r = r_bound / c_t
        if np.abs(s_t) > 1e-7:
            s_z = (z_bounds - x2) / s_t
        else:
            # don't consider this value
            s_z = -1
        s = np.hstack((s_r, s_z))
        if np.all(np.isnan(s)):
            raise(ValueError('No boundries specified'))
        si = np.bitwise_not(np.isnan(s))
        smin = s[si]
        smin = np.min(smin[smin >= 0])
        # Propagate ray to nearest boundry
        return self._prop(smin, param)

    def _prop(self, s, param):
        """populate initial ray with a given distance vector"""
        x0, x2, s0, s2, a0, tt, la = tuple(param)
        c_t = s0 * self.c_source
        s_t = s2 * self.c_source
        dr = s * c_t
        dz = s * s_t
        d_tt = s / self.c_source
        param_end = np.array([x0 + dr, x2 + dz, s0, s2, a0 + s, tt + d_tt, la])
        if np.any(np.isnan(param_end)):
            raise(ValueError('A nan snuck into the ray result'))
        return param_end

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    z = np.linspace(0, 100, 100)
    m = 0.1
    c0 = 1500
    r_max = 300
    z_source = 30
    z_bottom = 100
    ray_gun = Isospeed(c0)
    ray_gun(z_source)

    # shoot and plot a ray fan
    angles = np.deg2rad(np.r_[90: -90: 61j])
    bound_rays, rcr_rays = ray_gun.to_boundry(angles, z_bottom, r_max)
    fig, ax = plt.subplots()
    [ax.plot([0, r], [z_source, z], 'b') for r, z in
                     zip(bound_rays[:, 0], bound_rays[:, 1])]
    [ax.plot(r, z, 'g.') for r, z in
                     zip(rcr_rays[:, 0], rcr_rays[:, 1])]
    ax.set_ylim(105, -5)
    ax.set_xlim(0, r_max + 5)
    ax.set_title('arrival depth and range of iso_speed rays')
    plt.show(block=False)
