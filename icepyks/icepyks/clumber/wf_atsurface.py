"""Create a curve of arrival angle and delay at the surface"""
import numpy as np
from scipy.optimize import brentq
from icepyks.clumber.wavefront import Wavefront
from icepyks.lookup import lookup
from icepyks.clumber.interp1d import Interp1d

def wf_attop(ray_gun, r_max, tol, max_iter=50, r_rcr=None):
    """Basic setup
    if r_rcr is a position vector, ray at position vector is returned
    """
    z_tol = 1e-7  # a length scale
    shoot = lambda la: ray_gun.to_boundry(la, r_max)
    top_test = lambda ray: np.abs(ray[:, 1]) < z_tol
    bot_test = lambda ray: np.abs(ray[:, 1] - ray_gun.z_bottom) < z_tol
    wfs = _startup_wavefronts(shoot, top_test, bot_test, tol, max_iter, r_rcr)
    # check if direct ray is needed
    if r_rcr is not None:
        # Does this work with reciever higher than source?
        r_target = np.linalg.norm(r_rcr[: -1])
        z_target = r_rcr[-1]
        rs = lambda la: ray_gun.to_boundry(la, r_target)
        rooter = lambda la: float(rs(la)[:, 1] - z_target)
        dir_la = brentq(rooter, .99 * (np.pi / 2), .99 * (-np.pi / 2))
        #dir_la = brentq(rooter, wfs[1][0], wfs[1][1])
        result = (wfs[0], rs(dir_la))
    else:
        result = wfs
    return result

def _startup_wavefronts(shoot, top_test, bot_test, tol, max_iter, r_rcr):
    """propagating rays to constant range and depth creates wf from rg"""
    # sample the rays at the end surface well
    p0 = shoot(np.array([-np.pi / 2, np.pi / 2]))
    # TODO: impliment bottom boundry
    params = lookup(p0, shoot, tol, -1, max_iter=max_iter)
    rad_i = np.bitwise_not(np.bitwise_or(top_test(params),
                                            bot_test(params)))
    wf = _bounds_to_wavefront(params[top_test(params), :])
    # return a starting guess of lauch angle if radiating
    if r_rcr is not None:
        z_atbound = np.argsort(params[rad_i, 1])
        la_bound = (params[rad_i, -1][z_atbound[0]],
                    params[rad_i, -1][z_atbound[-1]])
    result = (wf, la_bound) if r_rcr is not None else wf
    return result

def _bounds_to_wavefront(bounded_rays):
    """Create wavefronts from densly sampled rays.
    unmerged rays will be properly sorted and merged into wavefront
    """
    # rays need to be sorted and unique in ray index
    _, ui = np.unique(bounded_rays[:, -1], return_index=True)
    bounded_rays = bounded_rays[ui, :]
    bounded_rays = bounded_rays[np.argsort(bounded_rays[:, -1]), :]
    # infer surface name from surface test
    # TODO: impliment surface other than top
    name = 'top'
    interp_param = bounded_rays[:, [0, 2, 3, 4, 5]]
    param_names = ['x0', 's0', 's2', 'a0', 'tt']
    # Create new wave fronts
    inverse_index = 0  # all inverse lookups are wrt x coord
    index_name = 'la'  # all wavefronts begin with slowness index
    index_variable = bounded_rays[:, -1]
    i_er = Interp1d(index_variable, interp_param, index_name,
                    param_names, inverse_name='x0')
    # create the travel time curve
    i_er.add_travel_time(bounded_rays[:, 0], bounded_rays[:, 5])
    wf = Wavefront(i_er, [name])
    return wf

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from icepyks.clumber import c_linear_rg
    from icepyks.clumber import iso_rg

    m = -0.1
    c0 = 1500
    z_source = -20
    r_receiver = [200, 0, -10]
    r_max = 210
    z_bottom = -50
    tol = (1e-2 / c0, 5)
    a_bounds = (-20, 220)
    src_rg = iso_rg.Isospeed(c0)
    src_rg(z_source)
    rrc_rg = iso_rg.Isospeed(c0)
    rrc_rg(r_receiver[-1])

    # Transmit from source to surfaces
    rcr_wf = wf_attop(rrc_rg, z_bottom, r_max, tol)
    scr_wf = wf_attop(src_rg, z_bottom, r_max, tol)

    r_range = np.r_[0.2: 200: 200j]

    # plot r vs travel_time
    par = ['tt', 'x_vec']
    s_x = rcr_wf(r_range, 'x_vec', is_inverse=True)
    s_tt = rcr_wf(r_range, 'tt', is_inverse=True)
    fig, ax = plt.subplots()
    ax.plot(s_x[:, 0], s_tt, 'b')
    ax.set_title('Travel time to surface vs distance')
    ax.set_xlabel('range, m')
    ax.set_ylabel('travel time, s')

    # plot s0 vs tt
    s_s = rcr_wf(r_range, 's_vec', is_inverse=True)
    s_tt = rcr_wf(r_range, 'tt', is_inverse=True)
    fig, ax = plt.subplots()
    ax.plot(s_s[:, 0], s_tt, 'b')
    ax.set_title('p vs travel time')
    ax.set_xlabel('p, s/m')
    ax.set_ylabel('travel time, s')

    plt.show(block=False)
