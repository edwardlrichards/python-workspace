import numpy as np
from itertools import groupby
#from scipy.interpolate import UnivariateSpline as interper
from scipy.interpolate import InterpolatedUnivariateSpline as interper
from scipy.interpolate import splrep, sproot

class Interp1d:
    """Interpolate ray parameters, return NaN when undefined"""
    def __init__(self, index_variable, parameters, index_name, param_names,
                 inverse_name=None):
        """Create interpolators of parameters as functions of index variable"""
        self.index_variable = index_variable
        self.index_name = index_name
        self.inverse_name = inverse_name
        # initilize interpolators
        s_sec = self._link_arrivals(index_variable, parameters)
        self._i_ers = self._create_interpolators(s_sec, param_names)

    def __call__(self, in_range, param_name, nu=0):
        """Return interpolated values for specified in_range"""
        in_range = np.array(in_range, ndmin=1, dtype=np.float_)
        # interpolate desired parameter
        y = self._i_ers[param_name](in_range, nu=nu)
        return y

    @property
    def bounds(self):
        """Regions over which interpolator is defined"""
        # most interpolators have same bounding regions
        r = sorted(self._i_ers['tt'].defined_regions,
                   key=lambda a: a[0])
        return r

    @property
    def param_names(self):
        """Names of defined interpolators"""
        return list(self._i_ers.keys())

    def knots(self, param_name):
        """Return the x values where an intepolator is defined"""
        return self._i_ers[param_name].knots

    def add_travel_time(self, rho, tau):
        """Travel time curve uses a non-standard index, but is important"""
        sort_i = np.argsort(rho)
        rvt = [interper(rho[sort_i], tau[sort_i], k=1, check_finite=True)]
        self._i_ers['tau'] = SectionedInterper(rvt)

    def value_bounds(self, value_name):
        """Return bounds for specified value"""
        if value_name == self.index_name:
            return (np.min(self.index_variable), np.max(self.index_variable))
        else:
            return self._i_ers[value_name].bounds

    def _link_arrivals(self, index_variable, parameters):
        """Index wave front by total horizonatal slowness, add empty regions
        """
        # sort each section
        if not isinstance(parameters, list):
            listed_params = [np.hstack((index_variable[:, None], parameters))]
        else:
            listed_params = [np.hstack((i_v[:, None], p)) for i_v, p
                             in zip(index_variable, parameters)]
        # sort each section by index variable
        s_sec = [s[np.argsort(s[:, 0]), :] for s in listed_params]
        # sort section list by lower index_variable bound
        s_sec.sort(key=lambda ds: float(ds[0, 0]))
        return s_sec

    def _create_interpolators(self, s_sec, param_names):
        """from a list of sections, create all interpolators"""
        indecies = [s[:, 0] for s in s_sec]
        all_interpers = {}
        for i, name in enumerate(param_names):
            values = [s[:, i+1] for s in s_sec]
            i_ers = [interper(x, y, k=1, check_finite=True) for x, y
                     in zip(indecies, values) if x.size > 3]
            all_interpers[name] = SectionedInterper(i_ers)
            #Calculate the inverse index
            if self.inverse_name is not None and self.inverse_name == name:
                # sort by value before interpolation
                sortI = [np.argsort(v) for v in values]
                i_ers = [interper(y[s], x[s], k=1,check_finite=True)
                         for x, y, s in zip(indecies, values, sortI)]
                all_interpers[self.index_name] = SectionedInterper(i_ers)
        return all_interpers


class SectionedInterper:
    """interpolator that is aware of it boundries, and returns NAN when out"""
    def __init__(self, interpers):
        """interpers is a list of interpolators"""
        # find the bounds of values
        extrema = lambda a: (np.min(a), np.max(a))
        bbox = [extrema(i_er.get_knots()) for i_er in interpers]
        # Sort both interpolators and bounding boxes by lower bound of bb
        self._ib = sorted(zip(interpers, bbox), key=lambda ib: ib[1][0])
        bounds = [extrema(i_er.get_coeffs()) for i_er in interpers]
        self.bounds = extrema(np.array(bounds))  # smallest and largest a value

    def __call__(self, x, nu=0):
        """Return interpolated values when possible, NAN when not"""
        x = np.array(x, dtype=float)
        y = np.full_like(x, np.nan)
        for i_er, bb in self._ib:
            active_i = np.bitwise_and(x >= bb[0], x <= bb[1])
            y[active_i] = i_er(x[active_i], nu=nu)
        return y

    @property
    def defined_regions(self):
        """List of (lb, ub) where the interpolator is defined"""
        bboxes = [bb for _, bb in self._ib]
        return bboxes

    @property
    def knots(self):
        """List of (lb, ub) where the interpolator is defined"""
        x_coords = np.hstack([i_er.get_knots() for i_er, _ in self._ib])
        return x_coords

def tau_stationary(i_src, i_rcr, rcr_rho_func):
    """Find points where the derivative of T(x) is zero"""
    sec_1 = i_src._i_ers['tau']
    r_axis = sec_1.knots
    rcr_axis = rcr_rho_func(r_axis)
    sec_2 = i_rcr._i_ers['tau']
    tau = np.squeeze(sec_1(r_axis, nu=1) - sec_2(rcr_axis, nu=1))
    tau_byi = lambda i: np.isnan(tau[i])
    ind = range(tau.size)
    an_sections = [list(g) for k, g in groupby(ind, tau_byi) if not k]
    def rooter(sections, xa, ta):
        """use b spline to find roots"""
        for sec in sections:
            if len(sec) < 8:
                continue
            tck = splrep(xa[sec], ta[sec], s=0)
            roots = sproot(tck)
            yield roots
    roots = np.hstack(list(rooter(an_sections, r_axis, tau)))
    return roots

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from icepyks.basenji import wf_atsurface
    from icepyks.basenji import catalog
    from icepyks.basenji import iso_rg as iso_rg
    from icepyks.basenji.propagator import Propagator
    from icepyks import sin_surface

    c0 = 1500
    m = 0.3
    r_source = [0, 0, 20]
    r_receiver = [200, 0, 10]
    a_bounds = (-20, 220)
    z_bottom = 30
    s_tol = (1e-2, 6)
    x2_tol = (1e-2, 2)
    amp = 1.5  # m
    wave_length = 40  # m

    src_rg = iso_rg.Isospeed(c0, r_source)
    rrc_rg = iso_rg.Isospeed(c0, r_receiver)

    # send the top wavefront to the surface
    eta = sin_surface.Eta(wave_length, amp)
    proper = Propagator(src_rg, rrc_rg, a_bounds, z_bottom)
    one_wave = proper.for_wave(eta)
    norm_oi = 0.
    one_wave(norm_oi)
    a_range = np.r_[a_bounds[0]: a_bounds[1] : 300j]
    
    # shadowed and unshadowed quantities
    sh_tt = one_wave.cat('shadow', a_range, 'tt', normal=norm_oi,
                          element='source')
    wa_tt = one_wave.cat('wave', a_range, 'tt', normal=norm_oi,
                          element='source')
    fig, ax = plt.subplots()
    ax.plot(a_range, wa_tt, 'b')
    ax.plot(a_range, sh_tt, 'r.')
    ax.set_title('Interpolated values, w & wo shadowing')
    plt.show(block=False)
