import numpy as np
from scipy.interpolate import UnivariateSpline

class ContinuousSignal:
    """Interpolator of a signal and the first derivative"""
    def __init__(self, signal, taxis):
        """Initialize interpolator with a signal, numpy array"""
        self.ts = signal
        self.taxis = taxis
        self.bbounds = (np.min(self.taxis), np.max(self.taxis))
        # calculate signal transform for stationary phase corrections
        dt = (taxis[-1] - taxis[0]) / (taxis.size - 1)
        fs = 1 / dt
        NFFT = int(2 ** np.ceil(np.log2(signal.size)))
        self.t_up = np.arange(NFFT) * dt
        # create new frequency axis
        f = np.arange(NFFT // 2 + 1) / NFFT * fs
        # Multiply transmitted signal by derivative factor
        sig_FT = np.fft.rfft(signal, NFFT)
        # setup upsampled signal
        self.f = f
        self.sig_FT = sig_FT
        sf, sFT = self.get_spectrum()
        # upsample signal to ifft length
        self.signal_ts = self.make_ier(sFT)
        self.signal_2d_ts = self.make_ier(np.conj(np.exp(-1j * np.pi / 4))
                                          * sFT / np.sqrt(2 * np.pi * sf))
        # make line source time series
        self.line_source_ts = self.make_ier(np.conj(1j) * sFT)
        self.hk_sta_ts = self.make_ier(np.conj(np.exp(3j * np.pi / 4))
                                       * np.sqrt(2 * np.pi * sf) * sFT)
    def __call__(self, t):
        """Return the signal at input time"""
        y = self.signal_ts(t)
        return y

    def get_spectrum(self):
        """Return the FT of the signal, which can be modified by user"""
        # don't return 0 frequency bin
        return self.f[1: ], self.sig_FT[1: ]

    def make_ier(self, user_FT):
        """Make a user modified Fourier transform into an interpolator"""
        sig_2d = np.fft.irfft(np.hstack([0. + 0j, user_FT]))
        # create interpolator
        spl = UnivariateSpline(self.t_up, sig_2d, ext=1, s=0., k=3)
        return spl

if __name__=='__main__':
    import matplotlib.pyplot as plt
    from icepyks import pulse_signal

    x_mit, taxis = pulse_signal.pulse()
    fs = 5e6
    t = np.arange(0, 5e-4, 1 / fs)
    plt.figure()
    x_inter = ContinuousSignal(x_mit, taxis)

    x_sig = x_inter(t)
    f, s_ft = x_inter.get_spectrum()
    sig_der = 1j * 2 * np.pi * f * s_ft
    der_ier = x_inter.make_ier(sig_der)
    x_der = der_ier(t) / (2 * np.pi * 200e3)
    plt.plot(t * 1e6, x_sig, label='s(t)')
    plt.plot(taxis * 1e6, x_mit, '--')
    plt.plot(t * 1e6, x_der, label='s\'(t) / omega')
    plt.grid()
    plt.legend()
    plt.xlabel('micro seconds')
    plt.title('Interpolated pulse signal and first derivative')
    plt.xlim(0, 20)
    plt.show(block=False)
