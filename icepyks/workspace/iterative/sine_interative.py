import numpy as np
import matplotlib.pyplot as plt

wave_l = 40
wave_h = 2
theta_inc = 30

wave_k = 2 * np.pi / wave_l
num_points = 300
xaxis = (np.arange(num_points) / num_points - 0.5) * wave_l
dx = (xaxis[-1] - xaxis[0]) / (num_points - 1)

eta = wave_h / 2 * np.cos(wave_k * xaxis)
eta_p = -wave_k * wave_h / 2 * np.sin(wave_k * xaxis)
gamma = np.sqrt(1 + eta_p ** 2)

kx = np.cos(np.radians(theta_inc))
kz = np.sin(np.radians(theta_inc))

pinc = np.exp(1j * (kx * xaxis - kz * eta))

ddn_pinc = (kz - eta_p * kx) / gamma * pinc
