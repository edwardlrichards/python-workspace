import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import RegularGridInterpolator
from scipy.special import jv
from icepyks.poodle import cosine_surface, wn_synthesis
from scipy.linalg import solve
import numexpr as ne
from concurrent.futures import ThreadPoolExecutor

# source and reciever geometry
r_src = np.array([0., 0, -20])
r_rcr = np.array([200., 0, -10])
r_img = np.sqrt((r_src[0] - r_rcr[0]) ** 2 + (r_src[-1] + r_rcr[-1]) ** 2)

# cosine surface specificatons
hwave = 2.
lwave = 40.
kwave = 2 * np.pi / lwave
max_freq = 2500  # Hz
min_freq = 500   # Hz
attn = 0.1
delta = lambda k: 1j * attn / 8686. / np.real(k + np.spacing(1))
kacous = lambda f: (1 + delta(2 * np.pi * f / 1500)) * 2 * np.pi * f / 1500
cos_ier = cosine_surface.CosineSurface(hwave, kwave, attn=attn)

# theta, frequency and order number axis specifications
num_theta_r = 300
eva_range = 0.1
num_eva = 3
num_frequencies = 1400

# wn integration axes
num_wni_theta = 3000
num_wni_phi = 5000
wni_eva = 0.1

# compaire result with stationary phase
wns = wn_synthesis.WaveNumberSynthesis(r_src, r_rcr, lwave)

wni_phi_axis = np.arange(num_wni_phi) / num_wni_phi * np.pi / 2
# create that wni axis with some evanescent tail
wni_theta_axis = np.linspace(0, np.pi / 2, num_wni_theta)
dth = wni_theta_axis[1] - wni_theta_axis[0]
eva = np.arange(dth, wni_eva, dth) * 1j

wni_theta_axis = np.hstack([eva[::-1], wni_theta_axis])

# wni differentials
wni_dth = np.diff(wni_theta_axis)
wni_dth = np.hstack([wni_dth, wni_dth[-1]])
wn_dphi = np.abs(wni_phi_axis[1] - wni_phi_axis[0])

# make a common angle axis for all frequencies
theta_axis = np.linspace(0, np.pi / 2, num_theta_r)
dth = theta_axis[1] - theta_axis[0]
eva = np.arange(dth, eva_range, dth) * 1j
theta_axis = np.hstack([eva[::-1], theta_axis])

# setup a common qvector for all frequencies
a = np.real(np.cos(theta_axis[0]))
qvec_1 = np.arange(np.ceil(-max_freq * lwave / 1500 * (1 + a)) - num_eva,
                    np.floor(max_freq * lwave / 1500 * (1 - a)) + num_eva)
a = np.real(np.cos(theta_axis[-1]))
qvec_2 = np.arange(np.ceil(-max_freq * lwave / 1500 * (1 + a)) - num_eva,
                    np.floor(max_freq * lwave / 1500 * (1 - a)) + num_eva)
all_qs = np.unique(np.hstack([qvec_1, qvec_2]))

# frequency axis
freq_axis = np.arange(num_frequencies) / (num_frequencies - 1)
freq_axis *= (max_freq - min_freq)
freq_axis += min_freq

calc_rs = np.zeros((num_frequencies, theta_axis.size, all_qs.size),
                       dtype=np.complex_)

# common angle and position quanitities
a_max = np.sqrt(1 - (np.real(np.cos(wni_theta_axis))[None, :]
                     * np.sin(wni_phi_axis)[:, None]) ** 2 + 0j)
a_i = np.bitwise_not(np.isnan(a_max))

# setup incident angles
alp_inc = np.cos(wni_phi_axis)[:, None] * np.cos(wni_theta_axis)[None, :]
beta_inc = np.sin(wni_phi_axis)[:, None] * np.cos(wni_theta_axis)[None, :]
gam_inc = np.sin(wni_theta_axis)

# common factors
diff_scale = np.abs(np.cos(wni_theta_axis)) 
rrx = r_rcr[0]
rsx = r_src[0]
rrz = r_rcr[-1]
rsz = r_src[-1]

def bragg_angles(fin, qval):
    """Computes the qth scattering angles"""
    ka = kacous(fin)
    ai = np.real(ka) * alp_inc
    gi = np.sqrt(ka ** 2 - ai ** 2
                     - (np.real(ka) * beta_inc) ** 2 + 0j)
    a_pos = ai + qval * kwave
    a_neg = ai - qval * kwave
    g_pos = np.sqrt(ka ** 2 - a_pos ** 2 - (np.real(ka) * beta_inc) ** 2 + 0j)
    g_neg = np.sqrt(ka ** 2 - a_neg ** 2 - (np.real(ka) * beta_inc) ** 2 + 0j)
    return ai, gi, a_pos, a_neg, g_pos, g_neg

def HK_rs(fin, qval, ai, gi, alphas, gammas):
    """Compute rayleigh reflection coefficents"""
    # compute a q vector
    ka = kacous(fin)
    # HK prefactor
    pre_fac = (ai * (alphas - ai) - gi * (gi + gammas))\
              / (gammas * (gi + gammas))
    arg = hwave / 2 * (gi + gammas)
    return 1j ** qval * pre_fac * jv(qval, arg)

def point_atq(fin, qval, ai, gi, a_pos, a_neg, g_pos, g_neg, r_pos, r_neg):
    """Calculate the scatter solution frequency by frequency, qth Bragg order
    """
    ka = kacous(fin)
    iterm = ne.evaluate('r_pos * exp(1j * (a_pos * rrx'+
                        '- ai * rsx - g_pos * rrz - gi * rsz))'+
                        '* diff_scale * wni_dth')

    wn = 1j * ka / (4 * np.pi ** 2) * np.sum(iterm) * wn_dphi
    # add negative alp_inc
    iterm = ne.evaluate('r_neg * exp(1j * (-a_neg * rrx'+
                        '+ ai * rsx - g_neg * rrz - gi * rsz))'+
                        '* diff_scale * wni_dth')

    wn += 1j * ka / (4 * np.pi ** 2) * np.sum(iterm) * wn_dphi

    iterm = ne.evaluate('r_neg * exp(1j * (a_neg * rrx'+
                        '- ai * rsx - g_neg * rrz - gi * rsz))'+
                        '* diff_scale * wni_dth')

    wn_neg = 1j * ka / (4 * np.pi ** 2) * np.sum(iterm) * wn_dphi
    # add negative alp_inc
    iterm = ne.evaluate('r_pos * exp(1j * (-a_pos * rrx'+
                        '+ ai * rsx - g_pos * rrz - gi * rsz))'+
                        '* diff_scale * wni_dth')

    wn_neg += 1j * ka / (4 * np.pi ** 2) * np.sum(iterm) * wn_dphi

    return wn, wn_neg

def calc_inputs(fin, qval):
    """setup the inputs to the one_freq multiply"""
    ai, gi, a_pos, a_neg, g_pos, g_neg = bragg_angles(fin, qval)
    r_pos = HK_rs(fin, qval, ai, gi, a_pos, g_pos)
    r_neg = HK_rs(fin, -qval, ai, gi, a_neg, g_neg)
    return ai, gi, a_pos, a_neg, g_pos, g_neg, r_pos, r_neg

def save_coeffs(fcalc):
    """Compute coefficents and save the result"""
    qvec = [0, 1, 2, 3, 4, 5, 6, 7]
    coeff_calc = lambda q: calc_inputs(fcalc, q)
    with ThreadPoolExecutor(max_workers=8) as executor:
        coeffs = list(executor.map(coeff_calc, qvec))
    wn_quad = []
    for i, ci in enumerate(coeffs):
        p_pos, p_neg = point_atq(fcalc, qvec[i], *ci)
        wn_quad.append(p_pos)
        if qvec[i] != 0:
            wn_quad.append(p_neg)

    np.savez('quad_coeffs/coeffs_%i'%fcalc, qvec=qvec, qcoefs=wn_quad)

[save_coeffs(f) for f in freq_axis]
#rhk = lambda a0, freq: cos_ier.r_hk(a0, all_qs, freq, return_bs=False)
#rtest = rhk(np.cos(theta_axis), ftest)
#wn_sta = wns.kx_sta_ky_sta(qtest, all_qs, rtest, theta_axis, ftest) / 4 / np.pi

#print()
#print('quad result:          %.6f       + %.6f j'%
      #(np.real(wn_quad[0]) * 1e3, np.imag(wn_quad[0]) * 1e3))
#print('stationary result:    %.6f       + %.6f j'%
      #(np.real(wn_sta) * 1e3, np.imag(wn_sta) * 1e3))
#print()
