import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import RegularGridInterpolator
from scipy.special import jv
from icepyks.poodle import cosine_surface, wn_synthesis
from scipy.linalg import solve
import numexpr as ne
from icepyks import pulse_signal, signal_interp
from icepyks.f_synthesis import synthesize_ts

# source and reciever geometry
r_src = np.array([0., 0, -20])
r_rcr = np.array([200., 0, -10])
r_img = np.sqrt((r_src[0] - r_rcr[0]) ** 2 + (r_src[-1] + r_rcr[-1]) ** 2)
tau_img = r_img / 1500

# cosine surface specificatons
hwave = 2.
lwave = 40.
kwave = 2 * np.pi / lwave

nfreq = 300
fc = 2500
fmin = 500
fmax = 4500

# theta, frequency and order number axis specifications
num_theta_r = 300
eva_range = 0.1
num_eva = 3

# wn integration axes
num_wni_theta = 3000
num_wni_phi = 5000
#num_wni_theta = 12000
#num_wni_phi = 35000

wni_eva = 0.1
#wni_eva = 0

faxis = np.arange(nfreq) / nfreq * (fmax - fmin) + fmin
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# compaire result with stationary phase
wns = wn_synthesis.WaveNumberSynthesis(r_src, r_rcr, lwave)

wni_phi_axis = np.arange(num_wni_phi) / num_wni_phi * np.pi / 2
# create that wni axis with some evanescent tail
wni_theta_axis = np.linspace(0, np.pi / 2, num_wni_theta)
dth = wni_theta_axis[1] - wni_theta_axis[0]
eva = np.arange(dth, wni_eva, dth) * 1j

wni_theta_axis = np.hstack([eva[::-1], wni_theta_axis])

# wni differentials
wni_dth = np.diff(wni_theta_axis)
wni_dth = np.hstack([wni_dth, wni_dth[-1]])
wn_dphi = np.abs(wni_phi_axis[1] - wni_phi_axis[0])

# make a common angle axis for all frequencies
theta_axis = np.linspace(0, np.pi / 2, num_theta_r)
dth = theta_axis[1] - theta_axis[0]
eva = np.arange(dth, eva_range, dth) * 1j
theta_axis = np.hstack([eva[::-1], theta_axis])

alp_inc = np.cos(wni_phi_axis)[:, None] * np.cos(wni_theta_axis)[None, :] + 0j
gam_inc = np.sin(wni_theta_axis)
diff_scale = np.abs(np.cos(wni_theta_axis)) 
rrx = r_rcr[0]
rsx = r_src[0]
rrz = r_rcr[-1]
rsz = r_src[-1]

def one_freq(fin):
    """Calculate the image source frequency by frequency"""
    ktest = 2 * np.pi * fin / 1500
    alp_n = alp_inc
    gam_n = np.sin(wni_theta_axis)

    iterm = ne.evaluate('exp(1j * ktest * (alp_n * rrx'+
                        '- alp_inc * rsx - gam_n * rrz - gam_inc * rsz))'+
                        '* diff_scale * wni_dth')

    wn = 1j * ktest / (4 * np.pi ** 2) * np.sum(iterm) * wn_dphi
    # add negative alp_inc
    iterm = ne.evaluate('exp(1j * ktest * (-alp_n * rrx'+
                        '+ alp_inc * rsx - gam_n * rrz - gam_inc * rsz))'+
                        '* diff_scale * wni_dth')

    wn += 1j * ktest / (4 * np.pi ** 2) * np.sum(iterm) * wn_dphi
    image = -np.exp(1j * ktest * r_img) / (4 * np.pi * r_img)
    return np.array([-wn, image])

temp = one_freq(798.657)
print()
print()
print('quad result:          %.9f       + %.9f j'%
      (np.real(temp[0] * 1e3), np.imag(temp[0] * 1e3)))
print('stationary result:    %.9f       + %.9f j'%
      (np.real(temp[1] * 1e3), np.imag(temp[1] * 1e3)))
print()
print()
1/0
comp_result = np.array([one_freq(f) for f in faxis])

# expand faxis
df = faxis[1] - faxis[0]
NFFT = 2 * 1024
fexp = faxis[0] + np.arange(NFFT) * df
comp_exp = np.vstack([comp_result, np.zeros((NFFT - comp_result.shape[0], 2))])
#y_synth, t_synth = synthesize_ts(comp_result[:, 0], i_er_signal, faxis)
#y_img, t_img = synthesize_ts(comp_result[:, 1], i_er_signal, faxis)
y_synth, t_synth = synthesize_ts(comp_exp[:, 0], i_er_signal, fexp)
y_img, t_img = synthesize_ts(comp_exp[:, 1], i_er_signal, fexp)

fig, ax = plt.subplots()
ax.plot(t_synth, y_synth)
ax.plot(t_img, y_img)
#ax.plot(t_synth, -i_er_signal(t_synth - tau_img) / (4 * np.pi * r_img))
plt.show(block = False)
