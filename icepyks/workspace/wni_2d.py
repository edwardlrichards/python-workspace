import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import RegularGridInterpolator
from scipy.special import jv
from icepyks.poodle import cosine_surface, wn_synthesis
from scipy.linalg import solve
import numexpr as ne
from concurrent.futures import ThreadPoolExecutor

# source and reciever geometry
r_src = np.array([0., 0, -20])
r_rcr = np.array([200., 0, -10])
r_img = np.sqrt((r_src[0] - r_rcr[0]) ** 2 + (r_src[-1] + r_rcr[-1]) ** 2)

# cosine surface specificatons
hwave = 2.
lwave = 40.
kwave = 2 * np.pi / lwave
max_freq = 1000  # Hz

# theta, frequency and order number axis specifications
num_theta_r = 300
eva_range = 0.1
num_eva = 3
num_frequencies = 150

# wn integration axes
num_wni_theta = 3000
num_wni_phi = 5000
wni_eva = 0.1

# compaire result with stationary phase
wns = wn_synthesis.WaveNumberSynthesis(r_src, r_rcr, lwave)

wni_phi_axis = np.arange(num_wni_phi) / num_wni_phi * np.pi / 2
# create that wni axis with some evanescent tail
wni_theta_axis = np.linspace(0, np.pi / 2, num_wni_theta)
dth = wni_theta_axis[1] - wni_theta_axis[0]
eva = np.arange(dth, wni_eva, dth) * 1j

wni_theta_axis = np.hstack([eva[::-1], wni_theta_axis])

# wni differentials
wni_dth = np.diff(wni_theta_axis)
wni_dth = np.hstack([wni_dth, wni_dth[-1]])
wn_dphi = np.abs(wni_phi_axis[1] - wni_phi_axis[0])

# make a common angle axis for all frequencies
theta_axis = np.linspace(0, np.pi / 2, num_theta_r)
dth = theta_axis[1] - theta_axis[0]
eva = np.arange(dth, eva_range, dth) * 1j
theta_axis = np.hstack([eva[::-1], theta_axis])

# setup a common qvector for all frequencies
a = np.real(np.cos(theta_axis[0]))
qvec_1 = np.arange(np.ceil(-max_freq * lwave / 1500 * (1 + a)) - num_eva,
                    np.floor(max_freq * lwave / 1500 * (1 - a)) + num_eva)
a = np.real(np.cos(theta_axis[-1]))
qvec_2 = np.arange(np.ceil(-max_freq * lwave / 1500 * (1 + a)) - num_eva,
                    np.floor(max_freq * lwave / 1500 * (1 - a)) + num_eva)
all_qs = np.unique(np.hstack([qvec_1, qvec_2]))

# frequency axis
freq_axis = np.arange(num_frequencies) / (num_frequencies - 1) * max_freq

calc_rs = np.zeros((num_frequencies, theta_axis.size, all_qs.size),
                       dtype=np.complex_)

# common angle and position quanitities
a_max = np.sqrt(1 - (np.real(np.cos(wni_theta_axis))[None, :]
                     * np.sin(wni_phi_axis)[:, None]) ** 2 + 0j)
a_i = np.bitwise_not(np.isnan(a_max))

alp_inc = np.cos(wni_phi_axis)[:, None] * np.cos(wni_theta_axis)[None, :] + 0j
gam_inc = np.sin(wni_theta_axis)
diff_scale = np.abs(np.cos(wni_theta_axis)) 
rrx = r_rcr[0]
rsx = r_src[0]
rrz = r_rcr[-1]
rsz = r_src[-1]

def rayleigh_rs(theta, qvec, ka):
    """Compute rayleigh reflection coefficents"""
    # compute a q vector
    alphas = ka * np.cos(theta) + qvec * kwave
    betas = np.sqrt(ka ** 2 - alphas ** 2 + 0j)
    # setup rayleigh system of equations
    bs = -1j ** qvec * jv(qvec, hwave / 2 * ka * np.sin(theta))
    nm_diff = qvec[:, None] - qvec[None, :]
    A = 1j ** nm_diff * jv(nm_diff, -betas * hwave / 2)
    return solve(A, bs)

# create interpolator
def r_interper(qnum):
    """Create 2 intepolators in k ** 2, kx for q number. One real, on imag"""
    q_i = all_qs == qnum
    a = np.real(np.cos(theta_axis))
    # force last value to 0
    a[-1] = 0.
    k = 2 * np.pi * freq_axis / 1500
    data = np.squeeze(calc_rs[:, :, q_i])

    # need to reverse a and  r data to make monotonically increasing
    inter = RegularGridInterpolator((k, a[:: -1]), data[:, ::-1])
    return inter

def one_freq(fin, qnum, pos_ier, neg_ier):
    """Calculate the scatter solution frequency by frequency, qth Bragg order
    """
    ktest = 2 * np.pi * fin / 1500
    alp_n = alp_inc + qnum * kwave / (ktest * a_max + np.spacing(1))
    gam_n = np.sqrt(a_max ** 2 - alp_n ** 2)

    # setup interpolation grid
    gen_in = np.moveaxis(np.array([ktest * a_max, np.real(alp_inc)]), 0, -1)
    interp_map = lambda ier: ier(gen_in)
    with ThreadPoolExecutor(max_workers=2) as executor:
        interps = list(executor.map(interp_map, (pos_ier, neg_ier)))
    r_pos = interps[0]
    r_neg = interps[1]

    iterm = ne.evaluate('r_pos * exp(1j * ktest * (alp_n * rrx'+
                        '- alp_inc * rsx - gam_n * rrz - gam_inc * rsz))'+
                        '* diff_scale * wni_dth')

    wn = 1j * ktest / (4 * np.pi ** 2) * np.sum(iterm) * wn_dphi

    # add negative alp_inc
    iterm = ne.evaluate('r_neg * exp(1j * ktest * (-alp_n * rrx'+
                        '+ alp_inc * rsx - gam_n * rrz - gam_inc * rsz))'+
                        '* diff_scale * wni_dth')

    wn += 1j * ktest / (4 * np.pi ** 2) * np.sum(iterm) * wn_dphi
    return wn

for i, freq in enumerate(freq_axis):
    k_acous = 2 * np.pi * freq / 1500
    for j, th in enumerate(theta_axis):
        a = np.real(np.cos(th))
        qvec = np.arange(np.ceil(-freq * lwave / 1500 * (1 + a)) - num_eva,
                         np.floor(freq * lwave / 1500 * (1 - a)) + num_eva)
        savei = np.bitwise_and(all_qs >= qvec[0],  all_qs <= qvec[-1])
        calc_rs[i, j, savei] = rayleigh_rs(th, qvec, k_acous)

# wni integral, q = 0
qtest = 0
ii = r_interper(qtest)
1/0
fi = np.argmin(np.abs(freq_axis - 800))

ftest = freq_axis[fi]
ktest = 2 * np.pi * ftest / 1500

rtest = np.squeeze(calc_rs[fi, :, :])

itest = lambda rin: -np.ones_like(rin[:, :, 0])
wn_img = one_freq(ftest, 0, itest, itest)
wn_sta_img = wns.kx_sta_ky_sta(0, all_qs,
                               -np.ones_like(rtest), theta_axis, ftest) / 4 / np.pi
print('image results:')
print('quad result:          %.6f       + %.6f j'%
      (np.real(wn_img * r_img), np.imag(wn_img * r_img)))
print('stationary result:    %.6f       + %.6f j'%
      (np.real(wn_sta_img * r_img), np.imag(wn_sta_img * r_img)))
wn_sta = wns.kx_sta_ky_sta(0, all_qs, rtest, theta_axis, ftest) / 4 / np.pi
wn_full = one_freq(ftest, 0, ii, ii)

print()
print('R0 results:')
print('quad result:          %.6f       + %.6f j'%
      (np.real(wn_full * r_img), np.imag(wn_full * r_img)))
print('stationary result:    %.6f       + %.6f j'%
      (np.real(wn_sta * r_img), np.imag(wn_sta * r_img)))
