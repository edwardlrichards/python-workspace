import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as colors
import matplotlib.cm as cmx
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field

# setup the batch load parameters
hwave = 2.
lwave = 40.
attn = 1.
#signal parameter
fc = [1.5e3, 2.5e3, 3.5e3]
frange = (500, 2500)  # generous bounds
fs = 50e3

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
t_wave = 0.7  # time in seconds
wp = 2 * np.pi * t_wave / wave_period

# specify source and receiver location
rsrc = np.array([0., 0, -20])
z_source = rsrc[-1]
rrcr = np.array([200., 0, -10])

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc[1])
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# HK integrations
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wp)
# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri
# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# ray solution
ray_pf = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                               range_buffer=r_buffer)
ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
ray_pf.to_top(ray_gun)
ray_pf.to_wave()

ray_HK = surface_integral.TimeHK(ray_pf, xaxis, taxis, i_er_signal)
ray_result = ray_HK(rrcr)

tau_shad = ray_pf.delay(xaxis, np.zeros_like(xaxis), 'source')\
           + ray_pf.delay(xaxis, np.zeros_like(xaxis), 'receiver')

# find contiguous shadowed sections
is_shad = ray_pf.is_shadow(xaxis)
edges = np.diff(np.array(is_shad, dtype=np.int_))
# find edges
upi = np.where(edges == 1)[0]
downi = np.where(edges == -1)[0]
if downi[0] < upi[0]:
    upi = np.hstack([0, upi])
if upi[-1] > downi[-1]:
    downi[-1] = xaxis.size - 1

ray_pf('wave')
tau_naive = ray_pf.delay(xaxis, np.zeros_like(xaxis), 'source')\
            + ray_pf.delay(xaxis, np.zeros_like(xaxis), 'receiver')

iso_pf = point_iso_field.IsospeedPoint(eta, z_source)
iso_HK = surface_integral.TimeHK(iso_pf, xaxis, taxis, i_er_signal)
eig_er = eigen_rays.EigenRays(iso_HK, rrcr)
eig_x = eig_er.eig_x
eig_ts = [ea * i_er_signal(taxis - et) for (ea, et)
            in zip(eig_er.eig_amp, eig_er.eig_tau)]

et_sort = np.argsort(eig_er.eig_tau)

# set up eigen ray color map
cmv = np.arange(len(eig_x))
cNorm = colors.Normalize(vmin=0, vmax=10)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=plt.get_cmap('copper_r'))

# create flat surface delay
flat_dist = np.sqrt(xaxis ** 2 + z_source ** 2)\
            + np.sqrt((rrcr[0] - xaxis) ** 2 + rrcr[-1] ** 2)
flat_tau = flat_dist / 1500.

# # plot eigen ray time series
# fig, ax = plt.subplots()
# for e_ts in eig_ts:
    # ax.plot((taxis - t_spec) * 1e3, e_ts, 'b')

gs = gridspec.GridSpec(2,2)
gs.update(left=0.1, hspace=0.1)
fig = plt.figure()
#axes = [plt.subplot(gs[0, :]), plt.subplot(gs[1, :])]
axes = [plt.subplot(gs[1, :]), plt.subplot(gs[0, :])]

tau_min = -1

axes[0].plot(xaxis, (tau_naive - t_spec) * 1e3, color='k')
axes[0].plot(xaxis, (flat_tau - t_spec) * 1e3, color='k')

# plot the half cycle time difference
half_scale = 1 / (2 * np.array(fc))
eig_tmin = min(eig_er.eig_tau)
eig_tmax = max(eig_er.eig_tau)
x_plot = np.array([8, 18, 28]) + 8

#for xp, hs, col in zip(x_plot, half_scale, ['C2', 'C4', 'k']):
    #axes[0].plot([xp - 3, xp + 3], [(eig_tmax - t_spec) * 1e3] * 2,
                 #color=col, linewidth=3.5)
    #axes[0].plot([xp - 3, xp + 3], [(eig_tmax - hs - t_spec) * 1e3] * 2,
                 #color=col, linewidth=3.5)

#axes[0].text(19, 1.1, r'$\frac{T_{c}}{2}$', fontsize=32)
#axes[0].plot(xbrace, ybrace / 2.5 + 0.7, 'k')

# plot side tick marks
#for i, eti in enumerate(et_sort):
    #cval = scalarMap.to_rgba(cmv[i])
    #axes[0].plot([0, 10],
                 #[(eig_er.eig_tau[eti] - t_spec) * 1e3] * 2, color=cval)

# plot and annote eigen points
for i, eti in enumerate(et_sort):
    cval = scalarMap.to_rgba(cmv[i])
    ah = 200  # arbitrary horizontal scale
    av = 2000  # arbitrary vertical scale


    t = (eig_er.eig_tau[eti] - t_spec) * 1e3
    a = eig_er.eig_amp[eti]
    x = eig_er.eig_x[eti]

    axes[0].plot(eig_er.eig_x[eti],
                 (eig_er.eig_tau[eti] - t_spec) * 1e3, 'o', color='k')


    if np.abs(np.imag(a)) > 1e-6:
        # Horizontal axis
        axes[0].plot(x, np.abs(a) * ah + tau_min,
                'D', color='k', markersize=6)
        axes[0].plot(np.array([x, x]), np.abs(np.array([0, a])) * ah + tau_min,
                'C2', color='k')
        # Vertical axis
        axes[0].plot(np.abs(a) * av, t, 'D', color='k', markersize=6)
        axes[0].plot(np.abs(np.array([0, a])) * av, np.array([t, t]),
                'C2', color='k')

    else:
        # Horizontal axis
        axes[0].plot(x, np.abs(a) * ah + tau_min, 'ko', markersize=6)
        axes[0].plot(np.array([x, x]),
                np.abs(np.array([0, a])) * ah + tau_min, 'k')
        # Vertical axis
        axes[0].plot(np.abs(a) * av, t, 'ko', markersize=6)
        axes[0].plot(np.abs(np.array([0, a])) * av, np.array([t, t]), 'k')

axes[1].plot(xaxis, eta.z(xaxis), color='k')
for u, d in zip(upi, downi):
    axes[1].plot(xaxis[u: d], eta.z(xaxis[u: d]) + 0.05, color='0.4', linewidth=5)
    axes[0].plot(xaxis[u: d], tau_naive[u: d] * 1e3, color='k', linewidth=2)

# plot eigen rays
for i, eti in enumerate(et_sort):
#for x in eig_x:
    cval = scalarMap.to_rgba(cmv[i])
    axes[1].plot([0, eig_x[eti]], [z_source, eta.z(eig_x[eti])], color='k')
    axes[1].plot([eig_x[eti], rrcr[0]], [eta.z(eig_x[eti]), rrcr[-1]], color='k')
    axes[1].plot(eig_x[eti], eta.z(eig_x[eti]), 'o', color='k', markersize=6)

axes[0].grid(color='0.9')
axes[1].grid(color='0.9')
axes[0].set_ylim(tau_min, 3)
axes[1].set_ylim(-2, 1.5)
axes[1].set_yticks([-1, 0, 1])
axes[0].set_yticks([0, 1, 2])
axes[1].set_xlim(0, 200)
axes[0].set_xlim(0, 200)
axes[1].set_xticklabels('')
fig.text(0.1, 0.882, '(a)')
fig.text(0.1, 0.48, '(b)')

#axes[0].set_title('Travel time curve and eigen rays')
axes[0].set_xlabel(r'position, $x$ (m)')
axes[0].set_ylabel(r'Delay re. image arrival, $\tau$ (ms)')
axes[1].set_ylabel('position, $z$ (m)')
#axes[1].yaxis.set_label_coords(-.034, 0.6)

#plt.savefig('/home/e2richards/personal_essays/figures/travel_time')

r1 = np.sqrt((z_source - eta.z(eig_x)) ** 2 + eig_x ** 2 )
r2 = np.sqrt((rrcr[-1] - eta.z(eig_x)) ** 2 + (rrcr[0] - eig_x) ** 2)
cos1 = eig_x / r1
cos2 = - (rrcr[0] - eig_x) / r2

np.arctan(eta.z_p(eig_x))

plt.show(block=False)
1/0
plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure3.pdf', format='pdf')
