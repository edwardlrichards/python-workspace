import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.surfaces import sin_surface
from icepyks.porty import point_iso_field

# setup the batch load parameters
hwave = 2.
lwave = 40.
attn = 1.
#signal parameter
#fc = 2.566e3  # max value
fc = 2.5e3  # max value
fs = 100e3

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
t_wave = 0.633  # time in seconds
wp = 2 * np.pi * t_wave / wave_period

# specify source and receiver location
rsrc = np.array([0., 0, -20])
z_source = rsrc[-1]
rrcr = np.array([200., 0, -10])

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# HK integrations
# setup spatial integration parameters
r_buffer = 50.
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wp)
# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri
# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# ray solution
iso_pf = point_iso_field.IsospeedPoint(eta, z_source)
iso_HK = surface_integral.TimeHK(iso_pf, xaxis, taxis, i_er_signal)
iso_result = iso_HK(rrcr)

_, t1 = iso_pf.psi_inc(xaxis)
_, t2 = iso_pf.rcr_greens(xaxis, rrcr)

tau_naive = t1 + t2

eig_er = eigen_rays.EigenRays(iso_HK, rrcr)
eig_x = eig_er.eig_x
eig_ts = [ea * i_er_signal(taxis - et) for (ea, et)
            in zip(eig_er.eig_amp, eig_er.eig_tau)]
et_sort = np.argsort(eig_er.eig_tau)

# plot eigen ray time series
cmv = np.arange(len(eig_x))
cNorm = colors.Normalize(vmin=0, vmax=10)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=plt.get_cmap('copper_r'))

fig, ax = plt.subplots()
ax.plot((taxis - t_spec) * 1e3, iso_result / direct_amp)
ax.grid()
ax.set_ylim(-1.4, 1.4)
ax.set_ylabel('pressure re. image arrival')
ax.set_xlabel('time, ms')
ax.set_title('HK timeseries')
ax.set_xlim(-0.5, 5)


fig, ax = plt.subplots()
for i, eti in enumerate(et_sort):
    cval = scalarMap.to_rgba(cmv[i])
    ax.plot((taxis - t_spec) * 1e3, eig_ts[eti] / direct_amp, color=cval)

ax.grid()
ax.set_ylim(-0.7, 0.7)
ax.set_ylabel('pressure re. image arrival')
ax.set_xlabel('time, ms')
ax.set_title('Eigen ray picture of arrival')
ax.set_xlim(-0.5, 3)
#plt.savefig('/home/e2richards/personal_essays/figures/travel_time')

plt.show(block=False)
