import numpy as np
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.porty import point_iso_field
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# setup the batch load parameters
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
t_wave = 0.633  # time in seconds

wp = 2 * np.pi * t_wave / wave_period

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wp)

# setup a plotting xaxis
x_bounds = (-20, rrcr[0] + 20)
dx = 1
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup eigen ray tracing
# Specular image source information isnt really used
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri
# taxis isn't really used
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
fs = 50e3
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
# xmitt signal information isnt really used
y_signal, t_signal = pulse_signal.pulse_q1(2500)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

iso_pf = point_iso_field.IsospeedPoint(eta, z_source)
iso_HK = surface_integral.TimeHK(iso_pf, xaxis, taxis, i_er_signal)
eig_er = eigen_rays.EigenRays(iso_HK, rrcr)
eig_x = eig_er.eig_x
eig_ts = [ea * i_er_signal(taxis - et) for (ea, et)
            in zip(eig_er.eig_amp, eig_er.eig_tau)]

et_sort = np.argsort(eig_er.eig_tau)

# set up eigen ray color map
cmv = np.arange(len(eig_x))
cNorm = colors.Normalize(vmin=0, vmax=10)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=plt.get_cmap('copper_r'))

# plot the test setup

fig, ax = plt.subplots(figsize=(8, 6))

startx = -10
endx = 30
starti = np.argmax(xaxis > startx)
endi = np.argmax(xaxis > endx)
ax.plot(xaxis[: starti], eta.z(xaxis[:starti]), 'k')
ax.plot(xaxis[np.bitwise_and(xaxis > startx, xaxis < endx)],
        eta.z(xaxis[np.bitwise_and(xaxis > startx, xaxis < endx)]), 'k')
ax.plot(xaxis[endi-1:], eta.z(xaxis[endi-1:]), 'k')
ax.plot(rsrc[0], rsrc[-1], 'k.', markersize=12)
ax.plot(rrcr[0], rrcr[-1], 'k.', markersize=12)

# eta definitions
xoff = lwave * (0.25 - wp / (2 * np.pi))
v1 = 2 * lwave + xoff
v2 = 3 * lwave + xoff
v3 = 1.25 * lwave + xoff
v4 = 1.4 * lwave + xoff

# wave definitions
ax.plot([v1, v1], np.array([hwave / 2, hwave / 2 + 1.5]) + 0.5, 'k')
ax.plot([v2, v2], np.array([hwave / 2, hwave / 2 + 1.5]) + 0.5, 'k')
plt.annotate("",
             xy=(v1, hwave / 2 + 1), xycoords='data',
             xytext=(v2, hwave / 2 + 1), textcoords='data',
             arrowprops={'arrowstyle': '<->'})
ax.text((v1 + v2) / 2 - 4, 3, '$\Lambda$', fontsize=32)

ax.plot([v3 - 2.5, v3 + 2.5], [hwave / 2, hwave / 2], 'k')
ax.plot([v3 - 2.5, v3 + 2.5], [-hwave / 2, -hwave / 2], 'k')
ax.text(v3 - 16, 1.2, '$H$', fontsize=32)


plt.annotate("$\eta(x)$",
             xy=(v4 + 105, 1), xycoords='data',
             xytext=(v4 + 70, 7), textcoords='data',
             arrowprops={'arrowstyle': '->',
                         'connectionstyle':'arc3, rad=-0.3'})
plt.annotate("$\hat{n}$",
             xy=(195, eta.z(195) - 0.1), xycoords='data',
             xytext=(195 - 12,  eta.z(195) + 3.5),
             textcoords='data',
             arrowprops={'arrowstyle': '<-'})

# element specifications
ax.text(rsrc[0] - 14, rsrc[-1] - 3.3, r'$\vec{r}_{src}$', fontsize=32)
ax.text(rrcr[0] - 14, rrcr[-1] - 3.3, r'$\vec{r}_{rcr}$', fontsize=32)

ax.plot([rsrc[0], rsrc[0]], [-800, 800], '--', color='0.7', linewidth=0.5)
ax.plot([rrcr[0], rrcr[0]], [-800, 800], '--', color='0.7', linewidth=0.5)
ax.plot([-800, 800], [rsrc[-1], rsrc[-1]], '--', color='0.7', linewidth=0.5)
ax.plot([-800, 800], [rsrc[-1], rsrc[-1]], '--', color='0.7', linewidth=0.5)
ax.plot([-800, 800], [0, 0], '--', color='0.7', linewidth=0.5)

for i, eti in enumerate(et_sort):
#for x in eig_x:
    cval = scalarMap.to_rgba(cmv[i])
    ax.plot([0, eig_x[eti]], [z_source, eta.z(eig_x[eti])], color='0.6')
    ax.plot([eig_x[eti], rrcr[0]], [eta.z(eig_x[eti]), rrcr[-1]], color='0.6')

# axis specification
ax.annotate("",
            xy=(0, 3), xycoords='data',
            xytext=(0, -0.1), textcoords='data',
            arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3"),
            )
ax.annotate("",
            xy=(16, 0), xycoords='data',
            xytext=(-1, 0), textcoords='data',
            arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3"),
            )

ax.text(-9, 2, '$z$', fontsize=32)
ax.text(7, -2.5, '$x$', fontsize=32)
ax.text(7, 2.5, '$\otimes y$', fontsize=32)

ax.text(85, -22, r'\begin{eqnarray*} H&:& 2 \ \textrm{m} \\'\
                 + r'\Lambda &:& 40 \ \textrm{m}\\'\
                 + r'\Delta x &:& 200 \ \textrm{m} \end{eqnarray*}',
         bbox=dict(boxstyle="square",
                   ec=(0.6, 0.6, 0.6),
                   fc=(1., 1., 1.),
                   ),
        fontsize=22)

ax.set_ylim(-25, 10)
ax.set_xlim(-20, 220)

#ax.set_title('Numerical simulation setup')
ax.set_ylabel('Position, $z$ (m)')
ax.set_xlabel('Position, $x$ (m)')
plt.subplots_adjust(bottom=0.15, left=0.145)

plt.show(block=False)
plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure1.pdf', format='pdf')
