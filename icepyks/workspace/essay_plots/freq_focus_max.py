import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.surfaces import sin_surface
from icepyks.porty import point_iso_field

# setup the batch load parameters
hwave = 2.
lwave = 40.
attn = 1.
#signal parameter
fc = np.arange(1000) * 3 + 1e3
fs = 100e3

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
t_wave = 0.633  # time in seconds
wp = 2 * np.pi * t_wave / wave_period

# specify source and receiver location
rsrc = np.array([0., 0, -20])
z_source = rsrc[-1]
rrcr = np.array([200., 0, -10])

# HK integrations
# setup spatial integration parameters
r_buffer = 50.
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 1e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wp)
# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

def one_freq(center_f):
    """Find the max timse series value for each center frequency"""
    if (center_f % 100) == 0:
        print(center_f)

    # xmitt signal information
    y_signal, t_signal = pulse_signal.pulse_q1(center_f)
    i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)
    # ray solution
    iso_pf = point_iso_field.IsospeedPoint(eta, z_source)
    iso_HK = surface_integral.TimeHK(iso_pf, xaxis, taxis, i_er_signal)
    iso_result = iso_HK(rrcr)
    return np.max(np.abs(iso_result))

max_arr = np.array([one_freq(f) for f in fc])

fig, ax = plt.subplots()
ax.plot(fc, max_arr)

plt.show(block=False)
