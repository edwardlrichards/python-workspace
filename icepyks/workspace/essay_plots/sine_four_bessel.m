%Bessel-Weighted four periods sine fuction
function src_sig = sine_four_bessel(fc_src, Fs)
T = 1 / fc_src;
num_cycles = 4;
total_time = num_cycles * T;
tpulse = (0:1/Fs:total_time); %time vector
src_sig = sin(2 * pi * fc_src * tpulse) .* kaiser(length(tpulse), 2.5 * pi).';


%tpulse=[0:itp-1]/Fs;
%figure;
%plot(tpulse,s);
