tic();
% clear;
clc;
%3D plane:point source
%Sound speed in water%
C_s=1500;%m/s
%Source & Receiver%
x_src=0.0;
y_src=0.0;
z_src=-20.0;

ang_obl=0;%degree

Dist_OB=200;
x_ob=Dist_OB*cosd(ang_obl);
y_ob=Dist_OB*sind(ang_obl);
z_ob=-10.0;

%Souce signal%
freq=2500;%Center frequency
Fs=freq*50;%Sampling frequency
k_num=2*pi*freq/C_s;
lamda=C_s/freq;


%four period hanning-weighted sine function
% sig_src=sine_four_hanning(freq,Fs);
sig_src=sine_four_bessel(freq,Fs);
t_src=[0:length(sig_src)-1]/Fs;
leng_src=length(t_src);

figure;
plot(t_src, sig_src)

% derivative of source function wrt time
sig_src_prime=gradient(sig_src)./gradient(t_src);%ds/dt
sig_src_prime_hil=imag(hilbert(sig_src_prime));

%Surface wave%
%lamda_surf=39.032700;h_surf=1.4;surf_b=h_surf*(-1.01);
lamda_surf=40;
% h_surf=0.4;
% h_surf=0.2;
% h_surf=0.1;
h_surf=1;
surf_b=h_surf*(-1.01);
k_surf=2*pi/lamda_surf;
g=9.8;%m/s^2
w_surf=sqrt(g*k_surf);
T_surf=2*pi/w_surf;%Hz
x_gaps=lamda/16;%x_gap=min(lamda_surf*0.1,lam*0.3);
x_strs=x_src-20;
x_ends=x_ob+20;
%x_strs=x_src;x_ends=x_ob;
x_surf=[x_strs:x_gaps:x_ends];
n_surfx=length(x_surf);
%Received signal
dist_dir=sqrt((x_src-x_ob)^2+(y_src-y_ob)^2+(z_src-z_ob)^2);
t_delay_dir=dist_dir/C_s;
tmin=0.9*t_delay_dir;tmax=2.0*t_delay_dir;
%tmin=0.12;tmax=0.17;
t_ob=[tmin:1/Fs:tmax];

% compute image source
ri = sqrt((z_src + z_ob) ^ 2 + (y_src - y_ob) ^ 2 ...
          + (x_src - x_ob) ^ 2);
ti = ri /1500;

[tmp, ti_start] = min(abs(t_ob - ri / C_s));
sig_im = zeros(size(t_ob));
sig_im(ti_start: ti_start + leng_src - 1) = -sig_src / ri;

%Time=[0:0.1:2*T_surf];
%Time=(1*pi/2)/(w_surf);
Time=0;
P_total=zeros(length(Time),length(t_ob));

for hh=1:length(Time)
    
    ttmp=num2str(Time(hh));
    ttmp=['Present time: ',ttmp];
    disp(ttmp);
    
    sig_ob=zeros(1,length(t_ob));
    ind_dir_str=round((t_delay_dir-tmin)*Fs);ind_dir_ed=ind_dir_str+leng_src-1;

%     if(ind_dir_ed<=length(t_ob))
%          sig_ob(ind_dir_str:ind_dir_ed)=sig_ob(ind_dir_str:ind_dir_ed)+sig_src/dist_dir;
%     else
%          ind_dir_ed=length(t_ob);  
%          sig_ob(ind_dir_str:ind_dir_ed)=sig_ob(ind_dir_str:ind_dir_ed)+sig_src(1:ind_dir_ed-ind_dir_str+1)/dist_dir;
%     end
    t_wave = 0.633;
    phs = 2 * pi * t_wave / T_surf;
    %phs=+pi/2;%-w_surf*Time(hh);
    %phs=0;
    NMM=200;
    for ii=1:n_surfx
          x_tmp=x_surf(ii);
          z_tmp=h_surf*sin(k_surf*x_tmp+phs);
          z_slope=k_surf*h_surf*cos(k_surf*x_tmp+phs);
          dist_sv=sqrt((x_tmp-x_src)^2+(z_tmp-z_src)^2);
          dist_rv=sqrt((x_tmp-x_ob)^2+(z_tmp-z_ob)^2);
          y_tmp=(dist_rv*y_src+dist_sv*y_ob)/(dist_rv+dist_sv);
        
          direc_xs=x_tmp-x_src;
          direc_ys=y_tmp-y_src;
          direc_zs=z_tmp-z_src;%directional vector(source to point on surface)
          x_str=direc_xs*(surf_b-z_tmp)/direc_zs+x_tmp;
          x_end=x_tmp;        
          x_gap=(x_end-x_str)/NMM;
          x_ray_rng_s=[x_str:x_gap:x_end];
          y_ray_rng_s=direc_ys*(x_ray_rng_s-x_tmp)/direc_xs+y_tmp;
          z_ray_rng_s=direc_zs*(x_ray_rng_s-x_tmp)/direc_xs+z_tmp;
          z_surf_rng_s=h_surf*sin(k_surf*x_ray_rng_s+phs);
          nn_tmp=length(z_surf_rng_s);
          diff_tmp_s=z_surf_rng_s-z_ray_rng_s;
          diff_tmp_s=diff_tmp_s(1:nn_tmp-1);
          Ind_s=find(diff_tmp_s<=0.0);
        
          direc_xr=x_tmp-x_ob;
          direc_yr=y_tmp-y_ob;
          direc_zr=z_tmp-z_ob;%directional vector(receiver to point on surface)
          x_str=direc_xr*(surf_b-z_tmp)/direc_zr+x_tmp;
          x_end=x_tmp;
          x_gap=(x_end-x_str)/NMM;
          x_ray_rng_r=[x_str:x_gap:x_end];
          y_ray_rng_r=direc_yr*(x_ray_rng_r-x_tmp)/direc_xr+y_tmp;
          z_ray_rng_r=direc_zr*(x_ray_rng_r-x_tmp)/direc_xr+z_tmp;
          z_surf_rng_r=h_surf*sin(k_surf*x_ray_rng_r+phs);nn_tmp=length(z_surf_rng_r);
          diff_tmp_r=z_surf_rng_r-z_ray_rng_r;diff_tmp_r=diff_tmp_r(1:nn_tmp-1);
          Ind_r=find(diff_tmp_r<=0.0);
          Ind_test=length(Ind_s)+length(Ind_r);         
          
%            if(Ind_test==0)
           if 1;
               dist_src=sqrt((x_tmp-x_src)^2+(y_tmp-y_src)^2+(z_tmp-z_src)^2);
               dist_ob=sqrt((x_tmp-x_ob)^2+(y_tmp-y_ob)^2+(z_tmp-z_ob)^2);        
               p_inc=1/(4*pi*dist_src);%Incident Green's function amplitude
               p_sct=1/(4*pi*dist_ob);%Scattered Green's function amplitude
               phi_dede=dist_sv^2/dist_src^3+dist_rv^2/dist_ob^3;
        
               Const=p_inc*p_sct*(-z_slope*(x_tmp-x_src)+(z_tmp-z_src))/dist_src;
               Const=Const*sqrt(2*pi/(k_num*phi_dede));            
               %Const=-2*Const/C_s*sig_src_prime*x_gap*4*pi;%radiation 1/r not 1/(4*pi*r)
            
               %Const1=-2*Const/C_s*sig_src_prime*x_gaps*4*pi;
               %Const2=0;
               
               Const1=-2*Const/C_s*sig_src_prime*x_gaps*4*pi*cos(pi/4);
               Const2=-2*Const/C_s*sig_src_prime_hil*x_gaps*4*pi*sin(pi/4);
                
               t_delay=(dist_src+dist_ob)/C_s;
               %hoho=hoho+1;
               %RNGTT(hoho)=x_tmp;
               %DELAY(hoho)=t_delay;
            
               delay_ind=round((t_delay-tmin)*Fs);
        
              if(delay_ind+leng_src-1<=length(t_ob))
                ind_str=delay_ind;
                ind_ed=delay_ind+leng_src-1;
                %sig_ob(ind_str:ind_ed)=sig_ob(ind_str:ind_ed)+Const1;
                sig_ob(ind_str:ind_ed)=sig_ob(ind_str:ind_ed)+(Const1+Const2);
              else
                ind_str=delay_ind;
                ind_ed=length(t_ob);  
                %sig_ob(ind_str:ind_ed)=sig_ob(ind_str:ind_ed)+Const1(1:ind_ed-ind_str+1);
                sig_ob(ind_str:ind_ed)=sig_ob(ind_str:ind_ed)+(Const1(1:ind_ed-ind_str+1)+Const2(1:ind_ed-ind_str+1));
              end
         end  
          
    end
    
    P_total(hh,:)=sig_ob;
    
end

figure;
%plot(t_ob,sig_ob/amps);
plot((t_ob - ti)*1000, sig_ob * ri);
xlabel('time, ms')
ylabel('pressure re image arrival')
% title('shadow corrected HK')
title('naive HK')
xlim([-0.5, 5])
ylim([-1.4, 1.4])
grid on
% hold on
% plot((t_ob - ti)*1000, sig_im * ri);
% 
% figure;
% plot(t_ob*1000, (sig_im - sig_ob)*ri);