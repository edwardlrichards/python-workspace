import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from scipy.special import hankel1, jv
from scipy.linalg import solve
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.poodle import plane_wave_rs
from icepyks.poodle import wn_synthesis
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field

# setup the wave specific solver
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
ftest = 500  # acoustic frequency
kac = 2 * np.pi * ftest / 1500

# specify source and receiver location
rsrc = np.array([0, 0., -20])
rrcr = np.array([200, 0., -10])

# wave number integration parameters
num_eva = 3
numa = 2 ** 16

# setup wave number integration
cos_ier = plane_wave_rs.CosineReflectionCoefficents(hwave, kwave, attn=attn)
# a static q vector seems fine for low frequencies
qvec = np.unique(np.hstack([cos_ier.qvec(num_eva, np.pi / 2, ftest),
                            cos_ier.qvec(num_eva, 0., ftest)]))

eva_range = 0.1  # excursion into imaginary land
num_thetas = 500  # number of points in reflection coefficent interpolation
theta_axis = wn_synthesis.make_theta_axis(num_thetas, eva_range)

wn_ier = wn_synthesis.WaveNumberSynthesis(rsrc, rrcr, lwave)

def rayleigh_rs(theta, qvec):
    """Compute rayleigh reflection coefficents"""
    # compute a q vector
    alphas = kac * np.cos(theta) + qvec * kwave
    betas = np.sqrt(kac ** 2 - alphas ** 2 + 0j)
    # setup rayleigh system of equations
    bs = -1j ** qvec * jv(qvec, hwave / 2 * kac * np.sin(theta))
    nm_diff = qvec[:, None] - qvec[None, :]
    A = 1j ** nm_diff * jv(nm_diff, -betas * hwave / 2)
    return solve(A, bs)

def hk_rs(theta, qvec):
    """Compute HK reflection coefficents"""
    a0 = kac * np.cos(theta)
    b0 = np.sqrt(kac ** 2 - a0 ** 2 + 0j)
    alphas = kac * np.cos(theta) + qvec * kwave
    betas = np.sqrt(kac ** 2 - alphas ** 2 + 0j)
    r_hk = (-1j) ** qvec * (a0 * (alphas - a0) - b0 * (b0 + betas))\
           * jv(qvec, hwave / 2 * (b0 + betas)) / (betas * (b0 + betas))
    return r_hk

# Wave number Rayliegh hypothesis
rs_RH = np.array([rayleigh_rs(ta, qvec) for ta in theta_axis])
rs_HK = np.array([hk_rs(ta, qvec) for ta in theta_axis])
qvec_sym, thetaaxis_sym, rs_RH_sym = wn_synthesis.make_symetric(qvec, theta_axis, rs_RH)
qvec_sym, thetaaxis_sym, rs_HK_sym = wn_synthesis.make_symetric(qvec, theta_axis, rs_HK)

# spatial HK integrations for eigen rays
# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
t_wave = 0.633  # time in seconds
wp = 2 * np.pi * t_wave / wave_period

# setup spatial integration parameters
r_buffer = 50.
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wp)
# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri
# time integration
fs = 100e3
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# ray solution
# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(2.5e3)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)
iso_pf = point_iso_field.IsospeedPoint(eta, rsrc[-1])
iso_HK = surface_integral.TimeHK(iso_pf, xaxis, taxis, i_er_signal)

eig_er = eigen_rays.EigenRays(iso_HK, rrcr)
eig_x = eig_er.eig_x
eig_theta = np.arctan2((eta.z(eig_x) - rsrc[-1]), eig_x)

et_sort = np.argsort(eig_er.eig_tau)

# bragg angles
theta_cut = np.arccos(1 - qvec_sym * kwave / kac)

# set up eigen ray color map
cmv = np.arange(len(eig_x))
cNorm = colors.Normalize(vmin=0, vmax=10)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=plt.get_cmap('copper_r'))

# plot low order reflection coefficents
ploti = np.where(np.bitwise_and(qvec_sym <= 0, qvec_sym >= -2))[0]
real_theta = np.abs(np.cos(thetaaxis_sym)) <= 1

fig, ax = plt.subplots()

marki = [np.argmin(np.abs(thetaaxis_sym[real_theta] - np.pi / 2))]
marki.append(np.argmin(np.abs(thetaaxis_sym[real_theta] - np.pi / 4)))
marki.append(np.argmin(np.abs(thetaaxis_sym[real_theta] - 3 * np.pi / 4)))

ax.plot(np.degrees(np.real(thetaaxis_sym[real_theta])),
        np.abs(rs_RH_sym[real_theta, ploti[2]]),
        '-', label='$R_{%i}$'%(0), linewidth=2.5, color='k')
ax.plot(np.degrees(np.real(thetaaxis_sym[real_theta])),
        np.abs(rs_RH_sym[real_theta, ploti[1]]),
        '-o', label='$R_{%i}$'%(-1), linewidth=2.5, markevery=marki,
        markersize=8, color='k')
ax.plot(np.degrees(np.real(thetaaxis_sym[real_theta])),
        np.abs(rs_RH_sym[real_theta, ploti[0]]),
        '-D', label='$R_{%i}$'%(-2), linewidth=2.5, markevery=marki, color='k')


plt.gca().set_prop_cycle(None)
#ax.plot(np.degrees(np.real(thetaaxis_sym[real_theta])),
        #np.abs(rs_HK_sym[np.ix_(real_theta, ploti)]),
        #marker='d', markevery=30, markersize=4, alpha=0.6)
ax.plot(np.degrees(np.real(thetaaxis_sym[real_theta])),
        np.abs(rs_HK_sym[np.ix_(real_theta, ploti[::-1])]),
        '--', linewidth=1.5, color='k')

for i, eti in enumerate(et_sort):
    cval = scalarMap.to_rgba(cmv[i])
    ax.plot([np.degrees(eig_theta[eti])] * 2, [-0.02, 0.03],
             color='k', clip_on=False)
    #ax.plot([180 - np.degrees(eig_theta[eti])] * 2, [-0.02, 0.03],
             #color=cval, clip_on=False)

ploti = np.bitwise_and(qvec_sym <= 2, qvec_sym >= 0)
for ba, q in zip(theta_cut[ploti], qvec_sym[ploti]):
    if q == 0: continue
    ba = 180 - np.degrees(ba)
    ax.annotate('', xy=(ba, 1.09), xytext=(ba, 1.15),
            clip_on=False,
            arrowprops=dict(arrowstyle="->"))
    #ax.plot([np.degrees(ba)]*2, [-10, 10], ':', linewidth=0.5, color='0.5')


ax.set_ylim(-0.01, 1.1)
ax.set_xlim(-1, 181)
ax.set_xticks([45, 90, 135])
ax.grid(color='0.9')
#ax.set_title('RH and HK scattered plane wave magnitudes'+
             #'\n' + r'$f$: ' + '%i Hz, '%ftest + '$H$: ' +
             #'%.1f m,'%hwave + r' $\Lambda$: ' + '%.1f m'%lwave)
ax.set_ylabel(r'Coefficent magnitude, $|R_n|$')
ax.set_xlabel(r'Incident angle, $\theta_{inc}$ (deg)')
ax.legend()

plt.show(block=False)
1/0
plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure5.pdf', format='pdf')
