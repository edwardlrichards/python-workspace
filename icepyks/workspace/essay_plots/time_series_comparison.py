import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.f_synthesis import synthesize_ts
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field
from icepyks.poodle.cosine_surface import CosineSurface
from icepyks.poodle import wn_synthesis
from concurrent.futures import ThreadPoolExecutor

num_dims = 3

# setup the batch load parameters
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
#save_dir = r'/enceladus0/e2richards/rayliegh_rs/'
save_dir = r'/Users/edwardlrichards/rayleigh_rs/'

#signal parameter
fc = 3.5e3
frange = (500, 6500)  # generous bounds
fs = 50e3

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
#t_wave = 0  # time in seconds
#t_wave = 0.633  # time in seconds
#t_wave = 1.39  # time in seconds
#t_wave = 2.53  # time in seconds
t_wave = 4.75  # time in seconds
wp = 2 * np.pi * t_wave / wave_period
x_shift = lwave * (wp - np.pi / 2) / (2 * np.pi)

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# wave number integration parameters
num_eva = 3
numa = 2 ** 16
eva_range = 0.1  # excursion into imaginary land
num_thetas = 200  # number of points in reflection coefficent interpolation
theta_axis = wn_synthesis.make_theta_axis(num_thetas, eva_range)

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)
fcompute = np.arange(frange[0], frange[1] + 1)

# HK integrations
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wp)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
kcenter = 2 * np.pi * fc / 1500

# direct arrival scaling term
if num_dims == 2:
    direct_amp = np.sqrt(2 / (np.pi * ri * kcenter))
else:
    direct_amp = 1 / ri

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
taxis = np.arange(num_t) / fs + t_bounds[0]

# ray solution
ray_pf = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                               range_buffer=r_buffer)
ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
ray_pf.to_top(ray_gun)
ray_pf.to_wave()

ray_HK = surface_integral.TimeHK(ray_pf, xaxis, taxis,
                                 i_er_signal, num_dims=num_dims)
ray_result = ray_HK(rrcr)

# isospeed solution
iso_field = point_iso_field.IsospeedPoint(eta, z_source)
isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis,
                                      i_er_signal, num_dims=num_dims)
iso_result = isospeed_HK(rrcr)

# compute scattered field using batch processing
# setup rayliegh wni
cos_ier = CosineSurface(hwave, kwave, attn=attn)
rs_shift = rsrc.copy(); rs_shift[0] = rs_shift[0] + x_shift
rr_shift = rrcr.copy(); rr_shift[0] = rr_shift[0] + x_shift
wn_ier = wn_synthesis.WaveNumberSynthesis(rs_shift, rr_shift, lwave)

def batch_run(frun, theta_axis, theory='HK', is_stationary=True):
    """Run through the rayleigh coefficents at each frequency"""
    # a static q vector seems fine for low frequencies
    qvec = np.unique(np.hstack([cos_ier.qvec(num_eva, np.pi / 2, frun),
                            cos_ier.qvec(num_eva, 0., frun)]))
    if theory == 'HK':
        rs = [cos_ier.r_hk(np.real(np.cos(ta)), qvec, frun, return_bs=False)
                    for ta in theta_axis]
        qvec, theta_axis, rs = wn_synthesis.make_symetric(qvec, theta_axis, rs)
    elif theory == 'RH':
        qvec, theta_axis, rs = wn_synthesis.read_coeff(hwave, lwave,
                                                       frun, save_dir)
    else:
        raise(ValueError('Theory has to be either HK or RH'))
    if is_stationary:
        p_ref = [wn_ier.kx_sta_ky_sta(q, qvec, rs, theta_axis, frun,
                                      num_dims=num_dims)
                 for q in qvec]
    else:
        p_ref = [wn_ier.theta_int(q, qvec, rs, theta_axis, frun, 20000,
                                  num_dims=num_dims)
                 for q in qvec]
    if (frun % 200) == 0:
        print('run number %i'%frun)
    return np.sum(p_ref)

rh_wn_FT = [batch_run(f, theta_axis, theory='RH') for f in fcompute]
#rh_wn_FT = [batch_run(f, theta_axis, theory='RH', is_stationary=False)
            #for f in fcompute]
rh_wn_ts, t_wn_rh = synthesize_ts(rh_wn_FT, i_er_signal, fcompute)
# upsample timeseries before plotting
rh_wn_ts, t_rh_wn = resample(rh_wn_ts, t_wn_rh.size * 5, t=t_wn_rh)

# eigen ray plot
#eig_er = eigen_rays.EigenRays(ray_HK, rrcr)
eig_er = eigen_rays.EigenRays(isospeed_HK, rrcr)
eig_ts = [ea * i_er_signal(taxis - et) for (ea, et)
          in zip(eig_er.eig_amp, eig_er.eig_tau)]
eig_ts = np.sum(np.array(eig_ts), axis=0)

# create differences plots
rh_ts = np.interp(taxis, t_rh_wn, rh_wn_ts, left=0., right=0.)

comp_ts = rh_ts
d2 = iso_result - comp_ts
d3 = ray_result - comp_ts

fig, axes = plt.subplots(2, 1, sharex=True)
fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
axes[0].plot((taxis - t_spec) * 1e3, rh_ts / direct_amp, label='RH')
axes[0].plot((taxis - t_spec) * 1e3, iso_result / direct_amp, label='nHK')
axes[0].plot((taxis - t_spec) * 1e3, ray_result / direct_amp, label='sHK')
#axes[0].plot((taxis - t_spec) * 1e3, eig_ts / direct_amp, label='eigen')

axes[1].plot((taxis - t_spec) * 1e3, d2 / direct_amp, color='C1')
axes[1].plot((taxis - t_spec) * 1e3, d3 / direct_amp, color='C2')

axes[0].yaxis.tick_right()
axes[1].yaxis.tick_right()

axes[1].set_xlim(-0.5, 5)
axes[0].set_ylim(-1.4, 1.4)
axes[1].set_ylim(-1.4, 1.4)
#axes[0].set_ylabel('pressure, re. image arrival')

if num_dims == 2:
    plt.ylabel(r'pressure, re. $\sqrt{\frac{2}{\pi \ r_{img} \ k_c}}$')
else:
    plt.ylabel('pressure, re. image arrival')

if num_dims == 2:
    axes[0].set_title('time series, t=%.2f sec\n'%t_wave+
                    'wave height = %.1f m, length = %.1f m'%(hwave, lwave))
else:
    axes[0].set_title('time series, t=%.2f sec\n'%t_wave+
                    'wave height = %.1f m, length = %.1f m'%(hwave, lwave))

axes[1].set_xlabel('time, ms')
axes[1].set_title('Difference from RH')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)

plt.show(block=False)
