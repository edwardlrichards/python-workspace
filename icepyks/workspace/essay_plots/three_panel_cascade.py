import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.porty import point_iso_field
from os.path import join

# load rh cascade as a precomputed result
rh_cascade = np.load('cascade_npzs/rh_cascade.npz')

# setup the HK
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km

#signal parameter
fc = 2.5e3
frange = (500, 4500)  # generous bounds
fs = 50e3
toi = 0.7

# wave phase
nump = 160
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
wp = np.arange(nump) / nump * 2 * np.pi  # time in seconds
t_wave = np.arange(nump) / nump * wave_period  # time in seconds
x_shift = lwave * (wp - np.pi / 2) / (2 * np.pi)

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
kcenter = 2 * np.pi * fc / 1500
direct_amp = 1 / ri

# HK integrations
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
taxis = np.arange(num_t) / fs + t_bounds[0]

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=0.)

naive_series = []
eigen_series = []
# HK solution
for p in wp:
    eta = sin_surface.Sine(lwave, hwave, phase=p)
    iso_field = point_iso_field.IsospeedPoint(eta, z_source)
    naive_HK = surface_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal)
    erays = eigen_rays.EigenRays(naive_HK, rrcr)
    naive_series.append(naive_HK(rrcr))
    eigen_series.append(erays.make_timeseries())

naive_series = np.array(naive_series)
eigen_series = np.array(eigen_series)
num_repeat = 20
plotymax = 3
expandt = np.hstack([-t_wave[num_repeat:0: -1], t_wave])

ploter = naive_series.T / direct_amp
ploter = np.concatenate([ploter[:, -num_repeat:], ploter], axis=-1)

ploter_e = eigen_series.T / direct_amp
ploter_e = np.concatenate([ploter_e[:, -num_repeat:], ploter_e], axis=-1)

ind_b = np.argmin(np.abs(toi - expandt))

# interpolate ploter
ploter_ds = []
t_rh = rh_cascade['Y'][:, 0]
for i, rh in enumerate(rh_cascade['rh_series']):
    ploter_ds.append(np.interp((taxis - t_spec) * 1e3, t_rh, rh))
ploter_ds = np.array(ploter_ds)
ploter_ds = ploter_ds / direct_amp
ploter_ds = np.concatenate([ploter_ds[-num_repeat:, :], ploter_ds], axis=0)

X1, Y1 = np.meshgrid(expandt, (taxis - t_spec) * 1e3)

fig, axes = plt.subplots(1, 3, sharey=True)
gs = gridspec.GridSpec(100,100)

axes = (plt.subplot(gs[0:90, 0:30]),
        plt.subplot(gs[0:90, 31:61]), 
        plt.subplot(gs[0:90, 62:92]), 
        plt.subplot(gs[0:90, 95:99]))

fig.set_size_inches(6.5, 3)

fig.subplots_adjust(left=0.08, right=0.9, top=0.9, bottom=0.08)

pcm1 = axes[0].pcolormesh(X1, Y1, ploter_e, vmin=-1.2, vmax=1.2,
              cmap=plt.cm.PuOr_r)
pcm1 = axes[1].pcolormesh(X1, Y1, ploter, vmin=-1.2, vmax=1.2,
              cmap=plt.cm.PuOr_r)
pcm2 = axes[2].pcolormesh(X1, Y1, ploter_ds.T,
                          vmin=-1.2, vmax=1.2, cmap=plt.cm.PuOr_r)

#ax.set_xlabel('Surface wave time (s)')
#ax.set_ylabel('Pulse time (ms)')
fig.text(0.5, 0.01, "Surface wave time, $t_{wv}$ (s)", ha="center", va="bottom", size=12)
fig.text(0.01, 0.5, "Time re image arrival, $t$ (ms)", ha="left", va="center", rotation="vertical", size=12)
fig.text(0.08, 0.91, '(a) ERA', size=12)
fig.text(0.35, 0.91, '(b) HKA', size=12)
fig.text(0.60, 0.91, '(c) RFM', size=12)
axes[0].plot((expandt[ind_b], expandt[ind_b]), (-100, 100), 'k', linewidth=1)
axes[1].plot((expandt[ind_b], expandt[ind_b]), (-100, 100), 'k', linewidth=1)
axes[2].plot((expandt[ind_b], expandt[ind_b]), (-100, 100), 'k', linewidth=1)
# label switch
axes[1].annotate('t=0.70 s',
            xy=(expandt[ind_b], 2),
            xycoords='data',
            xytext=(expandt[ind_b] + 0.3, 2.5),
            textcoords='data',
            size=12,
            arrowprops=dict(arrowstyle="->"),
            clip_on=False)

axes[0].set_ylim(0, plotymax)
axes[1].set_ylim(0, plotymax)
axes[2].set_ylim(0, plotymax)
axes[0].set_xticks([0, 2, 4])
axes[0].set_xticklabels(['0', '', '4'], size=12)
axes[1].set_xticks([0, 2, 4])
axes[1].set_xticklabels(['0', '', '4'], size=12)
axes[2].set_xticks([0, 2, 4])
axes[2].set_xticklabels(['0', '', '4'], size=12)
axes[1].tick_params(left=False)
axes[2].tick_params(left=False)
#ax.set_title('Amplitude of arrival over a full wave cycle')
# remove top time label
axes[0].yaxis.set_ticks([0, 1, 2, 3])
axes[0].yaxis.set_tick_params(labelsize=12)
axes[1].yaxis.set_ticks([0, 1, 2, 3])
axes[1].yaxis.set_ticklabels('')
axes[2].yaxis.set_ticks([0, 1, 2, 3])
axes[2].yaxis.set_ticklabels('')

cbar = fig.colorbar(pcm2, cax=axes[3], ticks=[-1.2, -0.6, 0, 0.6, 1.2])
cbar.ax.set_yticklabels(['-1.2', '', '0', '', '1.2'], size=12)
cbar.set_label('Pressure, re. image arrival', size=12)
#cbar.ax.yaxis.set_label_coords(3, .55)

plt.show(block=False)

plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure6.pdf', format='pdf')
#plt.savefig('/home/e2richards/personal_essays/figures/rayleigh_cascade_ts')
#plt.savefig('/home/e2richards/personal_essays/figures/rayleigh_cascade_ts.eps',
            #format='eps')
