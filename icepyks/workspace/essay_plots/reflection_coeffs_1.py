import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from scipy.special import hankel1, jv
from scipy.linalg import solve
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.poodle.cosine_surface import CosineSurface
from icepyks.poodle import wn_synthesis
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field

# setup the wave specific solver
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
ftest = 500  # acoustic frequency
kac = 2 * np.pi * ftest / 1500

# specify source and receiver location
rsrc = np.array([0, 0., -20])
rrcr = np.array([200, 0., -10])

# wave number integration parameters
num_eva = 3
numa = 2 ** 16

# setup wave number integration
cos_ier = CosineSurface(hwave, kwave, attn=attn)
# a static q vector seems fine for low frequencies
qvec = np.unique(np.hstack([cos_ier.qvec(num_eva, np.pi / 2, ftest),
                            cos_ier.qvec(num_eva, 0., ftest)]))

eva_range = 0.1  # excursion into imaginary land
num_thetas = 500  # number of points in reflection coefficent interpolation
theta_axis = wn_synthesis.make_theta_axis(num_thetas, eva_range)

def rayleigh_rs(theta, qvec):
    """Compute rayleigh reflection coefficents"""
    # compute a q vector
    alphas = kac * np.cos(theta) + qvec * kwave
    betas = np.sqrt(kac ** 2 - alphas ** 2 + 0j)
    # setup rayleigh system of equations
    bs = -1j ** qvec * jv(qvec, hwave / 2 * kac * np.sin(theta))
    nm_diff = qvec[:, None] - qvec[None, :]
    A = 1j ** nm_diff * jv(nm_diff, -betas * hwave / 2)
    return solve(A, bs)

def hk_rs(theta, qvec):
    """Compute HK reflection coefficents"""
    a0 = kac * np.cos(theta)
    b0 = np.sqrt(kac ** 2 - a0 ** 2 + 0j)
    alphas = kac * np.cos(theta) + qvec * kwave
    betas = np.sqrt(kac ** 2 - alphas ** 2 + 0j)
    r_hk = (-1j) ** qvec * (a0 * (alphas - a0) - b0 * (b0 + betas))\
           * jv(qvec, hwave / 2 * (b0 + betas)) / (betas * (b0 + betas))
    return r_hk

# Wave number Rayliegh hypothesis
rs_RH = np.array([rayleigh_rs(ta, qvec) for ta in theta_axis])
rs_HK = np.array([hk_rs(ta, qvec) for ta in theta_axis])
qvec_sym, thetaaxis_sym, rs_RH_sym = wn_synthesis.make_symetric(qvec, theta_axis, rs_RH)
qvec_sym, thetaaxis_sym, rs_HK_sym = wn_synthesis.make_symetric(qvec, theta_axis, rs_HK)

# bragg angles
theta_cut = np.arccos(1 - qvec_sym * kwave / kac)

# plot low order reflection coefficents
ploti = np.bitwise_and(qvec_sym <= 2, qvec_sym >= 0)
real_theta = np.abs(np.cos(thetaaxis_sym)) <= 1

fig, ax = plt.subplots()

for i, r in enumerate(rs_RH_sym[:, ploti].T):
    ax.plot(np.degrees(np.real(thetaaxis_sym[real_theta])),
            np.abs(r[real_theta]), label='R%i'%i, linewidth=2.5)

for i, r in enumerate(rs_HK_sym[:, ploti].T):
    ax.plot(np.degrees(np.real(thetaaxis_sym[real_theta])),
            np.abs(r[real_theta]), linewidth=1)

for ba, q in zip(theta_cut[ploti], qvec_sym[ploti]):
    if q == 0: continue
    ax.annotate('', xy=(np.degrees(ba), 1.09), xytext=(np.degrees(ba), 1.15),
            clip_on=False,
            arrowprops=dict(arrowstyle="->"))
    #ax.plot([np.degrees(ba)]*2, [-10, 10], ':', linewidth=0.5, color='0.5')


ax.set_ylim(-0.01, 1.1)
ax.set_xlim(0, 180)
ax.set_xticks([45, 90, 135])
ax.grid(color='0.9')
#ax.set_title('RH and HK scattered plane wave magnitudes'+
             #'\n' + r'$f$: ' + '%i Hz, '%ftest + '$H$: ' +
             #'%.1f m,'%hwave + r' $\Lambda$: ' + '%.1f m'%lwave)
ax.set_ylabel(r'Coefficent magnitude, $|R_n|$')
ax.set_xlabel(r'Incident angle, $\theta_{inc}$ (deg)')
ax.legend()

plt.show(block=False)
1/0
plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure5.eps', format='eps')
