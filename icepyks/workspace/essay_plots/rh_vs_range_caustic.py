import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.porty import point_iso_field
from icepyks.poodle import wn_synthesis
from icepyks.f_synthesis import synthesize_ts
from scipy.signal import resample, hilbert
from concurrent.futures import ProcessPoolExecutor

# setup the HK
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
# RH coefficents save dir
save_dir = r'/enceladus0/e2richards/rayliegh_rs/'

#signal parameter
fc = 2.5e3
frange = (500, 4500)  # generous bounds
fs = 50e3

# wave phase
nump = 160
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
wp = np.arange(nump) / nump * 2 * np.pi  # wave phase
x_shift = lwave * (wp - np.pi / 2) / (2 * np.pi)
kcenter = 2 * np.pi * fc / 1500
fcompute = np.arange(frange[0], frange[1] + 1)

# specify source and receiver location
rsrc = np.array([0., 0, -20])
z_source = rsrc[-1]

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)
sig_neg = i_er_signal.make_ier(np.conj(1j) * i_er_signal.get_spectrum()[1])

# used to compute RH over a number of frequencies
def batch_run(frun, wn_ier):
    """Convert to reflection coefficents to pressure field"""

    qvec, theta_axis, rs = wn_synthesis.read_coeff(hwave, lwave,
                                                    frun, save_dir)
    p_ref = [wn_ier.kx_sta_ky_sta(q, qvec, rs, theta_axis, frun, num_dims=3)
             for q in qvec]
    return np.sum(p_ref)


def caustic_shift(rtest):
    # Specular image source information
    rrcr = np.array([rtest, 0., -10])
    ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
    t_spec = ri / 1500.
    direct_amp = 1 / ri

    # HK integrations
    # setup spatial integration parameters
    r_buffer = 50.
    # spatial integration
    x_bounds = (-r_buffer, rrcr[0] + r_buffer)
    dx = 3e-2
    xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

    # time integration
    t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
    num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
    taxis = np.arange(num_t) / fs + t_bounds[0]

    naive_series = []
    tworays = []
    # HK solution
    for p in wp:
        eta = sin_surface.Sine(lwave, hwave, phase=p)
        iso_field = point_iso_field.IsospeedPoint(eta, z_source)
        naive_HK = surface_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal)
        eiger = eigen_rays.EigenRays(naive_HK, rrcr)

        ts = np.zeros_like(taxis)
        for et, ea in zip(eiger.eig_tau[:2], eiger.eig_amp[:2]):
        #for et, ea in zip(eiger.eig_tau[-2:], eiger.eig_amp[-2:]):
            if np.abs(ea.imag) < 1e-9:
                ts += ea.real * i_er_signal(taxis - et.real)
            else:
                ts += ea.imag * sig_neg(taxis - et.real)
        tworays.append(np.max(ts))

        # pick out the two rays closest to receiver

        naive_series.append(naive_HK(rrcr))
    naive_series = np.array(naive_series)
    tworays = np.array(tworays)
    causti = np.unravel_index(np.argmax(np.abs(naive_series)),
                              naive_series.shape)[0]
    #causti = np.argmax(tworays)

    # compute RH result at caustic
    xs = x_shift[causti]

    rs_shift = rsrc.copy(); rs_shift[0] = rs_shift[0] + xs
    rr_shift = rrcr.copy(); rr_shift[0] = rr_shift[0] + xs
    one_pos = wn_synthesis.WaveNumberSynthesis(rs_shift, rr_shift, lwave)

    rh_wn_FT = [batch_run(f, one_pos) for f in fcompute]
    rh_wn_ts, t_wn_rh = synthesize_ts(rh_wn_FT, i_er_signal, fcompute)

    # upsample result
    rh_wn_ts, rh_taxis = resample(rh_wn_ts, t_wn_rh.size * 10, t=t_wn_rh)
    hk_ts, hk_taxis = resample(naive_series[causti, :],
                               naive_series.shape[1] * 10, t=taxis)

    return hk_taxis - t_spec, hk_ts * ri, rh_taxis - t_spec, rh_wn_ts * ri

rranges = np.array([r * 50 + 50 for r in range(20)])
with ProcessPoolExecutor(max_workers=6) as executor:
    ns = list(executor.map(caustic_shift, rranges))


fig, axes = plt.subplots(1, 2, sharey=True)

axes[0].fill([1.0, 2.0, 2.0, 1.0], [-300, -300, 600.5, 600.5], alpha=0.2, facecolor='0.7', edgecolor=(0,0,0))
axes[1].fill([1.0, 2.0, 2.0, 1.0], [-300, -300, 600.5, 600.5], alpha=0.2, facecolor='0.7', edgecolor=(0,0,0))

for rr, n in zip(rranges, ns):
    if rr == 50:
        off = -200
    elif rr == 100:
        off = -100
    elif rr == 150:
        off = -50
    else:
        off = 0
    axes[0].plot(n[2] * 1e3, n[3] * 40 + rr + off, color='k', linewidth=1)
    axes[1].plot(n[0] * 1e3, n[1] * 40 + rr + off, color='k', linewidth=1)
axes[0].set_xlim(-0.5, 5)
axes[1].set_xlim(-0.5, 5)
axes[0].set_yticks([-150, 0, 100, 200, 400, 600, 800, 1000])
axes[0].set_yticklabels([50, 100, 150, 200, 400, 600, 800, 1000])
#axes[0].set_title('Rayleigh Hypothesis')
#axes[1].set_title('Helmholtz Kirchhoff')
axes[0].set_ylabel('$\Delta x$ (m)')
fig.text(0.35, 0.03, 'Time re image arrival, t (ms)')
fig.text(0.14, 0.89, '(a) RFM')
fig.text(0.57, 0.89, '(b) HKA')
plt.show(block=False)

plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure9.pdf', format='pdf')

sols = []
for n in ns:
    sols.append(np.array([n[1], np.interp(n[0], n[2], n[3])]))
sols = np.array(sols)
hilb_sols = hilbert(sols, axis=-1)
sol_diff = np.squeeze(np.diff(np.abs(hilb_sols), axis=1))

img_diff = []
for hi, n in zip(hilb_sols, ns):
    img_sol = -i_er_signal(n[0])
    hilb_img = hilbert(img_sol, axis=-1)
    img_diff.append(np.abs(hi) - np.abs(hilb_img))

img_diff = np.array(img_diff)

dt = (ns[0][0][-1] - ns[0][0][0]) / (ns[0][0].size - 1)  # upsampled from fs

sol_diff = np.sum(np.abs(sol_diff), axis=-1) * dt
sol_img_diff = np.sum(np.abs(img_diff), axis=-1) * dt

fig, ax = plt.subplots()
ax.plot(rranges, sol_diff * 1e3, '-*', label='HKA-RFM', color='k', markevery=3, markersize=10)
ax.plot(rranges, sol_img_diff[:, 0] * 1e3, '-o', label='HKA-image', color='k', markevery=3, markersize=8)
ax.plot(rranges, sol_img_diff[:, 1] * 1e3, label='RFM-image', color='k')

ax.legend()
ax.grid(color='0.9')
ax.set_ylim(0, 1.5)
ax.set_xlabel('$\Delta x$ (m)')
ax.set_ylabel('Integrated absolute difference (s)')

plt.show(block=False)

plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure10.pdf', format='pdf')
