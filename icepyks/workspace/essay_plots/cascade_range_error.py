import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.porty import point_iso_field
from os.path import join
from scipy.signal import hilbert

# load rh cascade as a precomputed result
loaddir = 'cascade_npzs'

# setup the HK
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave

#signal parameter
fc = 2.5e3
frange = (500, 4500)  # generous bounds
fs = 50e3
kcenter = 2 * np.pi * fc / 1500

# specify source and receiver location
rsrc = np.array([0., 0, -20])
z_source = rsrc[-1]

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

def compute_HK(xrcr):
    """Compute the HKA result to match the RFM at a given range"""
    rh_cascade = np.load(join(loaddir, 'rh_cascade_%i.npz'%xrcr))
    # Specular image source information
    rrcr = np.array([xrcr, 0, -10])
    ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
    t_spec = ri / 1500.
    direct_amp = 1 / ri

    # HK integrations
    # setup spatial integration parameters
    r_buffer = 50.
    z_const = -(hwave / 2 + 0.1)
    z_bottom = -30
    # spatial integration
    x_bounds = (-r_buffer, rrcr[0] + r_buffer)
    dx = 3e-2
    xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

    # time integration
    twv_axis = rh_cascade['Y'][:, 0] / 1e3 + t_spec
    taxis = rh_cascade['X'][0, :]
    # remove any time wrap
    ti = taxis >= 0
    taxis = taxis[ti]
    wp = taxis * np.sqrt(2 * np.pi * 9.81 / lwave)

    hka_series = []
    # HK solution
    for p in wp:
        eta = sin_surface.Sine(lwave, hwave, phase=p)
        iso_field = point_iso_field.IsospeedPoint(eta, z_source)
        naive_HK = surface_integral.TimeHK(iso_field, xaxis,
                                           twv_axis, i_er_signal)
        hka_series.append(naive_HK(rrcr))

    image_arrival = -i_er_signal(twv_axis - t_spec)
    hilb_imag = hilbert(image_arrival)

    hka_series = np.array(hka_series) / direct_amp
    hilb_hka = hilbert(hka_series, axis=-1)
    rfm_series = rh_cascade['ploter_ds'][ti, :]
    hilb_rfm = hilbert(rfm_series, axis=-1)

    d_sca = np.abs(hilb_hka) - np.abs(hilb_rfm)
    dim_hka = np.abs(hilb_hka) - np.abs(hilb_imag)
    dim_rfm = np.abs(hilb_rfm) - np.abs(hilb_imag)
    return taxis, twv_axis, d_sca, dim_hka, dim_rfm

rtest = [r * 50 + 50 for r in range(20)]
rtest.remove(200)
rtest = np.array(rtest)

error_tot = []
for r in rtest:
    print('Computing result for range %i'%r)
    ta, twva, d_sca, dim_hka, dim_rfm = compute_HK(r)
    dta = (ta[-1] - ta[0]) / (ta.size - 1)
    dtwva = (twva[-1] - twva[0]) / (twva.size - 1)
    #integrate errors
    err = np.array([np.sum(np.abs(d_sca)),
                    np.sum(np.abs(dim_hka)),
                    np.sum(np.abs(dim_rfm))])
    err *= dta * dtwva
    error_tot.append(err)

error_tot = np.array(error_tot).T

#mv = np.max(np.abs(diff))
#X, Y = np.meshgrid(ta, twva)
#fig, ax = plt.subplots()
#cm = ax.pcolormesh(X, Y, diff.T, vmax=mv, vmin=-mv, cmap=plt.cm.RdBu_r)
#fig.colorbar(cm)

fig, ax = plt.subplots()
for et, lab in zip(error_tot, ['hka-rfm', 'hka-img', 'rfm-img']):
    ax.plot(rtest, et, label=lab)

ax.legend()
ax.grid()
ax.set_xlabel('Source-receiver $x$-distance (m)')
ax.set_ylabel('Integrated absolute difference (s)')
plt.show(block=False)
#plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure9.eps', format='eps')
