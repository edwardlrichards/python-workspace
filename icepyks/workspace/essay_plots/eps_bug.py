import matplotlib
import matplotlib.pyplot as plt

params= {'text.latex.preamble' : [r'\usepackage{amsmath}']}
plt.rcParams.update(params)

fig, ax = plt.subplots(figsize=(8, 6))
ax.text(0.5, 0.5, r'\begin{align*} H&: 2 \ \textrm{m} \\'\
                 + r'\Lambda &: 40 \ \textrm{m} \end{align*}',
         bbox=dict(boxstyle="square",
                   ec=(0.6, 0.6, 0.6),
                   fc=(1., 1., 1.),
                   ),
        fontsize=32)

plt.show(block=False)
#plt.savefig('test.png', format='png')
plt.savefig('test.eps', format='eps')
