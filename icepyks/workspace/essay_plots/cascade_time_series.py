import numpy as np
import matplotlib.pyplot as plt
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.clumber import iso_rg, ray_field

# setup the batch load parameters
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
#save_dir = r'/enceladus0/e2richards/rayliegh_rs/'
save_dir = r'/Users/edwardlrichards/rayleigh_rs/'

#signal parameter
fc = 2.5e3
fs = 50e3
toi = 0.633

# wave phase
nump = 160
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
wave_phase = np.arange(nump) / nump * 2 * np.pi  # time in seconds
t_wave = np.arange(nump) / nump * wave_period  # time in seconds

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
kcenter = 2 * np.pi * fc / 1500
direct_amp = 1 / ri

# HK integrations
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
taxis = np.arange(num_t) / fs + t_bounds[0]

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=0.)

# ray solution
ray_pf = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                               range_buffer=r_buffer)
ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
ray_pf.to_top(ray_gun)

ray_series = []
naive_series = []

for p in wave_phase:
    eta = sin_surface.Sine(lwave, hwave, phase=p)
    ray_pf.eta = eta
    ray_pf.to_wave()
    ray_HK = surface_integral.TimeHK(ray_pf, xaxis, taxis, i_er_signal)
    ray_series.append(ray_HK(rrcr))
    ray_pf('wave')
    naive_HK = surface_integral.TimeHK(ray_pf, xaxis, taxis, i_er_signal)
    naive_series.append(naive_HK(rrcr))

ray_series = np.array(ray_series)
naive_series = np.array(naive_series)

num_repeat = 20
expandt = np.hstack([-t_wave[num_repeat:0: -1], t_wave])

ploter = naive_series.T / direct_amp
ploter = np.concatenate([ploter[:, -num_repeat:], ploter], axis=-1)

ind_b = np.argmin(np.abs(toi - expandt))

X, Y = np.meshgrid(expandt, (taxis - t_spec) * 1e3)
fig, ax = plt.subplots()
cm = ax.pcolormesh(X, Y, ploter, vmin=-1.2, vmax=1.2,
              cmap=plt.cm.PuOr_r)
ax.set_xlabel('surface wave time, s')
ax.set_ylabel('time re image arrival, ms')
#ax.plot((t_wave[ind_a], t_wave[ind_a]), (-100, 100), 'k')
ax.plot((expandt[ind_b], expandt[ind_b]), (-100, 100), 'k')
#ax.plot((t_wave[ind_c], t_wave[ind_c]), (-100, 100), 'k')
#ax.plot((t_wave[ind_d], t_wave[ind_d]), (-100, 100), 'k')

ax.annotate('time series selection',
             xy=(t_wave[ind_b], 3),
             xycoords='data',
             xytext=(t_wave[ind_b] + 0.3, 3.5),
             textcoords='data',
             arrowprops=dict(arrowstyle="->"))

ax.set_ylim(0, 4)
ax.set_title('Amplitude of arrival over a full wave cycle')
# remove top time label
labs = ax.yaxis.get_ticklabels()
labs[-1] = ''
#ax.yaxis.set_ticklabels(labs)
cbar = fig.colorbar(cm, ticks=[-1.2, -0.6, 0, 0.6, 1.2])
cbar.set_label('pressure, re. image arrival')

plt.show(block=False)

#plt.savefig('/home/e2richards/personal_essays/figures/cascade_ts')
