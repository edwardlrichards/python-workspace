import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field
from icepyks.poodle.cosine_surface import CosineSurface
from scipy.io import loadmat
import matplotlib.gridspec as gridspec

num_dims = 3

# setup the batch load parameters
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km

#signal parameter
fc = 2.5e3
frange = (500, 4500)  # generous bounds
fs = 50e3

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
t_wave = 0.633  # time in seconds

wp = 2 * np.pi * t_wave / wave_period
x_shift = lwave * (wp - np.pi / 2) / (2 * np.pi)

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# HK integration setup
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wp)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
taxis = np.arange(num_t) / fs + t_bounds[0]

# compute scattered field using batch processing
# setup rayliegh wni
cos_ier = CosineSurface(hwave, kwave, attn=attn)
fcompute = np.arange(frange[0], frange[1] + 1)
rs_shift = rsrc.copy(); rs_shift[0] = rs_shift[0] + x_shift
rr_shift = rrcr.copy(); rr_shift[0] = rr_shift[0] + x_shift

y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# ray solution
ray_pf = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                            range_buffer=r_buffer)
ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
ray_pf.to_top(ray_gun)
ray_pf.to_wave()

ray_HK = surface_integral.TimeHK(ray_pf, xaxis, taxis,
                                i_er_signal, num_dims=3)
ray_result = ray_HK(rrcr)

# isospeed solution
iso_field = point_iso_field.IsospeedPoint(eta, z_source)
isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis,
                                    i_er_signal, num_dims=3)
iso_result = isospeed_HK(rrcr)

# load ym results
naive = loadmat('naive.mat')
nta = np.squeeze(np.array(naive['taxis']))
ny = np.squeeze(np.array(naive['yaxis']))

shadow = loadmat('shadowed.mat')
sta = np.squeeze(np.array(shadow['taxis']))
sy = np.squeeze(np.array(shadow['yaxis']))

fig, ax = plt.subplots()

ax.plot((taxis - t_spec) * 1e3, iso_result / direct_amp, label='nHK', color='C2')
ax.plot((taxis - t_spec) * 1e3, ray_result / direct_amp, label='sHK', color='k')
ax.plot(nta, ny, '--', label='ymHK', color='C2')
ax.plot(sta, sy, '--', color='k')
ax.set_xlim(-0.5, 5)
ax.set_ylim(-1.5, 1.5)

ax.set_title('t=%.2f s, '%t_wave
                  + r'$H$' + ' = %.1f m, '%hwave
                  + r'$\Lambda$' + ' = %.1f m'%lwave
                  + '\n center frequency = %.1f kHz'%(fc / 1e3))

ax.grid()
ax.set_xlabel('time, ms')
ax.set_ylabel('pressure, re. image arrival')
ax.legend(loc=1)

plt.show(block=False)

#plt.savefig('/home/e2richards/personal_essays/figures/time_series')
