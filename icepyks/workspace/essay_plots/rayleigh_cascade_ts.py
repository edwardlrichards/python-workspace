import numpy as np
import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.poodle.cosine_surface import CosineSurface
from icepyks.poodle import wn_synthesis
from icepyks.f_synthesis import synthesize_ts
from scipy.signal import resample

# setup the batch load parameters
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
save_dir = r'/enceladus0/e2richards/rayliegh_rs/'
#save_dir = r'/Users/edwardlrichards/rayleigh_rs/'

#signal parameter
fc = 2.5e3
frange = (500, 4500)  # generous bounds
fs = 50e3
toi = 0.633

# wave phase
nump = 160
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
wp = np.arange(nump) / nump * 2 * np.pi  # time in seconds
t_wave = np.arange(nump) / nump * wave_period  # time in seconds
x_shift = lwave * (wp - np.pi / 2) / (2 * np.pi)

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
kcenter = 2 * np.pi * fc / 1500
direct_amp = 1 / ri

# compute scattered field using batch processing
# setup rayliegh wni
cos_ier = CosineSurface(hwave, kwave, attn=attn)
fcompute = np.arange(frange[0], frange[1] + 1)

def batch_run(frun, wn_ier):
    """Convert to reflection coefficents to pressure field"""

    qvec, theta_axis, rs = wn_synthesis.read_coeff(hwave, lwave,
                                                    frun, save_dir)
    p_ref = [wn_ier.kx_sta_ky_sta(q, qvec, rs, theta_axis, frun, num_dims=3)
             for q in qvec]
    return np.sum(p_ref)

rh_series = []
for i, xs in enumerate(x_shift):
    rs_shift = rsrc.copy(); rs_shift[0] = rs_shift[0] + xs
    rr_shift = rrcr.copy(); rr_shift[0] = rr_shift[0] + xs
    one_pos = wn_synthesis.WaveNumberSynthesis(rs_shift, rr_shift, lwave)

    rh_wn_FT = [batch_run(f, one_pos) for f in fcompute]
    rh_wn_ts, t_wn_rh = synthesize_ts(rh_wn_FT, i_er_signal, fcompute)
    rh_wn_ts, taxis = resample(rh_wn_ts, t_wn_rh.size * 10, t=t_wn_rh)
    rh_series.append(rh_wn_ts)
    print('run number %i'%i)

rh_series = np.array(rh_series)
t_rh = (taxis - t_spec) * 1e3

toi1 = 2.
toi2 = 4.

num_repeat = 20
expandt = np.hstack([-t_wave[num_repeat:0: -1], t_wave])

numploty = 300
plotymax = 4
tplot = np.arange(numploty) / numploty * plotymax

# interpolate ploter
ploter_ds = []
for i, rh in enumerate(rh_series):
    ploter_ds.append(np.interp(tplot, t_rh, rh))

ploter_ds = np.array(ploter_ds)
ploter_ds = ploter_ds / direct_amp
ploter_ds = np.concatenate([ploter_ds[-num_repeat:, :], ploter_ds], axis=0)

plot1 = True  # plot 1 line or 3
ind_b = np.argmin(np.abs(toi - expandt))
ind1 = np.argmin(np.abs(toi1 - expandt))
ind2 = np.argmin(np.abs(toi2 - expandt))

X, Y = np.meshgrid(expandt, tplot)
fig, ax = plt.subplots()
cm = ax.pcolormesh(X, Y, ploter_ds.T, vmin=-1.2, vmax=1.2,
              cmap=plt.cm.PuOr_r)
ax.set_xlabel('Surface wave time (s)')
ax.set_ylabel('Pulse time (ms)')

ax.plot((expandt[ind_b], expandt[ind_b]), (-100, 100), 'k')
# label switch
if plot1:
    ax.annotate('time series selection',
                xy=(expandt[ind_b], 3),
                xycoords='data',
                xytext=(expandt[ind_b] + 0.3, 3.5),
                textcoords='data',
                arrowprops=dict(arrowstyle="->"))
else:
    ax.plot((expandt[ind1], expandt[ind1]), (-100, 100), 'k')
    ax.plot((expandt[ind2], expandt[ind2]), (-100, 100), 'k')
    ax.annotate('A',
                xy=(expandt[ind_b], 3),
                xycoords='data',
                xytext=(expandt[ind_b] + 0.3, 3.5),
                textcoords='data',
                arrowprops=dict(arrowstyle="->"))
    ax.annotate('B',
                xy=(expandt[ind1], 3),
                xycoords='data',
                xytext=(expandt[ind1] + 0.3, 3.5),
                textcoords='data',
                arrowprops=dict(arrowstyle="->"))
    ax.annotate('C',
                xy=(expandt[ind2], 3),
                xycoords='data',
                xytext=(expandt[ind2] + 0.3, 3.5),
                textcoords='data',
                arrowprops=dict(arrowstyle="->"))

ax.set_ylim(0, plotymax)
#ax.set_title('Amplitude of arrival over a full wave cycle')
# remove top time label
labs = ax.yaxis.get_ticklabels()
labs[-1] = ''
#ax.yaxis.set_ticklabels(labs)
cbar = fig.colorbar(cm, ticks=[-1.2, -0.6, 0, 0.6, 1.2])
cbar.set_label('Pressure, re. image arrival')
cbar.ax.yaxis.set_label_coords(3, .55)

plt.show(block=False)

plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure6.eps', format='eps')
#plt.savefig('/home/e2richards/personal_essays/figures/rayleigh_cascade_ts')
#plt.savefig('/home/e2richards/personal_essays/figures/rayleigh_cascade_ts.eps',
            #format='eps')
