import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from concurrent.futures import ProcessPoolExecutor
from icepyks.porty import point_iso_field
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from scipy.signal import resample, hilbert

H = 2
L = 40
K = 2 * np.pi / L

c = 1500
fc = 2500
fs = 50e3

z_source = -20
z_rcr = -10
rsrc = np.array([0., 0, z_source])

# surface wave phase
nump = 160
wave_period = np.sqrt(L * 2 * np.pi / 9.81)
wp = np.arange(nump) / nump * 2 * np.pi  # wave phase

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

def caustic_shift(rtest):
    """
    Loop through all surface wave phases with HKA to find the largest arrival
    """
    # Specular image source information
    rrcr = np.array([rtest, 0., z_rcr])
    ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
    t_spec = ri / 1500.
    direct_amp = 1 / ri

    # HK integrations
    # setup spatial integration parameters
    r_buffer = 50.
    # spatial integration
    x_bounds = (-r_buffer, rrcr[0] + r_buffer)
    dx = 3e-2
    xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

    # time integration
    t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3,
                np.ceil(t_spec * 1e3 + 8) / 1e3)
    num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
    taxis = np.arange(num_t) / fs + t_bounds[0]

    naive_series = []
    taus = []
    # HK solution
    for p in wp:
        eta = sin_surface.Sine(L, H, phase=p)
        iso_field = point_iso_field.IsospeedPoint(eta, z_source)
        naive_HK = surface_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal)
        naive_series.append(naive_HK(rrcr))

    naive_series = np.array(naive_series)
    causti = np.unravel_index(np.argmax(np.abs(naive_series)),
                              naive_series.shape)[0]

    hk_ts, hk_taxis = resample(naive_series[causti, :],
                               naive_series.shape[1] * 10, t=taxis)

    # pull out tt curve for caustic feature
    eta = sin_surface.Sine(L, H, phase=wp[causti])
    iso_field = point_iso_field.IsospeedPoint(eta, z_source)
    naive_HK = surface_integral.TimeHK(iso_field, xaxis, hk_taxis, i_er_signal)
    tau_r = naive_HK.rcr_greens(rrcr)[1]
    tau_s = naive_HK.psi_inc()[1]
    taus = tau_r + tau_s

    # pull out eigen-rays
    erays = eigen_rays.EigenRays(naive_HK, rrcr)
    ets = erays.make_timeseries()

    return xaxis, taus - t_spec, hk_taxis, hk_ts, ets, erays.eig_tau - t_spec, erays.eig_amp

rspace = 50
rranges = np.array([r * rspace + rspace for r in range(12)])
ns = list(map(caustic_shift, rranges))
with ProcessPoolExecutor(max_workers=6) as executor:
    ns = list(executor.map(caustic_shift, rranges))

wfshift = 1
numsup = 4

fig = plt.figure()
spec2 = gridspec.GridSpec(ncols=1, nrows=20)
axes = []
axes += [fig.add_subplot(spec2[:numsup, 0])]
axes += [fig.add_subplot(spec2[numsup + 1:, 0])]
axes[0].spines['top'].set_visible(False)
axes[0].spines['right'].set_visible(False)
axes[0].spines['bottom'].set_visible(False)
axes[0].spines['left'].set_visible(False)
axes[0].set_xticklabels([])

axes[1].fill([0.3, 0.8, 0.8, 0.3], [0, 0, 10.5, 10.5], alpha=0.2, facecolor='0.7', edgecolor=(0,0,0))

for (i, n), xs in zip(enumerate(ns), rranges):
    for t, a in zip(n[5], n[6]):
        ri = np.sqrt(xs ** 2 + 30 ** 2)
        a *= ri
        if np.abs(np.imag(a)) > 1e-6:
            axes[1].plot(t * 1e3, np.imag(a) + i * wfshift, 'D', markersize=6,
                    color='k')
            axes[1].plot(np.array([t, t]) * 1e3,
                    np.imag(np.array([0, a])) + i * wfshift, color='k')
        else:
            axes[1].plot(t * 1e3, np.abs(a) + i * wfshift, 'ko', markersize=6)
            axes[1].plot(np.array([t, t]) * 1e3,
                    np.abs(np.array([0, a])) + i * wfshift, 'k')


rindex = 3
for t, a in zip(ns[rindex][5], ns[rindex][6]):
    xs = rranges[rindex]
    ri = np.sqrt(xs ** 2 + 30 ** 2)
    a *= ri
    if np.abs(np.imag(a)) > 1e-6:
        axes[0].plot(t * 1e3, np.imag(a), 'D', color='k', markersize=6)
        axes[0].plot(np.array([t, t]) * 1e3,
                np.imag(np.array([0, a])), color='k')
    else:
        axes[0].plot(t * 1e3, np.abs(a), 'ko', markersize=6)
        axes[0].plot(np.array([t, t]) * 1e3,
                np.abs(np.array([0, a])), 'k')


axes[0].text(1, 0.5, r'$\Delta x = 200 \ m$', fontsize=22)
axes[0].text(-0.47, 0.5, r'(a)', fontsize=22)
axes[1].text(-0.47, 11.25, r'(b)', fontsize=22)


axes[1].set_yticks(np.arange(rranges.size) * wfshift)
lbs = np.vstack([np.zeros_like(rranges[1::2]), rranges[1::2]])
lbs = lbs.T.flatten()
lbs = list(lbs)
lbs = [str(l) if l != 0 else '' for l in lbs]
axes[1].set_yticklabels(lbs)

axes[0].grid(color='0.8')
axes[1].grid(color='0.8')
axes[0].set_xlim(-0.5, 1.5)
axes[0].set_ylim(-1, 1.2)
axes[0].set_xticks([0, 1])
axes[0].set_yticks([0, 1])
axes[0].set_yticklabels(['0', '$|p_{img}|$'])
axes[1].set_xlim(-0.5, 1.5)
axes[1].set_xticks([0, 1])
axes[1].set_ylabel('$\Delta x$ (m)')
axes[1].set_xlabel('Time re image arrival, $t$ (ms)')

plt.show(block=False)
1/0
plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure8.pdf', format='pdf')
