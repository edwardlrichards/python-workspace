%Hanning-Weighted four periods sine fuction
function src_sig=sine_four_hanning(fc_src,Fs);

%fc_src=20000;%Hz
%Fs=fc_src*100;

tpulse=[0:1/Fs:1000/fc_src]; %time vector
BW=fc_src; %bandwidth
N=length(tpulse);
A=fc_src/BW;
itp=find(tpulse>4*A/fc_src,1)-1;
s=zeros(1,itp);
src_sig=1/2*sin(pi+2*pi*fc_src*tpulse(1:itp)).*(1-cos(pi/2/A*fc_src*tpulse(1:itp)));

%tpulse=[0:itp-1]/Fs;
%figure;
%plot(tpulse,s);
