import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.f_synthesis import synthesize_ts
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field
from icepyks.poodle.plane_wave_rs import CosineReflectionCoefficents
from icepyks.poodle import wn_synthesis
from concurrent.futures import ThreadPoolExecutor
import matplotlib.gridspec as gridspec

num_dims = 3

# setup the batch load parameters
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
save_dir = r'/enceladus0/e2richards/rayliegh_rs/'
#save_dir = r'/Users/edwardlrichards/rayleigh_rs/'

#signal parameter
center_f = [1.5e3, 2.5e3, 3.5e3]
frange = (500, 6500)  # generous bounds
fs = 50e3

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)
#t_wave = 0.633  # time in seconds
t_wave = 0.7  # time in seconds

wp = 2 * np.pi * t_wave / wave_period
x_shift = lwave * (wp - np.pi / 2) / (2 * np.pi)

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# wave number integration parameters
num_eva = 3
numa = 2 ** 16
eva_range = 0.1  # excursion into imaginary land
num_thetas = 200  # number of points in reflection coefficent interpolation
theta_axis = wn_synthesis.make_theta_axis(num_thetas, eva_range)

# HK integration setup
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=wp)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
taxis = np.arange(num_t) / fs + t_bounds[0]

# compute scattered field using batch processing
# setup rayliegh wni
cos_ier = CosineReflectionCoefficents(hwave, kwave, attn=attn)
fcompute = np.arange(frange[0], frange[1] + 1)
rs_shift = rsrc.copy(); rs_shift[0] = rs_shift[0] + x_shift
rr_shift = rrcr.copy(); rr_shift[0] = rr_shift[0] + x_shift
wn_ier = wn_synthesis.WaveNumberSynthesis(rs_shift, rr_shift, lwave)

def batch_run(frun, theta_axis, theory='HK', is_stationary=True):
    """Run through the rayleigh coefficents at each frequency"""
    # a static q vector seems fine for low frequencies
    qvec = np.unique(np.hstack([cos_ier.qvec(num_eva, np.pi / 2, frun),
                            cos_ier.qvec(num_eva, 0., frun)]))
    if theory == 'HK':
        rs = [cos_ier.r_hk(np.real(np.cos(ta)), qvec, frun, return_bs=False)
                    for ta in theta_axis]
        qvec, theta_axis, rs = wn_synthesis.make_symetric(qvec, theta_axis, rs)
    elif theory == 'RH':
        qvec, theta_axis, rs = wn_synthesis.read_coeff(hwave, lwave,
                                                       frun, save_dir)
    else:
        raise(ValueError('Theory has to be either HK or RH'))
    if is_stationary:
        p_ref = [wn_ier.kx_sta_ky_sta(q, qvec, rs, theta_axis, frun,
                                      num_dims=num_dims)
                 for q in qvec]
    else:
        p_ref = [wn_ier.theta_int(q, qvec, rs, theta_axis, frun, 20000,
                                  num_dims=num_dims)
                 for q in qvec]
    if (frun % 200) == 0:
        print('run number %i'%frun)
    return np.sum(p_ref)

rh_wn_FT = [batch_run(f, theta_axis, theory='RH') for f in fcompute]

def scatter_atfc(fc):
    """create scattering result once center frequency is specified"""
    # xmitt signal information
    y_signal, t_signal = pulse_signal.pulse_q1(fc)
    i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

    # ray solution
    ray_pf = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                                range_buffer=r_buffer)
    ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
    ray_pf.to_top(ray_gun)
    ray_pf.to_wave()

    ray_HK = surface_integral.TimeHK(ray_pf, xaxis, taxis,
                                    i_er_signal, num_dims=3)
    ray_result = ray_HK(rrcr)

    # isospeed solution
    iso_field = point_iso_field.IsospeedPoint(eta, z_source)
    isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis,
                                        i_er_signal, num_dims=3)
    iso_result = isospeed_HK(rrcr)
    eiger = eigen_rays.EigenRays(isospeed_HK, rrcr)
    eig_ts = eiger.make_timeseries()

    rh_wn_ts, t_wn_rh = synthesize_ts(rh_wn_FT, i_er_signal, fcompute)
    # upsample timeseries before plotting
    rh_wn_ts, t_rh_wn = resample(rh_wn_ts, t_wn_rh.size * 5, t=t_wn_rh)
    rh_ts = np.interp(taxis, t_rh_wn, rh_wn_ts, left=0., right=0.)
    return taxis, iso_result, ray_result, rh_ts, eig_ts

# compute results for each center frequency
t1, i1, r1, rh1, e1 = scatter_atfc(center_f[0])
t2, i2, r2, rh2, e2 = scatter_atfc(center_f[1])
t3, i3, r3, rh3, e3 = scatter_atfc(center_f[2])

fig, axes = plt.subplots(nrows=3)
fig.subplots_adjust(left=0.1, bottom=0.1, right=0.98, top=0.95)  # space for 'shared' labels

axes[2].plot((t1 - t_spec) * 1e3, rh1 / direct_amp, label='RFM', linewidth=3, dashes=[8,2])
axes[2].plot((t1 - t_spec) * 1e3, e1 / direct_amp, label='ERA', linewidth=1, color='0.2')
axes[2].plot((t1 - t_spec) * 1e3, i1 / direct_amp, label='HKA', linewidth=3, color='tomato')
axes[2].text(2.6, -1.08, '$f_c = 1.5 \mathrm{~kHz}$')
#axes[2].plot((taxis - t_spec) * 1e3, r1 / direct_amp, label='sHK')

axes[1].plot((t2 - t_spec) * 1e3, rh2 / direct_amp, label='RFM', linewidth=3, dashes=[8,2])
axes[1].plot((t2 - t_spec) * 1e3, e2 / direct_amp, label='ERA', linewidth=1, color='0.2')
axes[1].plot((t2 - t_spec) * 1e3, i2 / direct_amp, label='HKA', linewidth=3, color='tomato')
axes[1].text(2.6, -1.08, '$f_c = 2.5 \mathrm{~kHz}$')
#axes[1].plot((t2 - t_spec) * 1e3, r2 / direct_amp, label='sHK')

axes[0].plot((t3 - t_spec) * 1e3, rh3 / direct_amp, label='RFM', linewidth=3, dashes=[8,2])
axes[0].plot((t3 - t_spec) * 1e3, e3 / direct_amp, label='ERA', linewidth=1, color='0.2')
axes[0].plot((t3 - t_spec) * 1e3, i3 / direct_amp, label='HKA', linewidth=3, color='tomato')
axes[0].text(2.6, -1.08, '$f_c = 3.5 \mathrm{~kHz}$')
#axes[0].plot((t3 - t_spec) * 1e3, r3 / direct_amp, label='sHK')

axes[0].set_xlim(-0.5, 5)
axes[1].set_xlim(-0.5, 5)
axes[2].set_xlim(-0.5, 5)
axes[0].set_xticklabels('')
axes[1].set_xticklabels('')
axes[0].set_ylim(-1.7, 1.7)
axes[1].set_ylim(-1.7, 1.7)
axes[2].set_ylim(-1.7, 1.7)

axes[0].set_yticks([-1, 1, 1.7])

fig.text(0.01, 0.5, 'Pressure re image arrival',
         ha="left", va="center", rotation="vertical", size=28)

axes[0].set_title('$t_{wv}$=%.2f s, '%t_wave
                  + r'$H$' + ' = %.1f m, '%hwave
                  + r'$\Lambda$' + ' = %.1f m'%lwave, size=28)

#axes[0].set_title('t=%.2f s, '%t_wave
                  #+ r'$H$' + ' = %.1f m, '%hwave
                  #+ r'$\Lambda$' + ' = %.1f m'%lwave
                  #+ '\n center frequency = %.1f kHz'%(center_f[2] / 1e3))
#axes[1].set_title('center frequency = %.1f kHz'%(center_f[1] / 1e3))
#axes[2].set_title('center frequency = %.1f kHz'%(center_f[0] / 1e3))

fig.text(0.14, 0.96, '(a)')
fig.text(0.14, 0.66, '(b)')
fig.text(0.14, 0.36, '(c)')

axes[0].grid(color='0.9')
axes[1].grid(color='0.9')
axes[2].grid(color='0.9')
axes[2].set_xlabel('Time re image arrival, $t$ (ms)', size=28)
axes[0].legend(loc=1)

plt.show(block=False)
1/0
plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure7.pdf', format='pdf')
