import numpy as np
from icepyks import pulse_signal
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import matplotlib.gridspec as gridspec

x_mit, taxis = pulse_signal.pulse_q1(1500)

fs = 1 / (taxis[1] - taxis[0])
NFFT = 2 ** int(np.ceil(np.log2(taxis.size)) + 3)
faxis = np.arange(NFFT // 2 + 1) / NFFT * fs
#sig_FT = np.fft.rfft(x_mit, NFFT) / NFFT
sig_FT = np.fft.rfft(x_mit, NFFT) / fs

sig_db = 20 * np.log10(np.abs(sig_FT))

x_mit_p = [[taxis, x_mit]]
sig_p = [[faxis, sig_db]]

x_mit, taxis = pulse_signal.pulse_q1(2500)

fs = 1 / (taxis[1] - taxis[0])
NFFT = 2 ** int(np.ceil(np.log2(taxis.size)) + 3)
faxis = np.arange(NFFT // 2 + 1) / NFFT * fs
#sig_FT = np.fft.rfft(x_mit, NFFT) / NFFT
sig_FT = np.fft.rfft(x_mit, NFFT) / fs
sig_db = 20 * np.log10(np.abs(sig_FT))

x_mit_p.append([taxis, x_mit])
sig_p.append([faxis, sig_db])

x_mit, taxis = pulse_signal.pulse_q1(3500)

fs = 1 / (taxis[1] - taxis[0])
NFFT = 2 ** int(np.ceil(np.log2(taxis.size)) + 3)
faxis = np.arange(NFFT // 2 + 1) / NFFT * fs
#sig_FT = np.fft.rfft(x_mit, NFFT) / NFFT
sig_FT = np.fft.rfft(x_mit, NFFT) / fs
sig_db = 20 * np.log10(np.abs(sig_FT))

x_mit_p.append([taxis, x_mit])
sig_p.append([faxis, sig_db])

# half cycle lengths
hf1 = 1 / (2 * 1.5)
hf2 = 1 / (2 * 2.5)
hf3 = 1 / (2 * 3.5)

gs = gridspec.GridSpec(2,2)
gs.update(left=0.11, hspace=0.3)
fig = plt.figure()
axes = [plt.subplot(gs[0, :]), plt.subplot(gs[1, :])]
# time domain
axes[0].plot(x_mit_p[0][0] * 1e3, x_mit_p[0][1], color='k')
axes[0].plot((x_mit_p[0][0][-1] / 2) * 1e3, 0, 'D', color='k')
# left half width line
lline = 2.5 * hf1
# half cycle width
axes[0].plot([lline, lline], np.array([.75, .95]), color='k')
axes[0].plot([lline + hf1, lline + hf1], np.array([.75, .95]), color='k')
axes[0].annotate("",
             xy=(lline, .85), xycoords='data',
             xytext=(lline + hf1, .85), textcoords='data',
             arrowprops={'arrowstyle': '<->', 'color':'k'})

axes[0].plot(lline + hf1 / 2, .85, 'D', color='k')

axes[0].plot([lline, lline], np.array([1, 1.2]), color='k')
axes[0].plot([lline + hf2, lline + hf2], np.array([1., 1.2]), color='k')
axes[0].annotate("",
             xy=(lline, 1.1), xycoords='data',
             xytext=(lline + hf2, 1.1), textcoords='data',
             arrowprops={'arrowstyle': '<->', 'color':'k'})

axes[0].plot(lline + hf2 / 2, 1.1, 'o', color='k')

axes[0].plot([lline, lline], np.array([1.25, 1.45]), color='k')
axes[0].plot([lline + hf3, lline + hf3], np.array([1.25, 1.45]), color='k')
axes[0].annotate("",
             xy=(lline, 1.35), xycoords='data',
             xytext=(lline + hf3, 1.35), textcoords='data',
             arrowprops={'arrowstyle': '<->', 'color':'k'})

axes[0].text(lline - 0.4, .7, r'$f_c^{-1}/2$', fontsize=32)
# signal width
hc1 = 0.5 / 1.5
h3c1 = 3 / 1.5
hc2 = 0.5 / 2.5
h3c2 = 3 / 2.5
hc3 = 0.5 / 3.5
h3c3 = 3 / 3.5
axes[0].plot(np.array([0, 0]) + hc1,
             np.array([-.75, -.95]) - .3, color='k', label='1.5 kHz')
axes[0].plot(np.array([h3c1, h3c1]) + hc1, 
             np.array([-.75, -.95]) - .3, color='k')
axes[0].annotate("",
             xy=(hc1, -1.15), xycoords='data',
             xytext=(hc1 + h3c1, -1.15), textcoords='data',
             arrowprops={'arrowstyle': '<->', 'color':'k'})

axes[0].plot(hc1 + h3c1 / 2, -1.15, 'D', color='k')

axes[0].plot(np.array([0, 0]) + hc2,
             np.array([-1, -1.2]) - .35, color='k', label='2.5 kHz')
axes[0].plot(np.array([h3c2, h3c2]) + hc2,
             np.array([-1., -1.2]) - .35, color='k')
axes[0].annotate("",
             xy=(hc2, -1.45), xycoords='data',
             xytext=(hc2 + h3c2, -1.45), textcoords='data',
             arrowprops={'arrowstyle': '<->', 'color':'k'})

axes[0].plot(hc2 + h3c2 / 2, -1.45, 'o', color='k')

axes[0].plot(np.array([0, 0]) + hc3,
             np.array([-1.25, -1.45]) - .4, color='k', label='3.5 kHz')
axes[0].plot(np.array([h3c3, h3c3]) + hc3,
             np.array([-1.25, -1.45]) - .4, color='k')
axes[0].annotate("",
             xy=(hc3, -1.75), xycoords='data',
             xytext=(hc3 + h3c3, -1.75), textcoords='data',
             arrowprops={'arrowstyle': '<->', 'color':'k'})
axes[0].text(hc3 - 0.25, -1.65, r'$3 f_c^{-1}$', fontsize=32)

# frequency domain
marki = np.argmin(np.abs(sig_p[0][0] / 1e3 - 1.5))
axes[1].plot(sig_p[0][0] / 1e3, sig_p[0][1],'-D',  color='k', label='1.5 kHz',
             markevery=[marki])
marki = np.argmin(np.abs(sig_p[1][0] / 1e3 - 2.5))
axes[1].plot(sig_p[1][0] / 1e3, sig_p[1][1], '-o', color='k', label='2.5 kHz',
             markevery=[marki])
axes[1].plot(sig_p[2][0] / 1e3, sig_p[2][1], 'k', label='3.5 kHz')
axes[1].legend(loc=1)

# differentiate curves with marker

axes[0].set_xlabel('Time re 1m, $t$ (ms)')
axes[0].set_ylabel('Pressure re 1m')
axes[1].set_xlabel('Frequency, $f$ (kHz)')
axes[1].set_ylabel('Pressure re 1m per Hz (dB)')
axes[1].set_ylim(-95, -60)
axes[1].set_xlim(.5, 6)
#axes[0].set_xlim(-.2, 3.5)
axes[0].grid(color='0.9')
axes[0].set_ylim(-2, 1.5)
axes[1].grid(color='0.9')
fig.text(0.1, 0.89, '(a)')
fig.text(0.1, 0.4535, '(b)')

#axes[0].set_title('Transmitted signals')

#plt.savefig('/home/e2richards/personal_essays/figures/xmission.eps', format='eps')
#plt.savefig('/home/e2richards/personal_essays/figures/xmission')
plt.show(block=False)

plt.savefig('/home/e2richards/personal_essays/rh_timeseries/Figure2.pdf', format='pdf')
