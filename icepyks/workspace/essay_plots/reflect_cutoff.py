import numpy as np
import matplotlib.pyplot as plt
from scipy.special import jv
from scipy.linalg import solve

# setup the wave specific solver
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
ftest = 500  # acoustic frequency
kac = 2 * np.pi * ftest / 1500

# specify source and receiver location
rsrc = np.array([0, 0., -20])
rrcr = np.array([200, 0., -10])

# wave number integration parameters
num_eva = 3
numa = 2 ** 16

# bragg angles
theta_cut = np.arccos(1 - np.array([1, 2, 3]) * kwave / kac) + 1e-7

def rayleigh_rs(theta, qvec):
    """Compute rayleigh reflection coefficents"""
    # compute a q vector
    alphas = kac * np.cos(theta) + qvec * kwave
    betas = np.sqrt(kac ** 2 - alphas ** 2 + 0j)
    # setup rayleigh system of equations
    bs = -1j ** qvec * jv(qvec, hwave / 2 * kac * np.sin(theta))
    nm_diff = qvec[:, None] - qvec[None, :]
    A = 1j ** nm_diff * jv(nm_diff, -betas * hwave / 2)
    return solve(A, bs)

# setup wave number integration
#ttest = theta_cut[0] - .1
#ttest = theta_cut[0] + .1
#ttest = theta_cut[0] + .3
ttest = 0.01
a = np.real(np.cos(ttest))
q1 = np.arange(np.ceil(-ftest * lwave / 1500 * (1 + a)) - num_eva,
                    np.floor(ftest * lwave / 1500 * (1 - a)) + num_eva)

r1 = rayleigh_rs(ttest, q1)
q2 = np.arange(np.ceil(-ftest * lwave / 1500 * (1 + a)) - 2 * num_eva,
                    np.floor(ftest * lwave / 1500 * (1 - a)) + 2 * num_eva)

r2 = rayleigh_rs(ttest, q2)
q3 = np.arange(np.ceil(-ftest * lwave / 1500 * (1 + a)) - 3 * num_eva,
                    np.floor(ftest * lwave / 1500 * (1 - a)) + 3 * num_eva)

r3 = rayleigh_rs(ttest, q3)


fig, ax = plt.subplots()
ax.semilogy(q3, np.abs(r3), '.')
ax.semilogy(q2, np.abs(r2), '.')
ax.semilogy(q1, np.abs(r1), '.')

ax.set_xlim(0, 10)
ax.set_ylim(1e-4, 1)

plt.show(block=False)
