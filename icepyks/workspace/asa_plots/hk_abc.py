import numpy as np
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib.pyplot as plt
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.f_synthesis import synthesize_ts
from icepyks.surfaces import sin_surface
from icepyks.porty import point_iso_field
from icepyks.poodle import wn_synthesis
from concurrent.futures import ThreadPoolExecutor
import matplotlib.gridspec as gridspec

num_dims = 3

# setup the batch load parameters
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km

#signal parameter
fc = 2.5e3
fs = 50e3

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)

toi0 = 0.633  # time in seconds

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# HK integration setup
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
taxis = np.arange(num_t) / fs + t_bounds[0]

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

def hk_ts(t_wave, title):
    """plot a time series for each wave position"""
    wp = 2 * np.pi * t_wave / wave_period
    # setup surface
    eta = sin_surface.Sine(lwave, hwave, phase=wp)

    # isospeed solution
    iso_field = point_iso_field.IsospeedPoint(eta, z_source)
    isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis,
                                        i_er_signal, num_dims=3)

    eig_er = eigen_rays.EigenRays(isospeed_HK, rrcr)
    eig_x = eig_er.eig_x
    eig_ts = [ea * i_er_signal(taxis - et) for (ea, et)
                in zip(eig_er.eig_amp, eig_er.eig_tau)]

    et_sort = np.argsort(eig_er.eig_tau)
    # set up eigen ray color map
    cmv = np.arange(len(eig_x))
    cNorm = colors.Normalize(vmin=0, vmax=10)
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=plt.get_cmap('copper_r'))

    fig, ax = plt.subplots(figsize=(8, 6))

    ax.plot(xaxis, eta.z(xaxis))
    ax.plot(rsrc[0], rsrc[-1], '.', markersize=12)
    ax.plot(rrcr[0], rrcr[-1], '.', markersize=12)
    for i, eti in enumerate(et_sort):
        cval = scalarMap.to_rgba(cmv[i])
        ax.plot([0, eig_x[eti]], [z_source, eta.z(eig_x[eti])], color=cval)
        ax.plot([eig_x[eti], rrcr[0]], [eta.z(eig_x[eti]), rrcr[-1]], color=cval)

    plt.text(-40, -5, title, fontsize=32)

    ax.set_yticks([0, -10, -20])
    ax.set_xticks([0, 200])

    return isospeed_HK(rrcr)

i0 = hk_ts(toi0, 'A')
i1 = hk_ts(toi1, 'B')
i2 = hk_ts(toi2, 'C')

gs = gridspec.GridSpec(3,3)
gs.update(left=0.13, hspace=0.20)
fig = plt.figure()
axes = [plt.subplot(gs[0, :]), plt.subplot(gs[1, :]), plt.subplot(gs[2, :])]

fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

axes[0].plot((taxis - t_spec) * 1e3, i0 / direct_amp, label='HK')
axes[1].plot((taxis - t_spec) * 1e3, i1 / direct_amp)
axes[2].plot((taxis - t_spec) * 1e3, i2 / direct_amp)

axes[0].set_xlim(-0.5, 5)
axes[1].set_xlim(-0.5, 5)
axes[2].set_xlim(-0.5, 5)
axes[0].set_ylim(-1.5, 1.5)
axes[0].set_xticklabels('')
axes[1].set_xticklabels('')
axes[1].set_ylim(-1.5, 1.5)
axes[2].set_ylim(-1.5, 1.5)
#axes[0].set_ylabel('pressure, re. image arrival')

plt.ylabel('pressure, re. image arrival')

axes[0].set_title('t=%.2f s, '%toi0
                  + r'$H$' + ' = %.1f m, '%hwave
                  + r'$\Lambda$' + ' = %.1f m'%lwave
                  + '\n center frequency = %.1f kHz'%(fc / 1e3))
axes[1].set_title('t=%.2f s '%toi1)
axes[2].set_title('t=%.2f s '%toi2)

axes[0].grid()
axes[1].grid()
axes[2].grid()
axes[2].set_xlabel('time, ms')
axes[0].legend(loc=1)

plt.show(block=False)

#plt.savefig('/home/e2richards/personal_essays/figures/hk_abc')
