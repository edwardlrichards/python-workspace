import numpy as np
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib.pyplot as plt
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp, surface_integral, eigen_rays
from icepyks.f_synthesis import synthesize_ts
from icepyks.surfaces import sin_surface
from icepyks.porty import point_iso_field
from icepyks.poodle import wn_synthesis
from concurrent.futures import ThreadPoolExecutor
import matplotlib.gridspec as gridspec

num_dims = 3

# setup the batch load parameters
hwave = 2
lwave = 40
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km

#signal parameter
center_f = [1.5e3, 2.5e3, 3.5e3]
fs = 50e3

# wave phase
wave_period = np.sqrt(lwave * 2 * np.pi / 9.81)

t_wave = 0.633  # time in seconds
wp = 2 * np.pi * t_wave / wave_period

# specify source and receiver location
rsrc = np.array([0., 0, -20])
rrcr = np.array([200., 0, -10])
z_source = rsrc[-1]

# HK integration setup
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
taxis = np.arange(num_t) / fs + t_bounds[0]

def hk_ts(fc):
    """plot a time series for each wave position"""
    # xmitt signal information
    y_signal, t_signal = pulse_signal.pulse_q1(fc)
    i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

    # setup surface
    eta = sin_surface.Sine(lwave, hwave, phase=wp)

    # isospeed solution
    iso_field = point_iso_field.IsospeedPoint(eta, z_source)
    isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis,
                                        i_er_signal, num_dims=3)

    return isospeed_HK(rrcr)

i0 = hk_ts(center_f[0])
i1 = hk_ts(center_f[1])
i2 = hk_ts(center_f[2])

gs = gridspec.GridSpec(3,3)
#gs.update(left=0.13, hspace=0.20)
fig = plt.figure()
axes = [plt.subplot(gs[0, :]), plt.subplot(gs[1, :]), plt.subplot(gs[2, :])]

fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

axes[0].plot((taxis - t_spec) * 1e3, i0 / direct_amp, label='HK')
axes[1].plot((taxis - t_spec) * 1e3, i1 / direct_amp)
axes[2].plot((taxis - t_spec) * 1e3, i2 / direct_amp)

axes[0].set_xlim(-0.5, 5)
axes[1].set_xlim(-0.5, 5)
axes[2].set_xlim(-0.5, 5)
axes[0].set_xticklabels('')
axes[1].set_xticklabels('')
axes[0].set_ylim(-1.5, 1.5)
axes[1].set_ylim(-1.5, 1.5)
axes[2].set_ylim(-1.5, 1.5)

axes[0].set_yticks([-1.5, -1, 1, 1.5])
axes[0].set_yticklabels([-1.5, '', '', 1.5])
axes[1].set_yticks([-1.5, -1, 1, 1.5])
axes[1].set_yticklabels(['', '', '', ''])
axes[2].set_yticks([-1.5, -1, 1, 1.5])
axes[2].set_yticklabels(['', -1., 1., ''])

#plt.ylabel('pressure, re. image arrival')
plt.ylabel('re. image arrival')

axes[0].set_title('t=%.2f s, '%t_wave
                  + r'$H$' + ' = %.1f m, '%hwave
                  + r'$\Lambda$' + ' = %.1f m'%lwave
                  + '\n center frequency = %.1f kHz'%(center_f[0] / 1e3))
axes[1].set_title('fc = %.1f kHz'%(center_f[1] / 1e3))
axes[2].set_title('fc = %.1f kHz'%(center_f[2] / 1e3))

axes[0].grid()
axes[1].grid()
axes[2].grid()
axes[2].set_xlabel('time, ms')
axes[0].legend(loc=1)

plt.show(block=False)



gs = gridspec.GridSpec(3,3)
#gs.update(left=0.13, hspace=0.20)
fig = plt.figure()
axes = [plt.subplot(gs[0, :]), plt.subplot(gs[1, :]), plt.subplot(gs[2, :])]

fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

y_signal, t_signal = pulse_signal.pulse_q1(center_f[0])
axes[0].plot(t_signal * 1e3, y_signal)
y_signal, t_signal = pulse_signal.pulse_q1(center_f[1])
axes[1].plot(t_signal * 1e3, y_signal)
y_signal, t_signal = pulse_signal.pulse_q1(center_f[2])
axes[2].plot(t_signal * 1e3, y_signal)

axes[0].set_xlim(-0.5, 5)
axes[1].set_xlim(-0.5, 5)
axes[2].set_xlim(-0.5, 5)
axes[0].set_xticklabels('')
axes[1].set_xticklabels('')
axes[0].set_ylim(-1., 1.)
axes[1].set_ylim(-1., 1.)
axes[2].set_ylim(-1., 1.)

axes[0].set_yticks([-1, 0, 1.])
axes[0].set_yticklabels([-1., '', 1.])
axes[1].set_yticks([-1, 0, 1])
axes[1].set_yticklabels(['', '', ''])
axes[1].set_yticks([-1, 0, 1])
axes[2].set_yticklabels([-1., '', 1.])

plt.ylabel('pressure at 1m')

axes[0].set_title('center frequency = %.1f kHz'%(center_f[0] / 1e3))
axes[1].set_title('fc = %.1f kHz'%(center_f[1] / 1e3))
axes[2].set_title('fc = %.1f kHz'%(center_f[2] / 1e3))

axes[0].grid()
axes[1].grid()
axes[2].grid()
axes[2].set_xlabel('time, ms')

plt.show(block=False)


#plt.savefig('/home/e2richards/personal_essays/figures/hk_abc')
