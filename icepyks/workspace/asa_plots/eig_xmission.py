import numpy as np
from icepyks import pulse_signal
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import matplotlib.gridspec as gridspec

x_mit, taxis = pulse_signal.pulse_q1(2500)

# half cycle lengths
hf2 = 1 / (2 * 2.5)

fig, ax = plt.subplots()
# time domain
ax.plot(taxis * 1e3, x_mit, color='C4')
# left half width line
lline = 2.5 * hf2
# half cycle width
ax.plot([lline, lline], np.array([1, 1.2]), color='C4')
ax.plot([lline + hf2, lline + hf2], np.array([1., 1.2]), color='C4')
ax.annotate("",
             xy=(lline, 1.1), xycoords='data',
             xytext=(lline + hf2, 1.1), textcoords='data',
             arrowprops={'arrowstyle': '<->', 'color':'C4'})

ax.text(lline - 0.2, .7, r'$\frac{T_{c}}{2}$', fontsize=32)

# signal width
hc2 = 0.5 / 2.5
h3c2 = 3 / 2.5

ax.plot(np.array([0, 0]) + hc2,
             np.array([-1, -1.2]) - .35, color='C4')
ax.plot(np.array([h3c2, h3c2]) + hc2,
             np.array([-1., -1.2]) - .35, color='C4')
ax.annotate("",
             xy=(hc2, -1.45), xycoords='data',
             xytext=(hc2 + h3c2, -1.45), textcoords='data',
             arrowprops={'arrowstyle': '<->', 'color':'C4'})
ax.text(hc2 - 0.25, -1.65, r'$3 T_{c}$', fontsize=32)

ax.set_yticks([-1., 0., 1])

ax.set_xlabel('time, ms')
ax.set_ylabel('pressure, ref 1m')
ax.grid()
ax.set_ylim(-2, 1.5)
ax.set_title('Transmitted signal')

plt.show(block=False)
