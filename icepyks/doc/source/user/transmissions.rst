********************
Transmission signals
********************

All acoustic scattering is modeled as a linear process, and the transmission
signal does not have any effect on the scattering physics. The received time
series is calculated as a convolution of transmitted signal with the scattering
impulse response. The signals in this module are designed windowed to have
well bounded representations in both the time and frequency domain, as some
scattering theory is solved in the frequency domain. Additionally, signals are
designed to mimic acoustic probe signals used in experiments.
