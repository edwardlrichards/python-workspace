:: _user:

###################
Iceypyks user guide
###################

This is a collection of example script files meant to describe the basic use of
the icepyks module.

.. toctree::
   :maxdepth: 1

   transmissions
