.. _reference:

#################
Icepyks reference
#################

.. module:: icepyks

This is a description of the modules in the icepyks package.

.. toctree::
   :maxdepth: 1

   porty
   clumber
   poodle
   welchie
