"""
=======================================
Iterative method reflection coefficents
=======================================
Comparison of reflection coefficents computed with the Rayleigh 
Hypothesis (RH) to iterative solutions of the Helmholtz integral equation (HIE)
of the  2nd kind.
"""

import numpy as np
import matplotlib.pyplot as plt
from icepyks.poodle import plane_wave_rs

hwave = 2.
lwave = 40.
ftest = 1050
titest = 15 * np.pi / 180
numqs = 18  # number of evanescent orders

kwave = 2 * np.pi / lwave
cos_ier = plane_wave_rs.CosineReflectionCoefficents(hwave, kwave, attn=0)

xdec = 30 
dx = 1500 / (xdec * ftest)
xaxis = np.arange(0, lwave, dx)

kx = cos_ier.kacous(ftest) * np.cos(titest)

#HK calculations
qtest = cos_ier.qvec(numqs, kx, ftest)
r_HKA = cos_ier.r_HK(kx, qtest, ftest)

# Rayleigh hypothesis
r_RH = cos_ier.r_RH(kx, qtest, ftest)

# compute field at surface
psi_RH = cos_ier.back_project(r_RH, qtest, xaxis, kx, ftest) \
        + cos_ier.psi_inc(xaxis, kx, ftest)

# explicit coding of HIE of 2nd kind, solved by iteration
nmat = cos_ier._n(xaxis, kx, ftest, numqs)
psi_HIE2 = cos_ier.psi_HIE_2nd(xaxis, kx, ftest, num_eva=numqs)

psi_HK = 2 * cos_ier.psi_inc(xaxis, kx, ftest, is_per=True)

gamma = cos_ier.eta.norm_grad(xaxis)

psi_I1 = psi_HK + 2 * nmat @ psi_HK
psi_I2 = psi_HK + 2 * nmat @ psi_I1

ier = [psi_HK]
for i in range(20):
    ier.append(psi_HK + 2 * (nmat @ ier[-1]) * gamma)
    #ier.append(psi_HK + 2 * nmat @ ier[-1])
ier = np.array(ier)

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(xaxis, np.real(psi_RH), label='RH')
axes[0].plot(xaxis, np.real(psi_HK), label='HK')
axes[0].plot(xaxis, np.real(psi_I1), label='I1')
axes[0].plot(xaxis, np.real(psi_I2), label='I2')
axes[0].legend()
axes[0].grid()
axes[0].set_title('Pressure field at surface\n'+
                  '$\partial / \partial n$, real')

axes[1].plot(xaxis, np.imag(psi_RH))
axes[1].plot(xaxis, np.imag(psi_HK))
axes[1].plot(xaxis, np.imag(psi_I1))
axes[1].plot(xaxis, np.imag(psi_I2))
axes[1].grid()
axes[1].set_title('$\partial / \partial n$, imaginary')
axes[1].set_xlabel('Position, x')

fig, ax = plt.subplots()
for i in ier:
    #ax.plot(xaxis, np.abs(i - psi_HIE2))
    ax.plot(xaxis, np.abs(i - psi_RH))

ax.set_title('Error magnitude of pressure field vs. iteration')
ax.set_xlabel('Position, x')

plt.show(block=False)

# energy conservation calculations
r_I2 = cos_ier.r_psi(xaxis, psi_I2, kx, qtest, ftest)
r_Iend = cos_ier.r_psi(xaxis, ier[-1, :], kx, qtest, ftest)

e_RH = cos_ier.r_energy(r_RH, kx, qtest, ftest)
e_I2 = cos_ier.r_energy(r_I2, kx, qtest, ftest)

print('RH Energy conservation at %.1f degrees: %.8f'%
      (titest * 180 / np.pi, e_RH))
print('2nd iterate, HIE Energy conservation at %.1f degrees: %.8f'%
      (titest * 180 / np.pi, e_I2))

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(qtest, np.real(r_HKA), '.', label='HKA', markersize=10)
axes[0].plot(qtest, np.real(r_RH), '.', label='RH', markersize=10)
axes[0].plot(qtest, np.real(r_I2), '.', label='I 2', markersize=10)
axes[0].plot(qtest, np.real(r_Iend), '.', label='I 20', markersize=10)
axes[0].plot([-100, 100], [0, 0], color='red', alpha=0.6)
axes[0].legend()
axes[0].grid()
axes[0].set_title('Reflection coefficents\n' +
                  'Real part, ti = %.1f degrees'%(titest * 180 / np.pi))

axes[1].plot(qtest, np.imag(r_HKA), '.', markersize=10)
axes[1].plot(qtest, np.imag(r_RH), '.', markersize=10)
axes[1].plot(qtest, np.imag(r_I2), '.', markersize=10)
axes[1].plot(qtest, np.imag(r_Iend), '.', markersize=10)
axes[1].plot([-100, 100], [0, 0], color='red', alpha=0.6)
axes[1].grid()
axes[1].set_xlabel('coefficent number')
axes[1].set_title('Imaginary')

axes[1].set_xlim(-1, 4)

plt.show(block=False)
