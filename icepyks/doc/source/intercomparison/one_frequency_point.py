import numpy as np
import matplotlib.pyplot as plt
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.poodle.cosine_surface import CosineSurface
from icepyks.poodle import wn_synthesis
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field

# setup the wave specific solver
hwave = 0.8
lwave = 10
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km
ftest = 1050  # acoustic frequency

# specify source and receiver location
rsrc = np.array([0, 0., -15])
z_source = rsrc[-1]
rrcr = np.array([55, 0., -10])

# spatial integration parameters
r_buffer = 500.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30

# wave number integration parameters
num_eva = 3
numa = 2 ** 16

# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 0.1
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup wave number integration
cos_ier = CosineSurface(hwave, kwave, attn=attn)

# a static q vector seems fine for low frequencies
qvec = np.unique(np.hstack([cos_ier.qvec(num_eva, np.pi / 2, ftest),
                            cos_ier.qvec(num_eva, 0., ftest)]))
eva_range = 0.1  # excursion into imaginary land
num_thetas = 200  # number of points in reflection coefficent interpolation
theta_axis = wn_synthesis.make_theta_axis(num_thetas, eva_range)

# Wave number Rayliegh hypothesis
wn_ier = wn_synthesis.WaveNumberSynthesis(rsrc, rrcr, lwave)
rs = np.array([cos_ier.r_RH(np.cos(ta), qvec, ftest) for ta in theta_axis])
qvec_sym, thetaaxis_sym, rs_sym = wn_synthesis.make_symetric(qvec, theta_axis, rs)

# Wave number as a theta integral
rh_wn = [wn_ier.theta_int(q, qvec_sym, rs_sym, thetaaxis_sym, ftest, numa)
         for q in qvec_sym]
rh_wn_result = np.sum(rh_wn)
# Wave number as a stationary point approxcimation
rh_wn = [wn_ier.kx_sta_ky_sta(q, qvec_sym, rs_sym, thetaaxis_sym, ftest)
         for q in qvec_sym]
rh_wn_result_sta = np.sum(rh_wn)

# Wave number HK
rs = np.array([cos_ier.r_hk(np.cos(ta), qvec, ftest, return_bs=False)
               for ta in theta_axis])
qvec_sym, thetaaxis_sym, rs_sym = wn_synthesis.make_symetric(qvec, theta_axis, rs)
# Wave number as a theta integral
hk_wn = [wn_ier.theta_int(q, qvec_sym, rs_sym, thetaaxis_sym, ftest, numa)
         for q in qvec_sym]
hk_wn_result = np.sum(hk_wn)

# Wave number as a stationary point approxcimation
hk_wn = [wn_ier.kx_sta_ky_sta(q, qvec_sym, rs_sym, thetaaxis_sym, ftest)
         for q in qvec_sym]
hk_wn_result_sta = np.sum(hk_wn)

# Spatial HK solvers
eta = sin_surface.Sine(lwave, hwave, phase=np.pi / 2)
# ray solution
ray_field = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                               range_buffer=r_buffer)
ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
ray_field.to_top(ray_gun)
ray_field.to_wave()
ray_HK = surface_integral.FSurfaceIntegral(ray_field, xaxis, 'HK', num_dims=3)
ray_result = ray_HK(rrcr, ftest)

# isospeed freespace solution
iso_field = point_iso_field.IsospeedPoint(eta, z_source)
iso_HK = surface_integral.FSurfaceIntegral(iso_field, xaxis, 'HK', num_dims=3)
iso_result = iso_HK(rrcr, ftest)

print('')
print('Results at %i Hz'%ftest)
print('')

print('Naive HK:                      %.6f + %.6fj'%(
      np.real(iso_result), np.imag(iso_result)))
print('Shadow HK:                     %.6f + %.6fj'%(
      np.real(ray_result), np.imag(ray_result)))

print('')
print('WN RH:                         %.6f + %.6fj'%(
      np.real(rh_wn_result), np.imag(rh_wn_result)))
print('WN RH, stationary point:       %.6f + %.6fj'%(
      np.real(rh_wn_result_sta), np.imag(rh_wn_result_sta)))
print('WN HK:                         %.6f + %.6fj'%(
      np.real(hk_wn_result), np.imag(hk_wn_result)))
print('WN HK, stationary point:       %.6f + %.6fj'%(
      np.real(hk_wn_result_sta), np.imag(hk_wn_result_sta)))
