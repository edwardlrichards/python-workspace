import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.f_synthesis import synthesize_ts
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field
from icepyks.poodle.cosine_surface import CosineSurface
from icepyks.poodle import wn_synthesis

# setup the batch load parameters
hwave = 0.8
lwave = 10.
kwave = 2 * np.pi / lwave
attn = 1.  # dB / km

#signal parameter
fc = 1.5e3
frange = (500, 2500)  # generous bounds
fs = 50e3

# specify source and receiver location
rsrc = np.array([0., 0, -15])
z_source = rsrc[-1]
rrcr = np.array([55., 0, -10])

# wave number integration parameters
num_eva = 3
numa = 2 ** 16
eva_range = 0.1  # excursion into imaginary land
num_thetas = 200  # number of points in reflection coefficent interpolation
theta_axis = wn_synthesis.make_theta_axis(num_thetas, eva_range)

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)
fcompute = np.arange(frange[0], frange[1] + 1)

# HK integrations
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=np.pi / 2)

# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
kcenter = 2 * np.pi * fc / 1500
direct_amp = np.sqrt(2 / (np.pi * ri * kcenter))

# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
num_t = 2 ** int(np.ceil(np.log2((t_bounds[1] - t_bounds[0]) * fs)))
taxis = np.arange(num_t) / fs + t_bounds[0]

# ray solution
ray_pf = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                               range_buffer=r_buffer)
ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
ray_pf.to_top(ray_gun)
ray_pf.to_wave()

ray_HK = surface_integral.TimeHK(ray_pf, xaxis, taxis, i_er_signal, num_dims=2)
ray_result = ray_HK(rrcr)

# isospeed solution
iso_field = point_iso_field.IsospeedPoint(eta, z_source)
isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal, num_dims=2)
iso_result = isospeed_HK(rrcr)

# compute scattered field using batch processing
# setup rayliegh wni
cos_ier = CosineSurface(hwave, kwave, attn=attn)
wn_ier = wn_synthesis.WaveNumberSynthesis(rsrc, rrcr, lwave)

def batch_run(frun, theory='HK'):
    """Run through the rayleigh coefficents at each frequency"""
    # a static q vector seems fine for low frequencies
    qvec = np.unique(np.hstack([cos_ier.qvec(num_eva, np.pi / 2, frun),
                            cos_ier.qvec(num_eva, 0., frun)]))
    if theory == 'HK':
        rs = np.array([cos_ier.r_hk(np.real(np.cos(ta)), qvec, frun,
                                    return_bs=False)
                    for ta in theta_axis])
    elif theory == 'RH':
        rs = np.array([cos_ier.r_RH(np.real(np.cos(ta)), qvec, frun)
                    for ta in theta_axis])
    else:
        raise(ValueError('Theory has to be either HK or RH'))
    qvec_sym, thetaaxis_sym, rs_sym = wn_synthesis.make_symetric(qvec, theta_axis, rs)
    p_ref = [wn_ier.kx_sta_ky_sta(q, qvec_sym, rs_sym, thetaaxis_sym, frun,
                                  num_dims=2)
             for q in qvec_sym]
    if (frun % 200) == 0:
        print('run number %i'%frun)
    return np.sum(p_ref)

runner = lambda f: batch_run(f, theory='HK')

hk_wn_FT = list(map(runner, fcompute))
# synthesis frequency domain answer to time series
hk_wn_ts, t_wn_hk = synthesize_ts(hk_wn_FT, i_er_signal, fcompute)

# plot the frequency domain of answer
NFFT = ray_result.size * 2
ray_FT = np.fft.rfft(ray_result, NFFT)
iso_FT = np.fft.rfft(iso_result, NFFT)
faxis = np.arange(NFFT // 2 + 1) / NFFT * fs

# synthesis result
dts = t_wn_hk[1] - t_wn_hk[0]
NFFT = 2 ** int(np.ceil(np.log2(t_wn_hk.size)))
hk_wn_FT = np.fft.rfft(hk_wn_ts, NFFT)
wn_faxis = np.arange(NFFT // 2 + 1) / NFFT / dts

fig, ax = plt.subplots()
ax.plot(wn_faxis, np.abs(hk_wn_FT))
ax.plot(faxis, np.abs(iso_FT))
ax.plot(faxis, np.abs(ray_FT))
ax.grid()
ax.set_xlim(fcompute[0], fcompute[-1])
plt.show(block=False)

# upsample timeseries before plotting
hk_wn_up, t_hk_wn_up = resample(hk_wn_ts, t_wn_hk.size * 4, t=t_wn_hk)

# create differences plots
hk_ts_comp = np.interp(taxis, t_hk_wn_up, hk_wn_up, left=0., right=0.)
d1 = iso_result - hk_ts_comp
d2 = ray_result - hk_ts_comp

fig, axes = plt.subplots(2, 1, sharex=True)
fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
axes[0].plot((t_hk_wn_up - t_spec) * 1e3, hk_wn_up / direct_amp,
             color='C1', label='wn HK')
axes[0].plot((taxis - t_spec) * 1e3, iso_result / direct_amp,
             color='C4',label='nHK')
axes[0].plot((taxis - t_spec) * 1e3, ray_result / direct_amp,
             color='C2', label='sHK')

axes[1].plot((taxis - t_spec) * 1e3, d1 / direct_amp, color='C4')
axes[1].plot((taxis - t_spec) * 1e3, d2 / direct_amp, color='C2')

axes[0].yaxis.tick_right()
axes[1].yaxis.tick_right()

axes[1].set_xlim(-0.1, 7)
axes[0].set_ylim(-1, 1)
axes[1].set_ylim(-1, 1)
#axes[0].set_ylabel('pressure, re. image arrival')
plt.ylabel(r'pressure, re. $\sqrt{\frac{2}{\pi \ r_{img} \ k_c}}$')
#fig.text(0.04, 0.5, r'pressure, re. $\sqrt{\frac{2}{\pi r_i k_c}}$',
         #va='center', rotation='vertical')
axes[1].set_xlabel('time, ms')
axes[0].set_title('Line source scatter time series\n'+
                  'wave height = %.1f m, length = %.1f m'%(hwave, lwave))
axes[1].set_title('Difference from wave number HK')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)

plt.show(block=False)
