import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hilbert
# import direct iso-speed solver
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp
from icepyks.porty import hk_integral, iso_speed
from icepyks.porty import surface_field as port_field
# import ray tracer
from icepyks.clumber import surface_field as clum_field
from icepyks.clumber import surface_integral, iso_rg

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 40
w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
wave_amp = 1.5
c0 = 1500
z_source = -20
r_receiver = np.array((200., 0, -10))
r_source = np.array((0., 0, z_source))
x_bounds = (-r_receiver[0] * 0.2, r_receiver[0] * 1.2)
z_bottom = -300
z_const = -(wave_amp + 0.01)

# calculated values
t_spec = np.linalg.norm(r_source + r_receiver) / c0
direct_amp = 1 / np.linalg.norm(r_source - r_receiver)

#signal parameter
fc = 20e3
signal = pulse_signal.sine_four_hanning(fc)
i_er_signal = signal_interp.ContinuousSignal(signal)


# setup for HK solvers
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3,
            np.ceil(t_spec * 1e3 + 8) / 1e3)
fs = 5e5
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)
x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]

# porty solution
port_solver = hk_integral.HK(signal, taxis)
eta = sin_surface.Sine(r_receiver, z_source, z_const, z_bottom,
                       lamda_surf, wave_amp, phase=0.)
field1 = iso_speed.IsospeedField(eta, c0, is_source=True)
field2 = iso_speed.IsospeedField(eta, c0, is_source=False)
at_top = port_field.SurfaceField(field2, field1)
at_top.setup_stationary_phase(x_bounds, x1_bounds)
port_result = port_solver(xaxis, at_top)

unI = np.zeros(port_solver.weights.size, dtype=np.int_)
unI[port_solver.sortI] = np.arange(unI.size, dtype=np.int_)
port_a = port_solver.weights[unI]
port_t = port_solver.delays[unI]
port_tpp = np.squeeze(at_top.delay_pp(xaxis, np.zeros_like(xaxis)))

# clumber solution
ray_gun = iso_rg.Isospeed(c0)
# setup ray trace solver
clum = clum_field.SurfaceField(eta, tt_tol=5e-7, range_buffer=50)
clum.to_top(ray_gun)
clum.to_wave(eta)
clum('wave')
clum_solver = surface_integral.TimeHK(clum, xaxis, taxis, i_er_signal)
clum_result = clum_solver()

#intermediate values in time domain integral
weights, delays, sI = clum_solver._time_sort(clum_solver.dp_dn,
                                             clum_solver.rcr_greens)
ys = np.zeros_like(clum_solver.xaxis)
fpp = clum_solver.field.tau_d2dy2(clum_solver.xaxis, ys)
unI = np.zeros(weights.size, dtype=np.int_)
unI[sI] = np.arange(unI.size, dtype=np.int_)
clum_a = weights[unI]
clum_t = delays[unI]
clum_tpp = fpp

ca1 = clum.amplitude(xaxis, np.zeros_like(xaxis), 'source',
                      is_projected=True)
ca2 = clum.amplitude(xaxis, np.zeros_like(xaxis),
                    'receiver', is_projected=False)

fig, ax = plt.subplots()
ax.plot(xaxis, np.squeeze(field1.amplitude(xaxis, np.zeros_like(xaxis))),
        'b', label='porty')
ax.plot(xaxis, ca1, 'r--', label='clumber')
ax.legend()
ax.grid()

fig, ax = plt.subplots()
ax.plot(xaxis, np.squeeze(field2.amplitude(xaxis, np.zeros_like(xaxis))),
        'b', label='porty')
ax.plot(xaxis, ca2, 'r--', label='clumber')
ax.legend()
ax.grid()

fig, ax = plt.subplots()
ax.plot(xaxis, np.squeeze(field2.amplitude(xaxis, np.zeros_like(xaxis))),
        'b', label='porty')

ax.plot(xaxis, clum.amplitude(xaxis, np.zeros_like(xaxis),
                              'receiver', is_projected=False),
        'r--', label='clumber')
ax.legend()
ax.grid()

fig, ax = plt.subplots()
ax.plot(xaxis, np.squeeze(port_t), label='porty')
ax.plot(xaxis, clum_t, label='clumber')
ax.legend()
ax.grid()

fig, ax = plt.subplots()
ax.plot(xaxis, np.squeeze(port_tpp), label='porty')
ax.plot(xaxis, clum_tpp, label='clumber')
ax.legend()
ax.grid()

fig, ax = plt.subplots()
ax.plot(port_result.time * 1e3, port_result / direct_amp, label='porty')
ax.plot(taxis * 1e3, clum_result / direct_amp, label='clumber')
ax.set_xlim(134.5, 137)
ax.set_ylim(-0.6, 0.6)
ax.set_xlabel('delay, ms')
ax.set_title('Time series from Choo and Song letter')
ax.grid()
ax.legend()

plt.show(block=False)
