import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import resample
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.f_synthesis import synthesize_ts
from icepyks.surfaces import sin_surface
from icepyks.clumber import iso_rg, ray_field
from icepyks.porty import point_iso_field
from icepyks.poodle.cosine_surface import CosineSurface
from icepyks.poodle import wn_synthesis

# setup the batch load parameters
hwave = 0.8
lwave = 10.
attn = 1.
#signal parameter
fc = 1.5e3
frange = (500, 2500)  # generous bounds
fs = 50e3

# specify source and receiver location
rsrc = np.array([0., 0, -15])
z_source = rsrc[-1]
rrcr = np.array([55., 0, -10])

# xmitt signal information
y_signal, t_signal = pulse_signal.pulse_q1(fc)
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)
fcompute = np.arange(frange[0], frange[1] + 1)

# HK integrations
# setup spatial integration parameters
r_buffer = 50.
z_const = -(hwave / 2 + 0.1)
z_bottom = -30
# spatial integration
x_bounds = (-r_buffer, rrcr[0] + r_buffer)
dx = 3e-2
xaxis = np.arange(min(x_bounds), max(x_bounds), dx)

# wave number integrations
numa = 2 ** 16  # number of quadrature points in wni
eva_range = 0.1  # excursion into imaginary land
num_thetas = 200  # number of points in reflection coefficent interpolation
num_eva = 1
theta_axis = wn_synthesis.make_theta_axis(num_thetas, eva_range)

# setup surface
eta = sin_surface.Sine(lwave, hwave, phase=np.pi / 2)
# Specular image source information
ri = np.sqrt((rsrc[0] - rrcr[0]) ** 2 + (rsrc[-1] + rrcr[-1]) ** 2)
t_spec = ri / 1500.
direct_amp = 1 / ri
# time integration
t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3, np.ceil(t_spec * 1e3 + 8) / 1e3)
taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

# ray solution
ray_pf = ray_field.RayField(eta, z_source, rrcr, tt_tol=5e-7,
                               range_buffer=r_buffer)
ray_gun = iso_rg.Isospeed(1500., z_bottom, z_const)
ray_pf.to_top(ray_gun)
ray_pf.to_wave()

ray_HK = surface_integral.TimeHK(ray_pf, xaxis, taxis, i_er_signal)
ray_result = ray_HK(rrcr)

# isospeed solution
iso_field = point_iso_field.IsospeedPoint(eta, z_source)
isospeed_HK = surface_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal)
iso_result = isospeed_HK(rrcr)


# compute scattered field using batch processing
# setup rayliegh wni
cos_ier = CosineSurface(hwave, 2 * np.pi / lwave, attn=attn)
wn_ier = wn_synthesis.WaveNumberSynthesis(rsrc, rrcr, lwave)

def batch_run(frun, theory='HK'):
    """Run through the rayleigh coefficents at each frequency"""
    # a static q vector seems fine for low frequencies
    qvec = np.unique(np.hstack([cos_ier.qvec(num_eva, np.pi / 2, frun),
                            cos_ier.qvec(num_eva, 0., frun)]))
    if theory == 'HK':
        rs = np.array([cos_ier.r_hk(np.real(np.cos(ta)), qvec, frun,
                                    return_bs=False)
                    for ta in theta_axis])
    elif theory == 'RH':
        rs = np.array([cos_ier.r_RH(np.real(np.cos(ta)), qvec, frun)
                    for ta in theta_axis])
    else:
        raise(ValueError('Theory has to be either HK or RH'))
    qvec_sym, thetaaxis_sym, rs_sym = wn_synthesis.make_symetric(qvec, theta_axis, rs)
    p_ref = [wn_ier.kx_sta_ky_sta(q, qvec_sym, rs_sym, thetaaxis_sym, frun)
             for q in qvec_sym]
    if (frun % 200) == 0:
        print('run number %i'%frun)
    return np.sum(p_ref)

hk_wn_FT = [batch_run(f, theory='HK') for f in fcompute]
hk_wn_ts, t_hk_wn = synthesize_ts(hk_wn_FT, i_er_signal, fcompute)

# upsample timeseries before plotting
hk_wn_ts, t_hk_wn = resample(hk_wn_ts, t_hk_wn.size * 4, t=t_hk_wn)

# create differences plots
hk_ts_comp = np.interp(taxis, t_hk_wn, hk_wn_ts, left=0., right=0.)
d1 = iso_result - hk_ts_comp
d2 = ray_result - hk_ts_comp

fig, axes = plt.subplots(2, 1, sharex=True)
fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
axes[0].plot((t_hk_wn - t_spec) * 1e3, hk_wn_ts / direct_amp,
             color='C1', label='wnHK')
axes[0].plot((taxis - t_spec) * 1e3, iso_result / direct_amp,
             color='C4',label='nHK')
axes[0].plot((taxis - t_spec) * 1e3, ray_result / direct_amp,
             color='C2', label='sHK')

axes[1].plot((taxis - t_spec) * 1e3, d1 / direct_amp, color='C4')
axes[1].plot((taxis - t_spec) * 1e3, d2 / direct_amp, color='C2')
axes[1].set_xlim(-0.1, 7)
axes[0].set_ylim(-1, 1)
axes[1].set_ylim(-1, 1)
plt.ylabel('pressure, re. image arrival')
axes[1].set_xlabel('time, ms')
axes[0].set_title('Point source scatter time series\n'+
                  'wave height = %.1f m, length = %.1f m'%(hwave, lwave))
axes[1].set_title('Difference from wave number HK')
axes[0].grid()
axes[1].grid()
axes[0].legend(loc=1)

plt.show(block=False)
