:: _intercomparison:

###################################
Iceypyks solutions intercomparisons
###################################

Comparison between different solution methods.

.. toctree::
   :maxdepth: 1

Flat surface reflections

.. plot:: intercomparison/image_source_2d.py
.. plot:: intercomparison/image_source_3d.py
