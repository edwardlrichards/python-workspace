.. Icepyks documentation master file, created by
   sphinx-quickstart on Sat Feb 17 12:31:25 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Icepyks
==================

The package icepyks models acoustic scatter from the sea surface. There are a
number of different solution methods implimented for the purpose of
intercomparison. This comparison is born out in the name, pronounced "ice 
peaks", and derived from novICE staKES, with a PY thrown in for good measure.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   user/index
   intercomparison/index
   reference/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
