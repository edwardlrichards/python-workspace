from setuptools import setup, find_packages

setup(
    name="icepyks",
    version="0.1",
    packages=find_packages()
)
