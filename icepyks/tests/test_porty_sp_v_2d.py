"""
Stationary phase approximation and 2D surface integral should give same result.
Stationary phase seems to be more accurate that simple quadrature schemes in
practice, but the difference is minor.
"""
import unittest
import numpy as np
from scipy.signal import hilbert
# import direct iso-speed solver
from icepyks.surfaces import sin_surface
from icepyks import pulse_signal, signal_interp
from icepyks.porty import hk_integral, iso_speed
from icepyks.porty import surface_field as port_field

class IsoHKSPV2D(unittest.TestCase):
    """Perform HK integration using a sine surface"""
    def setUp(self):
        """parameters of specific test case"""
        #Sine wave parameters
        lamda_surf = 40
        w_surf = np.sqrt(9.81 * 2 * np.pi / lamda_surf)
        wave_amp = 1.5
        self.c0 = 1500
        z_source = -20
        r_receiver = np.array((200., 0, -10))
        r_source = np.array((0., 0, z_source))
        self.x_bounds = (-r_receiver[0] * 0.2, r_receiver[0] * 1.2)
        z_bottom = -300
        z_const = -(wave_amp + 0.01)
        # General test geometry specifications
        self.eta = sin_surface.Sine(r_receiver, z_source, z_const, z_bottom,
                                    lamda_surf, wave_amp, phase=0.)
        # time axis calculated values
        t_spec = np.linalg.norm(r_source + r_receiver) / self.c0
        # setup for HK solver
        t_bounds = (np.floor(t_spec * 1e3 - 1) / 1e3,
                    np.ceil(t_spec * 1e3 + 8) / 1e3)
        fs = 5e5
        self.taxis = np.arange(min(t_bounds), max(t_bounds), 1 / fs)

        #signal parameter
        fc = 20e3
        self.signal = pulse_signal.sine_four_hanning(fc)
        # spatial grid setup
        dx = 3e-2
        self.xaxis = np.arange(min(self.x_bounds), max(self.x_bounds), dx)
        self.x1_bounds = lambda x0: [(-10, 10) for x in np.array(x0, ndmin=1)]
        self.x1axis = np.arange(-10, 10, dx)


    def test_compare_2D_v_SP(self):
        """Compute result using stationary phase and 2D integral"""
        port_solver = hk_integral.HK(self.signal, self.taxis)
        field1 = iso_speed.IsospeedField(self.eta, self.c0, is_source=True)
        field2 = iso_speed.IsospeedField(self.eta, self.c0, is_source=False)
        at_top = port_field.SurfaceField(field2, field1)
        X, Y = np.meshgrid(self.xaxis, self.x1axis)
        port_2D = np.array(port_solver(X, at_top, y=Y))
        # stationary phase solver
        at_top.setup_stationary_phase(self.x_bounds, self.x1_bounds)
        port_stationary = np.array(port_solver(self.xaxis, at_top))
        max_value = np.max(np.abs(port_stationary))
        port_2D /= max_value
        port_stationary /= max_value
        # 2D integral rings
        # only test values where stationary phase result is greater that thresh
        thresh_i = np.abs(port_stationary) > 0.04
        np.testing.assert_allclose(port_2D[thresh_i],
                                       port_stationary[thresh_i],
                                       rtol=np.inf,
                                       atol=0.005)
