"""
2D llyods mirror using HK integration. This should be exact as there is only
one bounce in non-refracting media.
"""
import unittest
import numpy as np
from icepyks.clumber.surface_interaction import c_lin_flat
from icepyks.clumber.surface_field import SurfaceField
from icepyks.clumber.iso_rg import Isospeed
from icepyks.clumber.surface_integral import FreqHK
from icepyks.surfaces import flat_surface
from atpy import bellhop
import os

class IsoHKFlat2DTest(unittest.TestCase):
    def setUp(self):
        """Basic specification of a ray gun"""
        self.c0 = 1500
        self.z_source = -25.
        self.z_rcr = -200.
        self.rho_range = np.r_[50: 5000: 30j]
        self.fc = 150
        self.z_bottom = -5e3
        self.x_axis_buffer = 200

        # computed values
        self.w = 2 * np.pi * self.fc
        self.k = self.w / self.c0
        self.dx = self.c0 / self.fc / 5

        # setup incident field
        self.rg = Isospeed(self.c0)


    def hk_1d(self, inc_field, x_range):
        """compute scatter amplitude for a given range"""
        # need to impliment a custom hk for single frequency propagation
        point_to_line = np.sqrt(2 / np.pi / self.k) * np.exp(-1j * np.pi / 4)
        y_range = np.zeros_like(x_range)
        src_amp = inc_field.amplitude(x_range, y_range, 'source',
                                      is_projected=True, num_dims=2)
        rcr_amp = inc_field.amplitude(x_range, y_range, 'receiver', num_dims=2)
        # delays relate to the phase term
        delays = inc_field.delay(x_range, y_range, 'source')\
                + inc_field.delay(x_range, y_range, 'receiver')
        i_grand = src_amp * rcr_amp * np.exp(1j * self.w * delays)
        # a product of point sources needs to be advanced in phase 2x
        i_grand *= point_to_line * np.exp(-1j * np.pi / 4)
        # frequency domain solution, put in derivative term directly
        i_grand *= 1j * self.k  # derivative term correction
        # Basic quadrature
        surf_result = np.nansum(i_grand) * self.dx / 2 / 1j
        # Direct ray calculation
        rad_ray = inc_field.direct_ray(self.fc, num_dims=2)
        # radiated ray is also corrected for derivative term
        #rad_ray = rad_amp * np.exp(1j * self.w * rad_tau)
        return surf_result, rad_ray

    def hk_2d(self, inc_field, x_range):
        """compute scatter amplitude for a given range"""
        # need to impliment a custom hk for single frequency propagation
        y_range = np.zeros_like(x_range)
        src_amp = inc_field.amplitude(x_range, y_range, 'source',
                                      is_projected=True, num_dims=3)
        rcr_amp = inc_field.amplitude(x_range, y_range, 'receiver', num_dims=3)
        # delays relate to the phase term
        delays = inc_field.delay(x_range, y_range, 'source')\
                + inc_field.delay(x_range, y_range, 'receiver')
        i_grand = src_amp * rcr_amp * np.exp(1j * self.w * delays)
        # frequency domain solution, put in derivative term directly
        i_grand *= 1j * self.k  # derivative term correction
        # Basic quadrature
        surf_result = np.nansum(i_grand) * self.dx / 2 / np.pi
        # Direct ray calculation
        rad_ray = inc_field.direct_ray(self.fc, num_dims=3)
        # radiated ray is also corrected for derivative term
        #rad_ray = rad_amp * np.exp(1j * self.w * rad_tau)
        return surf_result, rad_ray

    def xmitt_totl(self, i_result):
        """simple conversion from the output of for loop to tl"""
        p = np.array([complex(tr[1] - tr[0]) for tr in i_result])
        tl = 20 * np.log10(np.abs(p))
        return tl

    def analytic_llyods(self, num_dims):
        """iso-speed llyod's mirror has an analytic solution"""
        # iso llyods mirror
        r1 = np.sqrt(self.rho_range ** 2 + (self.z_rcr - self.z_source) ** 2)
        r2 = np.sqrt(self.rho_range ** 2 + (self.z_rcr + self.z_source) ** 2)

        # point source greens function
        p_p = lambda r_r: np.exp(1j * self.k * r_r) / r_r
        # scale point source to be a line source, amp = 1 @ 1m
        cyln_ff = lambda r_r: np.exp(1j * np.pi / 4)\
                            * np.sqrt(2 * r_r / self.k / np.pi)\
                            * p_p(r_r)

        if num_dims == 3:
            analytic = np.array([p_p(r2), p_p(r1)]).T
        else:
            analytic = np.array([cyln_ff(r2), cyln_ff(r1)]).T
        return analytic

    def explicit_hk(self, num_dims):
        """Generator for HK scattered result through values of range"""
        for r_r in self.rho_range:
            r_receiver = [r_r, 0, self.z_rcr]
            test_eta = flat_surface.Flat(r_receiver, self.z_source,
                                         0., self.z_bottom)
            surf_in = SurfaceField(test_eta,
                                   max_iter=75,
                                   range_buffer=self.x_axis_buffer)
            surf_in.to_top(self.rg)
            num_x = np.ceil((r_r + self.x_axis_buffer) / self.dx)
            x_range = np.arange(num_x) * self.dx - (self.x_axis_buffer / 2)
            # solve for incident fields
            if num_dims == 3:
                result = self.hk_2d(surf_in, x_range)
            else:
                result = self.hk_1d(surf_in, x_range)
            yield result

    def module_hk(self, num_dims):
        """Generator for HK scattered result through values of range"""
        for r_r in self.rho_range:
            yield self.module_hk_one_range(num_dims, r_r)

    def module_hk_one_range(self, num_dims, one_range):
        """Generator for HK scattered result through values of range"""
        r_receiver = [one_range, 0, self.z_rcr]
        test_eta = flat_surface.Flat(r_receiver, self.z_source,
                                     0., self.z_bottom)
        surf_in = SurfaceField(test_eta,
                                max_iter=75,
                                range_buffer=self.x_axis_buffer)
        surf_in.to_top(self.rg)
        num_x = np.ceil((one_range + self.x_axis_buffer) / self.dx)
        x_range = np.arange(num_x) * self.dx - (self.x_axis_buffer / 2)
        weights = np.ones_like(x_range)
        hk_er = FreqHK(surf_in, x_range, weights, num_dims=num_dims)
        return hk_er(self.fc), hk_er.direct_ray(self.fc)

    def test_total_tl_line(self):
        """Compare HK result with analytic solution"""
        tl_ana = self.xmitt_totl(self.analytic_llyods(2))
        tl_calc = self.xmitt_totl(list(self.explicit_hk(2)))
        # check that results are good up to half a dB
        np.testing.assert_allclose(tl_ana, tl_calc, rtol=np.inf, atol=0.5)

    def test_explicit_v_module_hk_line(self):
        """Test the module HK integral vs expicitly coded version"""
        tl_mod = self.xmitt_totl(list(self.module_hk(2)))
        tl_exp = self.xmitt_totl(list(self.explicit_hk(2)))
        # check that results are good up to half a dB
        np.testing.assert_allclose(tl_mod, tl_exp, rtol=np.inf, atol=0.5)

    def test_total_tl_point(self):
        """Compare HK result with analytic solution"""
        tl_ana = self.xmitt_totl(self.analytic_llyods(3))
        tl_calc = self.xmitt_totl(list(self.explicit_hk(3)))
        # check that results are good up to half a dB
        np.testing.assert_allclose(tl_ana, tl_calc, rtol=np.inf, atol=0.5)

    def test_explicit_v_module_hk_point(self):
        """Test the module HK integral vs expicitly coded version"""
        tl_mod = self.xmitt_totl(list(self.module_hk(3)))
        tl_exp = self.xmitt_totl(list(self.explicit_hk(3)))
        # check that results are good up to half a dB
        np.testing.assert_allclose(tl_mod, tl_exp, rtol=np.inf, atol=0.5)
