import numpy as np
import pandas as pd
import xarray as xr
import os
import subprocess
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from helper import config
from os import path
from atpy import arrivals

bell_path = config.bellhop_dir()
bellhop = path.join(bell_path, 'bellhop.exe')

def runBellhop(bellpath, bellhop=bellhop):
    """Run bellhop fortran code"""
    filename, _ = os.path.splitext(bellpath)
    subprocess.check_output([bellhop, filename])

def read_rayFile(bellhopPath):
    """Parse a ray file."""
    # filePath works both with and without an extension
    fileName, fileExtension = os.path.splitext(bellhopPath)

    with open(fileName + '.ray') as f:
        # Header info
        title = f.readline()
        freq = float(f.readline())
        nBeams = int(f.readline())
        topDepth = float(f.readline())
        bottomDepth = float(f.readline())
        coords = f.readline()

        # read in all beams
        beams = []
        for _ in range(nBeams):
            #alpha = f.readline()
            #if alpha == '':
                #break
            #alpha = float(alpha)
            info = f.readline().split()
            nsteps = int(info[0])
            NumTopBnc = int(info[1])
            NumBotBnc = int(info[2])
            beamPath = _readBeam(f, nsteps)
            #beamDict = {'alpha': alpha, 'numTopBnc': NumTopBnc,
            beamDict = {'numTopBnc': NumTopBnc,
                        'numBotBnc': NumBotBnc, 'beamPath': beamPath}

            la = f.readline()
            beams.append(beamDict)
    return beams


def _readBeam(f, numberOfSteps):
    """Read a beam till end"""
    ray = []
    for _ in range(numberOfSteps):
        line = f.readline()
        ray.append([float(x) for x in line.split()])
    ray = np.array(ray)
    return xr.DataArray(ray[:, 1], dims=['distance'], coords=[ray[:, 0]])


def readArr(fileName):
    """read array file
    works with receiver batch runs
    """
    # TODO: source batch runs
    fileName, fileExtension = os.path.splitext(fileName)
    # structure of beam arrival data stored in .arr file
    names = ('amp', 'degree', 'delay', 'sourceAngle', 'receiveAngle',
             'numTopBounce', 'numBotBounce')
    with open(fileName + '.arr') as f:
        # read file header
        info = f.readline().split()
        freq = float(info[0])
        numSourceDepth = int(info[1])
        numReceiveDepth = int(info[2])
        numReceiveRange = int(info[3])
        sourceDepths = np.array([float(num) for num in f.readline().split()])
        receiveDepth = np.array([float(num) for num in f.readline().split()])
        receiveRange = np.array([float(num) for num in f.readline().split()])
        # each range is range for all receivers, hence need of meshgrid
        D, R = np.meshgrid(receiveDepth, receiveRange)
        # next line is necassary for source batch runs
        _ = f.readline()
        # read all arrivals in .arr file
        arrivalArray = list(_readAllArrivals(f, D.flatten(order='F'),
                                             R.flatten(order='F'), sourceDepths, names, freq))
    # return arrival data structure containing all parsed arrivals
    return arrivals.receiverArray(arrivalArray)


def _readAllArrivals(f, rd, rr, sd, names, freq):
    """function to read each arrivals from file after reading header"""
    # return arrival dictionary from a single receiver
    readNum = 0
    maxNumReads = f.readline()
    while maxNumReads != '':
        fromFile = list(_readReceiveArr(f, int(maxNumReads)))
        receiverArrivals = pd.DataFrame(fromFile, columns=names)
        arrivalDic = {'freq': freq, 'sourceDepth': sd,
                      'receiveDepth': rd[readNum], 'receiveRange': rr[readNum],
                      'arrivals': receiverArrivals}
        readNum += 1
        maxNumReads = f.readline()
        yield arrivalDic


def _readReceiveArr(f, maxNumReads):
    """Read arrivals for a single receiver"""
    for _ in range(maxNumReads):
        line = f.readline()
        splitLine = [float(x) for x in line.split()]
        if len(splitLine) == 0:
            return
        yield splitLine


def readEnv(fileName):
    """read ssp from env file

    Really only works for files that were created by my own routines
    """
    fileName, fileExtension = os.path.splitext(fileName)

    columnNames = ('z', 'c', 'c_s', 'rho', 'alpha_p', 'alpha_s')
    profile = pd.read_csv(fileName + '.env', skiprows=5,
                          comment='/', names=columnNames, delim_whitespace=True)
    # extract source and receiver depth
    sd = profile.iloc[-8, 0]
    rd = profile.iloc[-5, 0]
    # remove nonsense at bottom of read file
    # first index with a NaN value
    endIndex = pd.isnull(profile.T).any().nonzero()[0][0]
    profile = profile.ix[:endIndex, :]
    # need to drop last NaN value index now
    profile = profile.iloc[:-1, :]
    # Use z as the index
    profile.index = profile['z']
    del profile['z']
    profile = profile.astype(float)
    profile.index = profile.index.astype(float)

    return profile


def readSHD(fileName):
    """ read binary shade file """
    fileName, fileExtension = os.path.splitext(fileName)
    with open(fileName + '.shd') as f:
        record_length = 4 * int(np.fromfile(f, dtype=np.int32, count=1))
        #title = f.read(80)
        #f.seek(record_length, 0)
        #plot_type = f.read(10)
        f.seek(2 * record_length, 0)
        freq = float(np.fromfile(f, dtype=np.float32, count=1))
        temp = np.fromfile(f, dtype=np.int32, count=6)
        Ntheta, Nsx, Nsy, Nsd, Nrd, Nrr = tuple(temp)
        atten = float(np.fromfile(f, dtype=np.float32, count=1))
        f.seek(3 * record_length, 0)
        theta = np.fromfile(f, dtype=np.float32, count=Ntheta)
        # only consider basic data format
        f.seek(4 * record_length, 0)
        x_axis = np.fromfile(f, dtype=np.float32, count=Nsx)
        f.seek(5 * record_length, 0)
        y_axis = np.fromfile(f, dtype=np.float32, count=Nsy)
        f.seek(6 * record_length, 0)
        src_d = np.fromfile(f, dtype=np.float32, count=Nsd)
        f.seek(7 * record_length, 0)
        rcr_d = np.fromfile(f, dtype=np.float32, count=Nrd)
        f.seek(8 * record_length, 0)
        rcr_r = np.fromfile(f, dtype=np.float32, count=Nrr)
        # read data grid
        t = []
        for i_t, _ in enumerate(theta):
            s = []
            for i_s, _ in enumerate(src_d):
                r = []
                for i_r, _ in enumerate(rcr_d):
                    recnum = 9 + i_t * Nsd * Nrr + i_s * Nrr + i_r
                    f.seek(recnum * record_length, 0)
                    p = np.fromfile(f, dtype=np.float32, count=(2 * Nrr))
                    p_out = p[::2] + 1j * p[1::2]
                    r.append(p_out)
                s.append(r)
            t.append(s)
    return rcr_r, np.array(t)
