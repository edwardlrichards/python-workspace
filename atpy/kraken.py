import numpy as np
import xarray as xr
from os import path
import subprocess

kraken_exe = '/home/e2richards/at/Kraken/kraken.exe'

def run_kraken(envpath):
    filename, _ = path.splitext(envpath)
    subprocess.check_output([kraken_exe, filename])

def synthesize_pressure(modes, r, zs):
    """Compute pressure amplitude from normal modes"""
    km = modes.coords['k']
    r = xr.DataArray(r, coords=[r], dims=['r'])
    r_dependence = np.exp(1j * km * r)
    r_dependence /= np.sqrt(km)
    phi_source = modes.isel(z=float(zs))
    z_dependence = modes * phi_source
    p = np.dot(z_dependence.T, r_dependence)
    p *= 1j * np.exp(1j * np.pi /4) / np.sqrt(8 * np.pi * r)
    return p

def read_mod(envpath):
    """read binary mode file output by kraken"""
    envpath = path.splitext(envpath)[0] + '.mod'
    with open(envpath, 'rb') as f:
        lrec = int(4 * np.fromfile(f, dtype='i4', count=1))
        f.seek(4)
        title = f.read(80)
        freq = np.fromfile(f, dtype='f4', count=1)
        nums = np.fromfile(f, dtype='i4', count=3)

        if nums[1] < 0:
            raise Exception('No modes in file')

        f.seek(lrec)
        num=[]
        medium_type = []
        for _ in range(nums[0]):
            num.append(np.fromfile(f, dtype='i4', count=1))
            medium_type.append(f.read(8))

        #Boundry properties
        f.seek(2 * lrec)
        top_bc = f.read(1)
        top_soundspeed = np.fromfile(f, dtype='f4', count=4)
        top_soundspeed = (np.complex(top_soundspeed[0], top_soundspeed[1]),
                          np.complex(top_soundspeed[2], top_soundspeed[3]))
        top_rho = np.fromfile(f, dtype='f4', count=1)
        top_depth = np.fromfile(f, dtype='f4', count=1)
        bot_bc = f.read(1)
        bot_soundspeed = np.fromfile(f, dtype='f4', count=4)
        bot_soundspeed = (bot_soundspeed[0] + 1j * bot_soundspeed[1],
                          bot_soundspeed[2] + 1j * bot_soundspeed[3])
        bot_rho = np.fromfile(f, dtype='f4', count=1)
        bot_depth = np.fromfile(f, dtype='f4', count=1)

        #media properties
        f.seek(3 * lrec)
        bulk = np.fromfile(f, dtype='f4', count = 2 * nums[0])
        bulk = bulk.reshape(-1, nums[0])

        #number of modes
        f.seek(4 * lrec)
        M = int(np.fromfile(f, dtype='i4', count=1))

        #sample depths
        f.seek(5 * lrec)
        z = np.fromfile(f, dtype='f4', count = nums[1])

        #modes
        phi = np.zeros((M, nums[2]), dtype='complex_')
        for i in range(M):
            rec = f.seek((i+6) * lrec)
            phi_c = np.fromfile(f, dtype='f4', count=2 * nums[2])
            phi[i, :] = phi_c[::2] + 1j * phi_c[1::2]

        #wave numbers
        f.seek((6 + M) * lrec)
        k_c = np.fromfile(f, 'f4', count=2 * M)
        k = k_c[::2] + 1j * k_c[1::2]

        mode_da = xr.DataArray(phi, coords=[k, z], dims=['k', 'z'])
    return mode_da
