import numpy as np
import os

def writeKraken(z, c, name='test', saveDir='.', sourceD=93.5, receiveD=95,
             receiveRange=3, bottomDepth=100, frequency=10000, thorpFlag=False,
             attenuation=0, numZGrid=100, interpFlag='C', cbounds=None):
    """write a kraken env file"""

    if cbounds is None:
        cbounds = (0, 1800)

    envpath = _writeEnv(z, c, name=name, saveDir=saveDir,
                       sourceD=sourceD, receiveD=receiveD,
                       receiveRange=receiveRange, bottomDepth=bottomDepth,
                       frequency=frequency, thorpFlag=thorpFlag,
                       attenuation=attenuation, numZGrid=numZGrid,
                       interpFlag=interpFlag)
    with open(envpath, 'a') as f:
        f.write('%.1f %.1f \t!clow chigh, m/s\n' %cbounds)
        f.write('%.6f \t RMAX (km)\n' %receiveRange)

    _write_grid(envpath, sourceD, receiveD, numZGrid, bottomDepth)
    return envpath

def writeBellhop(z, c, runType, name='test', saveDir='.',
                 sourceD=93.5, receiveD=95, receiveRange=3, bottomDepth=100,
                 fanTheta=(-25, 25), frequency=10000, numAngles=None,
                 thorpFlag=False, attenuation=0, numRGrid=1, numZGrid=1,
                 interpFlag='C', beamType='B'):
    """Write a bellhop env file, mostly relies on writeEnv"""
    #Make TL runs default to a grid of receivers
    receiveRange = np.array(receiveRange)
    z = np.array(z)
    c = np.array(c)
    gridH = receiveRange.size
    if runType == 'C' or runType == 'I' or runType == 'S':
        if numRGrid == 1:
            numRGrid = 300
        if numZGrid == 1:
            numZGrid = 100
        if gridH == 1:
            receiveRange = np.array((0, receiveRange[0]))
            gridH = numRGrid
        elif gridH == 2:
            gridH = numRGrid
    envpath = _writeEnv(z, c, name=name, saveDir=saveDir,
                       sourceD=sourceD, receiveD=receiveD,
                       receiveRange=receiveRange, bottomDepth=bottomDepth,
                       frequency=frequency, thorpFlag=thorpFlag,
                       attenuation=attenuation, numZGrid=numZGrid,
                       interpFlag=interpFlag)
    _write_grid(envpath, sourceD, receiveD, numZGrid, bottomDepth)
    #Create bellhop specific appendix
    if runType == 'R':
        if numAngles is None:
            numAngles = 20.
    elif runType == 'A':
        if numAngles is None:
            numAngles = 0.
    elif runType == 'E':
        if numAngles is None:
            numAngles = 5e3
    elif runType == 'C' or runType == 'I' or runType == 'S':
        if numAngles is None:
            numAngles = 0.
    # Options 3
    # Explicitly specify beam type for TL calculations
    o3 = runType + beamType
    # Bound the problem
    rayStep = 0.
    zmax = bottomDepth * 1.1
    rmax = np.max(receiveRange) * 1.1
    with open(envpath, 'a') as f:
        if gridH == 1:
            f.write('%u\t ! NR\n%.6f/ \t! R(1:NR) (km) \n'
                    % (gridH, receiveRange))
        else:
            f.write('%u \t! NR\n' % gridH)
            for rD in receiveRange:
                f.write('%.6f ' % rD)
            f.write('/   \t! RD(1:NRD)\n')
        f.write('%s \t! R/C/I/S/E/A \n' %o3)
        f.write('%u \t! NBeams\n%.2f %.2f / ! ALPHA1,2 (degrees)\n'
                % (numAngles, fanTheta[0], fanTheta[1]))
        f.write('%.2f %.2f %.2f \t! STEP (m), ZBOX (m), RBOX (km)\n'
                % (rayStep, zmax, rmax))

    return envpath

def _writeEnv(z, c, name='test', saveDir='.', sourceD=93.5, receiveD=95,
             receiveRange=3, bottomDepth=100, frequency=10000, thorpFlag=False,
             attenuation=0, numZGrid=100, interpFlag='C'):
    """Write a general acoustic toolbox file, not specific to any model"""
    if thorpFlag:
        thorpFlag = 'T'

    # Target file
    saveName = '%s.env' %name
    title = name

    # Dummy variable in Bellhop
    nmedia = 1

    # Options 1
    # Attenuation flag
    if thorpFlag:
        o1 = interpFlag + 'VWT'
    else:
        o1 = interpFlag + 'VW'

    #nmesh estimated according to Kraken manual
    nmesh = bottomDepth / (1500 / frequency) * 10
    sigmas = 0

    # Bottom Block
    o2 = 'A'
    sigmab = 0.
    bottom = (bottomDepth, 1600, 0, 1.8, 0.8)

    envDir = os.path.join(saveDir, 'envs')

    # Make sure the the deepest sound speed is above the bottom
    index = (z <= bottomDepth)
    # z, c_p, c_s, rho, atten_p, atten_s
    profile = np.vstack((z[index], c[index], np.zeros(c[index].size),
                         np.ones(c[index].size), attenuation *
                         np.ones(c[index].size),
                         np.zeros(c[index].size)))
    # Ensure that there is a value at the depth of the bottom
    closestToBottom = np.copy(profile[:, np.argmin(
        np.abs(profile[0, :] - bottomDepth))])

    if (closestToBottom[0] != bottomDepth):
        closestToBottom[0] = bottomDepth
        profile = np.vstack((profile.T, closestToBottom)).T
        profile = profile[:, np.argsort(profile[0, :])]

    # Select the CTD file
    # Create the save directory if it doesn't exist
    if not os.path.isdir(envDir):
        os.mkdir(envDir)

    envPath = os.path.join(envDir, saveName)
    with open(envPath, 'w+') as f:
        # Parse out the file
        f.write('%s \t! TITLE\n' % title)
        f.write('%.1f    \t! FREQ (HZ)\n' % frequency)
        f.write('%u     \t! NMEDIA\n' % nmedia)
        f.write('%s  \t! SSPOPT (Analytic or C-linear interpolation)\n' % o1)
        f.write('%u %.1f %.2f \t! Depth of bottom (m)\n'
                % (nmesh, sigmas, bottomDepth))
        for pr in profile.T:
            f.write('%.3f %.3f %.3f %.3f %.8f %.3f/ \n' % tuple(pr.tolist()))
        f.write('%s %.2f \n' % (o2, sigmab))
        f.write('%.2f %.1f %.1f %.1f %.1f/\n' % bottom)
    return os.path.abspath(envPath)

def _write_grid(envpath, sourceD, receiveD, numZGrid, bottomDepth):
    """Write source and receiver depth grid"""
    sourceD = np.array(sourceD)
    receiveD = np.array(receiveD)
    nSources = sourceD.size
    # check if necassary to grid recievers
    gridV = receiveD.size
    if (gridV == 1) and (numZGrid != 1):
        receiveD = np.array((0, bottomDepth))
        gridV = numZGrid
    elif receiveD.size == 2:
        gridV = numZGrid

    with open(envpath, 'a') as f:
        # Cases below allow for 1 or more sources and recievers
        if nSources == 1:
            f.write('%u \t! NSD\n%.2f/\t! SD(1:NRD) (m)\n'
                    % (nSources, sourceD))
        else:
            f.write('%u \t! NSD\n%.2f %.2f/\t! SD(1:NRD) (m)\n'
                    % (nSources, sourceD))
        if gridV == 1:
            f.write('%u \t! NRD\n%.2f/   \t! RD(1:NRD)\n' %
                    (gridV, receiveD))
        else:
            f.write('%u \t! NRD\n' % gridV)
            for rD in receiveD:
                f.write('%.2f ' % rD)
            f.write('/   \t! RD(1:NRD)\n')
