import numpy as np
import pandas as pd


class receiverArray:
    """A collection of elements"""

    def __init__(self, arrivalList):
        """initilize one element for each bellhop arrival"""
        self.array = [element(arr) for arr in arrivalList]

    @property
    def arrivals(self):
        """list of arrival dataframes, one for each element"""
        return [arr.arrivals for arr in self.array]

    @property
    def rd(self):
        """receiver depths for each element"""
        return np.array([arr.rd for arr in self.array])

    @property
    def sd(self):
        """source depth of experiment"""
        return np.array([arr.sd for arr in self.array])

    def filterArrivals(self, numTopBounces=None, numBotBounces=None,
                       angleBounds=None, arrivalMagnitude=None):
        """Apply an arrival filter to every element"""
        for arr in self.array:
            arr.filterArrivals(numTopBounces, numBotBounces, angleBounds,
                               arrivalMagnitude)

    def sampleArrivals(self, fs, timeBounds):
        """Returns a sampled version of signal as a DataFrame"""
        allChan = [arr.sampleArrivals(fs, timeBounds) for arr in self.array]
        allChan = pd.concat(allChan, names=self.arrayDepths, axis=1)
        return allChan

    def ref0dB(self, norm='all', timeRef=True):
        """Create a common magnitude, with possiblity of time reference
        norm may be 'all' or 'channel'
        """
        refI = [arr.arrivals['mag'].idxmax() for arr in self.array]
        refMag = [arr.arrivals.loc[rI, 'mag']
                  for arr, rI in zip(self.array, refI)]

        # Time is referenced to the absolute max arrival
        maxChannel = refMag.index(max(refMag))
        refTime = self.array[maxChannel].arrivals.loc[refI[maxChannel],
                                                       'delay']
        # if norm is all, all channels refernce magnitude is max arrival
        if norm == 'all':
            refMag = np.ones(len(refMag)) * max(refMag)

        for arr, ref in zip(self.array, refMag):
            arr.magnitudeReference += ref
            if timeRef:
                arr.timeReference = refTime

class element:
    """Arrival structure for a single element"""

    def __init__(self, arrivalDictionary):
        """Pull all information from the dictionary"""
        self.rd = arrivalDictionary['receiveDepth']
        self.sd = arrivalDictionary['sourceDepth']
        self.rr = arrivalDictionary['receiveRange']
        self.freq = arrivalDictionary['freq']
        self._arrivals = arrivalDictionary['arrivals']
        self.magnitudeReference = 0
        self.timeReference = 0
        # initilize to an empty filter
        self.arrivalI = None

    @property
    def arrivals(self):
        """Return arrival data frame, correct time and magnitude references"""
        if self.arrivalI is None:
            selectionI = np.ones(self._arrivals.shape[0], dtype='bool_')
        else:
            selectionI = self.arrivalI
        sel = self._arrivals
        mag = 20 * np.log10(np.abs(sel.loc[selectionI, 'amp']) + np.spacing(1))
        mag -= self.magnitudeReference
        sel.loc[selectionI, 'mag'] = mag
        sel.loc[selectionI, 'time'] = (sel.loc[selectionI, 'delay']
                                             - self.timeReference) * 1e3
        return sel.ix[selectionI]

    def filterArrivals(self, numTopBounces=None, numBotBounces=None,
                       angleBounds=None, arrivalMagnitude=None):
        """Put a persistant filter on element properties"""
        magI = np.ones(self._arrivals.shape[0], dtype='bool_')
        topI = np.copy(magI)
        botI = np.copy(magI)
        angI = np.copy(magI)
        #reset filter
        self.arrivalI = magI
        if numTopBounces is not None:
            numTopBounces = np.array(numTopBounces, ndmin=1)
        if numBotBounces is not None:
            numBotBounces = np.array(numBotBounces, ndmin=1)
        # filter to match arrival magnitude
        if arrivalMagnitude is not None:
            magnitude = 20 * \
                np.log10(np.abs(self.arrivals['amp']) + np.spacing(1))
            magnitude -= self.magnitudeReference
            magI = magnitude >= arrivalMagnitude
        # filter to match top bounces
        if numTopBounces is not None:
            numTopBounces = np.array(numTopBounces)
            topI = np.zeros(self._arrivals.shape[0], dtype='bool_')
            for num in numTopBounces:
                topI |= self.arrivals['numTopBounce'] == num
        # filter to match bot bounces
        if numBotBounces is not None:
            numBotBounces = np.array(numBotBounces)
            botI = np.zeros(self.arrivals.shape[0], dtype='bool_')
            for num in numBotBounces:
                botI |= self.arrivals['numBotBounce'] == num
        # filter to match angle bounds
        if angleBounds is not None:
            angleBounds = np.array(angleBounds)
            if angleBounds.size != 2:
                raise Exception('need exactly 2 bounds')
            angI = (self.arrivals['receiveAngle'] > angleBounds[0]) &\
                (self.arrivals['receiveAngle'] <= angleBounds[1])
        self.arrivalI = magI & topI & botI & angI

    def sampleArrivals(self, fs, winBounds):
        """cast arrivals to a uniformly sampled time axis"""
        winBounds = np.array(winBounds)

        # Create a regularly sampled time axis
        tAxis = np.arange(winBounds[0], winBounds[1], 1 / fs)
        arrivalAxis = np.zeros(tAxis.shape, dtype='complex_')

        # Compute coherent signal amplitudes
        arrivals = self.arrivals['amp'] * \
            np.exp(1j * np.radians(self.arrivals['degree']))

        # Cast samples to right side
        insertI = np.searchsorted(tAxis, self.arrivals['delay'])
        # Remove arrivals before or after tAix boundries
        activeI = (insertI > 0) & (insertI < tAxis.size)

        # Add samples to uniform time series. Add coherently when multiple
        # arrivals fall at same sample time
        for aI, arr in zip(insertI[activeI], arrivals[activeI]):
            arrivalAxis[aI] += arr

        return pd.Series(arrivalAxis, index=tAxis)
