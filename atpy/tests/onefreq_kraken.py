from atpy import write_at, kraken
from kam11 import thermister
from datetime import datetime
import matplotlib.pyplot as plt

toi = datetime.strptime('2011 J184 03:51', '%Y J%j %H:%M')
ssp = thermister.getSoundSpeed(toi)

envpath = write_at.writeKraken(ssp.index, ssp.values)
kraken.run_kraken(envpath)
phi = kraken.read_mod(envpath)
