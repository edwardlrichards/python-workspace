import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
from atpy import bellhop, write_at
from kam11 import thermister
from plotting import bellhop_plot

c = 1500
rd = np.r_[97: 40: 16j]
rr = 3e3
sd = 93.5
time_ofex = '2011 J184 03:51'
toi = datetime.strptime(time_ofex, '%Y J%j %H:%M')
ssp = thermister.getSoundSpeed(toi)

envpath = write_at.writeBellhop(ssp.index, ssp.values, 'A',
        receiveD = rd, receiveRange=rr/1e3, sourceD=sd)
bellhop.runBellhop(envpath)
kam_arrivals = bellhop.readArr(envpath)
kam_arrivals.ref0dB()
bellhop_plot.spark_delay(kam_arrivals, profile=ssp, tbounds=(-8, 2))

envpath = write_at.writeBellhop(ssp.index, ssp.values, 'E',
        receiveD = rd, receiveRange=rr/1e3, sourceD=sd)

bellhop.runBellhop(envpath)
rays = bellhop.read_rayFile(envpath)
bellhop_plot.ray_trace(rays, rd, rr=rr)

#isolate the direct path
kam_arrivals.filterArrivals(numTopBounces=1, arrivalMagnitude=-50)
plt.show(block=False)
