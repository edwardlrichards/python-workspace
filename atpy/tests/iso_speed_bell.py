import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from atpy import bellhop, write_at
from plotting import bellhop_plot
from beamforming import curvedWaveFront

c = 1500
rd = np.r_[97: 40: 16j]
rr = 3e3
sd = 93.5
bd = 100
ssp = pd.Series([c, c], index=[0, 100])
envpath = write_at.writeBellhop(ssp.index.values, ssp.values, 'A',
        receiveD = rd, receiveRange=rr / 1e3, sourceD=sd, bottomDepth=bd)
bellhop.runBellhop(envpath)
iso_speed = bellhop.readArr(envpath)
iso_speed.ref0dB()
bellhop_plot.spark_delay(iso_speed, tbounds=(-1, 10))
#wave front from angle calculator
wf = curvedWaveFront.WavefrontDelays(ssp, rd, z0=rd[1])

#isolate the direct path
iso_speed.filterArrivals(numTopBounces=0, numBotBounces=0)
arrival_time = np.squeeze(np.array([np.array(arr['delay']) \
                                    for arr in iso_speed.arrivals]))
#compaire with snell's law
arrival_angle = iso_speed.arrivals[0]['receiveAngle']
q = 1e3 / c * np.sin(np.radians(arrival_angle))
tau = float(q) * (rd - rd[0])
wave_error = (arrival_time - arrival_time[0]) * 1e3 - tau

#Distance based delay
r = np.sqrt((sd - rd) ** 2 + rr ** 2)
d_time = r / c
direct_error = arrival_time - d_time

#geometric based planewave error
d_r = (np.max(d_time) - np.min(d_time)) * 1e3

#Image source boundry conditions
def relative_sd(zs, rd, D, m=0):
    """Source distance taken from COA 2.138"""
    zm1 = 2 * D * m - zs + rd
    zm2 = 2 * D * (m + 1) - zs - rd
    zm3 = 2 * D * m + zs + rd
    zm4 = 2 * D * (m + 1) + zs - rd
    return zm1, zm2, zm3, zm4

z01, z02, z03, z04 = relative_sd(sd, rd, bd, m=0)

# Test arrival 1
iso_speed.filterArrivals(numTopBounces=0, numBotBounces=0)
tau = np.sqrt(z01 ** 2 + rr ** 2) / c
arrival_time = np.squeeze(np.array([np.array(arr['delay']) \
                                    for arr in iso_speed.arrivals]))
print('Arrival 1 max error is %0.6f ms'%(1e3 * np.max(tau - arrival_time)))
print('Arrival angle at %.1f is %.1f degrees'%
      (iso_speed.rd[1], iso_speed.arrivals[1]['receiveAngle']))
first_curve = tau - tau[1]

# Test arrival 2
iso_speed.filterArrivals(numTopBounces=0, numBotBounces=1)
arrival_time = np.squeeze(np.array([np.array(arr['delay']) \
                                    for arr in iso_speed.arrivals]))
tau = np.sqrt(z02 ** 2 + rr ** 2) / c
print('Arrival 2 max error is %0.6f ms'%(1e3 * np.max(tau - arrival_time)))
print('Arrival angle at %.1f is %.1f degrees'%
      (iso_speed.rd[1], iso_speed.arrivals[1]['receiveAngle']))
est_fromfirst = arrival_time[1] + first_curve + 1e-3 * wf(iso_speed.arrivals[1]['receiveAngle'])
print('Max error from wf1 curvature is %0.6f ms'%
      (1e3 * np.max(est_fromfirst - arrival_time)))


# Test arrival 3
iso_speed.filterArrivals(numTopBounces=1, numBotBounces=0)
arrival_time = np.squeeze(np.array([np.array(arr['delay']) \
                                    for arr in iso_speed.arrivals]))
tau = np.sqrt(z03 ** 2 + rr ** 2) / c
print('Arrival 3 max error is %0.6f ms'%(1e3 * np.max(tau - arrival_time)))

#Test arrival 4
iso_speed.filterArrivals(numTopBounces=1, numBotBounces=1)
arrival_time = np.squeeze(np.array([np.array(arr['delay']) \
                                    for arr in iso_speed.arrivals]))
tau = np.sqrt(z04 ** 2 + rr ** 2) / c
print('Arrival 4 max error is %0.6f ms'%(1e3 * np.max(tau - arrival_time[:, 0])))

plt.show(block=False)
