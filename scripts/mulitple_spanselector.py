import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.widgets import SpanSelector
import numpy as np

#set up two plot channels
fig = plt.figure(figsize=(8, 6))
gs = gridspec.GridSpec(100, 100, bottom=0.18, left=0.18, right=0.88)
v_start = [0, 50]
width = 40

def make_subplot(start, width):
    """Initilize a constant width subplot at a given start value"""
    sub = gs[start: (start + width), : ]
    return plt.subplot(sub)
axes = []
for s in v_start: axes.append(make_subplot(s, width))

#Attach a span selector to each channel
def span_selection(x1, x2, index=None):
    """Append selected span to result array, and draw a red rectangle"""
    axes[index].axvspan(x1, x2, facecolor='r', alpha=0.5)

#Attach the functions to each axis
spans = []
for i, ax in enumerate(axes):
    #nested lambda retains index state
    selection = lambda i: lambda x1, x2: span_selection(x1, x2, index=i)
    span = SpanSelector(ax, selection(i), 'horizontal', useblit=True,
                        rectprops=dict(alpha=0.5, facecolor='red'))
    spans.append(span)

#plot nonsense data
test = [ax.pcolormesh(np.random.randn(20, 100)) for ax in axes]
plt.show(block=True)
