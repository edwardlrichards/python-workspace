import numpy as np

np.random.seed(0)
r1 = np.random.randn(3)
r2 = np.random.randn(3, 3)

a1 = np.argmax(r1)
m1 = np.amax(r1)
assert(m1 == r1[a1])

a2 = np.argmax(r2)
m2 = np.amax(r2)
assert(m2 == r2.flatten()[a2])

#Gets around flattening
it = np.nditer(r2, flags=['multi_index'])
max_value, max_index = it[0], it.multi_index
it.iternext()
while not it.finished:
    if it[0] > max_value:
        max_value, max_index = it[0], it.multi_index
    it.iternext()
assert(m2 == r2[max_index])

#iterate over one dimension
dim_num = 0
m_d0 = np.amax(r2, dim_num)

all_dims = np.arange(len(r2.shape))
tracker = np.arange(r2.shape[dim_num])
max_value = np.empty(tracker.shape)
track_max = np.full(tracker.shape, False, dtype='bool_')

#This is weird, and probibly won't work
not_dim = int(all_dims[all_dims != dim_num])
max_index = list(range(r2.shape[dim_num]))
max_index = [max_index.copy() for _ in range(len(r2.shape))]

it = np.nditer([tracker, r2], flags=['multi_index'])

while not it.finished:
    if (not track_max[it[0]]):
        max_value[it[0]], max_index[0][it[0]] = it[1], it.multi_index[0]
        track_max[it[0]] = True
    elif (it[1] > max_value[it[0]]):
        max_value[it[0]], max_index[0][it[0]] = it[1], it.multi_index[0]
    it.iternext()

assert(np.all(m_d0 == r2[max_index]))

