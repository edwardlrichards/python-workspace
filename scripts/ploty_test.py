import numpy
import plotly

plotly.plotly.sign_in("EdwardLRichards", "8c0uzeh94k")
x = numpy.arange(0.0, 100.0, 0.1)
y = numpy.sin(x)
trace0 = plotly.graph_objs.Scatter(x=x,y=y, name="Sin")
plotly.plotly.iplot([trace0])
