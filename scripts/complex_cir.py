import numpy as np
import matplotlib.pyplot as plt
from helper import test_signals
import scipy.signal as sig

orth_amps = (1, -0.25)
orth_amps = (1, 0)
orth_amps = (0, 1)
fs = 1e5
fc = 3e3
numT = 1e5
t = np.arange(numT) / fs
x = orth_amps[0] * np.sin(fc * 2 * np.pi * t) +\
        orth_amps[1] * np.cos(fc * 2 * np.pi * t)
#plt.figure()
#plt.plot(t, x)
#plt.xlim(0, 3e-4)

NFFT=int(2**np.ceil(np.log2(numT)))
f = np.arange(NFFT) / NFFT * fs
window = sig.kaiser(numT, 1.5 * np.pi)
x_FT = np.fft.fft(x * window, NFFT)
#plt.figure()
#plt.plot(f, np.abs(x_FT))
#plt.xlim(fc - 1e2, fc + 1e2)

maxI = np.argmin(np.abs(f - fc))
maxPhase = np.degrees(np.angle(x_FT[maxI]))
print('Phase at center frequency is %.1f'%maxPhase)

#Can we manufacture phase from a real signal?

testphase = 45  # degrees
x1 = np.sin(fc * 2 * np.pi * t)
x_hilb = sig.hilbert(x1)
x_hilb *= np.exp(1j * np.radians(testphase))
x2 = np.real(x_hilb)
x1_FT = np.fft.fft(x1 * window, NFFT)
x2_FT = np.fft.fft(x2 * window, NFFT)

diff_angles = np.degrees(np.angle(x1_FT[maxI])) -\
              np.degrees(np.angle(x2_FT[maxI]))
diff_mag = (np.abs(x1_FT[maxI]) - np.abs(x2_FT[maxI])) / np.abs(x2_FT[maxI])
print('difference in phases is %0.1f'%(diff_angles))


mfchirp = test_signals.mfchirp()

#plt.show(block=False)

