import numpy as np
import xarray as xr
from datetime import datetime
import matplotlib.pyplot as plt
from kam11 import thermister, context
from atpy import bellhop, write_at
from plotting import bellhop_plot
from beamforming import curvedWaveFront

num_receivers = 6
toi = datetime.strptime('2011 J184 03:51', '%Y J%j %H:%M')
ssp = thermister.getSoundSpeed(toi)
rd = context.VLA1().phoneDepths()[: num_receivers]
bell_file = write_at.writeBellhop(ssp.index, ssp.values, 'A',
                                  name='bottom_arrivals', receiveD=rd.values)
bellhop.runBellhop(bell_file)
bottom_A = bellhop.readArr(bell_file)
bottom_A.ref0dB()
fig, ax = bellhop_plot.spark_delay(bottom_A, profile=ssp)

#Compare beamformer expectation with bellhop for top bounce arrivals
tau_er = curvedWaveFront.WavefrontDelays(ssp, rd)
bottom_A.filterArrivals(numTopBounces=[1], arrivalMagnitude=[-50])
angle_r = bottom_A.arrivals[0]['receiveAngle'].values
time_r = bottom_A.arrivals[0]['time'].values

fig, ax = bellhop_plot.spark_delay(bottom_A, tbounds=(-5, 1))

#plot expected delays
wf_tau = []

for a, t in zip(angle_r, time_r):
    ax.plot(tau_er(a) + t, rd, 'ro', markerfacecolor='None')
    wf_tau.append(tau_er(a) + t)
wf_tau = np.array(wf_tau).T

wf_mismatch = []
for wf_chan, bell_chan in zip(wf_tau, bottom_A.arrivals):
    mis_match = wf_chan[None,: ] - bell_chan['time'].values[:, None]
    wf_mismatch.append(np.min(np.abs(mis_match), axis=-1))
wf_mismatch = np.array(wf_mismatch)

ax.set_ylim(100, rd.min() - 5)

#compare to iso-speed arrivals
def iso_delay(angle):
    """caluculate isospeed delays at each depth"""
    s = 1 / ssp[85]
    q = np.sign(angle) * np.sin(np.abs(np.radians(angle))) * s
    tau = q * rd.values * 1e3
    tau = np.array(tau, ndmin=2)
    tau = tau - tau[:, 0]
    return np.squeeze(tau)

approx_error = np.abs(tau_er(6) - iso_delay(6))
print(approx_error)
plt.show(block=False)
