from datetime import datetime, timedelta
import numpy as np
import matplotlib.pyplot as plt
from plotting import mf_plot, beam_plot
from kam11 import matchedFilter, time_beamforming

toi = datetime.strptime('2011 J184 03:51:05', '%Y J%j %H:%M:%S')
dt = timedelta(0, 5)
mf = matchedFilter.LFM_MatchedFilter(toi, dt).matchedFilter()
cmap = plt.cm.magma_r
cmap.set_under('w')
#fig, ax = mf_plot.plotMultipleChannel(mf, plotChannels=np.arange(1, 8), tStart=-8.5, tEnd = 4, cmap=cmap)
fig, ax = mf_plot.plotMultipleChannel(mf, tStart=-8.5, tEnd = 4, cmap=cmap)

#Hacky :)
im = ax.get_children()[-2]
im.set_clim(-45, 0)

(bf_er, wf) = time_beamforming.load_beamformer(toi)
fig_1, ax_1 = beam_plot.plot_ts(bf_er.ts, tbounds=(-8, -1))

plt.show()
