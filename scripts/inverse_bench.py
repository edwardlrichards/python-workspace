import numpy as np
import cProfile, pstats, io
pr = cProfile.Profile()
testA = np.random.randn(15000, 15000)
testb = np.random.randn(15000)
pr.run('np.linalg.solve(testA, testb)')

s = io.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print(s.getvalue())
