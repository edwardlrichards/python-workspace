import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import SpanSelector

test_data = np.random.randn(1000, 1000)

fig, ax = plt.subplots()

ax.imshow(test_data)

def selection(x1, x2):
    """This function isn't the point"""
    pass

span = SpanSelector(ax, selection, 'horizontal', #useblit=True,
                    rectprops=dict(alpha=0.5, facecolor='red'), span_stays=True)

plt.show()
