import numpy as np
import scipy.interpolate as interp

np.random.seed(0)

signal = lambda t: np.sin(t / (2 * np.pi))
numCycles = 4
samples_percycle = 10
interp_order = 2
tRange = np.r_[0: numCycles: numCycles * samples_percycle * 1j]

t_ofInterest = numCycles * np.random.rand(int(1e6))
t_ofInterest.sort()

y_signal = signal(tRange)
#Simplest interpolator
i_1d = interp.interp1d(tRange, y_signal, assume_sorted=False)

#knot interpolator
tck = interp.splrep(tRange, y_signal, k=interp_order, s=0)
tck_a = interp.splder(tck)

#OO interpolator
us = interp.UnivariateSpline(tRange, y_signal, k=interp_order, s=0)
us_a = us.derivative()

#Test 1, interp1d
y_interp1d = i_1d(t_ofInterest)

#knots
y_spl = interp.splev(t_ofInterest, tck_a)

#OO
y_oo = us_a(t_ofInterest)

#decimated interpolator
y_dec = y_signal[np.digitize(t_ofInterest, tRange)]

# speed loops, emphasize small interpolators
t_ofInterest = numCycles * np.random.rand(10)
t_ofInterest.sort()

i_1d = interp.interp1d(tRange, y_signal, assume_sorted=False)
y_interp1d = i_1d(t_ofInterest)

np_1d = np.interp(t_ofInterest, tRange, y_signal)
