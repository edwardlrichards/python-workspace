"""
Hopefully this will clarify *the* way to calculate a normal derivative.
"""
import numpy as np
from scipy.special import hankel1
import matplotlib.pyplot as plt

k = 5
r = np.r_[.1: 10: 500j]
dr = r[1] - r[0]

hank = hankel1(0, r)
hank_ana = -hankel1(1, r)
hank_num = np.diff(hank) / dr

fig, ax = plt.subplots()
ax.plot(r, np.real(hank_ana), label='analytical')
ax.plot(r[:-1], np.real(hank_num), 'g.', label='numerical')
ax.set_title('real first derivative, no scaling')

hank = hankel1(0, k * r)

# derivative calculation for dr
hank_num = np.diff(hank) / dr
hank_ana = -k * hankel1(1, k * r)

fig, ax = plt.subplots()
ax.plot(r[:-1], np.real(hank_num), label='numerical')
ax.plot(r, np.real(hank_ana), label='analytic')
ax.set_title('real part first derivativel, scaled by k')
ax.grid()
ax.legend()

# asymptotic behavior of hankel functions
h0_approx = np.sqrt(2 / np.pi / k / r) * np.exp(1j * (k * r - np.pi / 4))
h1_approx = -k * np.sqrt(2 / np.pi / k / r) *\
            np.exp(1j * (k * r - 3 * np.pi / 4))

plt.figure()
plt.subplot(2, 1, 1)
plt.plot(r, np.real(hank), label='analytic')
plt.plot(r, np.real(h0_approx), label='asymptotic')
plt.title('Asymptotic approximations of hankel functions')
plt.subplot(2, 1, 2)
plt.plot(r, np.real(hank_ana), label='analytic')
plt.plot(r, np.real(h1_approx), label='asymptotic')

# RMS error
h0_RMS = np.abs(h0_approx - hank)
h1_RMS = np.abs(h1_approx - hank_ana)

plt.figure()
plt.plot(k * r, h0_RMS, label='h0 approx')
plt.plot(k * r, h1_RMS, label='h1 approx')
plt.title('RMS error of asymptotic approximation')
plt.legend()
plt.ylim(0, 0.25)
plt.xlabel('k * r')

# integration of the hankel function about 0
dx = np.logspace(-6, -1)
int_approx = [(hankel1(0, k * d/2) - hankel1(0, -k * d/2)) / d for d in dx]
int_approx = np.array(int_approx)

fig, ax = plt.subplots()
ax.plot(dx, np.abs(int_approx), 'b.')
ax.plot(dx, np.abs(hankel1(0, k * dx / 2 / np.exp(1))), 'g.')


plt.show(block=False)
