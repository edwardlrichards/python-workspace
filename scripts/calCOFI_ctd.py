import pandas as pd
import matplotlib.pyplot as plt

def read_ctd(cast_file):
    """read calCOFI data into DataFrame"""
    return pd.read_csv(cast_file, header=0, index_col=12)

