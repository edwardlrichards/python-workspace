import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import spectrogram

fc = 4
bw = 2 * fc

t_max = 2
edge = 0.5

k = bw / t_max
f0 = fc - bw / 2

NFFT = 2 ** 8
N_sec = 2 ** 5

t_range = np.r_[0: t_max: NFFT * 1j]
y = np.sin(2 * np.pi * (f0 * t_range + k / 2 * t_range ** 2))

fig, ax = plt.subplots()
ax.plot(t_range, y)

fig, ax = plt.subplots()
dt = t_range[1] - t_range[0]
fs = 1 / dt
win = np.kaiser(N_sec, 0.5 * np.pi)
f, time, Pxx = spectrogram(y, fs=fs, window=win, nfft=2**8, nperseg=N_sec, scaling='spectrum')
[ax.plot(Pxx[:, i] + .2 * i, f, 'b')for i, t in enumerate(time)]
ax.set_ylim(0, 20)

plt.show(block=False)
