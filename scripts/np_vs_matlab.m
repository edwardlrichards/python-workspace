test_matrix = randn(1e4, 1e4);

%See whether Matlab can index faster
tic()
arrayI = (test_matrix > 2) & (test_matrix < 3);
toc()

test_matrix = randn(2, 1e4, 1e4);
red = [1; 0];

tic()
distance = squeeze(sum(bsxfun(@times, red, test_matrix), 1));
toc()

tic()
test_matrix = randn(3, 1e4, 1e4);
toc()

tic()
test_matrix(3, :, :) = cos(test_matrix(3, :, :));
toc()

tic()
test_norm = squeeze(sum(test_matrix .* conj(test_matrix), 1));
toc()

