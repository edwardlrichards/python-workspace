import numpy as np
import xarray as xr

#matrix sizes
n1 = int(1e3)
n2 = n1 + 10
n3 = n1 + 15

test_matrix_1 = np.random.randn(n1, n2)
test_matrix_2 = np.random.randn(n2, n3)

test_da_1 = xr.DataArray(test_matrix_1, dims=['a1', 'a2'])
test_da_2 = xr.DataArray(test_matrix_2, dims=['a2', 'a3'])

def dot_product():
    return np.dot(test_matrix_1, test_matrix_2)

def dim_agg():
    prod = test_da_1 * test_da_2
    return prod.sum(dim='a2')

