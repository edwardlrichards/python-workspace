import numpy as np
import cProfile, pstats, io
pr = cProfile.Profile()

#matlab really kicks ass on the cos
pr.run('test_matrix[:, :, 2] = np.cos(test_matrix[:, :, 2])')

norm = np.linalg.norm(test_matrix, ord=2, axis=-1)

s = io.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print(s.getvalue())
