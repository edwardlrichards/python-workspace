import numpy as np
from scipy.integrate import ode
from scipy.interpolate import UnivariateSpline, RegularGridInterpolator, interp1d
import savex_env

image_ssp = lambda z: savex_env.ssp(np.abs(z))  # create an image source
tau_ier = None

def get_tau():
    """Return a 2D interpolator of ray delay time"""
    # integration setup
    num_theta = 200
    x_grid = np.arange(250) + 50  # start interpolation at x = 50
    z_grid = np.arange(200) / 199 * 2 * savex_env.z_bottom
    z_grid = z_grid - savex_env.z_bottom

    theta_range = -np.arcsin(np.arange(num_theta) / (num_theta - 1) * 0.98)
    theta_range = theta_range[1:]

    points = []
    for t in theta_range:
        ier = ray_iers(t)
        points.append(ier(x_grid))

    points = np.array(points)

    # interpolate delay to a uniform depth grid
    taus = np.array([interp1d(p[0, :], p[1, :],
                              fill_value='extrapolate')(z_grid)
                     for p in points.T])

    tau_ier = RegularGridInterpolator((x_grid, z_grid), taus)

    return tau_ier

def ray_iers(theta_launch, smax=400.):
    """Return interpolators for z = f(x) and t = f(x)"""
    ds = 10  # integration step, meters

    u = 1 / savex_env.ssp(savex_env.z_bottom)
    s0 = np.cos(theta_launch) * u
    s2 = np.sin(theta_launch) * u

    y_init = np.array((0., s0, savex_env.z_bottom, s2, 0.))

    def rhseqn(s, y):
        """Right hand side of the eikonal equation"""
        c = image_ssp(y[2])
        ydot = [c * y[1],
                0.,
                c * y[3],
                -(1 / c ** 2) * savex_env.ssp(y[2], nu=1),
                1 / c]
        return ydot


    solver = ode(rhseqn).set_integrator('vode', method='bdf')
    solver.set_initial_value(y_init, 0.)
    ys = []
    while solver.successful() and solver.t < smax:
        ys.append(solver.integrate(solver.t + ds))
    ys = np.array(ys)

    xs = ys[:, 0]
    zs = ys[:, 2]
    ts = ys[:, 4]

    z_ier = UnivariateSpline(xs, zs, k=1, s=0, ext=0)
    t_ier = UnivariateSpline(xs, ts, k=1, s=0, ext=0)

    ier = lambda x: np.array((z_ier(x), t_ier(x)))
    return ier


if __name__== '__main__':
    import matplotlib.pyplot as plt
    import spherical_delays

    tau_ier = get_tau()
    x_test = np.arange(250) + 50  # start interpolation at x = 50
    z_test = savex_env.z_vla

    lin_taus = spherical_delays.linear_delays(x_test)
    X, Z = np.meshgrid(x_test, z_test)
    samps = np.array((X.flatten(), Z.flatten())).T
    ode_taus = np.reshape(tau_ier(np.array(samps)), (X.shape)).T


    fig, ax = plt.subplots()
    ax.plot(x_test, ode_taus)

    ode_taus = ode_taus - ode_taus[:, -1][:, None]


    fig, ax = plt.subplots()
    ax.plot(x_test, lin_taus)

    ax.set_prop_cycle(None)
    ax.plot(x_test, ode_taus, '--')

    plt.show(block=False)
