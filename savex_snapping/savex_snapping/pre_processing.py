import numpy as np
import load_sio
import scipy.signal as dsp
import savex_env

class PreProcess:
    """Load and pre-process data for snap detection"""
    def __init__(self, filename):
        """load data and save preprocessed timeseries"""
        self.filename = filename

    def __call__(self, tstart, dt):
        """Load specified data"""
        self.dt = dt
        adata = self._load(tstart)
        timeseries, taxis = self._preprocess(adata, tstart)
        return timeseries, taxis

    @property
    def tstart(self):
        """Start of the time series, not the same as input tstart"""
        return self.taxis[0]


    def _load(self, tstart):
        """Load raw data"""
        fs = savex_env.fs
        # setup time of interest in samples
        n_start = int(tstart * fs)
        dn = int(self.dt * fs)
        dn = int(2 ** np.ceil(np.log2(dn)))
        # load data
        adata = np.array(load_sio.load_selection(self.filename, n_start,
                                                dn, -1)['data'].T,
                         dtype=np.float_)
        # convert to pressure units
        adata = savex_env.record_to_pascal(adata)
        return adata

    def _preprocess(self, adata, tstart):
        """High pass filter"""
        fs = savex_env.fs
        adata = adata - np.mean(adata, axis=-1)[:, None]
        lp = dsp.firwin(1025, 5e3, pass_zero=False, nyq=fs / 2)
        adata = np.array([dsp.fftconvolve(f, lp, 'valid') for f in adata])
        taxis = tstart + np.arange(adata.shape[1]) / fs\
                + (lp.size / 2 - 1) / fs
        #taxis = self.tstart + np.arange(adata.shape[1]) / fs
        return adata, taxis

if __name__ == '__main__':
    from os.path import expanduser, join
    import matplotlib.pyplot as plt
    from matplotlib.gridspec import GridSpec

    home = expanduser("~")
    data_dir = join(home, 'data/SAVEX15/')
    filename = join(data_dir, 'RAVA3.15139122600.000.sio')
    # setup time of interest in samples
    tstart = 10  # sec
    dt = 3  # sec
    fs = savex_env.fs

    preprocess = PreProcess(filename)
    timeseries, taxis = preprocess(tstart, dt)
    rawdata = preprocess._load(tstart)
    rawt = np.arange(rawdata.shape[1]) / fs + tstart

    # Create FFTs
    NFFT = int(2 ** np.ceil(np.log2(rawdata.shape[1])))

    # fft of filtered data
    filt_ft = np.fft.rfft(rawdata, n=NFFT)
    raw_db = 20 * np.log10(np.abs(filt_ft))

    filt_ft = np.fft.rfft(timeseries, n=NFFT)
    filt_db = 20 * np.log10(np.abs(filt_ft))
    normer = np.max(filt_db, axis=-1)
    filt_db = filt_db - normer[:, None]
    raw_db = raw_db - normer[:, None]

    faxis = np.arange(NFFT / 2 + 1) / NFFT * fs

    fig, ax = plt.subplots()
    ax.plot(rawt, rawdata[-1, :], label='raw')
    ax.plot(taxis, timeseries[-1, :], label='filt')
    ax.set_xlabel('time, s')
    ax.set_ylabel('pressure')
    ax.set_title('Time series recorded on deepest channel')
    ax.grid()
    ax.legend()

    fig, ax = plt.subplots()
    ax.plot(faxis, raw_db[-1, :], label='raw')
    ax.plot(faxis, filt_db[-1, :], label='filt')
    ax.set_xlabel('freq, Hz')
    ax.set_ylabel('pressure')
    ax.set_title('Fourier transform of deepest channel')
    ax.set_ylim(-60, 2)
    ax.grid()
    ax.legend()

    plt.show(block=False)
