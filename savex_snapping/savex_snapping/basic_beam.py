import numpy as np
import matplotlib.pyplot as plt
from os.path import expanduser, join
import peakfinder, spherical_delays, pre_processing
from scipy.signal import hilbert

home = expanduser("~")
data_dir = home + '/Documents/SAVEX15/acoustic_data'
file_name = join(data_dir, 'RAVA3.15139122600.000.sio')

# data load specifications
t_start = 40  # seconds
dt = 0.1

#model width
reset_dist_left = 10
reset_dist_right = 30

# beamformer specifications
modelwidthfactor = 4  # expansion of model width
peak_threshold_dB = 110  # re 1 uPa
threshold = 10 ** (peak_threshold_dB / 20) * 1e-6

# load data
data_store = pre_processing.PreProcess(file_name)
adata, taxis = data_store(t_start, dt)

taxis = (taxis - t_start) * 1e3

# run thresholder code
test_data = np.abs(hilbert(adata, axis=0))

all_inds = []
for td in test_data:
    inds = peakfinder.model_arrivals(td, threshold, reset_dist_right, reset_dist_left)
    all_inds.append(inds)

plot_db = 20 * np.log10(test_data)

plot_i = 7
fig, ax = plt.subplots()
ax.plot(taxis, plot_db[plot_i])
ax.plot(taxis[all_inds[plot_i]], plot_db[plot_i, all_inds[plot_i]], '.')
ax.plot(taxis, np.ones_like(taxis) * (peak_threshold_dB - 120))
ax.set_title('Peak location after t = %.1f s'%t_start)
ax.set_ylabel('time, ms')
ax.set_ylabel('|pressure|, Pa')
ax.set_ylim(-20, 20)
ax.set_xlim(taxis[0] - 3, taxis[-1] + 3)
plt.show()
