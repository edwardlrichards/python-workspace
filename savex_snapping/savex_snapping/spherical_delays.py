import numpy as np
from scipy.integrate import quad
from scipy.interpolate import InterpolatedUnivariateSpline
import savex_env, eikonal_delays

# nominal sound speed
c = 1500
tau_ier = None

zrange = np.arange(savex_env.z_bottom + 100)
slow = InterpolatedUnivariateSpline(zrange, 1 / savex_env.ssp(zrange), ext=3)
slow_inter = slow.antiderivative()

def iso_delays(rs):
    """delays assuming a single sound speed value"""
    rs = np.array(rs, ndmin=1)
    taus = np.sqrt((savex_env.z_vla - savex_env.z_bottom) ** 2
                   + rs[:, None] ** 2) / c
    taus = taus - taus[:, -1][:, None]
    return taus

def iso_amplitude(rs):
    """delays assuming a single sound speed value"""
    rs = np.array(rs, ndmin=1)
    amp = 1 / np.sqrt((savex_env.z_vla - savex_env.z_bottom) ** 2
                       + rs[:, None] ** 2)
    amp = amp / amp[:, -1][:, None]
    return amp

def linear_delays(rs, zs=None, tilt=None):
    """delays assuming ray paths are linear, delays are integrated slowness"""
    rs = np.array(rs, ndmin=1)
    if zs is None:
        zs = np.array(savex_env.z_bottom, ndmin=1)
    else:
        zs = np.array(zs, ndmin=1)
    if tilt is None:
        tilt = np.array(0., ndmin=1)
    else:
        tilt = np.array(tilt, ndmin=1)
    xrcr = np.abs(savex_env.z_vla - savex_env.z_bottom)\
           * np.sin(tilt)[None, None, :, None]
    zrcr = -((savex_env.z_bottom - savex_env.z_vla)\
             * np.cos(tilt)[None, None, :, None] - savex_env.z_bottom)
    theta = np.arctan2(zs[None, :, None, None] - zrcr,
                       rs[:, None, None, None] - xrcr)
    i_slow = slow_inter(zs)[None, :, None, None] - slow_inter(zrcr)
    taus = i_slow / np.sin(theta)
    return np.squeeze(taus)

def ode_delays(rs, tau_ier=tau_ier):
    """delays from eikonal equation"""
    if eikonal_delays.tau_ier is None:
        eikonal_delays.tau_ier = eikonal_delays.get_tau()
    rs = np.array(rs, ndmin=1)
    r = np.sqrt((savex_env.z_vla - savex_env.z_bottom) ** 2 + rs[:, None] ** 2)
    X, Z = np.meshgrid(rs, savex_env.z_vla)
    taus = eikonal_delays.tau_ier(np.array((X.flatten(), Z.flatten())).T)
    taus = np.reshape(taus, r.T.shape).T
    taus = taus - taus[:, -1][:, None]
    return taus

if __name__=='__main__':
    import matplotlib.pyplot as plt

    # test for a small number of ranges
    r_test = np.array([50, 100, 150, 200])

    tau_c = iso_delays(r_test)
    tau_cz = linear_delays(r_test)
    tau_cz = tau_cz - tau_cz[:, -1][:, None]
    tau_ode = ode_delays(r_test)

    fig, ax = plt.subplots()
    for r, t in zip(r_test, tau_c):
        plt.plot(t * 1e3, savex_env.z_vla, '*', markerfacecolor='None',
                 label='%i'%r)

    ax.set_prop_cycle(None)

    for r, t in zip(r_test, tau_cz):
        plt.plot(t * 1e3, savex_env.z_vla, 'o', markerfacecolor='None')

    ax.set_prop_cycle(None)

    for r, t in zip(r_test, tau_ode):
        plt.plot(t * 1e3, savex_env.z_vla, '.')

    ax.set_ylim(90, 20)
    ax.grid()
    ax.set_title('Spherical spreading delays')
    ax.set_xlabel('delay, ms')
    ax.set_ylabel('depth, m')

    # test for a small number of depths
    r_test = 75
    z_test = np.array([104, 102, 100, 98, 96])

    tau_cz = linear_delays(r_test, zs=z_test)
    tau_cz = tau_cz - tau_cz[:, 0][:, None]

    fig, ax = plt.subplots()
    for r, t in zip(z_test, tau_cz):
        plt.plot(t * 1e3, savex_env.z_vla, 'o', markerfacecolor='None')

    ax.set_ylim(90, 20)
    ax.grid()
    ax.set_title('Spherical spreading delays')
    ax.set_xlabel('delay, ms')
    ax.set_ylabel('depth, m')

    plt.show(block=False)
