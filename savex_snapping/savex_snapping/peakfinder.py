"""reset threshold peak detector"""
import numpy as np

def model_arrivals(y, thres, reset_dist_right, reset_dist_left):
    """Peak detection routine.

    Parameters
    ----------
    y : ndarray
        1D amplitude data to search for peaks.
    tail_width : int
        number of indices before first arrival and after last
    thres : float
        Only the peaks with amplitude higher than the
        threshold will be detected.
    reset_dist : int
        Minimum distance between excedences before they are considered 2 peaks.

    Returns
    -------
    inds : ndarray
        indicies of non-zero values
    vals : ndarray
        model values at inds points
    """
    y = np.squeeze(y)
    above_i = np.where(y > thres)[0]
    sort_i = np.argsort(y[above_i])[: : -1]

    above_i = above_i[sort_i]
    sort_y = y[above_i]

    is_tooclose = np.zeros(sort_i.size, np.bool_)

    detect_i = []
    for i, (si, sy) in enumerate(zip(above_i, sort_y)):
        if not is_tooclose[i]:
            detect_i.append(si)
            dead_time = above_i - si
            is_left = np.bitwise_and(dead_time < 0,
                                     dead_time >= -reset_dist_left)
            is_right = np.bitwise_and(dead_time > 0,
                                     dead_time <= reset_dist_right)
            is_dead = np.bitwise_or(is_left, is_right)
            is_tooclose = np.bitwise_or(is_tooclose, is_dead)
    return detect_i


if __name__=='__main__':
    import matplotlib.pyplot as plt
    from os.path import expanduser, join
    import spherical_delays, pre_processing

    home = expanduser("~")
    data_dir = home + '/Documents/SAVEX15/acoustic_data'
    file_name = join(data_dir, 'RAVA3.15139122600.000.sio')

    # data load specifications
    t_start = 40  # seconds
    dt = 0.1

    #model width
    tail_width = 20
    reset_dist = 100

    # beamformer specifications
    modelwidthfactor = 4  # expansion of model width
    peak_threshold_dB = 110  # re 1 uPa
    threshold = 10 ** (peak_threshold_dB / 20) * 1e-6

    # load data
    data_store = pre_processing.PreProcess(file_name)
    adata, taxis = data_store(t_start, dt)
    taxis = (taxis - t_start) * 1e3

    # run thresholder code
    test_data = np.abs(adata[0, :])

    inds = model_arrivals(test_data, tail_width, threshold, reset_dist)

    fig, ax = plt.subplots()
    ax.plot(taxis, test_data)
    ax.plot(taxis[inds], test_data[inds], '.')
    ax.plot(taxis, np.ones_like(taxis) * threshold)
    ax.set_title('Peak location after t = %.1f s'%t_start)
    ax.set_ylabel('time, ms')
    ax.set_ylabel('|pressure|, Pa')
    plt.show()
