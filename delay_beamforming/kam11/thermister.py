import numpy as np
import pandas as pd
import datetime as d
import pytz
import os.path as path
import pickle
import gsw
""" Load thermister data from KAM11. WHOI and MPL thermisters. """

tempStringDir = '/Users/edwardlrichards/Documents/data/KAM11/enviornment/' +\
    'WHOI_Temp_String/TemperatureString'


def getSoundSpeed(timeOfInterest, array='WHOI'):
    """Load thermister data at time of interest and convert to sound speed

    timeOfInterest is a date time object
    """
    # TODO:impliment MPL thermister arrays
    if array == 'WHOI':
        ts = WHOI_TS().WHOIString
    else:
        raise Exception('Not Implimented')
    ssConverter = SoundSpeedFromTemp()
    ssp = ssConverter(ts.ix[ts.index.asof(timeOfInterest)])
    ssp = pd.Series(np.squeeze(np.array(ssp)), index=ssp.columns)
    return ssp


class WHOI_TS:
    """ Load and access data from WHOI TS. Pickle is used when avalible """

    def __init__(self, fileDir=tempStringDir):
        """ load data from pickle if possible, if not, load from data files """
        pickleFile = 'processedData/WHOI_Temp_String.pic'
        self.fileDir = fileDir
        self.pickleFile = path.join(path.dirname(__file__), pickleFile)

        try:
            self.WHOIString = pd.read_pickle(self.pickleFile)
        except FileNotFoundError:
            self.WHOIString = self.loadWHOIString()

    def loadWHOIString(self):
        """ Load WHOI string from instrument recorded data files """
        # Create a common time axis for data
        UTC = pytz.utc
        start_time = d.datetime(2011, 6, 24, 6, 0, 0, tzinfo=UTC)
        end_time = d.datetime(2011, 7, 11, 19, 45, 0, tzinfo=UTC)
        dtimeI = pd.date_range(start_time, end_time, freq='15s')
        # load data from both instrument arrays
        seabirdData = self._loadSeabirdString()
        starmonData = self._loadStarmonString()
        WHOIString = pd.merge(seabirdData, starmonData, left_index=True,
                              right_index=True, how='outer')
        # Resample to 15 second sampling
        WHOIString = WHOIString.resample('15S', how='mean')
        # solrt columns by depth
        columns = WHOIString.columns
        columnIndex = np.argsort(columns)
        WHOIString = WHOIString.iloc[:, columnIndex]
        # Save load results
        WHOIString.to_pickle(self.pickleFile)
        return WHOIString

    def _removeDroppedSensors(self):
        """ Both seabird sensors dropped at some point """
        # Seabird 1770 dropped at some point.
        # This is clear in pressure record
        if name == '1770':
            cutoffTime = d.datetime.strptime('2011-07-03 11:54:30',
                                             '%Y-%m-%d %H:%M:%S')
            fileData.ix[cutoffTime:, depth] = np.nan

    def _loadStarmonString(self):
        """ loop through all starmon files and load data """
        # starmon processing info
        starmonName = ('T3866', 'T3869', 'T3871', 'T3915', 'T3917', 'T3920',
                       'T3924', 'T3865', 'T3867', 'T3870', 'T3914', 'T3916',
                       'T3918', 'T3922')
        starmonDepths = np.array(
            (35, 90, 25, 85, 45, 50, 55, 40, 80, 20, 60, 70, 65, 30))
        # datetime parsing specification
        parseStarmon = lambda x: d.datetime.strptime(x, '%d.%m.%y %H:%M:%S')
        # load loop
        tempData = None
        for name, depth in zip(starmonName, starmonDepths):
            columnNames = ['index', 'datetime', depth]
            # One instrument has a strange name
            if name == 'T3915':
                loadString = path.join(self.fileDir, name, '2' + name + '.DAT')
            else:
                loadString = path.join(self.fileDir, name, '1' + name + '.DAT')
            # Load data
            fileData = pd.read_table(loadString, comment='#',
                                     names=columnNames,
                                     date_parser=parseStarmon,
                                     decimal=',', parse_dates=[1], index_col=1)
            # starmon loggers are exactly one hour fast
            fileData.index = fileData.index.shift(-1, 'H')

            del fileData['index']

            if tempData is None:
                tempData = fileData
            else:
                tempData = pd.merge(tempData, fileData, left_index=True,
                                    right_index=True, how='outer')

        return tempData

    def _loadSeabirdString(self):
        """ Loop through all seabird files and load data """
        # seabird processing information
        seabirdName = ('1770', '4079')
        seabirdFolder = 'SBE37'
        seabirdDepths = np.array((10, 75))
        # seabird date string parser

        def parseSeabird(day, tod):
            if isinstance(day, float):
                return None
            datetime = d.datetime.strptime(day + tod, ' %d %b %Y %H:%M:%S')
            return datetime

        tempData = None
        for name, depth in zip(seabirdName, seabirdDepths):
            loadString = path.join(self.fileDir, seabirdFolder, name + '.asc')
            columnNames = [depth, 'pressure', 'conductivity', 'day', 'tod']

            fileData = pd.read_csv(loadString, comment='*',
                                   parse_dates={'datetime': ['day', 'tod']},
                                   date_parser=parseSeabird, names=columnNames,
                                   index_col='datetime')

            fileData = fileData.ix[pd.notnull(fileData.index)]

            del fileData['pressure']
            del fileData['conductivity']

            if tempData is None:
                tempData = fileData
            else:
                tempData = pd.merge(tempData, fileData, left_index=True,
                                    right_index=True, how='outer')
        return tempData


class SoundSpeedFromTemp:
    """ Class that takes a DataFrame of temperature give soundspeed """

    def __init__(self):
        self.interpPoints = np.array((0, 70, 120))
        self.interpValues = np.array((35.05, 35.3, 35.3))
        self.lat = 22
        self.lon = -159

    def __call__(self, tempDF, extrapolate=True, maxDepth=100):
        """ convert tempurature to sound speed, assume salinity
        uses nominal pressure values based on depth
        """
        # Pressure is columns if DF, make this so if series
        try:
            pressure = tempDF.columns.values
        except AttributeError:
            pressure = tempDF.index.values
            tempDF = pd.DataFrame([tempDF.values], columns=pressure)
        # Extrapolate measurements to top and max depth, assuming constant
        # properties.
        if extrapolate:
            tempDF = tempDF.T
            newIndex = np.array(tempDF.index)
            newIndex = np.insert(newIndex, 0, 0)
            newIndex = np.append(newIndex, 100)
            newIndex = pd.Index(newIndex)
            tempDF = tempDF.reindex(newIndex).bfill()
            tempDF = tempDF.T
        interpLine = np.interp(tempDF.columns.values, self.interpPoints,
                               self.interpValues)
        abSal = gsw.SA_from_SP(interpLine, tempDF.columns.values,
                               self.lon, self.lat)
        conTemp = gsw.CT_from_t(abSal, tempDF.values, tempDF.columns.values)
        gsw_SSP = gsw.sound_speed(abSal, conTemp, tempDF.columns.values)
        SS_FromTemp = pd.DataFrame(gsw_SSP, index=tempDF.index,
                                   columns=tempDF.columns)
        if extrapolate:
            SS_FromTemp = SS_FromTemp.T.ffill().T

        return SS_FromTemp

    def getSalinityProfile(self):
        """ return a 1 meter sampled salinity profile for plotting """
        zTest = np.arange(np.max(self.interpPoints + 1))
        salInterp = np.interp(zTest, self.interpPoints, self.interpValues)
        return (zTest, salInterp)
