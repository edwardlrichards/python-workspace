import numpy as np
import pandas as pd
import xarray as xr
import pytz
import datetime as dt
import os
""" Central repository for KAM11 measurement metadata """
fs = 1e5


class LFM_Info:
    """Information relevent to SIO LFM probe signals
    """

    def __init__(self):
        self.fc = 23000
        self.bw = 23000
        self.startingSample = 48000
        self.repetitionSamples = 9600
        self.dutyCycleSamples = 4800


class CTD_Info:
    """ time and location for CTD casts """

    def __init__(self):
        self.ID = np.arange(12) + 1

    def timeUTC(self):
        """ return cast times as a pandas data frame """
        UTC = pytz.timezone('UTC')
        ctdTimes = ['Jun 24 2011 19:48:48', 'Jun 24 2011 23:33:12',
                    'Jun 25 2011 19:22:14', 'Jun 28 2011 16:54:47',
                    'Jul 03 2011 20:45:32', 'Jul 03 2011 21:37:50',
                    'Jul 04 2011 02:30:58', 'Jul 04 2011 03:05:38',
                    'Jul 06 2011 05:27:15', 'Jul 06 2011 06:30:11',
                    'Jul 06 2011 07:23:32', 'Jul 11 2011 06:42:49']
        ctdTimes = [UTC.localize(dt.datetime.strptime
                                 (t, '%b %d %Y %H:%M:%S')) for t in ctdTimes]
        ctdTimes = pd.Series(ctdTimes, index=self.ID)
        return ctdTimes

    def timeHST(self):
        """ return time of casts in local time """
        HST = pytz.timezone('US/Hawaii')
        ctdTimes = self.timeUTC()
        localTimes = [HST.localize(t) for t in ctdTimes]
        localTimes = pd.Series(localTimes, index=ctdTimes.index)
        return localTimes

    def location(self):
        """ Taken from ships position logs """
        ctdLocations = [(21.278697, -157.882938),
                        (22.135533, -159.809152),
                        (22.131061, -159.810719),
                        (22.136903, -159.809184),
                        (22.136232, -159.809722),
                        (22.151387, -159.800423),
                        (22.170438, -159.787152),
                        (22.180298, -159.775125),
                        (22.146672, -159.802379),
                        (22.136630, -159.811748),
                        (22.152987, -159.802898),
                        (22.152929, -159.803438)]

        ctdLocations = pd.DataFrame(ctdLocations, columns=['lat', 'lon'],
                                    index=self.ID)
        return ctdLocations


class MooringInfo:
    """ Mooring location information """

    def __init__(self):
        """ mooring data """
        filePath, _ = os.path.split(__file__)
        self.loadName = os.path.join(filePath, 'processedData/moorings.pic')
        self.info = self.loadMoorings()

    def loadMoorings(self):
        """ Copied and pasted from csv file taken from mooring Excel file """
        mooringInfo = pd.read_pickle(self.loadName)
        return mooringInfo


class VLA1:
    """ Closest vertical line array """

    def __init__(self, timeOfInterest=None):
        self.stationID = '8'
        # get water depth at mooring
        mi = MooringInfo().info
        self.waterDepth = mi.ix[self.stationID, 'depth']
        if timeOfInterest is not None:
            self.deploymentNum = self.getDeploymentNum(timeOfInterest)
        else:
            self.deploymentNum = 2
        self.fileString = 'AAS4.11%j%H%M00.000.sio'
        self.channelNames = np.arange(16) + 1
        self.sourceRange = 3e3

    def phoneDepths(self, channelNames=None, isDepth=True):
        """ phone depth varied by 1m between deployments [1,2] and 3
        if not isDepth, return height
        """
        if channelNames is None:
            channelNames = self.channelNames
        height = [8.6, 12.35, 16.1, 19.85, 23.6, 27.35, 31.1, 34.85, 38.6, 42.35,
                  46.1, 49.85, 53.6, 57.35, 61.1, 64.85]
        if self.deploymentNum == 3:
            height += 1
        # height is sometime useful too
        if isDepth:
            allDepths = pd.Series(self.waterDepth - height,
                                  index=self.channelNames)
        else:
            allDepths = pd.Series(height, index=self.channelNames)
        return allDepths[channelNames]

    def phonePosition(self, tiltAngle=0, channelNames=None):
        """return a tuple of values, receiver depth and range, m
        """
        phoneHeight = self.phoneDepths(channelNames, isDepth=False)
        # Trigonometric calculations to return phone position
        rr = self.sourceRange + phoneHeight * np.sin(np.radians(tiltAngle))
        rd = self.waterDepth - phoneHeight * np.cos(np.radians(tiltAngle))
        phonePosition = pd.concat([rd, rr], axis=1)
        phonePosition.columns = ['depth', 'range']
        return phonePosition

    def getDeploymentNum(self, timeOfInterest):
        """ deployment times from excel document """
        d1 = '2011 J177 19:45'
        d2 = '2011 J182 04:37'
        d3 = '2011 J186 01:20'
        dE = '2011 J191 17:45'
        fmt = '%Y J%j %H:%M'
        d1 = dt.datetime.strptime(d1, fmt)
        d2 = dt.datetime.strptime(d2, fmt)
        d3 = dt.datetime.strptime(d3, fmt)
        dE = dt.datetime.strptime(dE, fmt)
        if (timeOfInterest > d1) & (timeOfInterest <= d2):
            deploymentNum = 1
        elif (timeOfInterest > d2) & (timeOfInterest <= d3):
            deploymentNum = 2
        elif (timeOfInterest > d3) & (timeOfInterest <= dE):
            deploymentNum = 3
        else:
            raise Exception('VLA1NotInWater')
        return deploymentNum


class VLA2:
    """ Furthest vertical line array """

    def __init__(self):
        self.stationID = '16'


class SRA:
    """ moored acoustic source array """

    def __init__(self):
        self.stationID = '2'

    @property
    def sd(self):
        height = [31.62, 39.12, 46.62, 54.12, 61.62, 69.12, 76.62, 84.12]
        return xr.DataArray(height[::-1], coords = [np.arange(len(height)) + 1], dims=['channel'])
