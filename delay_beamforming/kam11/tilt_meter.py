import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seawater as sw
from kam11 import context
from helper import config
import datetime as dt

ENV_FOLDER = config.enviornment_dir()
TILT_FOLDER = '/tiltmeter/tiltMeter01_VLA1/VLA1_txt/'
FILE_PREFIX = '0111'

def load_file(jday):
    load_name = FILE_PREFIX + str(jday)
    load_file = os.path.join(ENV_FOLDER + TILT_FOLDER, load_name)
    data = pd.read_table(load_file, skiprows=1, delim_whitespace=True)

    time_start = dt.datetime.strptime('2011 J%i 00:00'%jday, '%Y J%j %H:%M')
    time_difference = pd.to_timedelta(data['seconds'], unit = 's')
    time_series = time_start + time_difference

    data.drop('count', 1, inplace=True)
    data.drop('seconds', 1, inplace=True)
    data.index = time_series
    data = inline_tilt(data)
    return data

def inline_tilt(vla_data, array='VLA1'):
    """add a column to data of tilt angle between source and receiver"""
    if array == 'VLA1':
        stationID = context.VLA1().stationID
    elif array == 'VLA2':
        stationID = context.VLA2().stationID
    receiveID  = context.SRA().stationID
    moorings = context.MooringInfo().info
    lat = [moorings.ix[stationID, 'lat'],
                    moorings.ix[receiveID, 'lat']]
    lon = [moorings.ix[stationID, 'lon'],
                    moorings.ix[receiveID, 'lon']]
    _, bearing = sw.dist(lat, lon)
    print(bearing)
    tilt_diff = bearing - vla_data['compass']
    inplane = vla_data['tilt'] * np.cos(np.radians(tilt_diff))
    vla_data['inplane'] = inplane
    return vla_data

def plot_column(vla_column):
    """default plotting method for tilt sensor"""
    date_formatter = mdates.DateFormatter('%H:%M')
    y = vla_column.values
    fig, ax = plt.subplots()
    line = ax.plot(vla_column.index, y)
    _ = ax.xaxis.set_major_formatter(date_formatter)
    xLabel = ax.set_xlabel('Time of day, UTC')
    ylabel = ax.set_ylabel('Tilt, degrees')
    #xlim = ax.set_xlim(xlim)
    #ylim = ax.set_ylim(ylim)
    fig.autofmt_xdate()
    fig.canvas.draw()
    return fig, ax

if __name__ == "__main__":
    jday = 184
    data = load_file(jday)
    plot_column(data['inplane'])
    plt.title('Tilt in plane of receiver, J%i'%jday)
    plt.show(block=False)
