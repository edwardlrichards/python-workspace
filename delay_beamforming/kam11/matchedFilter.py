import numpy as np
from xarray import DataArray, concat
from pandas import Index
import datetime as dt
import os
from sioLoad import loadSIO
from kam11 import context
from helper import baseband, config, passband
from scipy.signal import fftconvolve, remez
import concurrent.futures

def mf_bytime(loadtime, load_duration=None, topassband=True, tobaseband=False):
    """convience function to return match filter result

    Parameters:
    loadtime: datetime object of start time of loading
    load_duration: timedelta object of duration of loading
    topassband has precidence over tobaseband
    """
    lfm_mf = LFM_MatchedFilter(loadTime=loadtime, loadDuration=load_duration)
    mf_ts = lfm_mf.matchedFilter(topassband=topassband, tobaseband=tobaseband)
    return mf_ts

class MatchedFilter:
    """data structure for the result of a matched filter on data"""

    def __init__(self, mf_dict):
        self.mf_dict = mf_dict

    def __getitem__(self, index):
        """support of list type indexing"""
        #recursively special case slicing
        if isinstance(index, slice):
            return self.__class__(self[x]
                                  for x in xrange(*index.indices(len(self))))
        #check index, dealing with negative indicies too
        if not isinstance(index, int): raise TypeError
        if index<0: index+=len(self)
        if not(0<=index<len(self)): raise IndexError
        #index is now a correct int, within range(len(self))
        return self._single_index(index)


    def __len__(self):
        """number of matched filter measurements"""
        return self.mf_dict['num_samples']

    @property
    def rd(self):
        """ receiver depths """
        return self.mf_dict['rd']

    @property
    def data(self):
        """return an iterator for all channels"""
        return (self.mf_dict[chan] for chan in self.mf_dict['channelNames'])

    @property
    def time(self):
        """time of record"""
        return self.mf_dict['time']

    def channels(self, channels_ofinterest):
        """return only selected channels"""
        return (self.mf_dict[chan] for chan in channels_ofinterest)

    def single_channel(self, channelName):
        """Return result for a single channel"""
        return self.mf_dict[channelName]

    def _single_index(self, index):
        """return a DataFrame of time series on all channels, at int index"""
        speced_data = [chan[index, :] for chan in self.data]
        da = concat(speced_data, Index(self.rd, name='depth'))
        #keep timestamp in attrs
        da = da.drop('slowtime')
        da.attrs['timestamp'] = self.mf_dict['time'] +\
            dt.timedelta(0, self.mf_dict['mf_dt'][index])
        return da


class LFM_MatchedFilter:
    """ Load an SIO file and match filter it """

    def __init__(self, loadTime, loadDuration=None):
        """ set up the file to be loaded

        loadTime is a date time
        loadDuration is a time delta, no more than 1 file is loaded at a time.
        loadChannels is an np array of 1 indexed channel numbers to be loaded
        Transmissions are windowed to a 2.4 - 55.9 seconds, active measurement
        """
        acousticDir = config.acoustic_dir()
        probe_dir = config.probe_dir()
        #TODO:Choose between LFM and MSeq
        probe_file='TX1W_S02_N01_R09_TR_V01.sio'

        self.fs = context.fs
        # use VLA1
        self.vlaInfo = context.VLA1()
        self.lfmInfo = context.LFM_Info()

        loadName = loadTime.strftime(self.vlaInfo.fileString)
        fileName = os.path.join(acousticDir, loadName)

        self.loadChannels = self.vlaInfo.channelNames

        self.fileInfo = loadSIO.load_header(fileName,
                                            channelNames=self.loadChannels)
        # create a memory map of the file, if it exists
        self.memMap = loadSIO.createSIOMap(self.fileInfo)
        probesignal = os.path.join(probe_dir, probe_file)
        self.probesignal = loadSIO.load_selection(probesignal,
                                                  self.lfmInfo.startingSample,
                                                  self.lfmInfo.dutyCycleSamples,
                                                  [0])['data']
        self.loadTime = loadTime
        self.setLoadSamples(loadDuration)

    def matchedFilter(self, topassband=True, tobaseband=False):
        """load data from sio file and match filter it"""
        matchedFilter = self._mf_ts()
        # Acount for filter's delay in timestamp
        startTime = self.loadTime +\
            dt.timedelta(0, (self.lfmInfo.dutyCycleSamples / 2) / self.fs)

        # baseband if necassary
        if topassband:
            matchedFilter = passband.hilbert_pb(matchedFilter)
        elif tobaseband:
            matchedFilter = baseband.baseband(matchedFilter)

        # set 0 time on most energetic tap
        matchedFilter = self._centerCIR_Energy(matchedFilter, startTime)

        return matchedFilter

    def _mf_ts(self):
        # TODO:track absolute time
        numSamples = self.endSample - self.startSample
        # channelNames have 1 subtracted to make 0-indexed

        rawData = loadSIO.loadFromMap(self.memMap, self.fileInfo,
                                      self.startSample, numSamples,
                                      self.loadChannels - 1)['data']
        filterTaps = np.array(np.flipud(self.probesignal))
        matchedFilter = []
        chanFilter = lambda channel: fftconvolve(channel, filterTaps,
                                                 mode='valid')

        rawData = rawData.T
        rawData=np.array(rawData, ndmin=2)
        with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
            matchedFilter = executor.map(chanFilter, rawData)
        #matchedFilter = fftconvolve(rawData.T, filterTaps, mode='valid')
        matchedFilter = np.array(list(matchedFilter))
        taxis = np.arange(matchedFilter.shape[1]) / self.fs
        depth = self.vlaInfo.phoneDepths(channelNames=self.loadChannels)
        matchedFilter = DataArray(matchedFilter, dims=['depth','time'],
                                       coords=[depth, taxis])

        return np.squeeze(matchedFilter)

    def _centerCIR_Energy(self, data, startTime):
        """find index with most energy, and center CIR on it"""
        data = np.array(data, ndmin=2)
        # only center when we have more than 2 chirps of data
        repRate = self.lfmInfo.repetitionSamples
        if data.shape[1] < 2 * repRate:
            return data

        # prefixLength is samples of CIR that will be before max energy index
        prefixLength = int(np.floor(repRate / 4))
        numStacks = int(np.floor(data.shape[1] / repRate))

        # fortran order reshapes columns first
        stackedData = np.reshape(data[:, :numStacks * repRate], (-1, repRate))
        totalEnergy = np.sum(np.abs(stackedData)**2, axis=0)
        splitIndex = np.argmax(totalEnergy)

        # Make sure stack starts on a positive index
        startIndex = splitIndex - prefixLength
        if startIndex < 0:
            startIndex += repRate

        # Make sure stack ends before max number of samples
        endIndex = startIndex + numStacks * repRate
        if endIndex > (data.shape[0]):
            endIndex -= repRate

        def stackFunction(d, startTime=startTime):
            """make stacked rows into a dataframe"""
            stackRow = np.reshape(d[startIndex:endIndex], (-1, repRate))
            t = (np.arange(stackRow.shape[1]) - prefixLength) / self.fs
            #time is in ms
            t *= 1e3
            slow_start = startTime.second + startTime.microsecond / 1e6
            slow_time = np.arange(stackRow.shape[0]) * (repRate / self.fs)
            slow_time += slow_start
            result = DataArray(stackRow, coords=[slow_time, t],
                               dims=['slowtime', 'time'])
            #Window data to only return active transmission times
            result = result.sel(slowtime=slice(2.4, 55.9))
            return result


        stackMF = [stackFunction(d) for d in data]
        mf_result = {}

        # run filter on each channel
        for mf, chan in zip(stackMF, self.loadChannels):
            mf_result[chan] = mf

        # save result as DataArray
        mf_result['time'] = startTime
        mf_result['rd'] = self.vlaInfo.phoneDepths(
            channelNames=self.loadChannels)
        mf_result['channelNames'] = self.loadChannels
        mf_result['mf_dt'] = np.array(stackMF[0].coords['slowtime'])
        mf_result['num_samples'] = stackMF[0].shape[0]

        #save resulting dictionary in a custom data class
        mf = MatchedFilter(mf_result)
        return mf

    def setLoadSamples(self, loadDuration):
        """ calculate the sampes to load from a file
        """
        # load full minute if loadDuration does not exist, or is more than 1
        # minute
        if (loadDuration is None) or (loadDuration > dt.timedelta(0, 60)):
            loadDuration = dt.timedelta(0, 60)
        # Load all channels if no channels are specified
        self.startSample = self.loadTime.second * self.fs
        endTime = (self.loadTime + loadDuration)
        minStart = self.loadTime -\
            dt.timedelta(seconds=self.loadTime.second,
                         microseconds=self.loadTime.microsecond)
        # full file load
        if (endTime - self.loadTime).seconds >= 60:
            self.endSample = self.fileInfo['samplesPerChannel'] - 1
        # samples per file overflow
        elif (endTime - minStart).seconds >= 60:
            self.endSample = self.fileInfo['samplesPerChannel'] -\
                self.startSample - 1
        else:
            self.endSample = np.floor((endTime.second +
                                       (endTime.microsecond / 1e6)) * self.fs)
        self.startSample = int(self.startSample)
        self.endSample = int(self.endSample)
