import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
from kam11 import time_beamforming, matchedFilter, positionPertubation
from plotting import beam_plot, mf_plot

toi = dt.datetime.strptime('2011 J184 03:51:15', '%Y J%j %H:%M:%S')

#Create matchedFilter CIR estimate
loadDuration = dt.timedelta(0, 5)
cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
mf=cirEstimate.matchedFilter()

#initilize beamformer with subchannels
subchannels = mf.rd[3:8]
(bf_er, wf) = time_beamforming.load_beamformer(toi, channels=subchannels)


tbounds = (-8, -2)
abounds = (-8, 8)

t_x = np.r_[min(tbounds): max(tbounds): 300j]
a_x = np.r_[min(abounds): max(abounds): 301j]

bf = bf_er(t_x, wf(a_x))

#Solve for optimal delays for both up and downgoing wavefronts
#upgoing
t1 = (-4, -2.5)
a1 = (-7, -5)

max_er = positionPertubation.best_arrival(bf_er, wf)
bf_1 = bf_er(np.r_[min(t1): max(t1): 300j],
             wf(np.r_[min(a1): max(a1): 301j]))
tau_1 = max_er(t1, a1)
res_1 = tau_1 -  wf(np.r_[min(a1): max(a1): 301j])
res_1 = res_1.rename({'depth': 'depth', 'angle': 'angle_1'})

#downgoing
t2 = (-6, -4)
a2 = (5, 7.5)

bf_2 = bf_er(np.r_[min(t2): max(t2): 300j],
             wf(np.r_[min(a2): max(a2): 301j]))
tau_2 = max_er(t2, a2)
res_2 = tau_2 -  wf(np.r_[min(a2): max(a2): 301j])
res_2 = res_2.rename({'depth': 'depth', 'angle': 'angle_2'})

res_abs = (np.abs(res_1 - res_2)).sum('depth')
fig, ax = plt.subplots()
res_abs.min('angle_1').plot(ax)
ax.set_ylabel('sum of absolute of delay mismatch')
ax.set_title('Minimum error for each arrival angle')

fig, ax = plt.subplots()
res_abs.min('angle_2').plot(ax)
ax.set_title('MSE of array mismatch')
ax.set_ylabel('sum of absolute of delay mismatch')
ax.set_title('Minimum error for each arrival angle')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds = (-8, -1), channel_delays=tau_1)
ax.set_title('Beamforming result for upgoing wave')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds = (-8, -1), channel_delays=tau_2)
ax.set_title('Beamforming result for downgoing wave')

fig, ax = plt.subplots()
res_abs.plot(ax)
ax.set_title('sum of absolute array mismatch')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds=t1, channel_delays=tau_1)
_ = ax.set_title('optimally lineup of upgoing wave')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds=t2, channel_delays=tau_2)
_ = ax.set_title('optimally lineup of downgoing wave')

fig, ax = plt.subplots()
tau_1.plot(ax)
wf(-5.15).plot(ax)
ax.set_title('upgoing wave mismatch')

fig, ax = plt.subplots()
tau_2.plot(ax)
wf(5.15).plot(ax)
ax.set_title('downgoing wave mismatch')

fig, ax = mf_plot.plotMultipleChannel(mf, plotChannels=subchannels.index,
                                      tStart=-8, tEnd=2)

fig, ax = beam_plot.plot_grid(bf, drange=30)
_ = ax.set_title('sub channel beamformer')

fig, ax = beam_plot.plot_grid(bf_1)
_ = ax.set_title('sub channel beamformer, upgoing wave')

fig, ax = beam_plot.plot_grid(bf_2)
_ = ax.set_title('sub channel beamformer, downgoing wave')

plt.show(block=False)
