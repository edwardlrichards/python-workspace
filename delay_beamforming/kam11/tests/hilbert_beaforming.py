import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import context, matchedFilter, thermister
from plotting import mf_plot, beam_plot
from beamforming import basebanded, curvedWaveFront, timeDomain, beam_correlations
import datetime as dt
import scipy.signal as sig

look_time = '2011 J184 03:51:15'

#def main():
"""load KAM11 data and beamform with it"""
loadDuration = dt.timedelta(0 , 5)
angebounds = (2 , 8)
numangles = 300
timebounds = (-6, -2)  # ms
numtimes = 301
snapshotIndex = 10  # snapshot used, by location in array
upRate = 10
toi = dt.datetime.strptime(look_time, '%Y J%j %H:%M:%S')

#load signal information from kam11 context
fc = context.LFM_Info().fc
bw = context.LFM_Info().bw
fs = context.fs

#Create matchedFilter CIR estimate
cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
mf=cirEstimate.matchedFilter(tobaseband=False)

#Context of measurements
ssp=thermister.getSoundSpeed(toi)
rd=context.VLA1().phoneDepths()
wf_delays = curvedWaveFront.WavefrontDelays(ssp, rd, z0 = rd[1])

#create timeseries from a single index
ts = mf.single_index(snapshotIndex)

#beamform with timeseries
look_angle = np.r_[angebounds[0] : angebounds[1] : 1j * numangles]
angle_delays = wf_delays(look_angle)
look_time = np.r_[min(timebounds) : max(timebounds) : 1j * numtimes]

#baseband beamforming with phase correction
bb_beamformer = basebanded.BasebandBeamfomer(ts, fs, fc, bw, upsample=10,
                                             tobaseband=True)
bb_beamforming = bb_beamformer(look_time, angle_delays)

#hilbert beamforming
h_ts = sig.hilbert(ts, axis=0)
ts_beamformer = basebanded.ShiftAndAdd(h_ts, taxis = ts.index, upsample=10)
ts_beamforming = ts_beamformer(look_time, angle_delays)

fig, ax = mf_plot.plotMultipleChannel(mf , vmin = -50, tStart = -9,
                                            tEnd = 5, norm = 'total')
peak_angle = beam_correlations.maxangle(ts_beamforming, (-5, -3,5), (5, 7))
one_angle = np.squeeze(np.array(wf_delays(peak_angle)))
#6.5 is the magical angle output by optimal delays
one_angle = np.squeeze(np.array(wf_delays(6.5)))

fig, ax = beam_plot.plot_ts(np.abs(h_ts), taxis=ts.index, tbounds=(-5, -3.5),
                            channel_delays=one_angle)

fig, ax = beam_plot.plot_grid(bb_beamforming, x = look_time,
                                  y = look_angle, drange = 15)
ax.set_title('Baseband beamforming result with phase correction' +
             '\ntime of snapshot: %s'%snaptime.strftime('J%j %H:%M'))
ax.set_xlabel('delay, s')
ax.set_ylabel('angle, degree')

fig, ax = beam_plot.plot_grid(ts_beamforming, x = look_time,
                                  y = look_angle, drange = 15)
ax.set_title('Hilbert Beamforming' +
             '\ntime of snapshot: %s'%snaptime.strftime('J%j %H:%M'))
ax.set_xlabel('delay, s')
ax.set_ylabel('angle, degree')

plt.show(block=False)
