import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import context, matchedFilter, thermister, positionPertubation
from plotting import mf_plot, beam_plot
from beamforming import curvedWaveFront, timeDomain, beam_correlations
from helper import passband
from atpy import bellhop, write_at
import datetime as dt
import scipy.signal as sig

look_time = '2011 J184 03:51:15'

# def main():
"""load KAM11 data and beamform with it"""
loadDuration = dt.timedelta(0, 5)
anglebounds = (-4, -8)
numangles = 300
tbounds = (-4, -2)  # ms
numtimes = 301
snapshotIndex = 10  # snapshot used, by location in array
uprate = 10
toi = dt.datetime.strptime(look_time, '%Y J%j %H:%M:%S')

# Create matchedFilter CIR estimate
cirEstimate = matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
mf = cirEstimate.matchedFilter(tobaseband=False)

# Context of measurements
ssp = thermister.getSoundSpeed(toi)
rd = context.VLA1().phoneDepths()
wf_delays = curvedWaveFront.WavefrontDelays(ssp, rd, z0=rd[1])
# Compute a curvature correction from Bellhop
envfile = write_at.writeBellhop(
    ssp.index, ssp.values, 'A', receiveD=rd.values, name='curvature_correction')
bellhop.runBellhop(envfile)
curved_arrivals = bellhop.readArr(envfile)
curved_arrivals.ref0dB()
curved_arrivals.filterArrivals(
    numTopBounces=1, numBotBounces=0, arrivalMagnitude=-20)
wf_delays.bellhop_correction(curved_arrivals)

# create timeseries from a single index
h_ts = mf.single_index(snapshotIndex)

# hilbert beamformer and postion preturber
beamformer = timeDomain.ShiftAndAdd(h_ts, upsample=uprate)

look_time = np.r_[min(tbounds): max(tbounds): 1j * numtimes]
look_angle = np.r_[anglebounds[0]: anglebounds[1]: 1j * numangles]
angle_delays = wf_delays(look_angle)

# arrival estimates from beamformer
tbounds1 = (-5.0, -4.0)
abounds1 = (4, 8)
t1 = np.r_[min(tbounds1): max(tbounds1): 1j * numtimes]
a1 = np.r_[min(abounds1): max(abounds1): 1j * numangles]
tilt_bf = beamformer(t1, wf_delays(a1))
max_angle = beam_correlations.maxangle(tilt_bf, tbounds1, abounds1)
theta_test = np.r_[-np.pi: np.pi: 200j]
beam_ts = beamformer.ts_atdelay(wf_delays(max_angle), tbounds=tbounds1,
                                upsample=10)
opt_phase = positionPertubation.DelayMax(beam_ts)
time_shift = opt_phase.optimal_delay(theta_test)
optimal_delay = np.squeeze(wf_delays(max_angle)) + np.squeeze(time_shift)

p = curvedWaveFront.u_r(max_angle, rd[1], ssp)
_ = plt.figure()
_ = plt.plot(time_shift * 1 / p * (100 / 1e3), rd.values)
_ = plt.ylim(max(rd) + 5, min(rd) - 5)
_ = plt.title('Inferred array horizontal perturbance')
_ = plt.ylabel('depth, m')
_ = plt.xlabel('horizontal pertubation, cm')

fig, ax = beam_plot.plot_ts(beam_ts)
_ = ax.set_title('Channel impulse response, delayed to %0.1f tilt' % max_angle)

tilt_bf = beamformer(look_time, angle_delays)
fig, ax = beam_plot.plot_grid(tilt_bf, drange=20)
ax.set_title('Beamformer with no tilt correction')
ax.set_xlabel('delay, s')
ax.set_ylabel('angle, degree')

beamformer.t_shift = np.array(time_shift)

fig, ax = beam_plot.plot_ts(beamformer.ts_atdelay(wf_delays(max_angle),
                                                  tbounds=(-5, -4)))
ax.set_title('Optimal lineup of time series\n Linear scale')


corrtilt_bf = beamformer(look_time, angle_delays)
fig, ax = beam_plot.plot_grid(corrtilt_bf, drange=20)
ax.set_title('Beamformer with tilt correction')
ax.set_xlabel('delay, s')
ax.set_ylabel('angle, degree')

plt.show(block=False)
