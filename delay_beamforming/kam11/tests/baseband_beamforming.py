import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import context, matchedFilter, thermister
from plotting import mf_plot, beam_plot
from beamforming import basebanded, curvedWaveFront, timeDomain
import datetime as dt

look_time = '2011 J184 03:51:15'

#def main():
"""load KAM11 data and beamform with it"""
loadDuration = dt.timedelta(0 , 5)
angebounds = (2 , 8)
numangles = 300
timebounds = (-6, -2)  # ms
numtimes = 301
snapshotIndex = 10  # snapshot used, by location in array
upRate = 10
toi = dt.datetime.strptime(look_time, '%Y J%j %H:%M:%S')

#load signal information from kam11 context
fc = context.LFM_Info().fc
bw = context.LFM_Info().bw
fs = context.fs

#Create matchedFilter CIR estimate
cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
mf=cirEstimate.matchedFilter()

#Context of measurements
ssp=thermister.getSoundSpeed(toi)
rd=context.VLA1().phoneDepths()
wf_delays = curvedWaveFront.WavefrontDelays(ssp, rd, z0 = rd[1])

#create timeseries from a single index
ts, snaptime = mf.single_index(snapshotIndex)

#beamform with timeseries
look_angle = np.r_[angebounds[0] : angebounds[1] : 1j * numangles]
angle_delays = wf_delays(look_angle)
look_time = np.r_[min(timebounds) : max(timebounds) : 1j * numtimes]

#baseband beamforming with phase correction
bb_beamformer = basebanded.BasebandBeamfomer(ts, fs, fc, bw, upsample=10)
bb_beamforming = bb_beamformer.beamform(look_time, angle_delays)

#beamforming without phase correction
ts_beamformer = basebanded.ShiftAndAdd(ts, upsample=10)
ts_beamforming = ts_beamformer.beamform(look_time, angle_delays)

fig, ax = mf_plot.plotMultipleChannel(mf , vmin = -50, tStart = -9,
                                            tEnd = 5, norm = 'total')
one_angle = np.squeeze(np.array(wf_delays(6.5)))
ts_samples = pd.DataFrame(look_time[:,None] + one_angle, index=look_time,
                          columns=rd.values)
phase_ts = bb_beamformer.resampleData(ts_samples)
fig, ax = beam_plot.plot_ts(np.abs(phase_ts), isDB=False,  isNorm=False)
title = ax.set_title('time series with phase correction')

fig, ax = beam_plot.plot_grid(bb_beamforming, x = look_time,
                                  y = look_angle, drange = 15)
ax.set_title('Beamforming result with phase correction' +
             '\ntime of snapshot: %s'%snaptime.strftime('J%j %H:%M'))
ax.set_xlabel('delay, s')
ax.set_ylabel('angle, degree')

fig, ax = beam_plot.plot_grid(ts_beamforming, x = look_time,
                                  y = look_angle, drange = 15)
ax.set_title('Beamforming result without phase correction' +
             '\ntime of snapshot: %s'%snaptime.strftime('J%j %H:%M'))
ax.set_xlabel('delay, s')
ax.set_ylabel('angle, degree')

plt.show(block=False)
