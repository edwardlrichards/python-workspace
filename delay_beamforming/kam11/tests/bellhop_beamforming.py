import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from kam11 import context, thermister
from beamforming import timeDomain, curvedWaveFront, beam_correlations
from plotting import beam_plot, bellhop_plot
from helper import passband, test_signals
from atpy import bellhop, write_at
from matplotlib import rc
rc('text', usetex=True)

#beamforming specs
tbounds=(-6,-1)
angle_bounds=(4,8)
numtimes=300
numangles=301
receiver_range = 3
#enviormental data
toi = datetime.strptime('2011 J184 03:51', '%Y J%j %H:%M')
ssp = thermister.getSoundSpeed(toi)
rd = context.VLA1().phoneDepths()
wf_delays = curvedWaveFront.WavefrontDelays(ssp, rd, rd[1])
#bellhop
bellpath = write_at.writeBellhop(ssp.index, ssp.values, 'A',
                                name='curved_beamforming', receiveD=rd.values,
                                receiveRange=receiver_range)
bellhop.runBellhop(bellpath)
bell_array = bellhop.readArr(bellpath)
bell_array.ref0dB()
#synthetic time series
synth_ts = test_signals.create_ts(bell_array, tbounds = tbounds)
hilb_ts = passband.hilbert_pb(synth_ts)
bf = timeDomain.ShiftAndAdd(hilb_ts, upsample=10)
#beamforming
look_times = np.r_[min(tbounds): max(tbounds): 1j * numtimes]
look_angles = np.r_[min(angle_bounds): max(angle_bounds): 1j * numangles]
taus = wf_delays(look_angles)
looks = bf.beamform(look_times, taus)

#Curved wave front comparison
#bell_array.filterArrivals(numTopBounces=[1])
#bell_array.ref0dB()
#TODO:Horrible hardcode
#arrivalNum=114
#curve_tau = wf_delays(bell_array.array[0].arrivals.loc[arrivalNum,
                                                       #'receiveAngle'])
#fig, ax = bellhop_plot.spark_delay(ssp, bell_array, tbound=(-10, 0))
#plt.plot(curve_tau + bell_array.array[0].arrivals.loc[arrivalNum,'time'],
         #rd, 'o')


fig, ax = bellhop_plot.spark_delay(ssp, bell_array)

#Show beamformer time series
bell_array.filterArrivals(numTopBounces=[1], numBotBounces=[0])
bell_angle = float(bell_array.arrivals[0]['receiveAngle'])
bell_array.filterArrivals()
beam_angle = beam_correlations.maxangle(looks, (-3,-2), (5, 7))

#plot_angle = bell_angle
plot_angle = beam_angle
fig, ax = beam_plot.plot_ts(synth_ts, channel_delays=wf_delays(plot_angle))
ax.set_title(r'Time series delayed with plane wave at %0.1f $^o$'%plot_angle)
ax.set_xlim(-3, -2)


fig, ax = beam_plot.plot_grid(looks, drange=10)
plt.show(block=False)
