import numpy as np
import xarray as xr
from kam11 import tilt_meter
import matplotlib.pyplot as plt

jday = 184
tilt_VLA1 = tilt_meter.load_file(jday)
fig, ax= tilt_meter.plot_column(tilt_VLA1['tilt'])
ax.set_title('Magnitude of tilt, J%i'%jday)
fig, ax= tilt_meter.plot_column(tilt_VLA1['inplane'])
ax.set_title('Amplitude of in plane tilt, J%i'%jday)
plt.show(block=False)
