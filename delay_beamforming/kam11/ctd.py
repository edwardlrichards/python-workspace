import numpy as np
import pandas as pd
import datetime as d
import pytz
from os import path, listdir, listdir
import pickle
import gsw
from itertools import groupby
from scipy.signal import firwin, lfilter
from kam11 import context

""" Load thermister data from KAM11. WHOI and MPL thermisters. """
ctdDir = '/Users/edwardlrichards/Documents/data/KAM11/enviornment/' +\
    'CTD/shipsCTD'


class CTD:
    """ load and access CTD data. Pickle is used when avalible """

    def __init__(self, fileDir=ctdDir):
        """ load data from pickle if possible, if not, load from data files """
        # Path relative to this file
        ssPic = 'processedData/soundSpeedProfile.pic'
        tempPic = 'processedData/tempProfile.pic'
        salPic = 'processedData/salProfile.pic'

        self.fileDir = fileDir
        self.ssPic = path.join(path.dirname(__file__), ssPic)
        self.tempPic = path.join(path.dirname(__file__), tempPic)
        self.salPic = path.join(path.dirname(__file__), salPic)

        try:
            self.ss = pd.read_pickle(self.ssPic)
            self.temp = pd.read_pickle(self.tempPic)
            self.sal = pd.read_pickle(self.salPic)
        except FileNotFoundError:
            self.ss, self.temp, self.sa = self.loadCTD()
        self.timeUTC = context.CTD_Info().timeUTC()
        self.location = context.CTD_Info().location()

    def loadCTD(self):
        """ load CTD data from recorded files """
        ctd_files = listdir(self.fileDir)
        ssData = None
        lon = -159
        lat = 22

        # Filter used to detect upcast or downcast
        N = 512
        Fc = 0.001
        Fs = 1
        h = firwin(numtaps=N, cutoff=Fc, nyq=Fs / 2)
        for i, f in enumerate(ctd_files):
            # only load cnv files
            fPath = path.join(self.fileDir, f)
            fileName, fileExtension = path.splitext(f)
            if fileExtension != '.cnv':
                continue
            # Pasre cast index number from file name
            castIndex = fileName.split('km1119_cast')
            castIndex = int(castIndex[1].split('SSPconv')[0])
            # rediculous comment structure requires this additional loop
            with open(fPath) as fR:
                for i, line in enumerate(fR.readlines()):
                    if line == '*END*\n':
                        lineNumber = i
                        break

            # This file has an extra field
            if f != 'km1119_cast1SSPconv.cnv':
                data = pd.read_csv(fPath, skiprows=lineNumber + 1, sep=r"\s+",
                                   names=['db', 'S/m', 'PSU', 'C', 'shipCTD',
                                          'm', 'blah'], index_col='m')
            else:
                data = pd.read_csv(fPath, skiprows=lineNumber + 1, sep=r"\s+",
                                   names=['db', 'S/m', 'PSU', 'C', 'shipCTD',
                                          'avg m/s', 'm', 'blah'], index_col='m')
                del data['avg m/s']

            del data['S/m']
            del data['blah']

            # only save data from longest downcast of CTD
            # downcast detection, upcasts were not recorded
            eps = 0.0004
            y = lfilter(h, 1.0, data.index)
            yDiff = np.diff(y)
            diffI = np.arange(yDiff.size)

            # Group casts by sign of depth derivative
            blocks = [list(g) for k, g in groupby(list(yDiff > eps))]
            # find longest block
            blockI = 0
            longestBlock = 0
            currentI = 0
            for i, b in enumerate(blocks):
                if b[0] & (longestBlock < len(b)):
                    longestBlock = len(b)
                    blockI = i
                    startIndex = currentI
                currentI += len(b)
            # prune data to longest block
            data = data.iloc[startIndex:(startIndex + longestBlock), :]

            # interpolate data and sort by depth
            # Index to fractional meter spacing
            samplesPerMeter = 10
            newIndex = np.arange(np.ceil(np.max(np.array(data.index)))
                                 * samplesPerMeter) / samplesPerMeter
            data.sort_index()
            data = data.interpolate(method='nearest')
            # Extrapolate salinity and temperature to nearest recorded value
            _, index = np.unique(data.index, return_index=True)
            data = data.iloc[index]
            combinedIndex = data.index.append(pd.Index(newIndex))
            combinedIndex = pd.Index(np.unique(combinedIndex))

            data = data.reindex(combinedIndex)
            # pressure in nan values should still change based on depth, this
            # is done by adding a zero value of pressure at surface
            data.ix[pd.Index([0]), :] = data.iloc[0, :].values
            data.ix[pd.Index([0]), 'db'] = 0
            # Actual interpolation and extrapolation of CTD data

            data['PSU'] = data['PSU'].interpolate().ffill().bfill()
            data['C'] = data['C'].interpolate().ffill().bfill()
            data['db'] = data['db'].interpolate(method='linear')

            # calculate sound speed from gsw
            abSal = gsw.SA_from_SP(data['PSU'], data['db'], lon, lat)
            conTemp = gsw.CT_from_t(abSal, data['C'], data['db'])
            gsw_SSP = gsw.sound_speed(abSal, conTemp, data['db'])

            del data['db']
            del data['shipCTD']

            ss = pd.DataFrame(gsw_SSP, index=data.index)
            ss.columns = [castIndex]
            ss = ss.ix[newIndex]

            temp = pd.DataFrame(data.ix[newIndex, 'C'])
            temp.columns = [castIndex]

            sal = pd.DataFrame(data.ix[newIndex, 'PSU'])
            sal.columns = [castIndex]

            if ssData is None:
                ssData = ss
                tempData = temp
                salData = sal
            else:
                ssData = pd.merge(ss, ssData, left_index=True,
                                  right_index=True, how='outer')
                tempData = pd.merge(temp, tempData, left_index=True,
                                    right_index=True, how='outer')
                salData = pd.merge(sal, salData, left_index=True,
                                   right_index=True, how='outer')
        ssData.to_pickle(self.ssPic)
        tempData.to_pickle(self.tempPic)
        salData.to_pickle(self.salPic)

        return ssData, tempData, salData
