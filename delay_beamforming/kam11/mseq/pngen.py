"""
Copied from WHOI script. From Proakis p.436
"""
import numpy as np

def pngen():
    """return a single generation of random noise"""
    L = 12
    N = 4095
    g = np.zeros(L)
    g[0] = 1
    g[6] = 1
    g[8] = 1
    g[11] = 1

    b = np.zeros(N)
    b[L - 1] = 1

    for i in np.arange(L, N):
        b[i] = np.fmod(np.dot(b[i - L: i], g), 2)
    return b
