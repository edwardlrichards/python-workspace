"""
Recreate the m_seq transmitted in the KAM11 experiment by SRA
"""
import numpy as np
import xarray as xr
from scipy.signal import fftconvolve
from kam11.mseq import pngen

Fs = int(1e5)

def modulated_mseq(fc):
    """Modulate the basic basebanded mseq"""
    bb_pulse = baseband_mseq()
    fc = np.array(fc, ndmin=1)
    modulation = np.cos(2 * np.pi * fc[:, None] / Fs *
            np.arange(bb_pulse.size)[None, :])

    single_pulse = modulation * bb_pulse
    single_pulse = np.squeeze(single_pulse)
    if len(single_pulse.shape) != 1:
        modulated_signal = xr.DataArray(single_pulse, dims=['Hz', 'n'],
                                  coords=[fc, np.arange(bb_pulse.size)])
    else:
        modulated_signal = xr.DataArray(single_pulse, dims=['time'],
                                  coords=[np.arange(bb_pulse.size) / Fs])
    return modulated_signal

def baseband_mseq():
    """Generate an mseq like the one used in KAM11 (no guarentee)"""
    siglen = 60  # total signal length in seconds Ts = 0.5  # silent period at the start of the file in seconds Te = 6  # silent period at the end of the file in seconds maxamp = 0.999  # maximum signal amplitude Fs = 1e5  # sampling frequency for generation and SIO systems kaiserwinbeta = 1.5 
    # generate random noise sequence
    pn_noise = pngen.pngen()
    mseqlen = np.size(pn_noise)

    Ns = 8  # samples for system 1

    # generate bit windowing function
    gaussiansigma = Ns/3.2;
    n_range = np.arange(Ns * 2 + 1) - Ns
    bbpulse = np.exp(-n_range ** 2 / (2 * (gaussiansigma ** 2)))
    bbpulse = bbpulse / np.max(np.abs(bbpulse))

    zero_padded = np.zeros((mseqlen, Ns))
    zero_padded[:, 0] = (2 * pn_noise) - 1
    zero_padded = np.ravel(zero_padded)

    # convolve bit signal with window function
    baseband_pulse = fftconvolve(zero_padded, bbpulse, 'same')
    return baseband_pulse

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    # create ambiguity surface
    bb_pulse = baseband_mseq()
    Fc = 23e3   # Hz for system 1
    f_range = np.r_[Fc - 10: Fc + 10 : 100j]
    
    test_matrix = modulated_mseq(f_range)
    single_pulse = modulated_mseq(Fc)

    correlation = np.add.reduce(np.array(test_matrix) *
                                np.array(single_pulse), axis=-1)
    correlation_dB = 10 * np.log10(np.abs(correlation))
    correlation_dB -= np.max(correlation_dB)

    fig, ax = plt.subplots()
    ax.plot(f_range - Fc, correlation_dB)
    ax.set_xlabel('delta f')
    ax.set_ylabel('correlation strength')
    ax.grid()

    plt.show(block=False)
