import numpy as np
import pandas as pd
from kam11 import context, thermister
from atpy import bellhop


defaultDir = '/Users/edwardlrichards/cir_processing/bellhopFiles/default'


def getBellhopArrivals(toi, bellhopFilesDir=defaultDir, tiltAngle=None,
                       channelNames=None):
    """ run a bellhop arrival run for time of interest"""
    # TODO:Impliment other thermister arrays
    ssp = thermister.getSoundSpeed(toi)
    fileName = toi.strftime('TS_J%j_%H_%M')
    vlaInfo = context.VLA1(timeOfInterest=toi)
    # Specify receiver positions, both with or without tilt
    if tiltAngle is None:
        rd = vlaInfo.phoneDepths(channelNames=channelNames)
        receiveRange = vlaInfo.sourceRange / 1e3
    # Positive tilt angle moves receivers further from source
    else:
        receivePos = vlaInfo.phonePosition(
            tiltAngle, channelNames=channelNames)
        rd = receivePos['depth']
        receiveRange = receivePos['range'] / 1e3
    # Write bellhopfile, run bellhop and then import results
    bellFile = bellhop.writeBellhop(ssp.index,
                                    ssp.values, 'A', saveDir=bellhopFilesDir, name=fileName,
                                    receiveRange=receiveRange, receiveD=rd)
    bellhop.runBellhop(bellFile)
    arrivals = bellhop.readArr(bellFile)
    # If array location is 2D from tileAngle, return 1D array of interest
    if tiltAngle is not None:
        arrayLocation = np.array((1e3 * receiveRange, rd)).T
        arrivals._createSubarray(desiredGeometry=arrayLocation)
    return arrivals
