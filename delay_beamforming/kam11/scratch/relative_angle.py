import datetime as dt
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from kam11 import thermister, matchedFilter, positionPertubation, context
from beamforming import timeDomain, curvedWaveFront
from plotting import beam_plot, mf_plot
import concurrent.futures

toi = dt.datetime.strptime('2011 J184 03:51:15', '%Y J%j %H:%M:%S')
subchannels = context.VLA1().phoneDepths()[:4]

cmap = plt.cm.magma_r
cmap.set_under(color='w')

#Create matchedFilter CIR estimate
ttime=20  # seconds
loadDuration = dt.timedelta(0, ttime)
#cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
cirEstimate=matchedFilter.LFM_MatchedFilter(toi)
mf=cirEstimate.matchedFilter()

#plot matched filter over time of interest
fig, ax = mf_plot.plotMultipleChannel(mf, plotChannels=subchannels.index,
                                      tStart=-8, tEnd=2, cmap=cmap,
                                      vmin=-40)
ax.set_title('Uncorrected CIR measurement \n%.1f seconds, starting %s'%(ttime, toi.strftime('%Y J%j %H:%M')))

#create wave front delay calculator
ssp = thermister.getSoundSpeed(toi)
wf = curvedWaveFront.WavefrontDelays(ssp, subchannels.values)

#Initial arrival guesses
#upgoing
t1 = (-2.5, -1.6)
a1 = (-6, -4)
#downgoing
t2 = (-5, -3.5)
a2 = (5, 7.5)

def optimal_tau(index):
    """Find optimal delays for a given CIR measurement"""
    #initilize beamformer with subchannels
    bf_er = timeDomain.ShiftAndAdd(mf.single_index(index),
                                   channels=subchannels, upsample=10)
    #Solve for optimal delays for both up and downgoing wavefronts

    max_er = positionPertubation.best_arrival(bf_er, wf)
    #tau_1 = max_er.max_er(t1, a1)
    tau_1 = max_er(t1, a1)

    tau_2 = max_er(t2, a2)
    return tau_1, tau_2

def symetric_arrivals(tau_1, tau_2):
    """Return the angle that creates the largest symetric arrival"""
    tau_1 = np.squeeze(np.array(tau_1))
    tau_2 = np.squeeze(np.array(tau_2))
    #find the residues for the first arrival
    a_t = np.array((0, 8))
    res_1 = wf(np.r_[min(-a_t): max(-a_t): 301j]) - tau_1[:, None]
    err_1 = np.abs(res_1).sum('depth')
    err_1 = err_1.rename({'angle': 'angle_1'})
    #same for downgoing wave
    res_2 = wf(np.r_[min(a_t): max(a_t): 301j]) - tau_2[:, None]
    err_2 = np.abs(res_2).sum('depth')
    err_2 = err_2.rename({'angle': 'angle_2'})

    #find the angle that minimizes both errors
    a_min1 = err_1.angle_1[err_1.argmin()]
    a_min2 = err_2.angle_2[err_2.argmin()]
    a_mean = np.mean((abs(a_min1), abs(a_min2)))

    return wf(-a_mean) - tau_1, wf(a_mean) - tau_2, a_mean

i = 3
tau_1, tau_2 = optimal_tau(i)
off_1, off_2, a_mean = symetric_arrivals(tau_1, tau_2)
bf_er = timeDomain.ShiftAndAdd(mf.single_index(i), channels=subchannels)

def plot_opt(offsets):
    """plot beamformer response with user supplied optimal offsets"""
    t_range = (-5, -1)
    a_range = (-8, 8)

    bf_er.t_shift = -offsets
    bf = bf_er(np.r_[min(t_range): max(t_range): 200j],
            wf(np.r_[min(a_range): max(a_range): 201j]))

    bf_db = 20 * np.log10(np.abs(bf))
    fig, ax = plt.subplots()
    mesh = bf_db.plot(ax)
    mesh.set_clim(90, 105)
    mesh.set_cmap(cmap)
    ax.set_ylim(min(a_range), max(a_range))
    return fig, ax

fig, ax = plot_opt(off_1)
_ = ax.set_title('Beamformer output lined with upgoing delay estimates\n' +\
                 'index of CIR estimate: %i'%i)

fig, ax = plot_opt(off_2)
_ = ax.set_title('Beamformer output lined with downgoing delay estimates\n' +\
                 'index of CIR estimate: %i'%i)

fig, ax = beam_plot.plot_ts(mf.single_index(i), tbounds=t1, channel_delays=tau_1)
_ = ax.set_title('Optimal lineup of upgoing wave \n' +\
                 'index of CIR estimate: %i'%i)

fig, ax = beam_plot.plot_ts(mf.single_index(i), tbounds=t2, channel_delays=tau_2)
_ = ax.set_title('Optimal lineup of downgoing wave\n' +\
                 'index of CIR estimate: %i'%i)

def all_offsets():
    taus = []
    with concurrent.futures.ProcessPoolExecutor(max_workers=8) as executor:
        taus = executor.map(optimal_tau, range(mf.mf_dict['num_samples']))
    result_list = list(zip(*list(taus)))
    return np.array(result_list[0]), np.array(result_list[1])

all_tau1, all_tau2 = all_offsets()
sym_result = [symetric_arrivals(i1, i2) for i1, i2 in zip(all_tau1, all_tau2)]
off1, off2, mean_a = list(zip(*sym_result))
off1 = xr.concat(off1, dim='index')
off2 = xr.concat(off2, dim='index')
all_tau1 = xr.DataArray(all_tau1, dims=['index','depth'],
                          coords=[off1.index, off1.depth])
all_tau2 = xr.DataArray(all_tau2, dims=['index','depth'],
                          coords=[off1.index, off1.depth])
mean_a = xr.DataArray(mean_a, dims=['index'])

fig, ax = plt.subplots()
mean_a.plot(ax)
ax.set_ylabel('angle, degrees')
ax.set_title('Mean estimate of upgoing and downgoing arrival angles')
ax.set_xlabel('CIR estimate index')

def sub_sym(angle, all_tau1=all_tau1, all_tau2=all_tau2, off1=off1, off2=off2):
    """Assume symetric answer and subtract same angle from both arrivals"""
    rough_delay_up = wf(-angle) - all_tau1
    rough_delay_down = wf(angle) - all_tau2
    fig, ax = plt.subplots()
    ax.set_prop_cycle(plt.cycler('ls', ['-','--',':']))
    #ax.set_prop_cycle(plt.cycler('ls', ['-',':']))
    for i, l in off1.isel(depth=slice(1,4)).groupby('depth'):
        plt.plot(l, 'b', label=i)
    plt.plot(off2.isel(depth=slice(1,4)), 'r')
    plt.ylabel('delay, ms')
    plt.xlabel('CIR index')
    plt.title('Estimate of delay offset with symetric angle estimate removed')
    plt.legend()

    fig, ax = plt.subplots()
    ax.set_prop_cycle(plt.cycler('ls', ['-','--',':']))
    for i, l in rough_delay_up.isel(depth=slice(1,4)).groupby('depth'):
        plt.plot(l, 'b', label=i)
    plt.plot(np.array(rough_delay_down.isel(depth=slice(1,4))).T, 'r')
    plt.ylabel('delay, ms')
    plt.xlabel('CIR index')
    plt.title('Estimate of delay offset with a symetric angle of %.1f removed'%angle)
    plt.legend()

#Test which indicies are valid
maxError = 1 / (2 * context.LFM_Info().fc) * 1e3
validI = (np.abs(off2 - off1)).max('depth') < maxError

sub_sym(mean_a[validI][1], all_tau1=all_tau1, all_tau2=all_tau2, off1=off1, off2=off2)
#
#sub_sym(mean_a[validI][1], all_tau1=all_tau1[validI],
        #all_tau2=all_tau2[validI], off1=off1[validI], off2=off2[validI])

#emphaise continous sections
sections = np.sign(np.diff(mean_a[validI].index) - 1)
sectionI = np.arange(sections.size)[np.array(sections, dtype='bool_')]
splitSections = np.split(np.arange(sections.size), sectionI)
sections = [sec for sec in splitSections if len(sec) > 2]
sectionI = np.hstack(sections)
example = mean_a[validI].isel(index=sectionI)
sub_sym(mean_a[sectionI][1], all_tau1=all_tau1[sectionI],
        all_tau2=all_tau2[sectionI], off1=off1[sectionI], off2=off2[sectionI])

#Choose a well behaved index
i = 24
tau_1, tau_2 = optimal_tau(i)

off_1, off_2, a_mean = symetric_arrivals(tau_1, tau_2)
bf_er = timeDomain.ShiftAndAdd(mf.single_index(i), channels=subchannels,
                            upsample=10)
fig, ax = plot_opt(off_1)
_ = ax.set_title('Beamformer output lined with upgoing delay estimates\n' +\
                 'index of CIR estimate: %i'%i)

fig, ax = plot_opt(off_2)
_ = ax.set_title('Beamformer output lined with downgoing delay estimates\n' +\
                 'index of CIR estimate: %i'%i)

bf_er.t_shift = -off_1
bf = bf_er(np.r_[-2: 2: 200j], wf(np.r_[-4: 4: 200j]))
bf_db = 20 * np.log10(np.abs(bf))
fig, ax = plt.subplots()
mesh = bf_db.plot(ax)
mesh.set_clim(100, 120)
mesh.set_cmap(cmap)
_ = ax.set_title('Corrected beamformer focused on main arrival energy\n' +\
                 'index of CIR estimate: %i'%i)

fig, ax = beam_plot.plot_ts(np.abs(mf.single_index(i)), channel_delays=off_1,
                            tbounds=(-1.5, 1.5))
_ = ax.set_title('Magnitude of corrected CIR measurement at index %i'%i)

plt.show(block=False)
