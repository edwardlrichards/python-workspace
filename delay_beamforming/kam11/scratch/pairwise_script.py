import datetime as dt
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import thermister, matchedFilter, positionPertubation, context, select_arrivals
from beamforming import timeDomain, curvedWaveFront
from plotting import beam_plot, mf_plot
import concurrent.futures

"""First attempt at array pertubation tracking. No tracking algoritm, so peak jumps"""

toi = dt.datetime.strptime('2011 J184 03:51:15', '%Y J%j %H:%M:%S')
subchannels = context.VLA1().phoneDepths()[4:]
subchannels = context.VLA1().phoneDepths()

tbounds = (-7, -3.5)
tbounds = (-8, -4)

cmap = plt.cm.magma_r
cmap.set_under(color='w')

#Create matchedFilter CIR estimate
ttime = 30  # seconds
loadDuration = dt.timedelta(0, ttime)
cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
#cirEstimate=matchedFilter.LFM_MatchedFilter(toi)
mf=cirEstimate.matchedFilter()

#create wave front delay calculator
ssp = thermister.getSoundSpeed(toi)
wf = curvedWaveFront.WavefrontDelays(ssp, subchannels.values)

#Prompt user for best guess at arrivals
#tbounds_up = select_arrivals.select_arrivals(mf, start_t=tbounds[0], end_t=tbounds[1],
                                             #plot_channels=subchannels)

tbounds_up = np.array([[-4.4516129 , -4.23387097],
                       [-4.73387097, -4.50806452],
                       [-5.03494624, -4.83870968],
                       [-5.27419355, -5.09946237],
                       [-5.57795699, -5.38978495],
                       [-5.86021505, -5.64784946],
                       [-6.00806452, -5.80645161],
                       [-6.30107527, -6.12096774],
                       [-6.57795699, -6.3844086 ],
                       [-6.84139785, -6.62096774],
                       [-7.10215054, -6.87634409],
                       [-7.19354839, -6.97043011],
                       [-7.43010753, -7.09946237],
                       [-7.60483871, -7.22043011],
                       [-7.71505376, -7.25      ],
                       [-7.85215054, -7.36290323]])


#tbounds_up = select_arrivals.select_arrivals_old(mf, subchannels, tStart=-8, tEnd=-4)
#tbounds_down = select_arrivals.select_arrivals(mf, tStart=-4, tEnd=0)
index = 3
bf_er = timeDomain.ShiftAndAdd(mf.single_index(index),
                                channels=subchannels, upsample=10)

#Use optimal delay finder to calculate cross channel correlations
max_er = positionPertubation.BestArrival(bf_er, wf)

pair_corr = []
for i in range(mf.mf_dict['num_samples']):
    bf_er = timeDomain.ShiftAndAdd(mf.single_index(i),
                                    channels=subchannels, upsample=30)
    max_er = positionPertubation.BestArrival(bf_er, wf)
    #Use bottom channel as reference
    beam_ts = max_er.selected_ts(tbounds_up, windowed=False)
    pc = max_er.pairwise_correlation(beam_ts, ref_depth=subchannels.iloc[0])
    pair_corr.append(pc)

# concat values for each mf measurement
slowtime = pd.Index(mf.single_channel(1).slowtime, name='slowtime')
all_corr = xr.concat(pair_corr, slowtime)
all_corr.values = np.real(all_corr.values)

#draw maximum value on each correlation plot
max_track = []
for i, pc in all_corr.groupby('depth'):
    fig, ax = plt.subplots()
    pc.plot()
    maxI = pc.argmax('delay').values
    max_value = pc.isel(delay=maxI)
    plt.plot(max_value.delay, max_value.slowtime)
    max_track.append(xr.DataArray(max_value.delay, dims=['slowtime'],
                           coords=[max_value.slowtime]))
    t_ref = max_value.delay.values[0]
    plt.xlim(t_ref - .1, t_ref + .1)
    #plt.ylim(0, 40)
    plt.title('Pairwise correlations for depth %.1f'%i)

#Plot the demeaned correlation times
max_track = xr.concat(max_track, pd.Index(subchannels.values[1:],
                                              name='depth'))
demean_tracks = max_track - max_track.mean(dim='slowtime')

good_tracks = demean_tracks.isel(depth=[0, 1, 2, 3, 4, 7, 8, 12])
#for i, track in demean_tracks.groupby('depth'): track.plot(label='%.2f m'%i)
fig, ax = plt.subplots()
for i, track in good_tracks.groupby('depth'): track.plot(label='%.2f m'%i)

ax.legend(loc=2)
ax.set_xlabel('slow time, sec')
ax.set_title('Demeaned delay between each channel')

plt.show(block=False)
