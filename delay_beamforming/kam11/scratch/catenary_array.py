import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from kam11 import context
from scipy.optimize import brentq

#catenary is determined by buoy pulldown
dz = 2e-4
t_bot = 0.2

rd = context.VLA1().phoneDepths().values
array_length = 60
z = np.r_[0: array_length: 300j]

#The buoy depth is not exactly specified
z_buoy = np.min(rd) - 2.15
z_bottom = z_buoy + array_length
z_bottom = np.max(rd)
z += z_buoy

def array_fromangle(theta, z):
    """Pertubation in array position from angle at bottom depth"""
    theta = np.array(theta, ndmin=1)
    z = np.array(z, ndmin=1)

    y_p = np.sin(np.radians(theta))
    a = (z_bottom - z_buoy) / np.arcsinh(y_p)

    cat = lambda z: a[:, None] * np.cosh(z /  a[:, None])
    #Remove offset to have 0 dr at array bottom
    dr = cat(z - z_buoy) - cat(z_bottom - z_buoy)

    return xr.DataArray(dr, dims=['theta', 'depth'], coords=[theta, z])

def catenary(a, z):
    """Return a catenary shape at depths z. a is T / force / L"""
    a = np.array(a, ndmin=1)
    z = np.array(z, ndmin=1)
    cat = lambda z: a[:, None] * np.cosh(z /  a[:, None])
    #Remove offset to have 0 dr at array bottom
    dr = cat(z - z_buoy) - cat(z_bottom - z_buoy)
    return xr.DataArray(dr, dims=['cat_a', 'depth'], coords=[a, z])

def a_fromangle(theta):
    """Can we estimate a from apparent angle at the bottom element?"""
    y_p = np.sin(np.radians(theta))
    a = (z_bottom - z_buoy) / np.arcsinh(y_p)
    return a

def a_fromdepth(dz):
    """a small change in depth of buoy is attributed to array catenary"""
    h = 2 * (array_length - dz)
    root_eq = lambda a: array_length / a - np.sinh(h / 2 / a)
    #Simply guessed region of convergence seems to work
    a_0 = brentq(root_eq, 10, 1e9)
    return a_0

if __name__ == "__main__":
    a = a_fromangle(t_bot)
    test_a = np.logspace(-9, -3, 302)
    fig, ax = plt.subplots()
    c_z = catenary(a, z)
    a_z = -np.sin(np.radians(t_bot)) * (z_bottom - z)
    ax.plot(c_z, z)
    ax.plot(catenary(a, rd), rd, 'r*')
    ax.plot(a_z, z, 'r')

    ax.set_ylim(np.max(z), np.min(z))
    ax.set_xlim(np.min(c_z), np.max(c_z))

    fig, ax = plt.subplots()
    ax.plot(z, c_z)
    ax.plot(rd, catenary(a, rd), 'r*')
    ax.plot(z, a_z, 'r')

    ax.set_xlim(np.max(z), np.min(z))
    ax.set_ylim(np.min(a_z), np.max(a_z))


    fig, ax = plt.subplots()
    [ax.plot(catenary(a_fromdepth(t), z), z) for t in test_a]
    ax.set_ylim(np.max(z), np.min(z))

    plt.show(block=False)

