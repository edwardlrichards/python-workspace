import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from beamforming import timeDomain, beam_correlations
from beamforming.tests import lfm_planewave
from plotting import beam_plot
from helper import passband
from kam11 import positionPertubation

num_channels = 4
sigma_pos = .2  # m
angles = [6, 6, -5, -6]
delays = [-6, -4, -2, -0]  # wierd, relative to top channel

#set random state
np.random.seed(0)
position_pertubation = np.random.randn(num_channels) * sigma_pos

def time_pertubation(angle):
    """Use horizontal slowness to predict arrival time pertubation"""
    s = 1 / 1500
    p = np.cos(np.radians(angle)) * s
    dt = position_pertubation * p * 1e3
    return dt

#create a blank timeseries
ts = lfm_planewave.create_timeseries(num_channels, 0, signal='mfchirp',
                                     channel_delays=np.full(num_channels, 10.))
for a, t in zip(angles, delays):
    pos_tau = time_pertubation(a)
    ts += lfm_planewave.create_timeseries(num_channels, a, signal='mfchirp',
                                         channel_delays=t + pos_tau)
ts = passband.hilbert_pb(ts)
fig, ax = beam_plot.plot_ts(np.abs(ts))
ax.set_title('Synthetic channel impulse response, with small position pertubations')

bf_er = timeDomain.ShiftAndAdd(ts, upsample=10)
taus = lambda angle: np.squeeze(lfm_planewave.get_delays(num_channels,
                                                         angle, units='ms'))

t_look = np.r_[-7: 1: 300j]
a_look = np.r_[-10: 10: 301j]
bf_init = bf_er(t_look, taus(a_look))

fig, ax = beam_plot.plot_grid(bf_init)
ax.set_title('Time domain beamformer, uncorrected for pertubations')

#Use beamforming results to look for optimal lineups
a_bounds = [(3, 10), (3, 10), (-10, 2), (-10, 2)]
t_bounds = [(-7, -5), (-4.5, -3), (-3, -1), (-1, 1)]

opt_er = positionPertubation.BestArrival(bf_er, taus)

def optimal_delay():
    """find optimal delays for each initial beam guess"""
    for a, t in zip(a_bounds, t_bounds):
        yield opt_er(t, a)

opt_tau = np.array(list(optimal_delay()))

for i, t in enumerate(opt_tau):
    fig, ax = beam_plot.plot_ts(np.abs(ts), channel_delays=t)
    ax.set_title('Beamforming on %ith arrival'%(i+1))

#Possible angles of 1st arrival
test_angle = [5, 6, 7]
array_pert = opt_tau[0, :] - taus(test_angle[0])
bf_er.t_shift = array_pert
bf_corr = bf_er(t_look, taus(a_look))

fig, ax = beam_plot.plot_grid(bf_corr)
ax.set_title('Time domain beamformer, corrected for pertubations')

plt.show(block=False)
