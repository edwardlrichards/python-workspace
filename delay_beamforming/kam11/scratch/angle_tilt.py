import matplotlib
matplotlib.use("Agg")
import datetime as dt
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import thermister, matchedFilter, context, select_arrivals
from kam11.delay_optimization import positionPertubation
from beamforming import timeDomain, curvedWaveFront
from plotting import beam_plot, mf_plot
from atpy import bellhop, write_at
import concurrent.futures
from os.path import join

cmap = plt.cm.magma_r
savedir = '/home/e2richards/Desktop/progress_01_08/up_figures/'
down_front=True

toi = dt.datetime.strptime('2011 J184 03:51:00', '%Y J%j %H:%M:%S')
#subchannels = context.VLA1().phoneDepths()[4:]
subchannels = context.VLA1().phoneDepths()

cmap = plt.cm.magma_r
cmap.set_under(color='w')

# Create matchedFilter CIR estimate
ttime = 20  # seconds
loadDuration = dt.timedelta(0, ttime)
cirEstimate = matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
#cirEstimate=matchedFilter.LFM_MatchedFilter(toi)
mf = cirEstimate.matchedFilter()


# create wave front delay calculator
ssp = thermister.getSoundSpeed(toi)
wf_delays = curvedWaveFront.WavefrontDelays(ssp, subchannels.values)

# Compute a curvature correction from Bellhop
wf_delays.bellhop_correction(ssp)

tbounds_down = np.array([[-4.4516129, -4.23387097],
                       [-4.73387097, -4.50806452],
                       [-5.03494624, -4.83870968],
                       [-5.27419355, -5.09946237],
                       [-5.57795699, -5.38978495],
                       [-5.86021505, -5.64784946],
                       [-6.00806452, -5.80645161],
                       [-6.30107527, -6.12096774],
                       [-6.57795699, -6.3844086],
                       [-6.84139785, -6.62096774],
                       [-7.10215054, -6.87634409],
                       [-7.19354839, -6.97043011],
                       [-7.43010753, -7.09946237],
                       [-7.60483871, -7.22043011],
                       [-7.71505376, -7.25],
                       [-7.85215054, -7.36290323]])

#First wavefront
tbounds_up = np.array([[        np.nan,         np.nan],
                       [        np.nan,         np.nan],
                       [-3.15453797, -2.71061996],
                       [-3.0940037 , -2.36759241],
                       [-2.75097614, -2.20616767],
                       [-2.57946237, -2.09518817],
                       [-2.44830477, -1.95394153],
                       [-2.32723622, -1.80260585],
                       [-2.16581149, -1.47975638],
                       [-2.09518817, -1.58064684],
                       [        np.nan,         np.nan],
                       [        np.nan,         np.nan],
                       [        np.nan,         np.nan],
                       [        np.nan,         np.nan],
                       [        np.nan,         np.nan],
                       [        np.nan,         np.nan]])
#Second wavefront
#tbounds_up = np.array([[-2.16581149, -1.56046875],
                       #[-2.02456485, -1.48984543],
                       #[-1.88331821, -1.29815356],
                       #[-1.80260585, -1.11655074],
                       #[-1.59073589, -0.92485887],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan],
                       #[        np.nan,         np.nan]])

if down_front:
    look_tilt = np.r_[0.5: 2.0: 101j]
    look_tilt = np.r_[0: 1.5: 101j]
    look_angle = np.r_[5.0: 6: 100j]
    look_angle = np.r_[5.5: 7: 100j]

    look_tilt = np.r_[0.5: 2.5: 201j]
    look_angle = np.r_[4.5: 6.5: 200j]

    #trange = np.r_[np.nanmin(tbounds_down[0]): np.nanmax(tbounds_down[0]): 100j]
    tspan = np.nanmax(tbounds_down[0]) - np.nanmin(tbounds_down[0])
    numSamples = np.ceil(tspan * 1e2)
    trange = np.linspace(np.nanmin(tbounds_down[0]), np.nanmax(tbounds_down[0]), num=numSamples)
    tbounds = tbounds_down
else:
    look_tilt = np.r_[-2: 2.0: 301j]
    look_angle = np.r_[-7: -3: 300j]
    #trange = np.r_[np.nanmin(tbounds_up[0]): np.nanmax(tbounds_up[0]): 100j]
    trange=np.r_[-4: -2: 100j]
    tbounds = tbounds_up

get_bfer = lambda i: timeDomain.ShiftAndAdd(mf.single_index(i),
                                            upsample=5)
#bf_ers = map(get_bfer, range(mf.mf_dict['num_samples']))

def angle_tilt_energy(bf_er, angle_r, tilt_r):
    """compute energy over an angle and a tilt range"""
    bf_er.tbounds = tbounds

    taus = wf_delays(angle_r, array_tilt=tilt_r)
    complete = (bf_er(trange, ts) for t, ts in taus.groupby('tilt'))
    energy = (np.square(np.absolute(c)).sum('time') for c in complete)
    result = xr.concat(energy, pd.Index(tilt_r, name='tilt'))
    return result

def bf_ers():
    """step through the beamformers"""
    for i in range(mf.mf_dict['num_samples']):
        yield i, get_bfer(i)

def plot_angle_tilt(enum_bf_er):
    """plot arrival in angle, tilt space"""
    d_seconds = dt.timedelta(0, mf.mf_dict['mf_dt'][
                             enum_bf_er[0]] - mf.mf_dict['time'].second)
    title = 'First arrival beamformer energy\n %s' % dt.datetime.strftime(
        mf.mf_dict['time'] + d_seconds, 'J%j %H:%M:%S')
    test = angle_tilt_energy(enum_bf_er[1], look_angle, look_tilt)
    test = 20 * np.log10(np.abs(test))
    test -= np.max(test)
    fig, ax = plt.subplots()
    cm = test.T.plot(ax, cmap=cmap, vmin=-10)
    ax.grid()
    ax.set_xlabel('tilt, degrees')
    ax.set_ylabel('angle of arrival, degrees')
    ax.set_title(title)
    plt.plot([np.min(look_tilt), max(look_tilt)], [5.067, 5.067], 'b', alpha=0.8)
    #ax.annotate('refraction cutoff', xy=(2, 5.067), xytext=(1.5, 5.5),
                        #arrowprops=dict(facecolor='black', shrink=0.05),)
    ax.set_xlim(np.min(look_tilt), max(look_tilt))
    ax.set_ylim(np.min(look_angle), max(look_angle))
    plt.savefig(join(savedir, 'fig%03d' % enum_bf_er[0]))
    plt.close(fig)

#plot_angle_tilt((10, get_bfer(10)))
all_runs = map(plot_angle_tilt, bf_ers())
list(all_runs)
