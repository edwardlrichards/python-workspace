import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
from kam11 import time_beamforming, matchedFilter, context, select_arrivals, thermister
from beamforming import curvedWaveFront, timeDomain
from kam11.delay_optimization import positionPertubation
from plotting import beam_plot, mf_plot

toi = dt.datetime.strptime('2011 J184 03:51:15', '%Y J%j %H:%M:%S')

#Create matchedFilter CIR estimate
loadDuration = dt.timedelta(seconds=.3)
subchannels = context.VLA1().phoneDepths()[:5]
cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
mf=cirEstimate.matchedFilter()

# create wave front delay calculator
ssp = thermister.getSoundSpeed(toi)
wf = curvedWaveFront.WavefrontDelays(ssp, subchannels.values)

#initilize beamformer with subchannels

bf_er = timeDomain.ShiftAndAdd(mf[0], upsample=10)
opt_er = positionPertubation.BestArrival(bf_er, wf)

#Solve for optimal delays for both up and downgoing wavefronts
#upgoing
t1 = (-6.5, -3)
t2 = (-2.5, -0.5)

# select upgoing taus
down_taus = select_arrivals.select_arrivals(bf_er.ts, start_t=min(t1), end_t=max(t1), 
                                plot_channels=subchannels)

# crop time series around selection
crop_ts = bf_er.ts.sel(time = slice(np.min(down_taus) - 0.1, 
                                    np.max(down_taus) + 0.1)).copy()
crop_ts = crop_ts.sel(depth=subchannels)
for i in np.arange(len(crop_ts.depth)):
    ts = crop_ts[dict(depth=i)]
    ts.loc[dict(time=slice(None, min(down_taus[i])))] = 0
    ts.loc[dict(time=slice(max(down_taus[i]), None))] = 0

bf_1 = timeDomain.ShiftAndAdd(crop_ts)

# select downgoing taus
up_taus = select_arrivals.select_arrivals(bf_er.ts, start_t=min(t2), end_t=max(t2), 
                                plot_channels=subchannels)

# crop time series around selection
crop_ts = bf_er.ts.sel(time = slice(np.min(up_taus) - 0.1, 
                                    np.max(up_taus) + 0.1)).copy()

crop_ts = crop_ts.sel(depth=subchannels)
for i in np.arange(len(crop_ts.depth)):
    ts = crop_ts[dict(depth=i)]
    ts.loc[dict(time=slice(None, min(up_taus[i])))] = 0
    ts.loc[dict(time=slice(max(up_taus[i]), None))] = 0

bf_2 = timeDomain.ShiftAndAdd(crop_ts)

import sys; sys.exit("User break") # SCRIPT EXIT
max_er = pairwise_corr.ChannelCorrelator(bf_er, wf)
bf_1 = bf_er(np.r_[min(t1): max(t1): 300j],
             wf(np.r_[min(a1): max(a1): 301j]))
tau_1 = max_er(t1, a1)
res_1 = wf(np.r_[min(a1_t): max(a1_t): 301j]) - tau_1
res_1 = res_1.rename({'depth': 'depth', 'angle': 'angle_1'})

#downgoing
t2 = (-5, -3.5)
a2 = (5, 7.5)

bf_2 = bf_er(np.r_[min(t2): max(t2): 300j],
             wf(np.r_[min(a2): max(a2): 301j]))
tau_2 = max_er(t2, a2)
res_2 = wf(np.r_[-max(a1_t): -min(a1_t): 301j]) - tau_2
res_2 = res_2.rename({'depth': 'depth', 'angle': 'angle_2'})

# compute an absolute difference of residues
res_abs = (np.abs(res_1 - res_2)).sum('depth')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds=t1, channel_delays=tau_1)
_ = ax.set_title('optimally lineup of upgoing wave')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds=t2, channel_delays=tau_2)
_ = ax.set_title('optimally lineup of downgoing wave')

fig, ax = mf_plot.plotMultipleChannel(mf, plotChannels=subchannels.index,
                                      tStart=-8, tEnd=2)

#plt.figure()
#res_abs.plot()

plt.figure()
angle = np.r_[3: 7: 100j]
error = sum(np.abs((tau_1 + wf(angle)) - (tau_2 - wf(angle))))
error.plot()
#res_1 = wf(np.r_[min(a1_t): max(a1_t): 11j]) - tau_1
#for i, res in res_1.groupby('angle'):
    #plt.plot(res.depth, 1.5 * res)
#plt.title('possible array shapes predicted from upgoing wave')
#
#plt.figure()
#res_2 = wf(np.r_[-max(a1_t): -min(a1_t): 11j]) - tau_2
#for i, res in res_2.groupby('angle'):
    #plt.plot(res.depth, 1.5 * res)
#plt.title('possible array shapes predicted from downgoing wave')

plt.show(block=False)
