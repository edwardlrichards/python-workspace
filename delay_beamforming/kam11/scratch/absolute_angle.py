import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import xarray as xr
from kam11 import ctd, context
from atpy import bellhop, write_at
from beamforming import curvedWaveFront

#Do non-isospeed sound speed profiles break relative angle ambiguity?
rd = context.VLA1().phoneDepths()
simple_c = xr.DataArray([1535.3, 1535.3, 1531], dims=['depth'],
                          coords=[[0, 40, 110]])
wf = curvedWaveFront.WavefrontDelays(simple_c.to_series(), rd)

def tilt_delays(tilt_angle, wf_angle):
    """arrival delays due to array tilt"""
    c_ref = np.interp(rd[1], simple_c.depth, simple_c)
    p = (1 / c_ref) * np.cos(np.radians(wf_angle))
    ref_height = rd[1] - rd
    tau = p * np.sin(np.radians(tilt_angle)) * ref_height * 1e3
    return tau

def contrast_wf(test_angle):
    """Compare wf arrivals and array tilt for a range of tilt values"""
    array_angle = np.r_[0: 2: 4j]
    tilt_hyp = np.array([wf(test_angle - a) - tilt_delays(a, test_angle - a)
                for a in array_angle])
    tilt_hyp = xr.DataArray(tilt_hyp, dims=['tilt', 'depth'],
                              coords=[array_angle, rd.values])
    error = wf(test_angle) - tilt_hyp
    return error

#Compare wf estimates to bellhop
saveDir='/home/e2richards/python-workspace/kam11/scratch'
env_file = write_at.writeBellhop(simple_c.depth.values, simple_c.values, 'A',
                                 name='simple_linear', saveDir=saveDir,
                                 receiveD = rd.values, frequency=23000,
                                 bottomDepth=120)
bellhop.runBellhop(env_file)
arr = bellhop.readArr(env_file)
#Find first arrival
arr.ref0dB()
arr.filterArrivals(numTopBounces=1, numBotBounces=0)
bell_a = float(arr.arrivals[0]['receiveAngle'])
bell_time = np.array([float(a['time']) for a in arr.arrivals])
bell_time -= bell_time[0]

#Simulation setup
model_CTD = ctd.CTD().ss[4]
fig, ax = plt.subplots()
ax.plot(model_CTD.values, model_CTD.index, label='Ships CTD')
ax.plot(simple_c, simple_c.depth, 'g', label='linear fit')
ax.plot([1535]*rd.size, rd, 'ro', label='hydrophones')
ax.set_ylim(120, 0)
ax.legend(loc=2)
ax.set_title('Linear sound speed profile used in simulation')
ax.set_ylabel('depth, m')
ax.set_xlabel('sound speed, m/s')
y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
ax.xaxis.set_major_formatter(y_formatter)

#Plot receiver error
ref_angle = bell_a
error = contrast_wf(ref_angle)
fig, ax = plt.subplots()
[ax.plot(e, e.depth, 'o', label='%.1f deg tilt'%e.tilt)
    for _, e in error.groupby('tilt')]
max_error = 1 / (23 * 2)
plt.plot([max_error, max_error], [100, 0], 'r--')
plt.plot([-max_error, -max_error], [100, 0], 'r--')
ax.set_ylim(100, 0)
#ax.set_xlim(-0.05, 0.35)
ax.set_xlabel('delay mismatch, ms')
ax.set_ylabel('depth, m')
ax.set_title('Wave front angle + array tilt totals %.1f degrees'%ref_angle +\
             '\nMaximum non-destructive delay error shown as red line')
ax.legend()

#Plot curvature vs angle
array_angle = np.r_[3: 7: 5j]
curve_error = np.array([wf(a) - tilt_delays(a, 0)
            for a in array_angle])
curve_error = xr.DataArray(curve_error, dims=['tilt', 'depth'],
                            coords=[array_angle, rd.values])
fig, ax = plt.subplots()
[ax.plot(e, e.depth, 'o', label='%.1f'%e.tilt)
    for _, e in curve_error.groupby('tilt')]
ax.set_ylim(100, 0)
ax.set_xlabel('delay mismatch, ms')
ax.set_ylabel('depth, m')
ax.set_title('Difference between iso-speed delay and curved wave front delay')
ax.legend()


#Plot wavefront error
error = wf(ref_angle) - bell_time
fig, ax = plt.subplots()
ax.plot(error, rd.values, 'o')
max_error = 1 / (23 * 2)
plt.plot([max_error, max_error], [100, 0], 'r--')
plt.plot([-max_error, -max_error], [100, 0], 'r--')
ax.set_ylim(100, 0)
#ax.set_xlim(-0.05, 0.35)
ax.set_xlabel('delay mismatch, ms')
ax.set_ylabel('depth, m')
ax.set_title('Bellhop and snell\'s law mismatch at %.1f degrees'%ref_angle +\
             '\nMaximum non-destructive delay error shown as red line')

plt.show(block=False)
