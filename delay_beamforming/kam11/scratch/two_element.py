import datetime as dt
import numpy as np
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
from kam11 import matchedFilter, positionPertubation, context, thermister
from beamforming import timeDomain, curvedWaveFront
from plotting import beam_plot, mf_plot

manimation.ffmpeg_path = '/home/e2richards/ffmpeg/ffmpeg'
toi = dt.datetime.strptime('2011 J184 03:51:15', '%Y J%j %H:%M:%S')

plotchannels = context.VLA1().phoneDepths()[:12]
#Create matchedFilter CIR estimate
loadDuration = dt.timedelta(0, 10)
cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
mf=cirEstimate.matchedFilter()

#fig, ax = mf_plot.plotMultipleChannel(mf, plotChannels=plotchannels.index,
                                      #tStart=-8, tEnd=2)

channels = np.array((10,11))
two_channels = context.VLA1().phoneDepths().iloc[channels]
ssp = thermister.getSoundSpeed(toi)
wf_er = curvedWaveFront.WavefrontDelays(ssp, two_channels)

#Create a movie of an arrival

slowtime = mf.mf_dict[1].slowtime.values
for i, t in enumerate(slowtime):
    #bf_er = timeDomain.ShiftAndAdd(mf.single_index(int(i)),
                                #channels=two_channels)
    ts = mf.single_index(i).isel(depth=channels)
    fig, ax = beam_plot.plot_ts(np.abs(ts), tbounds=(-8, -4))
    ax.set_title('Real part of received signal, time %.2f seconds'%t)
    fig.savefig('/home/e2richards/Desktop/sub_channel/movie/frame_%0.3i'%i)
    plt.close(fig)

import sys; sys.exit("User break") # SCRIPT EXIT
#initilize beamformer with subchannels
(bf_er, wf) = time_beamforming.load_beamformer(toi, channels=subchannels)

tbounds = (-8, -2)
abounds = (-8, 8)

t_x = np.r_[min(tbounds): max(tbounds): 300j]
a_x = np.r_[min(abounds): max(abounds): 301j]

bf = bf_er(t_x, wf(a_x))

#Solve for optimal delays for both up and downgoing wavefronts
#upgoing
t1 = (-2.5, -1.6)
a1 = (-6, -4)
a1_t = (-7, -3)

max_er = positionPertubation.best_arrival(bf_er, wf)
bf_1 = bf_er(np.r_[min(t1): max(t1): 300j],
             wf(np.r_[min(a1): max(a1): 301j]))
tau_1 = max_er(t1, a1)
res_1 = wf(np.r_[min(a1_t): max(a1_t): 301j]) - tau_1
res_1 = res_1.rename({'depth': 'depth', 'angle': 'angle_1'})

#downgoing
t2 = (-5, -3.5)
a2 = (5, 7.5)

bf_2 = bf_er(np.r_[min(t2): max(t2): 300j],
             wf(np.r_[min(a2): max(a2): 301j]))
tau_2 = max_er(t2, a2)
res_2 = wf(np.r_[-max(a1_t): -min(a1_t): 301j]) - tau_2
res_2 = res_2.rename({'depth': 'depth', 'angle': 'angle_2'})

# compute an absolute difference of residues
res_abs = (np.abs(res_1 - res_2)).sum('depth')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds=t1, channel_delays=tau_1)
_ = ax.set_title('optimally lineup of upgoing wave')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds=t2, channel_delays=tau_2)
_ = ax.set_title('optimally lineup of downgoing wave')


#plt.figure()
#res_abs.plot()

plt.figure()
angle = np.r_[3: 7: 100j]
error = sum(np.abs((tau_1 + wf(angle)) - (tau_2 - wf(angle))))
error.plot()
#res_1 = wf(np.r_[min(a1_t): max(a1_t): 11j]) - tau_1
#for i, res in res_1.groupby('angle'):
    #plt.plot(res.depth, 1.5 * res)
#plt.title('possible array shapes predicted from upgoing wave')
#
#plt.figure()
#res_2 = wf(np.r_[-max(a1_t): -min(a1_t): 11j]) - tau_2
#for i, res in res_2.groupby('angle'):
    #plt.plot(res.depth, 1.5 * res)
#plt.title('possible array shapes predicted from downgoing wave')

plt.show(block=False)
