import numpy as np
import xarray as xa
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from kam11 import context, thermister, time_beamforming, array_shape
from plotting import beam_plot
from beamforming import curvedWaveFront
import catenary_array

toi = datetime.datetime.strptime('2011 J184 03:51:25', '%Y J%j %H:%M:%S')

rd = context.VLA1().phoneDepths()

# guess angle
near_angle = 6.5

# 3 dimensional search space
look_range = np.r_[4: 6: 300j]
cat_range = np.r_[-1: 1: 302j]
tilt_bounds = np.r_[0: 1.5: 301j]

bf_tbounds = np.arange(-4.8, -3.8,  1e-3)

# create wave front delay calculator
bf_er, wf = time_beamforming.load_beamformer(toi, channels=rd)
# Compute a curvature correction from Bellhop
wf.bellhop_correction()

# Array shape delay calculator
dr = array_shape.ArrayShape(rd=rd, ssp=wf.ssp)

#Create minimization function
def total_energy(x_vector):
    """Find the total beamformer energy given 3 input parameters in vector"""
    tau = dr(wf_out=wf(x_vector[0]), tilt_theta=x_vector[1],
             cat_theta=x_vector[2])
    tau = tau.values.squeeze()
    total_energy = (np.absolute(bf_er(bf_tbounds, tau)) ** 2).sum('time')
    # minimize, so make negative
    return -total_energy.values

class MyBounds(object):
    def __init__(self, xmax=[8, 2, 1], xmin=[5.2, 0, 1e-6] ):
        self.xmax = np.array(xmax)
        self.xmin = np.array(xmin)

    def __call__(self, **kwargs):
        x = kwargs["x_new"]
        tmax = bool(np.all(x <= self.xmax))
        tmin = bool(np.all(x >= self.xmin))
        return tmax and tmin

class MyTakeStep(object):
   def __init__(self, stepsize=[0.5, .1, .1]):
       self.stepsize = np.array(stepsize)

   def __call__(self, x):
       s = self.stepsize
       x[0] += np.random.uniform(-s[0], s[0])
       x[1] += np.random.uniform(-s[1], s[1])
       x[2] += np.random.uniform(-s[2], s[2])
       return np.array(x)


#opt = [6.34, 1.62e-1, 3.2e-4]  #  03:51:28
#opt = [6.435, 2e-1, 1.5e-3]  #  03:51:28
import sys; sys.exit("User break") # SCRIPT EXIT
#bf_tbounds are used only for visualizing beamformer results
bf_simple = bf_er(bf_tbounds, wf(look_range))

bf_simple = 20 * np.log10(np.absolute(bf_simple))

#Find the angle of the largest energy
opt_angle = bf_simple.argmax()
opt_angle = float(bf_simple[np.unravel_index(opt_angle, bf_simple.shape)].angle)

fig, ax = plt.subplots()
bf_simple.plot(ax=ax, vmin=95)
ax.set_ylim(np.min(look_range), np.max(look_range))
ax.set_title('Simple angle search beamforming result\n optimal angle %0.2f degrees' %(
    opt_angle))

fig, ax = plt.subplots()
beam_plot.plot_ts(bf_er.ts, tbounds=(np.min(bf_tbounds), np.max(bf_tbounds)),
                  channel_delays=wf(opt_angle), overaxis=ax)
ax.set_title('Time series delayed to angle of %0.2f degrees' %(opt_angle))


shape_tau = shape_tau.stack(all_tau=('angle', 'cat_theta', 'tilt_theta'))

def narrow_search_space(delay_da):
    """only search when angle is within a tolerance of the expected"""
    for _, ts in shape_tau.groupby('all_tau'):
        yield bf_er(bf_tbounds, ts)

complete = (bf_er(bf_tbounds, ts) for _, ts in shape_tau.groupby('all_tau'))
energy = (np.square(np.absolute(c)).sum('time') for c in complete)

import sys; sys.exit("User break") # SCRIPT EXIT
e_attilt = xa.concat(energy, shape_tau.all_tau)

#bf_tbounds are used only for visualizing beamformer results
bf_simple = bf_er(bf_tbounds, wf(look_range))

bf_simple = 20 * np.log10(np.absolute(bf_simple))

#Find the angle of the largest energy
opt_angle = bf_simple.argmax()
opt_angle = float(bf_simple[np.unravel_index(opt_angle, bf_simple.shape)].angle)


fig, ax = plt.subplots()
bf_simple.plot(ax=ax, vmin=95)
ax.set_ylim(np.min(look_range), np.max(look_range))
ax.set_title('Simple angle search beamforming result\n optimal angle %0.2f degrees' %(
    opt_angle))

fig, ax = plt.subplots()
beam_plot.plot_ts(bf_er.ts, tbounds=(np.min(bf_tbounds), np.max(bf_tbounds)),
                  channel_delays=wf(opt_angle), overaxis=ax)
ax.set_title('Time series delayed to angle of %0.2f degrees' %(opt_angle))

#Compute optimal arrival angle allowing for array tilt
is_cutoff = np.isnan(wf(look_range).sum('depth', skipna=False)).values

taus = wf(look_range[np.bitwise_not(is_cutoff)], array_tilt=tilt_range)
complete = (bf_er(bf_tbounds, ts) for _, ts in taus.groupby('tilt'))
energy = (np.square(np.absolute(c)).sum('time') for c in complete)
e_attilt = xa.concat(energy, taus.tilt)

energy_db = 10 * np.log10(e_attilt)

plt.figure()
energy_db.plot(vmin=energy_db.max()-5)


plt.show(block=False)
import sys; sys.exit("User break") # SCRIPT EXIT
# Is catenary a better search parameter?

cat_dr = catenary_array.array_fromangle(cat_range, context.VLA1().phoneDepths())
taus = wf(look_range[np.bitwise_not(is_cutoff)], dr=cat_dr)
complete = (bf_er(fa_tbounds, ts) for _, ts in taus.groupby('tilt'))
energy = (np.square(np.absolute(c)).sum('time') for c in complete)
e_atcat = xa.concat(energy, taus.tilt)

cat_db = 10 * np.log10(e_atcat)

plt.figure()
cat_db.plot()




def valid_positions(optimal_angle, look_angle):
    """Return valid array tilts and catenaries for each look angle"""
    tilt_arrivals = (optimal_angle - look_angle + tilt_range)
    allowed_tilt = tilt_arrivals[(tilt_arrivals >= min(a_bounds)) &
                                 (tilt_arrivals <= max(a_bounds))]
    #Simplification
    allowed_tilt = [optimal_angle - look_angle]
    #Catenary + tilt = position pertubation
    #|Catenary| < |tilt|
    for tilt in allowed_tilt:
        s_t = np.sign(tilt)
        cat_values = cat_range[cat_range < np.abs(tilt) / 2]
        tilt_values = tilt - cat_values
        #find a for each value in cat_range
        a_temp = catenary_array.a_fromangle(cat_values)
        #Compute displacement for each catenary value
        cat_disp = s_t * catenary_array.catenary(a_temp, rd)
        #add tilt displacement to each catenary value
        dz = - (rd - rd[1]).values
        tilt_disp = np.sin(np.radians(tilt_values)) * dz[:, None]
        dr = cat_disp + tilt_disp.T
        displacement = xa.DataArray(dr, dims=['tilt', 'depth'], coords=[
            tilt_values, rd])
        displacement.attrs['correction'] = tilt
        yield displacement


plt.show(block=False)
