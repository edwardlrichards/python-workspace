import numpy as np
import xarray as xa
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from kam11 import context, thermister, time_beamforming, matchedFilter
from plotting import beam_plot, mf_plot
from beamforming import curvedWaveFront
import catenary_array

toi = datetime.datetime.strptime('2011 J184 03:51:28', '%Y J%j %H:%M:%S')
toi = datetime.datetime.strptime('2011 J184 03:52:19', '%Y J%j %H:%M:%S')
#toi = datetime.datetime.strptime('2011 J184 03:52:52', '%Y J%j %H:%M:%S')
toi = datetime.datetime.strptime('2011 J184 05:51:29', '%Y J%j %H:%M:%S')
toi = datetime.datetime.strptime('2011 J184 05:51:19', '%Y J%j %H:%M:%S')

rd = context.VLA1().phoneDepths()[:6]

#user_plotangle = 6.6
user_plotangle = 6.15
#u_ta = 0
u_ta = -3

#tbounds = (-5, -4)
#tbounds = (-4.6, -3.8)
#tbounds = (-5, -4)
#tbounds = (-6, -5)
tbounds = (-3, -2)

dur = datetime.timedelta(seconds=10)
mf = matchedFilter.LFM_MatchedFilter(toi, loadDuration=dur).matchedFilter()

cmap = plt.cm.magma_r
vmax = 20 * np.log10(np.abs(mf[0].max()))
mf_plot.plotMultipleChannel(mf, norm='None', vmin=vmax-40, tStart=-9, tEnd=3)

# create wave front delay calculator
bf_er, wf = time_beamforming.load_beamformer(toi, channels=rd)

# Compute a curvature correction from Bellhop
wf.bellhop_correction()

look_range = np.r_[3.0: 8: 200j]

#bf_tbounds are used only for visualizing beamformer results
bf_tbounds = np.r_[min(tbounds): max(tbounds):  299j]
bf_simple = bf_er(bf_tbounds, wf(look_range))

bf_simple = 20 * np.log10(np.absolute(bf_simple))

#Find the angle of the largest energy
opt_angle = bf_simple.argmax()
opt_angle = float(bf_simple[np.unravel_index(opt_angle, bf_simple.shape)].angle)

fig, ax = plt.subplots()
bf_simple.plot(ax=ax, vmin=bf_simple.max()-5)
ax.set_ylim(np.min(look_range), np.max(look_range))
ax.set_title('Simple angle search beamforming result\n optimal angle %0.2f degrees' %(
    opt_angle))

fig, ax = plt.subplots()
beam_plot.plot_ts(bf_er.ts, tbounds=(np.min(bf_tbounds), np.max(bf_tbounds)),
                  channel_delays=wf(opt_angle), overaxis=ax)
ax.grid()
ax.set_title('Time series delayed to angle of %0.2f degrees' %(opt_angle))

fig, ax = plt.subplots()
beam_plot.plot_ts(bf_er.ts, tbounds=(np.min(bf_tbounds), np.max(bf_tbounds)),
                  channel_delays=wf(user_plotangle, array_tilt=u_ta), overaxis=ax)
ax.grid()
ax.set_title('Time series delayed to angle of %0.2f degrees\n tilt of %0.1f degrees' %(user_plotangle, u_ta))

plt.show(block=False)
