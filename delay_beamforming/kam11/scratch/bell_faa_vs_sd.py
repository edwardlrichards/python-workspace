import datetime
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pickle
from concurrent.futures import ThreadPoolExecutor
from beamforming import curvedWaveFront
from kam11 import thermister, context, plot_bathy
from atpy import write_at, bellhop, arrivals


toi = datetime.datetime.strptime('2011 J184 03:51', '%Y J%j %H:%M')
timespan = datetime.timedelta(minutes=0, hours=1)
ts = thermister.WHOI_TS().WHOIString

tStart = toi - timespan
tEnd = toi + timespan
startI = ts.index.searchsorted(tStart)
endI = ts.index.searchsorted(tEnd)

ts = ts[startI: endI]
ssConverter = thermister.SoundSpeedFromTemp()
ssp = ssConverter(ts)

time_stamps = ssp.index
ssp = xr.DataArray(ssp.values, dims=['index', 'depth'], coords=[
                     np.arange(ssp.shape[0]), ssp.columns])
# range independent bellhop run
bell_run = lambda profile, i, sd: write_at.writeBellhop(
    ssp.depth.values, profile.values, runType='A', bottomDepth=110,
    frequency=23000, sourceD=sd, name='ssp_%03d' % i)


def first_arrival(bellhop_envpath):
    """Run bellhop on a given enviornment and find aoa of first path"""
    bellhop.runBellhop(bellhop_envpath)
    arrs = bellhop.readArr(bellhop_envpath)
    arrs.ref0dB()
    arrs.filterArrivals(numTopBounces=1, numBotBounces=0, arrivalMagnitude=-30)
    taa = float(arrs.arrivals[0].receiveAngle)
    arrs.filterArrivals(numTopBounces=1, numBotBounces=1, arrivalMagnitude=-30)
    baa = arrs.arrivals[0].sort('time').iloc[0].receiveAngle
    return (taa, baa)

sra = context.SRA().sd

all_depths_file = '/home/e2richards/python-workspace/kam11/scratch/bell_sense.pic'
with open(all_depths_file, 'rb') as f:
    all_depths = pickle.load(f)

colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
ax, fig = plt.subplots()
for ts, d in zip(all_depths, sra.values):
    c = colors.pop()
    [plt.plot(j, label=d if i == 1 else "", color=c) for i, j in ts.groupby('isup')]
plt.legend()
plt.show(block=False)



import sys; sys.exit("User break") # SCRIPT EXIT


all_depths = []
for depth in sra:
    allruns = (bell_run(s, i, depth) for i, s in ssp.groupby('index'))
    with ThreadPoolExecutor(max_workers=8) as executor:
        first_arrival_angle = list(executor.map(first_arrival, allruns))
    first_arrival_angle = xr.DataArray(first_arrival_angle, dims=[
                                         'index', 'isup'], coords=[np.arange(time_stamps.size), [1, -1]])
    all_depths.append(first_arrival_angle)

#first_arrival_angle = list(map(first_arrival, allruns))


fig, ax = plt.subplots()
ax.plot(time_stamps, first_arrival_angle)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
plt.setp(ax.get_xticklabels(), rotation=45)
ax.set_title('Angle of first arrival on bottom receiver')

plt.show(block=False)
