import datetime
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from concurrent.futures import ThreadPoolExecutor
from beamforming import curvedWaveFront
from kam11 import thermister, context, plot_bathy
from atpy import write_at, bellhop, arrivals


toi = datetime.datetime.strptime('2011 J184 03:51', '%Y J%j %H:%M')
timespan = datetime.timedelta(minutes=0, hours=1)
ts = thermister.WHOI_TS().WHOIString

tStart = toi - timespan
tEnd = toi + timespan
startI = ts.index.searchsorted(tStart)
endI = ts.index.searchsorted(tEnd)

ts = ts[startI: endI]
ssConverter = thermister.SoundSpeedFromTemp()
ssp = ssConverter(ts)

time_stamps = ssp.index
ssp = xr.DataArray(ssp, dims=['datetime', 'depth'])

rd = context.VLA1().phoneDepths()
look_angles = np.r_[4: 8: 400j]

ref_ssp = pd.Series(ssp.sel(datetime=toi).values,
                            ssp.sel(datetime=toi).depth.values)
reference_arrival = curvedWaveFront.WavefrontDelays(ref_ssp, rd)(6).squeeze()

#Compute range of arrivals around reference for all times
profiles = (pd.Series(s_p.values, s_p.depth) for _, s_p in ssp.groupby('datetime'))
wf_ers = (curvedWaveFront.WavefrontDelays(s_p, rd) for s_p in profiles)
taus = (wf_er(look_angles) for wf_er in wf_ers)
tau_range = xr.concat(taus, time_stamps)

#compute minimum mismatch between reference at each time
max_channel = 10
mismatch = np.absolute(tau_range - reference_arrival)
mismatch = mismatch.isel(depth=slice(0, max_channel))
best_fit = mismatch.max('depth').min('angle')


import sys; sys.exit("User break") # SCRIPT EXIT


#time_mean = tau_range.mean('datetime')
#time_var = tau_range.var('datetime')

#[plt.plot(look_angles, t_p.T + np.arange(16) * 3) for _, t_p in tau_range.groupby('datetime')]

import sys; sys.exit("User break") # SCRIPT EXIT

fig, ax = plt.subplots()
[plt.plot(s, ssp.depth, 'b') for _, s in ssp.groupby('index')]
plt.ylim(100, 0)
plt.title('Sound speed profiles from WHOI thermister\n measured from %s to %s' % (
    datetime.datetime.strftime(tStart, 'J%j %H:%M'), datetime.datetime.strftime(tEnd, '%H:%M')))

plt.show(block=False)
