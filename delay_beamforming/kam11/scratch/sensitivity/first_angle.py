import datetime
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from concurrent.futures import ThreadPoolExecutor
from beamforming import curvedWaveFront
from kam11 import thermister, context, plot_bathy
from atpy import write_at, bellhop, arrivals


toi = datetime.datetime.strptime('2011 J184 03:51', '%Y J%j %H:%M')
timespan = datetime.timedelta(minutes=0, hours=1)
ts = thermister.WHOI_TS().WHOIString

tStart = toi - timespan
tEnd = toi + timespan
startI = ts.index.searchsorted(tStart)
endI = ts.index.searchsorted(tEnd)

ts = ts[startI: endI]
ssConverter = thermister.SoundSpeedFromTemp()
ssp = ssConverter(ts)

time_stamps = ssp.index
import sys; sys.exit("User break") # SCRIPT EXIT
ssp = xr.DataArray(ssp.values, dims=['index', 'depth'], coords=[
                     np.arange(ssp.shape[0]), ssp.columns])

# range independent bellhop run
bell_run = lambda profile, i: write_at.writeBellhop(
    ssp.depth.values, profile.values, runType='A', bottomDepth=110,
    frequency=23000, name='ssp_%03d' % i)


def first_arrival(bellhop_envpath):
    """Run bellhop on a given enviornment and find aoa of first path"""
    bellhop.runBellhop(bellhop_envpath)
    arrs = bellhop.readArr(bellhop_envpath)
    arrs.ref0dB()
    arrs.filterArrivals(numTopBounces=1, numBotBounces=0, arrivalMagnitude=-20)
    return float(arrs.arrivals[0].receiveAngle)

rd = context.VLA1().phoneDepths()


def cuttoff_angle(profile):
    """Bisection search for nan value"""
    start_range = [3, 7]
    ssp_series = pd.Series(profile.values, profile.depth.values)
    top_value = lambda angle: float(curvedWaveFront.delayFromProfile(
        angle, ssp_series, rd, rd[1])[-1])
    test_range = start_range
    test_state = np.isnan((top_value(min(start_range)),
                           top_value(max(start_range))))
    while (test_range[1] - test_range[0]) > 0.01:
        test_value = (test_range[1] + test_range[0]) / 2
        test_result = np.isnan(top_value(test_value))
        if test_result:
            test_range[0] = test_value
        else:
            test_range[1] = test_value
    return (test_range[0] + test_range[1]) / 2

refractive_co = np.array([cuttoff_angle(s) for _, s in ssp.groupby('index')])


allruns = (bell_run(s, i) for i, s in ssp.groupby('index'))
with ThreadPoolExecutor(max_workers=7) as executor:
    first_arrival_angle = list(executor.map(first_arrival, allruns))

first_arrival_angle = np.array(first_arrival_angle)

fig, ax = plt.subplots()
ax.plot(time_stamps, refractive_co)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
plt.setp(ax.get_xticklabels(), rotation=45)
plt.grid()
ax.set_title('Refractive cuttoff angle of channel')


fig, ax = plt.subplots()
ax.plot(time_stamps, first_arrival_angle)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
plt.setp(ax.get_xticklabels(), rotation=45)
plt.grid()
ax.set_title('Angle of first arrival on bottom receiver')

fig, ax = plt.subplots()
[plt.plot(s, ssp.depth, 'b') for _, s in ssp.groupby('index')]
plt.ylim(100, 0)
plt.grid()
plt.title('Sound speed profiles from WHOI thermister\n measured from %s to %s' % (
    datetime.datetime.strftime(tStart, 'J%j %H:%M'), datetime.datetime.strftime(tEnd, '%H:%M')))

# load bathymetry
pmrf = plot_bathy.PMRF()
pmrf.bathy_line(context.SRA().stationID, context.VLA1().stationID)

plt.show(block=False)
