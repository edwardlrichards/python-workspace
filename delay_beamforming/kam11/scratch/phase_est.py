import datetime as dt
import numpy as np
import xarray as xr
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
from kam11 import matchedFilter, positionPertubation, context, thermister
from beamforming import timeDomain, curvedWaveFront, beam_correlations
from plotting import beam_plot, mf_plot

manimation.ffmpeg_path = '/home/e2richards/ffmpeg/ffmpeg'
toi = dt.datetime.strptime('2011 J184 03:51:15', '%Y J%j %H:%M:%S')

plotchannels = context.VLA1().phoneDepths()[:12]
#Create matchedFilter CIR estimate
loadDuration = dt.timedelta(0, 10)
cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
mf=cirEstimate.matchedFilter()
tbounds=(-8, -5)

#fig, ax = mf_plot.plotMultipleChannel(mf, plotChannels=plotchannels.index,
                                      #tStart=-8, tEnd=2)

channels = np.array((10,11))
two_channels = context.VLA1().phoneDepths().iloc[channels]

#Creat an estimate of the stable phase common to signals
ts = mf.single_index(1).isel(depth=channels).sel(time=slice(min(tbounds),
                                                            max(tbounds)))
unwrap = np.unwrap(np.angle(ts))
#remove initial offset
unwrap = unwrap - unwrap[:, 0][:, None]

y = np.array(np.hstack([unwrap[0,: ], unwrap[1,: ]]), ndmin=2).T

norm_time = ts.time - min(ts.time)

#plt.figure()
#_ = plt.plot(norm_time, unwrap.T)

x = np.array(np.hstack([norm_time, norm_time]), ndmin=2).T

m = float(np.linalg.lstsq(x, y)[0])

demean = np.unwrap(np.angle(ts * np.exp(-1j * m * ts.time.values)))

#fig, ax = beam_plot.plot_ts(demean, taxis=ts.time)
#ax.grid()
fig, ax = beam_plot.plot_ts(np.abs(ts))
ax.grid()

#Beamform on energy to find optimal lineup
bf_tbounds = (-7.5, -5)
bf_dbounds = (-0.5, 0)

t_look = np.r_[min(bf_tbounds): max(bf_tbounds): 100j]
d_look = np.r_[min(bf_dbounds): max(bf_dbounds): 101j]
bf_er = timeDomain.ShiftAndAdd(np.abs(ts), upsample=None)

ssp = thermister.getSoundSpeed(toi)
wf_er = curvedWaveFront.WavefrontDelays(ssp, two_channels)

delays = np.vstack([np.zeros(d_look.shape), d_look])
delays = xr.DataArray(delays, dims=['depth', 'angle'],
                        coords=[two_channels, d_look])
bf = bf_er(t_look, delays)

#fig, ax = beam_plot.plot_grid(bf, drange=20)
#ax.set_ylabel('delay, ms')

tau_1 = beam_correlations.maxangle(bf, (-7.5,-6), (0, -0.2))
tau_2 = beam_correlations.maxangle(bf, (-6,-5), (0, -0.2))

#TODO:Should not be decimated to grid points. use bf_er
#create 2 shifted time series, one for each optimal delay
def delay_phasediff(tau):
    """Plot the phase difference between two segments delayed by tau"""
    delay = ts[-1,:].sel(time=slice(min(ts.time) + tau,
                                    max(ts.time) + tau)).copy()
    delay.coords['time'] = ts.time.sel(time=slice(min(ts.time) - tau,
                                                    max(ts.time) - tau))

    chan_0 = ts[0,:].copy()
    chan_1 = delay.copy()
    chan_0.values = np.angle(ts[0,:])
    chan_1.values = np.angle(delay)

    diff_phase = chan_0 - chan_1
    return diff_phase

diff_phase_1 = delay_phasediff(tau_1)
diff_phase_2 = delay_phasediff(tau_2)

plt.figure()
plt.plot(diff_phase_1.time, np.cos(diff_phase_1))
plt.plot(diff_phase_2.time, np.cos(diff_phase_2))
plt.ylim(-1.2, 1.5)
plt.grid()

plt.show(block=False)

import sys; sys.exit("User break") # SCRIPT EXIT
#Create a movie of an arrival
slowtime = mf.mf_dict[1].slowtime.values
for i, t in enumerate(slowtime):
    #bf_er = timeDomain.ShiftAndAdd(mf.single_index(int(i)),
                                #channels=two_channels)
    ts = mf.single_index(i).isel(depth=channels)
    fig, ax = beam_plot.plot_ts(np.abs(ts), tbounds=tbounds)
    ax.set_title('Real part of received signal, time %.2f seconds'%t)
    fig.savefig('/home/e2richards/Desktop/sub_channel/movie/frame_%0.3i'%i)
    plt.close(fig)

#initilize beamformer with subchannels
(bf_er, wf) = time_beamforming.load_beamformer(toi, channels=subchannels)

tbounds = (-8, -2)
abounds = (-8, 8)

t_x = np.r_[min(tbounds): max(tbounds): 300j]
a_x = np.r_[min(abounds): max(abounds): 301j]

bf = bf_er(t_x, wf(a_x))

#Solve for optimal delays for both up and downgoing wavefronts
#upgoing
t1 = (-2.5, -1.6)
a1 = (-6, -4)
a1_t = (-7, -3)

max_er = positionPertubation.best_arrival(bf_er, wf)
bf_1 = bf_er(np.r_[min(t1): max(t1): 300j],
             wf(np.r_[min(a1): max(a1): 301j]))
tau_1 = max_er(t1, a1)
res_1 = wf(np.r_[min(a1_t): max(a1_t): 301j]) - tau_1
res_1 = res_1.rename({'depth': 'depth', 'angle': 'angle_1'})

#downgoing
t2 = (-5, -3.5)
a2 = (5, 7.5)

bf_2 = bf_er(np.r_[min(t2): max(t2): 300j],
             wf(np.r_[min(a2): max(a2): 301j]))
tau_2 = max_er(t2, a2)
res_2 = wf(np.r_[-max(a1_t): -min(a1_t): 301j]) - tau_2
res_2 = res_2.rename({'depth': 'depth', 'angle': 'angle_2'})

# compute an absolute difference of residues
res_abs = (np.abs(res_1 - res_2)).sum('depth')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds=t1, channel_delays=tau_1)
_ = ax.set_title('optimally lineup of upgoing wave')

fig, ax = beam_plot.plot_ts(bf_er.ts, tbounds=t2, channel_delays=tau_2)
_ = ax.set_title('optimally lineup of downgoing wave')


#plt.figure()
#res_abs.plot()

plt.figure()
angle = np.r_[3: 7: 100j]
error = sum(np.abs((tau_1 + wf(angle)) - (tau_2 - wf(angle))))
error.plot()
#res_1 = wf(np.r_[min(a1_t): max(a1_t): 11j]) - tau_1
#for i, res in res_1.groupby('angle'):
    #plt.plot(res.depth, 1.5 * res)
#plt.title('possible array shapes predicted from upgoing wave')
#
#plt.figure()
#res_2 = wf(np.r_[-max(a1_t): -min(a1_t): 11j]) - tau_2
#for i, res in res_2.groupby('angle'):
    #plt.plot(res.depth, 1.5 * res)
#plt.title('possible array shapes predicted from downgoing wave')

plt.show(block=False)
