from datetime import datetime, timedelta
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
from kam11 import context, matchedFilter, thermister, select_arrivals
from beamforming import curvedWaveFront, timeDomain
from kam11.delay_optimization import pairwise_corr, peak_tracking
from plotting import beam_plot
from os.path import join

toi = datetime.strptime('2011 J184 03:51:00', '%Y J%j %H:%M:%S')
toi_pre = datetime.strptime('2011 J184 03:49:00', '%Y J%j %H:%M:%S')
toi_post = datetime.strptime('2011 J184 03:54:00', '%Y J%j %H:%M:%S')
channels = slice(None, None, None)
abounds = (-8, 8)
tbounds = (-6, -1)
#Movie save folder
save_dir = '/home/e2richards/Documents/kam11_beamforming/progress_01_15/demeaned_beamforming'

subarray = context.VLA1().phoneDepths()[channels]
arange = np.r_[min(abounds): max(abounds): 301j]
trange = np.r_[min(tbounds): max(tbounds): 300j]

cmap = plt.cm.magma_r
cmap.set_under(color='w')

#Create matchedFilter CIR estimate
cirEstimate=matchedFilter.LFM_MatchedFilter(toi)
mf=cirEstimate.matchedFilter()

#create wave front delay calculator
ssp = thermister.getSoundSpeed(toi)
wf = curvedWaveFront.WavefrontDelays(ssp, subarray.values)

tbounds_up = np.array([[-4.4516129 , -4.23387097],
                    [-4.73387097, -4.50806452],
                    [-5.03494624, -4.83870968],
                    [-5.27419355, -5.09946237],
                    [-5.57795699, -5.38978495],
                    [-5.86021505, -5.64784946],
                    [-6.00806452, -5.80645161],
                    [-6.30107527, -6.12096774],
                    [-6.57795699, -6.3844086 ],
                    [-6.84139785, -6.62096774],
                    [-7.10215054, -6.87634409],
                    [-7.19354839, -6.97043011],
                    [-7.43010753, -7.09946237],
                    [-7.60483871, -7.22043011],
                    [-7.71505376, -7.25      ],
                    [-7.85215054, -7.36290323]])

cc = pairwise_corr.ChannelCorrelator(mf, tbounds_up)
corr_da = cc()
tracks = peak_tracking.optimal_delays(cc)

offsets = tracks - tracks.mean('slowtime')

def init_beamformer(cirs, shifts):
    """Return a time shift corrected beamformer"""
    for cir, (_, off) in zip(cirs, shifts.groupby('slowtime')):
        bf_er = timeDomain.ShiftAndAdd(cir, upsample=5, channels=subarray)
        bf_er.t_shift = off[channels]
        yield bf_er

# plots for movie
def plot_subchannel(enum_bf_er):
    """plot beamformer response with user supplied optimal offsets"""
    bf_er = enum_bf_er[1]

    bf = bf_er(trange, wf(arange))
    bf_db = 20 * np.log10(np.abs(bf))
    fig, ax = plt.subplots()
    mesh = bf_db.plot(ax)
    mesh.set_clim(95, 105)
    #mesh.set_clim(85, 100)
    mesh.set_cmap(cmap)
    ax.set_ylim(min(arange), max(arange))
    ax.grid()
    plt.savefig(join(save_dir, 'fig_%03d' % enum_bf_er[0]))
    #plt.close(fig)

#plot all bf_ers
#[plot_subchannel(b) for b in enumerate(init_beamformer(mf, offsets))]

