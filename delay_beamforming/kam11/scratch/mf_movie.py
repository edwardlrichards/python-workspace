import matplotlib
matplotlib.use("Agg")
import numpy as np
import xarray as xa
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from kam11 import context, thermister, matchedFilter
from plotting import beam_plot
from beamforming import curvedWaveFront

toi = datetime.datetime.strptime('2011 J184 03:51:00', '%Y J%j %H:%M:%S')
ssp = thermister.getSoundSpeed(toi)
rd = context.VLA1().phoneDepths()
wf = curvedWaveFront.WavefrontDelays(ssp, rd)
wf.bellhop_correction()

plot_angle = 6.5

mf = matchedFilter.mf_bytime(toi)

fig, ax = plt.subplots()
for i, n in enumerate(mf):
    ax.clear()
    beam_plot.plot_ts(n, channel_delays=wf(plot_angle),
            tbounds=(-5, -3.5), overaxis=ax)
    ax.set_title('taken %s'%(
        datetime.datetime.strftime(n.attrs['timestamp'], 'J%j %M:%S.%f')))
    fig.savefig('/home/e2richards/Documents/kam11_beamforming/mf_plot/fig_%03i.png'%i)
