from kam11 import time_beamforming, context
from kam11.delay_optimization import positionPertubation
from plotting import beam_plot
import matplotlib.pyplot as plt
import datetime
import numpy as np

abounds = (-8, 8)
tbounds = (-6, -1)
tbounds_2 = (-8, -1)

subarray = context.VLA1().phoneDepths()[:6]
subarray_2 = context.VLA1().phoneDepths()[5:9]
toi = datetime.datetime.strptime('2011 J184 03:51:12', '%Y J%j %H:%M:%S')
bf_er, wf = time_beamforming.load_beamformer(toi, channels=subarray)
bf_er_2, _ = time_beamforming.load_beamformer(toi, channels=subarray_2)

trange = np.r_[min(tbounds): max(tbounds): 301j]
trange_2 = np.r_[min(tbounds_2): max(tbounds_2): 301j]
arange = np.r_[min(abounds): max(abounds): 300j]

def plot_db(bf_out, vbounds=None):
    """convert beamformer output to db"""
    bf = 20 * np.log10(np.absolute(bf_out + np.spacing(1)))
    fig, ax = plt.subplots()
    bf.plot(ax)
    ax.set_ylim(min(arange), max(arange))
    return fig, ax

#optimize around a beamformer angle guess
guess_a = 6.5
guess_t = (-6.5, -3.8)

guess_delay = wf(guess_a).values
tbounds = guess_delay + np.array(guess_t)
max_er = positionPertubation.BestArrival(bf_er)
taus = max_er(tbounds)

subI = subarray.index.values - 1
correction = guess_delay.squeeze()[subI] - taus.squeeze()
#correction[3] -= -0.06461000000000006

bf_er.t_shift = -correction

#sub array 2
guess_delay = wf(guess_a).values
tbounds = guess_delay + np.array(guess_t)
max_er = positionPertubation.BestArrival(bf_er_2)
taus = max_er(tbounds)

subI = subarray_2.index.values - 1
correction -= correction[0]
correction = guess_delay.squeeze()[subI] - taus.squeeze()
bf_er_2.t_shift = -correction

fig, ax = plot_db(bf_er(trange, wf(arange)))
ax.get_images()[0].set_clim(90, 105)

fig, ax = plot_db(bf_er_2(trange_2, wf(arange)))
ax.get_images()[0].set_clim(80, 105)

plt.show(block=False)
