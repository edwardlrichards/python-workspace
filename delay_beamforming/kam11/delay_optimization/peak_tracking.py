import scipy.signal as sig
import numpy as np
import copy
from xarray import DataArray, concat
from pandas import Index
import matplotlib.pyplot as plt

def optimal_delays(channel_correlator, track_shift=None):
    """Run single channel tracker on all channels"""
    #run each single channel that isn't the reference depth
    corr_depths = [d for d in channel_correlator.mf.rd if d != channel_correlator.ref_depth]
    if track_shift is None:
        tracks = [ChannelPeakTracker(channel_correlator.all_corr, d)() for d in corr_depths]
    else:
        tracks = [ChannelPeakTracker(channel_correlator.all_corr, d)(track_shift=ts) for d, ts in zip(corr_depths, track_shift)]

    #Put the gross time shift back into each channel
    start_times = np.mean(channel_correlator.tau_bounds, axis=1)
    ref_I = (channel_correlator.mf.rd == channel_correlator.ref_depth).values
    start_times = start_times[np.bitwise_not(ref_I)] - start_times[ref_I]
    #simply return the track delays as an xr DataArray
    tracks = [st - t.track for t, st in zip(tracks, start_times)]
    #Put zeros back into reference track
    tracks = concat(tracks, Index(corr_depths, name='depth'))
    tracks = tracks.reindex(depth=channel_correlator.mf.rd)
    tracks = tracks.fillna(0.)
    return tracks


def plot_demeaned(tracks):
    """plot each of the demeaned track values"""
    fig, ax = plt.subplots()
    for d, t in tracks.groupby('depth'):
        label = '%.2f' % d
        ts = t - np.mean(t)
        ax.plot(ts.slowtime, ts.values, label=label)
    # ax.legend()
    ax.set_title('peak correlation delay of each channel, demeaned')
    ax.set_xlabel('slow time, sec')
    ax.set_ylabel('delay, ms')
    return fig, ax


def ly_plot(tracks):
    """same plot as plot_demeaned, just with plotly"""
    import plotly
    plotly.plotly.sign_in("EdwardLRichards", "8c0uzeh94k")
    demean = [(t - np.mean(t), d) for d, t in tracks.groupby('depth')]
    traces = [plotly.graph_objs.Scatter(
        x=d.slowtime.values, y=d.values, name='%.2f' % rd) for d, rd in demean]
    plotly.plotly.iplot(traces)


class ChannelPeakTracker:
    """Cranks the tracking mechanism from a correllation da"""

    def __init__(self, correlation_da, channel, max_jump=0.02, max_missed_counts=2):
        """Set up a single channel peak tracker"""
        self.one_channel = correlation_da.sel(depth=channel)
        self.channel = channel
        self._get_peak_list()
        self.nn = NearestNeigbor(
            self.peak_list, channel, max_jump, max_missed_counts)

    def __call__(self, track_shift=0):
        """return strongest track"""
        return self.nn(track_shift)

    def longer_tracks(self, min_track_length):
        """Return all tracks that are a minimum length"""
        return self.nn.longer_tracks(min_track_length)

    def plot_onetime(self, slowtime, ax=None):
        """Plot time series with peaks at a given slowtime"""
        if ax is None:
            fig, ax = plt.subplots()
        else:
            fig = ax.get_figure()
        ts = self.one_channel.sel(slowtime=slowtime, method='nearest')
        plI = Index(self.one_channel.slowtime).get_loc(slowtime, 'nearest')
        peaks = self.peak_list[plI][1]
        peak_delay = [p['tau'] for p in peaks]
        peak_amp = [p['amp'] for p in peaks]
        ts.plot(ax)
        plt.plot(peak_delay, peak_amp, 'bo')
        return fig, ax

    def _get_peak_list(self):
        """return peaks for a given channel in the correlation xr da"""
        peak_finder = self._peak_finder('savgol')
        peak_list = []
        for i, ts in self.one_channel.groupby('slowtime'):
            peaks = peak_finder(ts)
            ts_values = self.one_channel.delay.isel(delay=peaks)
            amp_values = self.one_channel[dict(delay=peaks)].sel(slowtime=i)
            arrival_list = [dict(tau=ts.values, amp=amp.values)
                            for ts, amp in zip(ts_values, amp_values)]
            peak_list.append((i, arrival_list))
        self.peak_list = peak_list

    def _peak_finder(self, peak_type):
        """return a peak finding algorithm that takes a 1d time series"""
        if peak_type == 'cwt':
            peak_finder = lambda corr_ts: sig.find_peaks_cwt(
                corr_ts, np.arange(5, 20), noise_perc=15)
        elif peak_type == 'savgol':
            def peak_finder(ts, fudge_factor=3):
                """peak finder based on savgol filter
                find max within span of fudge factor
                """
                deriv_ts = sig.savgol_filter(ts.values, 11, 2, deriv=1)
                d_sign = np.diff(np.sign(deriv_ts))
                d_sign = np.concatenate(([0], d_sign))
                # Returning a list may not be the most convient in the future
                peakI = np.arange(d_sign.size)[d_sign == -2]

                # Fudge factor allows the peak to shift a bit to hit max value
                #shadingI = np.arange(-fudge_factor, fudge_factor)
                #for i, pI in enumerate(peakI):
                    #peak_range = shadingI + pI
                    #maxI = np.argmax(ts.values[peak_range])
                    #peakI[i] += shadingI[maxI]
                return peakI.tolist()

        else:
            raise Exception('peak finder type not known')
        return peak_finder


class NearestNeigbor:
    """Create a peak tracker that works on a list of peak position lists"""

    def __init__(self, peaks, rd, max_jump=0.02, max_missed_counts=2):
        """peaks is a list of peak position lists, each eg find_peaks_cwt"""
        self.max_jump = max_jump
        self.max_missed_counts = max_missed_counts
        self.rd = rd
        # Initilize tracker
        self.start_peaks, *self.peaks = peaks
        self.track_list = None
        self._track()

    def __call__(self, track_shift):
        """Return the strongest track, track shift is relative to strongest track"""
        track_strength = [t.strength for t in self.track_list]
        return self.track_list[track_strength.index(max(track_strength)) + track_shift]

    def longer_tracks(self, min_track_length):
        """return tracks greater than a certain length"""
        return [t for t in self.track_list if t.length > min_track_length]

    def _track(self):
        """Run tracker"""
        peak_list = copy.deepcopy(self.peaks)
        self._init_tracks()
        for i, pl in peak_list:
            active_tracks = [
                track for track in self.track_list if track.is_active]
            for t in active_tracks:
                t.check_peaks(i, pl)
            self._add_tracks(i, pl)

    def _init_tracks(self):
        """Initilize empty track list"""
        self.track_list = []
        self._add_tracks(*self.start_peaks)

    def _add_tracks(self, i, peak_list):
        """Add a track for each element in peak list"""
        for p in peak_list:
            self.track_list.append(
                Track(i, p, self.rd, self.max_jump, self.max_missed_counts))


class Track:
    """A list of indicies that represent a peak track"""

    def __init__(self, start_time, track_start, rd, max_jump, max_missed_counts):
        self.track_list = [track_start]
        self.slowtime = [start_time]
        self.max_jump = max_jump
        self.max_missed_counts = max_missed_counts
        self.is_active = True
        self.missed_counts = 0
        self.rd = rd

    @property
    def track(self):
        """return track as a xr DataArray"""
        track = DataArray([t['tau'] for t in self.track_list],
                          dims=['slowtime'], coords=[self.slowtime])
        return track

    @property
    def length(self):
        """number of points in track"""
        return len(self.track_list)

    @property
    def strength(self):
        """Integrated value of corrilation under track"""
        int_amp = np.nansum(np.array([t['amp'] for t in self.track_list]))
        return int_amp

    def check_peaks(self, slowtime, peak_list):
        """Check index values in peak list and see if any fit"""
        self.slowtime.append(slowtime)
        if len(peak_list) == 0:
            self.missed_counts += 1
            self.track_list.append(dict(tau=np.nan, amp=np.nan))
            return peak_list

        # extract delays from arrivals
        taus = [t['tau'] for t in peak_list]
        # find nearest arrival
        miss_amount = np.abs(np.array(taus) - self.track_list[-1]['tau'])
        best_track = np.argmin(miss_amount)
        if miss_amount[best_track] < self.max_jump:
            self.track_list.append(peak_list[best_track])
            peak_list.pop(best_track)
            self.missed_counts = 0
        else:
            self.missed_counts += 1
            self.track_list.append(dict(tau=np.nan, amp=np.nan))
            if self.missed_counts > self.max_missed_counts:
                self.is_active = False

if __name__=="__main__":
    from datetime import datetime, timedelta
    from kam11 import context, matchedFilter, thermister, select_arrivals
    from beamforming import curvedWaveFront
    from kam11.delay_optimization import pairwise_corr

    toi = datetime.strptime('2011 J184 03:51:00', '%Y J%j %H:%M:%S')
    subchannels = context.VLA1().phoneDepths()

    tbounds = (-7, -3.5)
    tbounds = (-8, -4)

    cmap = plt.cm.magma_r
    cmap.set_under(color='w')

    #Create matchedFilter CIR estimate
    ttime = 23  # seconds
    loadDuration = timedelta(0, ttime)
    #cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
    cirEstimate=matchedFilter.LFM_MatchedFilter(toi)
    mf=cirEstimate.matchedFilter()

    #create wave front delay calculator
    ssp = thermister.getSoundSpeed(toi)
    wf = curvedWaveFront.WavefrontDelays(ssp, subchannels.values)
    #tbounds_up = select_arrivals.select_arrivals(mf)

    tbounds_up = np.array([[-4.4516129 , -4.23387097],
                        [-4.73387097, -4.50806452],
                        [-5.03494624, -4.83870968],
                        [-5.27419355, -5.09946237],
                        [-5.57795699, -5.38978495],
                        [-5.86021505, -5.64784946],
                        [-6.00806452, -5.80645161],
                        [-6.30107527, -6.12096774],
                        [-6.57795699, -6.3844086 ],
                        [-6.84139785, -6.62096774],
                        [-7.10215054, -6.87634409],
                        [-7.19354839, -6.97043011],
                        [-7.43010753, -7.09946237],
                        [-7.60483871, -7.22043011],
                        [-7.71505376, -7.25      ],
                        [-7.85215054, -7.36290323]])

    cc = pairwise_corr.ChannelCorrelator(mf, tbounds_up)
    corr_da = cc()
    tracks = optimal_delays(cc)
