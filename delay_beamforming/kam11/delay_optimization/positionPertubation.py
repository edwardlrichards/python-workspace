import numpy as np
from pandas import DataFrame, Series
from xarray import DataArray, concat
from beamforming import curvedWaveFront, beam_correlations
from plotting import beam_plot
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline as interper
from kam11 import context

class BestArrival:
    """Return delays that best create a beamformed arrival from initial guess"""
    def __init__(self, beamformer, delay_function=None):
        self.bf_er = beamformer
        self.wf_delays = delay_function

    def max_er(self, tbounds, abounds, max_shift=0.3):
        """best delays from initial beamformer guess"""
        if self.wf_delays is None:
            Exception('Delay function not specified')
        beam_ts = self.selected_ts(tbounds, abounds,
                                              max_shift=max_shift)
        max_angle = beam_ts.attrs['max_angle']

        opt_phase = DelayMax(beam_ts)
        theta_test = np.r_[-np.pi: np.pi: 200j]
        time_shift = opt_phase.optimal_delay(theta_test)
        optimal_delay = self.wf_delays(max_angle) +\
                np.squeeze(time_shift)
        return np.squeeze(optimal_delay)

    def __call__(self, tbounds):
        """best delays from initial beamformer guess
        tIndex is used to select the tbounds used by beamformer. Default is
        the first channels
        """
        beam_ts = self.selected_ts(tbounds)
        mean_tau = np.mean(tbounds, axis=-1)

        #Zero is the auto correlation maximum
        chan_tau = [0.]
        corr_ts = self.pairwise_correlation(beam_ts)
        taus = corr_ts.delay.values

        for _, chan in corr_ts.groupby('depth'):
            #I think abs is a bad idea
            #chan = np.abs(chan)
            chan = np.real(chan)
            chan_tau.append(taus[np.argmax(chan)])

        chan_tau = np.array(chan_tau)
        chan_tau = np.cumsum(chan_tau)
        #select the delays on channels used by beamformer
        best_delay = mean_tau[self.bf_er.channelI] - chan_tau
        # reference delay to channel 0
        return best_delay - best_delay[0]

    def selected_ts(self, tbounds):
        """intepolate a time series for the time of interest
        tbounds is an Nx2 array of arrival time bounds
        if windowed is True, restrict time series to 0.15 ms around peak
        """
        mean_tau = np.mean(tbounds, axis=-1)
        demean_tau = tbounds - mean_tau[:, None]
        union_tbounds = (np.min(demean_tau), np.max(demean_tau))
        beam_ts = self.bf_er.ts_atdelay(mean_tau, tbounds=union_tbounds)
        #beam_ts only allows for a single tbounds specification, loop
        #makes tbounds specific to each channel
        for i, chan in enumerate(beam_ts.groupby('depth')):
            taxis = beam_ts.time.values
            tlim = demean_tau[i] + mean_tau[0]
            tI = (taxis < np.min(tlim)) |\
                 (taxis > np.max(tlim))
            beam_ts[dict(depth=i)][tI] = 0

        return beam_ts

    def pairwise_correlation(self, beam_ts, ref_depth=None):
        """correlate each channel with a reference channel"""
        dt = float(beam_ts.time[1] - beam_ts.time[0])
        taus = np.arange(beam_ts.shape[1] * 2 - 1, dtype='float_')
        taus -= (beam_ts.shape[1] - 1)
        taus *= dt
        channels = []
        corr_depths = beam_ts.depth.isel(depth=slice(1, None))

        if ref_depth is not None:
            #Use a single reference channel
            ref_ts = beam_ts.sel(depth=ref_depth)

        for i, d in enumerate(corr_depths):
            if ref_depth is None:
                #Correlate each channel with the one above it
                ref_ts = beam_ts.isel(depth=i)

            chan = np.correlate(ref_ts, beam_ts.sel(depth=d),
                                mode='full')
            channels.append(chan)

        corr_ts = DataArray(channels, dims=['depth', 'delay'],
                                 coords=[corr_depths, taus])
        return corr_ts


class DelayMax:
    """Find the optimal beamformer lineup for array"""
    def __init__(self, timeseries):
        """Compute optimal delay values for baseband beamformer"""
        self.phasemax = []
        for chan in timeseries:
            self.phasemax.append(self._make_phasemax(chan))
        self.ts = timeseries

    def max_value(self, phase):
        """return the max value at a given phase"""
        maxvalues = []
        for chan in self.phasemax:
            maxvalues.append(np.squeeze(chan.max_value(phase)))
        return np.array(maxvalues)

    def max_time(self, phase):
        """return the max value at a given phase"""
        maxtime = []
        for chan in self.phasemax:
            maxtime.append(np.squeeze(chan.max_time(phase)))
        return np.array(maxtime)

    def maxsum(self, phase):
        """Return the sum of all maxvalues in a single series"""
        all_max = self.max_value(phase)
        return np.sum(all_max, axis=0)

    def optimal_delay(self, phase):
        """return delays which line up each channel at the optimal phase"""
        maxsum = self.maxsum(phase)
        bestphase = phase[np.argmax(maxsum)]
        # index of max(phase) is the time at which the max occurs
        besttime = self.max_time(bestphase)
        return besttime - besttime[0]

    def _make_phasemax(self, chan):
        """construct a PhaseMax object for a channel of data"""
        sections = []
        for mono in self._make_monotonic(chan):
            sections += self._unique_phase(mono)
        #It takes 2 to make a linear interpolator
        len2 = filter(lambda x: x.size >= 2, sections)
        return PhaseMax(list(len2))

    def _make_monotonic(self, chan):
        """generator to return all monotonic segements"""
        segments = chan.copy()
        first_deriv = np.array(np.diff(np.unwrap(np.angle(chan))))
        sign_change = np.abs(np.diff(np.sign(first_deriv)))
        atend = False
        while not atend:
            #edge case where last sign change is at last index
            if sign_change.size == 0:
                break
            #add 2 to account for both differencing operations
            signI = np.argmax(sign_change) + 2
            #value of 2 when there is no sign change
            if signI == 2:
                atend = True
                yield segments
            #split array and start again
            segment = segments[: signI]
            segments = segments[signI: ]
            sign_change = sign_change[signI:]
            first_deriv = first_deriv[signI:]
            yield segment

    def _unique_phase(self, seg):
        """generator to split monotonic segment into phase stable segments"""
        # if segment has less than 3 elements don't bother
        if seg.size <= 3:
            return seg
        # find first index outside of unwraped region
        segphase = np.array(np.unwrap(np.angle(seg)))
        atend = False
        while not atend:
            # go 2 pi from current phase
            firstwrap = np.argmax(np.abs(segphase[0] - segphase) > 2 * np.pi)
            # test for end condition, where no index meets criteria
            if firstwrap == 0:
                atend = True
                yield seg
            section = seg[: firstwrap]
            seg = seg[firstwrap: ]
            segphase = segphase[firstwrap: ]
            yield section


class PhaseMax:

    def __init__(self, sectionedTimeSeries):
        """Find the minimum number of sections for maximum phase function"""
        self._maxlist = []
        self._timelist = []
        self._unwraplist = []
        self._create_maxinterp(sectionedTimeSeries)

    def max_value(self, phase):
        """return the maximum value at a given phase"""
        max_values, _ = self._maxinterper(phase)
        return max_values

    def max_time(self, phase):
        """return the time at which a maximum occurs at a given phase"""
        _, max_time = self._maxinterper(phase)
        return max_time

    def _maxinterper(self, angles):
        """An interpolator that returns only the maximum value"""
        max_values = []
        max_times = []
        for unwrap, interp, time in zip(self._unwraplist, self._maxlist,
                                        self._timelist):
            max_values.append(interp(unwrap(angles)))
            max_times.append(time(unwrap(angles)))

        #If no interpolator is defined, simply return NaN values
        if len(max_values) == 0:
            no_value = np.empty(angles.shape)
            no_value.fill(np.nan)
            return no_value, no_value

        max_values = np.array(max_values)
        max_times = np.array(max_times)
        maxarg = np.argmax(max_values, axis=0)
        max_values = max_values[maxarg, np.arange(maxarg.size)]
        max_times = max_times[maxarg, np.arange(maxarg.size)]
        return max_values, max_times

    def _create_maxinterp(self, sections):
        """an interpolator for the segment with the max"""
        # Add a new interpolator if any point of current version is less than
        # new section
        for section in self._popmax(sections):
            #find the magnitude of the interpolator at points in section
            mag_interp = self.max_value(np.angle(section))
            #NaN values represent unsampled areas
            mag_interp = np.nan_to_num(mag_interp)
            if np.any(mag_interp < np.abs(section)):
                self._add_interps(section)

    def _popmax(self, sections):
        """pop the section with the largest energy value"""
        while len(sections) > 0:
            segmax = np.array([np.max(np.abs(s)) for s in sections])
            # empty lists create nan max values
            if not np.all(np.isnan(segmax)):
                yield sections.pop(np.nanargmax(segmax))
            else:
                return

    def _add_interps(self, maxsection):
        """Add a section to max interpolator"""
        angle = np.unwrap(np.angle(maxsection))
        sortI = np.argsort(angle)
        newmax = interper(angle[sortI], np.abs(maxsection.values)[sortI],
                          ext=1, k=2)
        newtime = interper(angle[sortI], maxsection.time[sortI], ext=1, k=2)
        newunwrapper = self._create_unwraper(angle[sortI])
        # Append interpolators to lists
        self._maxlist.append(newmax)
        self._timelist.append(newtime)
        self._unwraplist.append(newunwrapper)

    def _create_unwraper(self, angle):
        """method to go between wraped and unwraped axes for input segment"""
        startangle = angle[0]
        direction = np.sign(angle[1] - angle[0])
        needcorrection = (startangle, -direction * np.pi)

        def angle_unwrap(inangle):
            """method to go from -pi to pi axis to one used in section"""
            # ensure input is from -pi to pi
            inangle = np.array(inangle)
            inangle = np.angle(np.exp(1j * inangle))
            inangle = np.array(inangle, ndmin=1)
            if direction > 0:
                wrapI = inangle < startangle
            else:
                wrapI = inangle > startangle
            inangle[wrapI] += direction * 2 * np.pi
            return inangle
        return angle_unwrap
