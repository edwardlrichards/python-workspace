import datetime as dt
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import scipy.signal as sig
from kam11 import thermister, matchedFilter, context, select_arrivals
from kam11.delay_optimization import positionPertubation
from beamforming import timeDomain, curvedWaveFront
from plotting import beam_plot, mf_plot
import concurrent.futures


class ChannelCorrelator:
    """works on kam11 matched filter output and delay bounds"""
    def __init__(self, mf, tau_bounds, ref_depth=None):
        """Set up cross-correlation, but no heavy lifting
        mf: kam11 matched filter object
        tau_bounds: delay bounds in list of 2 values
        ref_depth: default is lowest channel
        """
        self.mf = mf
        self.tau_bounds = tau_bounds
        if ref_depth is None:
            self.ref_depth = self.mf.mf_dict['rd'].iloc[0]
        else:
            self.ref_depth = ref_depth
        self.all_corr = None

    def __call__(self):
        """Correlate for all times and channels"""
        #TODO: calculate correlation here, and upsample after correlation
        #get_bfer = lambda i: timeDomain.ShiftAndAdd(self.mf[i],
                                                    #upsample=30)
        get_bfer = lambda i: timeDomain.ShiftAndAdd(self.mf[i],
                                                    upsample=None)
        bf_ers = map(get_bfer, range(self.mf.mf_dict['num_samples']))
        def correlate_allchannel_pairwise(bf_er):
            """given a beamformer, correlate all channels avalible"""
            max_er = positionPertubation.BestArrival(bf_er)
            #Use bottom channel as reference
            beam_ts = max_er.selected_ts(self.tau_bounds, windowed=False)
            pc = max_er.pairwise_correlation(beam_ts, ref_depth=self.ref_depth)
            return pc

        all_channels = list(map(correlate_allchannel_pairwise, bf_ers))
        # concat values for each mf measurement
        slowtime = pd.Index(self.mf[1].slowtime, name='slowtime')
        all_corr = xr.concat(all_channels, slowtime)
        #upsample slowtime
        values = all_corr.values
        taxis = all_corr.delay.values
        up_values, up_t = sig.resample(values, taxis.size * 30, axis=-1, t=taxis)
        up_values = np.real(up_values)
        all_corr = xr.DataArray(up_values, dims=['slowtime', 'depth', 'delay'], coords=[all_corr.slowtime, all_corr.depth, up_t])
        self.all_corr = all_corr
        return all_corr

    def plot_channel_corr(self, channel_depth, slowtime=None):
        """draw correlation for one channel, slowtime is used as bounds"""
        if self.all_corr is None:
            raise Exception('Need to run Correlation before plotting')
        pc = self.all_corr.sel(depth=channel_depth)
        if slowtime is not None:
            start_time = np.min(slowtime)
            end_time = np.max(slowtime)
            if start_time == end_time:
                pc = pc.sel(slowtime=start_time)
            else:
                pc = pc.sel(slowtime=slice(start_time, end_time))

        fig, ax = plt.subplots()
        pc.plot()
        maxI = pc.argmax('delay').values
        plt.title('Pairwise correlations for depth %.1f'%channel_depth)
        return fig, ax
