from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import os
import pickle
from kam11 import context, matchedFilter, thermister, select_arrivals
from beamforming import curvedWaveFront
from kam11.delay_optimization import pairwise_corr, peak_tracking

# peak track over a strech of data, plot and save the results

toi = datetime.strptime('2011 J184 05:51:00', '%Y J%j %H:%M:%S')
subchannels = context.VLA1().phoneDepths()
tbounds = (-8, -4)

cmap = plt.cm.magma_r
cmap.set_under(color='w')

#Create matchedFilter CIR estimate
#ttime = 23  # seconds
#loadDuration = timedelta(0, ttime)
#cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
cirEstimate=matchedFilter.LFM_MatchedFilter(toi)
mf=cirEstimate.matchedFilter()

#create wave front delay calculator
ssp = thermister.getSoundSpeed(toi)
wf = curvedWaveFront.WavefrontDelays(ssp, subchannels.values)
tbounds_up = select_arrivals.select_arrivals(mf)

cc = pairwise_corr.ChannelCorrelator(mf, tbounds_up)
corr_da = cc()
tracks = peak_tracking.optimal_delays(cc)

# setup saving framework
save_dir = os.path.realpath(__file__)
_, curr_dir = os.path.split(save_dir)
save_dir = os.path.join(curr_dir, tracks)

save_name = datetime.strftime(toi, 'J%j_%H_%M.nc')
save_file = os.path.join(save_dir, save_name)

with open(save_file, 'wb') as f:
    pickle.dump(tracks, f)

# example load code
#with open('J184_05_51.nc', 'rb') as f:
    #tracks = pickle.load(f)
