import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from kam11 import context, matchedFilter, thermister, select_arrivals
from beamforming import curvedWaveFront
from kam11.delay_optimization import pairwise_corr, peak_tracking
from plotting import beam_plot

# Infer array shape as residual from beamformer at best angle

toi = datetime.strptime('2011 J184 03:51:00', '%Y J%j %H:%M:%S')
subchannels = context.VLA1().phoneDepths()

tbounds = (-7, -3.5)
tbounds = (-8, -4)
#5.1 is about the cuttoff of wave-front propagation to the highest element
abounds = (5.1, 7.5)
tilt_bounds = (-1.3, 3)

#Set arrival angle
#a_in = 5.1
a_in = 6.5
#a_in = 5.5

cmap = plt.cm.magma_r
cmap.set_under(color='w')

# Create matchedFilter CIR estimate
ttime = 50  # seconds
loadDuration = timedelta(0, ttime)
#cirEstimate=matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
cirEstimate = matchedFilter.LFM_MatchedFilter(toi)
mf = cirEstimate.matchedFilter()

# create wave front delay calculator
ssp = thermister.getSoundSpeed(toi)
wf = curvedWaveFront.WavefrontDelays(ssp, subchannels.values)
wf.bellhop_correction(ssp)

tbounds_up = np.array([[-4.4516129 , -4.23387097],
[-4.73387097, -4.50806452],
[-5.03494624, -4.83870968],
[-5.27419355, -5.09946237],
[-5.57795699, -5.38978495],
[-5.86021505, -5.64784946],
[-6.00806452, -5.80645161],
[-6.30107527, -6.12096774],
[-6.57795699, -6.3844086 ],
[-6.84139785, -6.62096774],
[-7.10215054, -6.87634409],
[-7.19354839, -6.97043011],
[-7.43010753, -7.09946237],
[-7.60483871, -7.22043011],
[-7.71505376, -7.25      ],
[-7.85215054, -7.36290323]])

# Prompt user for best guess at arrivals
#tbounds_up = select_arrivals.select_arrivals(mf, start_t=tbounds[0], end_t=tbounds[1],
                                             #plot_channels=subchannels)

cc = pairwise_corr.ChannelCorrelator(mf, tbounds_up)
corr_da = cc()
#user specifed track shifts
track_shift = [0, 0, -1, -1, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0]
#track_shift = None
tracks = peak_tracking.optimal_delays(cc, track_shift=track_shift)

# find angle that explains the most of the delay
angles = np.r_[min(abounds): max(abounds): 300j]
tilts = np.r_[min(tilt_bounds): max(tilt_bounds): 301j]

#run through tilt, arrival angle space
taus = wf(angles, array_tilt=tilts)
error = np.abs(tracks - taus).sum('depth', skipna='True')

#error is totally boring, seems to be driving the angle of arrival as low as possible
tiltI = [int(st.sel(angle=a_in, method='nearest').argmin()) for _, st in error.groupby('slowtime')]
min_tilt = tilts[tiltI]

opt_delays = np.squeeze(wf(a_in, array_tilt=min_tilt).values)
opt_delays = xr.DataArray(opt_delays, dims=['depth', 'slowtime'],
                            coords=[tracks.depth, tracks.slowtime])
residual = tracks - opt_delays

# estimate array position from rough horizontal slowness
def postition_pertubation(residual, angle):
    """Use horizontal slowness to predict arrival time pertubation"""
    s = 1 / 1529 * 1e3
    p = np.cos(np.radians(angle)) * s
    dx = residual / p
    return dx

array_pertubation = [postition_pertubation(res, a_in) for _, res in
                     residual.groupby('slowtime')]
array_pertubation = xr.concat(array_pertubation, 'slowtime')

# plot results

#confirm peak lineups with a random optimal lineup plot
repI = 30
fig, ax = beam_plot.plot_ts(mf.single_index(repI), channel_delays=tracks.isel(slowtime=repI), tbounds=(-5, -3.5))
ax.set_title('Example of time series line-up, measurement %i' %repI)
ax.set_ylabel('optimal delays, ms')
ax.set_xlabel('slowtime, s')
#ax.set_title('Strongest track of peak correlation delay for each channel')
ax.set_title('')

fig, ax = peak_tracking.plot_demeaned(tracks)

fig, ax = plt.subplots()
[t.plot(ax) for _, t in tracks.groupby('depth')]
ax.set_ylabel('optimal delays, ms')
ax.set_xlabel('slowtime, s')
ax.set_title('Track of correlation delay for each channel')

fig, ax = plt.subplots()
plt.plot(tracks.slowtime, tilts[tiltI])
plt.xlabel('slowtime, s')
plt.ylabel('Array tilt angle, degrees')
plt.title('Array tilt angle, %0.2f degree angle of arrival' %a_in)

fig, ax = plt.subplots()
[ax.plot(arr.values, arr.depth, color='b')
 for _, arr in array_pertubation.groupby('slowtime')]
ax.set_title('Infered array position, %.2f degree angle of arrival'%a_in)
ax.set_ylim(100, 40)
ax.set_xlim(-.2, 0.3)
ax.set_xlabel('Array pertubation, m')
ax.set_ylabel('Element depth, m')

plt.show(block=False)
