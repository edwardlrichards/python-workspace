import matplotlib
matplotlib.use("Agg")
import datetime as dt
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import thermister, matchedFilter, context, select_arrivals
from kam11.delay_optimization import positionPertubation
from beamforming import timeDomain, curvedWaveFront
from plotting import beam_plot, mf_plot
from atpy import bellhop, write_at
import concurrent.futures
from os.path import join
import pickle

savedir = '/home/e2richards/Documents/kam11_beamforming/progress_01_15/sub_channel_55'

toi = dt.datetime.strptime('2011 J184 03:55:00', '%Y J%j %H:%M:%S')
subchannels = context.VLA1().phoneDepths()[:5]
subchannels = context.VLA1().phoneDepths()[:6]

cmap = plt.cm.magma_r
cmap.set_under(color='w')

#Import residuals from another routine
with open('/home/e2richards/python-workspace/kam11/delay_optimization/scratch/res60.pic', 'rb') as f:
    residual = pickle.load(f)

#Import associated tilts
with open('/home/e2richards/python-workspace/kam11/delay_optimization/scratch/tilt60.pic', 'rb') as f:
    tilts = pickle.load(f)

# Create matchedFilter CIR estimate
ttime = 20  # seconds
loadDuration = dt.timedelta(0, ttime)
#cirEstimate = matchedFilter.LFM_MatchedFilter(toi, loadDuration=loadDuration)
cirEstimate=matchedFilter.LFM_MatchedFilter(toi)
mf = cirEstimate.matchedFilter()

# create wave front delay calculator
ssp = thermister.getSoundSpeed(toi)
wf_delays = curvedWaveFront.WavefrontDelays(ssp, subchannels.values)
#wf_delays = curvedWaveFront.WavefrontDelays(ssp, context.VLA1().phoneDepths())

# Compute a curvature correction from Bellhop
#wf_delays.bellhop_correction(ssp)

#common routine for generating beamformers

get_bfer = lambda i: timeDomain.ShiftAndAdd(mf.single_index(i),
                                            upsample=5, channels=subchannels)

def all_bf_ers():
    """step through the beamformers"""
    for i in range(mf.mf_dict['num_samples']):
        yield i, get_bfer(i)

def plot_angle_tilt(enum_bf_er):
    """plot arrival in angle, tilt space"""
    d_seconds = dt.timedelta(0, mf.mf_dict['mf_dt'][
                             enum_bf_er[0]] - mf.mf_dict['time'].second)
    title = 'First arrival beamformer energy\n %s' % dt.datetime.strftime(
        mf.mf_dict['time'] + d_seconds, 'J%j %H:%M:%S')
    test = angle_tilt_energy(enum_bf_er[1], look_angle, look_tilt)
    test = 20 * np.log10(np.abs(test))
    test -= np.max(test)
    fig, ax = plt.subplots()
    cm = test.T.plot(ax, cmap=cmap, vmin=-10)
    ax.grid()
    ax.set_xlabel('tilt, degrees')
    ax.set_ylabel('angle of arrival, degrees')
    ax.set_title(title)
    plt.plot([np.min(look_tilt), max(look_tilt)], [5.067, 5.067], 'b', alpha=0.8)
    #ax.annotate('refraction cutoff', xy=(2, 5.067), xytext=(1.5, 5.5),
                        #arrowprops=dict(facecolor='black', shrink=0.05),)
    ax.set_xlim(np.min(look_tilt), max(look_tilt))
    ax.set_ylim(np.min(look_angle), max(look_angle))
    plt.savefig(join(savedir, 'fig%03d' % enum_bf_er[0]))
    plt.close(fig)

def plot_opt(enum_bf_er):
    """plot beamformer response with user supplied optimal offsets"""
    t_range = (-5, -0.5)
    a_range = (-8, -2)

    bf_er = enum_bf_er[1]

    bf_er.t_shift = np.array(residual.isel(slowtime=enum_bf_er[0]))
    taus = wf_delays(np.r_[min(a_range): max(a_range): 201j], array_tilt=tilts[enum_bf_er[0]])
    taus = taus.squeeze()

    bf = bf_er(np.r_[min(t_range): max(t_range): 200j], taus)
    bf_db = 20 * np.log10(np.abs(bf))
    fig, ax = plt.subplots()
    mesh = bf_db.plot(ax)
    mesh.set_clim(90, 105)
    mesh.set_cmap(cmap)
    ax.set_ylim(min(a_range), max(a_range))
    ax.grid()
    plt.savefig(join(savedir, 'fig_down2_%03d' % enum_bf_er[0]))
    plt.close(fig)

def plot_subchannel(enum_bf_er):
    """plot beamformer response with user supplied optimal offsets"""
    t_range = (-8, -4)
    a_range = (-8, 8)

    bf_er = enum_bf_er[1]

    taus = wf_delays(np.r_[min(a_range): max(a_range): 301j])
    taus = taus.squeeze()

    bf = bf_er(np.r_[min(t_range): max(t_range): 300j], taus)
    bf_db = 20 * np.log10(np.abs(bf))
    fig, ax = plt.subplots()
    mesh = bf_db.plot(ax)
    mesh.set_clim(90, 110)
    #mesh.set_clim(85, 100)
    mesh.set_cmap(cmap)
    ax.set_ylim(min(a_range), max(a_range))
    ax.grid()
    plt.savefig(join(savedir, 'fig_%03d' % enum_bf_er[0]))
    plt.close(fig)


#plot_opt((10, get_bfer(10)))
#plot_subchannel((10, get_bfer(10)))
#all_runs = map(plot_opt, all_bf_ers())
all_runs = map(plot_subchannel, all_bf_ers())
list(all_runs)
