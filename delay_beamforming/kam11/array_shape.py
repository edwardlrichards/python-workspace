import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from kam11 import context

# delay correction for array shape. Supports array tilt and catenary
rd = context.VLA1().phoneDepths().values
z_bottom = np.max(rd)

#The buoy depth is not exactly specified
z_buoy = np.min(rd) - 2.15


class ArrayShape:
    """Calculate element displacements and delay for simple array shapes"""

    def __init__(self, rd, ssp=None):
        """Initilize buoy shape, sound speed profile required for delays"""
        self.ssp = ssp
        self.rd = np.array(rd, ndmin=1)

    def __call__(self, cat_theta=None, tilt_theta=None, wf_out=None):
        """Return either array position or time delay, for catenary and tilt

        Return a corrected delay time if a output from a wave front delay
        calculator is included, and ssp is defined
        """
        #solve for array catentary position
        if cat_theta is not None:
            cat_dr = self.cat_dr(cat_theta)
        else:
            cat_dr = 0
        # solve for array tilt position
        if tilt_theta is not None:
            tilt_dr = self.tilt_dr(tilt_theta)
        else:
            tilt_dr = 0
        # all array motion
        total_dr = cat_dr + tilt_dr
        # Convert to delays if we have an ssp, and it is desired
        if wf_out is not None:
            result = self.wf_correction(self.ssp, wf_out, total_dr)
        else:
            result = total_dr
        return result

    def cat_dr(self, theta):
        """Pertubation in array position from angle at bottom depth"""
        # Allow vectorization
        theta = np.array(theta, ndmin=1)
        # determine appropriate a value from derivative at bottom element
        y_p = np.sin(np.radians(theta))
        a = (z_bottom - z_buoy) / np.arcsinh(y_p)
        cat = lambda z: a[:, None] * np.cosh(z /  a[:, None])
        #Remove offset to have 0 dr at array bottom
        dr = cat(self.rd - z_buoy) - cat(z_bottom - z_buoy)
        return xr.DataArray(dr, dims=['cat_theta', 'depth'],
                            coords=[theta, self.rd])

    def tilt_dr(self, theta):
        """Petubation in array position from constant angle in array"""
        # Allow vectorization
        theta = np.array(theta, ndmin=1)
        dr = (self.rd - z_bottom) * np.sin(np.radians(theta))[:, None]
        return xr.DataArray(dr, dims=['tilt_theta', 'depth'],
                            coords=[theta, self.rd])

    def time_correction(self, ssp, theta0, dr):
        """Calculate time delay correction in ms from dr in meters"""
        c0 = np.interp(z_bottom, ssp.index, ssp)

        u0 = 1 / np.interp(z_bottom, ssp.index, ssp)
        p = np.cos(np.radians(theta0)) * u0
        p = xr.DataArray(p, coords=[theta0], dims=['angle'])
        return p * dr * 1e3

    def wf_correction(self, ssp, wf_out, dr):
        """Given output of wave front delay calculator, correct for array shape"""
        shape_corrections = self.time_correction(ssp, wf_out.angle.values, dr)
        return wf_out + shape_corrections
