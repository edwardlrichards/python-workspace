import datetime as dt
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.widgets import RadioButtons, SpanSelector
from kam11 import thermister, matchedFilter, context
from kam11.delay_optimization import positionPertubation
from beamforming import timeDomain, curvedWaveFront
from plotting import beam_plot, mf_plot

toi = dt.datetime.strptime('2011 J184 03:51:15', '%Y J%j %H:%M:%S')
subchannels = context.VLA1().phoneDepths()[:4]

cmap = plt.cm.magma_r
cmap.set_under(color='w')


def select_arrivals(mf, start_t=-10, end_t=2, plot_channels=None):
    """Prompt user for time bounds on arrivals"""
    vsize = 10000
    hsize = 10000
    if plot_channels is None:
        plot_channels = mf.rd
    mfsize = (int(hsize), int(vsize))
    fig = plt.figure(figsize=(8, 6))
    gs = gridspec.GridSpec(vsize, hsize)
    # Plot all mf arrivals on thier own axis
    # TODO: Make the channel spacing look better
    off = vsize - mfsize[1]
    v_width = mfsize[1] // len(plot_channels)
    v_start = np.array([v_width * i for i in range(len(plot_channels))])
    h_start = hsize - mfsize[0]

    # Make a subplot for each channel
    def make_subplot(start, width, offset, h_start):
        """Initilize a constant width subplot at a given start value"""
        sub = gs[(start + offset): (start + width + offset - 1), h_start:]
        return plt.subplot(sub)
    axes = []
    for s in v_start:
        axes.append(make_subplot(s, v_width, off, h_start))

    # Attach a span selector to each channel
    # min and max values for each channel sub_section
    sub_sect = np.full((len(plot_channels), 2), fill_value=np.nan)
    sels = [None] * len(plot_channels)

    def span_selection(x1, x2, index=None):
        """Each axis gets it's own function"""
        x = np.sort(np.array([x1, x2]))
        x1 = x[0]
        x2 = x[1]
        sub_sect[index] = x
        if sels[index] is not None:
            sels[index].set_visible(False)
        sels[index] = axes[index].axvspan(x1, x2, facecolor='r', alpha=0.3, animated=True)
        rect = axes[index].add_patch(sels[index])
        #fig.canvas.blit(axes[index].bbox)
        fig.canvas.draw()
        #fig.canvas.blit(axes[index].bbox)

    # Attach the functions to each axis
    spans = []
    for i, ax in enumerate(axes):
        selection = lambda i: lambda x1, x2: span_selection(x1, x2, index=i)
        #span = SpanSelector(ax, selection(i), 'horizontal', useblit=True,
        span = SpanSelector(ax, selection(i), 'horizontal', span_stays=True,
                            rectprops=dict(alpha=0.5, facecolor='red'))
        spans.append(span)

    def plot_function(i, ax):
        """plot 1 channel with imshow"""
        # single channel only works with mf object
        try:
            abs_data = np.log10(np.abs(mf.single_channel(i)))
            abs_data.sel(time=slice(start_t, end_t)).plot(ax=ax,
                                                          vmin=2, vmax=6)
        # otherwise, it is a single data array
        except AttributeError:
            t_bounded = mf.isel(depth=i-1)
            t_bounded = t_bounded.sel(time=slice(start_t, end_t))
            ax.plot(t_bounded.time, np.real(t_bounded))
        ax.set_xlabel('')

    #_ = [mf_plot.plotSingleChannel(mf, i, overax=ax, cmap=cmap, tStart=start_t,
                                   #tEnd=end_t)
         #for i, ax in zip(plot_channels.index, axes[:: -1])]
    [plot_function(i, ax) for i, ax in zip(plot_channels.index, axes[:: -1])]

    _ = [plt.setp(ax.get_yticklabels(), visible=False) for ax in axes]
    _ = [plt.setp(ax.get_yticklines(), visible=False) for ax in axes]
    _ = [plt.setp(ax.get_xticklines(), visible=False) for ax in axes[:-1]]
    _ = [plt.setp(ax.get_xticklabels(), visible=False) for ax in axes[:-1]]
    #_ = [ax.yaxis.set_tick_params(size=0) for ax in axes]
    plt.show()
    return np.flipud(sub_sect)


def select_arrivals_old(mf, subchannels, tStart=-8, tEnd=2):
    """Return time bounds on 1 arrival of interest"""
    # setup plotting grid
    fig = plt.figure()
    gs = gridspec.GridSpec(4, 4)
    ax1 = plt.subplot(gs[:2, :])
    rax = plt.subplot(gs[2:, 0])
    ax2 = plt.subplot(gs[2, 1:])
    ax3 = plt.subplot(gs[3, 1:])

    # Populate the static top pannel of figure
    fig, ax = mf_plot.plotMultipleChannel(mf, plotChannels=subchannels.index,
                                          tStart=tStart, tEnd=tEnd, cmap=cmap,
                                          overax=ax1, vmin=-40)
    ax1.grid(axis='x')

    # Populate dyanmic full channel pannel
    make_name = lambda i: '%.2f m' % i
    chan_names = list(map(make_name, subchannels.values))
    radio = RadioButtons(rax, chan_names)
    lookup = {c: i for c, i in zip(chan_names, subchannels.index.values)}
    # min and max values for each channel sub_section
    sub_sect = np.full((subchannels.size, 2), fill_value=np.nan)

    def full_chan(name):
        chan = lookup[name]
        ax2.clear()
        _ = mf_plot.plotSingleChannel(mf, chan, tStart=tStart, tEnd=tEnd,
                                      vmin=-40, cmap=cmap, overax=ax2)
        ax3.clear()
        if np.any(np.isnan(sub_sect[chan - 1])):
            _ = mf_plot.plotSingleChannel(mf, chan, tStart=tStart, tEnd=tEnd,
                                          vmin=-40, cmap=cmap, overax=ax3)
        else:
            tBounds = (np.min(sub_sect[chan - 1]), np.max(sub_sect[chan - 1]))
            _ = mf_plot.plotSingleChannel(mf, chan, tStart=tBounds[0],
                                          tEnd=tBounds[1], vmin=-40,
                                          cmap=cmap, overax=ax3)
        plt.setp(ax2.get_yticklabels(), visible=False)
        plt.setp(ax3.get_yticklabels(), visible=False)
        plt.show(block=False)
    radio.on_clicked(full_chan)
    full_chan(radio.value_selected)

    def onsub_sect(xmin, xmax):
        # get the current channel from the radio button
        chan = lookup[radio.value_selected]
        sub_sect[chan - 1] = np.sort(np.array([xmin, xmax]))
        full_chan(radio.value_selected)

    # set useblit True on gtkagg for enhanced performance
    span = SpanSelector(ax2, onsub_sect, 'horizontal', useblit=True,
                        rectprops=dict(alpha=0.5, facecolor='red'))

    plt.show()
    return sub_sect


if __name__ == '__main__':
    # Create matchedFilter CIR estimate
    ttime = 15  # seconds
    loadDuration = dt.timedelta(0, ttime)
    cirEstimate = matchedFilter.LFM_MatchedFilter(
        toi, loadDuration=loadDuration)
    # cirEstimate=matchedFilter.LFM_MatchedFilter(toi)
    mf = cirEstimate.matchedFilter()
    sub_sect = select_arrivals(mf)
    print(sub_sect)
