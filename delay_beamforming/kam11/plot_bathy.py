import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import scipy.interpolate as interp
from kam11 import context
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.basemap import pyproj
from helper import config
from os import path
import pickle


class PMRF:
    """Methods to load and plot PMRF bathymetry data"""

    def __init__(self):
        """ load data from pickle if possible, if not, load from data files """
        pickleFile = 'processedData/pmrf_bathy.pic'
        self.env_dir = config.enviornment_dir()
        self.pickleFile = path.join(path.dirname(__file__), pickleFile)

        try:
            self.xyz = pickle.load(open(self.pickleFile, 'rb'))
        except FileNotFoundError:
            self.xyz = self.load_xyz()
            pickle.dump(self.xyz, open(self.pickleFile, 'wb'), protocol=-1)

    def load_xyz(self):
        """Force reload of xyz data"""
        f = path.join(
            self.env_dir, 'mapping/bathy/pmrf_bathy/pmrf_grid.small.xyz')
        self.xyz = self._read_xyz(f)
        pickle.dump(self.xyz, open(self.pickleFile, 'wb'), protocol=-1)

    def bathy_line(self, station1_id, station2_id):
        """Plot the battom bathymetry between two stations"""
        num_points = 100
        moorings = context.MooringInfo()

        s1 = moorings.info.ix[station1_id]
        s2 = moorings.info.ix[station2_id]

        lon = np.r_[s1.lon: s2.lon: num_points * 1j]
        lat = np.r_[s1.lat: s2.lat: num_points * 1j]

        z_interp = interp.RectBivariateSpline(
            self.xyz.lon, self.xyz.lat, self.xyz)
        z = z_interp(lon, lat).diagonal()

        # distance between points with haversine formula
        arg = np.sin((lat - s1.lat) * np.pi / 180 / 2) ** 2 +\
            np.cos(s1.lat * np.pi / 180) * np.cos(lat * np.pi / 180) *\
            np.sin((lon - s1.lon) * np.pi / 180 / 2) ** 2
        distance = 2 * 6378.137e3 * np.arcsin(np.sqrt(arg))

        contour = xr.DataArray(z, dims=['distance'], coords=[distance])

        fig, ax = plt.subplots()
        contour.plot(ax)

        ax.set_ylim(120, 100)
        ax.set_ylabel('depth, m')
        ax.set_xlabel('distance, m')
        ax.set_title('Bathymetry contour between station %s and %s'%(station1_id, station2_id))

        return ax

    def plot_contour(self):
        """Plot bathymetry as a contour"""
        # Make a simple transverse mercator
        llcrnrlat = 22.1
        llcrnrlon = -159.85
        urcrnrlat = 22.2
        urcrnrlon = -159.7

        m = Basemap(projection='tmerc', llcrnrlat=llcrnrlat, llcrnrlon=llcrnrlon,
                    urcrnrlat=urcrnrlat, urcrnrlon=urcrnrlon,
                    resolution='f', lat_0=22, lon_0=-159.9)

        x, y = m(*np.meshgrid(self.xyz.lon.values, self.xyz.lat.values))
        fig, ax = plt.subplots()
        V = [50, 100, 150, 200, 300, 400, 500, 600]
        cs = m.contour(x, y, self.xyz.to_masked_array().T,
                       V, colors='k', axis=ax)
        plt.clabel(cs, fontsize=9, inline=1, fmt='%.0f')
        m.drawcoastlines()
        m.fillcontinents(color='coral')
        m.drawmeridians(np.r_[llcrnrlon: urcrnrlon: 6j], labels=[0, 0, 0, 1])
        m.drawparallels(np.r_[llcrnrlat: urcrnrlat: 6j], labels=[1, 0, 0, 0])
        plt.title('UTM zone 04Q')

        moorings = context.MooringInfo().loadMoorings()
        VLA1 = moorings.ix[context.VLA1().stationID]
        VLA2 = moorings.ix[context.VLA2().stationID]
        SRA = moorings.ix[context.SRA().stationID]
        # Depth seems to be quite off (~20 m or so)
        test = self.xyz.sel(lat=VLA1['lat'], lon=VLA1['lon'], method='nearest')
        m.plot(VLA1['lon'], VLA1['lat'], 'go', latlon=True)
        m.plot(SRA['lon'], SRA['lat'], 'r*', latlon=True)

        return fig, ax

    def _read_xyz(self, bathy_file):
        """read bathymetry file and return masked lat, lon data"""
        xyz = pd.read_csv(bathy_file, names=['x', 'y', 'z'], sep=r"\s+")
        xyz = pd.pivot_table(xyz, values='z', index='x', columns='y')

        # Convert from UTM to lat/lon
        X, Y = np.meshgrid(xyz.index.values, xyz.columns.values)

        # Plotting UTM coordinates is a bit of special sauce
        # kauai specs
        lon_0 = -159
        lat_0 = 0
        zone_lat = (16, 24)
        m = Basemap(projection='tmerc', lon_0=lon_0, lat_0=lat_0, k_0=0.9996, ellps='WGS84',
                    llcrnrlon=lon_0 - 3, llcrnrlat=zone_lat[0], urcrnrlon=lon_0 + 3.,
                    urcrnrlat=zone_lat[1], resolution='c')

        # convert to UTM reference
        x0, y0 = m(lon_0, lat_0)
        lon, lat = m(X + x0 - 500e3, Y + y0, inverse=True)

        Z = xr.DataArray(xyz.values, dims=['lon', 'lat'], coords=[
                           lon[0, :], lat[:, 0]])
        return Z
