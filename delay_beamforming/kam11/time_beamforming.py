from datetime import timedelta
from beamforming import curvedWaveFront, timeDomain
from kam11 import thermister, context
from kam11.matchedFilter import mf_bytime
from helper import passband

def load_beamformer(time_ofinterest, channels=None):
    """Returns (beamformer, plane wave delay calculator)"""
    #load 1 second of data for match filtering
    loadDuration = timedelta(seconds=1)
    # load a snapshot in the center of the cir estmate
    snap_index = 5
    uprate = 10

    #load a single snapshot from match filter
    mf = mf_bytime(time_ofinterest, loadDuration)
    ts = mf[snap_index]
    beamformer = timeDomain.ShiftAndAdd(ts, upsample=uprate, channels=channels)

    #load sound speed information used in curved wavefront corrections
    rd=context.VLA1().phoneDepths()
    ssp=thermister.getSoundSpeed(time_ofinterest)
    if channels is None:
        z0 = max(rd)
    else:
        z0 = max(channels)

    wf_delays = curvedWaveFront.WavefrontDelays(ssp, rd, z0=z0)
    return beamformer, wf_delays
