import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
from helper import input_axis, shift_xarray

cmap = plt.cm.magma_r
cmap.set_under(color='w')

def plot_ts(ts, xdim='time', tbounds=None,
            taxis=None, drange=10, norm='channel', isDB=False, isNorm=False,
            channel_delays=None, overaxis=None, **kwargs):
    """plot all channels on same plot
    overaxis: use input axis to draw on
    channelDelays: offset of time axis of each channel, None is 0 offset
    """
    #If supplied, plot on provided axis
    if overaxis is None:
        fig, ax = plt.subplots()
    else:
        ax = overaxis
        fig = ax.get_figure()
    #Prep data for plotting
    ts = input_axis.prep_bylabel(ts, xdim, inaxis=taxis)
    ts = shift_xarray.shift(ts, channel_delays)

    if tbounds is not None:
        ts = ts.loc[dict(time=slice(min(tbounds), max(tbounds)))]
    #convert to dB if necassary
    cirValues = normalize(ts, isDB, isNorm, type=norm)

    # Normalize each channel to have specified dynamic range
    yaxis = ts.coords[ts.dims[0]].values
    taxis = ts.coords[xdim].values
    dy = np.max(np.abs(np.diff(yaxis)))
    maxValue = 0.9 * dy
    vmax = np.max(cirValues)
    if isDB:
        vmin = vmax - drange
        cirValues[cirValues < vmin] = vmin
        cirValues += drange
    #Minus assumed we are plotting depth, and axis will be displayed flipped
    cirValues *= maxValue / vmax

    for channel, depth in zip(cirValues, yaxis):
        if 'color' in kwargs:
            _ = ax.plot(taxis, -channel + depth, **kwargs)
        else:
            _ = ax.plot(taxis, -channel + depth, 'b', **kwargs)

    _ = ax.set_ylim(np.max(yaxis) + dy, np.min(yaxis) - dy)
    ylab, xlab = ts.dims
    _ = ax.set_xlabel(xlab)
    _ = ax.set_ylabel(ylab)
    fig.canvas.draw()
    return fig, ax

def plot_response(response, isDB=True, isNorm = True):
    """Plot a 1D beam reponse vector"""
    response = response.squeeze()
    response.values = normalize(response, isDB, isNorm)
    fig, ax = plt.subplots()
    response.plot(ax=ax)
    #limit dynamic range to 50 dB
    if isDB:
        max_response = float(response.max())
        ax.set_ylim(max_response - 50, max_response + 2)
    ax.grid()
    return fig, ax

def plot_grid(beamresponse, xdim='time', isDB = True, isNorm = True,
              xaxis=None, yaxis=None, drange = 10, vmax=None):
    beamresponse = input_axis.prep_bylabel(beamresponse, xdim)
    if xaxis is None:
        xaxis = beamresponse.coords[beamresponse.dims[1]].values
    if yaxis is None:
        yaxis = beamresponse.coords[beamresponse.dims[0]].values

    X, Y = np.meshgrid(xaxis, yaxis)
    y = np.copy(np.array(beamresponse))
    y = normalize(y, isDB, isNorm)

    fig, ax = plt.subplots()
    cm = ax.pcolormesh(X, Y, y, cmap=cmap)
    title = ax.set_title('Time domain beamformer')
    xLabel = ax.set_xlabel(beamresponse.dims[1])
    ylabel = ax.set_ylabel(beamresponse.dims[0])
    #limit dynamic range
    if isDB:
        if vmax is None:
            vmax = np.max(y)
        cm.set_clim(vmax - drange, vmax)
    plt.colorbar(cm)
    ax.grid()
    fig.canvas.draw()
    return fig, ax

def normalize(y, isDB, isNorm, type='total'):
    """common method to normalize before plotting
    norm type can be total or channel
    """
    y = np.array(y, ndmin=2)
    if isDB:
        y = 20 * np.log10(np.abs(y) + np.spacing(1))
    if isNorm:
        if isDB:
            if type == 'total':
                y -= np.max(y)
            elif type == 'channel':
                y -= np.max(y, axis=0)
            else:
                raise Exception('NormType')
        else:
            if type == 'total':
                y /= np.max(np.abs(y))
            elif type == 'channel':
                y /= np.max(np.abs(y), axis=0)
            else:
                raise Exception('NormType')
    return np.squeeze(y)
