import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pylab as plt
import datetime as dt
from matplotlib import gridspec
from helper import input_axis, shift_xarray

cmap = plt.cm.magma_r
cmap.set_under('w')
def plotSingleChannel(mfMulti, plotChannel, tStart=-10, tEnd=10, vmin=-40, vmax=0,
                      overax=None, **kwargs):
    """Plot a single channel of the matched filter"""
    if overax is None:
        fig, ax = plt.subplots()
    else:
        ax = overax
        fig = ax.get_figure()
    mfOutput = mfMulti.single_channel(plotChannel)
    X, Y = np.meshgrid(mfOutput.time.values, mfOutput.slowtime.values)
    arrDB = 20 * np.log10(np.abs(np.array(mfOutput))).T
    arrDB -= np.max(arrDB)
    cb = ax.pcolormesh(X, Y, arrDB.T, vmin=vmin, vmax=vmax, cmap=cmap,
                      **kwargs)
    _ = ax.set_xlim(tStart, tEnd)
    fig.colorbar(cb)
    #fig.canvas.draw()
    return fig, ax


def plotMultipleChannel(mfMulti, plotChannels=None, tStart=-15, tEnd=15,
                        norm='total', delays=None, overax=None, **kwargs):
    """plot all channels on same plot"""
    if overax is None:
        fig, ax = plt.subplots()
    else:
        ax = overax
        fig = ax.get_figure()

    # yAxis is a combination of depth and time
    yAxis = []
    colorValues = []

    # default is to plot all channels
    if plotChannels is None:
        plotData = list(mfMulti.data)
        receivers = mfMulti.rd
    else:
        plotData = list(mfMulti.channels(plotChannels))
        receivers = mfMulti.rd[plotChannels]

    if delays is None:
        delays = np.zeros(len(plotData))
    elif delays.size != len(plotData):
        raise Exception('Not enough channel delays specified')

    taxis = plotData[0].time.values
    for (rd, cur, tau) in zip(receivers, plotData, delays):
        # make an array for each receiver depth
        colDB = 20 * np.log10(np.abs(cur) + np.spacing(1))
        if norm == 'channel':
            colDB -= np.max(colDB)
        colorValues.append(np.array(colDB))
        # make an array of the time axis, make time small compaired to rd
        timeAxis = cur.slowtime.values
        yAxis.append(timeAxis / 1000 + rd)

    #Prep data for plotting
    colorValues = input_axis.prep_bylabel(np.array(colorValues), 'time',
                                          inaxis=taxis)
    #Acount for channel delays
    colorValues = shift_xarray.shift(colorValues, delays, dim='dim_0')
    taxis = colorValues.time.values
    colorValues = np.array(colorValues).reshape((-1, taxis.size))
    colorValues = xr.DataArray(colorValues, dims=['index', 'time'],
                                 coords=[np.arange(colorValues.shape[0]),
                                         taxis])
    colorValues = colorValues.sel(time=slice(tStart, tEnd))
    if norm == 'total':
        colorValues -= np.max(colorValues)
    yAxis = np.array(yAxis)
    yAxis = yAxis.reshape((-1))
    # Create yticks
    yAxisTicks, uI = np.unique(np.floor(yAxis), return_index=True)
    dUI = np.abs(uI[1] - uI[0])
    yAxisTickLocation = uI + dUI / 2

    pcol = colorValues.plot(ax=ax, cmap=cmap, **kwargs)
    _ = ax.set_xlim(tStart, tEnd)
    _ = ax.set_yticks(yAxisTickLocation)
    _ = ax.set_yticklabels(np.array(yAxisTicks, dtype=np.int))
    _ = ax.set_ylim(0, yAxis.shape[0])
    _ = ax.set_xlabel('time, ms')
    _ = ax.set_ylabel('depth, m')
    _ = ax.set_title('Channel impulse response recorded %s'
                     % mfMulti.time.strftime('J%j %H:%M:%S'))
    return fig, ax
