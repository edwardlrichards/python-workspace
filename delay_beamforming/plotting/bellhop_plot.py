import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec

def spark_delay(bellhop_array, profile=None, tbounds=(-6, 6), ybounds=(100, 0)):
    """Plot spark delay plot, with optional sound speed profile"""
    rd = bellhop_array.rd
    sd = bellhop_array.sd[0]
    spacing = np.abs(rd[2] - rd[1])

    fig, ax = _basic_setup(ybounds, profile, rd, sd)

    for arr, d in zip(bellhop_array.arrivals, rd):
        for i, row in arr.iterrows():
            _plotBeamSpark(row, ax[-1], spacing, offset=d)

    _ = ax[-1].set_xlim(min(tbounds), max(tbounds))
    _ = ax[-1].set_ylim(max(ybounds), min(ybounds))

    return fig, ax

def ray_trace(ray_list, rd, ybounds=(100, 0), rr=None, profile=None, sd=None):
    """Plot the results of a ray trace"""
    fig, ax = _basic_setup(ybounds, profile, rd, sd)

    for ray in ray_list:
        _plotRay(ray, ax[-1], rd, rr)

    xaxis = ray_list[0]['beamPath'].distance

    _ = ax[-1].set_xlim(min(xaxis), max(xaxis))
    _ = ax[-1].set_ylim(max(ybounds), min(ybounds))

    return fig, ax


def _basic_setup(ybounds, profile, rd, sd):
    """setup figure axes and handle optional plotting of ssp"""
    sspbound = (1525, 1540)

    fig = plt.figure(figsize=(8, 6))

    if profile is not None:
        gs = gridspec.GridSpec(100, 100, bottom=0.18, left=0.18, right=0.88)
        ax1 = plt.subplot(gs[:, :25])
        _plotContext(ax1, profile, rd, sd, sspbound, ybounds)
        ax2 = plt.subplot(gs[:, 30:100])
        #Turn off second axis y labels
        plt.setp(ax2.get_yticklabels(), visible=False)
        ax2.yaxis.set_tick_params(size=0)
        #Turn off first x label
        plt.setp(ax2.get_xticklabels()[0], visible=False)
        ax = (ax1, ax2)
    else:
        ax2 = plt.subplot()
        ax = (ax2,)
    # ax1.set_title('Sound speed profile', fontsize=16)
    return fig, ax

def _plotRay(ray, ax, rd, rr):
    """Plot a single ray"""
    numTB = ray['numTopBnc']
    numBB = ray['numBotBnc']
    ray = ray['beamPath']

    #Check if ray end is within lambda / 2 of receiver (23 kHz)
    range_end = ray[-5:].distance.values
    depth_end = ray[-5:].values
    final_depth = np.interp(rr, range_end, depth_end)
    if not np.any(np.abs(final_depth - rd) < 0.2):
        return
            
    if (numTB == 0) and (numBB == 0):
        _ = ray.plot(ax, color='r')
    elif (numTB == 0) and (numBB <= 2):
        alpha = 0.8 ** numBB
        _ = ray.plot(ax, color='b', alpha=alpha)
    elif (numTB <= 2) and (numBB <= 2):
        alpha = 0.8 ** (numBB + numTB)
        _ = ray.plot(ax, color='k', alpha=alpha)


def _plotBeamSpark(arrival_df_row, ax,lineRange, drange=30, offset=0):
    """Plot a single point on an arrival plot """
    normArrival = arrival_df_row['mag'] + drange
    numTB = arrival_df_row['numTopBounce']
    numBB = arrival_df_row['numBotBounce']
    time=arrival_df_row['time']
    if normArrival > 0:
        lineWidth = normArrival / 2 / (drange / lineRange)
        plotY = np.array([lineWidth, -lineWidth]) + offset
    else:
        return
    if (numTB == 0) and (numBB == 0):
        _ = ax.plot([time, time], plotY, 'r')
    elif (numTB == 0) and (numBB <= 2):
        #alpha = 0.8 ** numBB
        alpha = 1
        _ = ax.plot([time, time], plotY, 'b', alpha=alpha)
    #elif (numTB <= 2) and (numBB <= 2):
    else:
        #alpha = 0.8 ** (numBB + numTB)
        alpha = 1
        _ = ax.plot([time, time], plotY, 'k', alpha=alpha)

def _plotContext(ax, profile, rd, sd, xBound, yBound):
    """Plot sound speed profile, and array location"""
    _ = ax.plot(profile, profile.index)
    _ = ax.plot(np.mean((xBound[0], xBound[1])) * np.ones(rd.shape), rd, 'go')
    _ = ax.plot(np.mean((xBound[0], xBound[1])) + 1, sd, 'r*')

    _ = ax.set_xlim(min(xBound), max(xBound))
    _ = ax.set_ylim(max(yBound), min(yBound))
    ax.set_xticks(np.r_[xBound[0]:xBound[1]:3j])
    ax.set_yticks(np.r_[yBound[1]:yBound[0]:6j])
    ax.set_xlabel('Sound speed, m/s', fontsize=16)
    ax.set_ylabel('depth, m', fontsize=16)
