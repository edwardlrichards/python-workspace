from pandas import DataFrame
from numpy import array, exp, pi, complex_, nansum, squeeze
from scipy.signal import remez, fftconvolve
from beamforming.timeDomain import ShiftAndAdd
from helper import baseband

class BasebandBeamfomer(ShiftAndAdd):
    """Time domain beamformer which basebands data and adds phase correction"""

    def __init__(self, dataset, fs, fc, bw, taxis=None, upsample=None, b=None,
                 tobaseband=False):
        """ShiftAndAdd beamformer, with additional baseband parameters
        fc: center frequency, units of 1 / dt
        bw: bandwidth, units of 1 / dt
        b: FIR filter paramters, if none use default lowpass filter
        """
        self.fs = fs
        self.fc = fc
        self.bw = bw
        if tobaseband:
            dataset = baseband.baseband(dataset, fs = fs, fc = fc, bw = bw,
                                           taxis = taxis, b = b)

        super().__init__(dataset, upsample=upsample)

    def ts_atdelay(self, angle_delays, timebounds=None, upsample=None):
        """Overwrite to account for baseband phase shift
        data is sampled by linear interpolation at angle_delays
        """
        resampled = super().ts_atdelay(angle_delays, timebounds, upsample)
        #resampled *= self._bb_phase(angle_delays)
        return resampled

    def _resample_data(self, sampleTimes):
        """Overwrite index based resampling to account for phase shift"""
        s_data = super()._resample_data(sampleTimes)
        #Infer delay from time difference between channels
        angle_delays = sampleTimes[:, 0] - sampleTimes[0, 0]
        s_data *= self._bb_phase(angle_delays)[:, None]
        return s_data

    def _bb_phase(self, angle_delays):
        """Calculate appropriate phase shift for delays"""
        #ensure basebanding signal has dt of 1/FS
        dt = self.time[1] - self.time[0]
        scale = 1 / dt / self.fs
        #Beamformer may also have a user programed delay
        angle_delays = squeeze(angle_delays) + self.t_shift
        bb_phase = exp(1j * 2 * pi * self.fc * angle_delays * scale)
        return bb_phase
