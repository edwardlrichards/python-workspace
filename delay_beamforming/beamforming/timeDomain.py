import numpy as np
import pandas as pd
import xarray as xr
import datetime as dt
import pytz
from scipy.interpolate import interp1d
from scipy.signal import resample
from helper import input_axis


class ShiftAndAdd:
    """Simple time domain beamformer"""

    def __init__(self, dataset, taxis=None, upsample=None, channels=None):
        """data set is an array of evenly spaced data samples
        taxis: array of sample times
        default is to assume dataset is a DataArray
        columnNames: array of channel names
        default is to check for DataFrame columns, then no name
        """
        dataset = input_axis.prep_bylabel(dataset, 'time', inaxis=taxis)
        self.dtype = dataset.dtype
        rd = dataset.coords[dataset.dims[0]].values
        #Channels are persistant. A new sub array requires a new initilization
        self.channelI = self._channel_index(channels, rd)

        self.rd = rd[self.channelI]
        dataset = dataset[self.channelI, :]
        #Construct interplators, one for each channel in array
        self._samplers, self.time = self._getResampler(dataset, upsample)
        #tbounds enforce user specified time windows
        self.tbounds = None
        #t_shift is used to correct for time biases
        self.t_shift = np.zeros(len(self.samplers))

    def __call__(self, c_time, angle_delays):
        """Evaluate beamformer time series for each angle
        Returns result as a DataArray with dimensions num_ct x num_angles

        Parameters:
        c_time is a array of the beamformers center time on 1st element
        angle_delays is a DataArray or array, with dimension num_elements x num_angles
        """
        angle_specs = input_axis.prep_bylabel(angle_delays, 'angle')
        angle_delays = np.array(angle_specs, ndmin=2)
        c_time = np.array(c_time, ndmin=1)

        angles=[]
        #Broadcast each angle into a time series
        for taus in angle_delays.T:
            delays = c_time + taus[self.channelI, None]
            angles.append(self._resample_add(delays))
        #return result as a DataArray
        angles = np.array(angles, ndmin=2)
        angle_result = xr.DataArray(angles, dims=['angle', 'time'],
                                      coords=[angle_specs.angle.values,
                                              c_time])
        return angle_result

    @property
    def samplers(self):
        """Return samplers that are windowed in time"""
        if self.tbounds is None:
            return self._samplers
        else:
            samplers = []
            for tb, sam_er in zip(self.tbounds, self._samplers):
                def sampler(t_in, tb=tb, sam_er=sam_er):
                    """sampler with a bounded time window"""
                    out = np.zeros(t_in.size, dtype=self.dtype)
                    if (not np.isnan(tb[0])) and (not np.isnan(tb[1])):
                        activeI = (t_in > tb[0]) & (t_in < tb[1])
                        out[activeI] = sam_er(t_in[activeI])
                    return out
                samplers.append(sampler)
            return samplers

    @property
    def ts(self):
        """return the ts orginally used to construct beamformer"""
        delays = self.time + np.array(self.t_shift)[:, None]

        return self.resample_data(delays)

    def ts_atdelay(self, angle_delays, tbounds=None, upsample=None):
        """return the time series at a particular angle within time bounds"""
        delay = np.squeeze(np.array(angle_delays))
        taus = self.time.copy()
        #section timeseries with time bounds
        if tbounds is not None:
            tbounds = np.array(tbounds)
            if len(tbounds.shape) == 1:
                taus = taus[(taus > min(tbounds)) & (taus < max(tbounds))]
            else:
                #channel by channel bounds
                import ipdb; ipdb.set_trace() # BREAKPOINT
                taus = [taus[(taus > min(tb)) & (taus < max(tb))] for tb in tbounds]
        if upsample is not None:
            taus = np.r_[min(taus): max(taus): 1j * taus.size * upsample]
        #sample axis with delays
        ts = self.resample_data(taus + delay[self.channelI, None])
        return ts

    def resample_data(self, sampledTimes, dim='time'):
        """DataArray based rasampling, slower than shaped based"""
        sampledTimes = input_axis.prep_bylabel(sampledTimes, dim)
        result = self._resample_data(np.array(sampledTimes))
        taxis = sampledTimes[0, :].values
        result = xr.DataArray(result, dims=['depth', 'time'],
                                coords=[self.rd, taxis])
        return result

    def _resample_data(self, sampleTimes):
        """array based loop through channels of sampleTimes"""
        #Account for pre-beamforming beamforming delay
        sampleTimes += np.array(self.t_shift)[:, None]
        sampledData = np.zeros(sampleTimes.shape, dtype=self.dtype)
        for i, (tT, tS) in enumerate(zip(sampleTimes, self.samplers)):
            # Some channels may have sample delays of nans
            #if not np.isnan(tT[0]):
            sampledData[i, :] = tS(tT)
        return sampledData

    def _resample_add(self, sampleTimes):
        """array data is sampled then summed at sampleTimes
        """
        resampledData = self._resample_data(sampleTimes)
        return np.nansum(resampledData, axis=0)

    def _channel_index(self, channels, rd):
        """Return an positional indexer array"""
        if channels is None:
            channels = rd
        allI = pd.Index(rd)
        channelI = allI.get_indexer(channels)
        return channelI

    def _getResampler(self, dataset, upsample):
        """Make a resampler that returns multichannel data"""
        samplers = []
        for i, track in enumerate(dataset):
            # upsample track if needed
            if upsample is not None:
                numSamples = track.size * upsample
                #make power of 2 for efficency
                numSamples = int(2 ** np.ceil(np.log2(numSamples)))
                NFFT = int(2 ** np.ceil(np.log2(track.size)))
                temp = np.array(track)
                temp.resize(NFFT)
                #resample only uses first 2 samples from taxis, so length is ok
                (track, taxis) = resample(temp, numSamples, t=dataset.time.values)
            else:
                taxis = dataset.time.values
            # Save a resampler for later use
            in_er = interp1d(taxis, track, kind='linear',
                             bounds_error=False, fill_value=0,
                             assume_sorted=True)
            samplers.append(in_er)
        return samplers, taxis
