import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import context
from beamforming import timeDomain
from plotting import beam_plot
from lfm_planewave import create_timeseries, get_delays, get_units
import argparse

signal='mfchirp'
"""Example of baseband beamforming using sythetic timeseries"""
angle = 3.5  # degrees
numelements = 4

angleBounds = (-6,6)
numangles = 301
numlooks = 300
test_angle = np.r_[angleBounds[0] : angleBounds[1] : 1j * numangles]

timeSeries = create_timeseries(numelements, angle, signal)
t = timeSeries.time.values
numChannels = timeSeries.shape[1]

look_time = t

look_angle = np.r_[min(angleBounds) : max(angleBounds) : 1j * numangles]
angle_delay = get_delays(numelements, look_angle,
                            units=get_units(signal))

beamformer = timeDomain.ShiftAndAdd(timeSeries, upsample = 10)

#line up time series correctly
sampledelays = get_delays(numelements, angle, units=get_units(signal))
ts_delay = beamformer.ts_atdelay(sampledelays)

#beamform at a single angle
single_angle = beamformer(t, sampledelays)

#beamform at a single time
tmax = float(single_angle.time[np.abs(single_angle).argmax()])
single_time = beamformer(tmax, angle_delay)

#pcolor grid, compressed signals need much less range
look_time = np.r_[np.min(look_time) : np.max(look_time) : 1j * numlooks]
bb_beamforming_all = beamformer(look_time, angle_delay)

fig, ax = beam_plot.plot_ts(ts_delay)

fig, ax = beam_plot.plot_response(single_angle)
title = ax.set_title('Beamformer response at correct angle, %0.1f'%angle)
xLabel = ax.set_xlabel('time, s')
ylabel = ax.set_ylabel('beamformer magnitude')

fig, ax = beam_plot.plot_response(single_time)
title = ax.set_title('beamformer output at corrrect time, %0.4f s'%tmax)
xLabel = ax.set_xlabel('look angle')
ylabel = ax.set_ylabel('beamformer magnitude')

fig, ax = beam_plot.plot_grid(bb_beamforming_all, drange=20)
title = ax.set_title('Time domain beamformer, angle = %0.1f, t = %0.4f s'
                    %(angle, tmax))
xLabel = ax.set_xlabel('look delay')
ylabel = ax.set_ylabel('look angle')
plt.show(block=False)
