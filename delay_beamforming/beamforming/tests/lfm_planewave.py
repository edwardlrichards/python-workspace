import os
import numpy as np
import xarray as xr
import scipy.signal as sig
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from kam11 import context
from helper import config
from sioLoad import loadSIO
from plotting import beam_plot
import argparse

units = None

def create_timeseries(numChannels, angle, signal = 'lfm', channel_delays=None):
    """method to create time series relevent to KAM11 beamforming"""
    synth_upsample = 10

    #lfm signal specifications
    FS = context.fs
    lfmInfo = context.LFM_Info()

    #Units are a piece of state that need to be conserved
    global units
    if signal == 'lfm':
        x = lfmchirp()
        units = 's'
        scale = 1
    elif signal == 'mfchirp':
        x = mfchirp()
        units = 'ms'
        scale = 1e3
    units = get_units(signal)
    signalLength = x.size

    #create synthetic time series
    signalt = (np.arange(signalLength) / FS) * scale
    signalt -= np.mean(signalt)
    #mf result is esentially shorter than lfm
    if units == 'ms':
        t = signalt[(signalt > -8) & (signalt < 8)]
    else:
        t = signalt

    xUp, tUp = sig.resample(x, signalLength * synth_upsample, t = signalt)

    #interpolator used to create time series
    signal_interp = interp1d(tUp, xUp, bounds_error=False, fill_value=0,
            copy=False, assume_sorted=True)

    if np.array(angle).size == 1:
        delays = get_delays(numChannels, angle, units = units)
    else:
        delays = angle

    if channel_delays is not None:
        delays += np.array(channel_delays)[:, None]

    timeSeries = []
    for tau in delays:
        timeSeries.append(signal_interp(t - float(tau)))
    return xr.DataArray(timeSeries, dims=['depth', 'time'],
                          coords=[np.arange(numChannels), t])

def lfmchirp():
    probe_name = 'TX1W_S02_N01_R09_TR_V01.sio'
    probe_dir = config.probe_dir()
    probe_file = os.path.join(probe_dir, probe_name)
    #load lfm chirp from KAM11 data file
    signalLength=5000
    x = loadSIO.load_selection(probe_file, 47900, signalLength, [0])['data']
    return x

def mfchirp(probe_file = None):
    x = lfmchirp()
    mf = x[::-1]
    return sig.convolve(x, mf)

def get_delays(numelements, angles, units='s'):
    """plane wave delays for each element"""
    #Array arrival specifications
    c = 1500
    vert_slowness = 1/c * np.sin(np.radians(angles))
    element_spacing = spacing()
    tau = np.array(vert_slowness * element_spacing, ndmin=1)
    delays = np.arange(numelements)[:,None] * tau
    if units == 'ms':
        delays *= 1e3
    delays = np.array(delays, ndmin=2)
    angles = np.array(angles, ndmin=1)
    return xr.DataArray(delays, dims=['depth', 'angles'],
                          coords={'depth': np.arange(numelements),
                                  'angles': angles})

def spacing():
    element_spacing = np.abs(context.VLA1().phoneDepths().diff()[2])  # meters
    return element_spacing

def get_units(sigtype):
    if sigtype == 'lfm':
        units = 's'
    elif sigtype == 'mfchirp':
        units = 'ms'
    return units

def main(sigtype = 'lfm'):
    """Create time series for specified angle and create standard plots"""
    angle=3.5
    numelements = 4
    timeSeries = create_timeseries(numelements, angle, signal = sigtype)

    #plot channel for single channel plots
    fig, ax = beam_plot.plot_ts(timeSeries)
    ax.set_title('Time series of signal, angle of arrival of %0.1f'%angle)
    ax.set_xlabel('time, sec')
    ax.set_ylabel('value with channel offset')

    plotChannel = timeSeries.columns[0]
    chanFT = np.fft.fft(timeSeries[plotChannel])
    faxis = np.arange(chanFT.size) / chanFT.size
    plt.figure()
    plt.plot(faxis, np.abs(chanFT))
    plt.title('Frequecy content of one channel')
    plt.xlabel('Frequency, cycles / sample')
    plt.ylabel('Magnitude')

    plt.show(block=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--type", help="type of signal used in test")
    args = parser.parse_args()
    if args.type:
        main(args.type)
    else:
        main()
