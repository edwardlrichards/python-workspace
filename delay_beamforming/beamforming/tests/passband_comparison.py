import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import context
from beamforming import timeDomain
from plotting import beam_plot
from helper import baseband
from lfm_planewave import create_timeseries, get_delays, get_units
import argparse
import scipy.signal as sig

from matplotlib import rc
rc('text', usetex=True)

signal='mfchirp'
"""Example of baseband beamforming using sythetic timeseries"""
angle = 3.5  # degrees
numelements = 4

angleBounds = (2,6)
numangles = 201
numlooks = 200
tbounds=(-3,3)
test_angle = np.r_[angleBounds[0] : angleBounds[1] : 1j * numangles]

timeSeries = create_timeseries(numelements, angle, signal)
t = timeSeries.time.values
numChannels = timeSeries.shape[1]

look_angle = np.r_[min(angleBounds) : max(angleBounds) : 1j * numangles]
angle_delay = get_delays(numelements, look_angle,
                            units=get_units(signal))

beamformer = timeDomain.ShiftAndAdd(timeSeries, upsample = 10)

#line up time series correctly
look_delays = get_delays(numelements, angle, units=get_units(signal))
ts_delay = beamformer.ts_atdelay(look_delays)

#beamform at a single angle
single_angle = beamformer(t, get_delays(numelements, angle,
                                                 units=get_units(signal)))
#beamform at a single time
tmax = float(single_angle.time[np.abs(single_angle).argmax()])
single_time = beamformer(tmax, angle_delay)

#pcolor grid, compressed signals need much less range
#look_time = np.r_[np.min(t) : np.max(t) : 1j * numlooks]
look_time = np.r_[np.min(tbounds) : np.max(tbounds) : 1j * numlooks]
pb_beamforming_all = beamformer(look_time, angle_delay)

fig, ax = beam_plot.plot_ts(timeSeries)
title = ax.set_title('Common input to beamformers')
xLabel = ax.set_xlabel('time, ms')
_ = ax.set_xlim(min(tbounds), max(tbounds))

fig, ax = beam_plot.plot_response(single_angle)
title = ax.set_title('Beamformer response at correct angle, %0.1f'%angle)
xLabel = ax.set_xlabel('time, ms')
ylabel = ax.set_ylabel('beamformer magnitude')
_ = ax.set_xlim(min(tbounds), max(tbounds))

fig, ax = beam_plot.plot_response(single_time)
title = ax.set_title('beamformer output at corrrect time, %0.4f s'%tmax)
xLabel = ax.set_xlabel('look angle')
ylabel = ax.set_ylabel('beamformer magnitude')

fig, ax = beam_plot.plot_grid(pb_beamforming_all, drange=20)
title = ax.set_title('Pass band beamformer, angle = %0.1f, t = %0.4f s'
                    %(angle, tmax))
xLabel = ax.set_xlabel('look delay')
ylabel = ax.set_ylabel('beamformer magnitude')

_ = ax.set_xlim(min(tbounds), max(tbounds))

fig, ax = beam_plot.plot_grid(sig.hilbert(np.array(pb_beamforming_all), axis=1),
                                  drange=20, xaxis = pb_beamforming_all.time,
                                  yaxis = pb_beamforming_all.angle)
title = ax.set_title('Hilbert transform of pass band beamformer\n'+
                     'angle = %0.1f, t = %0.4f s' %(angle, tmax))
xLabel = ax.set_xlabel('look delay')
ylabel = ax.set_ylabel('beamformer magnitude')

_ = ax.set_xlim(min(tbounds), max(tbounds))

pb_beamforming_all = beamformer(t, angle_delay)
pb_bb = baseband.baseband(pb_beamforming_all, taxis = t)
fig, ax = beam_plot.plot_grid(pb_bb, drange=20)
title = ax.set_title('Pass band beamformer, basebanded \n'+
                     'angle = %0.1f, t = %0.4f s' %(angle, tmax))
xLabel = ax.set_xlabel('look delay')
ylabel = ax.set_ylabel('beamformer magnitude')

_ = ax.set_xlim(min(tbounds), max(tbounds))
plt.show(block=False)
