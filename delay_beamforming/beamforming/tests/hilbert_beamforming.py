import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import context
from beamforming import timeDomain, basebanded
from plotting import beam_plot
from helper import passband
from lfm_planewave import create_timeseries, get_delays
import scipy.signal as sig
from matplotlib import rc
rc('text', usetex=True)


FS = context.fs
FC = context.LFM_Info().fc
BW = context.LFM_Info().bw

angle = 3.5  # degrees
numelements = 4

angleBounds = (-6,6)
numangles = 201
numlooks = 200
test_angle = np.r_[angleBounds[0] : angleBounds[1] : 1j * numangles]

ts = create_timeseries(numelements, angle, 'mfchirp')
t = ts.time.values
numChannels = ts.shape[1]
look_delays = get_delays(numelements, angle, units = 'ms')

#baseband beamforming with phase correction
bb_beamformer = basebanded.BasebandBeamfomer(2 * ts, FS, FC,
                                                BW, upsample=10,
                                                tobaseband=True)
#Hilbert beamforming
h_ts = passband.hilbert_pb(ts)

h_beamformer = timeDomain.ShiftAndAdd(h_ts, taxis = t, upsample = 10)

#max angle beamforming
bb_max_angle = np.squeeze(bb_beamformer(t, look_delays))
h_max_angle = np.squeeze(h_beamformer(t, look_delays))

#Extract max arrival time for angle range comparisons
tmax = float(bb_max_angle.time[np.abs(bb_max_angle).argmax()])
look_delays = get_delays(numelements, test_angle, units = 'ms')
bb_max_time = np.squeeze(bb_beamformer(tmax, look_delays))
h_max_time= np.squeeze(h_beamformer(tmax, look_delays))

plt.figure()
plt.plot(t, 20 * np.log10(np.abs(h_max_angle)), label='hilbert')
plt.plot(t, 20 * np.log10(np.abs(bb_max_angle)), label='baseband')
plt.title(r'Beamformers at max angle, \theta=%.1f' %angle)
plt.xlabel('t, ms')
plt.ylabel('Beamformer magnitude')
plt.legend()

plt.figure()
plt.plot(test_angle, 20 * np.log10(np.abs(h_max_time)), label='hilbert')
plt.plot(test_angle, 20 * np.log10(np.abs(bb_max_time)), label='baseband')
plt.title(r'Beamformers at max time, t=%.1f' %tmax)
plt.xlabel('angle, degree')
plt.ylabel('Beamformer magnitude')
plt.legend(loc=2)

plt.show(block=False)

