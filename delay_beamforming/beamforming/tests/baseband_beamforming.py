import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from kam11 import context
from helper import baseband
from beamforming import timeDomain, basebanded
from plotting import beam_plot
from lfm_planewave import create_timeseries, get_delays, units
import argparse

"""Example of baseband beamforming using lfm timeseries"""
signal = 'mfchirp'
FS = context.fs
FC = context.LFM_Info().fc
BW = context.LFM_Info().bw

angle = 3.5  # degrees
numelements = 4

aBounds = (0,6)
tBounds = (-3,3)
numangles = 201
numlooks = 200

timeSeries = create_timeseries(numelements, angle, signal)
t = timeSeries.time.values
numChannels = timeSeries.shape[1]

look_delays = get_delays(numelements, angle, units = units)

#baseband beamforming with phase correction
bb_beamformer = basebanded.BasebandBeamfomer(timeSeries, FS, FC,
                                                BW, upsample=10,
                                                tobaseband=True)
bb_beamforming = bb_beamformer(t, look_delays)
tmax = float(bb_beamforming.time[np.abs(bb_beamforming).argmax()])


# beamform with complex data (no phase correction)
bb_ts = baseband.baseband(timeSeries, taxis = t)
c_beamformer = timeDomain.ShiftAndAdd(bb_ts, upsample = 10)
c_beamforming = c_beamformer(t, look_delays)

#pcolor grid, compressed signals need much less range
look_time = np.r_[np.min(tBounds) : np.max(tBounds) : 1j * numlooks]
look_angle = np.r_[min(aBounds) : max(aBounds) : 1j * numangles]
angle_delay = get_delays(numelements, look_angle, units = units)

bb_beamforming_all = bb_beamformer(look_time, angle_delay)
c_beamforming_all = c_beamformer(look_time, angle_delay)

#test the equivalence of modulated baseband beamformer and real beamformer
t = timeSeries.time.values
max_tau = get_delays(numelements, angle, units=units)
real_beamformer = timeDomain.ShiftAndAdd(timeSeries, upsample=10)
bb_max_angle = np.squeeze(bb_beamformer(t, max_tau))
scale = 1 / (t[1] - t[0]) / FS

bb_max_angle *= 2 * np.exp(1j * 2 * np.pi * FC * t * scale)
bb_max_angle = np.real(bb_max_angle)

real_max_angle = np.squeeze(real_beamformer(t, max_tau))

plotChannel = 0

plt.figure()
faxis = np.arange(bb_ts.shape[1]) / bb_ts.shape[1] * FS
y = 20 * np.log10(np.abs(np.fft.fft(bb_ts[plotChannel])) + np.spacing(1))
y -= np.max(y)
_ = plt.plot(faxis, y)
_ = plt.title('Basebanded frequency content of one signal channel')
_ = plt.xlabel('Frequency, Hz')
_ = plt.ylabel('beamformer magnitude, dB')
_ = plt.ylim(-50, 0)

fig, ax = beam_plot.plot_ts(c_beamformer.ts_atdelay(look_delays))
title = ax.set_title('time series without phase correction')

fig, ax = beam_plot.plot_ts(bb_beamformer.ts_atdelay(look_delays))
title = ax.set_title('time series with phase correction')

fig, ax = beam_plot.plot_response(np.abs(c_beamforming),
                                        isDB = False,
                                        isNorm = False)
title = ax.set_title('Base band beamformer without phase correction' +\
                    '\nexact tau = %0.4f'%tmax)
xLabel = ax.set_xlabel('look delay')
ylabel = ax.set_ylabel('beamformer magnitude')

fig, ax = beam_plot.plot_response(np.abs(bb_beamforming),
                                    isDB=False, isNorm = False)
title = ax.set_title('Base band beamformer with phase correction' +\
                    '\nexact tau = %0.4f'%tmax)
xLabel = ax.set_xlabel('look delay')
ylabel = ax.set_ylabel('beamformer magnitude')

fig, ax = plt.subplots()
_ = ax.plot(t, real_max_angle, label='real')
_ = ax.plot(t, bb_max_angle, label='baseband')
title = ax.set_title('Real beamforming result comparied to modulated'+
                        'baseband beamformer')
xLabel = ax.set_xlabel('look delay')
ylabel = ax.set_ylabel('beamformer amplitude')
ax.legend()

fig, ax = beam_plot.plot_grid(bb_beamforming_all, drange=20)
ax.set_title('Beamforming result with phase correction')
ax.set_xlabel('delay, s')
ax.set_ylabel('angle, degree')

fig, ax = beam_plot.plot_grid(c_beamforming_all, drange=20)
ax.set_title('Beamforming result without phase correction')
ax.set_xlabel('delay, s')
ax.set_ylabel('angle, degree')

plt.show(block=False)
