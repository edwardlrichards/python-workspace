import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from kam11 import context, thermister
from beamforming import timeDomain, curvedWaveFront
from plotting import beam_plot
from lfm_planewave import create_timeseries, get_delays, spacing, get_units
from helper import passband
from os import path
from matplotlib import rc
rc('text', usetex=True)

save_dir = '/home/e2richards/Desktop/beamforming_tilt/'
to_save=False
#front_type = 'linear'
front_type = 'curved'
#front_type = 'bellhop'
angle = 6.5  # degrees
arraytilt = -3 # degrees
numelements = 16
vmax=85  # set to None for auto scaling
#vmax=None  # set to None for auto scaling

if front_type == 'linear':
    delay_func = lambda angle: get_delays(numelements, angle, units='ms')
    u0 = 1 / 1500
    p = np.cos(np.radians(angle)) * u0
    rd = np.arange(numelements) * spacing()
    rd = rd[::-1]
else:
    toi = datetime.datetime.strptime('2011 J184 03:51', '%Y J%j %H:%M')
    ssp = thermister.getSoundSpeed(toi)
    rd = context.VLA1().phoneDepths()
    z0 = rd[1]
    delay_func = curvedWaveFront.WavefrontDelays(ssp, rd)
    p = curvedWaveFront.u_r(angle, z0, ssp)

angleBounds = (0, 12)
timebounds = (-2, 2)
numangles = 201
numlooks = 200
test_angle = np.r_[angleBounds[0] : angleBounds[1] : 1j * numangles]
test_time = np.r_[timebounds[0] : timebounds[1] : 1j * numlooks]

delays = delay_func(angle)
ts_notilt = create_timeseries(numelements, delays, 'mfchirp')
t = ts_notilt.time.values

#Create passband beamformer
notilt_bf = timeDomain.ShiftAndAdd(passband.hilbert_pb(ts_notilt), upsample=10)

#simulate array tilt
d_position = np.abs(rd - np.max(rd)) * np.sin(np.radians(arraytilt))
array_tau = d_position * p * 1e3
beamtau = delay_func(angle).squeeze() + np.array(array_tau)
ts_tilt = create_timeseries(numelements, beamtau, 'mfchirp')
tilt_bf = timeDomain.ShiftAndAdd(passband.hilbert_pb(ts_tilt), upsample=10)

#Naive beamforming
delays = delay_func(test_angle)
fig1, ax = beam_plot.plot_grid(notilt_bf(test_time, delays),
        drange=20, isNorm=False, vmax=vmax)
ax.set_title(r'Beamformer result with no array tilt')
fig2, ax = beam_plot.plot_grid(tilt_bf(test_time, delays),
        drange=20, isNorm=False, vmax=vmax)
ax.set_title(r'Beamformer result with %0.1f$^o$ array tilt, unmodeled'%arraytilt)

#Add delays to beamformers
notilt_bf.t_shift = array_tau
tilt_bf.t_shift = array_tau
fig3, ax = beam_plot.plot_grid(notilt_bf(test_time, delays),
        drange=20, isNorm=False, vmax=vmax)
ax.set_title(r'Beamformer result with no array tilt, with %0.1f$^o$ correction'%arraytilt)
fig4, ax = beam_plot.plot_grid(tilt_bf(test_time, delays),
        drange=20, isNorm=False, vmax=vmax)
ax.set_title(r'Beamformer result with %0.1f$^o$ array tilt, with %0.1f$^o$ correction'%(arraytilt, arraytilt))
plt.show(block=False)

if to_save:
    fig1.savefig(path.join(save_dir, '%s_%i'%(front_type,1)))
    fig2.savefig(path.join(save_dir, '%s_%i'%(front_type,2)))
    fig3.savefig(path.join(save_dir, '%s_%i'%(front_type,3)))
    fig4.savefig(path.join(save_dir, '%s_%i'%(front_type,4)))
