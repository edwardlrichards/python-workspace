import numpy as np
import pandas as pd

def maxangle(beam_result, tbounds, anglebounds):
    """Return the angle of beamformer output with highest value in bounds"""
    #TODO: Doesn't work for nagative values
    beam_result = beam_result.loc[dict(angle=slice(min(anglebounds),
                                                   max(anglebounds)),
                                   time=slice(min(tbounds), max(tbounds)))]
    max_angleI = int(np.abs(beam_result).max(dim='time').argmax())
    return float(beam_result.angle[max_angleI])

def correlate_signal(mf_timeseries, signal, taxis = None, tbounds=None,
                     nophase=True):
    """Corrilate a matched field time series result with a signal
    Returns a DataFrame with columns of max corrilation amplitude and index
    nophase uses real corrilation amplitude for maximum
    """
    if taxis is None:
        taxis = np.array(mf_timeseries.index)

    try:
        columns = mf_timeseries.columns
    except AttributeError:
        columns = None

    allchannels = np.array(mf_timeseries)

    signal = np.squeeze(np.array(signal))
    auto_amp = np.real(np.sum(signal * signal.conj()))

    col_cor = lambda row: np.correlate(row, signal, mode='valid')
    corr = np.apply_along_axis(col_cor, 0, allchannels)

    if nophase:
        corr = np.real(corr)

    #corr_time = taxis[np.floor(signal.size / 2) :
                      #-np.ceil((signal.size / 2 - 1))]
    corr_time = taxis[signal.size - 1:]

    if tbounds is not None:
        timeI = (corr_time > min(tbounds)) & (corr_time < max(tbounds))
        corr = corr[timeI, :]
        corr_time = corr_time[timeI]

    import ipdb; ipdb.set_trace()
    maxtime = corr_time[np.argmax(np.real(corr), axis = 0)]
    #maxtime -= maxtime[0]
    signal_amp = np.real(corr).max(axis=0) / auto_amp

    return pd.DataFrame([signal_amp, maxtime],
                columns = columns, index=['amp','tau']).T

def remove_signal(mf_timeseries, signal, channel_amp, channel_delay,
                  taxis = None):
    """Subtract signal from channels of mf_timeseries, with scale and delay"""
    #iterate over the shortest dimension of timeseries
    iterDim = np.argmin(mf_timeseries.shape)
    if iterDim != 0:
        mf_timeseries = mf_timeseries.T

    channelNames = None
    if taxis is None:
        taxis = mf_timeseries.columns
        channelNames = mf_timeseries.index

    mf_timeseries = np.array(mf_timeseries, ndmin = 2)

    for chan, amp, delay in zip(mf_timeseries, channel_amp, channel_delay):
        delayI = np.argmin(np.abs(taxis - delay))
        chan[delayI : delayI + signal.size] -= amp * signal

    mf_timeseries = pd.DataFrame(np.array(chan, ndmin = 2), columns = taxis)
    if channelNames is not None:
        mf_timeseries.idex = channelNames

    return mf_timeseries
