import numpy as np
import pandas as pd
import xarray as xr
from scipy.integrate import cumtrapz
from atpy import write_at, bellhop
#TODO: getDelays is slow because of wave front interpolator, optimize?

class WavefrontDelays:
    """object to calculate delays for a plane wave"""
    def __init__(self, ssp, rd, z0=None, unit='ms'):
        self.ssp = ssp
        self.rd = np.array(rd)
        if z0 is None:
            self.z0 = self.rd[0]
        else:
            self.z0 = z0
        self.unit = unit
        self.correction = 0  # Used to correct for curvature effects

    def __call__(self, angle, array_tilt=None, dr=None):
        """given an angle, calculate the delays
        array_tilt is measured towards source (positive leads to smaller r)
        """
        if array_tilt is not None:
            array_tilt = np.array(array_tilt, ndmin=1)
            t_dr = (self.rd - self.z0) *\
                    np.sin(np.radians(array_tilt))[:, None]
            t_dr = t_dr
            if dr is None:
                dr = t_dr
            else:
                dr = np.array(dr) + t_dr[None, :]
            dr = np.squeeze(dr)
        tau = getDelays(angle, self.ssp, self.rd, z0=self.z0, dr=dr,
                unit=self.unit, dr_axis=array_tilt)
        return tau + self.correction

    def bellhop_correction(self):
        """bellhop arrival and wf prodiction difference is corrected once"""
        ssp = self.ssp
        envfile = write_at.writeBellhop(
            ssp.index, ssp.values, 'A', receiveD=self.rd, name='curve_correct')
        bellhop.runBellhop(envfile)
        arrivals = bellhop.readArr(envfile)
        arrivals.ref0dB()
        arrivals.filterArrivals(
            numTopBounces=1, numBotBounces=0, arrivalMagnitude=-20)
        refI = np.argmin(np.abs(arrivals.rd - self.z0))
        arrival_angle = float(arrivals.arrivals[refI]['receiveAngle'])
        #run wf predictor for uncorrected wavefront
        wf_predict = self(arrival_angle)
        bell_result = np.array([float(a['time']) for a in arrivals.arrivals])
        #this only works if arrival has been set with a 1 wave front filter
        assert len(bell_result.shape) == 1
        bell_result -= bell_result[refI]
        self.correction = bell_result - wf_predict.squeeze()


def getDelays(thetaTest, ssp, rd, z0=None, dr=None, dr_axis=None, unit='ms'):
    """return a DataFrame of delays for each theta based on plane wave
    thetaTest: array of theta guesses
    ssp: Series of sound speed and depth
    rd: an iterable of receiver depths
    z0: theta reference depth
        default is first reciever depth
    dr: range variations across phones
    unit can be 's' or 'ms'
    """
    thetaTest=np.array(thetaTest, ndmin=1)
    # Reference depth is lowest receiver by default
    if z0 is None:
        z0 = np.array(rd)[0]
    # loop through test angles

    def delays():
        for theta in thetaTest:
            yield delayFromProfile(theta, ssp, rd, z0, dr=dr, cast_xr=False)

    #Transpose to make angle independent variable
    delayArray = np.array(list(delays())).T
    #Allow for optional tilt dimension
    if len(delayArray.shape) == 2:
        testDelay = xr.DataArray(delayArray, coords=[rd, thetaTest], dims=['depth', 'angle'])
    elif len(delayArray.shape) == 3:
        #dimensions are a bit wierd when there is tilt dimension, so swap
        if dr_axis is None:
            dr_axis = np.arange(delayArray.shape[1])
        testDelay = xr.DataArray(delayArray, coords=[rd, dr_axis, thetaTest], dims=['depth', 'tilt', 'angle'])
    else:
        raise Exception('num dimensions not supported')
    #I prefer miliseconds
    if unit == 'ms':
        testDelay *= 1e3
    # subtract delay at reference receiver
    testDelay = testDelay - testDelay.sel(depth=z0)
    return testDelay.squeeze()


def delayFromProfile(theta0, ssp, rd, z0, dr=None, cast_xr=True):
    """Calculate the delay over an array from measured sound speed values"""
    # upsample sound speed sampling, before slowness integration
    zMin = min(np.min(rd), z0)
    zMax = max(np.max(rd), z0)
    upZ = np.r_[zMin: zMax: 1e2 * 1j]
    upSSP = np.interp(upZ, ssp.index, ssp)
    # Calculate vertical slowness
    u = 1 / upSSP
    p = u_r(theta0, z0, ssp)
    #flip array to integrate from bottom
    q = np.sign(theta0) * np.sqrt(u**2 - p**2)
    taus = np.full_like(q, np.nan)
    #leave delays nan where the ray does not go
    activeI = ~np.isnan(q)
    taus[activeI] = cumtrapz(q[activeI], upZ[activeI], initial=0)
    taus = np.interp(np.array(rd)[::-1], upZ, taus)[::-1]
    if cast_xr:
        taus = xr.DataArray(taus, dims=['depth'], coords=[rd])
    # adjust for horizontal offsets
    if dr is not None:
        dr = np.array(dr, ndmin=2)
        taus = taus + p * dr
    return taus


def u_r(theta0, z0, ssp):
    """Calculate conserved horizontal slowness"""
    u0 = 1 / np.interp(z0, ssp.index, ssp)
    p = np.cos(np.radians(theta0)) * u0
    return p


def twoInterfaceDelay(theta0, c0, c1, z0, z1):
    """Calculate time delay between two receivers separated by depth dz
    theta is declination angle of ray
    """
    dz = z1 - z0
    u0 = 1 / c0
    u1 = 1 / c1
    cosT = u0 * np.cos(np.radians(theta0)) / u1
    theta1 = np.degrees(np.arccos(cosT))
    # Match the sign of the propagation angle
    theta1 *= np.sign(theta0)
    q0 = u0 * np.sin(np.radians(theta0))
    q1 = u1 * np.sin(np.radians(theta1))
    return theta1, np.mean((q0, q1)) * dz
