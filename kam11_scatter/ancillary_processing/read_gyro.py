import numpy as np
import matplotlib.pyplot as plt

all_data = np.loadtxt('_km1119_Gyro_gyro_191_raw',
                      usecols=(2, 3, 4, 5, 7))

time_stamp = all_data[:, 0] * 3600 + all_data[:, 1] * 60 + all_data[:, 2] \
             + all_data[:, 3] * 1e-3

heading = all_data[:, -1]


t_start = 21 * 3600 + 34 * 60
exp_i = time_stamp > t_start

np.savez('heading_191', time_stamp=time_stamp[exp_i], heading=heading[exp_i])

fig, ax = plt.subplots()
ax.plot(time_stamp[exp_i], heading[exp_i])

plt.show()
