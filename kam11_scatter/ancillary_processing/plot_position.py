import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from os.path import expanduser

# reference position is UDEL #1
ref_n = 22 + 8.835 / 60
ref_e = -(159 + 48.063 / 60)

# load dir
data_dir = '~/Dropbox/kam11_scatter/ancillary_data/'
file_name = '_km1119_Pos-Mv_pos-mv_191_raw'

# record start time in seconds
#t_start = 21 * 3600 + 46 * 60
#t_end = t_start + 40
t_start = 21 * 3600 + 46 * 60
t_end = t_start + 3 * 3600 - 46 * 60 - 10

all_data = np.loadtxt(expanduser(data_dir + file_name),
                      usecols=(2, 3, 4, 5, 7, 8))
time_stamp = all_data[:, 0] * 3600 + all_data[:, 1] * 60 + all_data[:, 2] \
             + all_data[:, 3] * 1e-3

# only plot position for experiment time
exp_i = np.bitwise_and(time_stamp > t_start, time_stamp < t_end)

pos_ne = all_data[exp_i, -2:]
pos_ne[:, 0] -= ref_n
pos_ne[:, 1] -= ref_e

# convert from degrees to m
pos_ne[:, 0] *= 111319.9
pos_ne[:, 1] *= np.cos(np.radians(ref_n)) * 111319.9

mean_val = np.mean(pos_ne, axis=0)

plot_ne = pos_ne.copy()
plot_ne[:, 0] -= mean_val[0]
plot_ne[:, 1] -= mean_val[1]

points = plot_ne.reshape(-1, 1, 2)
segments = np.concatenate([points[:-1], points[1:]], axis=1)

lc = LineCollection(segments, cmap=plt.get_cmap('copper_r'))
lc.set_array(time_stamp[exp_i])

fig, ax = plt.subplots()
ax.add_collection(lc)
ax.axis('equal')
ax.set_title('Ship motion, J191\n' + '%i s from %02i:%02i:%02i Z'%
             (t_end - t_start,
              t_start // 3600,
              (t_start % 3600) // 60,
              (t_start % 3600) % 60))
ax.set_xlabel('relative position, m')
ax.set_ylabel('relative position, m')
ax.grid()

plt.show(block=False)
