import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
import loadSIO
import peakutils

cmap = plt.cm.magma_r
cmap.set_under(color='w', alpha=0)

# find load file
load_file ='/enceladus0/e2richards/data/KAM11/monitor_data/mon_11191220000.sio'
exjday = load_file.split('/')[-1][6:9]
exhour = load_file.split('/')[-1][9:11]
exmin = load_file.split('/')[-1][11:13]
fs = 100000
f_start = 7000
f_end = 13000
data = loadSIO.load_selection(load_file, -1, -1, 4)['data']

dBthresh = -20
# data spectrogram
spec_win = sig.kaiser(256, 1.5 * np.pi)
sf, st, sxx = sig.spectrogram(data, fs=fs, window=spec_win, scaling='spectrum')
NFFT = int(2 ** np.ceil(np.log2(data.shape[0])))

# create a bandpass filter
ntaps = 2 ** 10
cutoff = [f_start, f_end]
bp_filter = sig.firwin(ntaps, cutoff, window=('kaiser', 1.5*np.pi),
                    pass_zero=False, fs=fs)
filt_data = sig.convolve(data, bp_filter, mode='valid')

# create a signal model
chirp_T = 0.048
total_t = (42.3047 - 2.5592) / 276
chirp_taxis = np.arange(chirp_T * fs) / fs

kaiser_beta = 1.5 * np.pi
fm_modulation = (f_end - f_start) * chirp_taxis / (2 * chirp_T)

chirp = np.cos(2 * np.pi * chirp_taxis * (f_start + fm_modulation))

chirp *= sig.kaiser(chirp_taxis.size, kaiser_beta)
mf_data = sig.convolve(filt_data, chirp[::-1], mode='valid')

# frequency domain signals
d_FT = np.fft.rfft(data, NFFT)
filt_FT = np.fft.rfft(filt_data, NFFT)
sig_FT = np.fft.rfft(chirp, NFFT)

# dB scale for plotting
raw_dB = 20 * np.log10(np.abs(d_FT))
filt_dB = 20 * np.log10(np.abs(filt_FT))
sig_dB = 20 * np.log10(np.abs(sig_FT))

mf_hilb = sig.hilbert(mf_data, NFFT)
raw_hilb = sig.hilbert(filt_data, NFFT)

# create gridded data
num_xt = int(total_t * fs)
num_xmission = mf_hilb.size // num_xt

grid_off = 7000
#grid_off = 0
grid_cir = np.reshape(mf_hilb[grid_off: num_xt * (num_xmission - 1) + grid_off],
                      (num_xmission - 1, num_xt))
grid_dB = 20 * np.log10(np.abs(grid_cir))
grid_dB -= np.max(grid_dB)

# remove non xmission data
tmpi = int(20 / total_t)  # somewhere in middle of xmission
iend = np.argmax(np.max(grid_dB[tmpi: , :], axis=1) < -10) + tmpi
grid_dB = grid_dB[: iend, :]

# basic line up procedure. Subtract first arrival time in last xmission from
# first arrival time in first xmission to get a linear slope correction.
xstarti = np.argmax(np.max(grid_dB, axis=1) > dBthresh)
# find index of first arrival, first xmission
sind = peakutils.indexes(grid_dB[xstarti, :])
sinds = sind[grid_dB[xstarti, sind] > dBthresh][0]
# find index of first arrival, last xmission
sind = peakutils.indexes(grid_dB[-1, :])
sinde = sind[grid_dB[-1, sind] > dBthresh][0]
numping = grid_dB.shape[0] - xstarti

# remove linear trend from data
xtstart = min(sinds, sinde)
ixplot = (xtstart, int(xtstart + 25e-3 * fs))  # ms
ploti = np.arange(ixplot[0] - 500, ixplot[1] + 500)

delt = (sinde - sinds) / numping
demean_dB = [grid_dB[i + xstarti, ploti + int(i * delt)]
             for i in range(numping)]
demean_dB = np.array(demean_dB)

X, T = np.meshgrid((ploti - sinds) / fs,
                   (np.arange(numping) + xstarti) * total_t)

fig, ax = plt.subplots()
cm = ax.pcolormesh(X * 1e3, T, demean_dB, vmin=-30, cmap=cmap)
ax.set_xlim(-1, 20)
ax.grid(color='0.8')
ax.set_xlabel('delay re first arrival, ms')
ax.set_ylabel('experiment time, s')
ax.set_title('Channel impulse response measured from UDel tripod\n'
        + 'J day %s, %s:%s Zulu'%(exjday, exhour, exmin))
cbar = fig.colorbar(cm)
cbar.set_label('dB re max')

ax.set_axisbelow(True)
plt.show(block=False)
