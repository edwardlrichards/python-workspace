import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from os.path import expanduser

# load dir
data_dir = '~/Dropbox/kam11_scatter/ancillary_data/'
file_name = 'ship_position.npz'

def calculate_range(xmitt_num, t_start, t_end):
    """calculate range to ship from each tripod, t_values in sec re 00:00:00"""
    if xmitt_num == 1:
        ref_n = 22 + 8.835 / 60
        ref_e = -(159 + 48.063 / 60)
    elif xmitt_num == 2:
        ref_n = 22 + 8.818 / 60
        ref_e = -(159 + 48.214 / 60)
    else:
        raise(ValueError('Transmit number must be 1 or 2'))

    all_data = np.load(expanduser(data_dir + file_name))
    time_stamp = all_data['time_stamp']
    pos_ne = all_data['pos_ne']

    pos_ne[:, 0] -= ref_n
    pos_ne[:, 1] -= ref_e

    # convert from degrees to m
    pos_ne[:, 0] *= 111319.9
    pos_ne[:, 1] *= np.cos(np.radians(ref_n)) * 111319.9

    dist_rect = np.linalg.norm(pos_ne, axis=-1)

    # only plot position for experiment time
    exp_i = np.bitwise_and(time_stamp > t_start, time_stamp < t_end)
    return time_stamp[exp_i], dist_rect[exp_i]
