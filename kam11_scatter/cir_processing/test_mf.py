import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig

fs = 100000

# contruct a chirp
chirp_T = 0.048
f_start = 7000
f_end = 14000

taxis = np.arange(10000) / fs
chirp_i = taxis <= chirp_T

fm_modulation = (f_end - f_start) * taxis[chirp_i] / (2 * chirp_T)
chirp = np.sin(2 * np.pi * taxis[chirp_i] * (f_start + fm_modulation))
test_signal = np.zeros_like(taxis)
test_signal[np.where(chirp_i)[0] + 3000] = chirp

mf_out = np.convolve(test_signal, chirp[:: -1], mode='valid')

chirp *= sig.kaiser(chirp.size, 1.5 * np.pi)

test_signal = np.zeros_like(taxis)
test_signal[np.where(chirp_i)[0] + 3000] = chirp

# matched filter
mf_out = np.convolve(test_signal, chirp[:: -1], mode='valid')

NFFT = int(2 ** np.ceil(np.log2(taxis.size) + 1))
faxis = np.arange(NFFT // 2 + 1) / NFFT * fs

mf_hilb = sig.hilbert(mf_out, NFFT)

fig, ax = plt.subplots()
ax.plot(faxis, np.abs(np.fft.rfft(chirp, NFFT)))
ax.grid()
ax.set_xlim(5000, 18000)

fig, ax = plt.subplots()
ax.plot(taxis[chirp.size - 1: ] * 1e3, mf_out)
ax.plot((taxis[chirp.size - 1] + np.arange(NFFT) / fs) * 1e3, np.abs(mf_hilb))
ax.grid()
ax.set_xlim(77, 79)

plt.show()
