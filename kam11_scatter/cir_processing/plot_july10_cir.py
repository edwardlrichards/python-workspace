import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
import loadSIO
import correct_position
import re

cmap = plt.cm.magma_r
cmap.set_under(color='w', alpha=0)

# find load file
sio_dir = '/enceladus0/e2richards/data/KAM11/monitor_data/'
#file_name ='mon_11191214600.sio'
#file_name ='mon_11191214800.sio'
#file_name ='mon_11191215000.sio'
#file_name ='mon_11191215200.sio'
#file_name ='mon_11191215400.sio'
#file_name ='mon_11191215600.sio'
#file_name ='mon_11191215800.sio'
#file_name ='mon_11191220000.sio'
#file_name ='mon_11191220400.sio'
file_name ='mon_11191220800.sio'
#file_name ='mon_11191230000.sio'
#file_name ='mon_11192000000.sio'
#file_name ='mon_11191220200.sio'
#file_name ='mon_11191220400.sio'

fs = 100000
f_start = 7000
f_end = 13000


# parse data time
load_file = sio_dir + file_name
time_stamp = re.findall('\d+', load_file)[-1]
min_num = int(time_stamp[-4: -2])

if min_num % 4 == 0 and min_num % 2 == 0:
    xmitt_num = 1
elif min_num % 4 == 2 and min_num % 2 == 0:
    xmitt_num = 2
else:
    raise(ValueError('Chirp on even minute only'))

dt_sec = float(time_stamp[-6: -4]) * 3600 + float(min_num) * 60

t_start = 2.5  # seconds
t_end = 42.4  # seconds

data = loadSIO.load_selection(load_file, -1, -1, 4)['data']
t_buff = 150 / 2
ship_range_t, ship_range = correct_position.calculate_range(xmitt_num,
                                                            dt_sec + t_start - t_buff,
                                                            dt_sec + t_end + t_buff)
ship_range_t -= dt_sec

# total transmission range
xmitt_range = np.sqrt(ship_range ** 2 + 80 ** 2)
xmitt_dtau = xmitt_range / 1525
xmitt_dtau -= xmitt_dtau[0]

#smooth_dtau = sig.savgol_filter(xmitt_dtau, 15, 2)
ship_fs = 2
lp_filt = sig.remez(256, [0, .05, .1, 1], [1, 0], fs=ship_fs)
smooth_dtau = sig.convolve(xmitt_dtau - np.mean(xmitt_dtau),
                           lp_filt, mode='same') + np.mean(xmitt_dtau)

# create a bandpass filter
ntaps = 2 ** 10
cutoff = [f_start, f_end]
bp_filter = sig.firwin(ntaps, cutoff, window=('kaiser', 1.5*np.pi),
                       pass_zero=False, fs=fs)
filt_data = sig.convolve(data, bp_filter, mode='valid')

# create a signal model
chirp_T = 0.048
num_xmission = 276 # infered from time series
total_t = (42.304 - 2.559) / num_xmission
#total_t = (42.304735 - 2.55925) / num_xmission
chirp_taxis = np.arange(chirp_T * fs) / fs

kaiser_beta = 1.5 * np.pi
fm_modulation = (f_end - f_start) * chirp_taxis / (2 * chirp_T)

chirp = np.sin(2 * np.pi * chirp_taxis * (f_start + fm_modulation))

chirp *= sig.kaiser(chirp_taxis.size, kaiser_beta)
mf_data = sig.convolve(filt_data, chirp[::-1], mode='valid')

NFFT = int(2 ** np.ceil(np.log2(data.size)))

mf_hilb = sig.hilbert(mf_data, NFFT)

# create gridded data
start_i_offset = 300
# find rough start index
start_i = np.where(np.abs(mf_hilb) > 10)[0][0] - start_i_offset
start_t = (ntaps + chirp.size + start_i - 2) / fs

num_xt = int(total_t * fs)  # decimated number of samples

grid_cir = np.zeros((num_xmission, num_xt), dtype=np.complex_)

for i in range(num_xmission):
    si = int(total_t * i * fs + start_i)
    grid_cir[i, :] = mf_hilb[si: si + num_xt]

grid_dB = 20 * np.log10(np.abs(grid_cir))
grid_dB -= np.max(grid_dB)

X, T = np.meshgrid(np.arange(num_xt) / fs,
                   np.arange(num_xmission) * total_t + start_t)

# interpolate ship distance values
xtaus = np.interp(T[:, 0], ship_range_t, smooth_dtau)
xtaus -= xtaus[0]

test_dB = np.zeros_like(grid_cir)
for i, x in enumerate(xtaus):
    dt = int(x * fs)
    si = int(total_t * i * fs + start_i)
    test_dB[i, :] = mf_hilb[si + dt: si + num_xt + dt]
test_dB = 20 * np.log10(np.abs(test_dB) + np.spacing(1))
test_dB -= np.max(test_dB)

fig, ax = plt.subplots()
cm = ax.pcolormesh(X * 1e3, T, test_dB, vmin=-30, cmap=cmap)
#ax.set_ylim(2.5, 42.4)
ax.set_ylim(42.4, 2.5)
ax.set_xlim(0, 25)
ax.grid(color='0.4')
ax.set_title('Channel impulse response, ship position correction')
ax.set_axisbelow(True)
fig.colorbar(cm)

fig, ax = plt.subplots()
cm = ax.pcolormesh(X * 1e3, T, grid_dB, vmin=-30, cmap=cmap)
#ax.set_ylim(2.5, 42.4)
ax.set_ylim(42.4, 2.5)
ax.set_xlim(0, 25)
ax.grid(color='0.4')
ax.set_title('Channel impulse response, J%s %s:%s Z'%(
    time_stamp[2: 5], time_stamp[5: 7], time_stamp[7: 9]))
ax.set_axisbelow(True)
fig.colorbar(cm)

fig, ax = plt.subplots()
ax.plot(ship_range_t, xmitt_dtau * 1e3, label='raw')
ax.plot(ship_range_t, smooth_dtau * 1e3, label='smooth')
ax.set_xlabel('Time, s')
ax.set_ylabel('Relative delay, ms')
ax.set_title('Range based delay, J%s %s:%s Z'%(
    time_stamp[2: 5], time_stamp[5: 7], time_stamp[7: 9]))
ax.grid()
plt.show(block=False)
