import numpy as np
import matplotlib.pyplot as plt
import sys,os,os.path
import scipy.signal as sig
import loadSIO

# find load file
load_file ='/Users/edwardlrichards/Documents/kam11_scatter/sio_files/mon_11187043800.sio'
fs = 100000
f_start = 22000
f_end = 28000
data = loadSIO.load_selection(load_file, -1, -1, 4)['data']

NFFT = int(2 ** np.ceil(np.log2(data.shape[0])))

# create a bandpass filter
ntaps = 2 ** 10
cutoff = [f_start, f_end]
bp_filter = sig.firwin(ntaps, cutoff, window=('kaiser', 2.5*np.pi),
                    pass_zero=False, fs=fs)
filt_data = sig.convolve(data, bp_filter, mode='valid')

# create a signal model
chirp_T = 0.048
chirp_taxis = np.arange(chirp_T * fs) / fs

kaiser_beta = 2.5 * np.pi
chirp = np.cos(2 * np.pi * chirp_taxis
               * (f_start + (f_end - f_start) / chirp_T / 2 * chirp_T))
chirp *= sig.kaiser(chirp_taxis.size, kaiser_beta)
mf_data = sig.convolve(filt_data, chirp[::-1], mode='valid')

# frequency domain signals
d_FT = np.fft.rfft(data, NFFT)
win_FT = np.fft.rfft(bp_filter, NFFT)
filt_FT = np.fft.rfft(filt_data, NFFT)
sig_FT = np.fft.rfft(chirp, NFFT)

# dB scale for plotting
win_dB = 20 * np.log10(np.abs(win_FT + np.spacing(1)))
raw_dB = 20 * np.log10(np.abs(d_FT))
filt_dB = 20 * np.log10(np.abs(filt_FT))
sig_dB = 20 * np.log10(np.abs(sig_FT))

mf_hilb = sig.hilbert(mf_data, NFFT)
raw_hilb = sig.hilbert(filt_data, NFFT)

# plotting axes
taxis = np.arange(data.shape[0]) / fs
faxis = np.arange(NFFT // 2 + 1) / NFFT * fs
filt_taxis = taxis[ntaps - 1: ]
mf_taxis = np.arange(NFFT) / fs

fig, ax = plt.subplots()
#ax.plot(filt_taxis, filt_data)
ax.plot(filt_taxis[chirp.size - 1: ] - filt_taxis[chirp.size - 1], mf_data)
ax.plot(mf_taxis, np.abs(mf_hilb))

fig, ax = plt.subplots()
ax.plot(mf_taxis, np.abs(raw_hilb))

fig, ax = plt.subplots()
ax.plot(faxis, raw_dB)
ax.plot(faxis, filt_dB)
#ax.plot(faxis, win_dB + np.max(filt_dB))
ax.set_ylim(20, 70)

plt.show()
