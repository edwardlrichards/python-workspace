import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hilbert
from icepyks.porty import point_iso_field
from icepyks import pulse_signal, signal_interp, surface_integral
from icepyks.surfaces import sin_surface

cmap = plt.cm.magma_r
cmap.set_under(color='w', alpha=0)

# Perform HK integration using a sine surface
# Sine wave parameters
lamda_surf = 40
wave_height = 2
num_phase = 120
wave_phase = np.r_[0: 2 * np.pi: num_phase * 1j]

wave_T = np.sqrt(2 * np.pi * lamda_surf / 9.81)
wave_time = wave_T * wave_phase / (2 * np.pi)

c0 = 1525
z_source = -105
r_receiver = np.array((500., 0, -25.))
xbounds = (-10, 510)

spec_r = np.sqrt((z_source + r_receiver[2]) ** 2 + r_receiver[0] ** 2
                 + r_receiver[1] ** 2)
t_spec = spec_r / c0

fs = 1e5
taxis = np.arange(int(20e-3 * fs)) / fs + t_spec - 1e-3
dec = c0 / 25000 / 8
xaxis = np.arange(int(np.diff(xbounds) / dec)) * dec + xbounds[0]

# solve for pressure field at surface
eta = sin_surface.Sine(lamda_surf, wave_height)
iso_field = point_iso_field.IsospeedPoint(eta, z_source, c=c0)

y_signal, t_signal = pulse_signal.pulse_udel()
i_er_signal = signal_interp.ContinuousSignal(y_signal, t_signal)

t_series = []

for p in wave_phase:
    iso_field.eta.phase = p
    hk_time = surface_integral.TimeHK(iso_field, xaxis, taxis, i_er_signal)
    t_series.append(hk_time(r_receiver))

t_series = np.array(t_series)

NFFT = int(2 ** np.ceil(np.log2(taxis.size)))


t_dB = 20 * np.log10(np.abs(hilbert(t_series, N=NFFT, axis=-1)
                     + np.spacing(1)))

t_dB = t_dB[:, :taxis.size]

# stack up to 40 seconds of data
num_reps = int((40 // wave_T) + 1)
t_exp = np.hstack([wave_time + wave_T * i for i in range(num_reps)])
t_dB = np.tile(t_dB, (num_reps, 1))

t_exp, t_xmitt = np.meshgrid(np.arange(num_phase), t_exp)

vmax = 5 * (np.max(t_dB) // 5 - 1)

fig, ax = plt.subplots()
cm = ax.pcolormesh(t_exp, t_xmitt, t_dB.T, cmap=cmap, vmin=vmax - 30, vmax=vmax)

fig.colorbar(cm)

plt.show(block=False)
