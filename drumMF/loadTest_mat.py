import numpy as np
import pandas as pd
import scipy.io
from scipy.signal import welch, resample
import glob
import os
import pickle


class LocationForcing(object):
    """
    Load readings from a single speaker placement
    """

    def __init__(self, dirPath, fs, readingRange=None):
        """Load from pickle if avalible"""
        self.dirList = glob.glob(os.path.join(dirPath, '*dataout*.mat'))
        self.fs = fs
        _, experimentName = os.path.split(dirPath)
        if experimentName == '':
            _, experimentName = os.path.split(dirPath)
        pickleDir = os.path.join(os.path.dirname(__file__), 'processedData')
        self.pickleFile = os.path.join(pickleDir, experimentName)
        self.readings, self.index = self.loadExperiments(readingRange)
        try:
            self.response = pd.read_pickle(self.pickleFile)
        except FileNotFoundError:
            self.response = None

    def loadExperiments(self, readingRange):
        def parseIndex(name):
            """get the index number from file name"""
            indexNumber = ''
            _, fileName = os.path.split(name)
            for s in (s for s in fileName if s.isdigit()):
                indexNumber += s
            return int(indexNumber)
        readings = []
        dirIndex = [parseIndex(i) for i in self.dirList]

        if readingRange is None:
            readings = [LaserReading(i, self.fs) for i in self.dirList]
            index = dirIndex
        else:
            try:
                readings = [LaserReading(self.dirList[dirIndex.index(i)],
                                         self.fs) for i in readingRange]
                index = [dirIndex.index(i) for i in readingRange]
            except TypeError:
                readings = LaserReading(self.dirList[
                    dirIndex.index(readingRange)], self.fs)
                index = dirIndex.index(readingRange)
        return readings, index

    def getSignal(self, testFile=0, outSignal=True):
        """
        Return the signal from test. outSignal is False for input
        """
        timeSeries = LaserReading(self.dirList[testFile],
                                  self.fs).getTimeSeries(outSignal)
        return timeSeries

    def calculateResponseAtF(self, f, df, method='DFT'):
        """
        load the max value of each reading in the range f+- df

        Method returns a response array
        """
        # TODO: include frequency bin data
        if method == 'DFT':
            response = np.array([i.getMaxInSpan(f, df, i.getDFT())
                                 for i in self.readings])
        elif method == 'Welch':
            response = np.array([i.getMaxInSpan(f, df, i.getWelch())
                                 for i in self.readings])
        self.response = pd.Series(np.squeeze(response),
                                  index=self.index).sort_index()
        self.response.to_pickle(self.pickleFile)


class LaserReading(object):
    """
    Read a laser vibrometer measure from mat file
    """

    def __init__(self, matFilePath, fs):
        self.getTimeSeries = self._setupProcessing(matFilePath, fs)
        self.number = self._getIndexNumber(matFilePath)

    def _getIndexNumber(self, matFilePath):
        """
        Parse file index number from name
        """
        dirParse = matFilePath.split('/')
        fileNumber = list(filter(str.isdigit, dirParse[-1]))
        fileNumber = ''.join(fileNumber)
        return int(fileNumber)

    def _setupProcessing(self, matFilePath, fs):
        """
        load data on command
        """
        def getTimeSeries(outSignal=True):
            response = self._loadData(matFilePath, fs, outSignal)
            return response
        return getTimeSeries

    def _loadData(self, matFilePath, fs, outSignal=True):
        """
        load mat file, return data after trigger
        """
        try:
            mat = scipy.io.loadmat(matFilePath)
        except ValueError:
            print('Possible corruption in file', matFilePath)
            print('Loading anyways')
            mat = scipy.io.loadmat(matFilePath,
                                   verify_compressed_data_integrity=False)

        signal = mat['AISignal']
        trigerSample = np.argmax(signal[0, :] > 0.5)
        # Flag to process input signal
        if outSignal:
            drumResponse = signal[2, trigerSample:]
        else:
            drumResponse = signal[1, trigerSample:]
        t = np.arange(drumResponse.size) / float(fs)
        result = pd.Series(drumResponse, index=t)
        return result

    def matchedFilter(self):
        """ Return match field result
        """
        ts = self.getTimeSeries()
        t = ts.index
        replica = self.getTimeSeries(outSignal=False)
        mf = np.convolve(np.array(ts), np.array(replica)[::-1], "full")
        tAxis = np.concatenate((-t[::-1], t[:-1]))
        result = pd.Series(mf, index=tAxis)
        return result

    def getDFT(self):
        """
        return the DFT of data
        """
        drumResponse = self.getTimeSeries()
        t = drumResponse.index
        dT = t[1] - t[0]
        NFFT = 2 ** np.ceil(np.log2(drumResponse.size))
        drumF = np.fft.fft(drumResponse - np.mean(drumResponse), int(NFFT))
        fNorm = np.arange(NFFT) / NFFT
        f = fNorm / dT
        result = pd.Series(drumF, index=f)
        return result

    def getWelch(self):
        """
        welch PSD estimate
        """
        drumResponse = self.getTimeSeries()
        t = drumResponse.index
        dT = t[1] - t[0]
        # TODO: Make welch parameters inputs
        numSections = 8
        sectionLength = (t.size // (numSections / 2))
        nfft = 2 ** np.ceil(np.log2(sectionLength))
        f, Pxx = welch(drumResponse, 1 / dT, nperseg=sectionLength,
                       nfft=nfft)
        result = pd.Series(Pxx, index=f)
        return result

    def getMaxInSpan(self, centerF, df, Y):
        """
        find the max frequency value within f +- df
        """
        centers = np.array(centerF, ndmin=1)
        lb = centers - df
        ub = centers + df
        maxF = [Y[np.argmax(np.abs(Y[lb[i]: ub[i]]))]
                for i in np.arange(centers.size)]
        return maxF


class ReducedLocationForcing(LocationForcing):

    def __init__(self, dirPath, fs, tStart=0, tEnd=-1, fResample=None):
        super().__init__(dirPath, fs)
        self.tStart = tStart
        self.tEnd = tEnd
        self.fResample = fResample

    def loadExperiments(self, readingRange=None):
        """ Load experiments with ReducedLaserReading object
        """
        def loader(filePath):
            fac = ReducedLaserReading(filePath, self.fs, self.tStart,
                                      self.tEnd, self.fResample)
            return fac

        if readingRange is None:
            readings = [loader(i) for i in self.dirList]
        else:
            try:
                readings = [loader(self.dirList[i]) for i in readingRange]
            except TypeError:
                readings = loader(self.dirList[readingRange])
        return readings


class ReducedLaserReading(LaserReading):
    """ Allow for time to be windowed, and for a resample
    """

    def __init__(self, matFilePath, fs, tStart=0, tEnd=-1, fResample=None):
        super().__init__(matFilePath, fs)
        self.getFullSeries = self.getTimeSeries
        self.getTimeSeries = self._setupTimeSeries(tStart, tEnd, fResample)

    def _setupTimeSeries(self, tStart, tEnd, fResample):
        def modifiedDataLoad(tStart=tStart, tEnd=tEnd,
                             fResample=fResample, outSignal=True):
            ts = self.getFullSeries(outSignal=outSignal)
            # Clip the time series, index is in seconds unless tEnd = -1
            if tEnd < tStart:
                tEnd = ts.index[-1]
            ts = ts[tStart:tEnd]
            if fResample is not None:
                ts = self._resampleData(ts, fResample)
            return ts
        return modifiedDataLoad

    def _resampleData(self, timeSeries, fResample):
        """ change sample rate of data with numpy function
        """
        fs = 1 / np.diff(timeSeries.index[0:2])
        numPoints = timeSeries.size
        resampleNumPoints = np.floor(numPoints * (fResample / fs))
        tsR, tR = resample(timeSeries, resampleNumPoints, t=timeSeries.index)
        result = pd.Series(tsR, index=tR)
        return result
