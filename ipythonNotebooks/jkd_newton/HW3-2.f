c
c
c
c
c
c
	integer indx(3), N

	real x(3), jacob(3,3), f(3), dx(3), b(3)
	
c	data a /1, 3, 2, 2, 1, 3, 3, 2, 1/
	
	data x /2.7, 0.4, 1.1/

	N=3
	 
c	print 100, ((jacob(i,j),j=1,3),i=1,3)
c	print 100, (f(i),j=1,3)

	do l = 1,10

	f(1) = x(1) + x(2) + x(3) - 4.
	f(2) = x(1)*x(2) + x(3)**2 - 2.
	f(3) = x(1)*x(2)*x(3) - 1.

	jacob(1,1) = 1.
	jacob(1,2) = 1.
	jacob(1,3) = 1.
	jacob(2,1) = x(2)
	jacob(2,2) = x(1)
	jacob(2,3) = 2.*x(3)
	jacob(3,1) = x(2)*x(3)
	jacob(3,2) = x(1)*x(3)
	jacob(3,3) = x(1)*x(2)
	
		do i = 1,N
		b(i) = -f(i)
		end do

	call ludcmp(jacob,N,N,indx,D)

	call lubksb(jacob,N,N,indx,b)

		do i = 1,N
		dx(i) = b(i)
		x(i) = x(i) + dx(i)
		end do

	print *
	print *, 'l=', l
	print 100, x
	print 200, f

	errmax = 0
		do j = 1,N
		err = abs(dx(j)/x(j))
		errmax = amax1(errmax,err)
		end do
	
	if (errmax < 1.e-5) exit
	
	end do

100 	format('x =',3F8.5)
200     format('f =',3F8.5)

	end

      SUBROUTINE LUDCMP(A,N,NP,INDX,D)
      PARAMETER (NMAX=100,TINY=1.0E-20)
      DIMENSION A(NP,NP),INDX(N),VV(NMAX)
      D=1.
      DO 12 I=1,N
        AAMAX=0.
        DO 11 J=1,N
          IF (ABS(A(I,J)).GT.AAMAX) AAMAX=ABS(A(I,J))
11      CONTINUE
        IF (AAMAX.EQ.0.) PAUSE 'Singular matrix.'
        VV(I)=1./AAMAX
12    CONTINUE
      DO 19 J=1,N
        IF (J.GT.1) THEN
          DO 14 I=1,J-1
            SUM=A(I,J)
            IF (I.GT.1)THEN
              DO 13 K=1,I-1
                SUM=SUM-A(I,K)*A(K,J)
13            CONTINUE
              A(I,J)=SUM
            ENDIF
14        CONTINUE
        ENDIF
        AAMAX=0.
        DO 16 I=J,N
          SUM=A(I,J)
          IF (J.GT.1)THEN
            DO 15 K=1,J-1
              SUM=SUM-A(I,K)*A(K,J)
15          CONTINUE
            A(I,J)=SUM
          ENDIF
          DUM=VV(I)*ABS(SUM)
          IF (DUM.GE.AAMAX) THEN
            IMAX=I
            AAMAX=DUM
          ENDIF
16      CONTINUE
        IF (J.NE.IMAX)THEN
          DO 17 K=1,N
            DUM=A(IMAX,K)
            A(IMAX,K)=A(J,K)
            A(J,K)=DUM
17        CONTINUE
          D=-D
          VV(IMAX)=VV(J)
        ENDIF
        INDX(J)=IMAX
        IF(J.NE.N)THEN
          IF(A(J,J).EQ.0.)A(J,J)=TINY
          DUM=1./A(J,J)
          DO 18 I=J+1,N
            A(I,J)=A(I,J)*DUM
18        CONTINUE
        ENDIF
19    CONTINUE
      IF(A(N,N).EQ.0.)A(N,N)=TINY
      RETURN
      END
      SUBROUTINE LUBKSB(A,N,NP,INDX,B)
      DIMENSION A(NP,NP),INDX(N),B(N)
      II=0
      DO 12 I=1,N
        LL=INDX(I)
        SUM=B(LL)
        B(LL)=B(I)
        IF (II.NE.0)THEN
          DO 11 J=II,I-1
            SUM=SUM-A(I,J)*B(J)
11        CONTINUE
        ELSE IF (SUM.NE.0.) THEN
          II=I
        ENDIF
        B(I)=SUM
12    CONTINUE
      DO 14 I=N,1,-1
        SUM=B(I)
        IF(I.LT.N)THEN
          DO 13 J=I+1,N
            SUM=SUM-A(I,J)*B(J)
13        CONTINUE
        ENDIF
        B(I)=SUM/A(I,I)
14    CONTINUE
      RETURN
      END
