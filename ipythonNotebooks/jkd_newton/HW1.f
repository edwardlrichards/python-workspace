c HW 1 Newton's Method
c
c
      real x,f,fp,error
      data  x/0.1/, M/10/
      error = 1.e-5
c
      print *
      print *,' Newton Method: HW 1'
      print *
c
c      fx = f(x)
      print 3
      print 4,0.1,x,fx
      do k = 1,M
	f = x**2 - x -2
	fp = 2*x - 1
	dx = f/fp
c
        x = x - dx
	t = abs(dx/x)
         print *,'k=',k,' x=',x,' fx=',f 
	 if (t < error) exit
c
 2    continue   
c
 3    format (3x,' k',12x,'x',21x,'f(x)')
 4    format(2x,i3,2x,d22.15,2x,d22.15)  
      end do

      stop
      end 
