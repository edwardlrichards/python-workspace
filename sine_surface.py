from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import os

save_name = 'exp_coordinate.svg'
save_dir = '/Users/edwardlrichards/Documents/scattering-report/figures/'
cmap = plt.cm.Blues_r
fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.arange(-2, 18, 0.25)
Y = np.arange(-5, 5, 0.25)
X, Y = np.meshgrid(X, Y)
k = 2 * np.pi / 5
Z = 0.5 * np.sin(k * X)
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cmap,
                       linewidth=0, antialiased=False, vmax=1.5,
                       vmin = -1.5, alpha=1)
source_width = 1
z_src = -8
u = np.linspace(0, 2 * np.pi, 100)
v = np.linspace(0, np.pi, 100)
rad = 0.5
x = rad * np.outer(np.cos(u), np.sin(v))
y = rad * np.outer(np.sin(u), np.sin(v))
z = rad * np.outer(np.ones(np.size(u)), np.cos(v))
surf = ax.plot_surface(x, y, z + z_src, rstride=10, cstride=10,
                       linewidth=0, color='g')
x_rcr = 15
z_rcr = -5
surf = ax.plot_surface(x + x_rcr, y, z + z_rcr, rstride=10, cstride=10,
                       linewidth=0, color='k', alpha=0.8)

ax.set_zlim(-10, 1.01)
ax.set_xticks([])
ax.set_yticks([])
ax.set_zticks([])
ax.azim = -70
ax.elev = 10
plt.show()
fig.savefig(os.path.join(save_dir, save_name))
